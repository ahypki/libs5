package net.hypki.libs5.reports.jasper.unittests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.hypki.libs5.utils.system.ExecutingLinuxCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import org.junit.Test;
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperPrint;

public class JasperUnitTest {
	
	@Test
	public void testPrint() throws Exception {

		try {
			// Get jasper report
			String jrxmlFileName = 	"$HOME/temp/rap/Raport14.jrxml";
			String jasperFileName = "$HOME/temp/rap/Raport14.jasper";
			String pdfFileName = 	"$HOME/temp/rap/Raport14.pdf";
	
			JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
	
			// Create arguments
			List list = new ArrayList();
			HashMap<String, Object> hm = new HashMap<String, Object>();
			hm.put("WystMiasto", "[łżąśźćęóń]");
			list.add(hm);
	
			// Generate jasper print
			JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperFileName, hm, new JRMapCollectionDataSource(list));
	
			// Export pdf file
			JasperExportManager.exportReportToPdfFile(jprint, pdfFileName);
			
			ExecutingLinuxCommand.executeInSeparateThread(new String[]{"evince", pdfFileName});
	
			System.out.println("Done exporting reports to pdf");
		} catch (Exception e) {
			System.out.println(e);
			throw e;
		}
	}
}
