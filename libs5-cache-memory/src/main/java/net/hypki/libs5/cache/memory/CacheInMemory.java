package net.hypki.libs5.cache.memory;

import net.hypki.libs5.cache.CacheProvider;
import net.hypki.libs5.utils.collections.TripleMap;

public class CacheInMemory implements CacheProvider {
	
	private TripleMap<String, String, Object> cache = new TripleMap<String, String, Object>();
	
	@Override
	public <T> T get(Class<T> clazz, String cacheName, String key) {
		Object obj = cache.get(cacheName, key);
		return obj != null ? (T) obj : null;
	}
	
	@Override
	public void put(String cacheName, String key, Object obj) {
		cache.put(cacheName, key, obj);
	}
	
	@Override
	public void clearCache(String cacheName) {
		if (cache.get(cacheName) != null)
			cache.get(cacheName).clear();
	}
	
	@Override
	public void remove(String cacheName, String key) {
		cache.get(cacheName).remove(key);
	}
	
	@Override
	public void close() {
		
	}
}
