package net.hypki.libs5.cache.memory.unittests;

import net.hypki.libs5.cache.memory.CacheInMemory;
import net.hypki.libs5.utils.tests.LibsTestCase;

public class CacheInMemoryUnitTest extends LibsTestCase {
	
	@org.junit.Test
	public void testSetGet() {
		CacheInMemory cacheManager = new CacheInMemory();
		
		cacheManager.put("cache1", "one", "two");
		
		String fromCache = cacheManager.get(String.class, "cache1", "one");
		assertTrue(fromCache.equals("two"));
	}
}
