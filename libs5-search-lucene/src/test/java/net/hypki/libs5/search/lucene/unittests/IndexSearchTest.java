package net.hypki.libs5.search.lucene.unittests;

import java.io.IOException;

import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.lucene.LuceneSearchEngineProvider;
import net.hypki.libs5.search.lucene.TestObject;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class IndexSearchTest extends LibsTestCase {
	
	@Test
	public void testIndex() throws IOException {
		LuceneSearchEngineProvider lucene = new LuceneSearchEngineProvider();
		
		TestObject t = new TestObject("jeden", 1);
		
		lucene.indexDocument("index", "type", "id", JsonUtils.objectToString(t));
		
		SearchResults<TestObject> res = lucene.search(TestObject.class, "index", "type", new Query().addTerm(new WildcardTerm("title", "jed")), 0, 10);
		for (TestObject to : res.getObjects()) {
			LibsLogger.debug(IndexSearchTest.class, "Found ", to.getTitle());
		}
		
		lucene.clearIndexType("index", "type");
		takeANap(1000);
		
		res = lucene.search(TestObject.class, "index", "type", new Query().addTerm(new WildcardTerm("title", "jed")), 0, 10);
		for (TestObject to : res.getObjects()) {
			LibsLogger.debug(IndexSearchTest.class, "Found ", to.getTitle());
		}
	}
}
