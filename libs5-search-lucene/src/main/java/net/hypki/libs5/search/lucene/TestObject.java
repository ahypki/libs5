package net.hypki.libs5.search.lucene;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.annotations.Expose;

public class TestObject {
	
	@Expose
	private UUID id = null;

	@Expose
	private String title = null;
	
	@Expose
	private int value = 0;
	
	@Expose
	private double doublevalue = 0.0;
	
	public TestObject() {
		setId(UUID.random());
	}
	
	public TestObject(String title) {
		setId(UUID.random());
		setTitle(title);
	}
	
	public TestObject(String title, int value) {
		setId(UUID.random());
		setTitle(title);
		setValue(value);
	}
	
	public String getData() {
		return JsonUtils.objectToString(this);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public double getDoublevalue() {
		return doublevalue;
	}

	public TestObject setDoublevalue(double doublevalue) {
		this.doublevalue = doublevalue;
		return this;
	}
}
