package net.hypki.libs5.search.lucene;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.BooleanTerm;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.DoubleTerm;
import net.hypki.libs5.search.query.IntTerm;
import net.hypki.libs5.search.query.LongRangeTerm;
import net.hypki.libs5.search.query.LongTerm;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.SortType;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermComparator;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;

import org.apache.commons.lang.NotImplementedException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.IntPoint;
//import org.apache.lucene.document.LegacyLongField;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.IndexSearcher;
//import org.apache.lucene.search.LegacyNumericRangeQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherFactory;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortedNumericSortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.AlreadyClosedException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import com.google.gson.JsonElement;

public class LuceneSearchEngineProvider implements SearchEngineProvider {
	
	public final String INIT_DIRECTORY = "directory";
	
	private static StandardAnalyzer analyzer = null;
	
	private static Directory index = null;

	private static IndexWriterConfig config = null;
	
	private static IndexWriter indexWriter = null;
	
	private static IndexReader indexReader = null;
	
	private static long lastReaderReload = System.currentTimeMillis();
	
	private static final int MAX_RESULTS = 1000000;
	
	private static int indexed = 0;
	
	private static SearcherManager searcherManager = null;
	
	private static final String luceneDirectoryDefault = "lucene-index";
	
	private String luceneDirectory = null;
	
	private boolean closing = false;
	
	private SearcherManager getSearcherManager() throws IOException {
		if (searcherManager == null) {
			try {
				searcherManager = new SearcherManager(getDirectory(), new SearcherFactory());
			} catch (AlreadyClosedException e) {
				LibsLogger.error(LuceneSearchEngineProvider.class, "Already close", e);
				close();
			}
		} else if ((System.currentTimeMillis() - lastReaderReload) > 1000) {
			try {
				lastReaderReload = System.currentTimeMillis();
				searcherManager.maybeRefresh();
			} catch (AlreadyClosedException e) {
				LibsLogger.error(LuceneSearchEngineProvider.class, "Already close", e);
				close();
			}
		}
		return searcherManager;
	}
		
	private StandardAnalyzer getAnalyzer() {
		if (analyzer == null) {
			analyzer = new StandardAnalyzer();
//			analyzer.setMaxTokenLength(1000000);
		}
		return analyzer;
	}
	
	private String getIndexLocation() {
		if (luceneDirectory == null) {
			if (nullOrEmpty(luceneDirectory))
				luceneDirectory = luceneDirectoryDefault;
			
			new FileExt(luceneDirectory).mkdirs();
			
			LibsLogger.debug(LuceneSearchEngineProvider.class, "Lucene directory finally set to ", luceneDirectory);
		}
		return luceneDirectory;
	} 
	
	private Directory getDirectory() {
		if (index == null) {
			try {
				LibsLogger.debug(LuceneSearchEngineProvider.class, "Opening Lucene index in ", 
						new FileExt(getIndexLocation()).getAbsolutePath());
				index = FSDirectory.open(Paths.get(getIndexLocation()));
			} catch (IOException e) {
				throw new RuntimeException("Cannot open Lucene search index directory", e);
			}
		}
		return index;
	}
	
	private IndexWriterConfig getConfig() {
		if (config == null)
			config = new IndexWriterConfig(getAnalyzer());
		return config;
	} 
	
	private IndexWriter getIndexWriter() {
		if (indexWriter == null) {
			synchronized (JVM_SEARCH_ENGINE_PROVIDER) {
				if (indexWriter != null)
					return indexWriter;
				
				try {
					indexWriter = new IndexWriter(getDirectory(), getConfig());
					
					new Thread() {
						@Override
						public void run() {
							while (true) {
								try {
									flush();
								} catch (Exception e) {
									LibsLogger.error(LuceneSearchEngineProvider.class, "Something wrong in Lucene flush Thread", e);
								}
								
								try {
									Thread.sleep(1000);
									
									if (closing)
										break;
								} catch (Exception e) {
									LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot sleep", e);
								}
							}
						}
					}.start();
				} catch (AlreadyClosedException e) {
					LibsLogger.error(LuceneSearchEngineProvider.class, "Index writer for Lucene already closed", e);
					indexWriter = null;
				} catch (IOException e) {
					LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot create Index writer for Lucene", e);
				} catch (IllegalStateException e) {
					LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot create Index writer for Lucene", e);
					
					if (e.getMessage().startsWith("do not share IndexWriterConfig instances across IndexWriters"))
						close();
				}
			}
		}
		return indexWriter;
	}
	
	private IndexReader getIndexReader() {
//		if ((System.currentTimeMillis() - lastReaderReload) > 1000) {
//			lastReaderReload = System.currentTimeMillis();
//			if (indexReader != null) {
//				try {
//					indexReader.close();
//					indexReader = null;
//				} catch (IOException e) {
//					LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot close reader", e);
//				}
//			}
//		}
		
		if (indexReader == null) {
			 try {
				indexReader = DirectoryReader.open(getDirectory());
			} catch (IOException e) {
				LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot create Index reader for Lucene", e);
			}
		}
		return indexReader;
	}

	@Override
	public void shutdown() {
		try {
			if (this.analyzer != null)
				analyzer.close();
			
			if (this.indexWriter != null)
				indexWriter.close();
			
			if (this.index != null)
				index.close();
		} catch (IOException e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot shutdown Lucene index", e);
		}
	}

	@Override
	public void init(JsonElement settings) throws IOException {
//		indexDocument("index", "type", "id", "{\"test\" : \"true\"}");
		
		if (notEmpty(JsonUtils.readString(settings, INIT_DIRECTORY))) {
			luceneDirectory = JsonUtils.readString(settings, INIT_DIRECTORY);
			
			LibsLogger.debug(LuceneSearchEngineProvider.class, "Read from setting Lucene directory: ", luceneDirectory);
		}
	}
	
	public void refresh() {
		try {
			flush();
			if (searcherManager != null)
				searcherManager.maybeRefresh();
		} catch (AlreadyClosedException e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Already closed Lucene", e);
		} catch (IOException e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot refresh Lucene", e);
		}
	}
	
	private void flush() throws IOException {
		if (indexed > 0) {
			getIndexWriter().commit();
			indexed = 0;
		}
	}
		
	@Override
	public void indexDocument(String index, String type, Map<String, String> idData) throws IOException {
		try {
			List<Document> docs = new ArrayList<Document>();
			
			for (String key : idData.keySet()) {
				docs.add(buildDoc(index, type, key, idData.get(key)));
			}
			
			// indexing new document
			getIndexWriter().addDocuments(docs);
			
			// commit changes
			if (++indexed >= 500)
				flush();
//			getIndexWriter().commit();
		} catch (IllegalStateException e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot index an object", e);
			
			if (e.getMessage().startsWith("do not share IndexWriterConfig instances across IndexWriters"))
				shutdown();
		}
	}

	@Override
	public void indexDocument(String index, String type, String id, String data) throws IOException {
		try {
			Document doc = buildDoc(index, type, id, data);
			
			// indexing new document
			getIndexWriter().addDocument(doc);
			
			// commit changes
			if (++indexed >= 500)
				flush();
		} catch (IllegalStateException e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot index an object", e);
			
			if (e.getMessage().startsWith("do not share IndexWriterConfig instances across IndexWriters"))
				shutdown();
			else if (e.getMessage().contains("this IndexWriter is closed"))
				shutdown();
		}
	}
	
	private Document buildDoc(String index, String type, String id, String data) throws IOException {
		Document doc = new Document();
		JsonElement je = JsonUtils.parseJson(data);
		
		doc.add(new StringField("index", index, Field.Store.NO));
		doc.add(new StringField("type", type, Field.Store.NO));
		doc.add(new StringField("id", id, Field.Store.NO));
//		doc.add(new TextField("data", data, Field.Store.YES));
		doc.add(new StoredField("data", data));
		
		parseJson(doc, je, "fields");
		
		// removing old document
		Builder b = new Builder()
			.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("id", id)), BooleanClause.Occur.MUST));
		getIndexWriter().deleteDocuments(b.build());
		
		return doc;
	}
	
	private void parseJson(final Document doc, final JsonElement je, final String name) {
		if (je == null || je.isJsonNull()) {
			return;
		} else if (je.isJsonObject()) {
			for (Entry<String, JsonElement> entry : je.getAsJsonObject().entrySet()) {
				if (entry.getValue().isJsonPrimitive()) {
					if (entry.getValue().getAsJsonPrimitive().isString())
						doc.add(new StringField(name + "." + entry.getKey(), entry.getValue().getAsString().toLowerCase(), Field.Store.NO));
					else if (entry.getValue().getAsJsonPrimitive().isNumber()) {
//						doc.add(new LongpoLegacyLongField(name + "." + entry.getKey(), entry.getValue().getAsLong(), Field.Store.NO));
//						doc.add(new LegacyLongField(entry.getKey(), entry.getValue().getAsLong(), Field.Store.YES));
//						doc.add(new LegacyLongField(name + "." + entry.getKey(), entry.getValue().getAsLong(), Field.Store.YES));
//						doc.add(new SortedNumericDocValuesField(entry.getKey(), entry.getValue().getAsLong()));
//						doc.add(new SortedNumericDocValuesField(name + "." + entry.getKey(), entry.getValue().getAsLong()));
//						doc.add(new SortedNumericSortField(entry.getKey(), Type.LONG));
//				        doc.add(new StoredField(entry.getKey(), entry.getValue().getAsLong()));
//				        doc.add(new StoredField(name + "." + entry.getKey(), entry.getValue().getAsLong()));
						
						BigDecimal bd = entry.getValue().getAsBigDecimal();
						if (bd.toString().indexOf('.') >= 0) {
							if (doc.getField(name + "." + entry.getKey()) == null)
								doc.add(new NumericDocValuesField(name + "." + entry.getKey(), entry.getValue().getAsLong()));
							doc.add(new DoublePoint(name + "." + entry.getKey(), entry.getValue().getAsDouble()));
						} else {
							if (doc.getField(name + "." + entry.getKey()) == null)
								doc.add(new NumericDocValuesField(name + "." + entry.getKey(), entry.getValue().getAsLong()));
							doc.add(new LongPoint(name + "." + entry.getKey(), entry.getValue().getAsLong()));
						}
					} else if (entry.getValue().getAsJsonPrimitive().isBoolean())
						doc.add(new StringField(name + "." + entry.getKey(), entry.getValue().getAsString(), Field.Store.NO));
					else
						throw new NotImplementedException();
				} else if (entry.getValue().isJsonObject()) {
					parseJson(doc, entry.getValue().getAsJsonObject(), name + "." + entry.getKey());
				} else if (entry.getValue().isJsonArray()) {
					for (JsonElement jje : entry.getValue().getAsJsonArray()) {
						parseJson(doc, jje, name + "." + entry.getKey());
					}
				} else
					throw new NotImplementedException();
			}
		} else if (je.isJsonPrimitive()) {
			if (je.getAsJsonPrimitive().isString())
				doc.add(new StringField(name, je.getAsString().toLowerCase(), Field.Store.NO));
			else if (je.getAsJsonPrimitive().isNumber())
				doc.add(new LongPoint(name, je.getAsLong()));
//				doc.add(new LegacyLongField(name, je.getAsLong(), Field.Store.NO));
			else if (je.getAsJsonPrimitive().isBoolean())
				doc.add(new StringField(name, je.getAsString(), Field.Store.NO));
			else
				throw new NotImplementedException();
		} else
			throw new NotImplementedException();
	}

	@Override
	public void removeDocument(String index, String type, String id) throws IOException {
		Builder b = new Builder()
			.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("id", id)), BooleanClause.Occur.MUST));
		getIndexWriter().deleteDocuments(b.build());
		
		if (++indexed >= 500)
			flush();
//		getIndexWriter().commit();
	}
	
	@Override
	public void remove(String index, String type, Query query) throws IOException {
		Builder fieldsQuery = new Builder();
		C3QueryToLuceneQuery(query.getTerms(), fieldsQuery);
		
		// the whole query
		Builder b = new Builder()
			.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST))
			.add(fieldsQuery.build(), BooleanClause.Occur.MUST);
//			.add(new BooleanClause(new WildcardQuery(new Term("fields.title", "*" + ((StringTerm) query.getTerms().get(0)).getTerm() + "*")), BooleanClause.Occur.MUST))
			;

		try {
			getIndexWriter().deleteDocuments(b.build());
		} catch (AlreadyClosedException e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Already closed", e);
//			indexWriter = null;
//			config = null;
			
			close();
		}
		
		if (++indexed >= 500)
			flush();
//		getIndexWriter().commit();
	}

	@Override
	public <T> T getDocument(Class<T> clazz, String index, String type, String id) throws IOException {
		Builder b = new Builder()
				.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
				.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST))
				.add(new BooleanClause(new TermQuery(new Term("id", id)), BooleanClause.Occur.MUST));

		org.apache.lucene.search.Query q = b.build();

//		IndexSearcher searcher = new IndexSearcher(getIndexReader());
		IndexSearcher searcher = getSearcherManager().acquire();
		
		try {
			TopDocs docs = searcher.search(q, 1);
			ScoreDoc[] hits = docs.scoreDocs;
			
			for (int i = 0; i < hits.length; ++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				return JsonUtils.fromJson(d.get("data"), clazz);
			}
		} catch (Exception e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot search in Lucene", e);
		} finally {
		   getSearcherManager().release(searcher);
		}
		
		// Do not use s after this!
		searcher = null;
		
		return null;
	}

	@Override
	public void clearIndexType(String index, String type) throws IOException {
		Builder b = new Builder()
			.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST));

		org.apache.lucene.search.Query q = b.build();
		
//		getIndexWriter().deleteDocuments(new Term("index", index), new Term("type", type));
		getIndexWriter().deleteDocuments(q);
		
		if (++indexed >= 500)
			flush();
//		getIndexWriter().commit();
	}

	@Override
	public void clearIndex(String index) throws IOException {
		getIndexWriter().deleteDocuments(new Term("index", index));
		
		if (++indexed >= 500)
			flush();
//		getIndexWriter().commit();
	}

	@Override
	public boolean isIndexTypeEmpty(String index, String type) throws IOException {
		Builder b = new Builder()
				.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
				.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST));

		org.apache.lucene.search.Query q = b.build();

//		IndexSearcher searcher = new IndexSearcher(getIndexReader());
		IndexSearcher searcher = getSearcherManager().acquire();
		TopDocs docs = null;
		try {
			docs = searcher.search(q, 10);
		} catch (Exception e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot search in Lucene", e);
		} finally {
		   getSearcherManager().release(searcher);
		}
		
		// Do not use s after this!
		searcher = null;
		
		return docs.totalHits == 0;
	}

	@Override
	public <T> SearchResults<T> search(Class<T> clazz, String index, String type, Query query, int from, int size) throws IOException {
		long start = System.currentTimeMillis();
		
		// field query
		Builder fieldsQuery = null;
		if (query.getTerms() != null && query.getTerms().size() > 0) {
			fieldsQuery = new Builder();
			C3QueryToLuceneQuery(query.getTerms(), fieldsQuery);
		}
		
		// the whole query
		Builder b = new Builder()
			.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST))
			;
		
		if (fieldsQuery != null)
			b.add(fieldsQuery.build(), BooleanClause.Occur.MUST);
//			.add(new BooleanClause(new WildcardQuery(new Term("fields.title", "*" + ((StringTerm) query.getTerms().get(0)).getTerm() + "*")), BooleanClause.Occur.MUST))
		
		org.apache.lucene.search.Query q = b.build();
		
		LibsLogger.trace(LuceneSearchEngineProvider.class, "Query ", q.toString());
		
//		IndexSearcher searcher = new IndexSearcher(getIndexReader());
//		getIndexWriter().commit();
		getSearcherManager().maybeRefresh();
		IndexSearcher searcher = getSearcherManager().acquire();
		TopDocs hits = null;
		ScoreDoc[] hitsDocs = null;
		List<T> objects = new ArrayList<>();
		try {
			
			if (query.isSortByDefined()) {
				String sortField = query.getSortBy().startsWith("fields.") ? query.getSortBy() : "fields." + query.getSortBy();
				
				hits = searcher.search(q, 
					MAX_RESULTS,
	//				new Sort(new SortedNumericSortField(query.getSortBy(), SortField.Type.LONG, query.getSortOrder() == SortOrder.DESCENDING ? true : false))
					new Sort(new SortField(sortField, 
							query.getSortType() == SortType.LONG ? SortField.Type.LONG : SortField.Type.STRING, 
							query.getSortOrder() == SortOrder.DESCENDING ? true : false))
	//				new Sort(SortField.FIELD_SCORE, new SortField(query.getSortBy(), SortField.Type.LONG))
					);
				
				hitsDocs = hits.scoreDocs;
				
				LibsLogger.trace(LuceneSearchEngineProvider.class, "Found " + hits.totalHits + " hits.");
				
				for (int i = from; i < (from + size) && i < hits.totalHits; ++i) {
					int docId = hitsDocs[i].doc;
					Document d = searcher.doc(docId);
					objects.add(JsonUtils.fromJson(d.get("data"), clazz));
				}
			} else {
				TopScoreDocCollector collector = TopScoreDocCollector.create(MAX_RESULTS);  // MAX_RESULTS is just an int limiting the total number of hits;
				searcher.search(q, collector);
				hits = collector.topDocs(from, size);
				
				hitsDocs = hits.scoreDocs;
				
				LibsLogger.trace(LuceneSearchEngineProvider.class, "Found " + hits.totalHits + " hits.");
				
				for (int i = 0; i < hitsDocs.length; ++i) {
					int docId = hitsDocs[i].doc;
					Document d = searcher.doc(docId);
					objects.add(JsonUtils.fromJson(d.get("data"), clazz));
				}
			}
		} catch (Exception e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot search in Lucene", e);
		} finally {
		   getSearcherManager().release(searcher);
		}
		
		// Do not use s after this!
		searcher = null;
		
		if ((System.currentTimeMillis() - start) > 100)
			LibsLogger.debug(LuceneSearchEngineProvider.class, "Search ", q, 
					" done in " + (System.currentTimeMillis() - start) + " [ms]");
		
		return new SearchResults<>(objects, hits.totalHits);
	}
	
	@Override
	public SearchResults<String> searchIds(String index, String type, Query query, int from, int size)
			throws IOException {
		// field query
		Builder fieldsQuery = new Builder();
		C3QueryToLuceneQuery(query.getTerms(), fieldsQuery);
		
		// the whole query
		Builder b = new Builder()
			.add(new BooleanClause(new TermQuery(new Term("index", index)), BooleanClause.Occur.MUST))
			.add(new BooleanClause(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST))
			.add(fieldsQuery.build(), BooleanClause.Occur.MUST);
//					.add(new BooleanClause(new WildcardQuery(new Term("fields.title", "*" + ((StringTerm) query.getTerms().get(0)).getTerm() + "*")), BooleanClause.Occur.MUST))
			;
				
		org.apache.lucene.search.Query q = b.build();
		
		LibsLogger.trace(LuceneSearchEngineProvider.class, "Query ", q.toString());
		
//		IndexSearcher searcher = new IndexSearcher(getIndexReader());
		IndexSearcher searcher= getSearcherManager().acquire();
				
		TopDocs hits = null;
		ScoreDoc[] hitsDocs = null;
		List<String> objects = new ArrayList<>();
		
		try {
			if (query.isSortByDefined()) {
				hits = searcher.search(q, 
					MAX_RESULTS,
					new Sort(new SortedNumericSortField(query.getSortBy(), SortField.Type.LONG)));
				
				hitsDocs = hits.scoreDocs;
				
				LibsLogger.trace(LuceneSearchEngineProvider.class, "Found " + hits.totalHits + " hits.");
				
				for (int i = from; i < (from + size) && i < hits.totalHits; ++i) {
					int docId = hitsDocs[i].doc;
					objects.add(String.valueOf(docId));
				}
			} else {
				TopScoreDocCollector collector = TopScoreDocCollector.create(MAX_RESULTS);  // MAX_RESULTS is just an int limiting the total number of hits; 
				searcher.search(q, collector);
				hits = collector.topDocs(from, size);
				
				hitsDocs = hits.scoreDocs;
				
				LibsLogger.trace(LuceneSearchEngineProvider.class, "Found " + hits.totalHits + " hits.");
				
				for (int i = 0; i < hitsDocs.length; ++i) {
					int docId = hitsDocs[i].doc;
					objects.add(String.valueOf(docId));
				}
			}
		} catch (Exception e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot search in Lucene", e);
		} finally {
		   getSearcherManager().release(searcher);
		}
		
		// Do not use s after this!
		searcher = null;	 
		
		return new SearchResults<>(objects, hits.totalHits);
	}

	private void C3QueryToLuceneQuery(List<net.hypki.libs5.search.query.Term> terms, Builder b) {
		for (net.hypki.libs5.search.query.Term t : terms) {
			b.add(toLuceneQuery(t), termReqToOccur(t));
		}
	}
	
	private BooleanClause.Occur termReqToOccur(net.hypki.libs5.search.query.Term t) {
		if (t.getRequirement() == TermRequirement.MUST)
			return BooleanClause.Occur.MUST;
		else if (t.getRequirement() == TermRequirement.MUST_NOT)
			return BooleanClause.Occur.MUST_NOT;
		else if (t.getRequirement() == TermRequirement.SHOULD)
			return BooleanClause.Occur.SHOULD;
		else
			throw new NotImplementedException();
	}
	
	private org.apache.lucene.search.Query toLuceneQuery(net.hypki.libs5.search.query.Term t) {
		if (t instanceof StringTerm)
			
			return new TermQuery(new Term("fields." + t.getField(), ((StringTerm) t).getTerm()));
		
		else if (t instanceof BooleanTerm)
			
			return new TermQuery(new Term("fields." + t.getField(), String.valueOf(((BooleanTerm) t).getTerm())));
		
		else if (t instanceof WildcardTerm)
			
			return new WildcardQuery(new Term("fields." + t.getField(), "*" + ((WildcardTerm) t).getTerm() + "*"));
		
		else if (t instanceof LongTerm) {
			
			LongTerm lt = (LongTerm) t;
			if (lt.getComparator() == TermComparator.E) {
				String fieldName = t.getField();
				long min = lt.getTerm();
				long max = min;
				return LongPoint.newRangeQuery("fields." + fieldName, 
						min, 
						max);
			} else if (lt.getComparator() == TermComparator.G) {
				return LongPoint.newRangeQuery("fields." + t.getField(), 
						lt.getTerm() + 1, 
						Long.MAX_VALUE);
			} else if (lt.getComparator() == TermComparator.GE) {
				return LongPoint.newRangeQuery("fields." + t.getField(), 
						lt.getTerm(), 
						Long.MAX_VALUE);
			} else if (lt.getComparator() == TermComparator.L) {
				return LongPoint.newRangeQuery("fields." + t.getField(), 
						Long.MIN_VALUE, 
						lt.getTerm() - 1);
			} else if (lt.getComparator() == TermComparator.LE) {
				return LongPoint.newRangeQuery("fields." + t.getField(), 
						Long.MIN_VALUE, 
						lt.getTerm());
			} else 
				throw new NotImplementedException();
			
//			return new LegacyNumericRangeQueryNode(lower, upper, lowerInclusive, upperInclusive, numericConfig); LongRange(1, 10);// Numeric Ranges..newFloatRange("fields." + t.getField(), BytesRef "*" + ((WildcardTerm) t).getTerm() + "*"));

		} else if (t instanceof IntTerm) {
				
			IntTerm dt = (IntTerm) t;
			String fieldName = t.getField();
			
//			return NumericRangeQuery.newIntRange
			
			// nie działa
//			return new TermQuery(new Term("fields." + fieldName, String.valueOf(dt.getTerm())));
			
			// nie działa
//			return LegacyNumericRangeQuery.newIntRange("fields." + fieldName, 
//					dt.getTerm(), 
//					dt.getTerm(), 
//					true, 
//					true);
			
			// nie działa
			int min = dt.getTerm();
			int max = min;
			return IntPoint.newRangeQuery("fields." + fieldName, 
					min, 
					max);
			
		} else if (t instanceof net.hypki.libs5.search.query.LongRangeTerm) {
			
			LongRangeTerm lt = (LongRangeTerm) t;
			long min = lt.getFrom();
			long max = lt.getTo();
			return LongPoint.newRangeQuery("fields." + t.getField(), 
					min, 
					max);

		} else if (t instanceof DoubleTerm) {
			
			DoubleTerm dt = (DoubleTerm) t;
			if (dt.getComparator() == TermComparator.E) {
				return DoublePoint.newRangeQuery("fields." + t.getField(), 
						dt.getTerm(), 
						dt.getTerm());
			} else if (dt.getComparator() == TermComparator.G) {
				return DoublePoint.newRangeQuery("fields." + t.getField(), 
						Math.nextUp(dt.getTerm()), 
						Double.POSITIVE_INFINITY);
			} else if (dt.getComparator() == TermComparator.GE) {
				return DoublePoint.newRangeQuery("fields." + t.getField(), 
						dt.getTerm(), 
						Double.POSITIVE_INFINITY);
			} else if (dt.getComparator() == TermComparator.L) {
				return DoublePoint.newRangeQuery("fields." + t.getField(), 
						Double.NEGATIVE_INFINITY, 
						Math.nextDown(dt.getTerm()));
			} else if (dt.getComparator() == TermComparator.LE) {
				return DoublePoint.newRangeQuery("fields." + t.getField(), 
						Double.NEGATIVE_INFINITY, 
						dt.getTerm());
			} else 
				throw new NotImplementedException();
			
			
//			double min = dt.getComparator() == TermComparator.G 
//					|| dt.getComparator() == TermComparator.GE 
//					|| dt.getComparator() == TermComparator.E ? dt.getTerm() : Double.MIN_VALUE;
//			double max = dt.getComparator() == TermComparator.L || dt.getComparator() == TermComparator.LE || dt.getComparator() == TermComparator.E ? dt.getTerm() : Double.MAX_VALUE;
//			
//			return DoublePoint.newRangeQuery("fields." + t.getField(), 
//					min, 
//					max);
			
//			return LegacyNumericRangeQuery.newDoubleRange("fields." + t.getField(), 
//					min, 
//					max, 
//					dt.getComparator() == TermComparator.GE || dt.getComparator() == TermComparator.LE, 
//					dt.getComparator() == TermComparator.GE || dt.getComparator() == TermComparator.LE);
//			return new LegacyNumericRangeQueryNode(lower, upper, lowerInclusive, upperInclusive, numericConfig); LongRange(1, 10);// Numeric Ranges..newFloatRange("fields." + t.getField(), BytesRef "*" + ((WildcardTerm) t).getTerm() + "*"));
		
		} else if (t instanceof Bracket) {
			
			Builder bracketQuery = new Builder();
			C3QueryToLuceneQuery(((Bracket) t).getTerms(), bracketQuery);
			return bracketQuery.build();
		
		} else
			
			throw new NotImplementedException("Unimplemented case " + t.getClass().getName());
	}
	
	@Override
	public void close() {
		try {
			flush();
			
			if (indexReader != null)
				getIndexReader().close();
			
			if (indexWriter != null && getIndexWriter().isOpen() == true)
				getIndexWriter().close();
			
			analyzer = null;			
			index = null;
			config = null;			
			indexWriter = null;			
			indexReader = null;			
			lastReaderReload = System.currentTimeMillis();			
			indexed = 0;
			searcherManager = null;
			closing = true;
		} catch (Throwable e) {
			LibsLogger.error(LuceneSearchEngineProvider.class, "Cannot close Lucene search engine", e);
			
			analyzer = null;			
			index = null;
			config = null;			
			indexWriter = null;			
			indexReader = null;			
			lastReaderReload = System.currentTimeMillis();			
			indexed = 0;
			searcherManager = null;
			closing = true;
		}
	}
}
