'''
Created on 18-07-2013

@author: ahypki
'''

import os
import inspect

def getThisScriptPath():
    return inspect.stack()[1][1] # inspect.getfile(inspect.currentframe()) # script filename (usually with path)


def getThisScriptDirectory():
    return os.path.dirname(os.path.abspath(inspect.stack()[1][1])) # script directory


def getSystemVariable(name):
    for param in os.environ.keys():
        if param == name:
            return os.environ[param]
    return None