'''
Created on 15-06-2013

@author: ahypki
'''

import sys

def getString(name, defaultvalue):
    for i in range(len(sys.argv)):
        if sys.argv[i] == "--" + name:
            return sys.argv[i + 1]
    return defaultvalue