'''
Created on 08-05-2013

@author: ahypki
'''

import os.path
import re
import subprocess
import sys
import time
import urllib.request

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    RED = '\033[91m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.RED = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''
        
def green( msg ):
    print(bcolors.OKGREEN + msg + bcolors.ENDC)
    return

def blue( msg ):
    print(bcolors.OKBLUE + msg + bcolors.ENDC)
    return

def red( msg ):
    print(bcolors.RED + msg + bcolors.ENDC)
    return

def msg( msg ):
    print(bcolors.OKBLUE + msg + bcolors.ENDC)
    return

def trace( parameters ):
    print("TRACE " + parameters)
    return

def log( parameters ):
    print("LOG   " + parameters)
    return

def info( parameters ):
    print("INFO  " + parameters)
    return

def warn( parameters ):
    print(bcolors.WARNING + "WARN  " + parameters + bcolors.ENDC)
    return

def error( parameters, end = '' ):
    print(bcolors.FAIL + "ERROR  " + parameters + bcolors.ENDC, end, file=sys.stderr)
    return