package net.hypki.libs5.cache.ehcache.unittests;

import java.io.UnsupportedEncodingException;

import net.hypki.libs5.cache.ehcache.EhcacheProvider;
import net.hypki.libs5.utils.date.Time;
import net.hypki.libs5.utils.tests.LibsTestCase;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Element;

import org.junit.Test;

public class EhcacheUnitTest extends LibsTestCase {

	@Test
	public void testSimple() throws CacheException, UnsupportedEncodingException {
		EhcacheProvider cache = new EhcacheProvider();
		
		cache.put("test1", "key1", "value1");
		assertTrue(cache.get(String.class, "test1", "key1").equals("value1"));
	}
	
	@Test
	public void testSimple2() throws CacheException, UnsupportedEncodingException {
		EhcacheProvider cache = new EhcacheProvider();
		
		cache.put("test1", "key2", "value1");
		assertTrue(cache.get(String.class, "test1", "key2").equals("value1"));
		
		Element ele = cache.getElement(String.class, "test1", "key2");
		System.out.println("creation time: " + new Time(ele.getCreationTime()).toStringyyyyMMdd_HHmm());
		System.out.println("expiration time: " + new Time(ele.getExpirationTime()).toStringyyyyMMdd_HHmm());
		System.out.println("time to idle [s]: " + ele.getTimeToIdle());
		System.out.println("time to live [s]: " + ele.getTimeToLive());
		System.out.println("hit count: " + ele.getHitCount());
	}
}
