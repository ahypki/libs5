package net.hypki.libs5.cache.ehcache;

import static java.lang.System.*;

import java.io.InputStream;

import net.hypki.libs5.cache.CacheProvider;
import net.hypki.libs5.utils.IModule;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class EhcacheProvider implements CacheProvider {
	
	private static CacheManager ehcacheManager = null;
	
	public EhcacheProvider() {
		
	}
	
	private static CacheManager getCacheManager() {
		if (ehcacheManager == null) {
			long start = currentTimeMillis();
			
			InputStream is = SystemUtils.getResourceAsStream("ehcache.xml");
			
			if (is != null)
				ehcacheManager = CacheManager.create(is);
			else {
				LibsLogger.error(EhcacheProvider.class, "Cannot open stream to ehcache configuration file. Creating default ehcache");
				ehcacheManager = CacheManager.create();
			}
			
			LibsInit.addToRunningModules(new IModule() {
				@Override
				public void shutdown() {
					LibsLogger.debug(EhcacheProvider.class, "Shutting down ehcache...");
					ehcacheManager.shutdown();
				}
			});
			
			LibsLogger.debug(EhcacheProvider.class, "Ehcache started " + (currentTimeMillis() - start) + " [ms]");
		}
		return ehcacheManager;
	}
	
	@Override
	public void close() {
		if (ehcacheManager != null) {
			ehcacheManager.shutdown();
		}
	}

	@Override
	public <T> T get(Class<T> clazz, String cacheName, String key) {
		Cache c = getCacheManager().getCache(cacheName);
		if (c == null) {
			try {
				getCacheManager().addCacheIfAbsent(cacheName);
			} catch (Exception e) {
				LibsLogger.error(EhcacheProvider.class, "Cannot add cache", e);
			}
			c = getCacheManager().getCache(cacheName);
		}
		Element ele = c.get(key);
		return (T) (ele != null ? ele.getObjectValue() : null);
	}
	
	public <T> Element getElement(Class<T> clazz, String cacheName, String key) {
		Cache c = getCacheManager().getCache(cacheName);
		if (c == null) {
			getCacheManager().addCache(cacheName);
			c = getCacheManager().getCache(cacheName);
		}
		Element ele = c.get(key);
		return (ele != null ? ele : null);
	}

	@Override
	public void put(String cacheName, String key, Object value) {
		Cache c = getCacheManager().getCache(cacheName);
		if (c == null) {
			getCacheManager().addCache(cacheName);
			c = getCacheManager().getCache(cacheName);
		}
		c.put(new Element(key, value));
	}

	@Override
	public void clearCache(String cacheName) {
		Cache c = getCacheManager().getCache(cacheName);
		if (c != null)
			c.removeAll();
	}

	@Override
	public void remove(String cacheName, String key) {
		Cache c = getCacheManager().getCache(cacheName);
		if (c != null)
			c.remove(key);
	}

}
