package net.hypki.libs5.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.IModule;
import net.hypki.libs5.utils.string.MetaList;

public interface StorageProvider extends IModule {
	
	public void			init(Map<String, Object> params)						throws IOException;
	public long 		save(String set, UUID fileId, InputStream dataStream, MetaList metaList) 	throws IOException;
	public boolean 		remove(String set, UUID fileId) 						throws IOException;
	public InputStream 	read(String set, UUID fileId) 							throws IOException;
	public boolean 		exist(String set, UUID fileId) 							throws IOException;
	public long 		size(String set, UUID fileId) 							throws IOException;
}
