package net.hypki.libs5.db.lucene;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.store.AlreadyClosedException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.Column;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.SearchIter;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.lucene.LuceneSearchEngineProvider;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.LongRangeTerm;
import net.hypki.libs5.search.query.LongTerm;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

public class LuceneDatabaseProvider implements DatabaseProvider {
	
	public static final String INIT_DIRECTORY = "directory";
	
	private static final String LUCENE_TABLE_SCHEMAS = "LUCENE_TABLE_SCHEMAS";
	
	private LuceneSearchEngineProvider luceneSearch = null;
	
	private boolean needRefresh = false;
		
	private MetaList initParams = null;
	
	public LuceneDatabaseProvider() {
		
	}
		
	private SearchEngineProvider getLuceneInstance() {
		if (luceneSearch == null) {
			try {
				luceneSearch = new LuceneSearchEngineProvider();
				
				JsonObject jo = new JsonObject();
				jo.addProperty(INIT_DIRECTORY, getInitParams().getAsString(INIT_DIRECTORY));
				
				luceneSearch.init(jo);
			} catch (IOException e) {
				LibsLogger.error(LuceneDatabaseProvider.class, "Lucene search init failed", e);
			}
		}
		return luceneSearch;
	}
	

	@Override
	public MetaList getInitParams() {
		if (this.initParams == null)
			this.initParams = new MetaList();
		return this.initParams;
	}

	@Override
	public void init(MetaList params) {
		this.initParams = params; 
		LibsLogger.debug(LuceneDatabaseProvider.class, "Initializing with meta params ", params);
	}

	@Override
	public void createKeyspace(String keyspace) {
		// do nothing
	}

	@Override
	public void clearKeyspace(String keyspace) {
		try {
			getLuceneInstance().clearIndex(toKeyspace(keyspace));
			needRefresh = true;
//			refresh();
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot clear keyspace " + keyspace, e);
		}
	}

	@Override
	public List<String> getTableList(String keyspace) {
		refreshLucene();
		
		List<String> tables = new ArrayList<String>();
		for (Row row : tableIterable(keyspace, TableSchema.class.getSimpleName().toLowerCase())) {
			TableSchema ts = JsonUtils.fromJson(row.getAsString(DbObject.COLUMN_DATA), TableSchema.class);
			tables.add(ts.getName());
		}
		return tables;
	}

	@Override
	public TableSchema getTableSchema(String keyspace, String table) {
		try {
			refreshLucene();
			
			// TODO cache
			return getLuceneInstance()
				.getDocument(TableSchema.class, 
						toKeyspace(keyspace), 
						toTable(LUCENE_TABLE_SCHEMAS), 
						table);
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot get Table schema for table " + table, e);
			return null;
		}
	}

	@Override
	public void createTable(String keyspace, TableSchema tableSchema) {
		try {
			List<Mutation> muts = new ArrayList<Mutation>();
			muts.add(new Mutation(MutationType.INSERT,
					keyspace,
					TableSchema.class.getSimpleName().toLowerCase(),
					tableSchema.getName(),
					"schema",
					"data",
					JsonUtils.objectToStringPretty(tableSchema)));
			save(muts);
//			getLuceneInstance()
//				.indexDocument(toKeyspace(keyspace), 
//						toTable(LUCENE_TABLE_SCHEMAS), 
//						tableSchema.getName(),
//						JsonUtils.objectToStringPretty(tableSchema));
		} catch (IOException | ValidationException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot save Table schema for " + tableSchema.getName(), e);
		}
	}

	@Override
	public void clearTable(String keyspace, String table) {
		try {
			getLuceneInstance()
				.clearIndexType(toKeyspace(keyspace), 
						toTable(table));
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot clear table " + table, e);
		}
	}

	@Override
	public void removeTable(String keyspace, String table) {
		clearTable(keyspace, table);
		
		// removing Table schema
		try {
			getLuceneInstance().remove(toKeyspace(keyspace), 
					toTable(LUCENE_TABLE_SCHEMAS),
					new Query()
						.addTerm(new StringTerm("name", table)));
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot remove Table schema for table " + table, e);
		}
		
		needRefresh = true;
//		refresh();
	}

	@Override
	public boolean isTableEmpty(String keyspace, String table) {
		try {
			refreshLucene();
			
			return getLuceneInstance().isIndexTypeEmpty(toKeyspace(keyspace), 
					toTable(table));
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot check if Table is empty", e);
			return false;
		}
	}

	@Override
	public boolean exist(String keyspace, String table, C3Where c3where, String column) throws IOException {
		refreshLucene();
		
		return get(keyspace, table, c3where, column) != null;
	}

	@Override
	public void save(List<Mutation> mutations) throws IOException, ValidationException {
		if (mutations == null || mutations.size() == 0)
			return;
				
		for (Mutation m : mutations) {
			if (m.getMutation() == MutationType.INSERT) {
				Row r = null;
				
				if (m.getWhere() == null || m.getWhere().getClauses().size() == 0) {
					// simple key-value
					r = new Row(m.getKey());
					r.set(m.getKeyName(), m.getKey());
					r.set(m.getColumn(), m.getObjData());
				} else if (m.getWhere() != null) {
					// with where clause
					r = new Row(UUID.random().getId());
					
					for (C3Clause clause : m.getWhere().getClauses()) {
						r.set(clause.getField(), clause.getValue());
					}
					r.set(m.getColumn(), m.getObjData());
				} 
//				else {
//					// check if in schema there are some columns
//					TableSchema ts = getTableSchema(m.getKeyspace(), m.getTable());
//					if (ts != null && ts.getColumns().size() > 1) {
//						for (Column c : ts.getColumns()) {
//							if (c.isIndexed()) {
//								r.set(c.getName(), clause.getValue());
//							}
//						}
//					}
//				}
				
				saveRow(m.getKeyspace(), m.getTable(), r);
			} else if (m.getMutation() == MutationType.REMOVE) {
				if (m.getWhere() == null || m.getWhere().getClauses().size() == 0) {
					// simple key-value
					remove(m.getKeyspace(), m.getTable(), String.valueOf(m.getKey()), 
							m.getKeyName(), DbObject.COLUMN_DATA);
				} else if (m.getWhere() != null) {
					// with where clause
					remove(m.getKeyspace(), m.getTable(), m.getWhere());
				}
			}
		}
	}
	
	private void refreshLucene() {
		if (needRefresh) {
			try {
				((LuceneSearchEngineProvider) luceneSearch).refresh();
			} catch (Throwable e) {
				LibsLogger.error(LuceneSearchEngineProvider.class, "Already closed", e);
				close();
			}
			needRefresh = false;
		}
	}
	
	public void saveRow(String keyspace, String table, Row r) throws IOException {
		Object data = r.get(DbObject.COLUMN_DATA);
		if (data != null 
				&& data instanceof String
				&& ((String) data).length() > 30000) {
			String dataStr = (String) data;
			int counter = 1;
			while (dataStr.length() > 30000) {
				String part = dataStr.substring(0, 30000);
				dataStr = dataStr.substring(30000);
				r.addColumn("data" + counter, part);
				counter++;
			}
			if (dataStr.length() > 0) {
				r.addColumn("data" + counter, dataStr);
			}
			r.removeColumn(DbObject.COLUMN_DATA);
		}
		getLuceneInstance().indexDocument(toKeyspace(keyspace), 
				toTable(table), 
				String.valueOf(r.getPk()), 
				JsonUtils.objectToString(r));
		
		needRefresh = true;
	}
	
	private void removeRow(String keyspace, String table, String key) throws IOException {
		getLuceneInstance().removeDocument(toKeyspace(keyspace), 
				toTable(table), 
				String.valueOf(key));
	}

	@Override
	public long countColumns(String keyspace, String table, String key) {
		refreshLucene();
		
		return getTableSchema(keyspace, table).getColumns().size();
	}

	@Override
	public long countRows(String keyspace, String table, C3Where c3where) {
		try {
			refreshLucene();
			
			return getLuceneInstance()
				.searchIds(toKeyspace(keyspace), 
						toTable(table), 
						toQuery(c3where, null), 
						0, 
						10)
				.maxHits();
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot count number of rows", e);
			return 0L;
		}
	}

	@Override
	public <T> T get(String keyspace, String table, C3Where c3where, String column, Class<T> type) throws IOException {
		refreshLucene();
		
		Object tmp = get(keyspace, table, c3where, column);
		return tmp != null ? JsonUtils.fromJson((String) tmp, type) : null;
	}
	
	public Row getRow(String keyspace, String table, String key) throws IOException {
		Row r = getLuceneInstance().getDocument(Row.class, 
				toKeyspace(keyspace),
				toTable(table), 
				key);
		
		// fixing row if field data is over 32000 characters long
		fixLargeRow(r);
		
		return r;
	}
	
	private Row fixLargeRow(Row row) {
		if (row != null && row.contains("data1")) {
			int c = 1;
			Object col = null;
			String data = "";
			while ((col = row.get("data" + c)) != null) {
				data += (String) col;
				row.removeColumn("data" + c);
				c++;
			}
			row.addColumn(DbObject.COLUMN_DATA, data);
		}
		return row;
	}
	
	public Row getRow(String keyspace, String table, C3Where c3where) throws IOException {
		Results res = getList(keyspace, 
				table, 
				null, 
				null, 
				1, 
				null, 
				c3where);
		return res != null && res.size() > 0 ? res.getRows().get(0) : null;
	}

	@Override
	public Object get(String keyspace, String table, C3Where c3where, String column) throws IOException {
		refreshLucene();
		
		Row r = getRow(keyspace, table, c3where);
		return r != null ? r.get(column) : null;
	}

	@Override
	public void remove(String keyspace, String table, String key, String keyName, String column) throws IOException {
//		refreshLucene();
		
//		Row r = getRow(keyspace, table, key);
//		r.removeColumn(column);
//		if (r.size() == 0)
//			removeRow(keyspace, table, key);
//		else if (r.size() == 1 && r.get(keyName) != null)
			removeRow(keyspace, table, key);
//		else
//			saveRow(keyspace, table, r);
		
		needRefresh = true;
//		refresh();
	}

	@Override
	public void remove(String keyspace, String columnFamily, C3Where where) throws IOException {
		getLuceneInstance()
			.remove(toKeyspace(keyspace), 
					toTable(columnFamily), 
					toQuery(where, null));
		
		needRefresh = true;
//		refresh();
	}

	@Override
	public Results getList(String keyspace, String table, String[] columns, Order order, int count, 
			String state, C3Where where) {
		try {
			refreshLucene();
//			if (where != null && where.size() > 0)
//				for (C3Clause clause : where.getClauses()) {
//					clause.setField("columns." + clause.getField());
//				}
			
			SearchResults<Row> resLuc = getLuceneInstance()
					.search(Row.class, 
							toKeyspace(keyspace), 
							toTable(table), 
							toQuery(where, order), 
							notEmpty(state) ? toInt(state) : 0, 
							count);
			
			Results res = new Results();
			res.setNextPage(String.valueOf(notEmpty(state) ? toInt(state) + resLuc.size() : resLuc.size()));
			for (Row row : resLuc) {
				res.addRow(fixLargeRow(row));
			}
			return res;
			
//			TableSchema tableSchema = getTableSchema(keyspace, table);
//			Results res = new Results();
//			for (String rowString : resLuc) {
//				JsonObject jo = (JsonObject) JsonUtils.parseJson(rowString);
//				Row row = new Row(jo.get(tableSchema.getPrimaryKeyName()));
//				
//				res.addRow(row);
//			}
//			return res;
		} catch (java.nio.channels.ClosedByInterruptException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Lucene already closed, opening again", e);
			close();
			return null;
		} catch (IOException e) {
			LibsLogger.error(LuceneDatabaseProvider.class, "Cannot query Lucene DB", e);
			return null;
		}
	}

	@Override
	public Iterable<Row> tableIterable(String keyspace, String table) {
		refreshLucene();
		
		return new SearchIter<Row>(getLuceneInstance(), 
				Row.class, 
				toKeyspace(keyspace), 
				toTable(table), 
				new Query());
	}

	@Override
	public void close() {
		getLuceneInstance().close();
	}
	
	private static String toKeyspace(String keyspace) {
		return "__DB_" + keyspace;
	}
	
	private static String toTable(String table) {
		return "__DB_" + table;
	}
	
	private Query toQuery(C3Where where, Order order) {
		Query q = new Query();
		
		if (where != null)
			for (C3Clause clause : where.getClauses()) {
				if (clause.getClauseType() == ClauseType.EQ)
					q.addTerm(new StringTerm("columns." + clause.getField().toLowerCase(), 
							String.valueOf(clause.getValue()).toLowerCase(), 
							TermRequirement.MUST));
				else if (clause.getClauseType() == ClauseType.GE)
					q.addTerm(new LongRangeTerm("columns." + clause.getField().toLowerCase(), 
							(Long) clause.getValue(), 
							Long.MAX_VALUE,
							TermRequirement.MUST));
				else if (clause.getClauseType() == ClauseType.GT)
					q.addTerm(new LongRangeTerm("columns." + clause.getField().toLowerCase(), 
							(Long) clause.getValue() + 1, 
							Long.MAX_VALUE,
							TermRequirement.MUST));
				else if (clause.getClauseType() == ClauseType.LE)
					q.addTerm(new LongRangeTerm("columns." + clause.getField().toLowerCase(), 
							0L,
							(Long) clause.getValue(), 
							TermRequirement.MUST));
				else if (clause.getClauseType() == ClauseType.LT)
					q.addTerm(new LongRangeTerm("columns." + clause.getField().toLowerCase(), 
							0L,
							(Long) clause.getValue() - 1, 
							TermRequirement.MUST));
				else if (clause.getClauseType() == ClauseType.IN) {
					Bracket bracket = new Bracket(TermRequirement.MUST);
					for (Object val : (List) clause.getValue()) {
						bracket.addTerm(new StringTerm(clause.getField(), String.valueOf(val), TermRequirement.SHOULD));
					}
					q.addTerm(bracket);
				} else
					throw new NotImplementedException();
			}
		
		if (order != null) {
			q.setSortBy("columns." + order.getColName().toLowerCase(), 
					order.isAsc() ? SortOrder.ASCENDING : SortOrder.DESCENDING);
		}
		
		return q;
	}
}
