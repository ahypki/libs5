package net.hypki.libs5.db.lucene.unittests;

import java.io.IOException;

import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.lucene.LuceneDatabaseProvider;
import net.hypki.libs5.utils.tests.LibsTestCase;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import org.junit.Test;

import com.google.gson.annotations.Expose;


public class TestDbObject extends LibsTestCase {

	@Test
	public void testApp() throws IOException, ValidationException {
		
		System.setProperty(DatabaseProvider.class.getSimpleName(), LuceneDatabaseProvider.class.getName());
		
		TestObject to = new TestObject("c1", "v1");
		to.save();
		
		TestObject toDb = TestObject.get("c1");
		assertTrue(toDb != null);
		assertTrue(to.getValue().equals(toDb.getValue()));
	}
	
}

class TestObject extends DbObject<TestDbObject> {
	
	@Expose
	@NotNull
	@NotEmpty
	private String column = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String value = null;
	
	public TestObject() {
		
	}
	
	public TestObject(String column, String value) {
		setColumn(column);
		setValue(value);
	}
	
	public static TestObject get(String column) throws IOException {
		return DbObject.getDatabaseProvider().get("test-keyspace", 
				"test-cf", 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, column)), 
				DbObject.COLUMN_DATA, 
				TestObject.class);
	}
	
	@Override
	public String getKeyspace() {
		return "test-keyspace";
	}
	
	@Override
	public String getColumnFamily() {
		return "test-cf";
	}
	
	@Override
	public String getColumn() {
		return column;
	}
	
	@Override
	public String getKey() {
		return "test-key";
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setColumn(String column) {
		this.column = column;
	}
}