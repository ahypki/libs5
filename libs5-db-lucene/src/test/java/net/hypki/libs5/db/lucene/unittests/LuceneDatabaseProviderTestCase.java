package net.hypki.libs5.db.lucene.unittests;

import java.io.IOException;

import org.junit.Test;

import net.hypki.libs5.db.DatabaseProviderTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.lucene.LuceneDatabaseProvider;

public class LuceneDatabaseProviderTestCase extends DatabaseProviderTestCase {

	public LuceneDatabaseProviderTestCase() {
		
	}
	
	@Test
	public void testAll() throws IOException, ValidationException {
		super.testAll(new LuceneDatabaseProvider());
	}
}
