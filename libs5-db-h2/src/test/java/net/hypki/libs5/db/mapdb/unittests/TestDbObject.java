package net.hypki.libs5.db.mapdb.unittests;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class TestDbObject extends LibsTestCase {

	@Test
	public void testApp() throws IOException, ValidationException {

//		System.setProperty(DatabaseProvider.class.getSimpleName(), H2DatabaseProvider.class.getName());
//		
//		TestObject to = new TestObject("c1", "v1");
//		to.save();
//		
//		TestObject toDb = TestObject.get("c1");
//		assertTrue(toDb != null);
//		assertTrue(to.getValue().equals(toDb.getValue()));

		try {
			Connection connection = DriverManager.getConnection("jdbc:h2:./sample");
			System.out.println("Connection established: " + connection);

			// Create a new table
			String createTableQuery = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, name TEXT NOT NULL)";
			Statement statement = connection.createStatement();
			statement.execute(createTableQuery);
			
			// clear database
			statement.execute("DELETE FROM users;");

			// Insert data
			String insertDataQuery = "INSERT INTO users (id, name) VALUES (1, 'John Doe')";
			statement.execute(insertDataQuery);

			// Query data
			String selectDataQuery = "SELECT * FROM users WHERE id = 1";
			ResultSet resultSet = statement.executeQuery(selectDataQuery);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				System.out.println("User ID: " + id + ", Name: " + name);
			}
			
			resultSet = statement.executeQuery("SHOW TABLES;");
			while (resultSet.next()) {
				System.out.println("Table: " + resultSet.getString("TABLE_NAME"));
			}
			
			resultSet = statement.executeQuery("show columns from users");
			while (resultSet.next()) {
				System.out.println("Schema: " + resultSet.toString());
			}
			


			// Close resources
			resultSet.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

}