package net.hypki.libs5.db.mapdb.unittests;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class TestObject extends DbObject<TestDbObject> {

	@Expose
	@NotNull
	@NotEmpty
	private String column = null;

	@Expose
	@NotNull
	@NotEmpty
	private String value = null;

	public TestObject() {

	}

	public TestObject(String column, String value) {
		setColumn(column);
		setValue(value);
	}

	public static TestObject get(String column) throws IOException {
		return DbObject.getDatabaseProvider().get("test-keyspace", "test-cf",
				new C3Where().addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, column)), DbObject.COLUMN_DATA,
				TestObject.class);
	}

	@Override
	public String getKeyspace() {
		return "test-keyspace";
	}

	@Override
	public String getColumnFamily() {
		return "test-cf";
	}

	@Override
	public String getColumn() {
		return column;
	}

	@Override
	public String getKey() {
		return "test-key";
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setColumn(String column) {
		this.column = column;
	}
}
