package net.hypki.libs5.db.mapdb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hc.core5.http.NotImplementedException;

import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.Column;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

public class H2DatabaseProvider implements DatabaseProvider {
		
	private String dbFile = null;
	
	private Connection connection = null;
	private Statement statement = null;
	
	private Map<String, TableSchema> tableToSchemaCache = new HashMap<>();
	
	private Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection("jdbc:h2:" + dbFile);
		}
		return connection;
	}
	
	private Statement getStatement() throws SQLException {
		if (statement == null) {
			statement = getConnection().createStatement();
		}
		return statement;
	} 

	@Override
	public MetaList getInitParams() {
		return new MetaList()
				.add("filename", dbFile);
	}

	@Override
	public void init(MetaList params) {
		if (params != null) {
			dbFile = params.getAsString("filename");
			
			// read all schemas to the cache
//			try {
//				for (String table : getTableList(null))
//					getTableSchema(null, table);
//			} catch (IOException e) {
//				LibsLogger.error(H2DatabaseProvider.class, "Cannot read all schemas from DB", e);
//			}
		}
	}

	@Override
	public void createKeyspace(String keyspace) {
		// TODO implement it
	}

	@Override
	public void clearKeyspace(String keyspace) throws IOException {
		for (String table : getTableList(keyspace)) {
			try {
				getStatement().execute("DELETE FROM " + table + ";");
			} catch (SQLException e) {
				throw new IOException("Cannot clear keyspace " + keyspace, e);
			}
		}
	}

	@Override
	public List<String> getTableList(String keyspace) throws IOException {
		List<String> tables = new ArrayList<>();
		try (ResultSet resultSet = getStatement().executeQuery("SHOW TABLES")) {
			while (resultSet.next()) {
				tables.add(resultSet.getString("TABLE_NAME"));
			}
		} catch (SQLException e) {
			throw new IOException("Cannot query for list of tables", e);
		}
		return tables;
	}

	@Override
	public TableSchema getTableSchema(String keyspace, String table) throws IOException {
		try {
			TableSchema sch = tableToSchemaCache.get(table);
			if (sch != null)
				return sch;
			
			TableSchema ts = new TableSchema(table);
			ResultSet resultSet = getStatement().executeQuery("show columns from " + table);
			while (resultSet.next()) {
				String key = resultSet.getString("KEY");
				
				ts.addColumn(resultSet.getString("FIELD").toLowerCase(), 
						toColumnType(resultSet.getString("TYPE")), 
						key != null ? key.contains("PRI") : false, 
						false);
			}
			
			tableToSchemaCache.put(table, ts);
			
			return ts;
		} catch (SQLException | NotImplementedException e) {
			throw new IOException("Cannot get Table Schema for " + table, e);
		}
	}

	private ColumnType toColumnType(String type) throws NotImplementedException {
		// TODO implement the rest
		if (type.contains("INTEGER"))
			return ColumnType.INTEGER;
		else if (type.contains("CLOB"))
			return ColumnType.STRING;
		else if (type.contains("VARCHAR"))
			return ColumnType.STRING;
		else
			throw new NotImplementedException("H2 don't know how to convert " + type + " to lib5 " + ColumnType.class.getSimpleName());
	}

	@Override
	public void createTable(String keyspace, TableSchema tableSchema) throws IOException {
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("CREATE TABLE IF NOT EXISTS ");
			sb.append(tableSchema.getName());
			sb.append("(");
			int colCount = 0;
			for (Column col : tableSchema.getColumns()) {
				if (colCount++ > 0)
					sb.append(", ");
				sb.append(col.getName());
				sb.append(" ");
				sb.append(toH2Type(col));
				if (col.isPrimaryKey())
					sb.append(" PRIMARY KEY");
			}
			sb.append(")");
			getStatement().execute(sb.toString());
			
			tableToSchemaCache.put(tableSchema.getName(), tableSchema);
		} catch (SQLException | NotImplementedException e) {
			throw new IOException("Cannot create table " + tableSchema.getName(), e);
		}
	}

	private String toH2Type(Column col) throws NotImplementedException {
		ColumnType type = col.getType();
		if (type == ColumnType.INTEGER)
			return "INTEGER";
		else if (type == ColumnType.LONG)
			return "BIGINT";
		else if (type == ColumnType.DOUBLE)
			return "DOUBLE";
		else if (type == ColumnType.STRING) {
			if (col.isPrimaryKey())
				return "VARCHAR";
			else
				return "VARCHAR";
		} else
			throw new NotImplementedException("Conversion from " + type + " to H2 column type");
	}

	@Override
	public void clearTable(String keyspace, String table) throws IOException {
		try {
			getStatement().execute("DELETE FROM " + table);
		} catch (SQLException e) {
			throw new IOException("Cannot clear table " + table, e);
		}
	}

	@Override
	public void removeTable(String keyspace, String table) throws IOException {
		try {
			getStatement().execute("DROP TABLE " + table);
		} catch (SQLException e) {
			throw new IOException("Cannot remove table " + table, e);
		}
	}

	@Override
	public boolean isTableEmpty(String keyspace, String table) throws IOException {
		try {
			ResultSet rs = getStatement().executeQuery("SELECT count(*) FROM " + table);
			rs.next();
			return rs.getInt(0) > 0;
		} catch (SQLException e) {
			throw new IOException("Cannot check if the table is empty " + table, e);
		}
	}

	@Override
	public boolean exist(String keyspace, String table, C3Where c3where, String column) throws IOException {
		return get(keyspace, table, c3where, column) != null;
	}

	@Override
	public void save(List<Mutation> mutations) throws IOException, ValidationException {
		try {
			StringBuilder query = new StringBuilder();
			
			for (Mutation mutation : mutations) {
				// insert
				if (mutation.getMutation() == MutationType.INSERT)
					query.append(toInsert(mutation));
//					getStatement().execute(toInsert(mutation));
				else
					query.append(toRemove(mutation));
//					getStatement().execute(toRemove(mutation));
			}
			
			getStatement().execute(query.toString());
		} catch (SQLException e) {
			throw new IOException("Cannot save mutations", e);
		}
	}

	private String toRemove(Mutation mutation) {
		StringBuilder ins = new StringBuilder();
		ins.append("DELETE FROM ");
		ins.append(mutation.getTable());
		ins.append(" WHERE ");
		ins.append(mutation.getKeyName());
		ins.append(" = '");
		ins.append(mutation.getKey().toString());
		ins.append("';");
		return ins.toString();
	}

	private String toInsert(Mutation mutation) throws IOException {
		StringBuilder ins = new StringBuilder();
		ins.append("MERGE INTO ");
		ins.append(mutation.getTable());
		ins.append(" VALUES ('");
		ins.append(mutation.getKey().toString());
		ins.append("', '");
		ins.append(mutation.getObjData());
		ins.append("'");
//		int c = 0;
//		for (Column col : getTableSchema(mutation.getKeyspace(), mutation.getTable()).getColumns()) {
//			if (c++ > 0)
//				ins.append(", ");
//			ins.append(mutation.get);
//		}
		ins.append(");");
		return ins.toString();
	}

	@Override
	public long countColumns(String keyspace, String table, String key) throws IOException {
		return getTableSchema(keyspace, table).getColumns().size();
	}
	
	private String toWhere(String keyspace, String table, C3Where where) throws NotImplementedException, IOException {
		StringBuilder sb = new StringBuilder();
		for (C3Clause clause : where.getClauses()) {
			sb.append(clause.getField());
			if (clause.getClauseType() == ClauseType.EQ)
				sb.append(" = ");
			else if (clause.getClauseType() == ClauseType.GE)
				sb.append(" >= ");
			else if (clause.getClauseType() == ClauseType.LE)
				sb.append(" <= ");
			else if (clause.getClauseType() == ClauseType.GT)
				sb.append(" > ");
			else if (clause.getClauseType() == ClauseType.LT)
				sb.append(" < ");
			else
				throw new NotImplementedException("Clause " + clause + " not implemented");
			if (getTableSchema(keyspace, table).getColumn(clause.getField()).getType() == ColumnType.STRING)
				sb.append("'" + clause.getValue() + "'");
			else
				sb.append(clause.getValue());
		}
		return sb.toString();
	}

	@Override
	public long countRows(String keyspace, String table, C3Where c3where) throws IOException {
		try {
			ResultSet rs = getStatement().executeQuery("SELECT count(*) FROM " + table + " WHERE " + toWhere(keyspace, table, c3where));
			rs.next();
			return rs.getInt(0);
		} catch (SQLException | NotImplementedException e) {
			throw new IOException("Cannot check if the table is empty " + table, e);
		}
	}

	@Override
	public <T> T get(String keyspace, String table, C3Where c3where, String column, Class<T> type) throws IOException {
		Object tmp = get(keyspace, table, c3where, column);
		return tmp != null ? JsonUtils.fromJson((String) tmp, type) : null;
	}

	@Override
	public Object get(String keyspace, String table, C3Where c3where, String column) throws IOException {
		try {
			if (getStatement().isClosed())
				return null;
			
			ResultSet rs = getStatement().executeQuery("SELECT " + column 
					+ " FROM " + table 
					+ " WHERE " + toWhere(keyspace, table, c3where));
			if (rs.next())
				return rs.getObject(1);
			return null;
		} catch (Throwable e) {
			throw new IOException("Cannot get object from db for " + table, e);
		}
	}

	@Override
	public void remove(String keyspace, String columnFamily, String key, String keyName, String column)
			throws IOException {
		try {
			getStatement().execute("DELETE FROM " + columnFamily 
					+ " WHERE " + keyName + " = " + key);
		} catch (SQLException e) {
			throw new IOException("Cannot remove row from table " + columnFamily, e);
		}
	}

	@Override
	public void remove(String keyspace, String columnFamily, C3Where where) throws IOException {
		try {
			getStatement().execute("DELETE FROM " + columnFamily + " WHERE " + toWhere(keyspace, columnFamily, where));
		} catch (SQLException | NotImplementedException e) {
			throw new IOException("Cannot remove row from table " + columnFamily, e);
		}
	}

	@Override
	public Results getList(String keyspace, String table, String[] columns, Order order, int count, String state,
			C3Where where) throws IOException {
		try {
			if (getStatement().isClosed())
				return new Results();
			
			ResultSet resultSet = getStatement().executeQuery("SELECT * FROM " + table + " LIMIT " + count + " OFFSET " + (state == null ? "0" : state));
			Results results = new Results();
			while (resultSet.next()) {
				Row row = new Row();
				for (Column col : getTableSchema(keyspace, table).getColumns()) {
					row.addColumn(col.getName(), resultSet.getObject(col.getName()));
				}
				results.addRow(row);
			} 
			if (results.size() == count)
				results.setNextPage("" + ((state != null ? NumberUtils.toLong(state) : 0) + count));
			return results;
		} catch (SQLException e) {
			throw new IOException("Cannot get list of objects from table " + table, e);
		}
	}

	@Override
	public Iterable<Row> tableIterable(String keyspace, String table) {
		return new ResultsIter(keyspace, table, null, null).setPerPage(100_000);
	}

	@Override
	public void close() throws IOException {
		try {
			getStatement().close();
			getConnection().close();
		} catch (SQLException e) {
			throw new IOException("Cannot close H2 database", e);
		}
	}

}
