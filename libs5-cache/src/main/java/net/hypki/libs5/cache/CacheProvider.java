package net.hypki.libs5.cache;


public interface CacheProvider {
		
	public <T> T get(Class<T> clazz, String cacheName, String key);
	public void put(String cacheName, String key, Object obj);
	public void clearCache(String cacheName);
	public void remove(String cacheName, String key);
	public void close();
}
