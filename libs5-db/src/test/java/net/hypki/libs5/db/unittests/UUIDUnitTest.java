package net.hypki.libs5.db.unittests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.collections.NotNullHashMap;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class UUIDUnitTest extends LibsTestCase {

	@Test
	public void testSort() {
		List<String> ids = new ArrayList<>();
		
		ids.add(new String("0000000000"));
		ids.add(new String("aaaaaaaaaa"));
		ids.add(new String("bbbbbbbbbb"));
		ids.add(new String("zzzzzzzzzz"));
		ids.add(new String("AAAAAAAAAA"));
		ids.add(new String("BBBBBBBBBB"));
		ids.add(new String("ZZZZZZZZZZ"));
		ids.add(new String("9999999999"));
		
		Collections.sort(ids);
		
		for (String id : ids) {
			System.out.println(id);
		}
	}
	
	
	@Test
	public void testQuintuplets() {
		final int partLength = 4;
		HashMap<String, Integer> czworki = new NotNullHashMap(0);
		
		for (int i = 0; i < 100000; i++) {
			UUID id = UUID.random();
			
			for (int j = 0; j < 32; j += partLength) {
				String part = id.getId().substring(j, j + partLength);
				czworki.put(part, czworki.get(part) + 1);
			}
		}
		
		int once = 0;
		int multiple = 0;
		for (Map.Entry<String, Integer> e : czworki.entrySet()) {
			if (e.getValue() > 1) {
				multiple++;
//				System.out.println("Part " + e.getKey() + " occured " + e.getValue() + " times");
			} else
				once++;
		}
		
		System.out.println("All parts once " + once + " AND multiple times " + multiple);
	}
}
