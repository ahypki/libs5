package net.hypki.libs5.db;

import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Person {
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String id = null;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String name = null;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private int age = 0;

	public Person() {
		
	}
	
	public Person(String id, String name, int age) {
		setId(id);
		setName(name);
		setAge(age);
	}
	
	public TableSchema getTableSchema() {
		return new TableSchema(getClass().getSimpleName())
			.addColumn(DatabaseProviderTestCase.COLUMN_PK, ColumnType.STRING, true)
			.addColumn(DatabaseProviderTestCase.COLUMN_DATA, ColumnType.STRING);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
