package net.hypki.libs5.db.db.weblibs;

public class C3Clause {
	
	private String field = null;
	
	private Object value = null;
	
	private ClauseType clauseType = null;

	public C3Clause() {
		
	}
	
	public C3Clause(String field, ClauseType clauseType, Object value) {
		setField(field);
		setClauseType(clauseType);
		setValue(value);
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s", getField(), getClauseType().toString(), String.valueOf(getValue()));
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public ClauseType getClauseType() {
		return clauseType;
	}

	public void setClauseType(ClauseType clauseType) {
		this.clauseType = clauseType;
	}
}
