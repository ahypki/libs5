package net.hypki.libs5.db.db.schema;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.collections.UniqueList;
import net.hypki.libs5.utils.json.JsonUtils;

public class TableSchema implements Serializable {

	@Expose
	private String name = null;
	
	@Expose
	private List<Column> columns = null;
	
	public TableSchema() {
		
	}
	
	public TableSchema(String name) {
		setName(name);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName());
		sb.append(": ");
		for (Column column : columns) {
			sb.append(column.toString());
			sb.append(" ");
		}
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TableSchema) {
			TableSchema in = (TableSchema) obj;
			
			for (Column column : getColumns()) {
				if (in.getColumn(column.getName()) == null)
					return false;
			}
			
			for (Column column : in.getColumns()) {
				if (getColumn(column.getName()) == null)
					return false;
			}
		}
		return true;
	}
	
	public String toJson() {
		return JsonUtils.objectToString(this);
	}
	
	public JsonElement toJsonElement() {
		return JsonUtils.toJson(this);
	}

	public String getName() {
		return name;
	}

	public TableSchema setName(String name) {
		this.name = name;
		return this;
	}
	
	public TableSchema addColumn(String name) {
		getColumns().add(new Column(name));
		return this;
	}
	
	public TableSchema addColumn(String name, ColumnType type) {
		getColumns().add(new Column(name, type));
		return this;
	}
	
	public TableSchema addColumn(String name, ColumnType type, boolean isPrimaryKey) {
		getColumns().add(new Column(name, type, isPrimaryKey));
		return this;
	}
	
	public TableSchema addColumn(String name, ColumnType type, boolean isPrimaryKey, boolean isIndexed) {
		getColumns().add(new Column(name, type, isPrimaryKey, isIndexed));
		return this;
	}

	public List<Column> getColumns() {
		if (columns == null)
			columns = new UniqueList<>();
		return columns;
	}

	private void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public Column getColumn(String column) {
		for (Column c : getColumns()) {
			if (c.getName().equals(column))
				return c;
		}
		return null;
	}

	public String getPrimaryKeyName() {
		for (Column column : getColumns()) {
			if (column.isPrimaryKey())
				return column.getName();
		}
		return null;
	}
}
