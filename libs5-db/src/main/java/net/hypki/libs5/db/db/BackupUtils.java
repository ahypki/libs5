package net.hypki.libs5.db.db;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.stream.JsonReader;

public class BackupUtils {
	
	public static void restoreKeyspace(final String keyspace, final List<String> skipTables, 
			final String inputFile) throws IOException {
		Watch w = new Watch();
		LibsLogger.debug(BackupUtils.class, "Restoring backup for keyspace ", keyspace, " from the file ",
				inputFile, "...");
		
		List<Mutation> muts = new ArrayList<Mutation>();
		
		try {
			try (JsonReader reader = new JsonReader(new FileReader(inputFile))) {

				// beginning of backup (JsonObject)
	            reader.beginObject();

	            while (reader.hasNext()) {

	            	// table name (key)
	            	final String tableName = reader.nextName();
	            	
	            	// table rows (JsonArray)
	            	reader.beginArray();
	            	
	            	while (reader.hasNext()) {
	            		// reading row (JsonObject)
	            		Row r = JsonUtils.getGson().fromJson(reader, Row.class);
	            		
	            		muts.add(new Mutation(MutationType.INSERT, 
	            				keyspace, 
	            				tableName, 
	            				r.getPk(), 
	            				DbObject.COLUMN_PK,
	            				DbObject.COLUMN_DATA, 
	            				r.getColumns().get(DbObject.COLUMN_DATA)));
	            		
	            		if (muts.size() >= 1) {
	            			try {
								DbObject
									.getDatabaseProvider()
									.save(muts);
							} catch (Exception e) {
								LibsLogger.error(BackupUtils.class, "Cannot save row " + r, e);
							}
	            			muts.clear();
	            		}
	            	}
	            	
	            	// end of table rows
	            	reader.endArray();
	            }

	            reader.endObject();
			}
			
			LibsLogger.debug(BackupUtils.class, "Backup for a keyspace ", keyspace, " restored from the file ",
					inputFile, " in ", w.toString());
		} catch (Throwable t) {
			LibsLogger.error(BackupUtils.class, "Cannot restore backup " + inputFile, t);
			throw new IOException("Cannot restore backup " + inputFile, t);
		}
	}

	/**
	 * The function creates the output folder.
	 * 
	 * @param keyspace
	 * @param skipTables
	 * @param outputFile
	 * @throws IOException
	 */
	public static void backupKeyspace(final String keyspace, final List<String> skipTables, final String outputFile) throws IOException {
		Watch w = new Watch();
		LibsLogger.debug(BackupUtils.class, "Creating backup for keyspace ", keyspace, " to the file ",
				outputFile, "...");
		
		// creating output folder
		FileExt out = new FileExt(outputFile);
		if (out.getParentFile() != null)
			out.getParentFile().mkdirs();
		
		// writing the whole database to the file
		try (final FileWriter fw = new FileWriter(outputFile)) {
			fw.append("{\n");
			
			boolean addSemi = false;
			for (String table : DbObject.getDatabaseProvider().getTableList(keyspace)) {
				if (skipTables != null && skipTables.contains(table)) {
					LibsLogger.debug(BackupUtils.class, "Skipping table " + table + " for backup");
					continue;
				}
				
				if (addSemi)
					fw.append(",\n");
				
				// starting JSON array
				fw.append(String.format("\t\"%s\" : [\n", table));
				
				// writing rows of the table
				boolean addSemi2 = false;
				LibsLogger.debug(BackupUtils.class, "Creating backup for table ", table);
				for (Row row : DbObject.getDatabaseProvider().tableIterable(keyspace, table)) {
					if (addSemi2)
						fw.append(",\n");
					fw.append(JsonUtils.objectToStringPretty(row));
					addSemi2 = true;
				}
				
				// finishing JSON array
				fw.append("\n\t]");
				
				addSemi = true;
			}
			
			fw.append("\n}");
			
			LibsLogger.debug(BackupUtils.class, "Backup for a keyspace ", keyspace, " done to the file ",
					out.getAbsolutePath(), " in ", w.toString());
		} catch (Throwable t) {
			LibsLogger.error(BackupUtils.class, "Cannot backup keyspace " + keyspace, t);
		}
	}
}
