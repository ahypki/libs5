package net.hypki.libs5.db.db.weblibs.utils;

import java.io.Serializable;

import net.hypki.libs5.utils.guid.RandomGUID;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class UUIDPair implements Serializable {
	
	@Expose
	private UUID value0 = null;
	
	@Expose
	private UUID value1 = null;
	
	public UUIDPair() {
		
	}
	
	public UUIDPair(UUID value0, UUID value1) {
		setValue0(value0);
		setValue1(value1);
	}
	
	@Override
	public String toString() {
		return String.format("%s:%s", getValue0(), getValue1());
	}

	public UUID getValue0() {
		return value0;
	}

	public void setValue0(UUID value0) {
		this.value0 = value0;
	}

	public UUID getValue1() {
		return value1;
	}

	public void setValue1(UUID value1) {
		this.value1 = value1;
	}
}
