package net.hypki.libs5.db;

import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Event {
	
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_DATE = "date";

	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String id = null;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String category = null;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private long created = 0;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private String data = null;
	
	public Event() {
		
	}
	
	public Event(String id, String category, SimpleDate created, String data) {
		setId(id);
		setCategory(category);
		setCreated(created);
		setData(data);
	}
	
	public String getKey() {
		return getId();
	}
	
	public TableSchema getTableSchema() {
		return new TableSchema(getClass().getSimpleName())
			.addColumn(DatabaseProviderTestCase.COLUMN_PK, ColumnType.STRING, true)
			.addColumn(COLUMN_CATEGORY, ColumnType.STRING, true)
			.addColumn(COLUMN_DATE, ColumnType.LONG)
			.addColumn(DatabaseProviderTestCase.COLUMN_DATA, ColumnType.STRING);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}
	
	public void setCreated(SimpleDate created) {
		this.created = created.getTimeInMillis();
	}

	public String getData() {
		this.data = JsonUtils.objectToString(this);
		return data;
	}
	
	public int getDataAsInt() {
		return NumberUtils.toInt(getData());
	}

	public void setData(String data) {
		this.data = data;
	}
}
