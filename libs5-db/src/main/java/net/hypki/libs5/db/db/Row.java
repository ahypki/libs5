package net.hypki.libs5.db.db;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.math.Expression;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.utils.sha.SHAManager;
import net.hypki.libs5.utils.utils.NumberUtils;

public class Row implements Serializable {
	
	@Expose
	private Object pk = null;

	@Expose
	private Map<String, Object> columns = null;
	
	public Row() {
		
	}
	
	public Row(Object pk) {
		setPk(pk);
	}
	
	public Row(Object pk, Map<String, Object> columns) {
		setPk(pk);
		setColumns(columns);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (getPk() != null) {
			sb.append(getPk().toString());
			sb.append(": ");
		}
		for (Map.Entry<String, Object> entry : getColumns().entrySet()) {
			sb.append(entry.getKey());
			sb.append("=");
			sb.append(entry.getValue());
			sb.append(" ");
		}
		return sb.toString();
	}
	
	public String toJson() {
		return JsonUtils.objectToString(this);
	}
	
	public String toJsonColumns() {
		return JsonUtils.objectToString(getColumns());
	}
	
	public JsonElement toJsonElement() {
		return JsonUtils.toJson(this);
	}

	public Map<String, Object> getColumns() {
		if (columns == null)
			columns = new HashMap<>();
		return columns;
	}
	
	public Object get(String columnName) {
		return getColumns().get(columnName);
	}
	
	public Object getColumnWhichEndsWith(String columnName) {
		for (Map.Entry<String, Object> entry : getColumns().entrySet())
			if (entry.getKey().endsWith(columnName))
				return entry.getValue();
		return null;
	}
	
	public Object get(String columnName, Object defaultValue) {
		Object o = getColumns().get(columnName);
		return o != null ? o : defaultValue;
	}
	
	public Double getAsDouble(String columnName) {
		Object o = get(columnName);
		if (o instanceof Double)
			return (Double) o;
		else if (o instanceof Long)
			return ((Long) o).doubleValue();
		else if (o instanceof Integer)
			return ((Integer) o).doubleValue();
		else if (o == null)
			return 0.0;
		else if (o instanceof String)
			return NumberUtils.toDouble((String) o);
		else if (o instanceof BigDecimal)
			return ((BigDecimal) o).doubleValue();
		else {
			LibsLogger.error(Row.class, "Unimplemented case, " + "Cannot convert " + o 
					+ "(" + o.getClass() + ") to Double");
			return 0.0;
//			throw new RuntimeException("Cannot convert " + o + " to Double");
		}
	}
	
	public String getAsString(String columnName) {
		Object o = get(columnName);
		if (o == null)
			return null;
		else if (o instanceof String)
			return (String) o;
		else
			return String.valueOf(o);
//		return (String) get(columnName);
	}
	
	public int getAsInt(String columnName) {
		Object o = get(columnName);
		if (o instanceof Double) {
			return ((Double) o).intValue();
		} else if (o instanceof Long) {
			return ((Long) o).intValue();
		} else if (o instanceof Integer) {
			return (int) o;
		} else if (o == null) {
			return 0;
		} else {
			LibsLogger.error(Row.class, "Unimplemented case, " + "Cannot convert " + o 
					+ "(" + o.getClass() + ") to Integer");
			return 0;
//			throw new RuntimeException("Cannot convert " + o + " to Integer");
		}
	}
	
	public long getAsLong(String columnName) {
		Object o = get(columnName);
		if (o instanceof Double) {
			return ((Double) o).longValue();
		} else if (o instanceof Long) {
			return (Long) o;
		} else if (o instanceof Integer) {
			return ((Integer) o).longValue();
		} else if (o == null) {
			return 0L;
		} else if (o instanceof String) {
			return NumberUtils.toLong((String) o);
		} else if (o instanceof BigDecimal) {
			return ((BigDecimal) o).longValue();
		} else {
			LibsLogger.error(Row.class, "Unimplemented case, " + "Cannot convert " + o 
					+ "(" + o.getClass() + ") to Long");
			return 0L;
//			throw new RuntimeException("Cannot convert " + o + " to Long");
		}
	}
	
	public Map getAsMap(String columnName) {
		return (Map) get(columnName);
	}
	
	public Row set(String columnName, Object value) {
		getColumns().put(columnName, value);
		return this;
	}

	public void setColumns(Map<String, Object> columns) {
		this.columns = columns;
	}
	
	public Row addColumn(String column, Object value) {
		getColumns().put(column, value);
		return this;
	}
	
	public Row removeColumn(String column) {
		getColumns().remove(column);
		return this;
	}

	public Object getPk() {
		return pk;
	}

	public void setPk(Object pk) {
		this.pk = pk;
	}
	
	public int size() {
		return this.columns != null ? this.columns.size() : 0;
	}

	public <T> T getAsObject(String columnName, Class<T> clazz) {
		return (T) JsonUtils.fromJson(getAsString(columnName), clazz);
	}

	public Row addColumns(Map<String, Object> value) {
		if (value != null)
			for (Map.Entry<String, Object> entry : value.entrySet()) {
				addColumn(entry.getKey(), entry.getValue());
			}
		return this;
	}
	
	public Row addColumns(Map<String, Object> value, String columnPrefix) {
		if (value != null)
			for (Map.Entry<String, Object> entry : value.entrySet()) {
				if (columnPrefix != null)
					addColumn(columnPrefix + entry.getKey(), entry.getValue());
				else
					addColumn(entry.getKey(), entry.getValue());
			}
		return this;
	}
	
	public int compare(List<String> columns, Row r2) {
		try {
			for (String col : columns) {
				int comp2 = ((Comparable) get(col)).compareTo((Comparable) r2.get(col));
				if (comp2 < 0)
					return comp2;
				else if (comp2 > 0)
					return comp2;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public int compare(String column, Comparable inputValue) {
		return compare((Comparable) get(column), inputValue);
	}
	
	private int compare(Comparable c1, Comparable c2) {
		if (c1 == null || c2 == null)
			return 0;
		else if (c1.getClass().equals(c2.getClass()))
			return c1.compareTo(c2);
		else if (c1 instanceof Double && c2 instanceof Integer)
			return ((Double)c1).compareTo(Double.valueOf((Integer) c2));
		else if (c1 instanceof Double && c2 instanceof Long)
			return ((Double)c1).compareTo(Double.valueOf((Long) c2));
		else if (c2 instanceof Double && c1 instanceof Integer)
			return ((Double)c2).compareTo(Double.valueOf((Integer) c1));
		else if (c2 instanceof Double && c1 instanceof Long)
			return ((Double)c2).compareTo(Double.valueOf((Long) c1));
		else
			return 0;
	}
	
	public void clear() {
		if (columns != null)
			columns.clear();
	}
	
	public boolean isFullfiled(C3Where where, boolean defaultIfColumnIsMissing) {
		if (where != null)
			for (C3Clause clause : where.getClauses()) {
				Object v = get(clause.getField());
				
				if (v == null && defaultIfColumnIsMissing == false)
					return false;
				
				if (clause.getClauseType() == ClauseType.EQ) {
					
					if (!clause.getValue().equals(v))
						return false;
					
				} else if (clause.getClauseType() == ClauseType.GE) {
						
					int comp = compare((Comparable) v, (Comparable) clause.getValue());
					if (comp < 0)
						return false;
					
				} else if (clause.getClauseType() == ClauseType.GT) {
						
					int comp = compare((Comparable) v, (Comparable) clause.getValue());
					if (comp <= 0)
						return false;

				} else if (clause.getClauseType() == ClauseType.LE) {
					
					int comp = compare((Comparable) v, (Comparable) clause.getValue());
					if (comp > 0)
						return false;
					
				} else if (clause.getClauseType() == ClauseType.LT) {
					
					int comp = compare((Comparable) v, (Comparable) clause.getValue());
					if (comp >= 0)
						return false;
					
				} else if (clause.getClauseType() == ClauseType.IN) {
					
					if (!((List)clause.getValue()).contains(v))
						return false;
					
				} else if (clause.getClauseType() == ClauseType.CONTAINS) {
					
					if (!((Set)v).contains(clause.getValue()))
						return false;
					
				} else if (clause.getClauseType() == ClauseType.NOT_CONTAINS) {
					
					if (((Set)v).contains(clause.getValue()))
						return false;
				} else
					
					throw new NotImplementedException();
			}
		return true;
	}

	public boolean contains(String column) {
		return this.columns.containsKey(column);
	}
	
	private BigDecimal computeNoPrefix(String expr) {
		if (expr == null || expr.length() == 0)
			return new BigDecimal(0.0);
		
		for (int wordStart = 0; wordStart < expr.length(); wordStart++) {
			if (!Character.isLetter(expr.charAt(wordStart)))
				continue;
						
			int wordStop = -1;
			for (int i = wordStart + 1; i < expr.length(); i++) {
				if (!(Character.isLetterOrDigit(expr.charAt(i))
						|| expr.charAt(i) == '_'
						|| expr.charAt(i) == ':'
						|| expr.charAt(i) == '.')) {
					wordStop = i;
					break;
				} 
			}
			
			if (wordStop == -1)
				wordStop = expr.length();
			
			String tmp = expr.substring(wordStart, wordStop);
			Object v = this.get(tmp);
			if (v != null) {
				String newVal = String.valueOf(v);
							
				expr = expr.substring(0, wordStart) + newVal + expr.substring(wordStop);
				
				wordStart += newVal.length();
			} else {
				wordStart += tmp.length();
			}
		}
		return net.hypki.libs5.utils.math.MathUtils.eval(expr);
	}

	public BigDecimal compute(String expr, String varPrefix) {
		if (expr == null || expr.length() == 0)
			return new BigDecimal(0.0);
		
		if (varPrefix == null)
			return computeNoPrefix(expr);
		
		while (true) {
			int wordStart = expr.indexOf('$');
			
			if (wordStart == -1)
				break;
			
			int wordStop = -1;
			for (int i = wordStart + 1; i < expr.length(); i++) {
				if (!(Character.isLetterOrDigit(expr.charAt(i))
						|| expr.charAt(i) == '_')) {
					wordStop = i;
					break;
				} 
			}
			
			if (wordStop == -1)
				wordStop = expr.length();
			
			String tmp = expr.substring(wordStart + 1, wordStop);
//			if (!tmp.equalsIgnoreCase("LOG10") 
//					&& !tmp.equalsIgnoreCase("PI")
//					&& !tmp.equalsIgnoreCase("MAX")
//					&& !tmp.equalsIgnoreCase("MIN"))
//			expr = expr.replace("$" + tmp, String.valueOf(get(tmp)));
			expr = expr.substring(0, wordStart) + String.valueOf(get(tmp)) + expr.substring(wordStop);
		}
		return MathUtils.eval(expr);
	}
	
	public long getMemorySize() {
		try {
			long mem = 0;
			
			for (Map.Entry<String, Object> e : getColumns().entrySet()) {
				if (e.getValue() != null && e.getValue() instanceof Double)
					mem += 8;
				else if (e.getValue() != null && e.getValue() instanceof Long)
					mem += 8;
				else if (e.getValue() != null && e.getValue() instanceof Integer)
					mem += 4;
				else if (e.getValue() != null && e.getValue() instanceof String)
					mem += ((String) e.getValue()).getBytes().length;
				else
					mem += 8;
			}
					
			return mem;
		} catch (Exception e) {
			LibsLogger.error(Row.class, "Cannot check the RAM size of the Row", e);
			return 512;
		}
	}
	
	public String getSha512() {
		try {
			StringBuilder sb = new StringBuilder();
			for (String colName : getColumns().keySet().stream().sorted().collect(Collectors.toList())) {
				sb.append(colName);
				sb.append(get(colName));
			}
			return SHAManager.getSHA(sb.toString());
		} catch (IOException e) {
			LibsLogger.error(Row.class, "Cannot compute SHA for row", e);
			return null;
		}
	}
}
