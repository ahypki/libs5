package net.hypki.libs5.db.db.schema;

import java.util.Set;

import net.hypki.libs5.utils.collections.UniqueList;

public class DatabaseSchema {

	private String name = null;
	
	private UniqueList<TableSchema> tables = null;
	
	public DatabaseSchema() {
		
	}
	
	public DatabaseSchema(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public DatabaseSchema setName(String name) {
		this.name = name;
		return this;
	}
	
	public DatabaseSchema addTable(TableSchema table) {
		getTables().add(table);
		return this;
	}

	public UniqueList<TableSchema> getTables() {
		if (tables == null)
			tables = new UniqueList<>();
		return tables;
	}

	private void setColumns(UniqueList<TableSchema> tables) {
		this.tables = tables;
	}
}
