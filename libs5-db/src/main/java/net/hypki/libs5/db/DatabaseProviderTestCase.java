package net.hypki.libs5.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;
import net.hypki.libs5.utils.utils.NumberUtils;

public class DatabaseProviderTestCase extends LibsTestCase {
	
	private static final String KEYSPACE = "test_keyspace";
	public static final String COLUMN_PK = "pk";
	public static final String COLUMN_DATA = "data";
	
	private static int personIdGen = 1;
	private static int eventIdGen = 1;

	protected void testAll(DatabaseProvider db) throws IOException, ValidationException {
		db.clearKeyspace(KEYSPACE);
		assertTrue(isKeyspaceEmpty(db), "Keyspace should be empty");
		
		db.createTable(KEYSPACE, new Person().getTableSchema());
		db.createTable(KEYSPACE, new Event().getTableSchema());
		
		testSimpleSave(db);
		
		testList(db);
		
		testEventList(db);
	}
	
	protected void testEventList(DatabaseProvider db) throws IOException, ValidationException {
		int all = 400;
		int allCat1 = 0;
		int sum = 0;
		
		List<Event> events = new ArrayList<Event>();
		for (int i = 0; i < all; i++) {
			Event p = createEvent("cat" + RandomUtils.nextInt(1, 5));
			sum += NumberUtils.toInt(p.getId());
			events.add(p);
			
			if (p.getCategory().equals("cat1"))
				allCat1++;
		}
		saveEvent(db, events);
		
		int sumCheck = 0;
		int allCheck = 0;
		for (Row row : db.tableIterable(KEYSPACE, Event.class.getSimpleName())) {
			Event e = row.getAsObject(COLUMN_DATA, Event.class);
			
//			if (last != null)
//				assertTrue(e.getCreated().isGE(last));

			allCheck++;
			sumCheck += NumberUtils.toInt(e.getId());
		}
		
		assertTrue(allCheck > 0, "Expected allCheck > 0");
		assertTrue(sumCheck > 0, "Expected sumCheck > 0");
		assertTrue(all == allCheck, "Expected all " + all + " and allCheck " + allCheck);
		assertTrue(sum == sumCheck, "Expected sum " + sum + " and sumCheck " + sumCheck);
		
		allCheck = 0;
		for (Row r : db.getList(KEYSPACE, 
				Event.class.getSimpleName(), 
				new String[]{COLUMN_DATA}, 
				new Order(Event.COLUMN_DATE), 
				500, 
				null,
//				null
				new C3Where()
					.addClause(new C3Clause(Event.COLUMN_CATEGORY, ClauseType.EQ, "cat1"))
				)
					) {
			LibsLogger.info(DatabaseProviderTestCase.class, r.getAsObject(COLUMN_DATA, Event.class).getCreated());
			
			allCheck++;
		}
		assertTrue(allCat1 == allCheck, "Expected all " + allCat1 + " and allCheck " + allCheck);
	}
	
	protected void testList(DatabaseProvider db) throws IOException, ValidationException {
		final int all = 345;
		int ageSum = 0;
		
		List<Person> persons = new ArrayList<Person>();
		for (int i = 0; i < all; i++) {
			Person p = createPersonSimple();
			ageSum += p.getAge();
			persons.add(p);
			if (persons.size() == 1000) {
				savePerson(db, persons);
				persons = new ArrayList<Person>();
			}
		}
		savePerson(db, persons);
		
		int testCount = 0;
		int testAgeSum = 0;
		for (Row r : db.tableIterable(KEYSPACE, Person.class.getSimpleName())) {
			Person p = r.getAsObject(COLUMN_DATA, Person.class);
			testCount++;
			testAgeSum += p.getAge();
		}
		
		assertTrue(all == testCount, "Expected " + all + " but there is " + testCount);
		assertTrue(ageSum == testAgeSum);
	}
	
	protected void testSimpleSave(DatabaseProvider db) throws IOException, ValidationException {
		Person p = new Person("1", "John", 10);
		
		savePerson(db, p);
//		takeANap(1500);
		
		Person pFromDb = db.get(KEYSPACE, 
				Person.class.getSimpleName(), 
				new C3Where()
					.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, "1")), 
				COLUMN_DATA, 
				Person.class);
		
		assertTrue(pFromDb != null);
		assertTrue(pFromDb.getName().equals(p.getName()));
		assertTrue(pFromDb.getAge() == p.getAge());
		
		db.remove(KEYSPACE, 
				Person.class.getSimpleName(), 
				"1", 
				COLUMN_PK, 
				COLUMN_DATA);
//		takeANap(1000);
		
		pFromDb = db.get(KEYSPACE, 
				Person.class.getSimpleName(), 
				new C3Where()
					.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, "1")), 
				COLUMN_DATA, 
				Person.class);
		assertTrue(pFromDb == null, "Object expected to be removed");
		
		assertTrue(isKeyspaceEmpty(db), "Keyspace should be empty");
	}
	
	private void savePerson(DatabaseProvider db, List<Person> persons) throws IOException, ValidationException {
		MutationList muts = new MutationList();
		for (Person p : persons) {
			muts.addMutation(toMut(p));
		}
		if (muts.size() > 0)
			db.save(muts);
	}
	
	private void saveEvent(DatabaseProvider db, List<Event> events) throws IOException, ValidationException {
		MutationList muts = new MutationList();
		for (Event e : events) {
			muts.addMutation(toMut(e));
		}
		if (muts.size() > 0)
			db.save(muts);
	}

	private void savePerson(DatabaseProvider db, Person p) throws IOException, ValidationException {
		db.save(new MutationList()
				.addMutation(toMut(p)));
	}
	
	private Mutation toMut(Person p) {
		return new Mutation(MutationType.INSERT, 
				KEYSPACE, 
				Person.class.getSimpleName(), 
				p.getId(), 
				COLUMN_PK, 
				COLUMN_DATA, 
				JsonUtils.objectToStringPretty(p));
	}
	
	private MutationList toMut(Event e) {
		return new MutationList()
//			.addMutation(new Mutation(MutationType.INSERT, 
//				KEYSPACE, 
//				Event.class.getSimpleName(),
//				new C3Where()
//					.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, e.getKey()))
//					.addClause(new C3Clause(Event.COLUMN_CATEGORY, ClauseType.EQ, e.getCategory())),
//				COLUMN_PK, 
//				e.getKey()))
//			.addMutation(new Mutation(MutationType.INSERT, 
//				KEYSPACE, 
//				Event.class.getSimpleName(),
//				new C3Where()
//					.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, e.getKey()))
//					.addClause(new C3Clause(Event.COLUMN_CATEGORY, ClauseType.EQ, e.getCategory())),
//				Event.COLUMN_CATEGORY, 
//				e.getCategory()))
//			.addMutation(new Mutation(MutationType.INSERT, 
//				KEYSPACE, 
//				Event.class.getSimpleName(),
//				new C3Where()
//					.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, e.getKey()))
//					.addClause(new C3Clause(Event.COLUMN_CATEGORY, ClauseType.EQ, e.getCategory())),
//				Event.COLUMN_DATE, 
//				e.getCreated().getTimeInMillis()))
			.addMutation(new Mutation(MutationType.INSERT, 
				KEYSPACE, 
				Event.class.getSimpleName(),
				new C3Where()
					.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, e.getKey()))
					.addClause(new C3Clause(Event.COLUMN_CATEGORY, ClauseType.EQ, e.getCategory()))
					.addClause(new C3Clause(Event.COLUMN_DATE, ClauseType.EQ, e.getCreated())),
				COLUMN_DATA, 
				e.getData()))
			;
	}
	
	private boolean isKeyspaceEmpty(DatabaseProvider db) throws IOException {
		if (db.isTableEmpty(KEYSPACE, Person.class.getSimpleName()) == false)
			return false;
		return true;
	}
	
	private Person createPersonSimple() {
		return new Person(String.valueOf(personIdGen++), 
				RandomUtils.firstName(),
				RandomUtils.nextInt(1, 100));
	}
	
	private Event createEvent(String category) {
		return new Event("" + eventIdGen++, 
				category,
				SimpleDate.now(),
				"" + RandomUtils.nextInt(1, 100)
				);
	}
}
