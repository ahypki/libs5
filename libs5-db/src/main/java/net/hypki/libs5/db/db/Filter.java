package net.hypki.libs5.db.db;

public interface Filter<T extends DbObject> {
	public boolean isAccepted(T obj);
}
