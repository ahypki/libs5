package net.hypki.libs5.db.weblibs;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.FileNotFoundException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;

import com.google.gson.JsonElement;

public class Settings {
	
	private static String settingsPath = null;
	
	private static JsonElement settingsCache = null;
	
	private static String[] argsCache = null;
	
	public final static String DEFAULT_SETTINGS_FILENAME = "libs4-settings.json";
	
	public static void init(String[] args) {
		if (argsCache == null) {
			argsCache = args;
			
			// read settings path from -D... input parameters for jvm
			String settingsPath = ArgsUtils.getString(argsCache, "settings", null);
			
			if (StringUtilities.nullOrEmpty(settingsPath)) {
				settingsCache = null;
				return;
			}
			
			// read json settings
			LibsLogger.debug("Reading configuration from " + Settings.class.getResource("/" + settingsPath));
			JsonElement settings = JsonUtils.parseJson(FileUtils.readString(Settings.class.getResourceAsStream("/" + settingsPath)));
			
			Settings.settingsPath = settingsPath;
			Settings.settingsCache = settings;
		}
	}
	
	private static void init() {
		try {
			// read settings from default settings file
			String settingsPath = DEFAULT_SETTINGS_FILENAME;
			
			// read json settings
			LibsLogger.debug("Reading configuration from " + Settings.class.getResource("/" + settingsPath));
			JsonElement settings = JsonUtils.parseJson(FileUtils.readString(Settings.class.getResourceAsStream("/" + settingsPath)));
			
			Settings.settingsPath = settingsPath;
			Settings.settingsCache = settings;
		} catch (Exception e) {
			LibsLogger.error(Settings.class, "Cannot read settings from default file " + 
					DEFAULT_SETTINGS_FILENAME, e);
		}
	}
	
	public static String getSettingsPath() {
		return Settings.settingsPath;
	}
	
	public static boolean getBoolean(String jsonPath, boolean defaultValue) {
		return JsonUtils.readBoolean(getSettings(), jsonPath, defaultValue);
	}
	
	public static String getString(String jsonPath) {
		return JsonUtils.readString(getSettings(), jsonPath);
	}
	
	public static String getString(String jsonPath, String defaultValue) {
		String tmp = JsonUtils.readString(getSettings(), jsonPath);
		return tmp != null ? tmp : defaultValue;
	}
	
	public static JsonElement getSettings() {
		if (settingsCache != null)
			return settingsCache;
		
		init();
		
		if (settingsCache != null)
			return settingsCache;
		
		throw new RuntimeException("Settings class is not initialized witn any of the init() methods"); 
	}

	public static int getInt(String jsonPath, int defaultValue) {
		return JsonUtils.readInt(getSettings(), jsonPath, defaultValue);
	}
	
	public static int getInt(String jsonPath) {
		return getInt(jsonPath, 0);
	}
	
	public static float getFloat(String jsonPath, float defaultValue) {
		return JsonUtils.readFloat(getSettings(), jsonPath, defaultValue);
	}
}
