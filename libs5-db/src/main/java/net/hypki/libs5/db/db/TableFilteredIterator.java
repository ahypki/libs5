package net.hypki.libs5.db.db;

import net.hypki.libs5.db.db.weblibs.C3Where;

public class TableFilteredIterator<T extends DbObject> extends TableIterator<T> {
	
	private Filter<T> filter = null;
	
	private T next = null;

	public TableFilteredIterator(final String keyspace, final String table, final String column, final C3Where c3Where,
			final Class<T> clazz, Filter<T> filter) {
		super(keyspace, table, column, c3Where, clazz);
		
		setFilter(filter);
	}

	public Filter<T> getFilter() {
		return filter;
	}

	public void setFilter(Filter<T> filter) {
		this.filter = filter;
	}
	
	@Override
	public boolean hasNext() {
		if (getFilter() == null)
			return super.hasNext();
		
		while (super.hasNext()) {
			T obj = super.next();
			if (getFilter().isAccepted(obj)) {
				next = obj;
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public T next() {
		return next;
	}
}
