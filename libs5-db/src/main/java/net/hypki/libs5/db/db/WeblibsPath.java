package net.hypki.libs5.db.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jodd.util.ArraysUtil;
import jodd.util.StringUtil;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.CheckWith;
import net.sf.oval.constraint.CheckWithCheck.SimpleCheck;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;
import net.sf.oval.constraint.ValidateWithMethod;

import com.google.gson.annotations.Expose;

@CheckWith(value = WeblibsPath.class, errorCode = "FileBrowser.Error.InvalidPath")
public class WeblibsPath implements SimpleCheck, Serializable {
	
	private static final long serialVersionUID = 1234469031547943542L;
	
	public static final char PATH_DELIMITER = '/';
	public static final String PATH_DELIMITER_STRING = "/";
	
//	public static final String PATH_DELIMITER_STRING_ALT = ",";
	
	@NotNull
	@Expose
	private String[] path = null;
	
//	@NotNull
	@NotEmpty
	@Length(min = 1, max = 256)
	@ValidateWithMethod(methodName = "isFilenameValid", parameterType = String.class, errorCode = "ErrorCode.FilenameIncorrect", message = "Filename name is incorrect")
	@Expose
	private String filename = null;

	public WeblibsPath() {
		path = new String[0];
	}
	
	public WeblibsPath(String path, String filename) {
		setPath(path);
		setFilename(filename);
	}
	
	public WeblibsPath(String path) {
		setPath(path);
	}
	
	public void setPath(String path) {
		if (path == null)
			this.path = null;
		
		String parts[] = StringUtil.splitc(path, WeblibsPath.PATH_DELIMITER);
		int size = 0;
		for (int i = 0; i < parts.length; i++)
			if (!StringUtilities.nullOrEmpty(parts[i]))
				size++;
		
		this.path = new String[size];
		for (int i = 0, k = 0; i < parts.length; i++)
			if (!StringUtilities.nullOrEmpty(parts[i]))
				this.path[k++] = parts[i];
	}

	public void setPath(String[] path) {
		this.path = path;
	}

	public String[] getPath() {
		return path;
	}
	
	public boolean isFile() {
		return !StringUtilities.nullOrEmpty(getFilename());
	}
	
	@Override
	public String toString() {
		return toString(0, true, PATH_DELIMITER_STRING);
	}
	
	/**
	 * 
	 * @param depth If -1 then returns path without last folder, if -2 it returns path without 2 last folders etc.
	 * @param withFilename
	 * @return
	 */
	public String toString(int depth, boolean withFilename) {
		return toString(depth, withFilename, PATH_DELIMITER_STRING);
	}
	
	/**
	 * 
	 * @param depth If -1 then returns path withous last folder, if -2 it returns path without 2 last folders etc.
	 * @param withFilename
	 * @return
	 */
	public String toString(int depth, boolean withFilename, String delimiter) {
		if (delimiter == null)
			delimiter = PATH_DELIMITER_STRING;
		StringBuilder builder = new StringBuilder();
		builder.append(delimiter);
		for (int i = 0; path != null && i < (path.length + depth); i++) {
			builder.append(path[i]);
			builder.append(delimiter);
		}
		if (withFilename && getFilename() != null)
			builder.append(getFilename());
		return builder.toString();
	}

	public byte[] getBytes() {
		String temp = toString();
		return temp != null ? temp.getBytes() : null;
	}

	public String getLastFolder() {
		return getPath() != null && getPath().length > 0 ? getPath()[getPath().length - 1] : null;
	}
	
	public String getUpFolder() {
		return getPath() != null && getPath().length > 1 ? getPath()[getPath().length - 2] : null;
	}
	
	public static boolean isNameValid(String name) {
		return name != null && name.matches("[^\\r\\n]+");
	}
	
	public WeblibsPath add(String folder) {
		path = ArraysUtil.append(path, folder);
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WeblibsPath) {
			
			WeblibsPath inputPath = (WeblibsPath) obj;
			return inputPath.toString().equals(this.toString());
			
		} else if (obj instanceof String) {
			
			return ((String) obj).equals(this.toString());
			
		} else
			return super.equals(obj);
	}
	
	public static boolean isFilenameValid(String filename) {
		return filename != null && filename.matches("[^\\n]+");//a-zA-Z0-9\\_\\-\\.]+");
	}

	@Override
	public boolean isSatisfied(Object validatedObject, Object valueToValidate) {
		if (validatedObject instanceof WeblibsPath) {
			WeblibsPath path = (WeblibsPath) validatedObject;
			
//			int min = valueToValidate.getClass().getAnnotation(ArrayValidationAnnotation.class).min();
//			int max = valueToValidate.getClass().getAnnotation(ArrayValidationAnnotation.class).max();
//			return objs.length >= min && objs.length <= max;
			
			if (path.getPath() == null || path.getPath().length > 8)
				return false;
//				throw new OValException(valueToValidate.getClass().getAnnotation(PathValidationAnnotation.class).errorKey());
			
			if (path.getPath().length == 0 && StringUtilities.nullOrEmpty(path.getFilename()))
				return false;
			
			for (String s : path.getPath()) {
				if (WeblibsPath.isNameValid(s) == false)
					return false;
//					throw new OValException(valueToValidate.getClass().getAnnotation(PathValidationAnnotation.class).errorKey());
			}
			
			return true;
		}
		return false;
	}

	public String getTopFolder() {
		return getPath() != null && getPath().length > 0 ? getPath()[0] : null;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}
	
	public void replaceTopFolder(String folderName) {
		if (getPath() != null && getPath().length > 0)
			getPath()[getPath().length - 1] = folderName;
	}
	
	/**
	 * For path like: /one/two/three/ it returns: [/one/, /one/two/, /one/two/three/]
	 * @return
	 */
	public List<String> getPathParts() {
		ArrayList<String> parts = new ArrayList<String>();
		String tmp = "/";
//		parts.add(tmp);
//		parts.add(tmp);
		for (String s : getPath()) {
			tmp += s + "/";
			parts.add(tmp);
//			parts.add(s);
		}
		return parts;
	}

	public void addToTop(String name) {
		String [] tmpPath = new String[path != null ? path.length + 1 : 1];
		tmpPath[0] = name;
		for (int i = 0; i < this.path.length; i++) {
			tmpPath[i + 1] = this.path[i];
		}
		this.path = tmpPath;
	}

}
