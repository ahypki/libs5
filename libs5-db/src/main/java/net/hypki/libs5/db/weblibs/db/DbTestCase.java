package net.hypki.libs5.db.weblibs.db;

import static net.hypki.libs5.db.weblibs.Settings.getSettings;
import static net.hypki.libs5.utils.json.JsonUtils.readString;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.TxObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;

public class DbTestCase extends LibsTestCase {

	private ArrayList<DbObject> toRemove = new ArrayList<DbObject>();
	
	private static HashMap<String, Boolean> cfToClear = new HashMap();
	
	private static String KEYSPACE;
	private static String KEYSPACE_LOWERCASE;
	
	private static boolean initiated = false;
	
	private static List<String> channelNames = new ArrayList<>();
	
	public DbTestCase() {
		
	}
	
	protected static List<String> getChannelNames() {
		return channelNames;
	}
	
	/**
	 * INFO: Remember to add all dependent objects here!
	 * @param cassandraObject
	 */
	public void addToRemove(DbObject cassandraObject) {
		if (cassandraObject != null) {
			toRemove.add(cassandraObject);
			cfToClear.put(cassandraObject.getColumnFamily(), true);
		}
	}
	
//	@BeforeClass
//	public static void beforeUnitTest() throws IOException, ValidationException {
	private synchronized static void init() throws IOException, ValidationException {
		if (initiated == false) {
			initiated = true;
			
			System.setProperty("hazelcast.logging.type", "log4j");
			System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
	
//			args = args != null ? args : new String[] {"--settings", "settingsUnitTests.json"};
//			Settings.init(args);
//			if (nullOrEmpty(System.getProperty(Settings.WEBLIBS_SETTINGS))) {
//				LibsLogger.debug(DbTestCase.class, "Setting default settings file to settingsUnitTests.json");
//				System.setProperty(Settings.WEBLIBS_SETTINGS, "settingsUnitTests.json");
//			}
			
			KEYSPACE = readString(getSettings(), "Settings/Keyspace");
			KEYSPACE_LOWERCASE = KEYSPACE.toLowerCase();
		
			try {
				clearDatabase();
			} catch (Exception e) {
				LibsLogger.error(DbTestCase.class, "Cannot clear database", e);
			}
			
			try {
				clearSearchIndex();
			} catch (Exception e) {
				LibsLogger.error(DbTestCase.class, "Cannot clear index", e);
			}
			
			DbObject.getDatabaseProvider().createKeyspace(KEYSPACE);
		}
		
//		try {
//			checkKeyspaceEmpty();
//		} catch (Exception e) {
//			LibsLogger.error(WeblibsTestCase.class, "Cass is not empty before UT, clearing Cass...", e);
//			
//			// clear Cass and ES for the next use
//			CassandraObject.clearKeyspace();
//			SearchManager.searchInstance().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
//		}
	}
	
	@AfterClass
	public static void afterUnitTest() throws IOException, ValidationException {
		try {
			long start = System.currentTimeMillis();
			LibsLogger.debug(DbTestCase.class, "Finishing TestCase...");
			
			// run all pending jobs
//			runAllJobs();
			
			// check if keyspace is empty
			checkKeyspaceEmpty();
			
			for (String cf : cfToClear.keySet()) {
				DbObject.getDatabaseProvider().clearTable(KEYSPACE, cf);
			}
			
			LibsLogger.debug(DbTestCase.class, "TestCase finished in " + (System.currentTimeMillis() - start));
		} catch (Exception e) {
			LibsLogger.error(DbTestCase.class, "Error occured while closing test case, clearing Cass and ES for the next use", e);
			
			// clear Cass and ES for the next use
			clearDatabase();
			clearSearchIndex();
			
			throw e;
		} finally {
			LibsInit.shutdown();
		}
	}

	private static void checkKeyspaceEmpty() throws IOException {
		LibsLogger.debug(DbTestCase.class, "Checking if keyspace is empty");
		for (Object cf : DbObject.getDatabaseProvider().getTableList(KEYSPACE)) {
			String cfStr = cf.toString();
			
			// INFO TxObject is "append" only
			if (cfStr.equalsIgnoreCase(TxObject.COLUMN_FAMILY)
//					|| hCfDef.getName().equalsIgnoreCase(MonitMsgCass.COLUMN_FAMILY)
					)
				continue;
			
			if (DbObject.getDatabaseProvider().isTableEmpty(KEYSPACE, cfStr) == false)
				throw new IOException("ColumnFamily " + cf + " is not empty after unit tests");
		}
	}
	
//	protected static void rebuildAllTags() throws IOException {
//		try {
//			// rebuild tags
//			ToolRunner.run(new Configuration(), new TagUserMapReduce(), null);
//			ToolRunner.run(new Configuration(), new TagAppMapReduce(), null);
//			
//			// remove tags with point to no items
//			ToolRunner.run(new Configuration(), new TagUserClearMapReduce(), null);
//			ToolRunner.run(new Configuration(), new TagAppClearMapReduce(), null);
//		} catch (Exception e) {
//			throw new IOException("Cannot rebuild tags", e);
//		}
//	}

	@Before
	public void before() throws IOException, ValidationException {
		DbTestCase.init();
	}
	
	@After
	public void removeAndCheck() {
		for (DbObject c : toRemove) {
			try {
				if (StringUtilities.nullOrEmpty(c.getKey(), c.getColumn()))
					continue;
				
				// removing in ordinary way
				if (DbObject.getDatabaseProvider().exist(KEYSPACE, c.getColumnFamily(), 
						new C3Where().addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, c.getKey())), 
						c.getColumn()) == true)
					c.remove();
			} catch (IOException e) {
				throw new RuntimeException("Cannot remove " + c.toString(), e);
			} catch (ValidationException e) {
				throw new RuntimeException("Cannot remove " + c.toString(), e);
			}
			
		}
		
		toRemove.clear();
	}
	
	protected void clearColumnFamily(String keyspace, String columnFamily) throws IOException {
		DbObject.getDatabaseProvider().clearTable(keyspace, columnFamily);
	}
	
	protected static void clearDatabase() throws IOException {
		DbObject.getDatabaseProvider().clearKeyspace(KEYSPACE);
	}
	
	protected static void clearSearchIndex() throws IOException {
		
	}
}
