package net.hypki.libs5.db.db.weblibs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.db.uuid.UUIDTime;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;


public class TxObject extends DbObject {
	
	public static final String COLUMN_FAMILY = "tx";
	
	@NotNull
	@NotEmpty
	@Expose
	private List<Mutation> txMutations = new ArrayList<Mutation>();
		
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUIDTime time = null;
	
	@Expose 
	private TxStatus status = TxStatus.PENDING;
	
	@Expose 
	@NotNull
	@NotEmpty
	private String keyspace = null;

	public TxObject() {
		
	}
	
	public TxObject(final String keyspace, Mutation ... mutations) {
		setKeyspace(keyspace);
		
		for (Mutation mut : mutations) {
			getTxMutations().add(mut);
		}
		setTime(new UUIDTime(System.currentTimeMillis(), UUID.random()));
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getKey() {
		return getTime().toString();
	}
	
	public static TxObject getTxObject(final String keyspace, long createMs, UUID id) throws IOException {
		return getTxObject(keyspace, new UUIDTime(createMs, id));
	}
	
	public static TxObject getTxObject(final String keyspace, UUIDTime time) throws IOException {
		return DbObject.getDatabaseProvider().get(keyspace, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, time.toString())), 
				DbObject.COLUMN_DATA, 
				TxObject.class);
	}

	public void setTxMutations(List<Mutation> txObjects) {
		this.txMutations = txObjects;
	}

	public List<Mutation> getTxMutations() {
		return txMutations;
	}
	
	public <T> T getCassandraObject(int index, Class<T> modelClass) {
		return (T) getTxMutations().get(index).getCassandraObject((Class<? extends DbObject>) modelClass);
	}

	public void setStatus(TxStatus status) {
		this.status = status;
	}

	public TxStatus getStatus() {
		return status;
	}

	public void setTime(UUIDTime time) {
		this.time = time;
	}

	public UUIDTime getTime() {
		return time;
	}

	public Mutation[] getTxMutationsAsArray() {
		return getTxMutations().toArray(new Mutation[getTxMutations().size()]);
	}

	@Override
	public String getKeyspace() {
		return keyspace;
	}

	public void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}	
}
