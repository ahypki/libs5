package net.hypki.libs5.db.db.weblibs;

import static net.hypki.libs5.utils.string.StringUtilities.substringLast;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.ConstraintViolation;

public class ValidationException extends Exception {
	private static String defaultLanguage = "en";
	private static final long serialVersionUID = 6421205589195242308L;
	
	private List<ValidationContext> violations = null;

	public ValidationException() {
		
	}
	
	public ValidationException(List<ConstraintViolation> violations) {
		for (ConstraintViolation violation : violations) {
			addViolation(violation);
			
			if (violation.getCauses() != null)
				for (ConstraintViolation constraintViolationCause : violation.getCauses()) {
					addViolation(constraintViolationCause);
					
					if (constraintViolationCause.getCauses() != null)
						for (ConstraintViolation constraintViolationCause2 : constraintViolationCause.getCauses()) {
							addViolation(constraintViolationCause2);
						}
				}
//			getViolations().add(new ValidationContext(violation.getValidatedObject().getClass(), 
//					substringLast(violation.getContext().toString(), '.') + "#" + substringLast(violation.getErrorCode(), '.'), 
//					violation.toString()));
		}
	}
	
	public ValidationException(Class clazz, String name, String defaultMsg) {
		getViolations().add(new ValidationContext(clazz, name, defaultMsg));
	}

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(Throwable cause) {
		super(cause);
	}

	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
	
	private void addViolation(ConstraintViolation violation) {
		getViolations().add(new ValidationContext(violation.getValidatedObject().getClass(), 
				substringLast(violation.getContext().toString(), '.') + "#" + substringLast(violation.getErrorCode(), '.'), 
				violation.toString()));
	}

	public List<ValidationContext> getViolations() {
		if (violations == null)
			violations = new ArrayList<>();
		return violations;
	}

	@Override
	public void printStackTrace() {
		super.printStackTrace();
		
		for (ValidationContext violation : violations) {
			System.err.println(violation.toString());
		}
	}
	
	public static void setDefaultLanguage(String defaultLanguage) {
		ValidationException.defaultLanguage = defaultLanguage.toLowerCase();
	}
	
	public static String getDefaultLanguage() {
		return ValidationException.defaultLanguage;
	}
	
	@Override
	public String toString() {
		return toString(defaultLanguage);
	}
	
	public String toString(String languageId, String prefix, String suffix) {
		return toString(languageId, prefix, suffix, 0);
	}
	
//	@Override
//	public String getMessage() {
//		return toString();
//	}
	
	public String toString(String languageId, String prefix, String suffix, int maxErrCount) {
		int errCount = 0;
		StringBuilder builder = new StringBuilder();
		if (violations != null) {
			for (ValidationContext violation : violations) {
				errCount++;
				
				if (maxErrCount > 0 && errCount > maxErrCount)
					break;
				
				if (builder.length() > 0)
					builder.append(suffix);
				
				String tmp = violation.toString(languageId);
				if (StringUtilities.notEmpty(tmp)) {
					builder.append(prefix);
					builder.append(tmp);
					builder.append(".");
				}
			}
		} else {
			if (StringUtilities.notEmpty(super.getMessage()))
				builder.append(super.getMessage());
			else
				builder.append(super.toString());
		}
		return builder.toString();
	}
	
	public String toString(String languageId) {
		return toString(languageId, "", "\n");
	}
}
