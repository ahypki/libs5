package net.hypki.libs5.db.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mjson.Json;

public class Results implements Iterable<Row> {

	private String nextPage = null;
	
	private List<Row> rows = null;
	
	public Results() {
		
	}
	
	@Override
	public Iterator<Row> iterator() {
		return getRows().iterator();
	}

	public String getNextPage() {
		return nextPage;
	}

	public Results setNextPage(String nextPage) {
		this.nextPage = nextPage;
		return this;
	}

	public List<Row> getRows() {
		if (rows == null)
			rows = new ArrayList<>();
		return rows;
	}
	
	public Json getRowsAsJson() {
		Json rows = Json.array();
		for (Row row : this) {
			rows.add(Json.read(row.toJson().replace("NaN", "0.0"))); // TODO think about it because NaN -> 0.0 can change logic...
		}
		return rows;
	}

	public Results setRows(List<Row> rows) {
		this.rows = rows;
		return this;
	}
	
	public Results addRow(Row row) {
		getRows().add(row);
		return this;
	}
	
	public int size() {
		return this.rows != null ? this.rows.size() : 0;
	}
}
