package net.hypki.libs5.db.db.weblibs;

public class ValidationUtils {

	public static void validateTrue(boolean condition, String message) throws ValidationException {
		if (condition == false)
			throw new ValidationException(message);
	}
	
	public static void validateFalse(boolean condition, String message) throws ValidationException {
		if (condition == true)
			throw new ValidationException(message);
	}
}
