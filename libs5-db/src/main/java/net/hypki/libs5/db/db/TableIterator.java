package net.hypki.libs5.db.db;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.lang.NotImplementedException;

import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;

public class TableIterator<T extends DbObject> implements Iterable<T>, Iterator<T> {
	
	private static int PER_PAGE = 10000;

	private String keyspace = null;
	
	private String table = null;
	
	private String column = null;
	
	private C3Where c3Where = null;
	
	private Results res = null;
	
	private int resCounter = 0;
	
	private int resTotalCounter = 0;
	
	private DatabaseProvider databaseProvider = null;
	
	private Class<T> clazz = null;
	
	public TableIterator(final String keyspace, final String table, final String column, final C3Where c3Where,
			final Class<T> clazz) {
		setKeyspace(keyspace);
		setTable(table);
		setColumn(column);
		setC3Where(c3Where);
		setClazz(clazz);
	}
	
	public TableIterator(DatabaseProvider databaseProvider, final String keyspace, final String table, 
			final String column, final C3Where c3Where, final Class<T> clazz) {
		setDatabaseProvider(databaseProvider);
		setKeyspace(keyspace);
		setTable(table);
		setColumn(column);
		setC3Where(c3Where);
		setClazz(clazz);
	}
	
	private void reset() {
		setRes(null);
		this.resCounter = 0;
		setResTotalCounter(0);
	}

	@Override
	public boolean hasNext() {
		if (getRes() != null && resCounter < getRes().size())
			return true;
		
		try {
			nextPage();
		} catch (IOException e) {
			LibsLogger.error(TableIterator.class, "Cannot load next page", e);
			setRes(null);
			return false;
		}
		
		return getRes() != null && resCounter < getRes().size();
	}

	private void nextPage() throws IOException {
		if (getRes() != null && getRes().getNextPage() == null)
			return; // no more results

		this.resCounter = 0;
		
		String state = getRes() != null ? getRes().getNextPage() : null;
		setRes(getDatabaseProvider().getList(getKeyspace(), 
				getTable(), 
				new String[] {getColumn()}, 
				null, 
				PER_PAGE, 
				state, 
				getC3Where()));
	}

	@Override
	public T next() {
		this.resTotalCounter++;
		Row row = getRes().getRows().get(resCounter++);
		return JsonUtils.fromJson((String) row.getColumns().get(getColumn()), getClazz());
	}

	@Override
	public void remove() {
		throw new NotImplementedException();
	}

	@Override
	public Iterator<T> iterator() {
		reset();
		return this;
	}

	private String getKeyspace() {
		return keyspace;
	}

	private void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}

	private String getTable() {
		return table;
	}

	private void setTable(String table) {
		this.table = table;
	}

	private String getColumn() {
		return column;
	}

	private void setColumn(String column) {
		this.column = column;
	}

	private C3Where getC3Where() {
		return c3Where;
	}

	private void setC3Where(C3Where c3Where) {
		this.c3Where = c3Where;
	}

	private Results getRes() {
		return res;
	}

	private void setRes(Results res) {
		this.res = res;
	}

	public int getResTotalCounter() {
		return resTotalCounter;
	}

	private void setResTotalCounter(int resTotalCounter) {
		this.resTotalCounter = resTotalCounter;
	}

	private DatabaseProvider getDatabaseProvider() {
		if (databaseProvider == null)
			databaseProvider = DbObject.getDatabaseProvider();
		return databaseProvider;
	}

	private void setDatabaseProvider(DatabaseProvider databaseProvider) {
		this.databaseProvider = databaseProvider;
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}
}
