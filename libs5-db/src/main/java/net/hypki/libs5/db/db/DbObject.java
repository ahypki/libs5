package net.hypki.libs5.db.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.Validator;

import org.apache.commons.lang.NotImplementedException;

public abstract class DbObject<T> {

	public abstract String getKeyspace();	
	public abstract String getColumnFamily();
	public abstract String getKey();
	
	private static DatabaseProvider databaseProvider = null;
	
	public static final String COLUMN_DATA = "data";
	public static final String COLUMN_PK = "pk";
	
	// validation
	private static Validator validator = null;
	
	private static final int FETCH_SIZE_ITERATOR = 10000;
	
	public static void init(DatabaseProvider provider) {
		LibsLogger.debug(DbObject.class, "Initiating ", DbObject.class.getSimpleName(), " with ", provider.getClass().getSimpleName());
		
		databaseProvider = provider;
//		if (databaseProvider == null) {
//			try {
//				// trying to get DatabaseProvider class name from the VM arguments
//				String vmArg = System.getProperty(DatabaseProvider.class.getSimpleName());
//				LibsLogger.debug(DbObject.class, "Initializing ", DatabaseProvider.class.getSimpleName(), " from VM argument -D", DatabaseProvider.class.getSimpleName(), "=", vmArg);
//				
//				if (vmArg != null)
//					databaseProvider = (DatabaseProvider) Class.forName(vmArg).newInstance();
//				
//				if (databaseProvider == null) {
//					// trying to read setting from a file
//					LibsLogger.debug(DbObject.class, "Trying to use default settings libs5-db.settings");
//					JsonElement settings = JsonUtils.parseJson(FileUtils.readString(Settings.class.getResourceAsStream("/libs5-db.settings")));
//					
//					if (settings != null)
//						databaseProvider = (DatabaseProvider) Class.forName(JsonUtils.readString(settings, DatabaseProvider.class.getSimpleName())).newInstance();
//				}
//				
//				if (databaseProvider == null)
//					throw new RuntimeException("Cannot initialize " + DatabaseProvider.class.getSimpleName() + ", cannot find settings");
//			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
//				throw new RuntimeException("Cannot initialize " + DatabaseProvider.class.getSimpleName(), e);
//			}
//		}
	}
	
	public static boolean isDatabaseProviderSpecified() {
		return databaseProvider != null;
	}
		
	@Override
	public String toString() {
		return JsonUtils.objectToStringPretty(this);
	}
	
	public TableSchema getTableSchema() {
		return new TableSchema(getColumnFamily())
				.addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true)
				.addColumn(DbObject.COLUMN_DATA, ColumnType.STRING);
	};
	
	public String getColumn() {
		return COLUMN_DATA;
	}
	
	public static DatabaseProvider getDatabaseProvider() {
		if (databaseProvider == null)
			throw new net.hypki.libs5.utils.utils.ValidationException(DbObject.class.getSimpleName() + 
					" has to be initialized with init() method");
//			init();
		return databaseProvider;
	}
	
	protected static Validator getValidator() {
		if (validator == null) {
			// creating validator
			long start = System.currentTimeMillis();
			validator = new Validator();
			LibsLogger.debug("Validator started in " + (System.currentTimeMillis() - start));
		}
		return validator;
	}
	
//	protected static void validate(DbObject ... dbobj) throws ValidationException {		
		// validate objects, collect the constraint violations
//		for (CassandraMutation cassandraMutation : mutations) {
//			if (cassandraMutation != null && cassandraMutation.getCassandraObject() != null && cassandraMutation.getMutation() != Mutation.REMOVE) {
//				DbObject cass = cassandraMutation.getCassandraObject();
//				
//				List<ConstraintViolation> violations = getValidator().validate(cass);
//				if (violations.size() > 0)
//					throw new ValidationException(violations);
//				
//				// additional validation
//				cass.additionalValidation();
////				cass.validate();
////				violations = 
////				if (violations.size() > 0)
////					throw new ValidationException(violations);
//			}
//		}
//	}

//	public static void saveMutations(CassandraMutation ... mutations) throws ValidationException {
//		saveMutations(HFactory.createMutator(getKeyspace(), strSerializer), mutations);
//	}
	
//	protected T save(Mutation ... mutations) throws ValidationException, IOException {
////		validate(mutations);
//		
////		Mutator<String> mutator = HFactory.createMutator(getCassKeyspace(), strSerializer);
//		
//		// saving transactions data
//		TxObject tx = new TxObject(mutations[0].getKeyspace(), mutations);
////		mutator.addInsertion(tx.getKey(), tx.getColumnFamily(), createStringColumn(tx.getColumn(), tx.getData()));
////		mutator.execute();
//		tx.save();
//	
//		getDatabaseProvider().save(mutations);//mutator, mutations);
//		
//		// committing
//		tx.setStatus(TxStatus.OK);
////		mutator.addInsertion(tx.getKey(), tx.getColumnFamily(), createStringColumn(tx.getColumn(), tx.getData()));
////		mutator.execute();
//		tx.save();
//		
//		return (T) this;
//	}
	
	public static void saveBulk(List<? extends DbObject> dbObjects) throws IOException, ValidationException {
		List<Mutation> saveMutations = new ArrayList<>();
		for (DbObject dbObject : dbObjects) {
			dbObject.validate();
			saveMutations.addAll(dbObject.getSaveMutations());
		}
		getDatabaseProvider().save(saveMutations);
	}
	
	public static void removeBulk(List<DbObject> dbObjects) throws IOException, ValidationException {
		List<Mutation> removeMutations = new ArrayList<>();
		for (DbObject dbObject : dbObjects) {
//			dbObject.validate();
			removeMutations.addAll(dbObject.getRemoveMutations());
		}
		getDatabaseProvider().save(removeMutations);
	}
	
	protected T save(List<Mutation> mutations) throws IOException, ValidationException {
		getDatabaseProvider().save(mutations);
		return (T) this;
	}
	
	public MutationList getSaveMutations() {
		return new MutationList()
				.addMutation(insertMutation(this));
	};
	
	public MutationList getRemoveMutations() {
		return new MutationList()
				.addMutation(removeMutation(this));
	}

	public void validate() throws ValidationException {
		this.additionalValidation();
		
		List<net.sf.oval.ConstraintViolation> errors = getValidator().validate(this);
		
		if (errors.size() > 0)
			throw new ValidationException(errors);
	}
		
	public T save() throws ValidationException, IOException {
		this.validate();
		
		getDatabaseProvider().save(getSaveMutations());
		return (T) this;
	}
	
	public void remove() throws ValidationException, IOException {
		getDatabaseProvider().save(getRemoveMutations());
	}
	
	public String getData() {
		return JsonUtils.objectToString(this);
	}
	
	public String getCombinedKey() {
		return buildCombinedKey(getKeyspace(), getColumnFamily(), getKey(), getColumn());
	}
	
	public static String buildCombinedKey(String keyspace, String columnFamily, String key, String column) {
		return String.format("%s.%s['%s'].%s", keyspace, columnFamily, key, column);
	}
	
	public static String getList(Class<? extends DbObject> clazz, String keyspace, String columnFamily, String column, Order order, int count, String state, List resultList) throws IOException {
		resultList.clear();
		Results results = getDatabaseProvider().getList(keyspace, columnFamily, new String[] {column}, order, count, state, null);
		for (Row row : results.getRows()) {
			DbObject obj = JsonUtils.fromJson((String) row.getColumns().get(column.toLowerCase()), clazz);
			if (obj != null)
				resultList.add(obj);
		}
		return results.getNextPage();
	}
	
//	public static <T> Iterable<T> iterateTable(final Class<T> clazz, final String keyspace, final String columnFamily, 
//			final String column) {
//		
//	}
	
	public static <T> Iterable<T> iterateTable(final Class<T> clazz, final String keyspace, final String columnFamily, final String column) {
		return new Iterable<T>() {
			@Override
			public Iterator<T> iterator() {
				return new Iterator<T>() {
					Results results = null;
					boolean initiated = false;
					int resultCounter = 0;
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							try {
								results = getDatabaseProvider().getList(keyspace, columnFamily, new String[] {column}, null, FETCH_SIZE_ITERATOR, null, null);
								initiated = true;
							} catch (IOException e) {
								LibsLogger.error(DbObject.class, "Cannot get list", e);
							}
						}
						if (resultCounter < results.getRows().size())
							return true;
						else if (results.getNextPage() == null)
							return false;
						else {
							// getting more results
							try {
								results = getDatabaseProvider().getList(keyspace, columnFamily, new String[] {column}, null, FETCH_SIZE_ITERATOR, results.getNextPage(), null);
							} catch (IOException e) {
								LibsLogger.error(DbObject.class, "Cannot get list", e);
							}
							resultCounter = 0;
							if (resultCounter < results.getRows().size())
								return true;
							else if (results.getNextPage() == null)
								return false;
							else
								return false;
						}
					}
					
					@Override
					public T next() {
						String tmp = (String) results.getRows().get(resultCounter++).getColumns().get(column);
						return JsonUtils.fromJson(tmp, clazz);
					}
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
				};
			}
		};
	}
	
	public static Mutation removeMutation(DbObject obj) {
		return new Mutation(MutationType.REMOVE, obj);
	}
	
//	public static Mutation removeMutation(CassandraId id) {
//		return new Mutation(MutationType.REMOVE, id);
//	}
	
//	public static CassandraMutation removeMutation(String keyspace, String columnFamily, String key, String column) {
//		return new CassandraMutation(Mutation.REMOVE, new CassandraId(keyspace, columnFamily, key, column));
//	}
	
	public static Mutation removeMutation(final String keyspace, final String columnFamily, final Object key, final String column) {
		return new Mutation(MutationType.REMOVE, keyspace, columnFamily, key, DbObject.COLUMN_PK, column, null);
	}
	
	public static Mutation insertMutation(DbObject obj) {
		return new Mutation(MutationType.INSERT, obj);
	}
	
	public void additionalValidation() throws ValidationException {
		
	}
}
