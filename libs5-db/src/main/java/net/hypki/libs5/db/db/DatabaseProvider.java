package net.hypki.libs5.db.db;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.string.MetaList;

public interface DatabaseProvider {
	
	public MetaList getInitParams();
	public void init(MetaList params);
	
	public void createKeyspace(String keyspace) throws IOException;
	public void clearKeyspace(String keyspace) throws IOException;
	public List<String> getTableList(final String keyspace) throws IOException;
	public TableSchema getTableSchema(final String keyspace, final String table) throws IOException;
	
	public void createTable(final String keyspace, final TableSchema tableSchema) throws IOException;
	public void clearTable(final String keyspace, final String table) throws IOException;
	public void removeTable(final String keyspace, final String table) throws IOException;
	public boolean isTableEmpty(final String keyspace, final String table) throws IOException;
	
	public boolean exist(String keyspace, String table, C3Where c3where, String column) throws IOException;
	public void save(List<Mutation> mutations) throws IOException, ValidationException;
	
	public long countColumns(String keyspace, String table, String key) throws IOException;
	public long countRows(String keyspace, String table, C3Where c3where) throws IOException;
	
	public <T> T get(String keyspace, String table, C3Where c3where, String column, Class<T> type) throws IOException;
	public Object get(String keyspace, String table, C3Where c3where, String column) throws IOException;
	
	public void remove(String keyspace, String columnFamily, String key, String keyName, String column) throws IOException;
	public void remove(final String keyspace, final String columnFamily, C3Where where) throws IOException;
	
	public Results getList(String keyspace, String table, String [] columns, 
			Order order, int count, String state, C3Where where) throws IOException;
	
	public Iterable<Row> tableIterable(String keyspace, String table);
	
	public void close() throws IOException;
}
