package net.hypki.libs5.db.db.weblibs;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.DbObject;

public class MutationList extends ArrayList<Mutation> {

	public MutationList() {
		
	}
	
	public MutationList addInsertMutations(DbObject m) {
		if (m != null)
			addAll(m.getSaveMutations());
		return this;
	}
	
	public MutationList addInsertMutations(boolean addIfTrue, DbObject m) {
		if (addIfTrue)
			if (m != null)
				addAll(m.getSaveMutations());
		return this;
	}
	
	public MutationList addMutation(Mutation m) {
		if (m != null)
			add(m);
		return this;
	}
	
	public MutationList addMutation(MutationList m) {
		if (m != null)
			add(m);
		return this;
	}
	
	public MutationList addRemoveMutations(DbObject m) {
		if (m != null)
			addAll(m.getRemoveMutations());
		return this;
	}

	public MutationList addRemoveMutations(Mutation m) {
		if (m != null)
			add(m);
		return this;
	}

	public MutationList add(MutationList mutations) {
		addAll(mutations);
		return this;
	}

}
