package net.hypki.libs5.db.db.weblibs.utils;

import java.io.Serializable;

import net.hypki.libs5.utils.guid.RandomGUID;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class UUID implements Serializable {
	private static final long serialVersionUID = 1856355291376901226L;
	
	public static final UUID ZERO 	= new UUID("00000000000000000000000000000000");
	
	public static final int LENGTH = ZERO.getId().length();
	
	public static final UUID START 	= new UUID("00000000000000000000000000000000"); // ASCII codes are: 0 < A < Z < a < z
	public static final UUID STOP 	= new UUID("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
	
	@NotNull
	@NotEmpty
	@Length(min = 32, max = 32)
	@Expose
	private String id = null;
	
	public UUID() {
		
	}
	
	public UUID(String id) {
		setId(id);
	}
	
	public UUID copy() {
		return new UUID(getId());
	}

	public void setId(String id) {
		if (id != null && id.length() != 32)
			id = id.replaceAll("[^\\w]", "");
		
		if (id != null && id.length() == 32)
			this.id = id;
		else
			this.id = null;
	}

	public String getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UUID) {
			return getId().equals(((UUID) obj).getId());
		} else if (obj instanceof String) {
			return ((String)obj).equals(getId());
		} else
			return false;
	}
	
	@Override
	public int hashCode() {
		return getId().hashCode();
	}

	public byte[] getBytes() {
		return getId() != null ? getId().getBytes() : null;
	}
	
	@Override
	public String toString() {
		return getId() != null ? getId().toString() : null;
	}
	
	public java.util.UUID toJavaUtilUUID() {
		return java.util.UUID.fromString(toString("-"));
	}
	
	public String toStringShort() {
		return String.format("%s..%s", id.substring(0, 4), id.substring(id.length() - 4));
	}
	
	public String toString(String separator) {
		int length = separator.length();
		return new StringBuffer(toString())
			.insert(8, separator)
			.insert(12 + 1 * length, separator)
			.insert(16 + 2 * length, separator)
			.insert(20 + 3 * length, separator)
			.toString();
	}
	
	public static UUID random() {
		return new UUID(RandomGUID.getRawRandomGUID());
	}
	
	public boolean isValid() {
		return id != null && id.length() == 32;
	}
	
	public static boolean isValid(String uuid) {
		return uuid != null && uuid.length() == 32;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			System.out.println(UUID.random());
			System.out.println(UUID.random().toString("-").toUpperCase());
		}
	}
}
