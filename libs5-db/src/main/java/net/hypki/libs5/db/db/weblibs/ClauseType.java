package net.hypki.libs5.db.db.weblibs;


public enum ClauseType {
	LT,
	LE,
	EQ,
	GE,
	GT,
	
	IN,
	CONTAINS,
	NOT_CONTAINS;

	public static ClauseType fromString(String comparator) {
		if (comparator == null)
			throw new IllegalArgumentException("Comparator not specified");
		else if (comparator.equals("<"))
			return LT;
		else if (comparator.equals("<="))
			return LE;
		else if (comparator.equals("=="))
			return EQ;
		else if (comparator.equals(">="))
			return GE;
		else if (comparator.equals(">"))
			return GT;
		else
			throw new IllegalArgumentException("Comparator " + comparator + " unknown");
	}
}
