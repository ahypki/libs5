package net.hypki.libs5.db.db.schema;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;

import net.hypki.libs5.utils.utils.NumberUtils;

public enum ColumnType implements Serializable {
	STRING, LONG, INTEGER, DOUBLE, FLOAT, DATE, UNKNOWN, BLOB,
	SET_STRING;

	public static ColumnType parse(String toParse) {
		if (toParse == null || toParse.length() == 0)
			return UNKNOWN;
		else if (toParse.equalsIgnoreCase("string")
				|| toParse.equalsIgnoreCase("chararray"))
			return STRING;
		else if (toParse.equalsIgnoreCase("integer")
				|| toParse.equalsIgnoreCase("int"))
			return INTEGER;
		else if (toParse.equalsIgnoreCase("long"))
			return LONG;
		else if (toParse.equalsIgnoreCase("double"))
			return DOUBLE;
		else if (toParse.equalsIgnoreCase("date"))
			return DATE;
		else if (toParse.equalsIgnoreCase("blob"))
			return BLOB;
		else if (toParse.equalsIgnoreCase("set<string>"))
			return SET_STRING;
		else
			return UNKNOWN;
	}
	
	public static ColumnType fromClass(Type type) {
		if (type == null)
			return UNKNOWN;
		else if (type.equals(String.class))
			return STRING;
		else if (type.equals(Integer.class))
			return INTEGER;
		else if (type.equals(Long.class))
			return LONG;
		else if (type.equals(Double.class))
			return DOUBLE;
		else if (type.equals(Date.class))
			return DATE;
//		else if (toParse.equalsIgnoreCase("blob"))
//			return BLOB;
//		else if (toParse.equalsIgnoreCase("set<string>"))
//			return SET_STRING;
		else
			return UNKNOWN;
	}
	
	public static ColumnType parse(Object o) {
		if (o == null)
			return UNKNOWN;
		else if (o instanceof String)
			return STRING;
		
		String s = o.toString();
		if (NumberUtils.isInt(s))
			return LONG;
		else if (NumberUtils.isDouble(s))
			return DOUBLE;
		else if (NumberUtils.isExpForm(s))
			return DOUBLE;
		
		return UNKNOWN;
		
		
	}
}
