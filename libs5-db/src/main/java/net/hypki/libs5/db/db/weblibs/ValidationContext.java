package net.hypki.libs5.db.db.weblibs;

import static java.lang.String.format;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.LocaleUtils;

public class ValidationContext {
	
	private Class clazz = null;
	private String name = null;
	private String msg = null;
		
	public ValidationContext() {
		
	}
	
	public ValidationContext(Class clazz, String name, String defaultMsg) {
		setClazz(clazz);
		setName(name);
		setMsg(defaultMsg);
	}
	
	@Override
	public String toString() {
//		return String.format("%s.%s#%s: %s", getClazz().getName(), getName(), getMsg());
		return String.format("%s.%s: %s", getClazz().getName(), getName(), getMsg());
	}
	
	public String toString(String languageId) {
		String tmp = LocaleUtils.getMsg(getClazz(), languageId, getName(), getMsg());
		
		if (tmp == null) {
			LibsLogger.error(ValidationContext.class, format("Cannot read '%s' for the class %s", getName(), getClazz().getName()));
			return format("%s.%s: %s", getClazz().getName(), getName(), getMsg());
		} else
			return tmp;
	}
	
	public Class getClazz() {
		return clazz;
	}

	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
