package net.hypki.libs5.db.db.weblibs;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.RegexUtils.ANY;
import static net.hypki.libs5.utils.string.RegexUtils.DOUBLE;
import static net.hypki.libs5.utils.string.RegexUtils.INTEGER;
import static net.hypki.libs5.utils.string.RegexUtils.SPACES;
import static net.hypki.libs5.utils.string.RegexUtils.allGroups;
import static net.hypki.libs5.utils.string.RegexUtils.firstGroup;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

// TODO remove this class, use Query instead to make everything consistent
@Deprecated
public class C3Where {

	private static final String CONJUCTION = "(AND|OR)";
	private static final String TERM = "([\\w\\d\\_\\-\\+\\.]+)";
	private static final String COMPARATOR = "(<=|<|==|>|>=)";
//	private static final String VALUE = "(" + DOUBLE + "|" + INTEGER + "|" + TERM + "|[\\d]+)";
	
	private List<C3Clause> clauses = null;

	public C3Where() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (clauses != null)
			for (C3Clause clause : clauses) {
				sb.append(clause);
				sb.append(" ");
			}
		return sb.toString();
	}

	public List<C3Clause> getClauses() {
		if (clauses == null)
			clauses = new ArrayList<>();
		return clauses;
	}

	public C3Where setClauses(List<C3Clause> clauses) {
		this.clauses = clauses;
		return this;
	}
	
	public C3Where addClause(C3Clause clause) {
		getClauses().add(clause);
		return this;
	}
	
	public int size() {
		return this.clauses != null ? this.clauses.size() : 0;
	}

	public C3Clause getClause(String name) {
		for (C3Clause c3Clause : clauses) {
			if (c3Clause.getField().equals(name))
				return c3Clause;
		}
		return null;
	}

	public static C3Where parse(String query) {
		C3Where where = null;

		String toParse = query;
		String term = null;
		
		String VALUE_REGEX = "([\\w\\d\\_\\-\\+\\.]+)";
		while (StringUtilities.notEmpty(toParse.trim())) {
			// term comparator value
			if (toParse.matches("^" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + VALUE_REGEX + SPACES + ANY)) {
				List<String> parts = allGroups(format("%s%s%s%s%s%s", SPACES, TERM, SPACES, COMPARATOR, SPACES, VALUE_REGEX), toParse);
				
				String name = parts.get(0);
				String comparator = parts.get(1);
				String valueStr = parts.get(2).toLowerCase();
				Object number = NumberUtils.autoParse(valueStr);
				
				if (where == null)
					where = new C3Where();
				
				where
					.addClause(new C3Clause(name, 
							ClauseType.fromString(comparator), 
							number));
				
				int cutFrom = toParse.indexOf(valueStr) + valueStr.length() + 1;
				toParse = cutFrom <= toParse.length() ? toParse.substring(cutFrom): "";//firstGroup("(" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + VALUE_REGEX + SPACES + ")", toParse).length());
				
				continue;
			}
			
			// term comparator string
//			if (toParse.matches("^" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + TERM + SPACES + ANY)) {
//				List<String> parts = allGroups(format("%s(%s)%s%s%s%s%s", SPACES, TERM, SPACES, COMPARATOR, SPACES, TERM, SPACES), toParse);
//				
//				String name = parts.get(0);
//				String comparator = parts.get(1);
//				String valueStr = parts.get(2).toLowerCase();
//				
//				if (where == null)
//					where = new C3Where();
//				
//				where
//					.addClause(new C3Clause(name, ClauseType.EQ, valueStr));
//				
//				toParse = toParse.substring(firstGroup("(" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + TERM + SPACES + ")", toParse).length());
//				
//				continue;
//			}
			
			break;
		}
		
		return where;
	}

	public boolean containsField(String name) {
		if (clauses != null)
			for (C3Clause c3Clause : clauses) {
				if (c3Clause.getField().equals(name))
					return true;
			}
		return false;
	}
}
