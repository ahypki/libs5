package net.hypki.libs5.db.db;

public class Order {

	private String colName = null;
	
	private boolean asc = true;
	
	public Order() {
		
	}
	
	public Order(String colName) {
		setColName(colName);
	}
	
	public Order(String colName, boolean isAsc) {
		setColName(colName);
		setAsc(isAsc);
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public boolean isAsc() {
		return asc;
	}

	public void setAsc(boolean asc) {
		this.asc = asc;
	}
}
