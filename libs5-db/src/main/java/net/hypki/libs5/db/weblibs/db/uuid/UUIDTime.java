package net.hypki.libs5.db.weblibs.db.uuid;

import java.util.concurrent.TimeUnit;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.Time;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.CheckWith;
import net.sf.oval.constraint.CheckWithCheck.SimpleCheck;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class UUIDTime implements SimpleCheck {

	@NotNull
	@CheckWith(value = UUIDTime.class, errorCode = "Common.DateNotValid")
	@Expose
	private Time time = null;
	
	@NotNull
	@AssertValid
	@Expose
	private UUID uuid = null;
	
	public UUIDTime() {
		setTime(new Time(System.currentTimeMillis()));
		setUuid(UUID.random());
	}
	
	public UUIDTime(Time time) {
		setTime(time);
		setUuid(UUID.random());
	}
	
	public UUIDTime(Time time, UUID uuid) {
		setTime(time);
		setUuid(uuid);
	}
	
	public UUIDTime(long ms, UUID uuid) {
		setTime(new Time(ms));
		setUuid(uuid);
	}
	
	public UUIDTime(long ms) {
		setTime(new Time(ms));
		setUuid(UUID.random());
	}
	
	@Override
	public boolean isSatisfied(Object validatedObject, Object value) {
		if (value instanceof Time) {
			Time time = (Time) value;
			return !time.isEmpty();
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UUIDTime) {
			UUIDTime uuidTime = (UUIDTime) obj;
			return this.getUuid().equals(uuidTime.getUuid()) && this.getTime().getMs() == uuidTime.getTime().getMs();
		} else if (obj instanceof Long) {
			Long ms = (Long) obj;
			return this.getTime().getMs() == ms;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("%s_%s", getTime().toString(), getUuid());
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Time getTime() {
		return time;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public UUID getUuid() {
		return uuid;
	}

	public boolean isOlderThan(int i, TimeUnit timeUnit) {
		return getTime().isOlderThan(i, timeUnit);
	}
}
