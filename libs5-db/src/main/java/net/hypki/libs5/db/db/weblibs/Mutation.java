package net.hypki.libs5.db.db.weblibs;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Mutation {
//	private CassandraObject cassandraObject = null;
	
//	@Expose
//	private String clazz = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String keyspace = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String table = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private Object key = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String keyName = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String column = null;
	
	@Expose
	private Object data = null;
	
	@NotNull
	@Expose
	private MutationType mutation = null;
	
	@Expose
	private C3Where where = null;
	
	public Mutation() {
		
	}
	
	public Mutation(MutationType mutation, DbObject obj) {
		setKeyspace(obj.getKeyspace());
		setTable(obj.getColumnFamily());
		setKey(obj.getKey());
		setKeyName(DbObject.COLUMN_PK);
		setColumn(obj.getColumn());
		setObjData(obj.getData());
		setMutation(mutation);
	}
	
	public Mutation(MutationType mutation, String keyspace, String columnFamily, Object key, String keyName, String column, Object data) {
		setKeyspace(keyspace);
		setTable(columnFamily);
		setKey(key);
		setKeyName(keyName);
		setColumn(column);
		setObjData(data);
		setMutation(mutation);
	}
	
	public Mutation(MutationType mutation, String keyspace, String columnFamily, C3Where where, String column, Object data) {
		setKeyspace(keyspace);
		setTable(columnFamily);
		setWhere(where);
		setColumn(column);
		setObjData(data);
		setMutation(mutation);
	}
	
	public static Mutation insertMutation(DbObject obj) {
		return new Mutation(MutationType.INSERT, obj);
	}

//	public void setCassandraObject(CassandraObject cassandraObject) {
//		this.cassandraObject = cassandraObject;
//		this.clazz = cassandraObject.getClass().getName();
//		this.data = cassandraObject.getData();
//	}
	
	@Override
	public String toString() {
		if (getWhere() != null)
			return String.format("%s %s[%s] where %s", getMutation(), getTable(), getColumn(), getWhere());
		else
			return String.format("%s %s.%s[%s]", getMutation(), getTable(), getKey(), getColumn());
	}
	
	public void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}

	public String getKeyspace() {
		return keyspace;
	}

	public void setKey(Object key) {
		this.key = key;
	}

	public Object getKey() {
		return key;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getTable() {
		return table;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getColumn() {
		return column;
	}

	public DbObject getCassandraObject(Class<? extends DbObject> clazz) {
		if (getObjData() != null) {
//			try {
//				Class<? extends DbObject> clazz = (Class<? extends DbObject>) Class.forName(getObjClass());
				return JsonUtils.fromJson((String) getObjData(), clazz);
//			} catch (ClassNotFoundException e) {
//				LibsLogger.error("Class was not found", e);
//				return null;
//			}
		} else 
			return null;
//		return cassandraObject;
	}

	public void setMutation(MutationType mutation) {
		this.mutation = mutation;
	}

	public MutationType getMutation() {
		return mutation;
	}


	public void setObjData(Object objData) {
		this.data = objData;
	}
	
	public Object getObjData() {
		return this.data;
	}

	public C3Where getWhere() {
		if (where == null)
			where = new C3Where();
		return where;
	}

	public Mutation setWhere(C3Where where) {
		this.where = where;
		return this;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
}
