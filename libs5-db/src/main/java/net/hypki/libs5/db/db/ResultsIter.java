package net.hypki.libs5.db.db;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.lang.NotImplementedException;

import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.utils.LibsLogger;

public class ResultsIter implements Iterable<Row>, Iterator<Row> {
	
	private static int PER_PAGE = 10000;

	private String keyspace = null;
	
	private String table = null;
	
	private String[] columns = null;
	
	private C3Where c3Where = null;
	
	private Results res = null;
	
	private int resCounter = 0;
	
	private int resTotalCounter = 0;
	
	private DatabaseProvider databaseProvider = null;
	
	private Order order = null;
	
	private int perPage = PER_PAGE;
	
	public ResultsIter(final String keyspace, final String table, final String[] columns, final C3Where c3Where) {
		setKeyspace(keyspace);
		setTable(table);
		setColumns(columns);
		setC3Where(c3Where);
	}
	
	public ResultsIter(DatabaseProvider databaseProvider, final String keyspace, final String table, final String[] columns, final C3Where c3Where) {
		setDatabaseProvider(databaseProvider);
		setKeyspace(keyspace);
		setTable(table);
		setColumns(columns);
		setC3Where(c3Where);
	}
	
	private void reset() {
		setRes(null);
		this.resCounter = 0;
		setResTotalCounter(0);
	}

	@Override
	public boolean hasNext() {
		if (getRes() != null && resCounter < getRes().size())
			return true;
		
		try {
			nextPage();
		} catch (IOException e) {
			LibsLogger.error(ResultsIter.class, "Cannot load next page from DB", e);
			setRes(null);
			return false;
		}
		
		return getRes() != null && resCounter < getRes().size();
	}

	private void nextPage() throws IOException {
		if (getRes() != null && getRes().getNextPage() == null)
			return; // no more results

		this.resCounter = 0;
		
		String state = getRes() != null ? getRes().getNextPage() : null;
		setRes(getDatabaseProvider()
				.getList(getKeyspace(), 
						getTable(), 
						getColumns(), 
						getOrder(), 
						getPerPage(), 
						state, 
						getC3Where()));
	}

	@Override
	public Row next() {
		this.resTotalCounter++;
		return getRes().getRows().get(resCounter++);
	}

	@Override
	public void remove() {
		throw new NotImplementedException();
	}

	@Override
	public Iterator<Row> iterator() {
		reset();
		return this;
	}

	private String getKeyspace() {
		return keyspace;
	}

	private void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}

	private String getTable() {
		return table;
	}

	private void setTable(String table) {
		this.table = table;
	}

	private String[] getColumns() {
		return columns;
	}

	private void setColumns(String[] columns) {
		this.columns = columns;
	}

	private C3Where getC3Where() {
		return c3Where;
	}

	private void setC3Where(C3Where c3Where) {
		this.c3Where = c3Where;
	}

	private Results getRes() {
		return res;
	}

	private void setRes(Results res) {
		this.res = res;
	}

	public int getResTotalCounter() {
		return resTotalCounter;
	}

	private void setResTotalCounter(int resTotalCounter) {
		this.resTotalCounter = resTotalCounter;
	}

	private DatabaseProvider getDatabaseProvider() {
		if (databaseProvider == null)
			databaseProvider = DbObject.getDatabaseProvider();
		return databaseProvider;
	}

	private void setDatabaseProvider(DatabaseProvider databaseProvider) {
		this.databaseProvider = databaseProvider;
	}

	public Order getOrder() {
		return order;
	}

	public ResultsIter setOrder(Order order) {
		this.order = order;
		return this;
	}

	public int getPerPage() {
		return perPage;
	}

	public ResultsIter setPerPage(int perPage) {
		this.perPage = perPage;
		return this;
	}
}
