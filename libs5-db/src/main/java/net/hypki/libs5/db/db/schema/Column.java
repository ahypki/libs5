package net.hypki.libs5.db.db.schema;

import java.io.Serializable;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import jodd.util.ComparableComparator;
import net.hypki.libs5.utils.utils.NumberUtils;

public class Column implements Comparable, Serializable {

	@Expose
	private String name = null;
	
	@Expose
	private ColumnType type = ColumnType.STRING;
	
	@Expose
	private boolean isPrimaryKey = false;
	
	@Expose
	private boolean isIndexed = false;
	
	public Column() {
		
	}
	
	public Column(String name) {
		setName(name);
	}
	
	public Column(String name, ColumnType type) {
		setName(name);
		setType(type);
	}
	
	public Column(String name, ColumnType type, boolean isPrimaryKey) {
		setName(name);
		setType(type);
		setPrimaryKey(isPrimaryKey);
	}
	
	public Column(String name, ColumnType type, boolean isPrimaryKey, boolean isIndexed) {
		setName(name);
		setType(type);
		setPrimaryKey(isPrimaryKey);
		setIndexed(isIndexed);
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s", getName(), getType().toString(), isPrimaryKey() ? "PRIMARY KEY" : "");
	}
	
	@Override
	public int compareTo(Object o) {
		return getName().compareTo(((Column) o).getName());
	}

	public String getName() {
		return name;
	}

	public Column setName(String name) {
		this.name = name;
		return this;
	}

	public ColumnType getType() {
		return type;
	}

	public Column setType(ColumnType type) {
		this.type = type;
		return this;
	}

	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public Column setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
		return this;
	}

	public boolean isIndexed() {
		return isIndexed;
	}

	public void setIndexed(boolean isIndexed) {
		this.isIndexed = isIndexed;
	}

	public static Object convert(String v, ColumnType type) {
		if (type == ColumnType.DOUBLE)
			return NumberUtils.toDouble(v);
		else if (type == ColumnType.FLOAT)
			return NumberUtils.toFloat(v);
		else if (type == ColumnType.INTEGER)
			return NumberUtils.toInt(v);
		else if (type == ColumnType.LONG)
			return NumberUtils.toLong(v);
		else
			throw new NotImplementedException("Unimplemented exception for value " + v + " and type " + type);
	}
}
