package net.hypki.libs5.db.mapdb;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.hypki.libs5.db.db.BackupUtils;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.Column;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

public class MapDbManager implements DatabaseProvider {
	
	public final static String MAPDB_VMARG_NAME = "mapdb";
	
	private final static String HASHMAP_INDEX = "__INDEXXX"; // TODO name to __INDEX
	
	private final static String TABLE_SCHEMAS = "___SCHEMAS";
	private final static String TABLE_SCHEMAS_KEY_NAME = "key";
	private final static String TABLE_SCHEMAS_COLUMN_DATA = "data";
	
	private final static String TABLE_SETTINGS = "___SETTINGS";
	private final static String SETTING_REINDEXED = "SETTING_REINDEXED";
	private final static String SETTING_VERSION = "SETTING_VERSION";
	
	private final static HashMap<String, TableSchema> schemasCache = new HashMap<>();
	
	private String path = null;
	
	private DB db = null;
	
	private final static boolean DUMP_DATABASE_ON_STARTUP = false;
	
	public MapDbManager() {
		
	}
	
	public MapDbManager(final String path) {
		setPath(path);
	}
	
	private DB getDb() {
		if (db == null) {
			synchronized (this) {
				if (db != null)
					return db;
				
				LibsLogger.debug(MapDbManager.class, "Opening MapDb database in ", new FileExt(getPath()).getAbsolutePath());
				
				// opening MapDb database
				File dbFile = new File(getPath());
				if (dbFile.getParentFile() != null)
					dbFile.getParentFile().mkdirs();
				setDb(DBMaker
						.fileDB(dbFile)
						.checksumHeaderBypass()
//							.concurrencyDisable()
//							.fileMmapEnableIfSupported()
//						.cleanerHackEnable()
						.make());
				
				if (getSettingInt(SETTING_VERSION, 0) == 0) {
					try {
						upgradeDbTo1();
					} catch (IOException | ValidationException e) {
						LibsLogger.error(MapDbManager.class, "Cannot reindex database", e);
					}
				}
				
				if (getSettingInt(SETTING_REINDEXED, 0) <= 12) {
					try {
						rebuildIndex(13);
					} catch (IOException | ValidationException e) {
						LibsLogger.error(MapDbManager.class, "Cannot reindex database", e);
					}
				}
				
				if (DUMP_DATABASE_ON_STARTUP) {
					try {
						dump(new File(SimpleDate.now().toStringISO() + ".dump"));
					} catch (IOException e) {
						LibsLogger.error(MapDbManager.class, "Cannot create dump for MapDb", e);
					}
				}

				// TODO removing empty keys move to separate Thread, BUT it should not be a problem anymore to have
				// empy keys...
//					if (columns != null && 
//							(columns.size() == 0 
//							|| (columns.size() == 1 && columns.containsKey(DbObject.COLUMN_PK)))) {
//						getDb().hashMap(keyspace + "___" + table).open().remove(key);
////						getDb().commit();
//						LibsLogger.debug(MapDbManager.class, "Removed empty key ", key, " from MapDb");
//					}
			}
		}
		return db;
	}
	
	@Override
	public MetaList getInitParams() {
		MetaList l = new MetaList();
		l.add("path", getPath());
		return l;
	}
	
	@Override
	public void init(MetaList params) {
		if (params != null) {
			String path = params.get("path").getAsString();
			
			if (notEmpty(path)) {
				close();
				
				setPath(path);
			}
		}
	}
	
	private void dump(File output) throws IOException {
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(output);
			
			fw.append("{\n");
			
			boolean addSemi = false;
			for (String name : getDb().getAllNames()) {
//				fw.append("[\n"); // TODO add table name
				Object o = getDb().get(name);
				Map<String, Object> map = null;
				if (o instanceof Map) {
					map = (Map<String, Object>) o;
					LibsLogger.debug(BackupUtils.class, "Creating dump for name ", name);
					
					try {
						for (Map.Entry<String, Object> e : map.entrySet()) {
							if (addSemi)
								fw.append(",\n");
							
							JsonObject jo = new JsonObject();
							jo.addProperty("name", name);
							jo.addProperty("key", e.getKey());
							jo.addProperty("value", String.valueOf(e.getValue()));
							
							fw.append(jo.toString());
							
							addSemi = true;
						}
					} catch (Throwable t) {
						LibsLogger.error(MapDbManager.class, "Cannot dump name " + name + ", skipping..", t);
					}
				}
			}
			
			fw.append("}\n");
			
			LibsLogger.debug(BackupUtils.class, "Dump of MapDb database done to the file ",
					output.getAbsolutePath());
		} catch (Throwable t) {
			throw new IOException("Cannot dump MapDb database into file ", t);
		} finally {
			if (fw != null)
				fw.close();
		}
	}

	private void upgradeDbTo1() throws IOException, ValidationException {
		int readRows = 0;
		for (String keyspace : iterateAllKeyspaces()) {
			for (String table : getTableList(keyspace)) {
				for (Row row : tableIterable(keyspace, table)) {
					if (!row.contains("data"))
						LibsLogger.debug(MapDbManager.class, "row ", row);
					
					for (Map.Entry<String, Object> col : row.getColumns().entrySet()) {
						saveOneColumn(keyspace, 
								table, 
								null, // keyname 
								(String) row.getPk(), 
								col.getKey(), 
								col.getValue(), 
								false);
					}
					
					readRows++;
				}
			}
		}
		
		getDb().commit();
		
		setSetting(SETTING_VERSION, 1);
		
		LibsLogger.debug(MapDbManager.class, "Database upgraded to version 1.0, upgraded ", readRows, " rows");
	}

	private Iterable<String> iterateAllKeyspaces() {
		List<String> keyspaces = new ArrayList<String>();
		for (String name : getDb().getAllNames()) {
			if (name.indexOf("__") >= 0) {
				String keyspace = name.substring(0, name.indexOf("__"));
				if (StringUtilities.notEmpty(keyspace) && !keyspaces.contains(keyspace))
					keyspaces.add(keyspace);
			}
		}
		return keyspaces;
	}
	
	private Iterable<TableSchema> iterateTableSchema(final String keyspace) {
		return new Iterable<TableSchema>() {
			@Override
			public Iterator<TableSchema> iterator() {
				return new Iterator<TableSchema>() {
					private Iterator<String> tableNames = getTableList(keyspace).iterator();
					
					@Override
					public TableSchema next() {
						return getTableSchema(keyspace, tableNames.next());
					}
					
					@Override
					public boolean hasNext() {
						return tableNames.hasNext();
					}
				};
			}
		};
	}

	@Override
	public <T> T get(String keyspace, String table, C3Where c3where, String column, Class<T> type) throws IOException {
		String v = get(keyspace, table, c3where, column);
		return v != null ? JsonUtils.fromJson(v, type) : null;
		
//		String key = buildKey(keyspace, table, c3where);
//		
//		
//		@SuppressWarnings("unchecked")
//		String value = (String) getDb().hashMap(keyspace + "___" + table + "__" + column).createOrOpen().get(key);
//		
//		return value != null ? JsonUtils.fromJson(value, type) : null;
	}
	
//	public Map<String, Object> get(String keyspace, String table, String key) throws IOException {
//		return (Map<String, Object>) getDb().hashMap(keyspace + "___" + table).createOrOpen().get(key);
//	}
	
	@Override
	public String get(String keyspace, String table, C3Where c3where, String column) throws IOException {
		String key = buildKey(keyspace, table, c3where);
		
		@SuppressWarnings("unchecked")
		String v = (String) getDb().hashMap(keyspace + "___" + table).createOrOpen().get(key + "__" + column);
		
		return v != null ? v : null;
	}
	
	private String buildKey(String keyspace, String table, C3Where where) {
		if (where.size() == 1)
			return (String) where.getClauses().get(0).getValue();
		
		String key = "";
		TableSchema ts = getTableSchema(keyspace, table);
		for (Column column : ts.getColumns()) {
			if (column.isPrimaryKey()) {
				C3Clause clause = where.getClause(column.getName());
				if (clause != null)
					key += clause.getValue();
				else
					return key;
			}
		}
		return key;
	}

	public String getPath() {
		if (path == null) {
			// reading settings for MapDb
			JsonElement mapdbSettings = null;
			
			final String mapdbVmArg = System.getProperty(MAPDB_VMARG_NAME);
			LibsLogger.debug(MapDbManager.class, "VM Arg ", MAPDB_VMARG_NAME , "=", mapdbVmArg);
			
			if (mapdbVmArg != null) {
				mapdbSettings = JsonUtils.parseJson(FileUtils.readString(Settings.class.getResourceAsStream("/" + mapdbVmArg)));
			}

			if (mapdbSettings == null) {
				// trying to use default mapdb.settings file
				LibsLogger.debug(MapDbManager.class, "Trying to use default settings for mapdb: mapdb.settings");
				mapdbSettings = JsonUtils.parseJson(FileUtils.readString(Settings.class.getResourceAsStream("/mapdb.settings")));				
			}
			
			if (mapdbSettings == null)
				throw new RuntimeException("Cannot open settings for MapDb");
			
			path = JsonUtils.readString(mapdbSettings, "MapDb/path");
			LibsLogger.debug(MapDbManager.class, "MapDb path set to ", new FileExt(path).getAbsolutePath());
			
			if (path.contains("$HOME"))
				path = path.replace("$HOME", SystemUtils.getHomePath());
			
			path = new FileExt(path).getAbsolutePath();
		}
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	private void rebuildIndex(int newIndexVer) throws IOException, ValidationException {
		LibsLogger.debug(MapDbManager.class, "Rebuilding the whole MapDb index...");
		
		for (String keyspace : iterateAllKeyspaces()) {
			for (TableSchema tableSchema : iterateTableSchema(keyspace)) {
				
				clearIndexes(keyspace, tableSchema.getName());
				
				getDb().commit();
				
				boolean columnAlreadyIndexed = false;
				for (Column column : tableSchema.getColumns()) {
					if (column.isIndexed() && !column.isPrimaryKey()) {
						if (columnAlreadyIndexed)
							throw new IOException("There are more than 1 indexed columns for schema " + 
									tableSchema.getName() + " - it is not implemented yet");
						rebuildIndex(keyspace, tableSchema, column);
						columnAlreadyIndexed = true;
					}
				}
			}
		}
		setSetting(SETTING_REINDEXED, newIndexVer);
	}
	
	private void rebuildIndex(String keyspace, TableSchema schema, Column column) throws IOException, ValidationException {
		LibsLogger.debug(MapDbManager.class, "Rebuilding MapDb index for keyspace ", keyspace, " schema ", schema.getName(), " for column ", column);
		
		int counter = 0;
		for (Row row : tableIterable(keyspace, schema.getName())) {
			String val = row.getAsString(column.getName());
			if (val != null)
				saveOneColumn(keyspace,				// keyspace 
					schema.getName() + "__" + val, 	// table
					"pk", 							// keyname, not used actually
					row.getPk().toString(), 		// key
					"data",							// column 
					JsonUtils.objectToString(row), 	// data
					false);							// commit
			
			if ((++counter % 1000) == 0) {
				LibsLogger.debug(MapDbManager.class, "Rebuild index for ", counter, " objects");
				getDb().commit();
			}
		}
	}

	private void setDb(DB db) {
		this.db = db;
	}

	@Override
	public void clearKeyspace(String keyspace) {
		for (String table : getTableList(keyspace)) {
			clearTable(keyspace, table);
		}
	}

	@Override
	public List<String> getTableList(final String keyspace) {
		List<String> tables = new ArrayList<String>();
		for (Object row : ((Map<String, Object>) getDb().hashMap(keyspace + "___" + TABLE_SCHEMAS).createOrOpen()).values()) {
//			LibsLogger.debug(MapDbManager.class, "row " + row);
//			String tableSchemaStr = row.getAsString(TABLE_SCHEMAS_COLUMN_DATA);
//			
//			assertTrue(notEmpty(tableSchemaStr), "Cannot find table schema in row ", row);
//			
			if (row instanceof String)
				tables.add(JsonUtils.fromJson((String) row, TableSchema.class).getName());
		}
		return tables;
	}
	
	private void clearIndex(final String keyspace, final String table, final String key, final String value) {
		if (getDb().exists(keyspace + "___" + table + "__" + value)) {
			getDb().hashMap(keyspace + "___" + table + "__" + value).open().remove(key);
		}
	}
	
	private void clearIndex(final String keyspace, final String table, final String key) throws IOException {
		Row row = getRow(keyspace, table, key);
		for (Column cSchema : getTableSchema(keyspace, table).getColumns()) {
			if (cSchema.isIndexed() && !cSchema.isPrimaryKey())
				clearIndex(keyspace, table, key, String.valueOf(row.get(cSchema.getName())));
		}
	}
	
	private void clearIndexes(final String keyspace, final String table) {
		LibsLogger.debug(MapDbManager.class, "Clearing indexes for keyspace ", keyspace, " table ", table);
		
		for (String name : getDb().getAllNames()) {
			if (name.startsWith(keyspace + "___" + table + "__")) {
				try {
					HTreeMap<Object, Object> tmp = (HTreeMap<Object, Object>) getDb().hashMap(name).open();
					if (tmp != null)
						tmp.clear();
				} catch (Throwable e) {
					LibsLogger.error(MapDbManager.class, "Cannot clear " + keyspace + ", table " + name, e);
				}
			}
		}
	}

	@Override
	public void clearTable(final String keyspace, final String table) {
		getDb().hashMap(keyspace + "___" + table).createOrOpen().clear();
		
		// clearing indexes
		clearIndexes(keyspace, table);
		
		getDb().commit();
	}

	@Override
	public boolean isTableEmpty(final String keyspace, final String table) {
		HTreeMap map = getDb().hashMap(keyspace + "___" + table).createOrOpen();
		return map != null ? map.size() == 0 : true;
	}

	@Override
	public boolean exist(String keyspace, String table, C3Where c3where, String column) throws IOException {
		String key = buildKey(keyspace, table, c3where);
		
		@SuppressWarnings("unchecked")
		String v = (String) getDb().hashMap(keyspace + "___" + table).createOrOpen().get(key + "__" + column);
		
		return v != null;
	}

	@Override
	public long countColumns(String keyspace, String table, String key) {
		return getTableSchema(keyspace, table).getColumns().size();
//		return getRow(keyspace, table, key).size();
//		@SuppressWarnings("unchecked")
//		HTreeMap<String, Object> columns = (HTreeMap<String, Object>) getDb().hashMap(keyspace + "___" + table).createOrOpen().get(key);
//		return columns != null ? columns.size() : 0L;
	}
	
	@Override
	public Results getList(String keyspace, String table, String[] columns, Order order, int count, String state, C3Where where) {
		final int from = state != null ? NumberUtils.toInt(state, 0) : 0;
		final int to = from + count;
		
		Watch w = new Watch();
		Results res = new Results();
		
		int counter = 0;
				
		boolean useIndices = false;
		if (where != null) {
			// using indices
			for (C3Clause c3Clause : where.getClauses()) {
				Column cSchema = getTableSchema(keyspace, table).getColumn(c3Clause.getField());
				if (cSchema != null && cSchema.isIndexed() && !cSchema.isPrimaryKey()) {
					useIndices = true;
					break;
				}
			}
		}
		
		if (useIndices) {
			Collection allValues = null;
			for (C3Clause c3Clause : where.getClauses()) {
				Column cSchema = getTableSchema(keyspace, table).getColumn(c3Clause.getField());
				if (cSchema.isIndexed() && !cSchema.isPrimaryKey()) {
					Collection oneColumnValues = getDb()
							.hashMap(keyspace + "___" + table + "__" + c3Clause.getValue())
							.createOrOpen()
							.values();
					
					if (allValues == null)
						allValues = oneColumnValues;
					else
						throw new NotImplementedException();
				}
			}
			
			int i = 0;
//			HTreeMap m = getDb().hashMap(keyspace + "___" + table).createOrOpen();
			if (allValues != null)
				for (Object row : allValues) {
					if (i >= from && i < to) {
	//					Map<String, Object> columns2 = (Map<String, Object>) m.get(row);
	//					res.addRow(new Row(row).addColumns(columns2));
						res.addRow(JsonUtils.fromJson((String) row, Row.class));
					}
					i++;
				}
			
//			Object [] allPksArr = allPks.toArray();
//			for (int i = from; i < to && i < allPksArr.length; i++) {
//				Map<String, Object> columns2 = (Map<String, Object>) getDb().hashMap(keyspace + "___" + table).createOrOpen().get(allPksArr[i]);
//				res.addRow(new Row(allPksArr[i]).addColumns(columns2));
//			}
			
			if (res.size() > 0)
				res.setNextPage(String.valueOf(to));
		} else {
			for (Row row : tableIterable(keyspace, table)) {
				if (row.isFullfiled(where, false)) {
					if (counter >= from && counter < to) {
						res.addRow(row);
					}
					counter++;
				}
				
				if (counter == to) {
					res.setNextPage(String.valueOf(to));
					break;
				}
			}
		}
		
		if (w.ms() > 200)
			LibsLogger.warn(MapDbManager.class, "getList() done in ", w.toString());
		return res;
	}

	@Override
	public void remove(String keyspace, String table, String key, String keyName, String column) throws IOException {
		removeOneColumn(keyspace, table, key, keyName, column, true);
	}
	
	private void removeOneColumn(String keyspace, String table, String key, String keyName, String column, boolean commit) {
		@SuppressWarnings("unchecked")
		String v = (String) getDb().hashMap(keyspace + "___" + table).createOrOpen().get(key + "__" + column);
		
		if (v != null) {
			for (Column cSchema : getTableSchema(keyspace, table).getColumns()) {
				if (cSchema.isIndexed() && !cSchema.isPrimaryKey()) {
					Row row = getRow(keyspace, table, key);
					clearIndex(keyspace, table, key, row.getAsString(column));// String.valueOf(v));
				}
				
				getDb().hashMap(keyspace + "___" + table).createOrOpen().remove(key + "__" + cSchema.getName());
			}
		}
	}

	@Override
	public void createKeyspace(String keyspace) {
		assertTrue(!keyspace.startsWith("___"), "Keyspace name cannot start with '___'");
	}

	@Override
	public void removeTable(String keyspace, String table) {
		clearIndexes(keyspace, table);
		getDb().hashMap(keyspace + "___" + table).createOrOpen().clear();
		
		try {
			remove(keyspace, 
					TABLE_SCHEMAS, 
					new C3Where()
						.addClause(new C3Clause(TABLE_SCHEMAS_KEY_NAME, ClauseType.EQ, table)));
		} catch (IOException e) {
			LibsLogger.error(MapDbManager.class, "Cannot remove Table schema", e);
		}
		
		getDb().commit();
	}
	
	private void saveOneColumn(String keyspace, String table, String keyname, String key, String column, Object data) throws IOException, ValidationException {
		saveOneColumn(keyspace, table, keyname, key, column, data, true);
	}
	
	private Row getRow(String keyspace, String table, String key) {
		// TODO DEBUG
		try {
//			for (Object o : getDb().hashMap(keyspace + "___" + table).createOrOpen().values())
//				LibsLogger.debug(MapDbManager.class, "o: " + o);
			
			Row row = new Row(key);
			for (Column col : getTableSchema(keyspace, table).getColumns())
				row.addColumn(col.getName(), getDb().hashMap(keyspace + "___" + table).createOrOpen().get(key + "__" + col.getName()));
			return row;
		} catch (Throwable e) {
			LibsLogger.error(MapDbManager.class, "Cannot get row", e);
			return null;
		}
	}

	private void saveOneColumn(String keyspace, String table, String keyname, String key, String column, 
			Object data, boolean commit) throws IOException, ValidationException {
//		try {
			((Map) getDb().hashMap(keyspace + "___" + table).createOrOpen()).put(key + "__" + column, data);
			
			// indexing
			TableSchema ts = getTableSchema(keyspace, table);
			if (ts != null) {
				Column colSchema = ts.getColumn(column);
				if (colSchema != null) // TODO DEBUG
					if (colSchema.isIndexed() && !colSchema.isPrimaryKey() && column.equals(colSchema.getName())) {
//						Map cc = (Map) getDb().hashMap(keyspace + "___" + table + "__" + String.valueOf(data)).createOrOpen();
//						cc.put(key, key);
						
						Row row = getRow(keyspace, table, key);// new Row(key).addColumns(columns);
						String val = String.valueOf(row.get(column));
						saveOneColumn(keyspace,					// keyspace 
								table + "__" + val, 			// table
								"pk", 							// keyname, not used actually
								row.getPk().toString(),			// key
								"data",							// column 
								JsonUtils.objectToString(row), 	// data
								false);							// commit
					}
			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
//			if (commit)
//		getDb().commit();
	}

	@Override
	public void createTable(String keyspace, TableSchema tableSchema) {
		assertTrue(!keyspace.startsWith("___"), "Keyspace name cannot start with '___'");
		assertTrue(!tableSchema.getName().startsWith("___"), "Table name cannot start with '___'");
		
		try {
			saveOneColumn(keyspace, 
					TABLE_SCHEMAS, 
					TABLE_SCHEMAS_KEY_NAME, 
					tableSchema.getName(), 
					TABLE_SCHEMAS_COLUMN_DATA, 
					JsonUtils.objectToString(tableSchema));
			
			schemasCache.put(keyspace + tableSchema.getName(), tableSchema);
		} catch (IOException | ValidationException e) {
			throw new RuntimeException("Cannot save schema for table " + e.getMessage(), e);
		}
	}
	
	private int getSettingInt(String key, int defaultValue) {
		return (Integer) getSetting(key, defaultValue);
	}
	
	private Object getSetting(String key, Object defaultValue) {
		Object o = getSetting(key);
		return o == null ? defaultValue : o;
	}
	
	private Object getSetting(String key) {
		Map<String, Object> settings = (Map<String, Object>) getDb().hashMap(TABLE_SETTINGS).createOrOpen();
		Object o = settings.get(key);
		return o == null ? null : o;
	}
	
	private void setSetting(String key, Object value) {
		Map<String, Object> settings = (Map<String, Object>) getDb().hashMap(TABLE_SETTINGS).createOrOpen();
		settings.put(key, value);
	}
	
	@Override
	public TableSchema getTableSchema(String keyspace, String table) {
		if (table.indexOf("__") > 0)
			return null; // skip schemas for indices
		
		TableSchema ts = schemasCache.get(keyspace + table);
		
		if (ts != null)
			return ts;
		
		try {
			if (table.equals(TABLE_SCHEMAS)) {
				ts = new TableSchema(TABLE_SCHEMAS);
				ts.addColumn(TABLE_SCHEMAS_COLUMN_DATA, ColumnType.STRING);
			} else {
				ts = get(keyspace, 
						TABLE_SCHEMAS, 
						new C3Where()
							.addClause(new C3Clause(TABLE_SCHEMAS_KEY_NAME, ClauseType.EQ, table)), 
						TABLE_SCHEMAS_COLUMN_DATA, 
						TableSchema.class);
			}
			
			if (ts != null)
				schemasCache.put(keyspace + table, ts);
		} catch (IOException e) {
			LibsLogger.error(MapDbManager.class, "Cannot get TableSchema for table " + table, e);
		}
		
		if (ts == null)
			LibsLogger.warn(MapDbManager.class, "No schema for table ", keyspace, 
					":", table);
		
		return ts;
	}

	@Override
	public void save(List<Mutation> mutations) throws IOException, ValidationException {
		for (Mutation m : mutations) {
			if (m.getMutation() == MutationType.INSERT) {
				
				String key = m.getKey() != null ? m.getKey().toString() : buildKey(m.getKeyspace(), m.getTable(), m.getWhere());
				
				saveOneColumn(m.getKeyspace(), m.getTable(), m.getKeyName(), key, m.getKeyName(), key, false);
				saveOneColumn(m.getKeyspace(), m.getTable(), m.getKeyName(), key, m.getColumn(), m.getObjData(), false);
				
				if (m.getKey() == null) {
					TableSchema ts = getTableSchema(m.getKeyspace(), m.getTable());
					for (Column column : ts.getColumns()) {
						if (column.isPrimaryKey()) {
							C3Clause clause = m.getWhere().getClause(column.getName());
							saveOneColumn(m.getKeyspace(), m.getTable(), m.getKeyName(), key, column.getName(), clause.getValue(), false);
						}
					}
				}
				
			} else {
				if (m.getKey() != null)
					removeOneColumn(m.getKeyspace(), m.getTable(), m.getKey().toString(), m.getKeyName(), m.getColumn(), false);
				else {
					String keyPrefix = buildKey(m.getKeyspace(), m.getTable(), m.getWhere());
					Set<Object> keys = getDb().hashMap(m.getKeyspace() + "___" + m.getTable()).createOrOpen().keySet();
					
					for (Object key : keys) {
						clearIndex(m.getKeyspace(), m.getTable(), key.toString());
						
						if (key.toString().startsWith(keyPrefix)) {
							getDb().hashMap(m.getKeyspace() + "___" + m.getTable()).createOrOpen().remove(key);
						}
					}
				}
					
			}
		}
//		getDb().commit(); // TODO conf
	}

	@Override
	public long countRows(String keyspace, String table, C3Where c3where) {
		return getDb().hashMap(keyspace + "___" + table).createOrOpen().size() 
				/ getTableSchema(keyspace, table).getColumns().size();
		
//		long found = 0;
//		for (Row row : tableIterable(keyspace, table)) {
//			if (row.isFullfiled(c3where, false))
//				found++;
//		}
//		return found;
		
//		HTreeMap<Object, Object> tableMap = (HTreeMap<Object, Object>) getDb().hashMap(keyspace + "___" + table).createOrOpen();
//		
//		if (c3where == null || c3where.getClauses().size() == 0)
//			return tableMap.sizeLong();
//		
//		int counter = 0;
//		for (Object key : tableMap.keySet()) {
//			Map<String, Object> columns = (Map<String, Object>) tableMap.get(key);
//			if (isColumnsFullfillWhere(columns, c3where))
//				counter++;
//		}
//		
//		return counter;
	}
	
	@Override
	public void remove(String keyspace, String table, C3Where c3where) throws IOException {
		TableSchema schema = getTableSchema(keyspace, table);
		
		for (Row row : tableIterable(keyspace, table)) {
			if (isColumnsFullfillWhere(row, c3where)) {
				clearIndex(keyspace, table, row.getPk().toString());
				for (Column col : schema.getColumns()) {					
					removeOneColumn(keyspace, table, row.getPk().toString(), "not used", col.getName(), false);
				}
			}
		}
		
//		HTreeMap<Object, Object> tableMap = (HTreeMap<Object, Object>) getDb().hashMap(keyspace + "___" + table).createOrOpen();
//		
//		if (c3where == null || c3where.getClauses().size() == 0)
//			tableMap.clear();
//		
//		for (Object key : tableMap.keySet()) {
//			Map<String, Object> columns = (Map<String, Object>) tableMap.get(key);
//			if (isColumnsFullfillWhere(columns, c3where)) {
//				clearIndex(keyspace, table, key.toString());
//				tableMap.remove(key);
//			}
//		}
	}

	private boolean isColumnsFullfillWhere(Row columns, C3Where c3where) {
		if (columns != null)
			for (C3Clause c3clause : c3where.getClauses()) {
				Object obj = columns.get(c3clause.getField());
				if (c3clause.getClauseType() == ClauseType.EQ) {
					if (!c3clause.getValue().equals(obj))
						return false;
				} else
					throw new NotImplementedException("Not implemented clause type " + c3clause.getClauseType());
			}
		return true;
	}
	
	@Override
	public Iterable<Row> tableIterable(final String keyspace, final String table) {
		final TableSchema schema = getTableSchema(keyspace, table);
		final String pkName = schema.getPrimaryKeyName();
		
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					private final String mapName = keyspace + "___" + table;
					
					private Map<String, Object> hashMap = (Map<String, Object>) getDb().hashMap(mapName).createOrOpen();
					
					private Iterator keyIterator = hashMap.keySet().iterator();
					
					private Row row = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Row next() {
						return row;
					}
					
					@Override
					public boolean hasNext() {
						while (keyIterator.hasNext()) {
							String key = keyIterator.next().toString();
							
							if (!key.endsWith("__" + pkName))
								continue;
							
							row = getRow(keyspace, table, key.substring(0, key.lastIndexOf("__")));
							
//							Map<String, Object> columnsMap = (Map<String, Object>) hashMap.get(key);
							
//							row = new Row(key).addColumns(columnsMap);
							return true;
						}
//						getDb().commit();
						return false;
					}
				};
			}
		};
	}
	
	@Override
	public void close() {
		if (db != null)
			getDb().close();
		db = null;
	}
}
