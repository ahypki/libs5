package net.hypki.libs5.distributed.hazelcast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import net.hypki.libs5.distributed.Distributed;
import net.hypki.libs5.distributed.Lock;
import net.hypki.libs5.distributed.Member;
import net.hypki.libs5.utils.LibsLogger;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

public class DistributedHazelcast implements Distributed {
	
	private static HazelcastInstance hazelcastInstance = null;
	
	private static Config config = null;
	
	private static Distributed distributed = null;
	
	private DistributedHazelcast() {
		
	}
	
	public static Distributed getInstance(final Config config) {
		if (config != null)
			DistributedHazelcast.config = config;
		return getInstance();
	}
	
	public static Distributed getInstance() {
		if (distributed == null) {
			distributed = new DistributedHazelcast();
		}
		return distributed;
	}
	
	private static Config getConfig() {
		if (config == null) {
			config = new Config();
			config.setProperty("hazelcast.logging.type", "log4j");
			config.setProperty("hazelcast.logging.level", "WARN");
//			config.getProperties().setProperty("hazelcast.logging.type", "log4j");
		}
		return config;
	}

	private static HazelcastInstance getHazelcastInstance() {
		if (hazelcastInstance == null) {
			hazelcastInstance = Hazelcast.newHazelcastInstance(getConfig());
		}
		return hazelcastInstance;
	}

	@Override
	public List<Member> getMembers() {
		List<Member> members = new ArrayList<>();
		
		for (final com.hazelcast.cluster.Member m : getHazelcastInstance().getCluster().getMembers()) {
			members.add(new Member() {
				private com.hazelcast.cluster.Member hazMember = m;
				
				@Override
				public int getPort() {
					return hazMember.getSocketAddress().getPort();
				}
				
				@Override
				public String getHost() {
					return hazMember.getSocketAddress().getHostName();
				}
				
				@Override
				public String toString() {
					return getHost() + ":" + getPort();
				}
			});
		}
		
		return members;
	}
	
	@Override
	public Map getMap(String name) {
		return getHazelcastInstance().getMap(name);
	}
	
	@Override
	public Lock getLock(final String name) {
		return new Lock() {
			private java.util.concurrent.locks.Lock lock = getHazelcastInstance().getCPSubsystem().getLock(name);
			
			@Override
			public boolean tryLock(int maxMs) {
				try {
					return lock.tryLock(maxMs, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					LibsLogger.error(DistributedHazelcast.class, "Cannot get lock " + name, e);
					return false;
				}
			}
			
			@Override
			public void unlock() {
				if (lock != null)
					lock.unlock();
			}
		};
	}
}
