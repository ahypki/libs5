Set of libraries, helper classes needed by a number of projects (e.g. http://BEANScode.net).


## Installation

1. Install _libs5_ witch Apache Maven:

```
git clone https://gitlab.com/ahypki/libs5.git
cd libs5
mvn clean install
```
