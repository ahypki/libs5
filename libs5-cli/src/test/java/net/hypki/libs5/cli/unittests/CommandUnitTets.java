package net.hypki.libs5.cli.unittests;

import java.io.IOException;

import net.hypki.libs5.cli.commands.Command;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class CommandUnitTets extends LibsTestCase {

	@Test
	public void testMatches() {
		Command cmd = new Command("one two") {
			
			@Override
			public void run(String[] params) throws IOException {
				
			}
		};
		
		assertTrue(cmd.isPartiallyMatched("one"));
		assertTrue(cmd.isPartiallyMatched("one "));
		assertTrue(cmd.isPartiallyMatched("on"));
		assertTrue(cmd.isPartiallyMatched("o"));
		assertTrue(cmd.isPartiallyMatched("on "));
		assertTrue(cmd.isPartiallyMatched(" one"));
		assertTrue(cmd.isPartiallyMatched(" one "));
		
		assertTrue(cmd.isPartiallyMatched("one t"));
		assertTrue(cmd.isPartiallyMatched("one tw"));
	}
}
