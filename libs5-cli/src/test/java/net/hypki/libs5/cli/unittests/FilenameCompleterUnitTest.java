package net.hypki.libs5.cli.unittests;

import java.util.List;

import net.hypki.libs5.cli.completer.FilenameCompleter;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class FilenameCompleterUnitTest extends LibsTestCase {

	@Test
	public void testFilenameCompleter() {
		FilenameCompleter completer = new FilenameCompleter();
		
		List<String> candidates = completer.getCandidates("jeden /h");
		for (String s : candidates) {
			System.out.println(s);
		}
	}
}
