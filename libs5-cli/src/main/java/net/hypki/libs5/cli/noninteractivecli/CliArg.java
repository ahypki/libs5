package net.hypki.libs5.cli.noninteractivecli;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface CliArg {
	String name();
	String description();
	boolean required() default true;
}
