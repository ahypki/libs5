package net.hypki.libs5.cli.noninteractivecli;

import static java.lang.String.*;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class Arg {

	private ArgDef def = null;
	private Object value = null;
		
	public Arg(ArgDef def, Object value) {
		setDef(def);
		setValue(value);
	}

	public ArgDef getDef() {
		return def;
	}

	private void setDef(ArgDef def) {
		this.def = def;
	}

	public Object getValue() {
		return value != null ? value : getDef().getDefaultValue();
	}

	private void setValue(Object value) {
		this.value = value;
	}
	
	public String stringValue() {
		return (String) getValue();
	}
	
	public Long longValue() {
		return (Long) getValue();
	}
	
	public Boolean boolValue() {
		return (Boolean) getValue();
	}
	
	public Double doubleValue() {
		return (Double) getValue();
	}

	public void validate() throws CLIException {
		if (getDef().isRequired() && getValue() == null)
			throw new CLIException(format("Arg %s is required, but no value is specified", getDef().getName()));
		
		if (getDef().isRequired() && getValue() instanceof String)
			if (((String) getValue()).trim().length() == 0)
				throw new CLIException(format("Arg %s is required, but no value is specified", getDef().getName()));
		
		switch (getDef().getType()) {
		case BOOLEAN:
			if (!(getValue() instanceof Boolean))
				throw new CLIException(format("Arg %s should be boolean", getDef().getName()));
			break;
			
		case DOUBLE:
			if (!(getValue() instanceof Double))
				throw new CLIException(format("Arg %s should be double", getDef().getName()));
			break;
			
		case LONG:
			if (!(getValue() instanceof Long))
				throw new CLIException(format("Arg %s should be long", getDef().getName()));
			break;
			
		case STRING:
			if (!(getValue() instanceof String))
				throw new CLIException(format("Arg %s should be string", getDef().getName()));
			break;

		default:
			throw new CLIException("Unimplemented Arg type " + getDef().getType());
	}
	}
}
