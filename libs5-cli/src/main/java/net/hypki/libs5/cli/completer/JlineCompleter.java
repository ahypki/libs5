package net.hypki.libs5.cli.completer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jline.Completor;
import net.hypki.libs5.cli.CLIManager;
import net.hypki.libs5.cli.commands.Command;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;

public class JlineCompleter implements Completor {
	

	public JlineCompleter() {
		
	}
	
	@Override
	public int complete(String buffer, int cursor, List candidates) {
		
//		List<ICompleter> possibleCompleters = new ArrayList<ICompleter>();
		
		try {
			List<String> candidatesString = new ArrayList<String>();
			for (ICompleter completer : CLIManager.instance().getCompleters()) {
				if (completer.isMatch(buffer))
					candidatesString.addAll(completer.getCandidates(buffer));
			}
			

			// finding common prefix for array of string
			String commonPrefix = StringUtilities.findCommonPrefix(candidatesString.toArray());
			
			if (candidatesString.size() > 1) {
				
				CLIManager.instance().backspaceAll();
				
				CLIManager.instance().printNewLine();
				for (String str : candidatesString) {
					CLIManager.instance().printString(str + "\n");
				}
				
				CLIManager.instance().printPrompt();
				CLIManager.instance().putString(commonPrefix);
			} else if (candidatesString.size() == 1) {
				CLIManager.instance().backspaceAll();
				CLIManager.instance().putString(commonPrefix);
			} else {
				// do nothing
			}
		} catch (IOException e) {
			LibsLogger.error(getClass(), "Cannot complete command", e);
		}
		
		return 0;
	}
}
