package net.hypki.libs5.cli.noninteractivecli;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.StringUtilities;

public class CLIManager {
		
	private List<CLICommand> commands = null;
	
	public CLIManager() {
		
	}
	
	public List<Class> findCLICLasses(Class<?> startingClass) throws ClassNotFoundException, IOException {
		List<Class> cls = new ArrayList<Class>();
		
		String jarPath = ReflectionUtility.getJarExecutionPath(startingClass);
		
		List<Class<?>> allClasses = new File(jarPath).isFile() == true ? 
				ReflectionUtility.getClassesFromJARFile(jarPath, startingClass.getPackage().getName()) 
				: ReflectionUtility.getClasses(startingClass, true);
		
		for (Class<?> clazz : allClasses/*ReflectionUtility.getClasses(startingClass, CLICommand.class, true)*/) {
			if (Modifier.isAbstract(clazz.getModifiers()) == false
					&& ReflectionUtility.isSubclassOf(clazz, CLICommand.class)) {
		
				cls.add(clazz);
			}
		}
		
		return cls;
	}
	
	public CLIManager searchForCLIClasses(Class<?> startingClass) throws CLIException {
		try {
			for (Class clazz : findCLICLasses(startingClass)) {
				addCommand(clazz);				
			}
			
			return this;
		} catch (ClassNotFoundException | IOException e) {
			throw new CLIException("Cannot find/create/add CLI command", e);
		}
	}
	
	public CLIManager addCommand(Class<?> cmdClass) {
		try {
			Constructor constructor = cmdClass.getDeclaredConstructors()[0];
			
			CLICommand cli = null;
			
			if (constructor.getParameterCount() == 1)
				cli = (CLICommand) constructor.newInstance(new Object[] {null});
			else if (constructor.getParameterCount() == 2)
				cli = (CLICommand) constructor.newInstance(new Object[] {null, null});
			else
				cli = (CLICommand) constructor.newInstance();
			
			// names have to be unique
			if (findCommand(cli.getCLICommandName()) == null)
				getCommands().add(cli);	
			
			// sort commands by name
			sortCommands(new Comparator<CLICommand>() {
				public int compare(CLICommand o1, CLICommand o2) {
					return o1.getCLICommandName().compareTo(o2.getCLICommandName());
				};
			});
		} catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LibsLogger.error(CLIManager.class, "Cannot create new instance of CLI command " + cmdClass, e);
		}
		
		return this;
	}
	
	public CLICommand findCommand(String name) {
		for (CLICommand cli : getCommands()) {
			if (cli.getCLICommandName().equals(name))
				return cli;
		}
		return null;
	}
	
	public CLICommand findCommand(String [] args) {
		String name = extractName(args);
		if (name == null)
			return null;
		
		for (CLICommand cli : getCommands()) {
			if (cli.getCLICommandName().equals(name))
				return cli;
		}
		return null;
	}
	
	public CLICommand run(String [] args) throws CLIException, IOException {
		// extracting name from args
		String name = extractName(args);
		if (name == null)
			throw new CLIException("No command specified");
		
		// searching for command
		CLICommand cmd = findCommand(name);
		if (cmd == null)
			throw new CLIException(format("Command %s not found", name));
				
		// run command
		cmd.run(args);
		
		return cmd;
	}

	public String extractName(String[] args) {
		if (args.length <= 0)
			return null;
		
		String name = "";
		
		for (String arg : args) {
			if (nullOrEmpty(arg) == false && arg.matches("^\\w.*$"))
				name += " " + arg;
			else
				break;
		}
		
		return name.trim();
	}

	public List<CLICommand> getCommands() {
		if (commands == null)
			commands = new ArrayList<>();
		return commands;
	}

	private void setCommands(List<CLICommand> commands) {
		this.commands = commands;
	}
	
	public void sortCommands(Comparator<? super CLICommand> comparator) {
		getCommands().sort(comparator);
	}
	
	public void printHelp() {
		printHelp(true);
	}

	public void printHelp(boolean printParams) {
		System.out.println(format("\nHELP:\n"));
		
		for (CLICommand cmd : getCommands()) {
			if (printParams)
				System.out.println();
			System.out.println(StringUtilities.padRight(cmd.getCLICommandName(), 25) + " -\t" + cmd.getCLICommandDescription());
			
			if (printParams) {
				for (Field field : ReflectionUtility.getFieldsRecursivelly(cmd.getClass(), cmd.getClass(), CliArg.class)) {
					final CliArg cliArg = (CliArg) field.getAnnotation(CliArg.class);
					final Class type = field.getType();
					System.out.println("\t" + cliArg.name() + "\t-\t" + cliArg.description() + " [required= " + cliArg.required() + ", type= " + type.toString() + "]");
				}
			}
		}
	}

	public CLIManager addCommands(List<Class<?>> classes) {
		for (Class clazz : classes) {
			LibsLogger.debug(CLIManager.class, "Found class ", clazz);
			addCommand(clazz);
		}
		return this;
	}
}
