package net.hypki.libs5.cli.noninteractivecli;

import static java.lang.String.*;

public class ArgDef {

	private String name = null;
	private ArgType type = null;
	private boolean required = false;
	private String description = null;
	private Object defaultValue = null;
		
	public ArgDef(String name, ArgType type, boolean required, Object defaultValue, String description) {
		setName(name);
		setType(type);
		setRequired(required);
		setDescription(description);
		setDefaultValue(defaultValue);
	}
	
	@Override
	public String toString() {
		return format("\t%-20s[type: %s, reguired: %s, default: %s]\t-\t%s", getName(), getType(), isRequired(), getDefaultValue(), getDescription());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArgType getType() {
		return type;
	}

	public void setType(ArgType type) {
		this.type = type;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
}
