package net.hypki.libs5.cli.utils;

public class Color {
	public static final String RED_START = "\033[22;31m";
	public static final String RED_STOP = "\033[0m";
	public static final String BLUE_START = "\033[22;34m";
	public static final String BLUE_STOP = "\033[0m";
}
