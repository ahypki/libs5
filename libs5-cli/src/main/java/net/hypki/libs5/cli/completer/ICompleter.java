package net.hypki.libs5.cli.completer;

import java.util.List;

public interface ICompleter {
	
	public boolean isMatch(String buffer);
	public List<String> getCandidates(String buffer);
}
