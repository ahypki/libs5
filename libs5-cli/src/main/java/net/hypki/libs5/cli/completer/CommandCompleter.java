package net.hypki.libs5.cli.completer;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.cli.CLIManager;
import net.hypki.libs5.cli.commands.Command;

public class CommandCompleter implements ICompleter {

	public CommandCompleter() {
		
	}
	
	@Override
	public boolean isMatch(String buffer) {
		for (Command cmd : CLIManager.instance().getCommandsList()) {
			if (cmd.isPartiallyMatched(buffer)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public List<String> getCandidates(String buffer) {
		// find all possible commands
		List<Command> possibleCommand = new ArrayList<Command>();
		for (Command cmd : CLIManager.instance().getCommandsList()) {
			if (cmd.isPartiallyMatched(buffer)) {
				possibleCommand.add(cmd);
			}
		}
		
		// create command names list
		List<String> cmdNames = new ArrayList<String>();
		for (Command command : possibleCommand) {
			if (command.getName().length() > buffer.length())
				cmdNames.add(command.getName());
		}
		
		return cmdNames;
		
//		try {
//			// find all possible commands
//			List<Command> possibleCommand = new ArrayList<Command>();
//			for (Command cmd : CLIManager.instance().getCommandsList()) {
//				if (cmd.commandMatches(buffer)) {
//					possibleCommand.add(cmd);
//				}
//			}
//			
//			// create command names list
//			Set<String> cmdNames = new HashSet<String>();
//			for (Command command : possibleCommand) {
//				cmdNames.add(command.getName());
//			}
//			
//			// finding common prefix for array of string
//			String commonPrefix = StringUtilities.findCommonPrefix(cmdNames);
//			
//			if (possibleCommand.size() > 1) {
//				
//				CLIManager.instance().backspaceAll();
//				
//				CLIManager.instance().printNewLine();
//				for (String key : cmdNames) {
//					CLIManager.instance().printString(key + "\n");
//				}
//				
//				CLIManager.instance().printPrompt();
//				CLIManager.instance().putString(commonPrefix);
//			} else if (possibleCommand.size() == 1) {
//				CLIManager.instance().backspaceAll();
//				CLIManager.instance().putString(commonPrefix);
//			} else {
//				// do nothing
//			}
//			
//			return 0;
//		} catch (IOException e) {
//			LibsLogger.error(this.getClass(), "Cannot complete command", e);
//			return 0;
//		}
	}
}
