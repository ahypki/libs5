package net.hypki.libs5.cli.noninteractivecli;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;

import net.hypki.libs5.cli.CLIManager;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.collections.CollectionUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.commons.lang.NotImplementedException;

public abstract class CLICommand {
	
	private String [] args = null;

	public abstract String getCLICommandName();
	public abstract String getCLICommandDescription();
//	public abstract ArgDef[] getArgsDescription();
	protected abstract CLICommand doCommand() throws CLIException, IOException;
	
	public CLICommand run(String [] args) throws CLIException, IOException {
		setArgs(args);
		
		LibsLogger.debug(getClass(), "Args: ", args != null ? CollectionUtils.toString(args, " ") : "");
		
		try {
			for (Field field : ReflectionUtility.getFieldsRecursivelly(getClass(), CLICommand.class, CliArg.class)) {
				final CliArg cliArg = (CliArg) field.getAnnotation(CliArg.class);
				Class type = field.getType();
				
				if (type.equals(String.class)) {
					
					final String argValue = ArgsUtils.getString(args, cliArg.name());
					
					assertTrue(cliArg.required() == false || StringUtilities.notEmpty(argValue), "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && argValue == null)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValue);
					
				} else if (type.equals(Double.class) || type.equals(double.class)) {
					
					final Double argValue = ArgsUtils.getDouble(args, cliArg.name());
					
					assertTrue(cliArg.required() == false || argValue != null, "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && argValue == null)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValue);
					
				} else if (type.equals(Long.class) || type.equals(long.class)) {
					
					final Long argValue = ArgsUtils.getLong(args, cliArg.name());
					
					assertTrue(cliArg.required() == false || argValue != null, "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && argValue == null)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValue);
					
				} else if (type.equals(Integer.class) || type.equals(int.class)) {
					
					final Integer argValue = ArgsUtils.getInt(args, cliArg.name());
					
					assertTrue(cliArg.required() == false || argValue != null, "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && argValue == null)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValue);
					
				} else if (type.equals(Float.class) || type.equals(float.class)) {
					
					final Float argValue = ArgsUtils.getFloat(args, cliArg.name());
					
					assertTrue(cliArg.required() == false || argValue != null, "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && argValue == null)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValue);
					
				} else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
										
					final boolean argExists = ArgsUtils.exists(args, cliArg.name());
					final Boolean argValue = ArgsUtils.getBoolean(args, cliArg.name());
					
					LibsLogger.debug(CLICommand.class, "Arg ", cliArg.name(), " value from args ", argValue);
					assertTrue(cliArg.required() == false || argValue != null, "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && !argExists)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValue);
					
				} else if (type.equals(List.class)) {
					
					final List<String> argValues = ArgsUtils.getStrings(args, cliArg.name());
					
					assertTrue(cliArg.required() == false || argValues.size() > 0, "Argument --" + cliArg.name() + " is required");
					
					if (!cliArg.required() && argValues.size() == 0)
						continue; // do nothing
					
					field.setAccessible(true);
					field.set(this, argValues);
					
				} else {
					try {
						final String argValue = ArgsUtils.getString(args, cliArg.name());
						
						LibsLogger.debug(CLICommand.class, "Arg ", cliArg.name(), " value from args ", argValue);
						assertTrue(cliArg.required() == false || argValue != null, "Argument --" + cliArg.name() + " is required");
												
						field.setAccessible(true);
						field.set(this, type.getDeclaredConstructor(String.class).newInstance(argValue));
					} catch (net.hypki.libs5.utils.utils.ValidationException e) {
						throw e;
					} catch (Exception e) {
//						LibsLogger.error(CLICommand.class, "Using String ", e);
						throw new NotImplementedException("Type " + type.toString() + " not implemented");
					}
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new CLIException("Cannot parse args", e);
		}
		
//		if (args != null) {
//			// converting string args to Arg[]
//			HashMap<String, Arg> argsMap = convertArgs(this, args);
//			
//			// validate Arg[]
//			for (Arg arg : argsMap.values()) {
//				arg.validate();
//			}
//			
//			setArgs(argsMap);
//		}
//		
//		assertTrue(getArgs() != null, "Args are not defined");
		
		return doCommand();
	}

//	private HashMap<String, Arg> convertArgs(CLICommand cmd, String[] args) throws CLIException {
//		HashMap<String, Arg> argsMap = new HashMap<>();
//		
//		for (ArgDef argConf : cmd.getArgsDescription()) {
//			switch (argConf.getType()) {
//				case BOOLEAN:
//					argsMap.put(argConf.getName(), new Arg(argConf, ArgsUtils.getBoolean(args, argConf.getName())));
//					break;
//					
//				case DOUBLE:
//					argsMap.put(argConf.getName(), new Arg(argConf, ArgsUtils.getDouble(args, argConf.getName())));
//					break;
//					
//				case LONG:
//					argsMap.put(argConf.getName(), new Arg(argConf, ArgsUtils.getLong(args, argConf.getName())));
//					break;
//					
//				case STRING:
//					argsMap.put(argConf.getName(), new Arg(argConf, ArgsUtils.getString(args, argConf.getName())));
//					break;
//	
//				default:
//					throw new CLIException("Unimplemented Arg type " + argConf.getType());
//			}
//		}
//		
//		return argsMap;
//	}

	
//	public Object getArgValue(String paramName) {
//		return getArgs().get(paramName).getValue();
//	}
//	
//	public String getArgString(String paramName) {
//		return getArgs().get(paramName).stringValue();
//	}
	
//	protected void info(String msg) {
//		LibsLogger.debug(CLICommand.class, msg);
//		System.out.println(msg);
//	}
//	
//	protected void error(String msg) {
//		LibsLogger.error(CLICommand.class, msg);
//		System.err.println(msg);
//	}
//
//	protected void warn(String msg) {
//		LibsLogger.warn(CLICommand.class, msg);
//		System.out.println("WANRING: " + msg);
//	}
	
//	public HashMap<String, Arg> getArgs() {
//		if (args == null)
//			args = new HashMap<>();
//		return args;
//	}
//	
//	private void setArgs(HashMap<String, Arg> args) {
//		this.args = args;
//	}
	
//	public CLICommand addArg(String name, Object value) {
//		ArgDef def = null;
//		for (ArgDef argDef : getArgsDescription()) {
//			if (argDef.getName().equalsIgnoreCase(name)) {
//				def = argDef;
//			}
//		}
//		
//		AssertUtils.assertTrue(def != null, "Unexpected param '" + name + "', cannot find arg's definition");
//		
//		getArgs().put(name, new Arg(def, value));
//		
//		return this;
//	}
	
	public String readString(String prompt, String defaultValue) {
		String tmp = CLIManager.instance().readLineNonEmpty(prompt);
		return StringUtilities.nullOrEmpty(tmp) ? defaultValue : tmp;
	}
	
	public boolean readBoolean(String prompt, boolean defaultValue) {
		while (true) {
			String tmp = CLIManager.instance().readLineNonEmpty(prompt);
			
			// check if CTRL + D was pressed
			if (tmp == null) {
				return defaultValue;
			}
			
			// return string if it's not empty
			return StringUtilities.nullOrEmpty(tmp) == false && tmp.equalsIgnoreCase("yes");
		}
	}
	
	protected String [] getArgs() {
		return args;
	}
	
	protected void setArgs(String [] args) {
		this.args = args;
	}
}
