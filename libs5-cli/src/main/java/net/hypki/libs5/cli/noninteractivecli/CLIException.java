package net.hypki.libs5.cli.noninteractivecli;

public class CLIException extends RuntimeException {

	public CLIException() {
		super();
	}
	
	public CLIException(String message) {
		super(message);
	}
	
	public CLIException(String message, Throwable t) {
		super(message, t);
	}
}
