package net.hypki.libs5.cli;

import static net.hypki.libs5.cli.utils.Color.*;
import static net.hypki.libs5.utils.string.StringUtilities.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jline.ConsoleReader;
import net.hypki.libs5.cli.commands.Command;
import net.hypki.libs5.cli.completer.ICompleter;
import net.hypki.libs5.cli.completer.JlineCompleter;
import net.hypki.libs5.cli.utils.Color;
import net.hypki.libs5.utils.LibsLogger;

public abstract class CLIManager {
	
	abstract public String getPrompt();
	abstract public List<Command> getCommandsList();
	abstract public List<ICompleter> getCompleters();
	
	private static ConsoleReader consoleReader = null;
	private static CLIManager thisInstance = null;
	
	private synchronized ConsoleReader getConsoleReader() {
		if (consoleReader == null) {
			try {
				consoleReader = new ConsoleReader();
				
				consoleReader.addCompletor(new JlineCompleter());
			} catch (IOException e) {
				throw new RuntimeException("Cannot create jline console reader", e);
			}
		}
		return consoleReader;
	}
	
	protected CLIManager() {
		if (thisInstance != null)
			throw new RuntimeException("Only one instance of " + CLIManager.class.getName() + " is allowed");
		
		CLIManager.thisInstance = this;
	}
	
	public synchronized static CLIManager instance() {
		if (thisInstance == null)
			thisInstance = new CLIManager() {
				@Override
				public List<Command> getCommandsList() {
					return null;
				}
				@Override
				public List<ICompleter> getCompleters() {
					return null;
				}
				@Override
				public String getPrompt() {
					return null;
				}
			};
		
		return thisInstance;
	}
	
	public void run() {
		// reading user commands and processing them...
		while (true) {
			
			try {
				// print prompt
				printPrompt();
	
				// read command
				String commandString = readLine().trim();
				if (nullOrEmpty(commandString))
					printError("Command not specified!");
				
				// search for possible commands
				List<Command> possibleCommand = new ArrayList<Command>();
				for (Command cmd : getCommandsList()) {
					if (cmd.isPartiallyMatched(commandString)) {
						possibleCommand.add(cmd);
					}
				}
				
				// run command if only one command was found
				if (possibleCommand.size() > 1) {
					printError("To many possible commands!");
					for (Command command : possibleCommand) {
						printStringLn(command.getName());
					}
				} else if (possibleCommand.size() == 0) {
					printError("No possible command was found!");
				} else if (possibleCommand.size() == 1) {
					possibleCommand.get(0).run(commandString.split(" "));
				} else if (possibleCommand.size() == 1 && possibleCommand.get(0).getName().equals(commandString) == false) {
					printStringLn(possibleCommand.get(0).getName());
				}
			} catch (IOException e) {
				LibsLogger.error("Command failed", e);
				printError("Command failed: " + e.getMessage());
			} catch (Exception e) {
				LibsLogger.error("Command failed", e);
				printError("Command failed: " + e.getMessage());
			}
		}
	}
	
	public void backspaceAll() throws IOException {
//		reader.backspaceAll();
		while (getConsoleReader().backspace());
	}

	public void printNewLine() throws IOException {
		printStringLn("");
	}
	
	public void putString(String s) throws IOException {
		getConsoleReader().putString(s);
	}
	
	public String readPassword(String prompt) {
		try {
			return getConsoleReader().readLine(new Character('*'));
		} catch (IOException e) {
			LibsLogger.error(CLIManager.class, "Cannot read command", e);
			printError("Cannot read command");
			return null;
		}
	}
	
	public String readLine() {
		try {
			return getConsoleReader().readLine("");
		} catch (IOException e) {
			LibsLogger.error(CLIManager.class, "Cannot read command", e);
			printError("Cannot read command");
			return null;
		}
	}
	
	public String readLineNonEmpty(String prompt) {
		try {
			return getConsoleReader().readLine(prompt);
		} catch (IOException e) {
			LibsLogger.error(CLIManager.class, "Cannot read command", e);
			printError("Cannot read command");
			return null;
		}
	}

	public void printPrompt() {
		printString(getPrompt());
	}
	
	public void printError(String errorMessage) {
//		reader.printString(s);
		System.out.println(RED_START + " ERROR: " + errorMessage + RED_STOP);
	}
	
	public void printInfo(String errorMessage) {
//		reader.printString(s);
		System.out.println(BLUE_START + errorMessage + BLUE_STOP);
	}

	public void printString(String s) {
		System.out.print(s);
	}
	
	public void printStringLn(String s) {
		System.out.println(s);
	}
}
