package net.hypki.libs5.cli.completer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.hypki.libs5.utils.string.StringUtilities;

public class FilenameCompleter implements ICompleter {

	public FilenameCompleter() {
		
	}
	
	@Override
	public boolean isMatch(String buffer) {
		String word = StringUtilities.lastWord(buffer);
		return (word != null && word.startsWith("/"));
	}
	
	@Override
	public List<String> getCandidates(String buffer) {
		String lastWord = StringUtilities.lastWord(buffer);
		String translated = lastWord;
		translated = (translated == null) ? "" : translated;

        // special character: ~ maps to the user's home directory
        if (translated.startsWith("~" + File.separator)) {
            translated = System.getProperty("user.home") + translated.substring(1);
        } else if (translated.startsWith("~")) {
            translated = new File(System.getProperty("user.home")).getParentFile().getAbsolutePath();
        } else if (!(translated.startsWith(File.separator))) {
            translated = new File("").getAbsolutePath() + File.separator + translated;
        }

        File f = new File(translated);

        final File dir;

        if (translated.endsWith(File.separator)) {
            dir = f;
        } else {
            dir = f.getParentFile();
        }

        final File[] entries = (dir == null) ? new File[0] : dir.listFiles();
        
        String prefix = buffer.replaceFirst("\\s*[\\w/]+\\s*$", "");
        prefix = prefix.length() > 0 ? prefix + " " : prefix;
        List<String> candidates = new ArrayList<String>();
        if (entries != null)
	        for (File file : entries) {
	        	if (file.getPath().startsWith(lastWord))
	        		candidates.add(prefix + file.getPath());
			}

        Collections.sort(candidates);
        
        return candidates;
	}
}