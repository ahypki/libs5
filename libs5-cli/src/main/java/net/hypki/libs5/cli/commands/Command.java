package net.hypki.libs5.cli.commands;

import java.io.IOException;

import jodd.util.StringUtil;
import net.hypki.libs5.cli.CLIManager;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

public abstract class Command {
	
	private String name = null;
	
	abstract public void run(String [] params) throws IOException;
	
	public Command(String commandName) {
		this.name = commandName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Command) {
			Command cmd = (Command) obj;
			return this.getName().equals(cmd.getName());
		}
		return false;
	}
	
	public boolean isPartiallyMatched(String cmdPart) {
		if (StringUtilities.nullOrEmpty(cmdPart))
			return false;
//		
//		return getName().matches("\\s*" + cmdPart + "[\\w\\s]*$");
//		
		String[] cmdPartNS = StringUtil.split(cmdPart.trim(), " ");
		String[] thisNS = StringUtil.split(getName(), " ");
		for (int i = 0; i < thisNS.length && i < cmdPartNS.length; i++) {
			if (thisNS[i].length() >= cmdPartNS[i].length()) {
				if (thisNS[i].startsWith(cmdPartNS[i]) == false)
					return false;
			} else
				return false;
		}
		return true;
//		
////		return cmdPartNS.length() > thisNS.length() ? cmdPartNS.startsWith(thisNS) : thisNS.startsWith(cmdPartNS);
	}
	
	
	public String readPassword(String prompt) {
		while (true) {
			String tmp = CLIManager.instance().readPassword(prompt);
			
			// check if CTRL + D was pressed
			if (tmp == null) {
				return null;
			}
			
			// return string if it's not empty
			if (StringUtilities.nullOrEmpty(tmp) == false)
				return tmp;
		}
	}
	
	public boolean readBoolean(String prompt, boolean defaultValue) {
		while (true) {
			String tmp = CLIManager.instance().readLineNonEmpty(prompt);
			
			// check if CTRL + D was pressed
			if (tmp == null) {
				return defaultValue;
			}
			
			// return string if it's not empty
			return StringUtilities.nullOrEmpty(tmp) == false && tmp.equalsIgnoreCase("yes");
		}
	}
	
	public String readString(String prompt) {
		while (true) {
			String tmp = CLIManager.instance().readLineNonEmpty(prompt);
			
			// check if CTRL + D was pressed
			if (tmp == null) {
				return null;
			}
			
			// return string if it's not empty
			if (StringUtilities.nullOrEmpty(tmp) == false)
				return tmp;
		}
	}
	
	public int readInt(String prompt) {
		while (true) {
			String tmp = CLIManager.instance().readLineNonEmpty(prompt);
			
			// check if CTRL + D was pressed
			if (tmp == null) {
				return 0;
			}
			
			// return string if it's not empty
			if (StringUtilities.nullOrEmpty(tmp) == false) {
				try {
					int tmpInt = NumberUtils.toInt(tmp);
					return tmpInt;
				} catch (NumberFormatException e) {
					LibsLogger.error(Command.class, "Cannot convert " + tmp + " to int", e);
				}
			}
		}
	}
	
	public void putString(String toPut) throws IOException {
		CLIManager.instance().putString(toPut);
	}
	
	public void print(String toPrint) {
		CLIManager.instance().printString(toPrint);
	}
	
	public void printInfo(String toPrint) {
		CLIManager.instance().printInfo(toPrint);
	}
	
	public void printLn(String toPrint) {
		CLIManager.instance().printStringLn(toPrint);
	}
	
	public void printErrorLn(String toPrint) {
		CLIManager.instance().printError(toPrint);
	}
}
