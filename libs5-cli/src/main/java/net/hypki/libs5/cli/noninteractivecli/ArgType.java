package net.hypki.libs5.cli.noninteractivecli;

public enum ArgType {
	STRING,
	LONG,
	DOUBLE,
	BOOLEAN
}
