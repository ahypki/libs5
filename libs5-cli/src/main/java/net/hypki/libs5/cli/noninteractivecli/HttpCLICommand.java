package net.hypki.libs5.cli.noninteractivecli;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;

import jodd.util.ArraysUtil;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.commons.lang.NotImplementedException;
import org.javatuples.Pair;

public abstract class HttpCLICommand extends CLICommand {
	
	public final String MAGIC_KEY = "magickey";
	
	public abstract String getHost();
	public abstract int getPort();
	public abstract String getUrlPath();
	public abstract boolean isLoginRequired();
	public abstract String getUserLC();
	public abstract String getLoginCookieName();
	public abstract void logout();
	public abstract void processResponse();
	public abstract Params getRESTParams();
	public abstract String getBody();
	
	private String restMethodType = null;
	
	private HttpResponse restResponse = null;
	
	private MediaType mediaType = null;
	
	public HttpCLICommand(final String restMethodType) {
		this.setRestMethodName(restMethodType);
	}
	
	public String getURL() {
		if (getPort() > 0)
			return getHost() + ":" + getPort() + "/" + getUrlPath();
		else
			return getHost() + "/" + getUrlPath();
	}
	
	protected HttpResponse delete(Params params) throws IOException {
		assertTrue(isLoginRequired() == false 
				|| (isLoginRequired() && getUserLC() != null)
				|| getRESTParams().contains(MAGIC_KEY), "User is not logged in");
		
		return UrlUtilities.delete(getURL(), new String[] {getLoginCookieName(), getUserLC()}, params);
	}
	
	protected HttpResponse post(Params params) throws IOException {
		assertTrue(isLoginRequired() == false 
				|| (isLoginRequired() && getUserLC() != null)
				|| getRESTParams().contains(MAGIC_KEY), "User is not logged in");
		
		return UrlUtilities.post(getURL(), new String[] {getLoginCookieName(), getUserLC()}, params);
	}
	
	protected HttpResponse post(String body) throws IOException {
		assertTrue(isLoginRequired() == false 
				|| (isLoginRequired() && getUserLC() != null)
				|| getRESTParams().contains(MAGIC_KEY), "User is not logged in");
		
		return UrlUtilities.post(getURL(), new String[] {getLoginCookieName(), getUserLC()}, body);
	}
	
	protected HttpResponse get(Params params, MediaType mediaType) throws IOException {
		assertTrue(isLoginRequired() == false 
				|| (isLoginRequired() && getUserLC() != null)
				|| getRESTParams().contains(MAGIC_KEY), "User is not logged in");
		
		return UrlUtilities.get(getURL(), new String[] {getLoginCookieName(), getUserLC()}, params, mediaType);
	}
	
	protected InputStream[] getPostInputStreams() {
		return null;
	}
	
	protected String[] getPostInputStreamsNames() {
		return null;
	}
	
	@Override
	protected CLICommand doCommand() throws CLIException, IOException {
		try {
			assertTrue(!nullOrEmpty(getHost()), 	"Host is not specified for command " + getClass().getSimpleName());
//			assertTrue(getPort() > 0, 				"Port is not specified for command " + getClass().getSimpleName());
			assertTrue(!nullOrEmpty(getUrlPath()), 	"Url path is not specified for command " + getClass().getSimpleName());
//			assertTrue(getRestMethodType() != null, "API not recognized");
			
			assertTrue(isLoginRequired() == false 
					|| (isLoginRequired() && getUserLC() != null)
					|| getRESTParams().contains(MAGIC_KEY), "User is not logged in");
						
			HttpResponse resString = null;
			
			if (getRestMethodType().equalsIgnoreCase("post")) {
				if (getPostInputStreams() != null)
					resString = UrlUtilities.post(getURL(), 
							new String[] {getLoginCookieName(), getUserLC()},
							getPostInputStreams(),
							getPostInputStreamsNames(),
							getRESTParams());
				else if (getBody() != null)
					resString = post(getBody());
				else
					resString = post(getRESTParams());
			} else if (getRestMethodType().equalsIgnoreCase("get"))
				resString = get(getRESTParams(), getMediaType());
			else if (getRestMethodType().equalsIgnoreCase("delete"))
				resString = delete(getRESTParams());
			else
				throw new NotImplementedException("Unknown method name: POST, GET expected");
						
			setRestResponse(resString);
			
			processResponse();
		} catch (Exception e) {
			throw new IOException("Error in HTTP CLI command", e);
		}
		
		return this;
	};
	
	private void setRestResponse(HttpResponse response) {
		this.restResponse = response;
	}
	
	public HttpResponse getResponse() {
		return this.restResponse;
	}
	
	protected String getRestMethodType() {
		return restMethodType;
	}
	
	protected void setRestMethodName(String methodName) {
		this.restMethodType = methodName;
	}
	
	public MediaType getMediaType() {
		return mediaType;
	}
	
	public HttpCLICommand setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
		return this;
	}
}
