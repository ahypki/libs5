package net.hypki.libs5.additions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class PdfUtils {

	public static void mergePdfs(List<String> pdfFiles, String outputFilename) throws IOException {
		try {
			PDFMergerUtility ut = new PDFMergerUtility();
			for (String f : pdfFiles)
				ut.addSource(f);
			ut.setDestinationFileName(outputFilename);
			ut.mergeDocuments();
			
			LibsLogger.debug(PdfUtils.class, "Pdf files merged to " + outputFilename);
		} catch (COSVisitorException e) {
			throw new IOException("Cannot merge pdf files into one", e);
		}
	}
	
	public static boolean addMetadata(File pdfFile, String title, String subject, String keywords, String creator, String author) {
		try {
			UUID tmpFile = UUID.randomUUID();
			PdfReader reader = new PdfReader(pdfFile.getAbsolutePath());
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(tmpFile.toString()));
			HashMap<String, String> info = reader.getInfo();
			info.put("Title", title);
			info.put("Subject", subject);
			info.put("Keywords", keywords);
			info.put("Creator", creator);
			info.put("Author", author);
			stamper.setMoreInfo(info);
			stamper.close();
			reader.close();
			
			FileUtils.moveFile(new File(tmpFile.toString()), pdfFile);
			
			LibsLogger.debug(PdfUtils.class, "Metadata added to file ", pdfFile);
			
			return true;
		} catch (DocumentException | IOException e) {
			LibsLogger.error(PdfUtils.class, "Cannot add metadata to the PDF file " + pdfFile, e);
			return false;
		}
	}
}
