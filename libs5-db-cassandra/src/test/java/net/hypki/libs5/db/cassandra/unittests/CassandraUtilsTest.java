package net.hypki.libs5.db.cassandra.unittests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.TableMetadata;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;

public class CassandraUtilsTest extends CassandraTestCase {

	@Test
	public void testListTables() {
		final String keyspace = "testKs";
		final String tb = "tab1";
		
		getCassandraProvider().createKeyspace(keyspace);
		
		getCassandraProvider().createTable(keyspace, new TableSchema(tb).addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true));
		
		List<String> expectedTables = new ArrayList<String>();
		expectedTables.add(tb);
		
		for (TableMetadata table : (Iterable<TableMetadata>) getCassandraProvider().getColumnFamilyListDefinitions(keyspace)) {
			LibsLogger.debug(CassandraUtilsTest.class, "Table: ", table.getName());
			assertTrue(expectedTables.contains(table.getName()), "Table " + table.getName() + " was NOT expected, but was found in the database");
			expectedTables.remove(table.getName());
		}
		
		assertTrue(expectedTables.size() == 0, "Not all tables from expected tables were found in the database");
	}
	
	@Test
	public void testIterator() throws IOException, ValidationException {
		final String keyspace = "testKs";
		final String tb = "tab2";
		final String column = "col1";
		
		getCassandraProvider().createKeyspace(keyspace);
		
		getCassandraProvider().createTable(keyspace, new TableSchema(tb)
				.addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true)
				.addColumn(column, ColumnType.STRING, false));
		
		List<String> expectedkeys = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			UUID id = UUID.random();
			
			expectedkeys.add(id.getId());
			
			DbObject.getDatabaseProvider().save(new MutationList()
				.addMutation(new Mutation(MutationType.INSERT, keyspace, tb, String.valueOf(i), DbObject.COLUMN_PK, column, id.getId())));
		}
		
		for (Row row : (Iterable<Row>) getCassandraProvider().iterateTable(keyspace, tb)) {
			String id = row.getString(column);
			expectedkeys.remove(id);
		}
		
		assertTrue(expectedkeys.size() == 0, "Not all rows were found with the table iterator");
		
		DbObject.getDatabaseProvider().removeTable(keyspace, tb);
	}
	
	@Test
	public void testPagination() throws IOException, ValidationException {
		final String keyspace = "testKs";
		final String tb = "tab3";
		final String column = "col1";
		
		getCassandraProvider().createKeyspace(keyspace);
		
		getCassandraProvider().createTable(keyspace, new TableSchema(tb)
				.addColumn("pk", ColumnType.STRING, true)
				.addColumn(column, ColumnType.STRING, false));
		
		List<String> expectedkeys = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			UUID id = UUID.random();
			
			expectedkeys.add(id.getId());
			
			getCassandraProvider().save(new MutationList()
				.addMutation(new Mutation(MutationType.INSERT, keyspace, tb, String.valueOf(i), DbObject.COLUMN_PK, column, id.getId())));
		}
		
		String currectPage = null;
		do {
			Results results = getCassandraProvider().getList(keyspace, tb, new String[] {column}, null, 100, currectPage, null);
			
			for (net.hypki.libs5.db.db.Row row  : results.getRows()) {
				expectedkeys.remove(row.getColumns().get(column));
			}
			
			currectPage = results.getNextPage();
		} while (currectPage != null);
				
		assertTrue(expectedkeys.size() == 0, "Not all rows were found with the table iterator");
		
		DbObject.getDatabaseProvider().removeTable(keyspace, tb);
	}
	
	@Test
	public void testGetList() throws IOException, ValidationException {
		final String keyspace = new TestColumn().getKeyspace();
		final String tb = new TestColumn().getColumnFamily();
		final String column =  "col1";
		
		DbObject.getDatabaseProvider().createKeyspace(keyspace);
		
		DbObject.getDatabaseProvider().createTable(keyspace, new TableSchema(tb)
				.addColumn("pk", ColumnType.STRING, true)
				.addColumn(column, ColumnType.STRING, false));
		
		List<String> expectedkeys = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			UUID id = UUID.random();
			
			expectedkeys.add(id.getId());
			
			new TestColumn(id.getId(), column, id.getId()).save();
		}
		
		List<TestColumn> results = new ArrayList<>();
		String currectPage = null;
		while ((currectPage = DbObject.getList(TestColumn.class, keyspace, tb, column, null, 100, currectPage, results)) != null) {			
			for (TestColumn tc : results) {
				expectedkeys.remove(tc.getValue());
			}
		}
				
		assertTrue(expectedkeys.size() == 0, "Not all rows were found with the table iterator");
		
		DbObject.getDatabaseProvider().removeTable(keyspace, tb);
	}
	
	@Test
	public void testIteratorClass() throws IOException, ValidationException {
		final String keyspace = new TestColumn().getKeyspace();
		final String tb = new TestColumn().getColumnFamily();
		final String column =  "col1";
		
		DbObject.getDatabaseProvider().createKeyspace(keyspace);
		
		DbObject.getDatabaseProvider().createTable(keyspace, new TableSchema(tb)
				.addColumn("pk", ColumnType.STRING, true)
				.addColumn(column, ColumnType.STRING, false));
		
		List<String> expectedkeys = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			UUID id = UUID.random();
			
			expectedkeys.add(id.getId());
			
			new TestColumn(id.getId(), column, id.getId()).save();
		}
		
		for (TestColumn tc : DbObject.iterateTable(TestColumn.class, keyspace, tb, column)) {
			expectedkeys.remove(tc.getValue());			
		}
				
		assertTrue(expectedkeys.size() == 0, "Not all rows were found with the table iterator");
		
		DbObject.getDatabaseProvider().removeTable(keyspace, tb);
	}
}
