package net.hypki.libs5.db.cassandra.unittests;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.apache.cassandra.thrift.Cassandra;
import org.apache.cassandra.thrift.Deletion;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class CassandraUnitTest extends LibsTestCase {
	
	
//	@Test
	public void testBatchMutateDeletion() throws FileNotFoundException, InvalidRequestException, UnavailableException, TimedOutException, TException {
		final String cf = "cfTest";
		
		// reading settings		
		String cassandraHost = Settings.getString("Cassandra/host");
		int cassandraPort = Settings.getInt("Cassandra/port", 9160);
		
		// connection settings for Cassandra
		TTransport tr = new TSocket(cassandraHost, cassandraPort);
		TProtocol proto = new TBinaryProtocol(tr);
		Cassandra.Client cassandraClient = new Cassandra.Client(proto);
		
		// opening the connection
		tr.open();
		
		// creating mutation map
		Map<String, Map<String, List<Mutation>>> mutation_map = new HashMap<String, Map<String,List<Mutation>>>();
		long timestamp = System.currentTimeMillis();
		String key = "key-test";
		
		Map<String, List<Mutation>> cfMap = mutation_map.get(key);
		if (cfMap == null) {
			cfMap = new HashMap<String, List<Mutation>>();
			mutation_map.put(key, cfMap);
		}
		
		List<Mutation> mutationsList = cfMap.get(cf);
		if (mutationsList == null) {
			mutationsList = new ArrayList<Mutation>();
			cfMap.put(cf, mutationsList);
		}
		
		
		Deletion deletion = new Deletion();
//		deletion.setSuper_column(null);
//		deletion.setSuper_columnIsSet(true);
		deletion.setTimestamp(System.currentTimeMillis());
		deletion.setTimestampIsSet(true);
		
		Mutation mut = new Mutation();
		mut.setDeletion(deletion);
		mut.setDeletionIsSet(true);
		mutationsList.add(mut);
		
		// INFO not valid any longer
//		cassandraClient.batch_mutate(WeblibsConst.KEYSPACE, mutation_map, ConsistencyLevel.QUORUM);
	}
}
