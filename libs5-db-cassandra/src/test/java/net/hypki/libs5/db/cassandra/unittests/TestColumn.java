package net.hypki.libs5.db.cassandra.unittests;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;

import com.google.gson.annotations.Expose;

public class TestColumn extends DbObject {

	public static final String COLUMN_FAMILY = "testcolumn";
	
	@Expose
	private String key = null;
		
	@Expose
	private String column = null;
	
	@Expose
	private String value = null;
	
	public TestColumn() {
		
	}
	
	public TestColumn(String key, String column) {
		this.key = key;
		this.column = column;
	}
	
	public TestColumn(String key, String column, String value) {
		this.key = key;
		this.column = column;
		this.value = value;
	}
	
	@Override
	public String getKeyspace() {
		return "ut";
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getColumn() {
		return column;
	}

	@Override
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setColumn(String column) {
		this.column = column;
	}
	
	public static TestColumn getTestObject(String key, String column) throws IOException {
		return (TestColumn) DbObject.getDatabaseProvider().get("unittest-keyspace", COLUMN_FAMILY,
				new C3Where().addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, key)), 
				column, TestColumn.class);
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public int getValueAsInt() {
		return Integer.valueOf(getValue());
	}
}
