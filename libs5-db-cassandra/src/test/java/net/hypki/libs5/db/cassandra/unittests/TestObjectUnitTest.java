package net.hypki.libs5.db.cassandra.unittests;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.db.DbTestCase;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;


public class TestObjectUnitTest extends DbTestCase {
	
	@Test
	public void testClearKeyspace() throws IOException, ValidationException {
		
		for (int i = 0; i < 200; i++) {
			TestColumn t = new TestColumn(RandomGUID.getRawRandomGUID(), RandomUtils.nextPronounceableWord(), RandomUtils.nextPronounceableWord());
			t.save();
			addToRemove(t);
		}
	}
	
}
