package net.hypki.libs5.db.cassandra.unittests;

import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.weblibs.db.DbTestCase;

public class CassandraTestCase extends DbTestCase {

	public CassandraTestCase() {
		
	}
	
	protected CassandraProvider getCassandraProvider() {
		return (CassandraProvider) DbObject.getDatabaseProvider();
	}
}
