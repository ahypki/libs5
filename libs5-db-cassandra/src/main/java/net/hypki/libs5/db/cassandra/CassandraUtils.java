package net.hypki.libs5.db.cassandra;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.system.Basher;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;

public class CassandraUtils {

	public static boolean isCassandraRunning() {
		try {
			String out = Basher.executeCommand("ps aux | grep CassandraDaemon | grep -v grep | awk '{print $2}'", true);
//			String out = Basher.executeCommand("ps aux | grep apache-cassandra | grep -v grep | awk '{print $2}'", true);
			
			if (notEmpty(out))
				return true;
			
		} catch (Exception e) {
			LibsLogger.debug(CassandraUtils.class, "Cannot check if cassandra is running, assuming it is not running");
		}
		
		return false;
	}

	public static boolean startCassandra(String cassHome) {
//		String cmd = "cd " + cassHome + " && ./bin/cassandra > cassandra.log 2>&1";
		String cmd = "cd " + cassHome + " && ./bin/cassandra";
		LibsLogger.debug(CassandraUtils.class, "Running command: ", cmd);
		
		LibsLogger.debug(CassandraUtils.class, Basher.executeCommand(cmd, false));
		
		return true;
	}
	
	public static void stopCassandra() {
		Basher.executeCommand("ps aux | grep CassandraDaemon | grep -v grep | awk '{print $2}' | xargs kill", true);
	}
	
	public static Object getYaml(String cassandraConfigPath, String key) {
		try {
			YamlReader reader = new YamlReader(new FileReader(cassandraConfigPath));
			Object object = reader.read();
			Map cassYaml = (Map) object;
			return cassYaml.get(key);
		} catch (FileNotFoundException | YamlException e) {
			LibsLogger.error(CassandraUtils.class, "Cannot read key from cassandra config YAML", e);
			return null;
		}
	}
	
	public static void setYaml(String cassandraConfigPath, String key, Object value) {
		try {
			List<String> newLines = new ArrayList<>();
			List<String> lines = FileUtils.readLines(new File(cassandraConfigPath));
			for (String line : lines) {
				if (line.matches("[\\s]*" + key + "[\\s]*:" + "[\\s]*.*")) {
					String newLine = RegexUtils.replaceFirstGroup("[\\s]*" + key + "[\\s]*:" + "[\\s]*(.*)", line, value.toString());
					LibsLogger.debug(CassandraUtils.class, line, " => ", newLine);
					newLines.add(newLine);
				} else
					newLines.add(line);
			}
			
			FileUtils.saveToFile(newLines, new File(cassandraConfigPath));
		} catch (IOException e) {
			LibsLogger.error(CassandraUtils.class, "Cannot set YAML conf for cassandra", e);
		}
	}
}
