package net.hypki.libs5.db.cassandra;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


import net.hypki.libs5.utils.LibsLogger;

//import org.apache.cassandra.db.IColumn;
import org.apache.cassandra.utils.ByteBufferUtil;

public class RepeatFailedTxMapReduce extends MapReduce2 {
	
//	static private BlockingQueue<Job> jobsQueue = null;
	
	@Override
	protected void beforeJob() {
//		jobsQueue = HazelcastManager.getInstance().getQueue(JobsWorker.JOBS_QUEUE);
	}
	
	@Override
	public void mapMap(ByteBuffer key, SortedMap<ByteBuffer, ByteBuffer> columns, org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {		
//		for (Entry<ByteBuffer, IColumn> col : columns.entrySet()) {
//			String value = ByteBufferUtil.string(col.getValue().value());
//			TxObject tx = CassandraObject.getGson().fromJson(value, TxObject.class);
//			if (tx.getStatus() == TxStatus.PENDING && tx.getTime().isOlderThan(5, TimeUnit.MINUTES)) {
//				jobsQueue.put(new RepeatTxJob(tx));
//				LibsLogger.debug(TxUnitTest.class, "Found failed Tx " + tx.getCombinedKey());
//			}
//		}
	}
	
	public RepeatFailedTxMapReduce(final String keyspace) {
		super(keyspace, net.hypki.libs5.db.db.weblibs.TxObject.COLUMN_FAMILY, net.hypki.libs5.db.db.weblibs.TxObject.COLUMN_FAMILY);
//		super(TxObject.COLUMN_FAMILY, TxObject.COLUMN_FAMILY, MapperImpl.class, ReducerImpl.class);
	}
}
