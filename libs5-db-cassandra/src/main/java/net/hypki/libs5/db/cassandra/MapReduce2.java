package net.hypki.libs5.db.cassandra;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;

import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;




//import org.apache.cassandra.db.IColumn;
import org.apache.cassandra.hadoop.ColumnFamilyInputFormat;
import org.apache.cassandra.hadoop.ColumnFamilyOutputFormat;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.utils.ByteBufferUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public abstract class MapReduce2 extends Configured implements Tool {
	
	private static final String THIS_CLASS = "THIS_CLASS";
	
	private boolean waitForCompletion = true;
	
	private String keyspace = null;
	private String inputCF = null;
	private String outputCF = null;
	
	private ByteBuffer startPredicate = null;
	private ByteBuffer stopPredicate = null;
	
//	private static MapReduce2 thisInstance = null;
	
	public MapReduce2() {
		
	}
	
	protected MapReduce2(String keyspace, String inputCF, String outputCF) {
		this.setKeyspace(keyspace);
		this.inputCF = inputCF;
		this.outputCF = outputCF;
	}

	protected void beforeJob() {
		
	}
	
	protected void afterJob() {
		
	}
	
	@Override
	public Configuration getConf() {
		if (super.getConf() == null) {
			setConf(new Configuration());
			
//			getConf().setInt("cassandra.input.split.size", 50);
//			LibsLogger.debug(MapReduce2.class, "cassandra.input.split.size set to 50");
//			
//			getConf().setInt("cassandra.range.batch.size", 50000);
//			LibsLogger.debug(MapReduce2.class, "cassandra.range.batch.size set to 50000");
			
			additionalConfiguration(getConf());
		}
		return super.getConf();
	}
	
	protected void additionalConfiguration(Configuration conf) {
		
	}
	
	public abstract void mapMap(ByteBuffer key, SortedMap<ByteBuffer, ByteBuffer> columns, org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException, InterruptedException;
	
	public void reduceReduce(Text keyIn, Iterable<Text> values, org.apache.hadoop.mapreduce.Reducer.Context context) throws IOException, InterruptedException {
		
	}
	
	public void reduceReduce(BytesWritable keyIn, Iterable<BytesWritable> values, org.apache.hadoop.mapreduce.Reducer.Context context) throws IOException, InterruptedException {
		
	}
	
	public static String toString(ByteBuffer bb) throws CharacterCodingException {
		return ByteBufferUtil.string(bb);
	}
	
	public static byte[] keyToBytes(ByteBuffer bb) {
		return ByteBufferUtil.getArray(bb);
	}
	
	public static String keyToString(ByteBuffer bb) {
		return new String(ByteBufferUtil.getArray(bb));
	}
	
	public static byte[] colNameToBytes(Entry<ByteBuffer, ByteBuffer> entry) {
		return ByteBufferUtil.getArray(entry.getKey());
	}
	
	public static String colNameToString(Entry<ByteBuffer, ByteBuffer> entry) {
		return new String(ByteBufferUtil.getArray(entry.getKey()));
	}
	
	public static byte[] colValueToBytes(Entry<ByteBuffer, ByteBuffer> entry) {
		return ByteBufferUtil.getArray(entry.getValue());
	}
	
	public static byte[] colValueToBytes(ByteBuffer col) {
		return ByteBufferUtil.getArray(col);
	}
	
	static class MapperTextImpl extends Mapper<ByteBuffer, SortedMap<ByteBuffer, ByteBuffer>, Text, Text> {
		public void map(ByteBuffer key, SortedMap<ByteBuffer, ByteBuffer> columns, Context context) throws IOException, InterruptedException {
//			if (thisInstance == null) {
				try {
					Class<? extends MapReduce2> thisClass = (Class<? extends MapReduce2>) Class.forName(context.getConfiguration().get(THIS_CLASS));
//					thisInstance = thisClass.newInstance();
					
					MapReduce2 mr2 = thisClass.newInstance();
					mr2.setConf(context.getConfiguration());
					mr2.mapMap(key, columns, context);
				} catch (ClassNotFoundException e) {
					LibsLogger.error(MapReduce2.MapperTextImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
				} catch (InstantiationException e) {
					LibsLogger.error(MapReduce2.MapperTextImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
				} catch (IllegalAccessException e) {
					LibsLogger.error(MapReduce2.MapperTextImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
				}
//			}
//			thisInstance.mapMap(key, columns, context);
		}
	}
	
	static class MapperByteImpl extends Mapper<ByteBuffer, SortedMap<ByteBuffer, ByteBuffer>, BytesWritable, BytesWritable> {
		public void map(ByteBuffer key, SortedMap<ByteBuffer, ByteBuffer> columns, Context context) throws IOException, InterruptedException {
//			if (thisInstance == null) {
				try {
					Class<? extends MapReduce2> thisClass = (Class<? extends MapReduce2>) Class.forName(context.getConfiguration().get(THIS_CLASS));
//					thisInstance = thisClass.newInstance();
					thisClass.newInstance().mapMap(key, columns, context);
				} catch (ClassNotFoundException e) {
					LibsLogger.error(MapReduce2.MapperByteImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
				} catch (InstantiationException e) {
					LibsLogger.error(MapReduce2.MapperByteImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
				} catch (IllegalAccessException e) {
					LibsLogger.error(MapReduce2.MapperByteImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
				}
//			}
//			thisInstance.mapMap(key, columns, context);
		}
	}
	
	static class ReducerTextImpl extends Reducer<Text, Text, ByteBuffer, List<Mutation>> {
		public void reduce(Text keyIn, Iterable<Text> values, Context context) throws IOException, InterruptedException {
//			thisInstance.reduceReduce(keyIn, values, context);
			try {
				Class<? extends MapReduce2> thisClass = (Class<? extends MapReduce2>) Class.forName(context.getConfiguration().get(THIS_CLASS));
//				thisInstance = thisClass.newInstance();
				thisClass.newInstance().reduceReduce(keyIn, values, context);
			} catch (ClassNotFoundException e) {
				LibsLogger.error(MapReduce2.MapperTextImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
			} catch (InstantiationException e) {
				LibsLogger.error(MapReduce2.MapperTextImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
			} catch (IllegalAccessException e) {
				LibsLogger.error(MapReduce2.MapperTextImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
			}
		}
	}

	static class ReducerByteImpl extends Reducer<BytesWritable, BytesWritable, ByteBuffer, List<Mutation>> {
		public void reduce(BytesWritable keyIn, Iterable<BytesWritable> values, Context context) throws IOException, InterruptedException {
//			thisInstance.reduceReduce(keyIn, values, context);
			try {
				Class<? extends MapReduce2> thisClass = (Class<? extends MapReduce2>) Class.forName(context.getConfiguration().get(THIS_CLASS));
//				thisInstance = thisClass.newInstance();
				thisClass.newInstance().reduceReduce(keyIn, values, context);
			} catch (ClassNotFoundException e) {
				LibsLogger.error(MapReduce2.MapperByteImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
			} catch (InstantiationException e) {
				LibsLogger.error(MapReduce2.MapperByteImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
			} catch (IllegalAccessException e) {
				LibsLogger.error(MapReduce2.MapperByteImpl.class, "Cannot create instance of " + context.getConfiguration().get(THIS_CLASS), e);
			}
		}
	}

	@Override
	public int run(String[] args) throws Exception {
		final String keyspace = ArgsUtils.getString(args, "keyspace", getKeyspace());
		
		assertTrue(StringUtilities.notEmpty(keyspace), "--keyspace arg cannot be empty");
		assertTrue(StringUtilities.notEmpty(inputCF), "inputCF cannot be empty");
		assertTrue(StringUtilities.notEmpty(outputCF), "outputCF cannot be empty");
				
		LibsLogger.debug(MapReduce2.class, "Starting map-reduce job with class " + getClass().getName());
		
		beforeJob();
				
		Job job = new Job(getConf(), getClass().getName());
		
		job.getConfiguration().set(THIS_CLASS, getClass().getName());
		
//		additionalConfiguration(job.getConfiguration());
		
		job.setJarByClass(MapReduce2.class);

		job.setMapperClass(getMapperImpl());
		job.setReducerClass(getReducerImpl());
		
//		job.setMapOutputKeyClass(ByteBuffer.class);
//		job.setMapOutputValueClass(ByteBuffer.class);
		
		job.setMapOutputKeyClass(getOutputKeyClass());
		job.setMapOutputValueClass(getOutputValueClass());
		
		job.setOutputKeyClass(Text.class);
//		job.setOutputKeyClass(ByteBuffer.class);
		job.setOutputValueClass(List.class);

		job.setOutputFormatClass(ColumnFamilyOutputFormat.class);

		ConfigHelper.setOutputColumnFamily(job.getConfiguration(), keyspace, outputCF);
//		ConfigHelper.setOutputRpcPort(job.getConfiguration(), "1234");//JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
		ConfigHelper.setOutputInitialAddress(job.getConfiguration(), JsonUtils.readString(Settings.getSettings(), "Cassandra/host"));
		ConfigHelper.setOutputPartitioner(job.getConfiguration(), "org.apache.cassandra.dht.Murmur3Partitioner");
		
		job.setInputFormatClass(ColumnFamilyInputFormat.class);
		
		
//		job.getConfiguration().set("cassandra.input.thrift.port", JsonUtils.readString(Settings.getSettings(), "Cassandra/port"));
//		job.getConfiguration().set("cassandra.output.thrift.port", JsonUtils.readString(Settings.getSettings(), "Cassandra/port"));		
		job.getConfiguration().set("cassandra.input.thrift.port", JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
		job.getConfiguration().set("cassandra.output.thrift.port", JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
		
		job.getConfiguration().set("cassandra.input.native.port", JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
		job.getConfiguration().set("cassandra.output.native.port", JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
//		job.getConfiguration().set("cassandra.input.thrift.rpc_port", JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
//		job.getConfiguration().set("cassandra.output.thrift.rpc_port", JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
//		ConfigHelper.setInputRpcPort(job.getConfiguration(), "1234");//JsonUtils.readString(Settings.getSettings(), "Cassandra/rpcPort"));
//		CqlConfigHelper.setinput ConfigHelper.setinputke
		
		
		job.getConfiguration().set("cassandra.input.keyspace", keyspace);
		job.getConfiguration().set("cassandra.output.keyspace", keyspace);
		
		
		ConfigHelper.setInputInitialAddress(job.getConfiguration(), JsonUtils.readString(Settings.getSettings(), "Cassandra/host"));
		ConfigHelper.setInputPartitioner(job.getConfiguration(), "org.apache.cassandra.dht.Murmur3Partitioner");
		ConfigHelper.setInputColumnFamily(job.getConfiguration(), keyspace, inputCF, true /*widerows*/);
//			SlicePredicate predicate = new SlicePredicate().setColumn_names(Arrays.asList(ByteBufferUtil.bytes(columnName)));
		
		SlicePredicate predicate = new SlicePredicate().setSlice_range(new SliceRange(getStartPredicate(), getStopPredicate(), false, Integer.MAX_VALUE));
		predicate.setSlice_rangeIsSet(true);
		ConfigHelper.setInputSlicePredicate(job.getConfiguration(), predicate);
		
//		List<IndexExpression> exprs = new ArrayList<>();
//		exprs.add(new IndexExpression());
//		ConfigHelper.setInputRange(job.getConfiguration(), null, null, exprs);
		
		additionalConfiguration(job.getConfiguration());

		job.waitForCompletion(isWaitForCompletion());
		
		afterJob();
					
		return 0;
	}
	
	private Class<? extends Mapper> getMapperImpl() {
		return getOutputKeyClass().equals(Text.class) ? MapperTextImpl.class : MapperByteImpl.class;
	}
	
	private Class<? extends Reducer> getReducerImpl() {
		return getOutputKeyClass().equals(Text.class) ? ReducerTextImpl.class : ReducerByteImpl.class;
	}

	/**
	 * Text.class, BytesWritable.class 
	 * @return
	 */
	protected Class<? extends Writable> getOutputValueClass() {
		return Text.class;
	}
	
	/**
	 * Text.class, BytesWritable.class 
	 * @return
	 */
	protected Class<? extends Writable> getOutputKeyClass() {
		return Text.class;
	}

	public void runMapReduce() throws Exception {
		ToolRunner.run(getConf(), this, new String[]{});
	}

	public ByteBuffer getStartPredicate() {
		return startPredicate != null ? startPredicate : ByteBufferUtil.EMPTY_BYTE_BUFFER;
	}

	public void setStartPredicate(ByteBuffer startPredicate) {
		this.startPredicate = startPredicate;
	}

	public ByteBuffer getStopPredicate() {
		return stopPredicate != null ? stopPredicate : ByteBufferUtil.EMPTY_BYTE_BUFFER;
	}

	public void setStopPredicate(ByteBuffer stopPredicate) {
		this.stopPredicate = stopPredicate;
	}

	public boolean isWaitForCompletion() {
		return waitForCompletion;
	}

	public MapReduce2 setWaitForCompletion(boolean waitForCompletion) {
		this.waitForCompletion = waitForCompletion;
		return this;
	}

	private String getKeyspace() {
		return keyspace;
	}

	private void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}
}
