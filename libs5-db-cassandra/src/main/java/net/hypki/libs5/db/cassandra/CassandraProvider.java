package net.hypki.libs5.db.cassandra;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.gt;
import static com.datastax.driver.core.querybuilder.QueryBuilder.gte;
import static com.datastax.driver.core.querybuilder.QueryBuilder.lt;
import static com.datastax.driver.core.querybuilder.QueryBuilder.lte;
import static com.datastax.driver.core.querybuilder.QueryBuilder.set;
import static net.hypki.libs5.db.weblibs.Settings.getInt;
import static net.hypki.libs5.db.weblibs.Settings.getString;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.db.db.schema.Column;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.DatabaseSchema;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.TxObject;
import net.hypki.libs5.db.db.weblibs.TxStatus;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;

import org.apache.cassandra.db.Keyspace;
import org.apache.cassandra.dht.BootStrapper.StringSerializer;
import org.apache.cassandra.serializers.BytesSerializer;
import org.apache.cassandra.serializers.DoubleSerializer;
import org.apache.cassandra.serializers.LongSerializer;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.NotImplementedException;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnDefinitions.Definition;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.DataType.Name;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PagingState;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.policies.ExponentialReconnectionPolicy;
import com.datastax.driver.core.querybuilder.Clause;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.datastax.driver.core.querybuilder.Select.Where;
import com.datastax.driver.core.querybuilder.Update.Assignments;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class CassandraProvider implements DatabaseProvider {
	
	private String clusterName = null;
	private String cassandraHost = null;
	private int cassandraPort = 0;
	private int cassandraRpcPort = 0;
	
	private Cluster cluster = null;
	private Keyspace cassKeyspace = null;
	private Session session = null;
	private QueryBuilder queryBuilder = null;

	private Set<String> createdKeyspacesCache = new HashSet<String>();
	
	private static final StringSerializer strSerializer = new StringSerializer();
	private static final DoubleSerializer doubleSerializer = new DoubleSerializer();
	private static final BytesSerializer byteSerializer = new BytesSerializer();
	private static final LongSerializer longSerializer = new LongSerializer();
	
	public static final String SYSTEM_PROPERTY_CASSANDRA_CONFIG = "cassandra.config";
	private static boolean cassandrConfigSet = false;
	
	private String CASSANDRA_HOME_CACHE = null;
	
	public void reloadColumnFamilies(Keyspace keyspace, DatabaseSchema databaseSchema) throws IOException {
		for (TableSchema tableSchema : databaseSchema.getTables()) {
			DbObject.getDatabaseProvider().createTable(keyspace.getName(), tableSchema);
		}
	}
	
	public static DoubleSerializer getDoubleSerializer() {
		return doubleSerializer;
	}
	
	public static StringSerializer getStringSerializer() {
		return strSerializer;
	}
	
	public static BytesSerializer getBytesArraySerializer() {
		return byteSerializer;
	}
	
	public static LongSerializer getLongSerializer() {
		return longSerializer;
	}
	
	public Session getSession() {
		if (session == null) {
			if (getCluster() != null) {
				session = getCluster().connect();
			}			
		}
		return session;
	}
	
	@Override
	public MetaList getInitParams() {
		return new MetaList();
	}
	
	@Override
	public void init(MetaList params) {
		
	}
	
	@Override
	public Iterable<net.hypki.libs5.db.db.Row> tableIterable(final String keyspace, final String table) {
		return new Iterable<net.hypki.libs5.db.db.Row>() {
			@Override
			public Iterator<net.hypki.libs5.db.db.Row> iterator() {
				return new Iterator<net.hypki.libs5.db.db.Row>() {
					private Iterator<Row> cassIterator = iterateTable(keyspace, table).iterator();
					
					@Override
					public net.hypki.libs5.db.db.Row next() {
						net.hypki.libs5.db.db.Row c3Row = new net.hypki.libs5.db.db.Row();
						Row cassRow = cassIterator.next();
						for (Definition cfDef : cassRow.getColumnDefinitions()) {
							c3Row.addColumn(cfDef.getName(), cassRow.getObject(cfDef.getName()));
						}
						return c3Row;
					}
					
					@Override
					public boolean hasNext() {
						return cassIterator.hasNext();
					}

					@Override
					public void remove() {
						throw new NotImplementedException();
					}
				};
			}
		};
	}
	
	public Iterable<Row> iterateTable(final String keyspace, final String table) {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return getSession().execute("SELECT * from " + keyspace + "." + table).iterator();
			}
		};
	}

	@Override
	public void createKeyspace(String keyspaceName) {
		if (getCluster() != null) {
			if (getKeyspaceDefinition(keyspaceName) != null)
				return;
			
			getSession().execute("CREATE KEYSPACE IF NOT EXISTS " + keyspaceName + " WITH replication = {'class':'SimpleStrategy', 'replication_factor':3};");
			LibsLogger.debug(CassandraProvider.class, "Keyspace " + keyspaceName + " created");
		}
	}
	
	public KeyspaceMetadata getKeyspaceDefinition(String keyspaceName) {
		if (getCluster() != null) {
			return getCluster().getMetadata().getKeyspace(keyspaceName);
		}
		return null;
	}
	
	public boolean cfExist(final String keyspace, final String columnFamily) {
		return getColumnFamilyDefinition(keyspace, columnFamily) != null;
	}
	
	@Override
	public void remove(final String keyspace, final String columnFamily, C3Where c3where) {
		createKeyspaceIfNotExist(keyspace);
		
		Delete where = getQueryBuilder()
				.delete()
				.from(keyspace, columnFamily)
//				.where(QueryBuilder.eq(DbObject.COLUMN_PK, "*"))
				;
		 
		if (c3where != null && c3where.getClauses().size() > 0)
			for (C3Clause c : c3where.getClauses())
				where.where(c3clauseToClause(c));
				
		getSession().execute(where);
		LibsLogger.debug(CassandraProvider.class, String.format("Removed all rows from %s.%s where %s", keyspace, columnFamily, where));
	}

	public String libs5DbSchemaToCQL3Schema(String keyspace, TableSchema tableSchema) {
		StringBuilder cfCreate = new StringBuilder();
		
		cfCreate.append("CREATE TABLE IF NOT EXISTS " + keyspace.toLowerCase() + "." + tableSchema.getName() + " (");
		boolean firstColumn = true;
		for (Column	column : tableSchema.getColumns()) {
			if (!firstColumn)
				cfCreate.append(", ");
			cfCreate.append(column.getName());
			cfCreate.append(" ");
			if (column.getType() == ColumnType.STRING)
				cfCreate.append("text");
			else if (column.getType() == ColumnType.SET_STRING)
				cfCreate.append("set<text>");
			else if (column.getType() == ColumnType.INTEGER)
				cfCreate.append("int");
			else if (column.getType() == ColumnType.LONG)
				cfCreate.append("bigint");
			else if (column.getType() == ColumnType.DOUBLE)
				cfCreate.append("double");
			else if (column.getType() == ColumnType.BLOB)
				cfCreate.append("blob");
			else
				throw new NotImplementedException("Type " + column.getType() + " is not implemented");
			firstColumn = false;
		}
		
		// primary key
		cfCreate.append(", PRIMARY KEY (");
		firstColumn = true;
		for (Column	column : tableSchema.getColumns()) {
			if (column.isPrimaryKey()) {
				if (!firstColumn)
					cfCreate.append(", ");
				cfCreate.append(column.getName());
			}
			firstColumn = false;
		}
		cfCreate.append("));");
		
		return cfCreate.toString();
	}
	
	@Override
	public void createTable(String keyspace, TableSchema tableSchema) {		
		if (cfExist(keyspace, tableSchema.getName())) {
			TableSchema schemaFromDB = getTableSchema(keyspace, tableSchema.getName());
			
			LibsLogger.debug(CassandraProvider.class, String.format("ColumnFamily %s for keyspace %s already exists", tableSchema.getName(), keyspace));

			if (schemaFromDB.equals(tableSchema)) {
				LibsLogger.debug(CassandraProvider.class, String.format("ColumnFamily %s for keyspace %s has the same schema", tableSchema.getName(), keyspace));
				return;
			}
		}
		
		if (cfExist(keyspace, tableSchema.getName())) {
			// removing old table
			removeTable(keyspace, tableSchema.getName());
		}
		
		String cfCreate = libs5DbSchemaToCQL3Schema(keyspace, tableSchema);
		
		LibsLogger.debug(CassandraProvider.class, "Cassandra statement: ", cfCreate.toString());
		ResultSet result = getSession().execute(cfCreate.toString());
		LibsLogger.debug(CassandraProvider.class, "Table ", tableSchema.getName(), " created in keyspace ", keyspace);
		
		for (Column column : tableSchema.getColumns()) {
			if (column.isIndexed() && !column.isPrimaryKey()) {
				final String cmd = "CREATE INDEX ON " + keyspace + "." + tableSchema.getName() + " (" + column.getName() + ");";
				LibsLogger.debug(CassandraProvider.class, "Creating index ", cmd);
				getSession().execute(cmd);
			}
		}
				
//		TableMetadata cfd = HFactory.createColumnFamilyDefinition(keyspace, columnFamily, ComparatorType.UTF8TYPE);
//		getCluster().addColumnFamily(cfd);
//		LibsLogger.debug(CassandraUtils.class, String.format("ColumnFamily %s for keyspace %s created", columnFamily, keyspace));
		return;
	}

//	public static List<HColumn<String, String>> getColumns(Keyspace keyspace, String columnFamily, String key, 
//			String startColumn, String finishColumn, boolean reversed, int count) {
//		ArrayList<HColumn<String, String>> objects = new ArrayList<HColumn<String,String>>();
//		
//		QueryResult<OrderedRows<String, String, String>> result = HFactory.createRangeSlicesQuery(keyspace, strSerializer, strSerializer, strSerializer)
//			.setColumnFamily(columnFamily)
//			.setKeys(key, key)
//			.setRange(startColumn, finishColumn, reversed, count)
//			.execute();
//		
//		for (Row<String, String, String> res : result.get()) {
//			objects.addAll(res.getColumnSlice().getColumns());
////			for (HColumn<String, String> col : res.getColumnSlice().getColumns()) {
////			}
//		}
//
//		return objects;
//	}
	
	private void createKeyspaceIfNotExist(final String keyspace) {
		if (createdKeyspacesCache.contains(keyspace))
			return;
		
		createKeyspace(keyspace);
		createdKeyspacesCache.add(keyspace);
	}
	
	public Collection<TableMetadata> getColumnFamilyListDefinitions(final String keyspace) {
//		if (getKeyspace(keyspace) != null)
		createKeyspaceIfNotExist(keyspace);
		
		KeyspaceMetadata meta = getCluster().getMetadata().getKeyspace(keyspace);
		return meta != null ? meta.getTables() : null;
//		else
//			return null;
	}
	
	private ColumnType cassandraTypeToLibs5(DataType dt) {
		if (dt.getName() == Name.INT)
			return ColumnType.INTEGER;
		else if (dt.getName() == Name.BIGINT)
			return ColumnType.LONG;
		else if (dt.getName() == Name.TEXT)
			return ColumnType.STRING;
		else if (dt.getName() == Name.DOUBLE)
			return ColumnType.DOUBLE;
		else if (dt.getName() == Name.BLOB)
			return ColumnType.BLOB;
		else
			throw new NotImplementedException("Conversion from Cassandra type " + dt.getName() + " to libs5 data type is not implemented");
	}
	
	@Override
	public TableSchema getTableSchema(String keyspace, String table) {
		TableMetadata tableMeta = getColumnFamilyDefinition(keyspace, table);
		
		if (tableMeta == null)
			return null;
		
		TableSchema schema = new TableSchema(table);
		
		String pkName = "";
		for (ColumnMetadata	colMeta : tableMeta.getPrimaryKey())
			pkName += colMeta.getName();
		
		for (ColumnMetadata	colMeta : tableMeta.getColumns()) {
			schema.addColumn(colMeta.getName(), cassandraTypeToLibs5(colMeta.getType()), colMeta.getName().equals(pkName));
		}
		
		return schema;
	}
	
	public TableMetadata getColumnFamilyDefinition(final String keyspace, final String columnFamily) {
		Collection<TableMetadata> tables = getColumnFamilyListDefinitions(keyspace);
		if (tables != null)
			for (TableMetadata def : tables) {
				if (def.getName().equalsIgnoreCase(columnFamily))
					return def;
			}
		return null;
	}

	
	

//	public static QueryResult<Rows<String, String, String>> getMany(final String keyspace, final String columnFamily, final String [] keys) {
//		MultigetSliceQuery<String, String, String> multigetSliceQuery = HFactory.createMultigetSliceQuery(getKeyspace(keyspace), strSerializer, strSerializer, strSerializer);
//		multigetSliceQuery.setColumnFamily(columnFamily);
//		multigetSliceQuery.setKeys(keys);
//		multigetSliceQuery.setRange("", "", false, Integer.MAX_VALUE);
//		return multigetSliceQuery.execute();
//	}
	
//	public static byte[] getData(final String keyspace, final String columnFamily, final byte[] key, final String column) {
//		LibsLogger.debug(String.format("Reading %s.%s.[%s].%s", keyspace, columnFamily, key, column));
//		return getSingleColumn(keyspace, columnFamily, key, column);
//	}
	
	public QueryBuilder getQueryBuilder() {
		if (queryBuilder == null) {
			queryBuilder = new QueryBuilder(getCluster());
		}
		return queryBuilder;
	}
	
//	protected static byte[] getSingleColumn(final String keyspace, final String columnFamily, final byte[] key, final String column) {
//		QueryResult<HColumn<String, byte[]>> result = HFactory.createColumnQuery(getKeyspace(keyspace), byteSerializer, strSerializer, byteSerializer)
//			.setColumnFamily(columnFamily)
//			.setKey(key)
//			.setName(column)
//			.execute();
//		return result.get() != null ? result.get().getValue() : null;
//	}
	
	public String getCassandraHost() {
		return cassandraHost;
	}
	
	public int getCassandraPort() {
		return cassandraPort;
	}
	
	public int getCassandraRpcPort() {
		return cassandraRpcPort;
	}
	
	public Cluster getCluster() {
		if (cluster == null || cluster.isClosed()) {
			clusterName = getString("Cassandra/clusterName");
			cassandraHost = getString("Cassandra/host");
			cassandraPort = getInt("Cassandra/port");
			cassandraRpcPort = getInt("Cassandra/rpcPort");
			
//			PoolingOptions poolingOptions = new PoolingOptions()
//			    .setCoreConnectionsPerHost(HostDistance.LOCAL,  4)
//			    .setMaxConnectionsPerHost( HostDistance.LOCAL, 100)
//			    .setCoreConnectionsPerHost(HostDistance.REMOTE, 2)
//			    .setMaxConnectionsPerHost( HostDistance.REMOTE, 100);
			
			cluster = Cluster.builder()
					.addContactPoint(cassandraHost)
//					.addContactPoint("para35.strw.leidenuniv.nl")
//					.addContactPoint("para36.strw.leidenuniv.nl")
//					.addContactPoint("para37.strw.leidenuniv.nl")
					.withPort(cassandraRpcPort)
//					.withPort(cassandraPort)
//					.withPoolingOptions(poolingOptions)
//					.withMaxSchemaAgreementWaitSeconds(120)
					.withReconnectionPolicy(new ExponentialReconnectionPolicy(60000, 120 * 10 * 60000))
//					.with
			        .withSocketOptions(
			                new SocketOptions()
			                        .setConnectTimeoutMillis(1200000)
			                        .setReadTimeoutMillis(1200000)
//			                        .set
			                        )
					.build();
			
			Metadata metadata = cluster.getMetadata();
			LibsLogger.debug(CassandraProvider.class, "Connected to cluster: ", metadata.getClusterName());
			for (Host host : metadata.getAllHosts() ) {
				LibsLogger.debug(CassandraProvider.class, String.format("Datacenter: %s; Host: %s; Rack: %s", host.getDatacenter(), host.getAddress(), host.getRack()));
			}
			
//			long start = System.currentTimeMillis();
//			cluster = HFactory.getOrCreateCluster(clusterName, cassandraHost + ":" + String.valueOf(cassandraPort));	
//			LibsLogger.debug("Cassandra cluster connection started in " + (System.currentTimeMillis() - start));
		}
		return cluster;
	}
	
	public void setCassandraHome(String cassandraHome) {
		assertTrue(notEmpty(cassandraHome), "Cassandra home path cannot be empty");
		CASSANDRA_HOME_CACHE = cassandraHome;
		LibsLogger.debug(CassandraProvider.class, "Cassandra home set to " + cassandraHome);
	}
	
	public String getCassandraHome() {
		if (notEmpty(CASSANDRA_HOME_CACHE))
			return CASSANDRA_HOME_CACHE;
			
		String cassHome = System.getProperty("cassandra.config");
		LibsLogger.debug(CassandraProvider.class, "System property cassandra.config=", cassHome);
		if (notEmpty(cassHome)) {
			return new FileExt(cassHome.startsWith("file:///") ? cassHome.substring("file://".length()) : cassHome).getParentFile().getParent();
		}
		
		String env = System.getenv("CASSANDRA_HOME");
		LibsLogger.debug(CassandraProvider.class, "System environment variable CASSANDRA_HOME=", env);
		if (notEmpty(env)) {
			return new FileExt(env).getAbsolutePath();
		}
		
		String cassHomeFromSettings = Settings.getString("Cassandra/CASSANDRA_HOME");
		LibsLogger.debug(CassandraProvider.class, "From settings CASSANDRA_HOME=", cassHomeFromSettings);
		if (notEmpty(cassHomeFromSettings)) {
			return new FileExt(cassHomeFromSettings).getAbsolutePath();
		}
		
		LibsLogger.debug(CassandraProvider.class, "Cannot find cassandra home path, bulk importing "
				+ "will not be possible! Define either -Dcassandra.config Java property or $CASSANDRA_HOME shell variables.");
		return null;
	}
	
	public void setSystemPropertyCassandraConfig() {
		if (cassandrConfigSet)
			return;
		
		String cassHome = getCassandraHome();
		assertTrue(notEmpty(cassHome), "Cassandra home path is not set. Set system property 'cassandra.config' or environment variable CASSANDRA_HOME");
		
		System.setProperty("cassandra.config", "file://" + cassHome + "/conf/cassandra.yaml");
		
		cassandrConfigSet = true;
		LibsLogger.debug(CassandraProvider.class, "cassandra.config system property set to: file://" + cassHome + "/conf/cassandra.yaml");
	}	

	public void repeatTransaction(final String keyspace, TxObject tx) throws ValidationException, IOException {
		if (tx.getStatus() == TxStatus.OK)
			throw new ValidationException("Transaction is already " + TxStatus.OK);
		
		save(tx.getTxMutations());
				
		// committing
		tx.setStatus(TxStatus.OK);
		tx.save();
	}

	public void clearColumnFamily(final TableMetadata hCfDef) {
		if (hCfDef == null)
			return;
		
		if (isTableEmpty(hCfDef.getKeyspace().getName(), hCfDef.getName()))
			return;
		
		long st = System.currentTimeMillis();
		getSession().execute(getQueryBuilder().truncate(hCfDef.getKeyspace().getName(), hCfDef.getName()));
		
//		getCluster().truncate(hCfDef.getKeyspaceName(), hCfDef.getName());
		LibsLogger.debug(CassandraProvider.class, "CF " + hCfDef.getName() + " truncated in " + (System.currentTimeMillis() - st) + " ms");
		
//		if (hCfDef.getColumnType() == ColumnType.STANDARD) {
//			RangeSlicesQuery<byte[], byte[], byte[]> rangeSlicesQuery = HFactory.createRangeSlicesQuery(getCassKeyspace(), byteSerializer, byteSerializer, byteSerializer)
//						.setColumnFamily(hCfDef.getName())
//						.setKeys(EMPTY_BYTE_BUFFER.array(), EMPTY_BYTE_BUFFER.array())
//						.setRowCount(500)
//						.setRange(EMPTY_BYTE_BUFFER.array(), EMPTY_BYTE_BUFFER.array(), false, 500);
//			
//			byte[] lastKey = EMPTY_BYTE_BUFFER.array();
//			boolean first = true;
//			boolean isMetaFile = BlobFile.COLUMN_FAMILY.equals(hCfDef.getName());
//			
//			while (true) {
//				OrderedRows<byte[], byte[], byte[]> orderedRows = rangeSlicesQuery.execute().get();
//				
//				if (orderedRows.getList().size() == 0)
//					break;
//				
//				Row<byte[],byte[],byte[]> lastRow = orderedRows.peekLast();
//				
//				if (first == false && ByteUtils.areEqual(lastRow.getKey(), EMPTY_BYTE_BUFFER.array()))// lastRow.equals(""))
//					break;
//				else if (ByteUtils.areEqual(lastRow.getKey(), lastKey))//(lastRow.getKey().equals(lastKey))
//					break;
//				
//				boolean dataExist = false;
//				Mutator<byte[]> mut = HFactory.createMutator(getCassKeyspace(), byteSerializer);
//				for (Row<byte[], byte[], byte[]> row : orderedRows) {
//					for (HColumn<byte[], byte[]> col : row.getColumnSlice().getColumns()) {
//						dataExist = true;
//						mut.delete(row.getKey(), hCfDef.getName(), col.getName(), byteSerializer);
//						
//						// remove file from storage
//						if (isMetaFile) {
//							throw new NotImplementedException();
////							try {
////								BlobFile mf = getGson().fromJson(col.getValue(), BlobFile.class);
////								mf.removeFileFromStorage();
////							} catch (Exception e) {
////								LibsLogger.error(CassandraObject.class, "Cannot remove file from storage", e);
////							}
//						}
//					}
//				}
//				
//				if (dataExist)
//					mut.execute();
//				else
//					lastKey = lastRow.getKey();
//				
//				rangeSlicesQuery.setKeys(lastKey, EMPTY_BYTE_BUFFER.array());
//				first = false;
//			}
//			LibsLogger.debug(CassandraObject.class, "ColumnFamily " + hCfDef.getName() + " cleared");
//		} else {
//			throw new NotImplementedException("SuperColumns not implemented");
//		}
	}

	@Override
	public void clearKeyspace(String keyspace) {
		LibsLogger.debug(CassandraProvider.class, "Clearing keyspace: " + keyspace);
		
		// clear column families
		Collection<TableMetadata> CFs = getColumnFamilyListDefinitions(keyspace);
		if (CFs != null)
			for (TableMetadata hCfDef : CFs) {
				if (isTableEmpty(keyspace, hCfDef.getName()) == false)
					clearColumnFamily(hCfDef);
			}
	}

	@Override
	public void clearTable(final String keyspace, final String columnFamily) {
		clearColumnFamily(getColumnFamilyDefinition(keyspace, columnFamily));
	}
	
	@Override
	public void removeTable(final String keyspace, final String table) {
		if (cfExist(keyspace, table)) {
			getSession().execute("DROP TABLE " + keyspace + "." + table);
	//		getCluster().dropColumnFamily(keyspace, columnFamily);
			LibsLogger.debug(CassandraProvider.class, "CF ", table, " from keyspace ", keyspace, " removed");
		}
	}

	@Override
	public List<String> getTableList(String keyspace) {
		List<String> cfNames = new ArrayList<String>();
		Collection<TableMetadata> tables = getColumnFamilyListDefinitions(keyspace);
		if (tables != null)
			for (TableMetadata cfd : tables) {
				cfNames.add(cfd.getName());
			}
		return cfNames;
	}

	@Override
	public boolean isTableEmpty(final String keyspace, final String columnFamily) {
		if (!cfExist(keyspace, columnFamily))
			return true;
			
		ResultSet result = getSession().execute(getQueryBuilder()
				.select()
				.from(keyspace, columnFamily)
				.limit(1)
				);
		return result.one() == null;
	}

	@Override
	public boolean exist(String keyspace, String columnFamily, C3Where c3where, String column) throws IOException {
		return get(keyspace, columnFamily, c3where, column) != null;
	}
	
//	@Override
//	public void save(String keyspace, String columnFamily, String key, String column, Object data) throws IOException, ValidationException {
//		getSession()
//			.execute(getQueryBuilder()
//						.update(keyspace, columnFamily)
//						.with(set(column, data))
//						.where(eq(DbObject.COLUMN_PK, key)));
//	}
	
	private int save(List<Mutation> mutations, int from, int size) throws IOException, ValidationException {
		StringBuilder bulk = new StringBuilder();
		
		bulk.append("BEGIN UNLOGGED BATCH\n");
		
		int saved = 0;
		for (int i = from; i < from + size; i++) {
			if (i == mutations.size())
				break;
			
			Mutation mut = mutations.get(i);
			
			if (mut == null)
				continue;
			
			if (mut.getMutation() == MutationType.INSERT) {
				
				Assignments ass = getQueryBuilder()
						.update(mut.getKeyspace(), mut.getTable())
						.with(set(mut.getColumn(), mut.getObjData()));
				
				if (mut.getKey() != null)
					ass
						.where(eq(mut.getKeyName(), mut.getKey()));
				else {
					for (C3Clause c3Clause : mut.getWhere().getClauses()) {
						if (c3Clause.getClauseType() == ClauseType.EQ)
							ass.where(eq(c3Clause.getField(), c3Clause.getValue()));
						else
							throw new NotImplementedException();
					}
				}
				
				bulk.append(ass);
				
			} else if (mut.getMutation() == MutationType.REMOVE) {
				
				Delete del = getQueryBuilder()
						.delete()
						.from(mut.getKeyspace(), mut.getTable());
				
				if (mut.getKey() != null)
					del
						.where(eq(mut.getKeyName(), mut.getKey()));
				else {
					for (C3Clause c3Clause : mut.getWhere().getClauses()) {
						if (c3Clause.getClauseType() == ClauseType.EQ)
							del.where(eq(c3Clause.getField(), c3Clause.getValue()));
						else
							throw new NotImplementedException();
					}
				}
				
				bulk.append(del);
				
			}
			
			bulk.append("\n");
			
			saved++;
		}
		
		bulk.append("APPLY BATCH;");
		
		getSession()
			.execute(bulk.toString());
		
		return saved;
	}
	
//	private void save(List<Mutation> mutations, int batchSize) throws IOException, ValidationException {
//		
//	}
	
	@Override
	public void save(List<Mutation> mutations) throws IOException, ValidationException {
		int batchSize = mutations.size();
		int saved = 0;
		while (true) {
			try {
				for (int from = 0; from < mutations.size(); from += batchSize) {
					saved += save(mutations, from, batchSize);
				}
				LibsLogger.debug(CassandraProvider.class, "Saved ", saved, " mutations");
				break;
			} catch (com.datastax.driver.core.exceptions.InvalidQueryException ex) {
				if (batchSize > 1
						&& ex.getMessage() != null 
						&& ex.getMessage().equalsIgnoreCase("Batch too large")) {
					batchSize = Math.max(batchSize / 4, 1);
					LibsLogger.warn(CassandraProvider.class, "Cassandra batch update is too large, batchSize changed to ", batchSize);
					saved = 0;
					continue;
				}
				throw ex;
			}
		}
		assertTrue(saved == mutations.size(), "Not all or too many mutations saved");
	}

//
//
//	@Override
//	public void remove(DbObject<T> dbObject) throws IOException {
//		removeColumn(dbObject.getKeyspace(), dbObject.getColumnFamily(), dbObject.getKey(), dbObject.getColumn());
//	}
	
//	public static long getRowsCount(final String keyspace, final List<String> tables) {
//		try {
//			long totalRows = 0;
//			for (String table : tables) {
//				long rows = getRowsCount(keyspace, table);
//				if (rows == -1)
//					return -1; // cannot count rows without timeout, so returning -1
//				totalRows += rows;
//			}
//			return totalRows;
//		} catch (com.datastax.driver.core.exceptions.NoHostAvailableException ex) {
//			LibsLogger.warn(CassandraUtils.class, "Cannot count rows count, most likely tables are to big (or Cassandra is really not running)", ex.getMessage());
//			return -1;
//		}
//	}
	
	@Override
	public long countRows(String keyspace, String table, C3Where c3where) {
		try {
			Select select = getQueryBuilder()
					.select()
					.countAll()
					.from(keyspace, table)
					.allowFiltering()
					;
			
			if (c3where != null) {
				for (C3Clause c3clause : c3where.getClauses()) {
					select.where(c3clauseToClause(c3clause));
				}
			}
			
			ResultSet result = getSession()
					.execute(select);
			Row one = result.one();
			return one != null ? one.getLong(0) : null;
		} catch (com.datastax.driver.core.exceptions.NoHostAvailableException ex) {
			LibsLogger.warn(CassandraProvider.class, "Cannot count rows count, most likely tables are to big (or Cassandra is really not running)", ex.getMessage());
			return -1;
		}
	}

	@Override
	public long countColumns(String keyspace, String columnFamily, String key) {
		return getColumnFamilyDefinition(keyspace, columnFamily).getColumns().size();
//		return HFactory.createCountQuery(getKeyspace(keyspace), strSerializer, strSerializer)
//				.setColumnFamily(columnFamily)
//				.setKey(key)
//				.setRange(null, null, 1)
//				.execute().get();
	}



//	@Override
//	public List<T> getList(Class<T> type, String keyspace, String columnFamily, String key, String startColumn, String finishColumn, boolean reversed, int count) {
//		return (List<T>) getCassandraObjectList(type, keyspace, columnFamily, key, startColumn, finishColumn, reversed, count);
//	}



	@Override
	public void remove(String keyspace, String columnFamily, String key, String keyName, String column) throws IOException {
		getSession().execute(getQueryBuilder().delete(column)
				.from(keyspace, columnFamily)
				.where(QueryBuilder.eq(keyName, key)));
//		Mutator<String> mutator = HFactory.createMutator(getKeyspace(keyspace), strSerializer);
//		mutator.delete(key, columnFamily, column, strSerializer);
//		mutator.execute();
		LibsLogger.debug(CassandraProvider.class, String.format("Removed %s.%s['%s']['%s']", keyspace, columnFamily, key, column));
	}


	@Override
	public String get(String keyspace, String columnFamily, C3Where c3where, String column) throws IOException {
		Select select = getQueryBuilder()
				.select(column)
				.from(keyspace, columnFamily)
				.allowFiltering();
		
		if (c3where != null) {
			for (C3Clause c3clause : c3where.getClauses()) {
				select.where(c3clauseToClause(c3clause));
			}
		}
		
		Row one = getSession().execute(select).one();
		return one != null ? one.getString(column) : null;
	}
	
//	public <T> T get(Class<T> clazz, final String keyspace, final String columnFamily, final String key, final String keyName, final String column) {
//		try {
//			String data = get(keyspace, columnFamily, key, keyName, column);
//			if (data == null)
//				return null;
//			
//			LibsLogger.debug(clazz, String.format("Read %s.%s.[%s].%s", keyspace, columnFamily, key, column));
//			return JsonUtils.getGson().fromJson(data, clazz);
//		} catch (JsonParseException | IOException e) {
//			throw new RuntimeException("Cannot parse json string of the object", e);
//		}
//	}

	@Override
	public <T> T get(String keyspace, String columnFamily, C3Where c3where, String column, Class<T> type) throws IOException {
		try {
			String data = get(keyspace, columnFamily, c3where, column);
			if (data == null)
				return null;
			
//			LibsLogger.debug(clazz, "Read " + buildCombinedKey(keyspace, columnFamily, key, column));
			return JsonUtils.fromJson(data, type);
		} catch (JsonParseException e) {
			throw new RuntimeException("Cannot parse json string of the object", e);
		}
	}
	
	@Override
	public Results getList(String keyspace, String columnFamily, String[] columns, Order order, int count, String state, C3Where c3where) {
		Watch w = new Watch();
		Results results = new Results();
		try {
			Statement st = null;
			
			if (columns != null)
				st = getQueryBuilder()
					.select(columns)
					.from(keyspace, columnFamily)
//					.allowFiltering()
					;
			else
				st = getQueryBuilder()
					.select()
					.from(keyspace, columnFamily)
//					.allowFiltering()
					;
			
			if (c3where != null && c3where.getClauses().size() > 0) {
				((Select) st).allowFiltering();
				
				Where where = ((Select) st)
					.where(c3clauseToClause(c3where.getClauses().get(0)));
				
				// TODO DEBUG
				if (!columnFamily.equals("job")) {
//				where.and(com.datastax.driver.core.querybuilder.QueryBuilder. gt(DbObject.COLUMN_PK, UUID.ZERO.getId()));
					for (int i = 1; i < c3where.getClauses().size(); i++) {
						where.and(c3clauseToClause(c3where.getClauses().get(i)));
					}
				}
			}
			st.setFetchSize(count);
			
			if (!columnFamily.equals("job")) {
				if (order != null) {
					((Select) st).orderBy(order.isAsc() ? QueryBuilder.asc(order.getColName()) : QueryBuilder.desc(order.getColName()));
				}
			}

			// This will be absent for the first page
			if (state != null)
			    st.setPagingState(PagingState.fromString(state));
			
			
			ResultSet rs = getSession().execute(st);
			
			// This will be null if there are no more pages
			PagingState nextPage = rs.getExecutionInfo().getPagingState();
			results.setNextPage(nextPage != null ? nextPage.toString() : null);
			
			// Note that we don't rely on RESULTS_PER_PAGE, since Cassandra might
			// have not respected it, or we might be at the end of the result set
			int remaining = rs.getAvailableWithoutFetching();
			rs.fetchMoreResults();
			for (Row row : rs) {
				
				if (columns == null) {
					columns = new String[row.getColumnDefinitions().size()];
					for (int col = 0; col < columns.length; col++) {
						columns[col] = row.getColumnDefinitions().getName(col);
					}
				}
			    
				net.hypki.libs5.db.db.Row r = new net.hypki.libs5.db.db.Row();
				
				for (String columnName : columns) {
					Object tmp = row.getObject(columnName);
					
					if (tmp instanceof ByteBuffer)
						r.addColumn(columnName, MapReduce2.toString((ByteBuffer) tmp));
					else
						r.addColumn(columnName, tmp);
//							StandardCharsets.UTF_8.decode((ByteBuffer) row.getObject(columnName)).toString()
//							new String( ( ).get array())
//					);
				}
				
				if (columnFamily.equals("job")) {
					JsonObject jo = (JsonObject) JsonUtils.parseJson(r.getAsString(DbObject.COLUMN_DATA));
					r.addColumn("channel", jo.get("channel").getAsString());
					r.addColumn("time", jo.get("time").getAsLong());
					if (r.isFullfiled(c3where, false) == false)
						continue;
				}
				
				results.addRow(r);
			
			    if (--remaining == 0)
			        break;
			}

//			LibsLogger.info(CassandraProvider.class, "Cql statement ", st, " in ", w);
		} catch (Exception e) {
			LibsLogger.error(CassandraProvider.class, "Cannot retrieve list", e);
		} finally {
			return results;
		}
	}
	

	public Clause c3clauseToClause(C3Clause c3clause) {
		if (c3clause.getClauseType() == ClauseType.EQ)
			return eq(c3clause.getField(), c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.GT)
			return gt(c3clause.getField(), c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.GE)
			return gte(c3clause.getField(), c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.LT)
			return lt(c3clause.getField(), c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.LE)
			return lte(c3clause.getField(), c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.IN)
			return QueryBuilder.in(c3clause.getField(), (List<String>) c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.CONTAINS)
			return QueryBuilder.contains(c3clause.getField(), c3clause.getValue());
		else if (c3clause.getClauseType() == ClauseType.NOT_CONTAINS)
			return QueryBuilder.contains(c3clause.getField(), c3clause.getValue());
		else
			throw new NotImplementedException();
	}
	
//	public Clause clauseToC3Clause(Clause clause) {
//		if (clause. get ClauseType() == ClauseType.EQ)
//			return eq(c3clause.getField(), c3clause.getValue());
//		else
//			throw new NotImplementedException();
//	}
	
	@Override
	public void close() {
		getCluster().close();
		getSession().close();
	}
}
