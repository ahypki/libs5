package net.hypki.libs5.db.cassandra;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;

import org.apache.cassandra.service.StorageServiceMBean;

public class JmxBulkLoader {

	private JMXConnector connector;
	private StorageServiceMBean storageBean;

	public JmxBulkLoader(String host, int port) throws Exception {
		connect(host, port);
	}

	private void connect(String host, int port) throws IOException, MalformedObjectNameException {
		JMXServiceURL jmxUrl = new JMXServiceURL(String.format("service:jmx:rmi:///jndi/rmi://%s:%d/jmxrmi", host, port));
		Map<String, Object> env = new HashMap<>();
		
		LibsLogger.debug(JmxBulkLoader.class, "JMX connecting to ", jmxUrl);
		
		connector = JMXConnectorFactory.connect(jmxUrl, env);
		MBeanServerConnection mbeanServerConn = connector.getMBeanServerConnection();
		ObjectName name = new ObjectName("org.apache.cassandra.db:type=StorageService");
		storageBean = JMX.newMBeanProxy(mbeanServerConn, name, StorageServiceMBean.class);
	}

	public void close() throws IOException {
		connector.close();
	}

	public void bulkLoad(String path) {
		storageBean.bulkLoad(path);
	}

	public static void main(String[] args) throws Exception {
		final String path = ArgsUtils.getString(args, "path");
		final String host = ArgsUtils.getString(args, "host", "localhost");
		final int port = ArgsUtils.getInt(args, "port", 7199);
		
		assertTrue(notEmpty(path), "Expected --path argument");
		assertTrue(notEmpty(host), "Expected --host argument");
		assertTrue(port > 0, "Expected --port argument");

		// JMX port is specified in conf/cassandra-env.sh
		JmxBulkLoader np = new JmxBulkLoader(host, port);
//		for (String arg : args) {
			LibsLogger.debug(JmxBulkLoader.class, "Importing files from path: " + path + " to " + host + ":" + port);
			np.bulkLoad(path);
//		}
		np.close();
		LibsLogger.debug(JmxBulkLoader.class, "Bulk load from path: " + path + " to " + host + ":" + port + " done successfully");
	}
}