package net.hypki.libs5.db.cassandra;

import java.io.File;
import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Time;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.GsonStreamWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;

import com.datastax.driver.core.TableMetadata;


public final class BackupUtils {

	private BackupUtils() {
		
	}
	
	private static File buildFilename(final String keyspace, File backupDirectory, TableMetadata cf, Time time) {
		return new File(new FileExt(backupDirectory).getDirectoryOnly(), keyspace + "_" + time.toString() + "_" + cf.getName() + ".json");
	}
	
	public static void backupCfWithMapReduce(final String keyspace, File backupDirectory, TableMetadata cf, Time time) throws Exception {
		File file = buildFilename(keyspace, backupDirectory, cf, time);
		ToolRunner.run(new Configuration(), new BackupCfMapReduce(), new String[] {cf.getName(), file.getAbsolutePath()});
	}
	
	public static void backupKeyspaceWithMapReduce(final String keyspace, File backupDirectory) throws Exception {
		Time time = new Time();
		for (TableMetadata cf : (Iterable<TableMetadata>) ((CassandraProvider) DbObject.getDatabaseProvider()).getColumnFamilyListDefinitions(keyspace)) {
			backupCfWithMapReduce(keyspace, backupDirectory, cf, time);
		}
	}
	
	public static void backupCfWithIterator(final String keyspace, File backupDirectory, TableMetadata cf, Time time) throws IOException {
//		throw new NotImplementedException();

		File file = buildFilename(keyspace, backupDirectory, cf, time);
		Watch start = new Watch();
////		boolean isFile = cf.getName().equals(BlobFile.COLUMN_FAMILY);
//		
		GsonStreamWriter writer = new GsonStreamWriter(file.getAbsolutePath());
		writer.beginArray();
		
		int count = 0;
		CassandraProvider cass = new CassandraProvider();
//		for (String tableName : cass.getTableList(keyspace)) {
			for (Row row : cass.tableIterable(keyspace, cf.getName())) {
				writer.append(row.toJsonElement());
				count++;
			}
//		}
//		
////		for (HColumn<String, String> column : new ColumnFamilyHColumnIterator(keyspace, cf)) {
////			writer.append(column.getName(), JsonUtils.readJson(column.getValue()));
////			
//////			if (isFile) {
//////				BlobFile mf = JsonUtils.fromJson(column.getValue(), BlobFile.class);
//////				mf.saveBlobToFile(new FileExt(backupDirectory.getAbsolutePath(), "blobs", mf.getFileId().toString()));
//////			}
////			
////			count++;
////		}
//		
		writer.endArray();
		writer.close();
		LibsLogger.debug(BackupUtils.class, cf.getName() + " backup made in " + start + " to file " + backupDirectory.getAbsolutePath() + ". Saved " + count + " objects.");
	}
	
	public static void backupKeyspaceWithIterator(final String keyspace, File backupDirectory, TableMetadata ... skip) throws IOException {
		Time time = new Time();
		
		CassandraProvider cass = (CassandraProvider) DbObject.getDatabaseProvider();
		
		for (TableMetadata cfDef : (Iterable<TableMetadata>) cass.getColumnFamilyListDefinitions(keyspace)) {
			
			boolean skipCF = false;
			if (skip != null) {
				for (TableMetadata cfToSkip : skip) {
					if (cfDef.getName().equals(cfToSkip.getName())) {
						LibsLogger.debug(BackupUtils.class, "Skipping CF " + cfDef.getName());
						skipCF = true;
						break;
					}
				}
			}
			
			if (skipCF == false)
				backupCfWithIterator(keyspace, backupDirectory, cfDef, time);
		}
		
		LibsLogger.debug(BackupUtils.class, "Keyspace backup made in " + (System.currentTimeMillis() - time.getMs()) + " ms");
	}
}
