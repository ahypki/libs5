package net.hypki.libs5.db.cassandra;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.GsonStreamWriter;
import net.hypki.libs5.utils.json.JsonUtils;

import org.apache.cassandra.hadoop.ColumnFamilyInputFormat;
import org.apache.cassandra.hadoop.ColumnFamilyOutputFormat;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.utils.ByteBufferUtil;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;

import com.datastax.driver.core.TableMetadata;

public class BackupCfMapReduce extends Configured implements Tool {
	
	public static final String KEYSPACE_NAME = "KEYSPACE_NAME";
	public static final String CF_NAME = "CF_NAME";
	public static final String BACKUP_FILENAME = "BACKUP_FILENAME";
	
	private static String writerFilename = null;
	private static GsonStreamWriter writer = null;
	
	private static CassandraProvider cassandraProvider = null;
	
	private static /*synchronized*/ GsonStreamWriter getWriter() {
//		if (writer == null) {
//			new GsonStreamWriter(backupFilename.getAbsolutePath());
//		}
		return writer;
	}
	
	public static class TokenizerMapper extends Mapper<ByteBuffer, SortedMap<ByteBuffer, ByteBuffer>, Text, Text> {

		protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException, InterruptedException {
			
		}

		public void map(ByteBuffer key, SortedMap<ByteBuffer, ByteBuffer> columns, Context context) throws IOException, InterruptedException {
			final String keyspace = context.getConfiguration().get(KEYSPACE_NAME);
			TableMetadata cf = cassandraProvider.getColumnFamilyDefinition(keyspace, context.getConfiguration().get(CF_NAME));
			
//			boolean isMetafile = cf.getName().equals(BlobFile.COLUMN_FAMILY);
			String base = new FileExt(writerFilename).getDirectoryOnly();
			
			for (Entry<ByteBuffer, ByteBuffer> col : columns.entrySet()) {
//				String colKey = ByteBufferUtil.string(col.getKey());
				String value = ByteBufferUtil.string(col.getValue());
//				context.write(new Text("toBackup"), new Text(value));
				
				getWriter().append(JsonUtils.parseJson(value.toString()));
				
//				if (isMetafile) {
//					BlobFile mf = JsonUtils.fromJson(value.toString(), BlobFile.class);
//					mf.saveBlobToFile(new FileExt(base, "blobs", mf.getFileId().toString()));
//				}
			}
		}
	}


	public static class ReducerToCassandra extends Reducer<Text, Text, ByteBuffer, List<Mutation>> {

		protected void setup(org.apache.hadoop.mapreduce.Reducer.Context context) throws IOException, InterruptedException {
			
		}

		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
//			ColumnFamilyDefinition cf = CassandraObject.getColumnFamilyDefinition(context.getConfiguration().get(CF_NAME));
//			boolean isMetafile = cf.getName().equals(MetaFile.COLUMN_FAMILY);
//			File backupFilename = new File(context.getConfiguration().get(BACKUP_FILENAME));
//			String base = new FileExt(backupFilename).getDirectoryOnly();
//			GsonStreamWriter writer = new GsonStreamWriter(backupFilename.getAbsolutePath());
			
//			writer.beginArray();
//			for (Text value : values) {
//				writer.append(JsonUtils.readJson(value.toString()));
				
//				if (isMetafile) {
//					MetaFile mf = CassandraObject.getGson().fromJson(value.toString(), MetaFile.class);
//					mf.saveBlobToFile(new FileExt(base, "blobs", mf.getFileId().toString()));
//				}
//			}
//			writer.endArray();
//			writer.close();
		}
	}

	public int run(String[] args) throws Exception {
		final String keyspace = ArgsUtils.getString(args, "keyspace");
		final String cf = ArgsUtils.getString(args, "columnFamily");
		final String output = ArgsUtils.getString(args, "output");
		
		assertTrue(notEmpty(keyspace), "--keyspace arg is empty");
		assertTrue(notEmpty(cf), "--columnFamily arg is empty");
		assertTrue(notEmpty(output), "--output arg is empty");
		
		if (writer != null)
			throw new RuntimeException(BackupCfMapReduce.class.getSimpleName() + " can be used only by one thread at once");
		
		LibsLogger.debug(BackupCfMapReduce.class, "Creating writer for CF " + output + "...");
		writerFilename = output;
		writer = new GsonStreamWriter(writerFilename);
		writer.beginArray();
		
		long start = System.currentTimeMillis();
		cassandraProvider = (CassandraProvider) DbObject.getDatabaseProvider();
		TableMetadata cfName = cassandraProvider.getColumnFamilyDefinition(keyspace, cf);
		Job job = new Job(getConf(), getClass().getName());
		
		job.setJarByClass(BackupCfMapReduce.class);
		job.setMapperClass(TokenizerMapper.class);

		job.setReducerClass(ReducerToCassandra.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(ByteBuffer.class);
		job.setOutputValueClass(List.class);

		job.setInputFormatClass(ColumnFamilyInputFormat.class);
		job.setOutputFormatClass(ColumnFamilyOutputFormat.class);
		
		job.getConfiguration().set(KEYSPACE_NAME, keyspace);
		job.getConfiguration().set(CF_NAME, cf);
		job.getConfiguration().set(BACKUP_FILENAME, output);
		
		ConfigHelper.setInputRpcPort(job.getConfiguration(), JsonUtils.readString(Settings.getSettings(), "Cassandra/port"));
		ConfigHelper.setInputInitialAddress(job.getConfiguration(), JsonUtils.readString(Settings.getSettings(), "Cassandra/host"));
		ConfigHelper.setInputPartitioner(job.getConfiguration(), "org.apache.cassandra.dht.RandomPartitioner");
		ConfigHelper.setInputColumnFamily(job.getConfiguration(), keyspace, cfName.getName());
		ConfigHelper.setOutputColumnFamily(job.getConfiguration(), keyspace, cfName.getName());
//			SlicePredicate predicate = new SlicePredicate().setColumn_names(Arrays.asList(ByteBufferUtil.bytes(columnName)));
		SlicePredicate predicate = new SlicePredicate().setSlice_range(new SliceRange(ByteBufferUtil.bytes(""), ByteBufferUtil.bytes(""), false, Integer.MAX_VALUE));
		ConfigHelper.setInputSlicePredicate(job.getConfiguration(), predicate);

		job.waitForCompletion(true);
		LibsLogger.debug(BackupCfMapReduce.class, "CF " + cfName.getName() + " backup made in " + (System.currentTimeMillis() - start) + " ms");
		
		LibsLogger.debug(BackupCfMapReduce.class, "Closing writer...");
		writer.endArray();
		writer.close();
		writer = null;
		writerFilename = null;
			
		return 0;
	}
}
