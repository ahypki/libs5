package net.hypki.libs5.db.cassandra;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.LibsLogger;

import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.Deletion;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.utils.ByteBufferUtil;
import org.apache.hadoop.io.Text;

public class MapReduceUtils {
	
	public static byte[] getBytes(ByteBuffer bb) {
		return ByteBufferUtil.getArray(bb);
	}
	
	public static ByteBuffer toByteBuffer(String tmp) {
		return ByteBufferUtil.bytes(tmp);
	}
	
	public static ByteBuffer toByteBuffer(long l) {
		return ByteBufferUtil.bytes(l);
	}
	
	public static Text toText(ByteBuffer bb) {
		return new Text(bb.array());
	}
	
	public static Text toText(String s) {
		try {
			if (s == null)
				return null;
			return new Text(s.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			LibsLogger.error(MapReduceUtils.class, "Cannot convert to utf8", e);
			return null;
		}
	}
	
	public static String toString(ByteBuffer bb) {
		try {
			return ByteBufferUtil.string(bb);
		} catch (CharacterCodingException e) {
			LibsLogger.debug(MapReduceUtils.class, "Cannot convert ByteBuffer to String", e);
			return null;
		}
	}
	
	public static org.apache.cassandra.thrift.Mutation deleteMutation(String ... columnNames) {
		Deletion deletion = new Deletion();
		SlicePredicate slicePredicate = new SlicePredicate();

		List<ByteBuffer> colNames = new ArrayList<>();
		for (String colName : columnNames) {
			colNames.add(ByteBufferUtil.bytes(colName));
		}
		slicePredicate.setColumn_names(colNames);
		slicePredicate.setColumn_namesIsSet(true);
		
		deletion.setPredicate(slicePredicate);
		deletion.setPredicateIsSet(true);
		deletion.setTimestamp(System.currentTimeMillis());
		deletion.setTimestampIsSet(true);
		
		org.apache.cassandra.thrift.Mutation m = new org.apache.cassandra.thrift.Mutation();
		m.setDeletion(deletion);
		m.setDeletionIsSet(true);
		return m;
	}
	
//	public static org.apache.cassandra.thrift.Deletion deleteMutation(String column) {
//		Deletion deletion = new Deletion();
//	    SlicePredicate slicePredicate = new SlicePredicate();
////	    slicePredicate.addToColumn_names(StringSerializer.get().toByteBuffer("c_name"));
//	    deletion.setPredicate(slicePredicate);
//	    return deletion;
////	    batchMutate.addDeletion("key1", columnFamilies, deletion);
//	    
////		org.apache.cassandra.thrift.Deletion m = new org.apache.cassandra.thrift.Deletion();
////		m. column_or_supercolumn = new ColumnOrSuperColumn();
////		m.column_or_supercolumn.column = c;
////		return m;
//	}
	
	public static org.apache.cassandra.thrift.Mutation createMutation(String column, String value) {
		Column c = new Column();
		c.name = ByteBuffer.wrap(column.getBytes());
		c.value = ByteBufferUtil.bytes(value);
		c.setTimestamp(System.currentTimeMillis() * 1000);
		org.apache.cassandra.thrift.Mutation m = new org.apache.cassandra.thrift.Mutation();
		m.column_or_supercolumn = new ColumnOrSuperColumn();
		m.column_or_supercolumn.column = c;
		return m;
	}
	
	public static org.apache.cassandra.thrift.Mutation createMutation(String column, byte[] value) {
		Column c = new Column();
		c.name = ByteBuffer.wrap(column.getBytes());
		c.setValue(value);
		c.setTimestamp(System.currentTimeMillis() * 1000);
		org.apache.cassandra.thrift.Mutation m = new org.apache.cassandra.thrift.Mutation();
		m.column_or_supercolumn = new ColumnOrSuperColumn();
		m.column_or_supercolumn.column = c;
		return m;
	}
}
