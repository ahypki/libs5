package net.hypki.libs5.db.cassandra;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.schema.Column;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.UrlUtilities;

import org.javatuples.Pair;

public class UpdatePigStatement {

	private String keyspace = null;
	
	private String table = null;
	
	private List<String> columns = null;
	
	public UpdatePigStatement() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(String.format("UPDATE %s.%s SET ", getKeyspace().toLowerCase(), getTable()));
		for (int i = 0; i < getColumns().size(); i++) {
			if (i > 0)
				s.append(", ");
			s.append(getColumns().get(i));
			s.append(" = ?");
		}
		LibsLogger.debug(UpdatePigStatement.class, "Update statement: ", s.toString());
		return UrlUtilities.UrlEncode(s.toString());
	}

	public String getKeyspace() {
		return keyspace;
	}

	public UpdatePigStatement setKeyspace(String keyspace) {
		this.keyspace = keyspace;
		return this;
	}

	public String getTable() {
		return table;
	}

	public UpdatePigStatement setTable(String table) {
		this.table = table;
		return this;
	}

	public List<String> getColumns() {
		if (columns == null)
			columns = new ArrayList<>();
		return columns;
	}

	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
	
	public UpdatePigStatement addColumn(String columnName) {
		getColumns().add(columnName);
		return this;
	}
	
	public UpdatePigStatement addColumns(List<Column> columns) {
		for (Column column : columns) {
			if (!column.isPrimaryKey())
				addColumn(column.getName());
		}
		return this;
	}
}
