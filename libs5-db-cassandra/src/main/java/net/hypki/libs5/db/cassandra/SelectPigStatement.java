package net.hypki.libs5.db.cassandra;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.schema.Column;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.UrlUtilities;

import org.javatuples.Pair;

public class SelectPigStatement {

	private String keyspace = null;
	
	private String table = null;
	
	private List<String> columns = null;
	
	public SelectPigStatement() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		if (getColumns() == null || getColumns().size() == 0)
			s.append(" * ");
		else {
			for (int i = 0; i < getColumns().size(); i++) {
				if (i > 0)
					s.append(", ");
				s.append(getColumns().get(i));
	//			s.append(" = ?");
			}
		}
//		s.append(String.format(" FROM %s.%s", getKeyspace().toLowerCase(), getTable()));
//		s.append(String.format(" FROM %s.%s where PK > 0 ", getKeyspace().toLowerCase(), getTable()));
//		s.append(String.format(" FROM %s.%s where pk > 0 ", getKeyspace().toLowerCase(), getTable()));
//		s.append(String.format(" FROM %s.%s where PK > 0 AND token(PK) > ? and token(PK) <= ?", getKeyspace().toLowerCase(), getTable()));
		s.append(String.format(" FROM %s.%s where pk > 0 AND token(pk) > ? and token(pk) <= ?", getKeyspace().toLowerCase(), getTable()));
		LibsLogger.debug(SelectPigStatement.class, "Select statement: ", s.toString());
		return UrlUtilities.UrlEncode(s.toString());
	}

	public String getKeyspace() {
		return keyspace;
	}

	public SelectPigStatement setKeyspace(String keyspace) {
		this.keyspace = keyspace;
		return this;
	}

	public String getTable() {
		return table;
	}

	public SelectPigStatement setTable(String table) {
		this.table = table;
		return this;
	}

	public List<String> getColumns() {
		if (columns == null)
			columns = new ArrayList<>();
		return columns;
	}

	public SelectPigStatement setColumns(List<String> columns) {
		this.columns = columns;
		return this;
	}
	
	public SelectPigStatement addColumn(String columnName) {
		getColumns().add(columnName);
		return this;
	}
	
	public SelectPigStatement addColumns(List<Column> columns) {
		for (Column column : columns) {
			if (!column.isPrimaryKey())
				addColumn(column.getName());
		}
		return this;
	}
}
