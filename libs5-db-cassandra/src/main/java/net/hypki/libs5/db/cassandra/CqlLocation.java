package net.hypki.libs5.db.cassandra;

import static java.lang.String.format;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.UrlUtilities;

public class CqlLocation {

	private String keyspace = null;
	
	private TableSchema tableSchema = null;
	
	private int splitSize = -1;
	
	private int pageSize = -1;
	
	private boolean withUpdateStatement = false;
	
	private String whereClause = null;
	
	public CqlLocation() {
		
	}
	
	@Override
	public String toString() {
		assertTrue(notEmpty(getKeyspace()), 	"Keyspace is not set in ", CqlLocation.class.getSimpleName());
		assertTrue(getTableSchema() != null, 	"Table is not set in ", CqlLocation.class.getSimpleName());
		 
		return format("cql://%s/%s?reuse_address=true&init_address=%s&native_port=%s&partitioner=%s%s%s%s%s", 
				getKeyspace().toLowerCase(), 
				getTableSchema().getName(), 
				((CassandraProvider) DbObject.getDatabaseProvider()).getCassandraHost(), 
//				"localhost",
				String.valueOf(((CassandraProvider) DbObject.getDatabaseProvider()).getCassandraRpcPort()), 
				"org.apache.cassandra.dht.Murmur3Partitioner",
				getSplitSize() > 0 ? "&split_size=" + getSplitSize() : "",
				getPageSize() > 0 ? "&page_size=" + getPageSize() : "",
				isWhereClauseDefined() ? "&where_clause=" + UrlUtilities.UrlEncode(getWhereClause()) : "",
				isWithUpdateStatement() ? "&output_query=" + new UpdatePigStatement()
					.setKeyspace(getKeyspace())
					.setTable(getTableSchema().getName())
					.addColumns(getTableSchema().getColumns())
					.toString() : ""
					);
	}

	public String getKeyspace() {
		return keyspace;
	}

	public CqlLocation setKeyspace(String keyspace) {
		this.keyspace = keyspace;
		return this;
	}

	public TableSchema getTableSchema() {
		return tableSchema;
	}

	public CqlLocation setTableSchema(TableSchema tableSchema) {
		this.tableSchema = tableSchema;
		return this;
	}

	public int getSplitSize() {
		return splitSize;
	}

	public CqlLocation setSplitSize(int splitSize) {
		this.splitSize = splitSize;
		return this;
	}

	public int getPageSize() {
		return pageSize;
	}

	public CqlLocation setPageSize(int pageSize) {
		this.pageSize = pageSize;
		return this;
	}

	public boolean isWithUpdateStatement() {
		return withUpdateStatement;
	}

	public CqlLocation setWithUpdateStatement(boolean withUpdateStatement) {
		this.withUpdateStatement = withUpdateStatement;
		return this;
	}

	public String getWhereClause() {
		return whereClause;
	}

	public CqlLocation setWhereClause(String whereClause) {
		this.whereClause = whereClause;
		return this;
	}
	
	public boolean isWhereClauseDefined() {
		return notEmpty(getWhereClause());
	}
}
