package net.hypki.libs5.storage.hadoop;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.storage.StorageProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class HdfsStorageProvider implements StorageProvider {
	
	private FileSystem filesystem = null;
	
	public static String getHadoopPath() throws IOException {
		// TODO move to init()
		return JsonUtils.readString(Settings.getSettings(), "StorageProvider/hadoopPath");
	}
	
	@Override
	public void init(Map<String, Object> params) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutdown() {
		if (filesystem != null) {
			try {
				filesystem.close();
			} catch (IOException e) {
				LibsLogger.error(getClass(), "Cannot close fs", e);
			}
			filesystem = null;
		}
	}
	
	private FileSystem getFileSystem() {
		if (filesystem == null) {
			try {
				long start = System.currentTimeMillis();
				
				// first shutdown if needed
				shutdown();
				
				String hadoopPath = getHadoopPath();
				
				Configuration conf = new Configuration();
				conf.addResource(new Path(FileUtils.concatPaths(hadoopPath, "conf", "core-site.xml"))); 
				conf.addResource(new Path(FileUtils.concatPaths(hadoopPath, "conf", "hdfs-site.xml")));
				
				filesystem = FileSystem.get(conf);
				
				LibsLogger.debug("HDFS Storage provider started in " + (System.currentTimeMillis() - start));
			} catch (IOException e) {
				LibsLogger.error(getClass(), "Cannot connect to HDFS", e);
				filesystem = null;
			}
		}
		return filesystem;
	}
	
	@Override
	public long save(String set, UUID fileId, InputStream dataStream, MetaList metaList) throws IOException {
		FSDataOutputStream out = null;
		try {
			out = getFileSystem().create(getHdfsPath(set, fileId));
			byte[] buffer = new byte[1024 * 1024];
			int readBytes = 0;
			while ((readBytes = dataStream.read(buffer)) > 0)
				out.write(buffer, 0, readBytes);
//			IOUtils.copyBytes(dataStream, out, null);
			return readBytes;
		} finally {
			if (out != null)
				out.close();
		}
	}
	
	@Override
	public boolean remove(String set, UUID fileId) throws IOException {
		return getFileSystem().delete(getHdfsPath(set, fileId), false);
	}
	
	/**
	 * Method is not optimal, use it only for testing. It reads maximum of 15 MB of the file.
	 */
	@Override
	public InputStream read(String set, UUID fileId) throws IOException {
		FSDataInputStream in = null;
		try {
			in = getFileSystem().open(getHdfsPath(set, fileId));
			
			return in.getWrappedStream();
		} finally {
			if (in != null)
				in.close();
		}
	}
	
	@Override
	public long size(String set, UUID fileId) throws IOException {
		// TODO check it
		return getFileSystem().getLength(getHdfsPath(set, fileId));
	}
	
	@Override
	public boolean exist(String set, UUID fileId) throws IOException {
		return getFileSystem().exists(getHdfsPath(set, fileId));
	}

	public static Path getHdfsPath(String set, UUID fileId) {
		StringBuilder builder = new StringBuilder(100);
		builder.append("/" + set + "/");
		builder.append(fileId.toString());
		return new org.apache.hadoop.fs.Path(builder.toString());
	}
}
