package net.hypki.libs5.storage.hadoop.unittests;

import static net.hypki.libs5.storage.hadoop.HdfsStorageProvider.getHadoopPath;

import java.io.IOException;
import java.io.InputStream;

import jodd.io.StreamUtil;
import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.storage.hadoop.HdfsStorageProvider;
import net.hypki.libs5.utils.string.ByteUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit test for simple HdfsStorageProvider.
 */
public class HdfsStorageProviderUnitTest extends LibsTestCase {
	
	public static final String theFilename = "hello.txt";
	public static final String message = "Hello, world!\n";
	
	@BeforeClass
	public static void beforeClass() {
		System.setProperty("storageSettings", "settings-storage-hadoop.json");
	}
	
	@Test
	public void testFile() throws IOException {

		Configuration conf = new Configuration();
		conf.addResource(new Path(getHadoopPath(), "/conf/core-site.xml"));
	    conf.addResource(new Path(getHadoopPath(), "/conf/hdfs-site.xml"));
		
		FileSystem fs = FileSystem.get(conf);

		Path filenamePath = new Path(theFilename);

		try {
			if (fs.exists(filenamePath)) {
				// remove the file first
				fs.delete(filenamePath);
			}

			FSDataOutputStream out = fs.create(filenamePath);
			out.writeUTF(message);
			out.close();

			FSDataInputStream in = fs.open(filenamePath);
			String messageIn = in.readUTF();
			System.out.print(messageIn);
			in.close();
		} catch (IOException ioe) {
			System.err.println("IOException during operation: " + ioe.toString());
			System.exit(1);
		}
	}
	
	@Test
	public void testWriteRead() throws IOException {
		final String set = "libs5";
		UUID userId = UUID.random();
		HdfsStorageProvider hdfs = new HdfsStorageProvider();
		
		String inputContent = "one\ntwo three";
		WeblibsPath path = new WeblibsPath("/test/hello.txt");
		byte [] inputBytes = inputContent.getBytes();
		
		// saving file
		hdfs.save(set, userId, ByteUtils.toStream(inputBytes), null);
		
		// reading file
		InputStream readContent = hdfs.read(set, userId);
		String readContentString = new String(ByteUtils.toByte(readContent));
		
		// checking content
		assertTrue(readContentString.equals(inputContent));
		
		// checking if file exists
		assertTrue(hdfs.exist(set, userId));
		
		// to check file content in Linux console type: hadoop dfs -cat /test/hello.txt
	}
}
