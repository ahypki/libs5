package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpSetAttr extends Op {

	public OpSetAttr(String jQuerySelector, String attrName, String attrValue) {
		super("setattr");
		
		set("jQuerySelector", jQuerySelector);
		set("name", attrName);
		set("value", Base64Utils.encode(attrValue));
	}
}
