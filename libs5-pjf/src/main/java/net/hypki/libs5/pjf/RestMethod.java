package net.hypki.libs5.pjf;

public enum RestMethod {
	POST,
	GET,
	DELETE
}
