package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpSet extends Op {

	public OpSet(String jQuerySelector, String html) {
		super("set");
		
		set("jQuerySelector", jQuerySelector);
		set("html", html != null ? Base64Utils.encode(html) : "");
	}
}
