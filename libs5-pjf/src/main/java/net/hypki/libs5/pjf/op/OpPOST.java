package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.UrlUtilities;

public class OpPOST extends Op {

	public OpPOST(String url, Object ... params) {
		super("post");
		
		set("url", url);
		set("params", UrlUtilities.toQueryString(params));
	}
}
