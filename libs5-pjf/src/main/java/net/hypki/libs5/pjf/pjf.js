var DEBUG = true;
var timers = {};
var thisDialog;
var PJF_VERSION = "1.0.0";

function pjfOps(ops, confirmation) {
//	debug("ops count " + ops.length);
	if (confirmation != null) {
		pjfConfirmation(confirmation, function() {
			pjfOps(ops);
		});
	} else {
		for (var i = 0; i < ops.length; i++) {
			var op = ops[i];
			try {
				if (op.type == "ajax") {
					
					if (op.confirmation != null) {
						pjfConfirmation(op.confirmation, function() {
							pjfAjax(op.url, op.params, pjfOps, false, op.restMethod, op.paramsJS);
						});
					} else {
						pjfAjax(op.url, op.params, pjfOps, false, op.restMethod, op.paramsJS);
					}
					
				} else if (op.type == "get") {
					
					if (op.confirmation != null) {
						pjfConfirmation(op.confirmation, function() {
							pjfAjax(op.url, op.params, pjfOps, false, "GET", op.paramsJS);
						});
					} else {
						pjfAjax(op.url, op.params, pjfOps, false, "GET", op.paramsJS);
					}
	
				} else if (op.type == "post") {
					
					if (op.confirmation != null) {
						pjfConfirmation(op.confirmation, function() {
							pjfAjax(op.url, op.params, pjfOps, false, "POST", op.paramsJS);
						});
					} else {
						pjfAjax(op.url, op.params, pjfOps, false, "POST", op.paramsJS);
					}
					
				} else if (op.type == "delete") {
					
					if (op.confirmation != null) {
						pjfConfirmation(op.confirmation, function() {
							pjfAjax(op.url, op.params, pjfOps, "DELETE");
						});
					} else {
						pjfAjax(op.url, op.params, pjfOps, "DELETE");
					}
					
				} else if (op.type == "js") {
					
					debug("[pjf] eval: ", b64_to_utf8(op.command));
					eval(b64_to_utf8(op.command));
					
				} else if (op.type == "react") {
					
//					debug("[pjf] react: ", b64_to_utf8(op.command));
//					const codee = b64_to_utf8(op.command).replace('"use strict";', "").trim();
//
//					debug("[pjf] react: ", codee);
//					eval(codee);
//					var rr = ReactDOM.createRoot(document.getElementById("some_random_id"));
//					rr.render(<Garage />);

					var q = document.getElementById(op.jQuerySelector);
				    if (q) q.innerHTML = b64_to_utf8(op.command);
					
				} else if (op.type == "msg") {
					
					if (op.severity == "info") {
						pjfInfo(op.title, op.msg, op.iconUrl);
					} else if (op.severity == "error") {
						pjfError(op.title, op.msg, op.iconUrl);
					} else if (op.severity == "warn") {
						pjfWarn(op.title, op.msg, op.iconUrl);
					}
					
				} else if (op.type == "redirect") {
					
					window.location = op.url + (op.params != undefined ? "?" + op.params : "");
					
				} else if (op.type == "form-post") {
					
					post_to_url(op.url, op.params);
					
				} else if (op.type == "class-set") {
					
					$(op.jQuerySelector).attr("class", op.clazz);
					
				} else if (op.type == "class-add") {
					
					$(op.jQuerySelector).addClass(op.clazz);
					debug("class " + op.clazz + " added to " + op.jQuerySelector);
					
				} else if (op.type == "class-remove") {
					
					$(op.jQuerySelector).removeClass(op.clazz);
					debug("class " + op.clazz + " removed from " + op.jQuerySelector);
					
				} else if (op.type == "check") {
					
					$(op.jQuerySelector).attr('checked', "checked");
					
				} else if (op.type == "uncheck") {
					
					$(op.jQuerySelector).removeAttr('checked');
					
				} else if (op.type == "check-toggle") {

					$(op.jQuerySelector).click();//.attr('checked', !$(op.jQuerySelector).attr("checked"));
					
				} else if (op.type == "replace") {
					
					$(op.jQuerySelector).replaceWith(b64_to_utf8(op.html));
					debug("[pjfOps] ", op.jQuerySelector, " replaced");
					
				} else if (op.type == "enable") {
					
					$(op.jQuerySelector).prop('disabled', false);
					
				} else if (op.type == "disable") {
					
					$(op.jQuerySelector).prop('disabled', true);
					
				} else if (op.type == "toggle") {
					
					$(op.jQuerySelector).toggle();
					
				} else if (op.type == "toggle-class") {
					
					$(op.jQuerySelector).toggleClass(op.clazz);
					
				} else if (op.type == "show") {
					
					$(op.jQuerySelector).show();
					debug(op.jQuerySelector + " shown");
					
				} else if (op.type == "hide") {
					
					$(op.jQuerySelector).hide();
					debug(op.jQuerySelector + " hidden");
					
				} else if (op.type == "remove") {
					
					$(op.jQuerySelector).remove();
					debug(op.jQuerySelector + " removed");
					
				} else if (op.type == "cookie-remove") {
					
					eraseCookie(op.name);
				} else if (op.type == "cookie-create") {
					
					createCookie(op.name, op.value, op.days);
					
				} else if (op.type == "set") {
					
					$(op.jQuerySelector).html(b64_to_utf8(op.html));
					
				} else if (op.type == "set-readonly") {
					
					$(op.jQuerySelector).attr("readonly", "true");
					
				} else if (op.type == "remove-readonly") {
					
					$(op.jQuerySelector).removeAttr("readonly");
					
				} else if (op.type == "setattr") {
					
					$(op.jQuerySelector).attr(op.name, b64_to_utf8(op.value));
					
				} else if (op.type == "remove-attr") {
					
					$(op.jQuerySelector).removeAttr(op.name);
					
				} else if (op.type == "val") {
					
					$(op.jQuerySelector).val(op.value);
					
				} else if (op.type == "val-append") {
					
					$(op.jQuerySelector).val($(op.jQuerySelector).val() + ' ' + op.value);
					
				} else if (op.type == "after") {
					
					$(op.jQuerySelector).after(b64_to_utf8(op.html));
	
				} else if (op.type == "before") {
					
					$(op.jQuerySelector).before(b64_to_utf8(op.html));
					
				} else if (op.type == "append") {
					
					$(op.jQuerySelector).append(b64_to_utf8(op.html));
					
				} else if (op.type == "prepend") {
					
					$(op.jQuerySelector).prepend(b64_to_utf8(op.html));
					
				} else if (op.type == "set-data") {
					
					$(op.jQuerySelector).data(op.name, op.value);
					
				} else if (op.type == "resize") {
					
					var width = $(op.jQuerySelector).width();
					var height = $(op.jQuerySelector).height();
	//				console.log("sizes: " + width + "  " + height);
					$(op.jQuerySelector).width(width + op.resizeX);
					$(op.jQuerySelector).height(height + op.resizeY);
	//				var width = $(op.jQuerySelector).width();
	//				var height = $(op.jQuerySelector).height();
	//				console.log("sizes: " + width + "  " + height);
					
				} else if (op.type == "timer") {
					
					pjfTimer(op.jQuerySelector, op.timerId, op.url, op.ms, op.restMethod, op.params, op.paramsJS);
	
				} else if (op.type == "dialog-close") {
					
//					alert(window.thisDialog);
					window.thisDialog.dialog('destroy').remove();
					
				} else if (op.type == "dialog") {
					var _onOK = op.onOK;
					var _additionalParams = op.params != null ? "&" + op.params : "";
					var buttonsList = null;
					if (!isEmpty(_onOK)) {
						buttonsList = {
							"Save": {
								visible : false,
								text : op.onOKBtnCaption,
								click : function() {
									var thisDialog = $(this);
									paramsJS = thisDialog.find(":input").serialize();
									params = "";
									if (!isEmpty(paramsJS)) {
										paramsJS.split("&").forEach(function(entry) {
	//										debug(entry);
											parts = entry.split("=");
										    params = (isEmpty(params) ? "" : params + "&") + parts[0] + "=" + encodeURIComponent(parts[1]);
										  }, this);
									}
									pjfAjax(_onOK, params + "&" + _additionalParams, function(modalOps) {
										pjfOps(modalOps);
										for (var i = 0; i < modalOps.length; i++) {
											if (modalOps[i].type == "dialog-close")
												thisDialog.dialog('destroy').remove();
										}
									}, true, op.restMethod, null);
								},
								class: "btn btn-small btn-success btn-line"
							},
							"Cancel": {
								text : op.onCancelBtnCaption,
								click : function() {
									$(this).dialog('destroy').remove();
								},
								class: "btn btn-small btn-default btn-line"
							}
						};
					} else {
						buttonsList = {
							"OK": {
								text : op.onCancelBtnCaption,
								click : function() {
									$(this).dialog('destroy').remove();
								},
								class: "btn btn-small btn-default btn-line"
							}
						};
					}
					window.thisDialog = $(b64_to_utf8(op.html)).dialog({
						width: op.minWidth,
						height: op.minHeight,
						modal: true,
						title: op.title,
						buttons: buttonsList,
						close: function() {
							var thisDialog = $(this);
							thisDialog.dialog('destroy').remove();
						}
					});
				} else if (op.type == "dialogMsg") {
					$(b64_to_utf8(op.html)).dialog({
						width: op.minWidth,
						height: 480,
						modal: true,
						title: op.title,
						buttons: {
							"OK": {
								text : "Close",
								click : function() {
									$(this).dialog('destroy').remove();
								},
								class: "btn btn-small btn-default btn-line"
							}
						},
						close: function() {
							
						}
					});
				} else {
					error('Unknown case ' + JSON.stringify(op) + ' has occurred: ');
				}
			} catch (e) {
				error('An error in op ' + JSON.stringify(op) + ' has occurred: ' + e);
			}
		}
	}
}

function urldecode(url) {
  return decodeURIComponent(url.replace(/\+/g, ' '));
}

function pjfAjax(url, paramsStatic, onOKFunction, waitForAjax, restMethod, paramsJS) {
	debug("[pjfAjax] Ajax url= ", url, " paramsStatic= ", paramsStatic, " waitForAjax= ", waitForAjax, " restMethod= ", restMethod, " paramsJS= ", b64_to_utf8(paramsJS));
	
	debug("paramsJS dialog Save ", paramsJS);
//	debug("paramsJS dialog Save utf8_decode", utf8_decode(paramsJS));
	
	params = "";
	if (!isEmpty(paramsStatic))
		params = paramsStatic;
	if (!isEmpty(paramsJS)) {
		eval(b64_to_utf8(paramsJS)).split("&").forEach(function(entry) {
//			debug(entry);
			parts = entry.split("=");
		    params = (isEmpty(params) ? "" : params + "&") + parts[0] + "=" + encodeURIComponent(parts[1]);
		    debug("[pjfAjax] added " + parts[0] + "=" + parts[1]);
		  }, this);
	}
	
	if (!isEmpty(readCookie("userId"))) {
		params = params + "&userIdCookie=" + readCookie("userId");
	    debug("[pjfAjax] added: " + "&userIdCookie=" + readCookie("userId"));
	}
	
	if (!isEmpty(readCookie("lc"))) {
		params = params + "&lc=" + readCookie("lc");
	    debug("[pjfAjax] added: " + "&lc=" + readCookie("lc"));
	}
	debug("[pjfAjax] added lc cookie: " + "&lc=" + readCookie("lc"));
	
	debug("[pjfAjax] cookies: ", document.cookie);
	debug("[pjfAjax] Ajax url= ", url, " params= ", params, " restMethod= ", restMethod);
		
	pjfShowProgress();
	asyncBool = (waitForAjax != undefined && waitForAjax == true ? false : true);
	try {
		$.ajax({
			type : (restMethod == null ? "POST" : restMethod), 
			cache : false, 
			dataType : "json", 
//	        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			url : (url.indexOf('http') == 0 ? url : "/" + url), 
			data : params, 
//			async: asyncBool,
			success : function(resp) {
				if (resp.result == "FAIL") {
					try {
						error("Ajax function failed: ", resp);
						pjfOps(resp);
					} catch (e) {
						error("Ajax function exception: ", xhr, textStatus, error);
						throw e;
					}
				} else {
					if (onOKFunction != null) {
						onOKFunction(resp);
					}
				}
				pjfHideProgress();
			},
			error : function(xhr, textStatus, error) {
				try {
					err("Ajax call failed: ", xhr, textStatus, error);
					if (xhr.responseText)
						pjfOps(jQuery.parseJSON(xhr.responseText));
					
					// call this ajax later in 30 seconds
					info("pjfAjax: Ajax call postponed 30 s...");
					window.setTimeout(function() {
						pjfAjax(url, paramsStatic, onOKFunction, waitForAjax, restMethod, paramsJS);
					}, 
					30000);
				} catch (e) {
					err("Ajax call exception: ", xhr, textStatus, error);
					throw e;
				}
				pjfHideProgress();
			}
		});
	} catch (error) {
		err("Error catch: ", error);
		// expected output: ReferenceError: nonExistentFunction is not defined
		// Note - error messages will vary depending on browser
	} finally {
//		pjfHideProgress();
	}
}

function pjfTimer(jQSelector, id, url, ms, restMethod, paramsStatic, paramsJS) {
	if (restMethod === undefined || restMethod.length == 0)
		restMethod = "POST";
	
	debug("[pjfTimer] jQSelector= ", jQSelector, ", id= ", id, url, ms, restMethod, paramsStatic, paramsJS);
	
	if (timers[id] != null) {
		// stopping old timer
		window.clearTimeout(timers[id]);
		timers[id] = null;
	}
	
	if ($(jQSelector).length == 0) {
		debug("Element ", jQSelector, " does not exist, consuming timer...");
		return;
	}
	
	timers[id] = window.setTimeout(function() {
						var jQS = jQSelector;
						var timerId = id;
						
						debug("[pjfTimer] jQS= ", jQS, ", timerId= ", timerId, url, ms, restMethod, paramsStatic, paramsJS);
						
						var currTime = new Date().getTime();
						var allowed = ((currTime - $(jQS).data(timerId+'-prevTime')) > ms ? true : false);
						
//						params = "";
//						if (!isEmpty(paramsStatic))
//							params = paramsStatic;
//						if (!isEmpty(paramsJS)) {
//							eval(b64_to_utf8(paramsJS)).split("&").forEach(function(entry) {
////								debug(entry);
//								parts = entry.split("=");
//							    params = (isEmpty(params) ? "" : params + "&") + parts[0] + "=" + encodeURIComponent(parts[1]);
//							  }, this);
//						}
//						debug("[pjfAjax] Ajax url= ", url, " params= ", params, " restMethod= ", restMethod);
						
//						debug("[pjfTimer] params= ", params);
						if (allowed || $(jQS).data(timerId+'-prevTime') == null) {
							$(jQS).data(timerId+'-prevTime', currTime);
							pjfAjax(url, paramsStatic, pjfOps, false, restMethod, paramsJS);
						} else {
							debug("[pjfTimer] jQS= ", jQS, ", timerId= ", timerId, url, ms, restMethod, 
								paramsStatic, paramsJS, allowed, timerId+'-prevTime', $(jQS).data(timerId+'-prevTime'));
						}
						;
					}, 
					ms);

}

function pjfConfirmation(options, onYesFunction) {
	var dialog = $("<div>")
		.addClass("dialog-confirmation")
		.attr("title", b64_to_utf8(options.title))
		.append($("<p>")
			.addClass("comment")
			.html(b64_to_utf8(options.msg)))
		;
	$(dialog).dialog({
		height: (options.height != null ? options.height : 280),
		width: (options.width != null ? options.width : 480),
		modal: true,
		buttons: {
			"Yes": {
			 click: function() {
				 onYesFunction();
				 $( this ).dialog( "close" );
			 },
			 class: "btn btn-small btn-success btn-line",
			 text: (options.yesBtnName != null ? options.yesBtnName : "Yes")
			},
			"No": {
			 click: function() {
				 $( this ).dialog( "close" );
			 } ,
			 class: "btn btn-small btn-default btn-line",
			 text: (options.noBtnName != null ? options.noBtnName : "No")
			}
		}
	});
}

function pjfWarn(title, message, icon) {
	if (message === undefined || message.length == 0)
		message = " ";
	if (icon != null)
		$.gritter.add({title: title, text: message, image: icon});
	else
		$.gritter.add({title: title, text: message, image: 'static/pjf/alert.png'});
}

function pjfInfo(title, message, icon) {
	if (message === undefined || message.length == 0)
		message = " ";
	if (icon != null)
		$.gritter.add({title: title, text: message, image: icon});
	else
		$.gritter.add({title: title, text: message, image: 'static/pjf/OK.png'});
}

function pjfError(title, message, icon) {
	if (message === undefined || message.length == 0)
		message = " ";
	if (icon != null)
		$.gritter.add({title: title, text: message, image: icon});
	else
		$.gritter.add({title: title, text: message, image: 'static/pjf/FAIL.png'});
}

function showMessagesWithGritter() {
    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            if (name_value[0].substr(0, 7) === "msgInfo") {
            	pjfInfo("Info", decodeURIComponent(name_value[1]).replace(/\+/g, " "));
            	eraseCookie(name_value[0]);
            } else if (name_value[0].substr(0, 8) === "msgError") {
            	pjfError("Error", decodeURIComponent(name_value[1]).replace(/\+/g, " "));
            	eraseCookie(name_value[0]);
            }
        }
    }
}

function isEmpty(str) {
    return (str == null || str === undefined || !str || 0 === str.length);
}

function post_to_url(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
//    console.log(form);
    form.submit();
}

function getPageParam(name) {
  var regexS = "/" + name + "/([\\w]+)";//"[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent( str )));
}

function b64_to_utf8(str) {
	if (str == null || str === undefined)
		return null;
    return decodeURIComponent(escape(window.atob( str )));
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	} else
		var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function isCookie(name) {
	return readCookie(name) != null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function pjfShowProgress() {
	$("#progress-icon").show();
}

function pjfHideProgress() {
	$("#progress-icon").hide();
}

function debug() {
	if (DEBUG) console.log("DEBUG " + arrToStr(arguments));
}

function info() {
	console.log("INFO " + arrToStr(arguments));
}

function err() {
	console.log("ERROR " + arrToStr(arguments));
}

function error() {
	console.log("ERROR " + arrToStr(arguments));
}

function arrToStr(arguments) {
	var tmp = "";
	for(var arg = 0; arg < arguments.length; ++ arg)
		tmp = tmp + arguments[arg] + " ";
	return tmp;
}

var delay = ( function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();