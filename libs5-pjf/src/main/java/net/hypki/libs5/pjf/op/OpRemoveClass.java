package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpRemoveClass extends Op {

	public OpRemoveClass(final String jQuerySelector, final String clazz) {
		super("class-remove");
		
		set("jQuerySelector", jQuerySelector);
		set("clazz", clazz);
	}
}
