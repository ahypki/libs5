package net.hypki.libs5.pjf.op;


public class OpUncheck extends Op {

	public OpUncheck(final String jQuerySelector) {
		super("uncheck");
		
		set("jQuerySelector", jQuerySelector);
	}
}
