package net.hypki.libs5.pjf.op;

public class OpRemoveReadonly extends Op {

	public OpRemoveReadonly(final String jQuerySelector) {
		super("remove-readonly");
		
		set("jQuerySelector", jQuerySelector);
	}
}
