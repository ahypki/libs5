package net.hypki.libs5.pjf.op;

import static net.hypki.libs5.weblibs.WeblibsConst.COOKIE_MSG_INFO;
import jodd.servlet.UrlEncoder;

public class OpInfoCookie extends Op {

	public OpInfoCookie(final String message) {
		super("cookie-create");
		
		set("name", COOKIE_MSG_INFO);
		set("value", UrlEncoder.encode(message));
		set("days", 1);
	}
}
