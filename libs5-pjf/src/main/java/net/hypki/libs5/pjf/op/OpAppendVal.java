package net.hypki.libs5.pjf.op;


public class OpAppendVal extends Op {

	public OpAppendVal(final String jQuerySelector, final String value) {
		super("val-append");
		
		set("jQuerySelector", jQuerySelector);
		set("value", value);
	}
	
	public OpAppendVal(final String jQuerySelector, final int value) {
		super("val-append");
		
		set("jQuerySelector", jQuerySelector);
		set("value", value);
	}
}
