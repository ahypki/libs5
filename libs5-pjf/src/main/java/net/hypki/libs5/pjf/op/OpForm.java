package net.hypki.libs5.pjf.op;

import com.google.gson.JsonObject;

public class OpForm extends Op {

	public OpForm(final String url, final Object ... params) {
		super("form-post");
		
		set("url", url);
		JsonObject obj = new JsonObject();
		for (int i = 0; i < params.length; i = i + 2) {
			if (params[i + 1] instanceof String)
				obj.addProperty((String) params[i], (String) params[i + 1]);
			else if (params[i + 1] instanceof Long)
				obj.addProperty((String) params[i], (Long) params[i + 1]);
			else if (params[i + 1] instanceof Integer)
				obj.addProperty((String) params[i], (Integer) params[i + 1]);
			else
				obj.addProperty((String) params[i], params[i + 1].toString());
		}
		set("params", obj);
	}
}
