package net.hypki.libs5.pjf.op;


public class OpCookieCreate extends Op {

	public static final String OP_COOKIE_CREATE = "cookie-create";

	public OpCookieCreate(final String name, final String value, final int days) {
		super(OP_COOKIE_CREATE);
		
		set("name", name);
		set("value", value);
		set("days", days);
	}
}
