package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpError extends Op {
	
	public OpError(String message) {
		super("msg");
		
		set("severity", "error");
		set("title", "Error");
		set("msg", message);
	}

	public OpError(String title, String message) {
		super("msg");
		
		set("severity", "error");
		set("title", title);
		set("msg", message);
	}
	
	public OpError(String title, String message, final String iconUrl) {
		super("msg");
		
		set("severity", "error");
		set("title", title);
		set("msg", message);
		if (iconUrl != null)
			set("iconUrl", iconUrl);
	}
}
