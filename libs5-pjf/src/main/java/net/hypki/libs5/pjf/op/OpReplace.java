package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpReplace extends Op{
	
	public OpReplace(final String jQuerySelector, final String html) {
		super("replace");
		
		set("jQuerySelector", jQuerySelector);
		set("html", Base64Utils.encode(html));
	}
}
