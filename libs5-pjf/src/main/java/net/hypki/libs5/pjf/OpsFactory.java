package net.hypki.libs5.pjf;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.UrlUtilities;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class OpsFactory {
	
	public static JsonElement opConfirmation(String title, String msg) {
		JsonObject c = new JsonObject();
		c.addProperty("title", Base64Utils.encode(title));
		c.addProperty("msg", Base64Utils.encode(msg));
		return c;
	}
	
	public static JsonElement opConfirmation(String title, String msg, int width, int height) {
		JsonObject c = new JsonObject();
		c.addProperty("title", Base64Utils.encode(title));
		c.addProperty("msg", Base64Utils.encode(msg));
		c.addProperty("width", width);
		c.addProperty("height", height);
		return c;
	}
	
	public static JsonElement opConfirmation(String title, String msg, int width, int height, String yesBtnName, String noBtnName) {
		JsonObject c = new JsonObject();
		c.addProperty("title", Base64Utils.encode(title));
		c.addProperty("msg", Base64Utils.encode(msg));
		c.addProperty("width", width);
		c.addProperty("height", height);
		c.addProperty("yesBtnName", yesBtnName);
		c.addProperty("noBtnName", noBtnName);
		return c;
	}
	
	public static String opsCall(Object ... ops) {
		boolean first = true;
		StringBuilder arr = new StringBuilder("pjfOps([");
		for (Object op : ops) {
			if (op == null)
				continue;
			
			if (first == false)
				arr.append(",");
			
			if (op instanceof JsonElement) {
				JsonElement element = (JsonElement) op;
				arr.append(element.toString());
			} else if (op instanceof OpList) {
				for (Op op2 : (OpList) op) {
					if (op2 == null)
						continue;
					
					if (first == false)
						arr.append(",");
					
					arr.append(op2.toString());
					first = false;
				}
			} else {
				arr.append(op.toString());
			}
			first = false;
		}
		arr.append("]);");
		return arr.toString().replaceAll("\\\"", "'");
	}
}
