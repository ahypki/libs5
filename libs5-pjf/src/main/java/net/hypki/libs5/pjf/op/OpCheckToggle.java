package net.hypki.libs5.pjf.op;

public class OpCheckToggle extends Op {

	public OpCheckToggle(final String jQuerySelector) {
		super("check-toggle");
		
		set("jQuerySelector", jQuerySelector);
	}
}
