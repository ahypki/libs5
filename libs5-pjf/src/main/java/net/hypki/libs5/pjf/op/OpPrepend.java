package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpPrepend extends Op {

	public OpPrepend(final String jQuerySelector, final String html) {
		super("prepend");
		
		set("jQuerySelector", jQuerySelector);
		set("html", Base64Utils.encode(html));
	}
}
