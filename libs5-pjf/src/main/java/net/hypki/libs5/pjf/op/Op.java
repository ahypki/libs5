package net.hypki.libs5.pjf.op;

import java.util.HashMap;
import java.util.Map;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

public class Op {
	
	@Expose
	private String type = null;
	
	@Expose
	// TODO change it to class Params
	private Map<String, Object> params = new HashMap<>();
	
	public Op() {
		
	}
	
	public Op(String type) {
		setType(type);
	}

	public JsonElement getJson() {
		JsonObject msg = new JsonObject();
		
		msg.addProperty("type", getType());
		
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			if (entry.getValue() instanceof String)
				msg.addProperty(entry.getKey(), (String) entry.getValue());
			else if (entry.getValue() instanceof Boolean)
				msg.addProperty(entry.getKey(), (Boolean) entry.getValue());
			else if (entry.getValue() instanceof Number)
				msg.addProperty(entry.getKey(), (Number) entry.getValue());
			else if (entry.getValue() instanceof JsonElement)
				msg.add(entry.getKey(), (JsonElement) entry.getValue());
			else if (StringUtilities.nullOrEmpty(entry.getValue()))
				msg.addProperty(entry.getKey(), "");
			else
				msg.add(entry.getKey(), JsonUtils.toJson(entry.getValue()));
//				throw new NotImplementedException();
		}
		
		return msg;
	}
	
	@Override
	public String toString() {
		return getJson().toString();
	}
	
	public String toStringOnclick() {
		return toStringOnclick(true);
	}
	
	public String toStringOnclick(boolean withSemicolon) {
		if (withSemicolon == false) {
			String tmp = OpsFactory.opsCall(this);
			return tmp.substring(0, tmp.length() - 1);
		}
		return OpsFactory.opsCall(this);
	}
	
	public String toStringAdhock() {
		return "<script type=\"text/javascript\">" + this.toStringOnclick() +  "</script>";
	}
	
	//TODO there is set() and addParamParam - ugly!
	public Op set(String key, Object value) {
		params.put(key, value);
		return this;
	}
	
	public Map<String, Object> getParams() {
		return params;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Op addParamParam(String key, Object value) {
		// TODO here I parse url encoded string, should I decode it in the Params() constructor?
		Params params = new Params((String) getParams().get("params"));
		params.add(key, value);
		set("params", params.toString(true));
		return this;
	}
}
