package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpToggleClass extends Op {

	public OpToggleClass(String jQuerySelector, String clazz) {
		super("toggle-class");
		
		set("jQuerySelector", jQuerySelector);
		set("clazz", clazz);
	}
}
