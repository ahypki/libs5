package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpHide extends Op {

	public OpHide(String jQuerySelector) {
		super("hide");
		
		set("jQuerySelector", jQuerySelector);
	}
}
