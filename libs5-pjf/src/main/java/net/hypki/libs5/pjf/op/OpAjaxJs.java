package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.UrlUtilities;

import com.google.gson.JsonElement;

public class OpAjaxJs extends Op {
	
	public OpAjaxJs(String url, String paramsAsJs, JsonElement confirmation, RestMethod restMethod) {
		super("ajax");
		
		set("restMethod", restMethod.toString());
		set("url", url);
		set("params", "");
		set("paramsJS", Base64Utils.encode(paramsAsJs));
		if (confirmation != null)
			set("confirmation", confirmation);
	}

	public OpAjaxJs(String url, String paramsAsJs, RestMethod restMethod) {
		super("ajax");
		
		set("restMethod", restMethod.toString());
		set("url", url);
		set("params", "");
		set("paramsJS", Base64Utils.encode(paramsAsJs));
	}
	
	public OpAjaxJs(String url, String paramsAsJs, JsonElement confirmation) {
		super("ajax");
		
		set("restMethod", RestMethod.POST.toString());
		set("url", url);
		set("params", "");
		set("paramsJS", Base64Utils.encode(paramsAsJs));
		if (confirmation != null)
			set("confirmation", confirmation);
	}
	
	public OpAjaxJs(String url, String paramsAsJs) {
		super("ajax");
		
		set("restMethod", RestMethod.POST.toString());
		set("url", url);
		set("params", "");
		set("paramsJS", Base64Utils.encode(paramsAsJs));
	}
}
