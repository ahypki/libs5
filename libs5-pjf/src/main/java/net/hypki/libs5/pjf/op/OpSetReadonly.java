package net.hypki.libs5.pjf.op;

public class OpSetReadonly extends Op {

	public OpSetReadonly(final String jQuerySelector) {
		super("set-readonly");
		
		set("jQuerySelector", jQuerySelector);
	}
}
