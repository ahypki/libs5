package net.hypki.libs5.pjf.op;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.RestMethod;

public class OpTimer extends Op {

	public OpTimer(final String urlToCall, final String jQToSerialize, final int delayMs, final RestMethod restMethod) {
		super("timer");
		
		set("url", urlToCall);
		set("jQuerySelector", jQToSerialize);
		set("timerId", UUID.random().getId());
		set("ms", delayMs);
		set("restMethod", restMethod.toString());
	}
}
