package net.hypki.libs5.pjf.op;

import static net.hypki.libs5.weblibs.WeblibsConst.COOKIE_MSG_ERROR;
import jodd.servlet.UrlEncoder;

public class OpErrorCookie extends Op {

	public OpErrorCookie(final String message) {
		super("cookie-create");
		
		set("name", COOKIE_MSG_ERROR);
		set("value", UrlEncoder.encode(message));
		set("days", 1);
	}
}
