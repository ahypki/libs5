package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.url.UrlUtilities;

public class OpRedirect extends Op {

	public OpRedirect(final String url, Object ... params) {
		super("redirect");
		
		set("url", url);
		if (params != null)
			set("params", UrlUtilities.toQueryString(params));
	}
	
	public OpRedirect(final String url) {
		super("redirect");
		
		set("url", url);
	}
}
