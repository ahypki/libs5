package net.hypki.libs5.pjf.utils;

import java.io.IOException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.TrickyString;

public class HtmlToRenderSnake {
	public static void main(String[] args) throws IOException {
		final String htmlFile = "HtmlToRenderSnake.html";
		final String htmlContent = new FileExt(htmlFile).readString();
		
		RS rs = new RS()
					.parse(new TrickyString(htmlContent));
		
		LibsLogger.info(HtmlToRenderSnake.class, rs.toString());
	}
}
