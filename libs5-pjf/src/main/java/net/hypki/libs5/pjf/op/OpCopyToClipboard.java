package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpCopyToClipboard extends OpJs {

	/**
	 * https://www.w3schools.com/howto/howto_js_copy_clipboard.asp
	 * @param id
	 */
	public OpCopyToClipboard(final String id) {
		super("var $temp = $('<input>'); "
									+ "$('body').append($temp); "
									+ "$temp.val($('#" + id + "').html()).select(); "
									+ "document.execCommand('copy'); "
									+ "$temp.remove();");
//		super("var copyText = document.getElementById(\"" + id + "\");"
//				+ "copyText.select();"
//				+ "copyText.setSelectionRange(0, 99999);"
//				+ "document.execCommand(\"copy\");"
////				+ "alert(\"Copied the text: \" + copyText.value);"
//				);
	}
	
	
}
