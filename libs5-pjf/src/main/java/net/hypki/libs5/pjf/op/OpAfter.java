package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpAfter extends Op {

	public OpAfter(final String jQuerySelector, final String html) {
		super("after");
		
		set("jQuerySelector", jQuerySelector);
		set("html", Base64Utils.encode(html));
	}
}
