package net.hypki.libs5.pjf.op;



public class OpInfo extends Op {
	
	public OpInfo(final String message) {
		super("msg");
		
		set("severity", "info");
		set("title", "OK");
		set("msg", message);
	}

	public OpInfo(final String title, final String message) {
		super("msg");
		
		set("severity", "info");
		set("title", title);
		set("msg", message);
	}
	
	public OpInfo(final String title, final String message, final String iconUrl) {
		super("msg");
		
		set("severity", "info");
		set("title", title);
		set("msg", message);
		if (iconUrl != null)
			set("iconUrl", iconUrl);
	}
	
}
