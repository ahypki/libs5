package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpSetData extends Op {

	public OpSetData(final String jQuerySelector, final String dataName, final String dataValue) {
		super("set-data");
		
		set("jQuerySelector", jQuerySelector);
		set("name", dataName);
		set("value", Base64Utils.encode(dataValue));
	}
}
