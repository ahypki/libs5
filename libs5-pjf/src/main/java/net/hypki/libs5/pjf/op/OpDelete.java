package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.url.UrlUtilities;

public class OpDelete extends Op {

	public OpDelete(final String url, Object ... params) {
		super("delete");
		
		set("url", url);
		set("params", UrlUtilities.toQueryString(params));
	}
}
