package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;

public class OpDialogOpen extends Op {
	
	private static final String OP_DIALOG_OPEN = "dialog";
	
	private String onOKBtnCaption = "Save";
	
	private String onCancelBtnCaption = "Cancel";

	public OpDialogOpen(String urlOnOK, Params params, String restMethod, String title, String html, int width, int height) {
		super(OP_DIALOG_OPEN);
		
		set("minWidth", width);
		if (height > 0)
			set("minHeight", height);
		set("onOK", urlOnOK);
		set("html", Base64Utils.encode(html.startsWith("<div") ? html : "<div>" + html + "</div>"));
		set("title", title);
		if (params != null)
			set("params", params.toString(true));
		set("restMethod", restMethod);
		set("onOKBtnCaption", getOnOKBtnCaption());
		set("onCancelBtnCaption", getOnCancelBtnCaption());
	}
	
	public OpDialogOpen(String urlOnOK, String title, String html, int width, int height) {
		super(OP_DIALOG_OPEN);
		
		set("minWidth", width);
		if (height > 0)
			set("minHeight", height);
		set("onOK", urlOnOK);
		set("html", Base64Utils.encode(html.startsWith("<div") ? html : "<div>" + html + "</div>"));
		set("title", title);
		set("onOKBtnCaption", getOnOKBtnCaption());
		set("onCancelBtnCaption", getOnCancelBtnCaption());
	}
	
	public OpDialogOpen(String urlOnOK, String title, String html) {
		super(OP_DIALOG_OPEN);
		
		set("minWidth", 480);
		set("minHeight", 480);
		set("onOK", urlOnOK);
		set("html", Base64Utils.encode(html.startsWith("<div") ? html : "<div>" + html + "</div>"));
		set("title", title);
		set("onOKBtnCaption", getOnOKBtnCaption());
		set("onCancelBtnCaption", getOnCancelBtnCaption());
	}

	public String getOnOKBtnCaption() {
		return onOKBtnCaption;
	}

	public OpDialogOpen setOnOKBtnCaption(String onOKBtnCaption) {
		this.onOKBtnCaption = onOKBtnCaption;
		set("onOKBtnCaption", onOKBtnCaption);
		return this;
	}

	public String getOnCancelBtnCaption() {
		return onCancelBtnCaption;
	}

	public OpDialogOpen setOnCancelBtnCaption(String onCancelBtnCaption) {
		this.onCancelBtnCaption = onCancelBtnCaption;
		set("onCancelBtnCaption", onCancelBtnCaption);
		return this;
	}
}
