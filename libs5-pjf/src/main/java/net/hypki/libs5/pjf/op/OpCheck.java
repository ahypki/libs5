package net.hypki.libs5.pjf.op;

public class OpCheck extends Op {

	public OpCheck(final String jQuerySelector) {
		super("check");
		
		set("jQuerySelector", jQuerySelector);
	}
}
