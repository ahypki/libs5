package net.hypki.libs5.pjf.op;


public class OpSetVal extends Op {

	public OpSetVal(final String jQuerySelector, final String value) {
		super("val");
		
		set("jQuerySelector", jQuerySelector);
		set("value", value);
	}
	
	public OpSetVal(final String jQuerySelector, final int value) {
		super("val");
		
		set("jQuerySelector", jQuerySelector);
		set("value", value);
	}
}
