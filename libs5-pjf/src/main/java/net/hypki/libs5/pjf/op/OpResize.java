package net.hypki.libs5.pjf.op;

public class OpResize extends Op {

	/**
	 * If > 1.0 then the size increases, if < 1.0 then descreases
	 * @param resize
	 * @return
	 */
	public OpResize(final String jQuerySelector, final int addX, final int addY) {
		super("resize");
		
		set("jQuerySelector", jQuerySelector);
		set("resizeX", addX);
		set("resizeY", addY);
	}
}
