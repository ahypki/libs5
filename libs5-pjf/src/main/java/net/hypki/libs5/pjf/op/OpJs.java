package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpJs extends Op {

	public OpJs(final String jsCode) {
		super("js");
		
		set("command", Base64Utils.encode(jsCode));
	}
	
	public OpJs(final String jsCode, final int timeoutMs) {
		super("js");
		
		set("command", Base64Utils.encode(timeoutMs > 0 ? "window.setTimeout(function() { " + jsCode + "; }, " + timeoutMs + ");" : jsCode));
	}
	
	protected OpJs setJs(String js) {
		set("command", Base64Utils.encode(js));
		return this;
	}
}
