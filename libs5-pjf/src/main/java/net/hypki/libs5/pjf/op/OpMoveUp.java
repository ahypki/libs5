package net.hypki.libs5.pjf.op;

import static java.lang.String.format;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpMoveUp extends OpJs {

	public OpMoveUp(String thisJQS, String thisClass) {
		super(format("var $current = $('%s').closest('%s'); var $previous = $current.prev('%s'); if($previous.length !== 0){ $current.insertBefore($previous); };", thisJQS, thisClass, thisClass));
	}
}
