package net.hypki.libs5.pjf;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;

public class HtmlSpecial {

	public static final String ASTERIX = "&lowast;"; 
	public static final String BLACK_DIAMOND = "&diams;";
	public static final String DOTS = "&#8230;";
	public static final String X_IN_CIRCLE = "&#8855;";
	public static final String BULLET = "&bull;";
	public static final String MDASH = "&mdash;";
	public static final String SPACE = "&nbsp;";
	
	public static final int KEYCODE_ENTER = 13;
	public static final int KEYCODE_ESCAPE = 27;
	
	public static String ifKeyCode(int code, OpList ops) {
		return "if (event.keyCode == " + code + ") { " + ops.toStringOnclick() + " }; ";
	}
	
	public static String ifKeyCodeWithCtrl(int code, OpList ops) {
		return "if (event.ctrlKey && event.keyCode == " + code + ") { " + ops.toStringOnclick() + " }; ";
	}
	
	public static String ifKeyCodeWithCtrl(int code, Op op) {
		return "if (event.ctrlKey && event.keyCode == " + code + ") { " + op.toStringOnclick() + " }; ";
	}
	
	public static String ifKeyCode(int code, Op op) {
		return "if (event.keyCode == " + code + ") { " + op.toStringOnclick() + " }; ";
	}
}
