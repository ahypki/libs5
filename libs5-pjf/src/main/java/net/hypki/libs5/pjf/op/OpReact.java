package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpReact extends Op {

	public OpReact(String jQuerySelector, final String jsCode) {
		super("react");
		
		set("jQuerySelector", jQuerySelector);
		set("command", Base64Utils.encode(jsCode));
	}
	
	public OpReact(final String jsCode, final int timeoutMs) {
		super("react");
		
		set("command", Base64Utils.encode(timeoutMs > 0 ? "window.setTimeout(function() { " + jsCode + "; }, " + timeoutMs + ");" : jsCode));
	}
	
}
