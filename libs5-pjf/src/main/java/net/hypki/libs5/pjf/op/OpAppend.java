package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpAppend extends Op {

	public OpAppend(final String jQuerySelector, final String html) {
		super("append");
		
//		msg.addProperty("type", OP_APPEND);
		set("jQuerySelector", jQuerySelector);
		set("html", Base64Utils.encode(html));
	}
}
