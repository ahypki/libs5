package net.hypki.libs5.pjf.op;



public class OpWarn extends Op {

	public OpWarn(final String title, final String message) {
		super("msg");
		
		set("severity", "warn");
		set("title", title);
		set("msg", message);
	}
	
	public OpWarn(final String title, final String message, final String iconUrl) {
		super("msg");
		
		set("severity", "warn");
		set("title", title);
		set("msg", message);
		if (iconUrl != null)
			set("iconUrl", iconUrl);
	}
	
}
