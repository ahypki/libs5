package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpEnable extends Op {

	public OpEnable(String jQuerySelector) {
		super("enable");
		
		set("jQuerySelector", jQuerySelector);
	}
}
