package net.hypki.libs5.pjf.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.utils.string.TrickyString;

public class RS {
	
	private String name = null;

	private List<RS> children = new ArrayList<>();
	
	private Map<String, String> attr = new HashMap<>();
	
	private String text = null;
	
	public RS() {
		
	}
	
	public RS(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RS> getChildren() {
		return children;
	}

	public void setChildren(List<RS> children) {
		this.children = children;
	}

	public RS parse(TrickyString ts) {
		while (!ts.isEmpty()) {
			if (ts.checkLine().trim().isEmpty()) {
				ts.getLine();
			} else if (ts.startsWith("<!--")) {
				// skipping comment
				ts.getNextStringUntil("-->", true);
			} else if (ts.checkNextMatch("\\<[\\w\\d]+") != null) {
				// new DOM element
				String t = ts.getNextMatch("\\<[\\w\\d]+");
				RS rs = new RS(t.replace("<", ""));
				
				// reading aatributes
				while (true) {
					if (ts.checkNextMatch("[\\w]+[\\s]*=[\\s]*[\\\"\\']") != null) {
						String attrName = ts.getNextAlphanumericWordWithMinus();
						ts.expectedPrintableCharacter("=");
						String sep = ts.getNextPrintableCharacter(); // " or '
						String attrValue = ts.getNextStringUntil(sep, false);
						ts.getNextPrintableCharacter();
						rs.getAttr().put(attrName, attrValue);
					} else if (ts.checkNextPrintableCharacter().equals(">")) {
						ts.expectedPrintableCharacter(">");
						getChildren().add(rs);
						rs.parse(ts);
					} else {
						ts.expectedPrintableCharacter("<");
						ts.expectedPrintableCharacter("/");
						ts.expectedAlphanumericWord(rs.getName());
						ts.expectedPrintableCharacter(">");
						return this;
					}
				}
			} else {
				setText(getText() + ts.getNextPrintableCharacter());
			}
		}
		
		return this;
	}

	public Map<String, String> getAttr() {
		return attr;
	}

	public void setAttr(Map<String, String> attr) {
		this.attr = attr;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
