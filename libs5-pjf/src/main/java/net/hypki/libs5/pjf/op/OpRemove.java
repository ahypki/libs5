package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpRemove extends Op {

	public OpRemove(final String jQuerySelector) {
		super("remove");
		
		set("jQuerySelector", jQuerySelector);
	}
}
