package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;

import com.google.gson.JsonElement;

public class OpAjax extends Op {

	public OpAjax(String url, RestMethod restMethod, JsonElement confirmation, Object ... params) {
		super("ajax");
		
		set("restMethod", restMethod.toString());
		set("url", url);
		if (params != null && params.length == 1 && params[0] instanceof Params)
			set("params", ((Params) params[0]).toString(true));
		else
			set("params", UrlUtilities.toQueryString(params));
		if (confirmation != null)
			set("confirmation", confirmation);
	}
	
	public OpAjax(String url, RestMethod restMethod, Object ... params) {
		super("ajax");
		
		set("restMethod", restMethod.toString());
		set("url", url);
		if (params != null && params.length == 1 && params[0] instanceof Params)
			set("params", ((Params) params[0]).toString(true));
		else
			set("params", UrlUtilities.toQueryString(params));
	}
	
	public OpAjax(String url, JsonElement confirmation, Object ... params) {
		super("ajax");
		
		set("restMethod", RestMethod.GET.toString());
		set("url", url);
		if (params != null && params.length == 1 && params[0] instanceof Params)
			set("params", ((Params) params[0]).toString(true));
		else
			set("params", UrlUtilities.toQueryString(params));
		if (confirmation != null)
			set("confirmation", confirmation);
	}
}
