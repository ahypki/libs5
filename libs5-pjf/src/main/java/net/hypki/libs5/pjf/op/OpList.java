package net.hypki.libs5.pjf.op;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.pjf.OpsFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class OpList implements Iterable<Op> {
	
	@Expose
	private List<Op> opList = null;
	
	@Expose
	private List<Op> opListLast = null;

	public OpList() {
		
	}
	
	private List<Op> getOpList() {
		if (opList == null)
			opList = new ArrayList<Op>();
		return opList;
	}
	
	private List<Op> getOpListLast() {
		if (opListLast == null)
			opListLast = new ArrayList<Op>();
		return opListLast;
	}
	
	@Override
	public String toString() {
		return toJsonArray().toString();
	}
	
	public String toStringAdhock() {
		return "<script type=\"text/javascript\">" + this.toStringOnclick() +  "</script>";
	}
	
	@Override
	public Iterator<Op> iterator() {
		ArrayList<Op> all = new ArrayList<Op>();
		all.addAll(getOpList());
		all.addAll(getOpListLast());
		return all.iterator();
	}
		
	public OpList addIf(boolean addIfTrue, Op op) {
		if (addIfTrue)
			add(op);
		return this;
	}
	
	public OpList add(boolean addIfTrue, Op op) {
		if (addIfTrue)
			add(op);
		return this;
	}
	
	public OpList add(boolean addIfTrue, OpList op) {
		if (addIfTrue)
			add(op);
		return this;
	}
	
	public OpList add(Op op) {
		if (op != null)
			getOpList().add(op);
		return this;
	}
	
	public OpList addLast(Op op) {
		if (op != null)
			getOpListLast().add(op);
		return this;
	}
	
	public OpList add(OpList opList) {
		if (opList != null)
			for (Op op : opList)
				getOpList().add(op);
		return this;
	}
	
	public OpList addLast(OpList opList) {
		if (opList != null)
			for (Op op : opList)
				addLast(op);
		return this;
	}
	
	public String toStringOnclick() {
		return OpsFactory.opsCall(this);
	}
	
	public OpList addAll(Collection<? extends Op> c) {
		for (Op op : c)
			add(op);
		return this;
	}
	
	private JsonArray toJsonArray() {
		JsonArray arr = new JsonArray();
		for (Op op : this) {
			arr.add(op.getJson());
		}
		return arr;
	}

	public int size() {
		return (opList != null ? opList.size() : 0) + (opListLast != null ? opListLast.size() : 0);
	}

	public OpList clear() {
		if (opList != null)
			opList.clear();
		if (opListLast != null)
			opListLast.clear();
		return this; 
	}
	
	public OpList addParamParam(String key, Object value) {
		for (Op op : getOpList()) {
			op.addParamParam(key, value);
		}
		for (Op op : getOpListLast()) {
			op.addParamParam(key, value);
		}
		return this;
	}
}
