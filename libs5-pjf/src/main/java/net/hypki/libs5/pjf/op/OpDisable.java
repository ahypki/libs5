package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpDisable extends Op {

	public OpDisable(String jQuerySelector) {
		super("disable");
		
		set("jQuerySelector", jQuerySelector);
	}
}
