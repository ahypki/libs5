package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpDialogMsg extends Op {

	public OpDialogMsg(String title, String html, int width, int height) {
		super("dialogMsg");
		
		set("title", title);
		set("minWidth", width);
		if (height > 0)
			set("minHeight", height);
		set("html", Base64Utils.encode("<div>" + html + "</div>"));
	}
	
	public OpDialogMsg(String title, String html) {
		super("dialogMsg");
		
		set("title", title);
		set("minWidth", 480);
		set("minHeight", 480);
		set("html", Base64Utils.encode("<div>" + html + "</div>"));
	}
}
