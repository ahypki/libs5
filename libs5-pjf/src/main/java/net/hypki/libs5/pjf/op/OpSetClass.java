package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpSetClass extends Op {

	public OpSetClass(final String jQuerySelector, final String clazz) {
		super("class-set");
		
		set("jQuerySelector", jQuerySelector);
		set("clazz", clazz);
	}
}
