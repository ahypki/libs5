package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.url.UrlUtilities;

public class OpGET extends Op {

	public OpGET(final String url, Object ... params) {
		super("get");
		
		set("url", url);
		set("params", UrlUtilities.toQueryString(params));
	}
}
