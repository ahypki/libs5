package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpAddClass extends Op {

	public OpAddClass(final String jQuerySelector, final String clazz) {
		super("class-add");
		
		set("jQuerySelector", jQuerySelector);
		set("clazz", clazz);
	}
}
