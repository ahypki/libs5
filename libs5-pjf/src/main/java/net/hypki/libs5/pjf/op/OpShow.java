package net.hypki.libs5.pjf.op;

import net.hypki.libs5.pjf.OpsFactory;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpShow extends Op {

	public OpShow(String jQuerySelector) {
		super("show");
		
		set("jQuerySelector", jQuerySelector);
	}
}
