package net.hypki.libs5.pjf.op;

public class OpCookieRemove extends Op {

	public OpCookieRemove(final String cookieName) {
		super("cookie-remove");
		
		set("name", cookieName);
	}
}
