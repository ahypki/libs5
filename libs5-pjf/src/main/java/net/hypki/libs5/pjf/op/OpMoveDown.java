package net.hypki.libs5.pjf.op;

import static java.lang.String.format;
import net.hypki.libs5.utils.string.Base64Utils;

public class OpMoveDown extends OpJs {

	public OpMoveDown(String thisJQS, String thisClass) {
		super(format("var $current = $('%s').closest('%s'); var $previous = $current.next('%s'); if($previous.length !== 0){ $current.insertAfter($previous); } ", thisJQS, thisClass, thisClass));
	}
}
