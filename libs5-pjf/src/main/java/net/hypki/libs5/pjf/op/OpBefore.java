package net.hypki.libs5.pjf.op;

import net.hypki.libs5.utils.string.Base64Utils;

public class OpBefore extends Op {

	public OpBefore(final String jQuerySelector, final String html) {
		super("before");
		
		set("jQuerySelector", jQuerySelector);
		set("html", Base64Utils.encode(html));
	}
}
