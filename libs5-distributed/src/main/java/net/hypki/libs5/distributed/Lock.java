package net.hypki.libs5.distributed;

public interface Lock {

	public boolean tryLock(int maxMs);
	
	public void unlock();
}
