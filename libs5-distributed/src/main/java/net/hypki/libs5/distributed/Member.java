package net.hypki.libs5.distributed;

public interface Member {
	
	public String getHost();
	public int getPort();
	
}
