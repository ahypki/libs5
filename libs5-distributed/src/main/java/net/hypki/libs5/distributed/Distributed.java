package net.hypki.libs5.distributed;

import java.util.List;
import java.util.Map;

public interface Distributed {
	
	public List<Member>	getMembers();
	
	public Map getMap(String name);
	
	public Lock getLock(String name);
}
