package net.hypki.libs5.db.memory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.string.MetaList;

public class MemoryDbProvider implements DatabaseProvider {
	
	/**
	 * keyspace -> table -> schema      -- tables
	 * keyspace___table -> key -> row   -- rows
	 */
	private TripleMap<String, String, Object> db = new TripleMap<>();

	@Override
	public MetaList getInitParams() {
		return new MetaList();
	}

	@Override
	public void init(MetaList params) {
		
	}

	@Override
	public void createKeyspace(String keyspace) throws IOException {
		
	}

	@Override
	public void clearKeyspace(String keyspace) throws IOException {
		if (db.containsKey(keyspace))
			db.remove(keyspace);
	}

	@Override
	public List<String> getTableList(String keyspace) throws IOException {
		List<String> tables = new ArrayList<>();
		for (String key : db.keySet())
			if (!key.contains("___"))
				tables.add(key);
		return tables;
	}

	@Override
	public TableSchema getTableSchema(String keyspace, String table) throws IOException {
		return (TableSchema) db.get(keyspace, table);
	}

	@Override
	public void createTable(String keyspace, TableSchema tableSchema) throws IOException {
		db.put(keyspace, tableSchema.getName(), tableSchema);
	}

	@Override
	public void clearTable(String keyspace, String table) throws IOException {
		for (String key : db.keySet())
			if (key.equals(keyspace + "___" + table))
				db.remove(keyspace, table);
	}

	@Override
	public void removeTable(String keyspace, String table) throws IOException {
		clearTable(keyspace, table); // removing rows
		db.remove(keyspace, table); // removing schema
	}

	@Override
	public boolean isTableEmpty(String keyspace, String table) throws IOException {
		for (String key : db.keySet())
			if (key.equals(keyspace + "___" + table))
				return false;
		return true;
	}

	@Override
	public boolean exist(String keyspace, String table, C3Where c3where, String column) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void save(List<Mutation> mutations) throws IOException, ValidationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long countColumns(String keyspace, String table, String key) throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long countRows(String keyspace, String table, C3Where c3where) throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <T> T get(String keyspace, String table, C3Where c3where, String column, Class<T> type) throws IOException {
//		for (String key : db.keySet())
//			if (key.equals(keyspace + "___" + table)) 
		
		
		T t = (T) db.get(keyspace, table);
		return null;
	}

	@Override
	public Object get(String keyspace, String table, C3Where c3where, String column) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(String keyspace, String columnFamily, String key, String keyName, String column)
			throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(String keyspace, String columnFamily, C3Where where) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Results getList(String keyspace, String table, String[] columns, Order order, int count, String state,
			C3Where where) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Row> tableIterable(String keyspace, String table) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

}
