package net.hypki.libs5.utils.string;

import static net.hypki.libs5.utils.utils.NumberUtils.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javatuples.Pair;
import org.javatuples.Triplet;

public class RegexUtils {

	public static final String WORD = "[\\w]+";
	public static final String SPACES = "[\\s]*";
	public static final String STRING = "\\\"[^\\\"]\\\"";
	public static final String DIGITS = "[\\d]+";
	public static final String INTEGER = "[\\+\\-]*[\\d]+";
	public static final String DOUBLE = "[\\+\\-]*" + DIGITS + "\\." + DIGITS + "[eEdD\\+\\-\\d]*";
	public static final String DOUBLE_NONEXPFORM = "[\\+\\-]*" + DIGITS + "\\." + DIGITS;
	public static final String ANY = ".*";
	public static final String COMPARATORS = "[\\>\\<\\=]+";
	public static final String PL_CHARS = "ążźśćęńłóĄŻŹŚĆĘŃŁÓ";
	
	public static String group(String s) {
		return "(" + s + ")";
	}
	
	public static String no(String s) {
		return "[^" + s + "]+";
	}
	
	public static String bracket(String s) {
		return "[" + s + "]+";
	}
	
	public static boolean contains(String regex, String s) {
		return firstGroup("(" + regex + ")", s) != null;// s.matches("^" + ANY + regex + ANY + "$");
	}
	
	public static Triplet<String, String, String> split(String regex, String s) {
		Pattern p = Pattern.compile(group(regex));
		Matcher m = p.matcher(s);
		while (m.find()) {
			String tmp = m.group(1);
			return new Triplet<String, String, String>(s.substring(0, m.start()), tmp, s.length() >= (m.end() + 1) ? s.substring(m.end() + 1) : "");
		}
		return null;
		
//		ArrayList<String> groups = allGroups("^" + group(ANY) + group(regex) + group(ANY) + "$", s);
//		return groups.size() == 3 ? new Triplet<String, String, String>(groups.get(0), groups.get(1), groups.get(2)) : null;
	}

	public static String firstGroup(String regex, String s) {
		return firstGroup(regex, s, null);
	}
	
	public static int firstGroupLocation(String regex, String s, Integer regexFlags) {
		if (regex == null || s == null)
			return -1;
		Pattern p = (regexFlags != null ? Pattern.compile(regex, regexFlags) : Pattern.compile(regex));
		Matcher m = p.matcher(s);
		while (m.find()) {
			return m.start(1);
		}
		return -1;
	}
	
	/**
	 * Returns pair of <the index of the first character matched, the last index of the match>
	 * @param regex
	 * @param s
	 * @return
	 */
	public static Pair<Integer, Integer> indexOf(String regex, String s) {
		Matcher matcher = Pattern.compile(regex).matcher(s);
		return matcher.find() ? 
				new Pair<Integer, Integer>(matcher.start(), matcher.end()) 
				: null;
	}
	
	public static String firstGroupCaseInsensitive(String regex, String s) {
		return firstGroup(regex, s, Pattern.CASE_INSENSITIVE);
	}
	
	public static String firstGroup(String regex, String s, Integer regexFlags) {
		if (regex == null || s == null)
			return null;
		Pattern p = (regexFlags != null ? Pattern.compile(regex, regexFlags) : Pattern.compile(regex));
		Matcher m = p.matcher(s);
		while (m.find()) {
			return m.group(1);
		}
		return null;
	}
	
	public static List<String> allGroups(String regex, String s) {
		return allGroups(regex, s, null);
	}
	
	/**
	 * Flags like Pattern.CASE_INSENSITIVE....
	 * 
	 * Returns at most 1000 groups.
	 * 
	 * @param regex
	 * @param stringToParse
	 * @param regexFlags
	 * @return
	 */
	public static List<String> allGroups(String regex, String stringToParse, Integer regexFlags) {
		Pattern p = (regexFlags != null ? Pattern.compile(regex, regexFlags) : Pattern.compile(regex));
		Matcher m = p.matcher(stringToParse);
		int start = 0;
		ArrayList<String> found = new ArrayList<String>();
		for (int group = 0; group < 1000; group++) {
			if (m.find(start)) {
				for (int i = 1; i <= m.groupCount(); i++) {
					found.add(m.group(i));
				}
				
				start = m.end();
				if (start < stringToParse.length())
					continue;
				
				return found;
			}
		}
		return found;
	}

	public static boolean isDouble(String value) {
		return value != null && value.matches(DOUBLE);
	}
	
	public static boolean isDoubleNonExpForm(String value) {
		return value != null && value.matches(DOUBLE_NONEXPFORM);
	}

	public static boolean isInt(String value) {
		return value != null && value.matches(INTEGER);
	}
	
	public static String removeLeading(String s, String regexToRemove) {
		if (s == null)
			return null;
		String tmp = firstGroup("^(" + regexToRemove + ")", s);
		return tmp != null ? s.substring(tmp.length()) : s;
	}
	
	public static String removeTrailing(String s, String regexToRemove) {
		if (s == null)
			return null;
		String tmp = firstGroup("(" + regexToRemove + ")$", s);
		return tmp != null ? s.substring(0, s.length() - tmp.length()) : s;
	}
	
	public static String trim(String s, String regexToRemove) {
		if (s == null)
			return null;
		return removeTrailing(removeLeading(s, regexToRemove), regexToRemove); 
	}

	public static String replaceFirstGroup(String regex, String str, String replacement) {
		if (regex == null || str == null)
			return null;
		Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(str);
		while (m.find()) {
//			m.group(1)
			return str.substring(0, m.start(1)) + replacement + str.substring(m.end(1));
		}
		return str;
	}
}
