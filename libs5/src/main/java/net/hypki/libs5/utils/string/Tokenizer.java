package net.hypki.libs5.utils.string;

import static net.hypki.libs5.utils.string.StringUtilities.substringMax;

import java.io.IOException;

import org.javatuples.Pair;

public class Tokenizer {
	
	private String s = null;

	public Tokenizer(String s) {
		setS(s);
	}
	
	@Override
	public String toString() {
		return getS();
	}

	private String getS() {
		return s;
	}

	private void setS(String s) {
		this.s = s;
	}

	public String expectedMatchRegex(String regex) throws IOException {
		Pair<Integer, Integer> p = RegexUtils.indexOf(regex, getS());
		
		if (p == null || p.getValue0() != 0)
			throw new IOException("Expected " + regex + ", but string starts with: " + substringMax(getS(), 0, 100));
		
		String ret = getS().substring(p.getValue0(), p.getValue1());
		
		setS(getS().substring(p.getValue1()));
		
		return ret;
	}
	
	public boolean contains(String regex) throws IOException {
		Pair<Integer, Integer> p = RegexUtils.indexOf(regex, getS());
		
		if (p == null || p.getValue0() < 0)
			return false;
		
		return true;
	}
	
	public Tokenizer trim() {
		setS(getS().trim());
		return this;
	}

	public String getAll() {
		return getS();
	}

	public String pullOut(String regex) {
		Pair<Integer, Integer> p = RegexUtils.indexOf(regex, getS());
		
		if (p == null)
			return null;
		
		String ret = getS().substring(p.getValue0(), p.getValue1());
		String before = p.getValue0() > 0 ? getS().substring(0, p.getValue0()) : "";
		String after = p.getValue1() < getS().length() ? getS().substring(p.getValue1()) : "";
		setS(before + after);
		return ret;
	}

	public boolean isEmpty() {
		return StringUtilities.nullOrEmpty(getS());
	}

	public void clear() {
		setS("");
	}
	
	public Tokenizer remove(int from, int length) {
		setS(getS().substring(from + length));
		return this;
	}

	public Tokenizer consumeLeft(String toConsume) {
		if (getS().trim().startsWith(toConsume))
			return remove(0, getS().indexOf(toConsume) + toConsume.length());
		return this;
	}

	public Tokenizer consumeRight(String toConsume) {
		if (getS().trim().endsWith(toConsume))
			return remove(getS().lastIndexOf(toConsume), toConsume.length());
		return this;
	}

	public Tokenizer replaceAll(String regex, String replacement) {
		setS(getS().replaceAll(regex, replacement));
		return this;
	}
	
	public Tokenizer insert(int index, String toInsert) {
		String tmp = "";
		if (index > 0)
			tmp += getS().substring(0, index);
		tmp += toInsert;
		if (index < getS().length())
			tmp += getS().substring(index);
		setS(tmp);
		return this;
	}

	public Tokenizer appendBeforeRegex(String regex, String toAppend) {
		Pair<Integer, Integer> idx = RegexUtils.indexOf(regex, getS());
		return idx != null ? insert(idx.getValue0(), toAppend) : this;
	}
	
	public static void main(String[] args) throws IOException {
//		Tokenizer t = new Tokenizer("(stringcolumn($tbid) eq '71a5fcbfddcf5a6f574d275687e3e1de' && $tphys>1000.0 && $tphys<1010.0 ? $tphys : NaN):($smt)  w l lw 3.0,     ''      u (stringcolumn($tbid) eq 'b1f09c8a6cb5fea14e791067941f2016' && $tphys>1000.0 && $tphys<1010.0 ? $tphys : NaN):($smt) title 'POP2+mix' w l lw 2.0;");
//		System.out.println(t.contains(",[\\s]*(\\'|\\\")"));
		
		Tokenizer t = new Tokenizer("($tphys):($a1p) w l , 	 '' u ($tphys):($a10p) w l title '10% Lagr [DSNAME($tbid)]',      '' u ($tphys):($a50p) w l title '50% Lagr [DSNAME($tbid)]',      '' u ($tphys):($a70p) w l title '70% Lagr [DSNAME($tbid)]',      '' u ($tphys):($atot) w l title '100% Lagr [DSNAME($tbid)]';");
		System.out.println(t.expectedMatchRegex("[^,]+,[\\s]*"));
	}

	public boolean startsWith(String prefix) {
		return getS().startsWith(prefix);
	}

	public String getLine() {
		int nl = getS().indexOf('\n');
		if (nl >= 0) {
			String line = getS().substring(0, nl);
			setS(getS().substring(nl + 1));
			return line;
		} else {
			String line = getS();
			setS(null);
			return line;
		}
	}

//	public Tokenizer getRegex(String string) {
//		// TODO Auto-generated method stub
//		return null;
//	}
}
