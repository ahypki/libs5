package net.hypki.libs5.utils.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.hypki.libs5.utils.string.StringUtilities;

public class NestedKeyMapList<V> {
	
	private static final String SEPARATOR_DEFAULT = "/";
	
	private String separator = SEPARATOR_DEFAULT;
	
	private List<String> keysOrdered = new ArrayList<String>();
	
	private HashMap<String, List<V>> data = new HashMap<>();
	
	private int maxLevel = 0;

	public NestedKeyMapList() {
		
	}
	
	public NestedKeyMapList(String separator) {
		setSeparator(separator);
	}
	
	public String add(String nestedKey, V value) {
		String fullpath = buildPath(nestedKey);
		List<V> list = getList(fullpath);
		
		setMaxLevel(Math.max(getMaxLevel(), StringUtilities.countCharacters(fullpath, getSeparator()) - 1));
		
		list.add(value);
		
		return fullpath;
	}
	
	public List<V> get(String nestedKey) {
		return getList(buildPath(nestedKey));
	}
	
	public boolean isEmpty(String nestedKey) {
		List<V> list = data.get(buildPath(nestedKey));
		return list == null || list.size() == 0 ? true : false;
	}
	
	private List<V> getList(String path) {
		List<V> list = data.get(path);
		
		if (list == null) {
			list = new ArrayList<>();
			data.put(path, list);
			keysOrdered.add(path);
		}
		return list;
	}
	
	private String buildPath(String nestedKey) {
		if (!nestedKey.startsWith(getSeparator()))
			nestedKey = getSeparator() + nestedKey;
		if (!nestedKey.endsWith(getSeparator()))
			nestedKey = nestedKey + getSeparator();
		return nestedKey;
	}
	
	public boolean hasChildrenKeys(final String path) {
		final String pathFixed = buildPath(path);
		for (String key : NestedKeyMapList.this.data.keySet()) {
			if (key.startsWith(pathFixed) && pathFixed.length() < key.length())
				return true;
		}
		return false;
	}
	
	public Iterable<String> getKeys(final String path) {
		final String pathFixed = buildPath(path);
		final int pathFixedLength = pathFixed.length();
		
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {
					private Iterator<String> allKeys = keysOrdered.iterator();// NestedKeyMapList.this.data.keySet().iterator();
					private boolean init = false;
					private String nextKey = null;
					private Set<String> keysDone = new HashSet<>();
					
					@Override
					public String next() {
						if (init == false)
							hasNext();
						
						return nextKey;
					}
					
					@Override
					public boolean hasNext() {
						init = true;
						
						while (allKeys.hasNext()) {
							String tmp = allKeys.next();
							if (tmp.startsWith(pathFixed) && pathFixed.length() < tmp.length()) {
								int idx = tmp.indexOf(separator, pathFixedLength);
								if (idx >= 0) {
									nextKey = tmp.substring(pathFixedLength, idx);
									if (!keysDone.contains(nextKey)) {
										keysDone.add(nextKey);
										return true;
									}
								} else
									return false;
							}
						}
						
						return false;
					}
				};
			}
		};
	}

	public String getSeparator() {
		return separator;
	}

	public NestedKeyMapList<V> setSeparator(String separator) {
		this.separator = separator;
		return this;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	private void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}
}
