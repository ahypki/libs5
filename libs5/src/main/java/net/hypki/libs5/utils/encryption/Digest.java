package net.hypki.libs5.utils.encryption;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
 
/**
 * <code>Digest</code> provides some static helper methods for creating 
 * and updating message digests (MD5, SHA-1, etc.).
 */
public class Digest {
	/**
	 * Gets an MD5 message digest.
	 * 
	 * @return  the message digest
	 */
	public static MessageDigest getMD5MessageDigest() {
		return getMessageDigest("MD5");
	}
 
	/**
	 * Gets an SHA-1 message digest.
	 * 
	 * @return  the message digest
	 */
	public static MessageDigest getSHA1MessageDigest() {
		return getMessageDigest("SHA-1");
	}
 
	/**
	 * Gets a message digest of the specified algorithm.
	 * 
	 * @param  alg  the algorithm (MD5, SHA-1, etc.)
	 * @return  the message digest
	 */
	public static MessageDigest getMessageDigest(String alg) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance(alg);
			md.reset(); // always reset first
		} catch(java.security.NoSuchAlgorithmException nsae) {}
		return md;
	}
 
	/**
	 * Updates the message digest with the specified string message.  
	 * The string's bytes are taken using UTF-8 encoding.  
	 * 
	 * @param  md   the message digest
	 * @param  msg  the message
	 */
	public static void updateDigest(MessageDigest md, String msg) {
		if(msg != null) {
			try {
				updateDigest(md, msg.getBytes("UTF-8"));
			} catch(UnsupportedEncodingException uee) {}
		}
	}
 
	/**
	 * Updates the message digest with the specified byte[] message.  
	 * 
	 * @param  md   the message digest
	 * @param  msg  the message
	 */
	public static void updateDigest(MessageDigest md, byte[] msg) {
		if(md != null && msg != null) {
			md.update(msg);
		}
	}
 
	/**
	 * Returns the hex encoded digest value.  
	 * Note:  The digest itself is reset before this method returns.  
	 * 
	 * @param  md   the message digest
	 * @return  the hex encoded digest
	 */
	public static String getDigest(MessageDigest md) {
		if(md != null) {
			byte[] digest = md.digest();
			md.reset(); // reset to clean up before returning
			StringBuffer sb = new StringBuffer();
			int b = 0;
			for(int i = 0; i < digest.length; i++) {
				b = (int)digest[i] & 0xFF;
				if(b < 0x10) {
					sb.append("0");
				}
				sb.append(Integer.toHexString(b));
			}
			return sb.toString();
		}
		return null;
	}
}