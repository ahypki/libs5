package net.hypki.libs5.utils.string;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jodd.util.Wildcard;
import net.hypki.libs5.utils.LibsLogger;

import org.apache.commons.lang.StringUtils;
import org.javatuples.Pair;

/**
 * Much more methods for operating on strings one can find in class jodd.util.StringUtil
 * and documentation is here: http://jodd.org/api/jodd/index.html
 * @author ahypki
 *
 */
public class StringUtilities {
	
	public static final String ENCODING_UTF8 = "UTF-8";
	public static final String ENCODING_ISO_8859_1 = "ISO-8859-1";

	static final String HEXES = "0123456789ABCDEF";
	
	public static String replaceNonAlphanumeric(String s, String replacement) {
		return s.replaceAll("[^A-Za-z0-9]", replacement);
	}
	
	public static boolean areParenthesesMatching(String s) {
		if (s == null || s.length() == 0)
			return true;
		
		int par = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '(')
				par++;
			else if (s.charAt(i) == ')')
				par--;
		}
		return par == 0;
	}
	
	public static String substring(String s, char fromCharExclusive, char toCharExclusive) {
		int from = s.indexOf(fromCharExclusive);
		int to = s.indexOf(toCharExclusive);
		return from >= 0 && to > 0 ? s.substring(from + 1, to) : null;
	}
	
	public static String repeat(String toRepeat, int count) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++)
			sb.append(toRepeat);
		return sb.toString();
	}
	
	public static boolean equalsContent(String s1, String s2) {
		if (s1 != null && s2 != null)
			return s1.equals(s2);
		if (nullOrEmpty(s1) && nullOrEmpty(s2))
			return true;
		return false;
	}
	
	public static int locateColumns(String s, int[] columnsStarts, int[] columnsEnds) {
		int columnsCount = 0;
		boolean printable = false;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ' ') {
				if (printable) {
					columnsEnds[columnsCount - 1] = i;
					printable = false;
				}
			} else if (s.charAt(i) != ' ') {
				if (!printable) {
					// starting reading a new column
					columnsStarts[columnsCount] = i;
					columnsCount++;
					printable = true;
				}
			}
		}
		if (printable)
			columnsEnds[columnsCount - 1] = s.length();
		return columnsCount;
	}
	
    public static String serialize(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();
        return Base64.encodeToString(baos.toByteArray(), false);
    }
    
    public static byte[] serializeToBytes(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject(o);
        oos.close();
        return baos.toByteArray();
    }

    public static Object deserialize(String s) throws IOException, ClassNotFoundException {
		byte[] data = Base64.decode(s);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}
    
    public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}

	public static String getHex(byte[] raw) {
		if (raw == null) {
			return null;
		}
		final StringBuilder hex = new StringBuilder(2 * raw.length);
		for (final byte b : raw) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
		}
		return hex.toString();
	}

	public static String changeEncoding(String str, String srcEncoding, String destEncoding) {
		try {
			return new String(str.getBytes(srcEncoding), destEncoding);
		} catch (UnsupportedEncodingException e) {
			LibsLogger.error(StringUtilities.class, "Cannot change encoding from " + srcEncoding + " to " + destEncoding, e);
			return null;
		}
	}
	
	public static String capitalizeFirst(String input) {
		return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
	}
	
	public static String removeHTML(String input) {
		return input != null ? input.replaceAll("\\<[^\\>]*\\>", "").replaceAll("[\\s]+", " ") : "";
	}
	
	public static String substringMax(String s, int from, int length) {
		return s != null ? s.substring(from, Math.min(s.length(), from + length)) : "";
	}
	
	/**
	 * 
	 * @param s
	 * @param from
	 * @param length
	 * @param suffix - String which is appended to the returned text if the text had to be truncated
	 * @return
	 */
	public static String substringMax(String s, int from, int length, String suffix) {
		return s != null ? s.substring(from, Math.min(s.length(), from + length)) + (s.length() > (from + length) ? suffix : "") : "";
	}
	
	public static String convertStreamToString(InputStream is) throws IOException {
		/*
		 * To convert the InputStream to String we use the Reader.read(char[]
		 * buffer) method. We iterate until the Reader return -1 which means
		 * there's no more data to read. We use the StringWriter class to
		 * produce the string.
		 */
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}
	
	public static String[] split(String toSplit) {
		return StringUtils.split(toSplit);
	}
	
	public static String[] split(String toSplit, char separator) {
		if (toSplit == null)
			return null;
		return StringUtils.split(toSplit, separator);
	}
	
	public static String[] split(String toSplit, String separator) {
		return StringUtils.split(toSplit, separator);
	}
	
	public static List<String> split(String toSplit, String separator1, String separator2) {
		List<String> t = new ArrayList<String>();
		if (toSplit != null)
			for (String s1 : split(toSplit, separator1)) {
				for (String s2 : split(s1, separator2)) {
					if (notEmpty(s2))
						t.add(s2);
				}
			}
		return t;
	}
	
	public static List<String> splitByRegex(String toSplit, String separatorRegex) {
		List<String> splits = new ArrayList<>();
		
		while (true) {
			Pair<Integer, Integer> idx = RegexUtils.indexOf(separatorRegex, toSplit);
			
			if (idx == null) {
				if (notEmpty(toSplit))
					splits.add(toSplit);
				break;
			}
			
			if (idx.getValue0() > 0) {
				String tmp = toSplit.substring(0, idx.getValue0());
				if (notEmpty(tmp))
					splits.add(tmp);
			}
			
			toSplit = toSplit.substring(idx.getValue1());
		}
		
		return splits;
	}
	
	public static String join(Object[] toJoin, String separator, int columnWidth) {
		StringBuilder sb = new StringBuilder();
		for (Object o : toJoin) {
			sb.append(padLeft(String.valueOf(o), columnWidth));
		}
		return sb.toString();
	}
	
	public static String join(Object [] toJoin) {
		return StringUtils.join(toJoin);
	}
	
	public static String join(Object[] toJoin, String separator) {
		return StringUtils.join(toJoin, separator);
	}
	
	public static String join(Collection toJoin, String separator) {
		return StringUtils.join(toJoin, separator);
	}
	
	public static String join(Object [] toJoin, char separator) {
		return StringUtils.join(toJoin, separator);
	}
	
	/**
	 * When all characters to be converted are ASCII characters. It's much faster than getBytes() 
	 * 
	 * Taken from: http://www.javacodegeeks.com/2010/11/java-best-practices-char-to-byte-and.html
	 * @param str
	 * @return
	 */
	public static byte[] stringToBytesASCII(String str) {
		char[] buffer = str.toCharArray();
		byte[] b = new byte[buffer.length];
		for (int i = 0; i < b.length; i++) {
			b[i] = (byte) buffer[i];
		}
		return b;
	}
	
	/**
	 * For UTF-16 (the default character encoding in Java) characters we can use the methods shown below to convert a String to a byte array and vice – versa.
	 * Much faster than new String()
	 * 
	 * Taken from: http://www.javacodegeeks.com/2010/11/java-best-practices-char-to-byte-and.html
	 * @param str
	 * @return
	 */
	public static byte[] stringToBytesUTFCustom(String str) {
		char[] buffer = str.toCharArray();
		byte[] b = new byte[buffer.length << 1];
		for (int i = 0; i < buffer.length; i++) {
			int bpos = i << 1;
			b[bpos] = (byte) ((buffer[i] & 0xFF00) >> 8);
			b[bpos + 1] = (byte) (buffer[i] & 0x00FF);
		}
		return b;
	}
	
	/**
	 * For UTF-16 (the default character encoding in Java) characters we can use the methods shown below to convert a String to a byte array and vice – versa.
	 * Much faster than new String()
	 * 
	 * Taken from: http://www.javacodegeeks.com/2010/11/java-best-practices-char-to-byte-and.html
	 * @param str
	 * @return
	 */
	public static String bytesToStringUTFCustom(byte[] bytes) {
		char[] buffer = new char[bytes.length >> 1];
		for (int i = 0; i < buffer.length; i++) {
			int bpos = i << 1;
			char c = (char) (((bytes[bpos] & 0x00FF) << 8) + (bytes[bpos + 1] & 0x00FF));
			buffer[i] = c;
		}
		return new String(buffer);
	}
	
	public static String startsWith(String inputString, String regexToFind) {
		Pattern regex = Pattern.compile(regexToFind);
		Matcher m = regex.matcher(inputString);
		if (m.find()) {
			return m.group().trim();
		}
		return null;
	}
	
	public static String toString(String [] str) {
		StringBuilder builder = new StringBuilder();
		builder.append("[\n");
		for (String string : str) {
			builder.append(string);
			builder.append("\n");
		}
		builder.append("]");
		return builder.toString();
	}
	
	public static String toString(List objects, String separator) {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (Object string : objects) {
			if (!first)
				builder.append(separator);
			builder.append(string);
			first = false;
		}
		return builder.toString();
	}
	
	public static boolean nullOrEmpty(Object ... input) {
		for (Object s : input) {
			if (s == null || s.toString() == null || s.toString().length() == 0)
				return true;
		}
		return false;
	}
	
	public static boolean nullOrEmpty(String input) {
		return (input == null || input.trim().length() == 0);
	}
	
	public static boolean notEmpty(String input) {
		return !(input == null || input.trim().length() == 0);
	}
		
	public static boolean isValid(String input, String allowedChars) {
		if (input == null)
			return false;
		
		for (Byte b : input.getBytes()) {
			if (allowedChars.indexOf(b) < 0)
				return false;
		}
		return true;
	}
	
	public static String surround(String input, String what, String withWhatLeft, String withWhatRight) {
		if (StringUtilities.nullOrEmpty(input) || StringUtilities.nullOrEmpty(what))
			return input;
		
		int offset = 0;
		int index = 0;
		String lowerInput = input.toLowerCase();
		String lowerWhat = what.toLowerCase();
		StringBuilder buf = new StringBuilder();
		while ((index = lowerInput.indexOf(lowerWhat, offset)) >= 0) {
			buf.append(input.substring(offset, index));
			buf.append(withWhatLeft);
			buf.append(input.substring(index, index + what.length()));
			buf.append(withWhatRight);
			offset = index + what.length();
		}
		buf.append(input.substring(offset));
		return buf.toString();
	}
	
	public static boolean wildcardMatches(String pattern, String textToMatch) {
		if (pattern == null || textToMatch == null)
			return false;
		return Wildcard.match(textToMatch, pattern);
	}

	public static String divideToLines(String input, int lineLength, String linePrefix, String newLine) {
		if (nullOrEmpty(input))
			return input;
		
		StringBuilder buf = new StringBuilder();
		String parts[] = input.split(" ");
		int line = 0;
		for (int i = 0; i < parts.length; i++) {
			buf.append(parts[i]);
			buf.append(" ");
			line += parts[i].length();
			if (line > lineLength) {
				line = 0;
				if (i < (parts.length - 1)) {
					buf.append(newLine);
					buf.append(linePrefix);
				}
			}
		}
		return buf.toString();
	}
	
	public static boolean compareWithoutWhiteChars(String left, String right, boolean ignoreCase) {
		String l = left.replaceAll(" ", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
		String r = right.replaceAll(" ", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
		return l.equals(r);
//		String leftParts [] = left.split(" |\r\n|\t");
//		String rightParts [] = right.split(" |\r\n|\t");
//		
//		if (leftParts.length != rightParts.length)
//			return false;
//		
//		for (int i = 0; i < leftParts.length; i++) {
//			if (ignoreCase && !leftParts[i].toLowerCase().equals(rightParts[i].toLowerCase()))
//				return false;
//			else if (!leftParts[i].equals(rightParts[i]))
//				return false;
//		}
//		
//		return true;
	}

	public static boolean isAlphanumerical(String strToCheck) {
//		for (int i = 0; i < strToCheck.length(); i++) {
//			if (!Character.isLetterOrDigit(strToCheck.charAt(i)))
//				return false;
//		}
//		return true;
		return StringUtils.isAlphanumeric(strToCheck);
	}
	
	public static boolean isAlphanumerical(char charToCheck) {
//		return (Character.isLetterOrDigit(charToCheck));
		return StringUtils.isAlphanumeric(String.valueOf(charToCheck));
	}
	
	public static boolean isAlpha(String strToCheck) {
		return StringUtils.isAlpha(strToCheck);
	}
	
	public static boolean isNumeric(String strToCheck) {
		return StringUtils.isNumeric(strToCheck);
	}
	
	public static boolean isWhitespace(String strToCheck) {
		return StringUtils.isWhitespace(strToCheck);
	}
	
	public static boolean isAsciiPrintable(String strToCheck) {
		return StringUtils.isAsciiPrintable(strToCheck);
	}
	
	public static String buildStringFromString(String str, int count) {
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < count; i++)
			buf.append(str);
		
		return buf.toString();
	}
	
	public static String buildStringFromChar(char character, int count) {
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < count; i++)
			buf.append(character);
		
		return buf.toString();
	}

	public static char [] toChar(byte [] buffer) {
		char chars[] = new char[buffer.length];
		for (int x = 0; x < buffer.length; x++) {
			chars[x] = (char) buffer[x];
		}
		return chars;
	}

	public static byte[] toByte(char[] chars) {
		byte bytes [] = new byte[chars.length];
		for (int i = 0; i < chars.length; i++) {
			bytes[i] = (byte)chars[i];
		}
		return bytes;
	}

	public static String findCommonPrefix(String[] array) {
		StringBuilder builder = new StringBuilder();
		int index = 0;
		while (index >= 0) {
			if (array.length == 0)
				return builder.toString();
			
			for (int i = 0; i < (array.length - 1); i++) {
				if (index >= array[i].length() || index >= array[i + 1].length())
					return builder.toString();
				if (array[i].charAt(index) != array[i + 1].charAt(index))
					return builder.toString();
			}
			
			if (index < array[0].length())
				builder.append(array[0].charAt(index++));
			else
				return builder.toString();
		}
		return builder.toString();
	}

	public static String findCommonPrefix(Object[] array) {
		String[] strings = new String[array.length];
		int i = 0;
		for (Object object : array) {
			strings[i++] = object.toString();
		}
		return findCommonPrefix(strings);
	}

	public static String findCommonPrefix(Set<String> keySet) {
		return findCommonPrefix(keySet.toArray(new String[keySet.size()]));
	}

	public static String divideTolines(String longLine, int maxLength, int indentation) {
		StringBuilder result = new StringBuilder();
		String[] parts = longLine.split(" ");
		int currentLineLength = 0;
		boolean insideBrackets = false;

		for (int i = 0; i < parts.length; i++) {

			// check if the current line length is sufficiently long
			if ((parts[i].length() + currentLineLength) > maxLength) {
				// check if we are inside "...." brackets
				if (insideBrackets == true) {
					// result.append("\\\n" +
					// Instruction.getIndent(Instruction.indentCount + 1));
				} else {
					result.append("\n" + getIndent(indentation + 1));
					currentLineLength = 0;
				}

			}

			result.append(parts[i]);
			if ((i + 1) < parts.length)
				result.append(" ");

			currentLineLength += parts[i].length() + 1;

			// check if we entered into brackets
			int found = countCharacters(parts[i], '\"')
					+ (insideBrackets ? 1 : 0);
			insideBrackets = (found % 2) == 0 ? false : true;
		}

		return result.toString();
	}
	
	private static String getIndent(int indentation) {
		String indent = "";
		for (int i = 0; i < indentation; i++) {
			indent += "\t";
		}
		return indent;
	}

	public static int countCharacters(String s, char toSearch) {
		if (s == null)
			return 0;
		
		int counter = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == toSearch)
				counter++;
		}
		return counter;
	}
	
	public static int countCharacters(String s, String toSearch) {
		int counter = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.startsWith(toSearch, i))
				counter++;
		}
		return counter;
	}

	public static String padding(int nrOfChars, char paddingChar, String toPrint) {
		return new String(buildStringFromChar(paddingChar, nrOfChars - toPrint.length()) + toPrint);
	}
	
	public static String padRight(String s, int n) {
	     return String.format("%1$-" + n + "s", s);  
	}

	public static String padLeft(String s, int n) {
	    return String.format("%1$" + n + "s", s);  
	}

	public static String join(char c, String ... parts) {
		StringBuilder builder = new StringBuilder();
		for (String part : parts) {
			builder.append(part);
			builder.append(c);
		}
		return builder.toString();
	}

	public static String lastWord(String buffer) {
		if (nullOrEmpty(buffer))
			return null;
		
		String [] parts = split(buffer);
		return parts.length > 0 ? parts[parts.length - 1] : null;
	}
	
	public static String removeLastWord(String buffer) {
		if (nullOrEmpty(buffer))
			return null;
		
//		int i = buffer.indexOf(lastWord(buffer));
//		return i == -1 ? "" : buffer.substring(0, i);
		return buffer.replaceFirst("\\s*[^\\s]+\\s*$", "");
	}

	/**
	 * Substring last i characters from a string
	 * @param fieldId
	 * @param i
	 * @return
	 */
	public static String substringLast(String s, int i) {
		return s != null && s.length() > i ? s.substring(s.length() - i) : null;
	}
	
	public static String substringLast(String s, char c) {
		int indexLast = s.lastIndexOf(c);
		return s != null ? s.substring(indexLast + 1) : null;
	}

	public static String removeLastCharacters(String tmp, int count) {
		return tmp.substring(0, tmp.length() - count);
	}

	public static String removePolishChars(String s) {
		return s
				.replaceAll("ą", "a")
				.replaceAll("ż", "z")
				.replaceAll("ź", "z")
				.replaceAll("ś", "s")
				.replaceAll("ę", "e")
				.replaceAll("ć", "c")
				.replaceAll("ń", "n")
				.replaceAll("ó", "o")
				.replaceAll("ł", "l")
				;
	}

	public static boolean isTheSameNullSafe(String s1, String s2) {
		return (s1 == null ? "" : s1).equals(s2 == null ? "" : s2);
	}
	
	public static List<String> splitToList(String input, int maxLineLength) {
	    StringTokenizer tok = new StringTokenizer(input, " ");
	    List<String> out = new ArrayList<String>();
	    int lineLen = 0;
	    String tmp = "";
	    
	    while (tok.hasMoreTokens()) {
	        String word = tok.nextToken();

	        if ((lineLen + word.length()) > maxLineLength) {
	            out.add(tmp.trim());
	            lineLen = 0;
	            tmp = "";
	        }
	        tmp += word + " ";
	        lineLen += word.length();
	    }
	    
	    if (tmp.trim().length() > 0)
	    	out.add(tmp.trim());
	    
	    return out;
	}
	
	public static int findClosingBracket(String s, int start) {
		int openedBrackets = 0;
		for (int i = start; i < s.length(); i++) {
			if (s.charAt(i) == '(')
				openedBrackets++;
			else if (s.charAt(i) == ')')
				openedBrackets--;
			
			if (openedBrackets == 0)
				return i;
		}
		
		return -1;
	}

	public static void main(String[] args) {
//		System.out.println(String.format("%e", 12345678987652.251637458473625436));
//		System.out.println(":" + Long.MAX_VALUE + " :" + ("" + Long.MAX_VALUE).length());
//		System.out.println(":" + Integer.MAX_VALUE + " :" + ("" + Integer.MAX_VALUE).length());
		
		String s = "(abc)";
		System.out.println(s + " closing bracket at " + findClosingBracket(s, 0));
		s = "(abc)(ced)";
		System.out.println(s + " closing bracket at " + findClosingBracket(s, 0));
		s = "(abc(ced))";
		System.out.println(s + " closing bracket at " + findClosingBracket(s, 0));
	}
}
