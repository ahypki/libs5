package net.hypki.libs5.utils.compression;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class CompressionUtils {

	public static void uncompress(final File source, final File dest) throws IOException {
//		FileInputStream fin = new FileInputStream("archive.tar.gz");
//		BufferedInputStream in = new BufferedInputStream(fin);
		FileOutputStream out = new FileOutputStream(dest);
		GZIPInputStream is = new GZIPInputStream(new FileInputStream(source));
//		GZipCompressorInputStream gzIn = new GZipCompressorInputStream(in);
		final byte[] buffer = new byte[1024];
		int n = 0;
		while (-1 != (n = is.read(buffer))) {
		    out.write(buffer, 0, n);
		}
		out.close();
		is.close();
	}
}
