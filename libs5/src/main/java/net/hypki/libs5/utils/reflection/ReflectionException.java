/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.hypki.libs5.utils.reflection;

/**
 *
 * @author ahypki
 */
public class ReflectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2006816905548216260L;

	public ReflectionException() {

	}

	public ReflectionException(String message) {
		super(message);
	}

	public ReflectionException(String message, Throwable t) {
		super(message, t);
	}
}
