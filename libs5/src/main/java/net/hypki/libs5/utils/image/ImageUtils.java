package net.hypki.libs5.utils.image;

import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class ImageUtils {
	
	public static Dimension getImageDimension(File file) throws IOException {
		Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("jpg");
		ImageReader reader = (ImageReader)readers.next();
		
		ImageInputStream iis = ImageIO.createImageInputStream(file);

		reader.setInput(iis, true);
		
		return new Dimension(reader.getWidth(0), reader.getHeight(0));
	}
	
	public static Dimension getImageDimension(URL url) throws IOException {
		Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("jpg");
		ImageReader reader = (ImageReader)readers.next();
		URLConnection uc = url.openConnection();
		
		ImageInputStream iis = ImageIO.createImageInputStream(uc.getInputStream());

		reader.setInput(iis, true);
		
		return new Dimension(reader.getWidth(0), reader.getHeight(0));
	}
	
	/**
	 * Snippet taken from: http://viralpatel.net/blogs/2009/05/20-useful-java-code-snippets-for-java-developers.html
	 * @param inputFilename
	 * @param thumbWidth
	 * @param thumbHeight
	 * @param quality
	 * @param outFilename
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static BufferedImage getThumbnailAndCrop(BufferedImage originalImage, int scaledWidth, 
			int scaledHeight, boolean keepRatio, int widthCrop, int heightCrop)
			throws InterruptedException, FileNotFoundException, IOException {
		
		Graphics2D g = null;
		try {
			float ratio = 1.0f;
			if (scaledWidth > 0)
				ratio = scaledWidth / (float) originalImage.getWidth();
			else
				ratio = scaledHeight / (float) originalImage.getHeight();
			
			int newWidth = (int) (originalImage.getWidth() * ratio);
			int newHeight = (int) (originalImage.getHeight() * ratio);
			
//			if (keepRatio == false)
//				newHeight = newWidth;

			BufferedImage scaledBI = new BufferedImage(newWidth, newHeight, BufferedImage.SCALE_SMOOTH);
			g = scaledBI.createGraphics();
			g.setComposite(AlphaComposite.Src);
			g.drawImage(originalImage, 0, 0, newWidth, newHeight, null);
			widthCrop = Math.min(scaledBI.getWidth(), widthCrop);
			heightCrop = Math.min(scaledBI.getHeight(), heightCrop);
			if (keepRatio == false) {
				widthCrop = Math.min(widthCrop, heightCrop);
				heightCrop = widthCrop;
			}
			return scaledBI.getSubimage(0, 0, widthCrop, heightCrop);
		} finally {
			if (g != null)
				g.dispose();
		}
	}
		
	/**
	 * Snippet taken from: http://viralpatel.net/blogs/2009/05/20-useful-java-code-snippets-for-java-developers.html
	 * @param inputFilename
	 * @param thumbWidth
	 * @param thumbHeight
	 * @param quality
	 * @param outFilename
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static BufferedImage getThumbnail(BufferedImage originalImage, int scaledWidth, 
			int scaledHeight, boolean keepRatio)
			throws InterruptedException, FileNotFoundException, IOException {
		
		Graphics2D g = null;
		try {
			float ratio = 1.0f;
			if (scaledWidth > 0)
				ratio = scaledWidth / (float) originalImage.getWidth();
			else
				ratio = scaledHeight / (float) originalImage.getHeight();
			
			int newWidth = (int) (originalImage.getWidth() * ratio);
			int newHeight = (int) (originalImage.getHeight() * ratio);

			BufferedImage scaledBI = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
			g = scaledBI.createGraphics();
			g.setComposite(AlphaComposite.Src);
			g.drawImage(originalImage, 0, 0, newWidth, newHeight, null);
			return scaledBI;
		} finally {
			if (g != null)
				g.dispose();
		}
	}
	
	/**
	 * Snippet taken from: http://viralpatel.net/blogs/2009/05/20-useful-java-code-snippets-for-java-developers.html
	 * @param inputFilename
	 * @param thumbWidth
	 * @param thumbHeight
	 * @param quality - in percent (0, 100)
	 * @param outFilename
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
//	public static void createThumbnail(String inputFilename, int thumbWidth,
//			int thumbHeight, int quality, String outFilename)
//			throws InterruptedException, FileNotFoundException, IOException {
//		
//		// load image from inputFilename
//		Image image = Toolkit.getDefaultToolkit().getImage(inputFilename);
//		MediaTracker mediaTracker = new MediaTracker(new Container());
//		mediaTracker.addImage(image, 0);
//		mediaTracker.waitForID(0);
//		// use this to test for errors at this point:
//		// System.out.println(mediaTracker.isErrorAny());
//
//		// determine thumbnail size from WIDTH and HEIGHT
//		double thumbRatio = (double) thumbWidth / (double) thumbHeight;
//		int imageWidth = image.getWidth(null);
//		int imageHeight = image.getHeight(null);
//		double imageRatio = (double) imageWidth / (double) imageHeight;
//		if (thumbRatio < imageRatio) {
//			thumbHeight = (int) (thumbWidth / imageRatio);
//		} else {
//			thumbWidth = (int) (thumbHeight * imageRatio);
//		}
//
//		// draw original image to thumbnail image object and
//		// scale it to the new size on-the-fly
//		BufferedImage thumbImage = new BufferedImage(thumbWidth, thumbHeight, BufferedImage.TYPE_INT_RGB);
//		Graphics2D graphics2D = thumbImage.createGraphics();
//		graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
//
//		// save thumbnail image to outFilename
//		BufferedOutputStream out = null;
//		try {
//			out = new BufferedOutputStream(new FileOutputStream(outFilename));
//			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
//			JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(thumbImage);
//			quality = Math.max(0, Math.min(quality, 100));
//			param.setQuality((float) quality / 100.0f, false);
//			encoder.setJPEGEncodeParam(param);
//			encoder.encode(thumbImage);
//		} finally {
//			if (out != null)
//				out.close();
//		}
//	}
	
	/**
	 * Snippet taken from: http://viralpatel.net/blogs/2009/05/20-useful-java-code-snippets-for-java-developers.html
	 * @param outputFileName
	 * @throws Exception
	 */
	public static void captureScreenshotToFile(String outputFileName) throws IOException {
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle screenRectangle = new Rectangle(screenSize);
			Robot robot = new Robot();
			BufferedImage image = robot.createScreenCapture(screenRectangle);
			ImageIO.write(image, "png", new File(outputFileName));
		} catch (AWTException e) {
			throw new IOException("Cannot capture screen", e);
		}
	}
	
	public static byte[] captureScreenshotAsBinary() throws IOException {
		ByteArrayOutputStream baos = null;
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle screenRectangle = new Rectangle(screenSize);
			Robot robot = new Robot();
			BufferedImage image = robot.createScreenCapture(screenRectangle);

			baos = new ByteArrayOutputStream(1000);
			ImageIO.write(image, "jpeg", baos);
			baos.flush();
			return baos.toByteArray();
		} catch (AWTException e) {
			throw new IOException("Cannot capture screen", e);
		} finally {
			if (baos != null)
				baos.close();
		}
	}
	
//	public static byte[] bufferedImageToByteArray(BufferedImage img) throws IOException {
//		ByteArrayOutputStream os = new ByteArrayOutputStream();
//		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(os);
//		encoder.encode(img);
//		return os.toByteArray();	
//	}
	
	/**
	 * Snippet taken from http://mindprod.com/jgloss/imageio.html#FROMURL
	 * @param img
	 * @param format - png, jpg etc. (use getWriterFormatNames() to get a list
	 * of supported extensions)
	 * @return
	 * @throws IOException
	 */
	public static byte[] bufferedImageToByteArray(BufferedImage img, String format) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(10000);

		try {
			ImageIO.write( img, format, baos );
			baos.flush();
			return baos.toByteArray();
		} finally {
			if (baos != null)
				baos.close();
		}
		
	}
	
	public static BufferedImage byteArrayToBufferedImage(byte [] imageAsBytes) throws IOException {
		 return ImageIO.read(new ByteArrayInputStream(imageAsBytes));
	}
	
//	public static BufferedImage fileToBufferedImage(String filename) throws IOException {
////		 return ImageIO.read(new File(inputFilename));
//		
//		FileInputStream input = new FileInputStream(filename);
//		try {
//			JPEGImageDecoder decoder = JPEGCodec.createJPEGDecoder(input);
//			BufferedImage buffImg = decoder.decodeAsBufferedImage();
//			return buffImg;
//		} finally {
//			if (input != null)
//				input.close();
//		}
//	}
	
	public static BufferedImage urlToBufferedImage(URL url) throws IOException {
		return ImageIO.read(url);
	}
	
	public static void bufferedImageToFile(BufferedImage img, String filename) throws IOException {
		ImageIO.write(img, "jpeg" /* "png" "jpeg" ... format desired */,
	               new File ( filename ) /* target */ );
	}
	
	public static String [] getWriterFormatNames() {
		return ImageIO.getWriterFormatNames();
	}

	public static BufferedImage cropImage(BufferedImage img, int x, int y, int width, int height) {
		if ((x + width) < img.getWidth()
				|| (y + height) < img.getHeight()) {
			return img.getSubimage(x, y, 
				Math.min(img.getWidth(), width), 
				Math.min(img.getHeight(), height));
		} else
			return img;
	}

	public static byte[] bufferedImageToByteArray(BufferedImage originalImage) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}

	public static BufferedImage fileToBufferedImage(String path) throws IOException {
		return ImageIO.read(new File(path));
	}
	
	public static BufferedImage inputStreamToBufferedImage(InputStream is) throws IOException {
		return ImageIO.read(is);
	}
}
