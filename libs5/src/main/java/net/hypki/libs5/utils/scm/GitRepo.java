package net.hypki.libs5.utils.scm;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.Basher;

import com.google.gson.annotations.Expose;

public class GitRepo {
	
	@Expose
	private String path = null;
		
	@Expose
	private Set<String> groups = null;
	
	public GitRepo() {
		
	}
	
	public GitRepo(String path, String groups) {
		setPath(path);
		addGroups(groups);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GitRepo) {
			GitRepo inputRepo = (GitRepo) obj;
			return inputRepo.getPath().equals(getPath());
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("Path: ");
		builder.append(path);
		
		boolean first = true;
		builder.append("\nGroups: ");
		for (String s : this.getGroups()) {
			if (first == false) {
				builder.append(", ");
			}
			builder.append(s);
			
			first = false;
		}
		return builder.toString();
	}
	
	public boolean exist() {
		return FileUtils.exist(getPath());
	}

	private void addGroups(String groups) {
		if (StringUtilities.nullOrEmpty(groups))
			return;
		
		for (String group : groups.split("[,\\s]+")) {
			if (group.trim().length() > 0)
				getGroups().add(group.trim().toLowerCase());
		}
	}

	public String getPath() {
		return path;
	}
	
	public String getAbsolutePath() {
		return new File(path).getAbsolutePath();
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Set<String> getGroups() {
		if (groups == null)
			groups = new HashSet<String>();
		return groups;
	}

	public void setGroups(Set<String> groups) {
		this.groups = groups;
	}
	
	public List<GitFile> getStatus() throws IOException {
		List<GitFile> files = new ArrayList<GitFile>();
		
		for (String line : Basher.exec(new String[]{"git", "status", "-su"}, new File(getPath()))) {
			line = line.trim();
			final int spaceIdx = line.indexOf(' ');
			final String statusStr = line.substring(0, spaceIdx).trim();
			String fileStr = line.substring(spaceIdx).trim();
			
			if (statusStr.equals("??")) {
				if (fileStr.startsWith("\"") && fileStr.endsWith("\""))
					fileStr = fileStr.substring(1, fileStr.length() - 1);
				files.add(new GitFile(fileStr, GitFileStatus.UNTRACKED));
			} else if (statusStr.equals("D"))
				files.add(new GitFile(fileStr, GitFileStatus.REMOVED));
			else if (statusStr.equals("M"))
				files.add(new GitFile(fileStr, GitFileStatus.MODIFIED));
			else if (statusStr.equals("RM"))
				files.add(new GitFile(fileStr, GitFileStatus.MOVED));
			else if (statusStr.equals("A"))
				files.add(new GitFile(fileStr, GitFileStatus.NEW));
			else if (statusStr.equals("AD"))
				files.add(new GitFile(fileStr, GitFileStatus.REMOVED));
			else
				LibsLogger.error(GitRepo.class, "Uknown line ", line);
		}
		
		return files;
	}
	
	public boolean isClean() throws IOException {
		boolean isClean = true;
		for (String line : Basher.exec(new String[]{"git", "status", "-s"}, new File(getPath()))) {			
			isClean = false;
		}
		return isClean;
	}
	
	private boolean isRemoteUrlDefined() throws IOException {
		if (isGitRepo(new File(getAbsolutePath()))) {
			for (String line : FileUtils.readLines(new File(getAbsolutePath() + "/.git/config"))) {
				if (line.matches("[\\s]*url[\\s]*=[\\s]*.*"))
					return true;
			}
		}
		return false;
	}

	/**
	 * Returns error message, or null if pull was OK
	 * @return
	 * @throws IOException
	 */
	public String pull() throws IOException {
		if (!isRemoteUrlDefined())
			return null;
		
		String err = null;
		List<String> lines = Basher.exec(new String[]{"git", "pull", "--all"}, new File(getPath()));
		
		if (lines.get(lines.size() - 1).matches("Already up[\\-\\s]*to[\\-\\s]*date."))
			LibsLogger.debug(GitRepo.class, getAbsolutePath(), "\tPull OK");
		else {
			// check once again whether pull works
			lines = Basher.exec(new String[]{"git", "pull", "--all"}, new File(getPath()));
			
			if (lines.get(lines.size() - 1).matches("Already up[\\-\\s]*to[\\-\\s]*date."))
				LibsLogger.debug(GitRepo.class, getAbsolutePath(), "\tPull OK");
			else {
				LibsLogger.warn(GitRepo.class, getAbsolutePath(), "\tPull FAILED");
				for (String l : lines) {
					LibsLogger.warn(GitRepo.class, l);
					err += l + "\n";
				}
			}
		}
		
		return notEmpty(err) ? err : null;
	}
	
	public String push() throws IOException {
		if (!isRemoteUrlDefined())
			return null;
		
		String err = null;
		List<String> lines = Basher.exec(new String[]{"git", "push", "--all"}, new File(getPath()));
		
		if (lines.get(lines.size() - 1).equals("Everything up-to-date"))
			LibsLogger.debug(GitRepo.class, "\tPush OK");
		else {
			boolean rejected = false;
			for (String l : lines)
				if (l.contains("rejected")) {
					rejected = true;
					break;
				}
			
			if (!rejected)
				LibsLogger.debug(GitRepo.class, "\tPush OK");
			else {
				LibsLogger.warn(GitRepo.class, "\tPush FAILED");
				for (String l : lines) {
					LibsLogger.warn(GitRepo.class, l);
					err += l + "\n";
				}
			}
		}

		return notEmpty(err) ? err : null;
	}
	
	public static boolean isGitRepo(File path) {
		return FileUtils.exist(path.getAbsolutePath(), ".git");
	}

	/**
	 * Returns number of bytes added to git repo
	 * @param gitFile
	 * @return
	 * @throws IOException
	 */
	public long add(GitFile gitFile) throws IOException {
		return add(new File(gitFile.getPath()));
//		return new FileExt(gitFile.getPath()).getSizeBytes();
	}
	
	public long add(File file) throws IOException {
		Basher.exec(new String[]{"git", "add", file.getPath()}, new File(getPath()));
		return new FileExt(getPath(), file.getPath()).getSizeBytes();
	}

	public void ignore(GitFile gitFile) throws IOException {
//		LibsLogger.info(GitRepo.class, "Ignoring file: " + gitFile);
//		LibsLogger.info(GitRepo.class, "Ignoring file: " + new FileExt(getPath(), gitFile.getPath()));
		ignore(new FileExt(getPath(), gitFile.getPath()));
	}
	
	public void ignore(File file) throws IOException {
//		LibsLogger.info(GitRepo.class, "path: " + getPath());
		final String ignoringPath = new FileExt(file.getAbsolutePath()).getRelativePathTo(new FileExt(getPath()).getAbsolutePath());
//		LibsLogger.info(GitRepo.class, "Ignoring file: " + ignoringPath);
//		LibsLogger.info(GitRepo.class, "gitignore file: " + new FileExt(getPath(), ".gitignore").getAbsolutePath());
		FileUtils.appendToFile(ignoringPath + "\n", new FileExt(getPath(), ".gitignore").getAbsolutePath());
	}

	public void commit(String commitMsg) throws IOException {
		Basher.exec(new String[]{"git", "commit", "-a", "-m", "'" + commitMsg + "'"}, new File(getPath()));
	}

	public boolean isInGroup(String g) {
		return getGroups().size() == 0
				|| (getGroups().size() > 0 && getGroups().contains(g));
	}
}
