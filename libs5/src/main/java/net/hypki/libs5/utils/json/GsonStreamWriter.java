package net.hypki.libs5.utils.json;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map.Entry;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;

public class GsonStreamWriter implements JsonStreamWriter {
	
	private String filename = null;
	private JsonWriter writer = null;
	
	public GsonStreamWriter(String filename) {
		setFilename(filename);
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}

	@Override
	public void beginObject() throws IOException {
		getJsonWriter().beginObject();
	}

	@Override
	public void endObject() throws IOException {
		getJsonWriter().endObject();
	}

	@Override
	public void close() throws IOException {
		getJsonWriter().close();
	}

	@Override
	public void append(String name, String value) throws IOException {
		getJsonWriter().name(name);
		getJsonWriter().value(value);
	}
	
	public void append(JsonElement obj) throws IOException {
		if (obj.isJsonObject()) {
			beginObject();
			for (Entry<String, JsonElement> entry : obj.getAsJsonObject().entrySet()) {
				append(entry.getKey(), entry.getValue());
			}
			endObject();
		} else if (obj.isJsonArray()) {
			beginArray();
			for (JsonElement entry : obj.getAsJsonArray()) {
				append(entry);
			}
			endArray();
		} else if (obj.isJsonPrimitive()) {
			getJsonWriter().value(obj.getAsJsonPrimitive().toString());
		} else if (obj.isJsonNull()) {
			// ignore
		} else
			throw new NotImplementedException();
	}
	
	public void append(String name, JsonElement obj) throws IOException {
		getJsonWriter().name(name);
		if (obj.isJsonObject()) {
			beginObject();
			for (Entry<String, JsonElement> entry : obj.getAsJsonObject().entrySet()) {
				append(entry.getKey(), entry.getValue());
			}
			endObject();
		} else if (obj.isJsonArray()) {
			beginArray();
			for (JsonElement entry : obj.getAsJsonArray()) {
				append(entry);
			}
			endArray();
		} else {
			getJsonWriter().value(obj.getAsString());
		}
	}
	
	@Override
	public void append(String name, long value) throws IOException {
		getJsonWriter().name(name);
		getJsonWriter().value(value);
	}

	private JsonWriter getJsonWriter() throws IOException {
		if (writer == null) {
			writer = new JsonWriter(new BufferedWriter(new FileWriter(getFilename())));
			writer.setIndent("\t");
		}
		return writer;
	}

	@Override
	public void name(String name) throws IOException {
		getJsonWriter().name(name);
	}

	@Override
	public void value(String value) throws IOException {
		getJsonWriter().value(value);
	}

	@Override
	public void beginArray() throws IOException {
		getJsonWriter().beginArray();
	}

	@Override
	public void endArray() throws IOException {
		getJsonWriter().endArray();
	};
}
