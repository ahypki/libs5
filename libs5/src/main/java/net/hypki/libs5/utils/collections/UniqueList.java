package net.hypki.libs5.utils.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class UniqueList<T> extends ArrayList<T> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3296293215415867165L;

	public UniqueList() {
		
	}
	
	public boolean add(T e) {
		if (super.contains(e) == false)
			return super.add(e);
		else
			return false;
	};
	
	public void add(int index, T element) {
		if (super.contains(element) == false)
			super.add(index, element);
	};
	
	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean result = false;
		for (T t : c) {
			if (add(t) == true)
				result = true;
		}
		return result;
	}
	
	public String toString(String separator) {
		StringBuilder builder = new StringBuilder(100);
		for (T t : this) {
			builder.append(t.toString());
			builder.append(separator);
		}
		return builder.toString();
	}
}
