package net.hypki.libs5.utils.file;

import jodd.util.StringUtil;
import net.hypki.libs5.utils.string.StringUtilities;

public class Line {
	private String line = null;
	private String[] lineSplitted = null;
	private char separator;
	private boolean separatorSet = false;
	
	public Line() {
		
	}
	
	public Line(String line) {
		setLine(line);
	}
	
	public Line(String line, char separator) {
		setSeparator(separator);
		setLine(line);
	}

	public void setLine(String line) {
		this.line = line;
		if(isSeparatorSet())
			lineSplitted = StringUtil.split(line.trim(), String.valueOf(getSeparator()));
		else
			lineSplitted = StringUtilities.split(line);
	}

	public String getLine() {
		return line;
	}
	
	@Override
	public String toString() {
		return line;
	}
	
	public String get(int i) {
		return lineSplitted[i];
	}
	
	public double getDouble(int i) {
		return Double.parseDouble(lineSplitted[i]);
	}
	
	public int getInt(int i) {
		return Integer.parseInt(lineSplitted[i]);
	}
	
	public long getLong(int i) {
		return Long.parseLong(lineSplitted[i]);
	}

	public int size() {
		return lineSplitted != null ? lineSplitted.length : 0;
	}

	public Object getObject(int i) {
		if (get(i).matches("[\\d+-]+"))
			return getLong(i);
		else if (get(i).matches("[+-]*\\d[\\d\\.eE+-]+"))
			return getDouble(i);
		else
			return get(i);
	}

	public char getSeparator() {
		return separator;
	}

	public void setSeparator(char separator) {
		this.separator = separator;
		this.separatorSet = true;
	}

	private boolean isSeparatorSet() {
		return separatorSet;
	}

	private void setSeparatorSet(boolean separatorSet) {
		this.separatorSet = separatorSet;
	}
}
