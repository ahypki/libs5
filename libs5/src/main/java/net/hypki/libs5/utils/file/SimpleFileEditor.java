package net.hypki.libs5.utils.file;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class SimpleFileEditor {
	private String filename = null;
	private BufferedWriter out = null;
	
	public SimpleFileEditor(String filename) {
		this.filename = filename;
	}
	
	public void open(boolean append) throws IOException {
		if (out != null)
			throw new IOException("File is already opened!");
		
		// Create or append file
		FileWriter fstream = new FileWriter(filename, append);
		out = new BufferedWriter(fstream);
	}
	
	public void close() throws IOException {
		if (out != null) {
			out.close();
			out = null;
		}
	}
	
	public void write(String textToWrite) throws IOException {
		out.write(textToWrite);
	}
}
