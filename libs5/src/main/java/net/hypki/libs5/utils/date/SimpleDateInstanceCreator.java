package net.hypki.libs5.utils.date;

import java.lang.reflect.Type;

import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class SimpleDateInstanceCreator implements InstanceCreator<SimpleDate>, JsonDeserializer<SimpleDate>, JsonSerializer<SimpleDate> {

	@Override
	public SimpleDate createInstance(Type type) {
		return new SimpleDate();
	}
	
	@Override
	public JsonElement serialize(SimpleDate src, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(src.toString());
	}
	
	@Override
	public SimpleDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if (json.isJsonPrimitive() && json.getAsJsonPrimitive().isNumber())
			return new SimpleDate(json.getAsLong());
		return SimpleDate.parse(json.getAsString());
	}
}
