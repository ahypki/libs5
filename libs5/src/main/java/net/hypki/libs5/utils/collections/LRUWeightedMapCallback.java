package net.hypki.libs5.utils.collections;

public interface LRUWeightedMapCallback {

	public void onRemove(String key);
}
