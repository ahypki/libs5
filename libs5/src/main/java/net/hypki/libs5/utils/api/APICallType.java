package net.hypki.libs5.utils.api;

public enum APICallType {
	GET,
	POST,
	DELETE
}
