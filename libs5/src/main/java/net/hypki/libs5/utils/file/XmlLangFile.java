package net.hypki.libs5.utils.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import net.hypki.libs5.utils.LibsLogger;

@Deprecated
public class XmlLangFile {
	
	private Locale locale = null;
	private String filename = "";
	private Properties properties = null;
	
	public XmlLangFile() {

	}
	
	public XmlLangFile(String filename, Locale locale) {
		init(filename, locale);
	}
	
	public String getString(String key, String defaultValue) {
		String s = getString(key);
		if (s == null || s.length() == 0)
			return defaultValue;
		return s;
	}
	
	private String getString(String key) {
		if (getProperties() != null) {
			String ret = getProperties().getProperty(key);
			if (ret != null)
				return ret;
		}
		return "";
	}

	public void init(String filename, Locale locale) {
		this.setLocale(locale);
		this.setFilename(filename);
	}

	public String getFormatedString(String key, String defaultValue, Object[] parameters) {
		if (getProperties() != null) {
			String ret = getProperties().getProperty(key);
			if (ret != null)
				return String.format(key, parameters);
		}
		return defaultValue;
	}

	
	private Properties getProperties() {
		if (properties == null) {
			properties = new Properties();
			try {
				FileExt file = new FileExt(getFilename());
				if (file.getExtensionOnly().equalsIgnoreCase("xml"))
					properties.loadFromXML(new FileInputStream(file));
				else
					properties = null;
			} catch (FileNotFoundException ex) {
				LibsLogger.error(XmlLangFile.class, "File not found", ex);
				properties = null;
			} catch (IOException ex) {
				LibsLogger.error(XmlLangFile.class, "Can not open file", ex);
				properties = null;
			}
		}
		return properties;
	}

	private void setFilename(String filename) {
		this.filename = filename;
		this.properties = null;
	}

	private String getFilename() {
		return filename;
	}

	public String getError(int errorCode) {
		return getString(String.valueOf(errorCode));
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Locale getLocale() {
		return locale;
	}
}
