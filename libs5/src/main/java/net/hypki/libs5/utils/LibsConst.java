/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.hypki.libs5.utils;


/**
 *
 * @author ahypki
 */
public class LibsConst {
	public static final String VERSION = "0.5.1";
	
	public static final int NO_ERROR = 0;
	public static final int ERROR_APPLICATION_INITIALIZATION_FAILED = 1;
	public static final int ERROR_UNIT_TEST_FAILED = 2;

	public static final boolean DEBUG_ENABLED = true;
}
