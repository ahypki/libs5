package net.hypki.libs5.utils.encryption;

import net.hypki.libs5.utils.system.StandardInput;



public class StringEncrypterMain {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		PasswordsKeeper passwordKeeper = new PasswordsKeeper();
		StringEncrypter encrypter = null;
		
		System.out.print("Type password: ");
		String password1 = StandardInput.readStringFromStandardInput();
		System.out.print("Type password again: ");
		String password2 = StandardInput.readStringFromStandardInput();
		if (!password1.equals(password2)) {
			System.out.println("Passwords don't match!");
			return;
		}

		passwordKeeper.setPassword(password1);
		encrypter = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME, passwordKeeper.getPassword());
		
		System.out.print("Type text for encrypt: ");
		String toEncrypt = StandardInput.readStringFromStandardInput();
		
		System.out.println(encrypter.encrypt(toEncrypt));
	}

}
