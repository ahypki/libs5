package net.hypki.libs5.utils.encryption;

import org.apache.commons.codec.binary.Base64;

public class EncryptionUtils {

//	private static final String KEY = "some-secret-key-of-your-choice";
	
	public static void main(String[] args) {
		String enc = null;
		System.out.println(enc = encrypt("aa", "test"));
		System.out.println(decrypt("aa", enc));
	}

	public static String encrypt(final String key, final String text) {
		return new String(Base64.encodeBase64(xor(key.getBytes(), text.getBytes())));
	}

	public static String decrypt(final String key, final String hash) {
		try {
			return new String(xor(key.getBytes(), Base64.decodeBase64(hash.getBytes())), "UTF-8");
		} catch (java.io.UnsupportedEncodingException ex) {
			throw new IllegalStateException(ex);
		}
	}

	private static byte[] xor(final byte[] key, final byte[] input) {
		final byte[] output = new byte[input.length];
		final byte[] secret = key;
		int spos = 0;
		for (int pos = 0; pos < input.length; ++pos) {
			output[pos] = (byte) (input[pos] ^ secret[spos]);
			spos += 1;
			if (spos >= secret.length) {
				spos = 0;
			}
		}
		return output;
	}
}
