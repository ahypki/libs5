/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.hypki.libs5.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ahypki
 */
public class LibsInit {
	
	private static List<IModule> runningModules = new ArrayList<IModule>();
	
	public static void addToRunningModules(IModule module) {
		runningModules.add(module);
	}
	
	public static List<IModule> getRunningModules() {
		return runningModules;
	}

	/**
	 * Shutdown application, connections to database, loggers, cache etc.
	 * Exits from java to the system if @param exitFromApplication is true.
	 * @param errorCode
	 */
	public static void shutdown() {
		long start = System.currentTimeMillis();
		for (int i = 0; i < runningModules.size(); i++) {
			IModule module = runningModules.get(i);
			if (module != null)
				module.shutdown();
		}
		LibsLogger.debug(LibsInit.class, LibsInit.class.getSimpleName() + " finished in " + (System.currentTimeMillis() - start));
	}
}
