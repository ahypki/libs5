package net.hypki.libs5.utils.compression;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.hypki.libs5.utils.file.FileExt;

public class GzipCompressor {
	
	private FileExt outputGzip = null;
	
	private ZipOutputStream zos = null;

	public GzipCompressor(FileExt outputGzip) {
		setOutputGzip(outputGzip);
	}
	
	public GzipCompressor(ZipOutputStream zos) {
		this.zos = zos;
	}
	
	public void addFile(String filePath, InputStream inputStream, FileExt pathRelativeTo) throws IOException {
		ZipEntry zipEntry = new ZipEntry(new FileExt(filePath).getRelativePathTo(pathRelativeTo.getAbsolutePath()));
		byte[] buffer = new byte[1024];
		
		getZipOutputStream().putNextEntry(zipEntry);
		
		int length;
		
		while ((length = inputStream.read(buffer)) > 0) {
			getZipOutputStream().write(buffer, 0, length);
			getZipOutputStream().flush();
		}

		getZipOutputStream().closeEntry();
		inputStream.close();
	}
	
	public void addFile(FileExt toAdd, FileExt pathRelativeTo) throws IOException {
		ZipEntry zipEntry = new ZipEntry(toAdd.getRelativePathTo(pathRelativeTo.getAbsolutePath()));
		byte[] buffer = new byte[1024];
		
		getZipOutputStream().putNextEntry(zipEntry);
		
		FileInputStream fis = new FileInputStream(toAdd);
		int length;
		
		while ((length = fis.read(buffer)) > 0) {
			getZipOutputStream().write(buffer, 0, length);
			getZipOutputStream().flush();
		}

		getZipOutputStream().closeEntry();
		fis.close();
	}
	
	public void close() throws IOException {
		getZipOutputStream().close();
	}
	
	private ZipOutputStream getZipOutputStream() throws FileNotFoundException {
		if (zos == null) {
			zos = new ZipOutputStream(new FileOutputStream(getOutputGzip()));
		}
		return zos;
	}

	public FileExt getOutputGzip() {
		return outputGzip;
	}

	public void setOutputGzip(FileExt outputGzip) {
		this.outputGzip = outputGzip;
	}
}
