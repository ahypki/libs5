package net.hypki.libs5.utils.string;

import java.io.Serializable;
import java.util.UUID;

import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Meta implements Serializable {

	@Expose
	@NotEmpty
	@NotNull
	private String name = null;
	
	@Expose
	private Object value = null;
	
	@Expose
	private String description = null;
	
//	@Expose
//	@NotEmpty
//	@NotNull
//	@AssertValid
//	private UUID userId = null;
	
	public Meta() {
		
	}
	
	public Meta(String name, Object value, String description) {
		setName(name);
		setValue(value);
		setDescription(description);
	}
	
	public Meta(String name, Object value) {
		setName(name);
		setValue(value);
	}
	
	protected Meta cloneThis() {
		return JsonUtils.fromJson(JsonUtils.objectToString(this), Meta.class);
	}
	
	public String getData() {
		return JsonUtils.objectToStringPretty(this);
	}
	
	@Override
	public String toString() {
		return String.format("%s = %s%s", getName(), 
				getValue() != null ? getValue().toString() : "", 
				getDescription() != null ? " [" + getDescription() + "]" : "");
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String) {
			return getName().equals((String) obj);
		}
		return super.equals(obj);
	}

	public String getName() {
		return name;
	}

	public Meta setName(String name) {
		this.name = name;
		return this;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Double getAsDouble() {
//		if (getValue() instanceof JsonPrimitive)
//			return ((JsonPrimitive) getValue()).getAsDouble();
//		return (Double) getValue();
		if (getValue() instanceof Double)
			return (Double) getValue();
//		else if (getValue() instanceof Double)
//			return (int) ((double) getValue());
		else
			return NumberUtils.toDouble(String.valueOf(getValue()));
	}
	
	public String getAsString() {
		if (getValue() instanceof String)
			return (String) getValue();
		else if (getValue() != null)
			return getValue().toString();
		else
			return null;
	}
	
	public Long getAsLong() {
		return (Long) getValue();
	}
	
	public Integer getAsInteger() {
		if (getValue() instanceof Integer)
			return (Integer) getValue();
//		else if (getValue() instanceof Double)
//			return (int) ((double) getValue());
		else
			return NumberUtils.toInt(String.valueOf(getValue()), 0);
	}

	public boolean isNameSpecified() {
		return StringUtilities.notEmpty(name);
	}
	
	public boolean isDescriptionSpecified() {
		return StringUtilities.notEmpty(description);
	}

	public Boolean getAsBoolean() {
		if (getValue() == null)
			return null;
		else if (getValue() instanceof Boolean)
			return (Boolean) getValue();
		else
			return getAsString().equalsIgnoreCase("true");
	}

	public boolean isValueDouble() {
		if (getValue() != null && getValue() instanceof Double)
			return true;
		
		if (getValue() != null && NumberUtils.isDouble(getAsString()))
			return true;
		
		return false;
	}

//	public UUID getUserId() {
//		return userId;
//	}
//
//	public void setUserId(UUID userId) {
//		this.userId = userId;
//	}
}
