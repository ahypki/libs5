package net.hypki.libs5.utils.db;

import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.StringUtilities;


public class BlobUtils {
	
	public static String blobToString(Blob blob) throws IOException {
		try {
			return new String(blob.getBytes(1, (int) blob.length()));
		} catch (SQLException e) {
			throw new IOException("Cannot convert blob to string", e);
		}
	}
	
//	public static Clob toClob(String text) throws SerialException, SQLException {
//		return Hibernate.createClob(text);
//	}

	public static Blob toBlob(String text) throws SerialException, SQLException {
		return new SerialBlob(text.getBytes());
	}
	
	public static SerialBlob getBlobFromFile(File file) throws IOException, SerialException, SQLException {
		return new SerialBlob(FileUtils.getBytesFromFile(file));
	}

	public static void saveToFile(Blob blob, File outFile) throws IOException, SQLException {
		if (blob == null)
			throw new IOException("Can not save null object");
		FileUtils.saveToFile(blob.getBinaryStream(), outFile);
	}

	public static Blob toBlob(char[] chars) throws SerialException, SQLException {
		return new SerialBlob(StringUtilities.toByte(chars));
	}

	public static char [] toChar(Blob blob) {
		try {
			if (blob == null) {
				return null;
			}
			return StringUtilities.toChar(blob.getBytes(1, (int) blob.length()));
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
