package net.hypki.libs5.utils.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import net.hypki.libs5.utils.LibsLogger;

public class BigFilePartial implements Iterable<String> {
	private FileInputStream fis;
	private BufferedInputStream bis;

	public BigFilePartial(String filePath) throws IOException {
		if (filePath.endsWith("gz")) {
			fis = new FileInputStream(filePath);
		    GZIPInputStream gzis = new GZIPInputStream(fis);
//		    if (skipBytes > 0)
//		    	gzis.skip(skipBytes);
//		    InputStreamReader xover = new InputStreamReader(gzis);
//		    return new BufferedReader(xover, 256000);
		    bis = new BufferedInputStream(gzis);
		} else {
			fis = new FileInputStream(filePath);
			bis = new BufferedInputStream(fis);
	    }
	}

	public BigFilePartial(InputStream is) throws IOException {
		bis = new BufferedInputStream(is);
	}

	public void close() {
		try {
			bis.close();
			fis.close();
		} catch (Exception ex) {
			LibsLogger.error(BigFilePartial.class, "Cannot close files");
		}
	}
	
	public Iterator<String> iterator() {
		return iterator(0, 1).iterator();
	}

	public Iterable<String> iterator(final int part, final int partsCount) {
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {
					private static final int SIZE = 128_000;
					private String line = null;
					
					private byte[] cache = new byte[SIZE];
					private int cacheIdx = -1;
					
					private long filesize = 0L;
					private long partMaxBytesToRead = 0L;
					private long partReadBytes = 0L;
					private boolean skipPartialLine = false;
					
					@Override
					public String next() {
						try {
							if (line == null)
								hasNext();
							
							return line;
						} finally {
							line = null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (cacheIdx == -1)
							readBlock();
						
						if (partsCount > 1 && partReadBytes > partMaxBytesToRead)
							return false;
						
						while (true) {
							int lineStop = -1;
							for (int i = cacheIdx; i < cache.length; i++) {
								if (cache[i] == '\n') {
									if (skipPartialLine) {
										skipPartialLine = false;
										
										lineStop = i;
										partReadBytes += lineStop - cacheIdx;
										partReadBytes += 1; // new line
										cacheIdx = i + 1;
										continue;
									}
									
									lineStop = i;
									
									// create line without new line character
									line = new String(cache, cacheIdx, lineStop - cacheIdx);
									
									partReadBytes += lineStop - cacheIdx;
									partReadBytes += 1; // new line
									
									cacheIdx = i + 1;
									return true;
								}
							}
							
							// could not find new line
							if (readBlock() == 0)
								break;
						}
						
						return false;
					}
					
					private int readBlock() {
						if (cacheIdx == -1) {
							try {
								if (partsCount > 1) {
									// skipping some data from the beginning
									if (filesize == 0) {
										filesize = fis.getChannel().size();
										partMaxBytesToRead = filesize / partsCount;
									}
									bis.skip(partMaxBytesToRead * part);
									if (part > 0)
										skipPartialLine = true;
								}
								
								// initial read of the first block
								int cacheRead = bis.read(cache, 0, SIZE);
								cacheIdx = 0;
								return cacheRead;
							} catch (IOException e) {
								LibsLogger.error(BigFilePartial.class, "Cannot read cache", e);
							}
						}
						
						System.arraycopy(cache, cacheIdx, cache, 0, SIZE - cacheIdx);
						try {
							int s = SIZE - (SIZE - cacheIdx);
							byte[] newCache = new byte[s];
							int cacheRead = bis.read(newCache);
							System.arraycopy(newCache, 0, cache, SIZE - cacheIdx, s);
							cacheIdx = 0;
							return cacheRead;
						} catch (IOException e) {
							LibsLogger.error(BigFilePartial.class, "Cannot read cache", e);
						}
						
						return 0;
					}
				};
			}
		};
	}
}