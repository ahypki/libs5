package net.hypki.libs5.utils.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.utils.LibsLogger;

public class LRUWeightedMap {
	
	private double totalWeight = 0.0;
	
	private double overWeight = 0.0;
	
	private Map<String, Double> keyToWeight = new HashMap<>();
	
	private Map<String, Long> keyToTime = new HashMap<>();
	
	private List<LRUWeightedMapCallback> onRemoveCallbacks = null;

	public LRUWeightedMap() {
		
	}
	
	public LRUWeightedMap(double overWeight, LRUWeightedMapCallback onRemoveCallback) {
		setOverWeight(overWeight);
		if (onRemoveCallback != null)
			getOnRemoveCallbacks().add(onRemoveCallback);
	}
	
	public LRUWeightedMap put(String key, double weight) {
		if (getKeyToWeight().containsKey(key)) {
			removeFromTotalWeight(getKeyToWeight().get(key));
		}
		
		getKeyToWeight().put(key, weight);
		getKeyToTime().put(key, System.currentTimeMillis());
		addToTotalWeight(weight);
		return this;
	}

	private double getTotalWeight() {
		return totalWeight;
	}

	private void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}
	 
	private void addToTotalWeight(double weight) {
		setTotalWeight(getTotalWeight() + weight);
		
		if (getTotalWeight() >= getOverWeight())
			removeLRU();
	}
	
	private void removeFromTotalWeight(double weight) {
		setTotalWeight(getTotalWeight() - weight);
	}
	
	private void removeLRU() {
		// sort keys by the last access time
		List<String> keys = new ArrayList<String>();
		keys.addAll(getKeyToTime().keySet());
		Collections.sort(keys, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				long l1 = getKeyToTime().get(o1);
				long l2 = getKeyToTime().get(o2);
				if (l1 < l2)
					return -1;
				else if (l1 > l2)
					return 1;
				else
					return 0;
			}
		});
		
		// removing until overweighted elements are removed
		for (String key : keys) {
			// remove one element, which lowers the total weight, and then check the threshold
			remove(key);
			
			if (getTotalWeight() <= (getOverWeight() * 0.9))
				break;
		}
		
		LibsLogger.debug(LRUWeightedMap.class, "LRU cleaned, size ", size(), ", weight ", getTotalWeight());
	}
	
	public boolean contains(String key) {
		return getKeyToTime().containsKey(key);
	}
	
	public double getWeight(String key) {
		if (contains(key))
			return getKeyToWeight().get(key);
		return 0.0;
	}
	
	public void remove(String key) {
		removeFromTotalWeight(getKeyToWeight().get(key));
		getKeyToTime().remove(key);
		getKeyToWeight().remove(key);
		if (this.onRemoveCallbacks != null)
			for (LRUWeightedMapCallback callback : getOnRemoveCallbacks()) {
				callback.onRemove(key);
			}
	}
	
	public int size() {
		return getKeyToTime().size();
	}

	private double getOverWeight() {
		return overWeight;
	}

	private void setOverWeight(double overWeight) {
		this.overWeight = overWeight;
	}

	private List<LRUWeightedMapCallback> getOnRemoveCallbacks() {
		if (this.onRemoveCallbacks == null)
			this.onRemoveCallbacks = new ArrayList<LRUWeightedMapCallback>();
		return onRemoveCallbacks;
	}

	private void setOnRemoveCallbacks(List<LRUWeightedMapCallback> onRemoveCallbacks) {
		this.onRemoveCallbacks = onRemoveCallbacks;
	}

	private Map<String, Double> getKeyToWeight() {
		return keyToWeight;
	}

	private void setKeyToWeight(Map<String, Double> keyToWeight) {
		this.keyToWeight = keyToWeight;
	}

	private Map<String, Long> getKeyToTime() {
		return keyToTime;
	}

	private void setKeyToTime(Map<String, Long> keyToTime) {
		this.keyToTime = keyToTime;
	}
}
