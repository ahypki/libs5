package net.hypki.libs5.utils.url;

//import javax.ws.rs.core.Response;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.Response;

import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

// TODO remove this class, there is class javax.ws.rs.core.Response
@Deprecated
public class HttpResponse {

	@Expose
	private int code = 200;
	
	// TODO remove it
	@Expose
	private String message = null;
	
	@Expose
	private String body = null;
	
	private JsonElement bodyJson = null;
	
	public HttpResponse() {
		
	}
	
	public HttpResponse(int statusCode, String body) {
		setCode(statusCode);
		setBody(body);
	}
	
	@Override
	public String toString() {
		return JsonUtils.objectToString(this);
	}

	public int getCode() {
		return code;
	}

	public HttpResponse setCode(int code) {
		this.code = code;
		return this;
	}
	
//	public RestResponse setCode(Response.Status code) {
//		this.code = code.getStatusCode();
//		return this;
//	}

	public String getMessage() {
		return message;
	}

	public HttpResponse setMessage(String message) {
		this.message = message;
		return this;
	}

	public String getBody() {
		return body;
	}

	public HttpResponse setBody(String body) {
		this.body = body;
//		this.bodyJson = null;
		return this;
	}
	
	public HttpResponse setBody(Object body) {
		this.body = JsonUtils.objectToStringPretty(body);
		return this;
	}
	
	private HttpResponse initBodyAsObject() {
		setBody("{}");
		this.bodyJson = null;
		return this;
	}
	
	private HttpResponse initBodyAsArray() {
		setBody("[]");
		this.bodyJson = null;
		return this;
	}

	public JsonElement getAsJson() {
		if (bodyJson == null) {
			if (body != null)
				bodyJson = JsonUtils.parseJson(body); 
		}
		return bodyJson;
	}
	
	public boolean isFailed() {
		return getCode() >= 400;
	}
	
	public boolean isOK() {
		return getCode() >= 200 && getCode() <= 204;
	}

	public boolean getAsBoolean(String path) {
		Boolean b = JsonUtils.readBoolean(getAsJson(), path);
		return b != null ? b : false;
	}
	
	public boolean getAsBoolean(String path, boolean defaultValue) {
		Boolean b = JsonUtils.readBoolean(getAsJson(), path, defaultValue);
		return b != null ? b : false;
	}
	
	public long getAsLong(String path) {
		return JsonUtils.readLong(getAsJson(), path, 0L);
	}

	public HttpResponse set(String path, boolean value) {
		if (this.body == null)
			initBodyAsObject();
		JsonUtils.putSetting(getAsJson(), path, value);
		setBody(getAsJson().toString());
		return this;
	}
	
	public HttpResponse set(String path, int value) {
		if (this.body == null)
			initBodyAsObject();
		JsonUtils.putSetting(getAsJson(), path, value);
		setBody(getAsJson().toString());
		return this;
	}
	
	public HttpResponse set(String path, long value) {
		if (this.body == null)
			initBodyAsObject();
		JsonUtils.putSetting(getAsJson(), path, value);
		setBody(getAsJson().toString());
		return this;
	}
	
	public HttpResponse set(String path, String value) {
		if (this.body == null)
			initBodyAsObject();
		JsonUtils.putSetting(getAsJson(), path, value);
		setBody(getAsJson().toString());
		return this;
	}
	
	public HttpResponse set(String path, JsonElement value) {
		if (this.body == null)
			initBodyAsObject();
		JsonUtils.putSetting(getAsJson(), path, value);
		setBody(getAsJson().toString());
		return this;
	}

	public String getAsString(String path) {
		return JsonUtils.readString(getAsJson(), path);
	}
	
	public String getAsString(String path, String defaultValue) {
		String tmp = JsonUtils.readString(getAsJson(), path);
		return tmp != null ? tmp : defaultValue;
	}

	public boolean isBodyArray() {
		return getAsJson().isJsonArray();
	}

	public HttpResponse add(String path, Object o) {
		if (this.body == null) {
			if (nullOrEmpty(path) || path.trim().equals("/"))
				initBodyAsArray();
			else
				initBodyAsObject();
		}
		if (o instanceof Collection) {
			for (Object o2 : (Collection) o)
				JsonUtils.add(getAsJson(), path, o2);
		} else
			JsonUtils.add(getAsJson(), path, o);
		setBody(getAsJson().toString());
		return this;
	}

	public <T> T getAsObject(Class<T> clazz) {
		if (notEmpty(getBody()))
			return JsonUtils.fromJson(getAsJson(), clazz);
		else 
			return null;
	}
	
	public <T> List<T> getAsList(String bodyJsonPath, Class<T> clazz) {
		return JsonUtils.readList(getAsJson(), bodyJsonPath, clazz);
	}

	public HttpResponse ok() {
		setCode(200);
		return this;
	}

	public Response toResponse() {
		return Response
			.ok()
			.entity(JsonUtils.objectToString(this))
			.build();
	}

	public SimpleDate getAsDate(String jsonPath) {
		String v = getAsString(jsonPath);
		
		if (notEmpty(v)) {
			if (NumberUtils.isNumber(v)) {
				return new SimpleDate(NumberUtils.toLong(v));
			} else {
				return SimpleDate.parse(v);
			}
		}
		
		return null;
	}

	public boolean isBodyEmpty() {
		return notEmpty(getBody());
	}
}
