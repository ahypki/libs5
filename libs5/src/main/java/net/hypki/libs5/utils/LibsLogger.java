package net.hypki.libs5.utils;

import java.net.URL;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Initiates automatically from log4j-weblibs.properties from jar (getClassLoader()) and if it fails in CLASSPATH  
 * @author ahypki
 *
 */
public class LibsLogger {
	
	public static final String LOG4J_SYSTEM_PROPERTY_NAME = "log4j";
	
	private static Logger libLogger = null;
	
	static {
		String log4jFilename = System.getProperty(LOG4J_SYSTEM_PROPERTY_NAME) != null ? 
				System.getProperty(LOG4J_SYSTEM_PROPERTY_NAME) : "log4j.properties";

		init(log4jFilename);
		
		libLogger = Logger.getLogger(LibsLogger.class);
	}
	
	public static void init(final String log4jFilename) {
		long start = System.currentTimeMillis();
		
		// search for log4j.properties in jar using getClassLoader()
		URL url = LibsLogger.class.getResource(log4jFilename);
		
//		System.out.println("log4jFilename " + log4jFilename);
//		System.out.println("url " + (url != null ? url.toString() : ""));
		
		if (url == null) {
			// search for log4j.properties in CLASSPATH
			url = LibsLogger.class.getClassLoader().getResource(log4jFilename);
		}
		
		if (url == null)
			throw new RuntimeException("Cannot find " + url + " file in CLASSPATH");
		
//		System.out.println("url " + (url != null ? url.toString() : ""));
		
		// init log4j
		PropertyConfigurator.configure(url);
		
		trace(LibsLogger.class, "LibsLogger started in " + (System.currentTimeMillis() - start));
		debug(LibsLogger.class, "LibsLogger configuration file read from " + url.toString());
		debug(LibsLogger.class, "Libs4 code version " + LibsConst.VERSION);
	}
	
	public static String toString(Object ... message) {
		StringBuilder b = new StringBuilder(200);
		for (Object obj : message) {
			if (obj == null)
				continue;
			
			b.append(obj);
							
			if (!(obj instanceof String))
				b.append(" ");
		}
		return b.toString();
	}
	
	public static void trace(Class<?> clazz, String message) {
		Logger.getLogger(clazz).trace(message);	
	}
	
	public static void trace(Class<?> clazz, Object ... message) {
		if (Logger.getLogger(clazz).isEnabledFor(Level.TRACE)) {
			trace(clazz, toString(message));
		}
	}
	
	public static void debug(Class<?> clazz, Object ... message) {
		if (Logger.getLogger(clazz).isDebugEnabled()) {
			debug(clazz, toString(message));
		}
	}

	public static void debug(String message) {
		libLogger.debug(message);
	}

	public static void debug(Class<?> clazz, String message) {
		Logger.getLogger(clazz).debug(message);
	}

	public static void debug(Class<?> clazz, String message, Throwable t) {
		Logger.getLogger(clazz).debug(message, t);
	}

	public static void info(Class<?> clazz, Object ... message) {
		info(clazz, toString(message));
	}
	
	public static void info(String message) {
		libLogger.info(message);
	}

	public static void info(String message, Throwable t) {
		libLogger.info(message, t);
	}

	public static void info(Class<?> clazz, String message, Throwable t) {
		libLogger.info(message, t);
	}

	public static void info(Class<?> clazz, String message) {
		Logger.getLogger(clazz).info(message);
	}

	public static void warn(Class<?> clazz, String message) {
		Logger.getLogger(clazz).warn(message);
	}

	public static void warn(Class<?> clazz, String message, Throwable t) {
		Logger.getLogger(clazz).warn(message, t);
	}
	
	public static void warn(Class<?> clazz, Object ... message) {
		Logger.getLogger(clazz).warn(toString(message));
	}

	public static void error(Class<?> clazz, Object ... message) {
		error(clazz, toString(message));
	}
	
	public static void error(Class<?> clazz, Throwable t, Object ... message) {
		error(clazz, toString(message), t);
	}

	public static void error(String message) {
		libLogger.error(message);
	}
		
	public static void error(String message, Throwable t) {		
		libLogger.error(message, t);
	}
	
	public static void error(Throwable t) {
		libLogger.error(null, t);
	}
	
	public static void error(Class<?> clazz, String message, Throwable t) {
		Logger.getLogger(clazz).error(message, t);
	}
	
	public static void error(Class<?> clazz, String message) {
		Logger.getLogger(clazz).error(message);
//		Logger.getLogger(clazz).error("------------------------------------------------------------------------------------------------");
//		Logger.getLogger(clazz).error("|");
//		Logger.getLogger(clazz).error("|\t\t" + message);
//		Logger.getLogger(clazz).error("|");
//		Logger.getLogger(clazz).error("------------------------------------------------------------------------------------------------");
	}
		
	public static void fatal(String message, Throwable t) {		
		libLogger.fatal(message, t);
	}
	
	public static void fatal(Class<?> clazz, String message, Throwable t) {		
		Logger.getLogger(clazz).fatal(message, t);
	}
}
