package net.hypki.libs5.utils.file;

import static java.lang.Math.abs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang.NotImplementedException;

import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.hypki.libs5.utils.utils.NumberUtils;


public class FuzzyFileComparator {
	final double doubleDiff = 1.0e-6;
	
	BufferedReader aReader = null;
	BufferedReader bReader = null;
	TrickyString tsA = null;
	TrickyString tsB = null;
	String lineA = null;
	String lineB = null;
	
	public FuzzyFileComparator(String path1, String path2) throws FileNotFoundException {		
		aReader = new BufferedReader(new FileReader(path1));
		bReader = new BufferedReader(new FileReader(path2));
	}
	
	private TrickyString getTsA() throws IOException {
		if (tsA != null && tsA.isEmpty() == false)
			return tsA;
			
		lineA = aReader.readLine();
		if (lineA == null)
			return new TrickyString("");
		if (tsA == null || tsA.isEmpty())
			tsA = new TrickyString(lineA.toLowerCase());
		return tsA;
	}

	private TrickyString getTsB() throws IOException {
		if (tsB != null && tsB.isEmpty() == false)
			return tsB;
		
		lineB = bReader.readLine();
		if (lineB == null)
			return new TrickyString("");
		if (tsB == null || tsB.isEmpty())
			tsB = new TrickyString(lineB.toLowerCase());
		return tsB;
	}
	
	public void run() throws IOException {
		
		while (true) {
			
			if (getTsA().isEmpty())
				break;
			else if (getTsA().checkNextNumber() != null) {
				// comparing numbers
				String aNum = getTsA().getNextNumber().replaceAll("[dD]", "e");
				String bNum = getTsB().getNextNumber().replaceAll("[dD]", "e");
				
				if (aNum.contains(".") || aNum.contains("e") || bNum.contains(".") || bNum.contains("e")) {
					// comparing doubles
					double a = NumberUtils.toDouble(aNum);
					double b = NumberUtils.toDouble(bNum);
					if (abs(a - b) > doubleDiff)
						linesAreDifferent("FORTRAN " + a + " C " + b + " -> (a - b) = " + (a - b));
					else
						theSame(a, b);
				} else {
					// comparing integers
					long a = NumberUtils.toLong(aNum);
					long b = NumberUtils.toLong(bNum);
					if (a != b)
						linesAreDifferent("FORTRAN " + a + " C " + b + " -> (a - b) = " + (a - b));
					else
						theSame(a, b);
				}
					
				
			} else if (getTsA().checkNextAlphanumericWord() != null) {
				String wa = getTsA().getNextAlphanumericWord();
				String wb = getTsB().getNextAlphanumericWord();
				if (wa.startsWith("inf") && wb.startsWith("inf"))
					theSame(wa, wb);
				else if (wa.equals(wb) == false)
					linesAreDifferent("" + wa + "  " + wb);
				else
					theSame(wa, wb);
			} else if (getTsA().checkNextPrintableCharacter() != null) {
				String ca = getTsA().getNextPrintableCharacter();
				String cb = getTsB().getNextPrintableCharacter();
				if (ca.equals(cb) == false)
					linesAreDifferent("" + ca + "  " + cb);
				else
					theSame(ca, cb);
			} else {
				System.out.println("not implemented: " + getTsA().checkLine());
				throw new NotImplementedException();
			}
		}
		
//		while (true) {
//			TrickyString tsA = new TrickyString(aReader.readLine());
//			TrickyString tsB = new TrickyString(bReader.readLine());
//						
//			String aParts[] = StringUtil.splitc(aLine.toLowerCase().trim(), ' ');
//			String bParts[] = StringUtil.splitc(bLine.toLowerCase().trim(), ' ');
//			
//			for (int i = 0; i < aParts.length; i++) {
//				if (aParts.length != bParts.length) {
//					linesAreDifferent(aLine, bLine);
//					break; // go to the next line
//				}
//				
//				String a = aParts[i];
//				String b = bParts[i];
//				
//				if (a.matches("[\\w]+[\\w\\d\\-\\+\\=]+")) {
//					// comparing words
//					if (a.equals(b) == false) {
//						linesAreDifferent(a, b);
//					}
//				} else if (a.matches("[\\d]+\\.[\\ded+]+")) {
//					// comparing double
//					if (abs(parseDouble(a) - parseDouble(b)) > doubleDiff) {
//						linesAreDifferent(a, b);
//					}
//				} else if (a.matches("[\\d]+")) {
//					// comparing integers
//					if (a.equals(b) == false) {
//						linesAreDifferent(a, b);
//					}
//				} else {
//					throw new RuntimeException("Unimplemented case for string: " + aParts[i] + ", line: " + aLine);
//				}
//			}
//		}
		
		System.out.println("Finished! Files are fuzzy indentical!");
		
	}
	
	private void theSame(Object a, Object b) {
		System.out.println(a + " == " + b);
	}

	private void linesAreDifferent(String message) throws IOException {
		System.out.println(message);
		System.out.println("FORTRAN: " + StringUtilities.substringMax(getTsA().checkLine(), 0, 20));
		System.out.println("C: " + StringUtilities.substringMax(getTsB().checkLine(), 0, 20));
		System.out.println("FORTRAN LINE: " + lineA);
		System.out.println("C       LINE: " + lineB);
		System.exit(1);
	}
	
	public static void main(String[] args) throws RuntimeException, Throwable {
		new FuzzyFileComparator(args[0], args[1]).run();
	}
}
