package net.hypki.libs5.utils.system;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;

import net.hypki.libs5.utils.LibsLogger;

public class GCUtils {

	public static void printMemoryUsage() {

		try {
			MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
			MemoryUsage heap = memBean.getHeapMemoryUsage();
			MemoryUsage nonHeap = memBean.getNonHeapMemoryUsage();
			// Retrieve the four values stored within MemoryUsage:
			// init: Amount of memory in bytes that the JVM initially requests
			// from the OS.
			// used: Amount of memory used.
			// committed: Amount of memory that is committed for the JVM to use.
			// max: Maximum amount of memory that can be used for memory
			// management.
			LibsLogger.debug(GCUtils.class,
					String.format(
							"Heap: Init: %dM, Used: %dM, Committed: %dM, Max.: %dM  ||  "
							+ "Non-Heap: Init: %dM, Used: %dM, Committed: %dM, Max.: %dM",
							heap.getInit() / 1000000, heap.getUsed() / 1000000, heap.getCommitted() / 1000000,
							heap.getMax() / 1000000,
							nonHeap.getInit() / 1000000, nonHeap.getUsed() / 1000000, nonHeap.getCommitted() / 1000000,
							nonHeap.getMax() / 1000000));
		} catch (Throwable e) {
			LibsLogger.error(GCUtils.class, "Cannot clear memory", e);
		}
	}
}
