package net.hypki.libs5.utils.file;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;

import com.google.common.io.Files;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.Basher;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;



public class FileUtils {
	
	private static MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
	
	public static String tail(File file, long maxBytes) {
		try {
			StringBuilder sb = new StringBuilder();
			
			for (String line : new FileExt(file)) {
				sb.append(line);
				sb.append("\n");
				
				if (sb.length() * 2 /*byter per char*/ > maxBytes) {
					int idx = sb.indexOf("\n");
					if (idx >= 0)
						sb.delete(0, idx + 1);
				}
			}
			
			return sb.toString();
		} catch (Throwable t) {
			LibsLogger.error(FileUtils.class, "Cannot tail the file", t);
			return null;
		}
	}

	public static String tail(File file) {
		RandomAccessFile fileHandler = null;
		try {
			fileHandler = new RandomAccessFile(file, "r");
			long fileLength = fileHandler.length() - 1;
			StringBuilder sb = new StringBuilder();

			for (long filePointer = fileLength; filePointer != -1; filePointer--) {
				fileHandler.seek(filePointer);
				int readByte = fileHandler.readByte();

				if (readByte == 0xA) {
					if (filePointer == fileLength) {
						continue;
					}
					break;

				} else if (readByte == 0xD) {
					if (filePointer == fileLength - 1) {
						continue;
					}
					break;
				}

				sb.append((char) readByte);
			}

			String lastLine = sb.reverse().toString();
			return lastLine;
		} catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (java.io.IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (fileHandler != null)
				try {
					fileHandler.close();
				} catch (IOException e) {
					LibsLogger.error(FileUtils.class, "Cannot close the file", e);
				}
		}
	}
	
	public static boolean isDirectory(String ... pathParts) throws IOException {
		return new FileExt(pathParts).isDirectory();
	}
	
	public static boolean isSymlink(File file) throws IOException {
		if (file == null)
			throw new NullPointerException("File must not be null");
		File canon;
		if (file.getParent() == null) {
			canon = file;
		} else {
			File canonDir = file.getParentFile().getCanonicalFile();
			canon = new File(canonDir, file.getName());
		}
		return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
	}
	
	public static int countLines(String filename) throws IOException {
		return countLines(new BufferedInputStream(new FileInputStream(filename)));
	}

	public static int countLines(InputStream is) throws IOException {
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}
	
	public static String getFilePosixPermissions(Path path) {
		Set<PosixFilePermission> filePerm = null;
		try {
		    filePerm = java.nio.file.Files.getPosixFilePermissions(path);
		} catch (IOException e) {
			LibsLogger.error(FileUtils.class, "Cannot check file " + path + " for permissions", e);
		}
		return PosixFilePermissions.toString(filePerm);
	}
	
	public static long getCreateDate(String path) {
		try {
			BasicFileAttributes attr = java.nio.file.Files.readAttributes(Paths.get(path), BasicFileAttributes.class);
			return attr.creationTime().toMillis();
		} catch (Throwable e) {
			LibsLogger.error(FileUtils.class, "Cannot get last modification date", e);
			return -1;
		}
	}

	public static long getLastEditDate(String path) {
		try {
			BasicFileAttributes attr = java.nio.file.Files.readAttributes(Paths.get(path), BasicFileAttributes.class);
			return attr.lastModifiedTime().toMillis();
		} catch (IOException e) {
			LibsLogger.error(FileUtils.class, "Cannot get last modification date", e);
			return -1;
		}
	}
	
	public static void deleteFiles(File startingFolder, String fileRegexPattern) {
		for (File file : startingFolder.listFiles()) {
			 if (file.isFile() && file.getName().matches(fileRegexPattern))
				 file.delete();
		}
	}
	
	public static void setFilePerm(String file, String permsLinuxFormat) throws IOException {
		setFilePerm(new File(file), permsLinuxFormat);
	}
	
	public static void setFilePerm(File file, String permsLinuxFormat) throws IOException {
		Basher.exec(new String[] {"chmod", permsLinuxFormat, file.getAbsolutePath(),});
	}
	
	public static String getMimeType(File file) {
		return mimeTypesMap.getContentType(file);
	}
	
	public static String getMimeType(String file) {
		return getMimeType(new File(file));
	}
	
	public static String getMimeTypeMagic(String file) {
		return getMimeTypeMagic(new File(file));
	}
	
	public static String getMimeTypeMagic(File f) {
		try {
			MagicMatch match = Magic.getMagicMatch(f, false);
			return match.getMimeType();
		} catch (MagicParseException e) {
			LibsLogger.error(FileUtils.class, "Cannot determine mimetype of the file " + f, e);
			return null;
		} catch (MagicMatchNotFoundException e) {
			LibsLogger.error(FileUtils.class, "Cannot determine mimetype of the file " + f, e);
			return null;
		} catch (MagicException e) {
			LibsLogger.error(FileUtils.class, "Cannot determine mimetype of the file " + f, e);
			return null;
		}
	}
	
	public static String readString(File file) {
		try {
			return new FileExt(file).readString();
		} catch (IOException e) {
			LibsLogger.error(FileUtils.class, "Cannot read content of the file " + file.getAbsolutePath(), e);
			return null;
		}
	}
	
	public static String readString(File file, Charset charset) throws IOException {
		return Files.toString(file, charset == null ? Charset.defaultCharset() : charset);
	}
	
	public static String readString(InputStream is) {
		Writer writer = new StringWriter();

		char[] buffer = new char[1024];
		try {
			Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int n;
			while ((n = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, n);
			}
		} catch (UnsupportedEncodingException e) {
			LibsLogger.error("Cannot read from stream", e);
		} catch (IOException e) {
			LibsLogger.error("Cannot read from stream", e);
		} finally {
			try {
				is.close();
				is = null;
			} catch (IOException e) {
				LibsLogger.error("Cannot close stream", e);
			}
		}
		return writer.toString();
	}
	
	public static boolean exist(String ... pathParts) {
		String tmpPath = concatPaths(pathParts);
		return new FileExt(tmpPath).exists();
	}
	
	public static BufferedImage downloadImage(String urlLocation) throws IOException {
		InputStream in = null;
		try {
			URL url = new URL(urlLocation);
			
			URLConnection conn = url.openConnection();
			in = conn.getInputStream();
			
			return ImageIO.read(in);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}
	
	/**
	 * Source code snippet downloaded from: http://www.spartanjava.com/2008/downloadToFile-a-file-using-java/
	 * @param address
	 * @param localFileName
	 * @throws IOException
	 */
	public static String downloadToString(String address) throws IOException {
//		return new WebPageFetcher(address).getPageContent();
		StringBuilder builder = new StringBuilder(50000);
		URLConnection conn = null;
		InputStream in = null;
		try {
			// Get the URL
			URL url = new URL(address);
			
			// Open an output stream to the destination file on our local filesystem
			conn = url.openConnection();
			in = conn.getInputStream();

			// Get the data
			byte[] buffer = new byte[10024];
			int numRead;
			while ((numRead = in.read(buffer)) != -1) {
				builder.append(new String(buffer, 0, numRead));
			}
			
			return builder.toString();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}
	
	/**
	 * Source code snippet downloaded from: http://www.spartanjava.com/2008/downloadToFile-a-file-using-java/
	 * @param address
	 * @param localFileName
	 * @throws IOException
	 */
	public static void downloadToFile(String address, String localFileName) throws IOException {
		OutputStream out = null;
		URLConnection conn = null;
		InputStream in = null;
		try {
			// Get the URL
			URL url = new URL(address);
			
			// Open an output stream to the destination file on our local filesystem
			out = new BufferedOutputStream(new FileOutputStream(localFileName));
			conn = url.openConnection();
			in = conn.getInputStream();

			// Get the data
			byte[] buffer = new byte[5024];
			int numRead;
			while ((numRead = in.read(buffer)) != -1) {
				out.write(buffer, 0, numRead);
			}
			
			out.flush();
		} finally {
			if (out != null) {
				out.close();
			}
			if (in != null) {
				in.close();
			}
		}
	}
	
	public static List<File> getFiles(File startingFolder, boolean recursively) {
		return getFiles(startingFolder, recursively, true);
	}
	
	public static List<File> getFiles(File startingFolder, boolean recursively, final boolean withSymlinks) {
		final ArrayList<File> files = new ArrayList<File>();
		ArrayList<File> folders = getFolders(startingFolder, recursively, withSymlinks);
		folders.add(startingFolder);
		for (File folder : folders) {
			folder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					if (file.isFile()) {
						try {
							boolean isSymlink = FileUtils.isSymlink(file);
							if (!isSymlink || (isSymlink && withSymlinks))
								files.add(file);
						} catch (IOException e) {
							LibsLogger.error(FileUtils.class, "Cannot check if " + file + " is a symlink", e);
						}
					}
					return false;
				}
			});
			
		}
		
		return files;
	}
	
	public static ArrayList<File> getFiles(File startingFolder, boolean recursively, FileFilter fileFilter) {
		final ArrayList<File> files = new ArrayList<File>();
		ArrayList<File> folders = recursively ? getFolders(startingFolder, recursively) : new ArrayList<File>();
		folders.add(startingFolder);
		for (File folder : folders) {
			for (File f : folder.listFiles(fileFilter))
				files.add(f);
		}
		return files;
	}
	
	public static ArrayList<File> getFolders(File startingFolder, boolean recursively) {
		return getFolders(startingFolder, recursively, false);
	}
	
	public static ArrayList<File> getFolders(File startingFolder, boolean recursively, final boolean withSymlinks) {
		ArrayList<File> listAr = new ArrayList<File>();
		// uses FileFilter to just pull out specific directories, then puts in an array
		File[] list = startingFolder.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				if (file.isDirectory()) {
					try {
						boolean isSymlink = FileUtils.isSymlink(file);
						if (!isSymlink || (isSymlink && withSymlinks))
							return true;
					} catch (IOException e) {
						LibsLogger.error(FileUtils.class, "Cannot check if " + file + " is a symlink", e);
					}
				}
				return false;
			}
		});
		
		if (recursively == true) {
			for (int i = 0; list != null && i < list.length; i++) {
				listAr.addAll(getFolders(list[i], recursively, withSymlinks));
			}
		}

		// put all items from the array into the arraylist
		for (int i = 0; list != null && i < list.length; i++)
			listAr.add(list[i]);

		return listAr;
	}
	
	public static ArrayList<File> getFolders(File startingFolder, boolean recursively, FileFilter dirFilter) {
		ArrayList<File> listAr = new ArrayList<File>();

		// uses FileFilter to just pull out directories, then puts in an array
		File[] list = startingFolder.listFiles(dirFilter);
		
		if (recursively == true && list != null) {
			for (int i = 0; i < list.length; i++) {
				listAr.addAll(getFolders(list[i], recursively, dirFilter));
			}
		}

		// puts all the items from the array in an arraylist
		if (list != null) {
			for (int i = 0; i < list.length; i++)
				listAr.add(list[i]);
		}

		return listAr;
	}
	
	public static InputStream getInputStream(File file) throws IOException {
		return new FileInputStream(file);
	}
		
	/**
	 * Returns the contents of the file in a byte array.
	 * @source http://www.exampledepot.com/egs/java.io/File2ByteArray.html
	 */
	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = null;

		try {
			is = new FileInputStream(file);
			
			// Create the byte array to hold the data
			byte[] bytes;
			// Get the size of the file
			long length = file.length();

			// You cannot create an array using a long type. It needs to be an int type.
			// Before converting to an int type, check to ensure that file is not larger 
			// than Integer.MAX_VALUE.
			if (length > Integer.MAX_VALUE) {
				throw new IOException("File is too large to read");
			}

			bytes = new byte[(int) length];

			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}

			// Ensure all the bytes have been read in
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}
			
			return bytes;
		} finally {
			if (is != null)
				// Close the input stream and return bytes
				is.close();
		}
	}
	
	public static long saveToFile(InputStream inputStream, File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file, false);
		byte[] buf = new byte[1000000];
		int numRead = 0;
		long numReadTotal = 0;
		while ((numRead = inputStream.read(buf)) > 0) {
			fos.write(buf, 0, numRead);
			numReadTotal += numRead;
		}
		fos.close();
		return numReadTotal;
	}
	
	public static void saveToFile(List<String> lines, File file) throws IOException {
		FileWriter fw = new FileWriter(file);
		for (String line : lines) {
			fw.append(line);
			fw.append("\n");
		}
		fw.close();
	}
	
	public static void copyFile(File sourceFile, File targetFile) throws IOException {
		Files.copy(sourceFile, targetFile);
	}
	
	public static void moveFile(File sourceFile, File targetFile) throws IOException {
		Files.move(sourceFile, targetFile);
	}
	
	public static boolean equalFiles(File file1, File file2) throws IOException {
		return Files.equal(file1, file2);
	}
	
	public static void touchFile(File file) throws IOException {
		Files.touch(file);
	}
	
	public static void saveToFile(byte [] data, File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file, false);
		fos.write(data);
		fos.close();
	}
	
	public static void appendToFile(byte [] data, File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file, true);
		fos.write(data);
		fos.close();
	}
	
	public static void appendToFile(String data, String file) throws IOException {
		FileOutputStream fos = new FileOutputStream(new File(file), true);
		fos.write(data.getBytes());
		fos.close();
	}
	
	public static void saveToFile(String textToWrite, String path) throws IOException {
		saveToFile(textToWrite, new File(path));
	}
	
	public static void saveToFile(String textToWrite, File file) throws IOException {
		if (file.exists())
			throw new IOException("File " + file.getPath() + " already exists");
		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			bw.write(textToWrite);
		} finally {
			if (bw != null)
				bw.close();
		}
	}

	public static void saveToFile(String textToWrite, File file, boolean replaceIfExist) throws IOException {
		if (file.exists() && replaceIfExist == false)
			throw new IOException("File " + file.getPath() + " already exists");
		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			bw.write(textToWrite);
		} finally {
			if (bw != null)
				bw.close();
		}
	}
	
	/**
	 * Snippet taken from: http://viralpatel.net/blogs/2009/05/20-useful-java-code-snippets-for-java-developers.html
	 * @param in
	 * @param out
	 * @throws IOException
	 */
	public static void fileCopy(File in, File out) throws IOException {
		FileChannel inChannel = null;
		FileChannel outChannel = null;
		try {
			inChannel = new FileInputStream(in).getChannel();
			outChannel = new FileOutputStream(out).getChannel();
			
			// inChannel.transferTo(0, inChannel.size(), outChannel);
			// original -- apparently has trouble copying large files on Windows

			// magic number for Windows, 64Mb - 32Kb)
			int maxCount = (64 * 1024 * 1024) - (32 * 1024);
			long size = inChannel.size();
			long position = 0;
			while (position < size) {
				position += inChannel
						.transferTo(position, maxCount, outChannel);
			}
		} finally {
			if (inChannel != null) {
				inChannel.close();
			}
			if (outChannel != null) {
				outChannel.close();
			}
		}
	}

	public static String concatPaths(String basicPath, String toAdd) {
		if (StringUtilities.nullOrEmpty(toAdd))
			return basicPath;
		
		StringBuilder path = new StringBuilder();
		
		if (basicPath.startsWith("."))
			path.append(System.getProperty("user.dir") + basicPath.substring(1));
		else if (basicPath.trim().startsWith("~"))
			path.append(System.getProperty("user.home") + basicPath.substring(1));
		else
			path.append(basicPath);
		
		if (path.charAt(path.length() - 1) == '/') {
			if (toAdd.startsWith("/"))
				path.append(toAdd.substring(1));
			else
				path.append(toAdd);
		} else {
			if (toAdd.startsWith("/"))
				path.append(toAdd);
			else {
				path.append("/");
				path.append(toAdd);
			}
		}
		
		return path.toString();
	}
	
	public static String concatPaths(String ... pathParts) {
		if (pathParts == null || pathParts.length == 0)
			return null;
		
		String path = pathParts[0];
		
		if (path.indexOf("$HOME") >= 0)
			path = path.replaceAll("\\$HOME", System.getProperty("user.home"));
		
		for (int i = 1; i < pathParts.length; i++) {
			path = concatPaths(path, pathParts[i]);
		}
		
		return path;
	}
	
	public static List<String> readLines(File file) throws IOException {
		return Files.readLines(file, Charset.defaultCharset());
	}
	
	public static File createTmpFile(final String prefix, final String suffix) throws IOException {
		File f = File.createTempFile(prefix, suffix);
		f.deleteOnExit();
		return f;
	}
	
	public static File createTmpFile() throws IOException {
		File f = File.createTempFile("libs5-temp-", null);
		f.deleteOnExit();
		return f;
	}

	public static File createTmpRandomDir() throws IOException {
		return Files.createTempDir();
//		// there are 100 attempts for creating such tmp random directory
//		for (int i = 0; i < 100; i++) {
//			String guid = RandomGUID.getRawRandomGUID();
//			FileExt f = new FileExt(FileUtils.concatPaths(parentDir, guid));
//			if (f.exists() == false)
//				return f;
//		}
//		
//		LibsLogger.error("Cannot generate random directory. Tried 100 times.");
//		return null;
	}

	public static boolean isEmpty(String path) throws IOException {
		try(DirectoryStream<Path> dirStream = java.nio.file.Files.newDirectoryStream(Paths.get(path))) {
	        return !dirStream.iterator().hasNext();
	    }
	}
}
