package net.hypki.libs5.utils.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class ArrayUtils {

	public static String toString(Object[] arr) {
		return Arrays.toString(arr);
	}
	
	public static String toString(Object[] arr, String separator) {
		return StringUtils.join(arr, separator);
	}
	
	public static String toString(List arr, String separator) {
		return StringUtils.join(arr, separator);
	}
	
	public static List<?> toList(Object[] arr) {
		if (arr == null)
			return null;
		return Arrays.asList(arr);
	}
	
	public static boolean contains(Object[] arr, Object o) {
		return Arrays.asList(arr).contains(o);
	}
	
	public static Object[] concat(Object[] arr1, Object[] arr2) {
		return org.apache.commons.lang.ArrayUtils.addAll(arr1, arr2);
	}
	
	public static String[] toArray(List<String> list) {
		if (list == null || list.size() == 0)
			return null;
		String[] objArr = new String[list.size()];
		list.toArray(objArr);
		return objArr;
	}
	
	public static Object[] removeElement(Object[] arr, Object element) {
		return ArrayUtils.removeElement(arr, element); // create a new array
	}
	
	public static boolean isEmpty(Collection<?> c) {
		return c == null || c.size() == 0;
	}
	
	public static double[] resize(double [] input, int newSize) {
		double[] largerArray = new double[newSize];
		System.arraycopy(input, 0, largerArray, 0, newSize);
		return largerArray;
	}
	
	public static int[] resize(int [] input, int newSize) {
		int[] largerArray = new int[newSize];
		System.arraycopy(input, 0, largerArray, 0, newSize);
		return largerArray;
	}
	
	public static long[] resize(long [] input, int newSize) {
		long[] largerArray = new long[newSize];
		System.arraycopy(input, 0, largerArray, 0, newSize);
		return largerArray;
	}
}
