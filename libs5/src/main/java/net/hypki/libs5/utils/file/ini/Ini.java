package net.hypki.libs5.utils.file.ini;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.utils.file.BigFile;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.AssertTrue;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

/**
 * Class to handle configuration INI files. 
 * 
 * @author ahypki
 *
 */
public class Ini implements Iterable<Value>, Iterator<Value> {

	@Expose
	private Path path = null;
	
	@Expose
	private List<Line> lines = null;
	
	private Iterator<Value> valueIterator = null;
	
	public Ini() {
		
	}
	
	public Ini(FileExt iniFile) throws IOException {
		parseIniFile(new BigFile(iniFile.getAbsolutePath()));
	}
	
	public Ini(Path iniFile) throws IOException {
		parseIniFile(new BigFile(iniFile.toFile().getAbsolutePath()));
	}
	
	public Ini(InputStream iniFile) throws IOException {
		parseIniFile(new BigFile(iniFile));
	}
	
	private void parseIniFile(BigFile iniFile) throws IOException {
		String currentSection = null;
		for (String line : iniFile) {
			
			if (nullOrEmpty(line)) {
				
				getLines().add(new EmptyLine());
				
			} else if (line.trim().startsWith("#")) {
				
				getLines().add(new Comment(line.substring(1)));
			
			} else if (line.matches("\\[[\\w\\d]+\\][\\s]*")) {
				
				currentSection = line.replaceAll("[\\[\\]]", "").trim();
				getLines().add(new Section(currentSection));
				
			} else if (line.matches("[\\s]*[\\w\\d\\:]+[\\s]*=[\\s]*[\\w\\d\\.eEdD\\-\\+]+[\\s]*")) {
				
				// key = value pair
				String [] keyValuePair = StringUtilities.split(line, '=');
				final String key = keyValuePair[0].trim();
				final String value = keyValuePair[1].trim();
				
				if (value.matches(RegexUtils.INTEGER))
					
					getLines().add(new Value(currentSection, key, NumberUtils.toLong(value)));
				
				else if (value.matches(RegexUtils.DOUBLE))
					
					getLines().add(new Value(currentSection, key, NumberUtils.toDouble(value)));
				
				else
					
					getLines().add(new Value(currentSection, key, value));
				
			} else if (line.matches("[\\s]*[\\w\\d\\:\\_]+[\\s]*=[\\s]*.*")) {
				
				// key = value pair
				String [] keyValuePair = StringUtilities.split(line, '=');
				final String key = keyValuePair[0].trim();
				final String value = keyValuePair[1].trim();
				
				getLines().add(new Value(currentSection, key, value));
				
			} else
				throw new NotImplementedException(line);
		}
	}
	
	public boolean containsKey(String key) {
		return containsKey(null, key);
	}
	
	public boolean containsKey(String section, String key) {
		for (Line line : getLines()) {
			if (line instanceof Value) {
				Value val = (Value) line;
				if (val.getKey().equals(key)) {
					if (section != null) {
						if (val.getSection() != null && val.getSection().equals(section))
							return true;
						return false;
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public Object getValue(String key) {
		return getValue(key, null);
	}
	
	public boolean isList(String key) {
		Value keyValue = getKeyValue(null, key);
		return keyValue != null ? keyValue.isList() : false;
	}

	public Object getValue(String key, Object defaultValue) {
		return getValue(null, key, defaultValue);
	}

	public Object getValue(String section, String key, Object defaultValue) {
		Value keyValue = getKeyValue(section, key);
		return keyValue != null ? keyValue.getValue() : defaultValue;
	}
	
	public Value getKeyValue(String section, String key) {
		if (lines == null
				|| key == null)
			return null;
		
		for (Line line : lines) {
			if (line instanceof Value) {
				Value val = (Value) line;
				if ((section == null || (val.getSection() != null && val.getSection().equals(section))) 
						&& val.getKey() != null && val.getKey().equals(key))
					return val;
			}
		}
		return null;
	}
	
	private int getSectionIndex(final String section) {
		int counter = 0;
		for (Line line : lines) {
			if (line instanceof Section) {
				Section sec = (Section) line;
				if (sec.getName().equals(section)) {
					return counter;
				}
			}
			counter++;
		}
		return -1;
	}

	public int getInt(String section, String key, int defaultValue) {
		Object v = getValue(section, key, defaultValue);
		if (v instanceof Long)
			return (int) ((long) v);
		return (int) v;
	}
	
	public String getString(String key, String defaultValue) {
		return getString(null, key, defaultValue);
	}
	
	public String getString(String section, String key, String defaultValue) {
		Object v = getValue(section, key, defaultValue);
		if (v == null)
			return defaultValue;
		if (v instanceof String)
			return (String) v;
		return String.valueOf(v);
	}
	
	public int getInt(String key) {
		return getInt(null, key, 0);
	}
	
	public double getDouble(String section, String key, double defaultValue) {
		Object obj = getValue(section, key, defaultValue);
		if (obj instanceof Double)
			return (double) obj;
		else
			return Double.valueOf(obj.toString());
	}
	
	public double getDouble(String key) {
		return getDouble(null, key, 0.0);
	}
	
	public void setValue(String section, String key, Object value, String optionalComment) {
		Value keyValue = getKeyValue(section, key);
		
		if (keyValue != null) {
			keyValue.setValue(value);
//			if (notEmpty(optionalComment))
//				keyValue.se lines.add(++sectionIndex, new Comment(optionalComment));
			return;
		} 
		
		int sectionIndex = getSectionIndex(section);
		
		if (sectionIndex == -1) {
			lines.add(new EmptyLine());
			lines.add(new Comment("_____________________________________________________________________________________"));
			lines.add(new Section(section));
			sectionIndex = lines.size() - 1;
		}
		
		if (sectionIndex > -1) {
			lines.add(++sectionIndex, new EmptyLine());
			if (notEmpty(optionalComment))
				lines.add(++sectionIndex, new Comment(optionalComment));
			lines.add(++sectionIndex, new Value(section, key, value));
			lines.add(++sectionIndex, new EmptyLine());
		}
	}

	public void setValue(String key, Object value) {
//		setValue(null, key, value);
		setValue(null, key, value, null);
	}
	
	public void setValue(String section, String key, Object value) {
		setValue(section, key, value, null);
//		Value v = getKeyValue(section, key);
//		if (v == null)
//			throw new NotImplementedException();
//		v.setValue(value);
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public List<Line> getLines() {
		if (lines == null)
			lines = new ArrayList<Line>();
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	@Override
	public Iterator<Value> iterator() {
		List<Value> values = new ArrayList<Value>();
		for (Line line : lines) {
			if (line instanceof Value)
				values.add((Value) line);
		}
		return values.iterator();
	}

	@Override
	public boolean hasNext() {
		if (valueIterator == null)
			valueIterator = iterator();
		return valueIterator.hasNext();
	}

	@Override
	public Value next() {
		return valueIterator.next();
	}

	@Override
	public void remove() {
		throw new NotImplementedException();
	}
	
	public void store(Path path) throws IOException {
		store(path, true);
	}
	
	public void store(Path path, boolean replaceIfExist) throws IOException {
		FileExt iniFile = new FileExt(path.toFile());
		if (iniFile.exists()) {
			if (replaceIfExist)
				iniFile.delete();
			else
				return;
		}
		
		assertTrue(iniFile.exists() == false, "File ", path, " already exists");
		
		FileWriter fw = null;
		try {
			fw = new FileWriter(path.toFile(), false);
			
			for (Line line : getLines()) {
				fw.append(line.toString());
				fw.append("\n");
			}
			
			fw.close();
		} catch (Exception e) {
			throw new IOException("Cannot save INI file", e);
		} finally {
			if (fw != null)
				fw.close();
		}
	}

	public void removeValue(String toRemove) {
		Value keyValue = getKeyValue(null, toRemove);
		
		if (keyValue != null) {
			keyValue.setKey(null);
			keyValue.setValue(null);
			keyValue.setSection(null);
		}
	}
	
}
