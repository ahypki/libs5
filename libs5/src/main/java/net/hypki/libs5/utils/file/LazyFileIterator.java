package net.hypki.libs5.utils.file;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import net.hypki.libs5.utils.LibsLogger;

import org.apache.commons.lang.NotImplementedException;

public class LazyFileIterator implements Iterable<FileExt>, Iterator<FileExt> {
	
	private List<FileExt> folders = null;
	
	private List<FileExt> files = null;
	
	private boolean returnFiles = true;
	
	private boolean returnFolders = false;
	
	private boolean recursively = false;
	
	private FileFilter fileFilter = null;
	
	private Stack<FileExt> foldersToScan = new Stack<FileExt>();
	
	private Stack<FileExt> resultQueue = new Stack<FileExt>();

	public LazyFileIterator(File folderToScan, boolean recursively, boolean returnFiles, boolean returnFolders) {
		getFolders().add(new FileExt(folderToScan));
		setRecursively(recursively);
		setReturnFiles(returnFiles);
		setReturnFolders(returnFolders);
		
		foldersToScan.push(new FileExt(folderToScan));
		
		if (returnFolders)
			if (fileFilter == null 
					|| (fileFilter != null && fileFilter.accept(folderToScan)))
				resultQueue.add(new FileExt(folderToScan));
	}
	
	/**
	 * 
	 * @param folderToScan - folder to scan for files
	 * @param recursively - scan folder recursively
	 * @param fileFilter - file filter
	 */
	public LazyFileIterator(File folderToScan, boolean recursively, boolean returnFiles, boolean returnFolders, FileFilter fileFilter) {
		getFolders().add(new FileExt(folderToScan));
		setRecursively(recursively);
		setReturnFiles(returnFiles);
		setReturnFolders(returnFolders);
		setFileFilter(fileFilter);
		
		foldersToScan.push(new FileExt(folderToScan));
		
		if (returnFolders)
			if (fileFilter == null 
				|| (fileFilter != null && fileFilter.accept(folderToScan)))
				resultQueue.add(new FileExt(folderToScan));
		
		if (returnFiles)
			for (File file : getFiles()) {
				if (fileFilter == null 
						|| (fileFilter != null && fileFilter.accept(file)))
					resultQueue.push(new FileExt(file));
			}
	}
	
	/**
	 * 
	 * @param folders - list of folder to scan for files
	 * @param files - additional list of files added to the iterated files
	 * @param recursively - scan folder recursively
	 * @param fileFilter - file filter
	 */
	public LazyFileIterator(List<FileExt> folders, List<FileExt> files, boolean recursively, boolean returnFiles, boolean returnFolders, FileFilter fileFilter) {
		setFolders(folders);
		setFiles(files);
		setRecursively(recursively);
		setReturnFiles(returnFiles);
		setReturnFolders(returnFolders);
		setFileFilter(fileFilter);
		
		for (File folder : folders)
			foldersToScan.push(new FileExt(folder));

		if (returnFolders)
			for (File folder : folders) {
				if (fileFilter == null 
						|| (fileFilter != null && fileFilter.accept(folder)))
					resultQueue.add(new FileExt(folder));
			}
		
		if (returnFiles)
			for (File file : getFiles()) {
				if (fileFilter == null 
						|| (fileFilter != null && fileFilter.accept(file)))
					resultQueue.push(new FileExt(file));
			}
	}
		
	@Override
	public void remove() {
		throw new NotImplementedException();		
	}
	
	@Override
	public boolean hasNext() {
		if (resultQueue.size() == 0) {
			// trying to read more files
			
			while (foldersToScan.size() > 0) {
				final File folder = foldersToScan.pop();
				
				final File [] files = (getFileFilter() != null ? folder.listFiles(getFileFilter()) : folder.listFiles());
				if (files != null)
					for (File file : files) {
						if (file.isDirectory()) {
							if (isRecursively())
								foldersToScan.push(new FileExt(file));
							
							if (isReturnFolders())
								resultQueue.push(new FileExt(file));
						} else if (isReturnFiles())
							resultQueue.push(new FileExt(file));
					}
			
				if (resultQueue.size() > 0)
					return true;
			}
		}
		
		return resultQueue.size() > 0;
	}
	
	@Override
	public FileExt next() {
		return resultQueue.pop();
	}
	
	@Override
	public Iterator<FileExt> iterator() {
		if (isReturnFolders() == false && isReturnFiles() == false)
			throw new IllegalArgumentException("Either files or folder have to be read with " + getClass().getName());
		return new LazyFileIterator(getFolders(), getFiles(), isRecursively(), isReturnFiles(), isReturnFolders(), getFileFilter());
	}

	private List<FileExt> getFolders() {
		if (folders == null)
			folders = new ArrayList<FileExt>();
		return folders;
	}

	private void setFolders(List<FileExt> folders) {
		this.folders = folders;
	}

	private List<FileExt> getFiles() {
		if (files == null)
			files = new ArrayList<FileExt>();
		return files;
	}

	private void setFiles(List<FileExt> files) {
		this.files = files;
	}

	public boolean isRecursively() {
		return recursively;
	}

	public void setRecursively(boolean recursively) {
		this.recursively = recursively;
	}

	public FileFilter getFileFilter() {
		return fileFilter;
	}

	public void setFileFilter(FileFilter fileFilter) {
		this.fileFilter = fileFilter;
	}

	public boolean isReturnFiles() {
		return returnFiles;
	}

	public void setReturnFiles(boolean returnFiles) {
		this.returnFiles = returnFiles;
	}

	public boolean isReturnFolders() {
		return returnFolders;
	}

	public void setReturnFolders(boolean returnFolders) {
		this.returnFolders = returnFolders;
	}

}
