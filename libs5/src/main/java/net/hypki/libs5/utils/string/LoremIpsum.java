package net.hypki.libs5.utils.string;

/**
 * This class is used to generate some test texts starting with word: 
 * 		"Lorem ipsum dolor sit amet..."
 * @author ahypki
 *
 */
public class LoremIpsum {
	private static final String loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, " +
		"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, " +
		"quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute " +
		"irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
		"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim " +
		"id est laborum.";
	
	public static String createSampleString(int length) {
		if (length <= 0)
			return "";
		
		StringBuilder buf = new StringBuilder();
		String [] parts = loremIpsum.split(" ");
		int i = 0;
		
		while(true) {
			if (i == parts.length)
				i = 0;
			
			if ((buf.length() + parts[i].length() + 1/*space*/) > length) {
				buf.append(parts[i].substring(parts[i].length() - (length - buf.length())));
				break;
			}
			
			buf.append(parts[i++]);
			buf.append(" ");
		}
				
		return buf.toString();
	}
}
