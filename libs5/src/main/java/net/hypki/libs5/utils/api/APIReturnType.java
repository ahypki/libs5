package net.hypki.libs5.utils.api;

public enum APIReturnType {
	VOID,
	STRING,
	RESPONSE,
	OUTPUTSTREAM,
	BOOLEAN,
	LONG
}
