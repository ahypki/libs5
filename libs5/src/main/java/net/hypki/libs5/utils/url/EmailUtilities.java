package net.hypki.libs5.utils.url;

import java.util.regex.Pattern;

public class EmailUtilities {
	// RFC 2822 token definitions for valid email - only used together to form a
	// java Pattern object:
	private static final String sp = "\\!\\#\\$\\%\\&amp;\\'\\*\\+\\-\\/\\=\\?\\^\\_\\`\\{\\|\\}\\~";
	private static final String atext = "[a-zA-Z0-9" + sp + "]";
	private static final String atom = atext + "+"; // one or more atext chars
	private static final String dotAtom = "\\." + atom;
	
	// one atom followed by 0 or more dotAtoms.
	private static final String localPart = atom + "(" + dotAtom + ")*"; 

	// RFC 1035 tokens for domain names:
	private static final String letter = "[a-zA-Z]";
	private static final String letDig = "[a-zA-Z0-9]";
	private static final String letDigHyp = "[a-zA-Z0-9-]";
	public static final String rfcLabel = letDig + "(" + letDigHyp + "{0,61}"
			+ letDig + ")?";
	private static final String domain = rfcLabel + "(\\." + rfcLabel + ")*\\."
			+ letter + "{2,6}";

	// Combined together, these form the allowed email regexp allowed by RFC 2822:
	private static final String addrSpec = "^" + localPart + "@" + domain + "$";

	// now compile it:
	public static final Pattern VALID_PATTERN = Pattern.compile(addrSpec);
	
	/**
	 * Utility method that checks to see if the specified string is a valid
	 * email address according to the RFC 2822 specification.
	 * 
	 * @param email
	 *            the email address string to test for validity.
	 * @return true if the given text valid according to RFC 2822, false
	 *         otherwise.
	 */
	public static boolean isValidEmail(String email) {
		return (email != null) && VALID_PATTERN.matcher(email).matches();
	}
}
