package net.hypki.libs5.utils.scm;

import java.io.File;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;

public class GitUtils {

	/**
	 * Checks if the given file is inside some git repository. If it is then
	 * the root folder of the git repository is returned. Null otherwise.
	 * @param file
	 * @return
	 */
	public static FileExt getGitRepoPath(FileExt file) {
		FileExt tmp = file;
		while (tmp != null) {
//			LibsLogger.debug(GitUtils.class, "tmp", tmp);
			
			if (tmp.isDirectory()
					&& new FileExt(tmp, ".git").exists())
				return tmp;
			
			String parent = tmp.getAbsolutePath().substring(0, tmp.getAbsolutePath().lastIndexOf('/'));
			tmp = parent != null && parent.length() > 0 ? new FileExt(parent) : null;
		}
		return null;
	} 
}
