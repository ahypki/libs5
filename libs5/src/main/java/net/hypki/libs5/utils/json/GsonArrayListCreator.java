package net.hypki.libs5.utils.json;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.InstanceCreator;

@Deprecated
@SuppressWarnings("rawtypes")
public class GsonArrayListCreator implements InstanceCreator<ArrayList> {
	public ArrayList<?> createInstance(Type type) {
		// No need to use a parameterized list since the actual instance will
		// have the raw type anyway.
		return new ArrayList();
	}

}