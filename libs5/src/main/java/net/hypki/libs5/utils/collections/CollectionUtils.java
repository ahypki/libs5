package net.hypki.libs5.utils.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import jodd.util.StringUtil;


public class CollectionUtils {

	public static Collection<?> intersection(Collection<?> a, Collection<?> b) {
		return org.apache.commons.collections.CollectionUtils.intersection(a, b);
	}
	
	public static boolean isTheSame(Collection<?> a, Collection<?> b) {
		for (Object obj : a) {
			if (!b.contains(obj))
				return false;
		}
		for (Object obj : b) {
			if (!a.contains(obj))
				return false;
		}
		return true;
	}
	
	public static boolean isEmpty(Collection<?> a) {
		return org.apache.commons.collections.CollectionUtils.isEmpty(a);
	}
	
	/**
	 * Returns a new Collection containing a - b
	 * @param a
	 * @param b
	 * @return
	 */
	public static Collection<?> subtract(Collection<?> a, Collection<?> b) {
		return org.apache.commons.collections.CollectionUtils.subtract(a, b);
	}
	
	public static String[] keysAsArray(Map<String, ?> map) {
		String[] keys = new String[map.size()];
		int i = 0;
		for (String key : map.keySet()) {
			keys[i++] = key;
		}
		return keys;
	}

	public static String toString(Collection parts, String separator) {
		StringBuilder builder = new StringBuilder();
		int counter = parts != null ? parts.size() : 0;
		if (parts != null)
			for (Object o : parts) {
				builder.append(o.toString());
				if (--counter > 0)
					builder.append(separator);
			}
		return builder.toString();
	}
	
	public static String toString(Object [] parts, String separator) {
		StringBuilder builder = new StringBuilder();
		if (parts != null && parts.length > 0) {
			for (int i = 0; i < (parts.length - 1); i++) {
				if (parts[i] != null)
					builder.append(parts[i].toString());
				builder.append(separator);
			}
			if ((parts.length - 1) >= 0)
				builder.append(parts[parts.length - 1]);
		}
		return builder.toString();
	}

	public static void reverse(List<?> list) {
		Collections.reverse(list);
	}
	
	public static boolean areAllNull(Object ... objects) {
		for (int i = 0; i < objects.length; i++) {
			if (objects[i] != null)
				return false;
		}
		return true;
	}
	
	public static boolean areAllNotNull(Object ... objects) {
		for (int i = 0; i < objects.length; i++) {
			if (objects[i] == null)
				return false;
		}
		return true;
	}

	public static void numericalSort(List<String> toSort, final int column) {
		Collections.sort(toSort, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				double t1 = Double.parseDouble(StringUtil.splitc(o1, ' ')[column]);
				double t2 = Double.parseDouble(StringUtil.splitc(o2, ' ')[column]);
				return t1 > t2 ? 1 : -1;
			}
		});
	}
	
	public static void autoSort(List toSort) {
		Collections.sort(toSort, new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				if (o1 instanceof Double) {
					return ((Double) o1).compareTo((Double) o2);
				} else if (o1 instanceof Long) {
					return ((Long) o1).compareTo((Long) o2);
				} else if (o1 instanceof Integer) {
					return ((Integer) o1).compareTo((Integer) o2);
				} else if (o1 instanceof Float) {
					return ((Float) o1).compareTo((Float) o2);
				} else if (o1 instanceof String) {
					return ((String) o1).compareTo((String) o2);
				} else {
					return 0;
				}
			}
		});
	}
}
