package net.hypki.libs5.utils.date;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import jodd.datetime.JDateTime;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.annotations.Expose;

@Deprecated
public class Time implements Serializable {
	
//	private static final Format simpleDateFormat_yyyyMMdd_HHmmss = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSSS");
	private static final DateTimeFormatter parser = DateTimeFormat.forPattern("yyyy-MM-dd_HH:mm:ss.SSSS");
	private static final DateTimeFormatter parser_yyyyMMdd_HHmm = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
	private static final DateTimeFormatter parserDate = DateTimeFormat.forPattern("yyyy-MM-dd");
	private static final DateTimeFormatter parserHHmm = DateTimeFormat.forPattern("HH:mm");

	@Expose
	private long ms = 0;

	public Time() {
		if (isEmpty())
			ms = System.currentTimeMillis();
//		time = new DateTime(ms);
	}
	
	public Time(int year, int month, int day) {
		ms = new JDateTime(year, month, day).getTimeInMillis();
	}
	
	public Time(long ms) {
		this.ms = ms;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Time) {
			Time time = (Time) obj;
			return time.getMs() == this.getMs();
		} else if (obj instanceof JDateTime) {
			JDateTime time = (JDateTime) obj;
			return time.getTimeInMillis() == this.getMs();
		}  
		return super.equals(obj);
	}
	
	public long getMs() {
		return getJodaDateTime().getMillis();
	}
	
	@Override
	public String toString() {
		return parser.print(getMs());
	}
	
	public String toString(String format) {
		return DateTimeFormat.forPattern(format).print(getMs());
	}
	
	/**
	 * Returns only date in format yyyy-MM-dd
	 * @return
	 */
	public String toStringDate() {
		return parserDate.print(getMs());
	}
	
	public String toStringyyyyMMdd_HHmm() {
		return parser_yyyyMMdd_HHmm.print(getMs());
	}

	public void clear() {
		ms = 0;
	}
	
	public boolean isEmpty() {
		return ms == 0;
	}

	public void setTime(long ms) {
		this.ms = ms;
	}
	
	private DateTime getJodaDateTime() {
		return new DateTime(ms);
	}
	
	public Time subtractHours(int hours) {
		setTime(this.ms - hours * 60 * 60 * 1000);
		return this;
	}
	
	public Time addHours(int hours) {
		setTime(this.ms + hours * 60 * 60 * 1000);
		return this;
	}

	public int getYear() {
		return getJodaDateTime().getYear();
	}

	public int getMonth() {
		return getJodaDateTime().getMonthOfYear();
	}
	
	public int getDay() {
		return getJodaDateTime().getDayOfMonth();
	}
	
	public boolean isOlderThan(long time, TimeUnit timeUnit) {
		long ms = TimeUnit.MILLISECONDS.convert(time, timeUnit);
		return getMs() < (System.currentTimeMillis() - ms);
	}

	public static Time parse(String time) {
		return new Time(parser.parseDateTime(time).getMillis());
	}
	
	public static Time parseDate(String date) {
		return new Time(parserDate.parseDateTime(date).getMillis());
	}

	public static Time parseTime(String time) {
		return new Time(parserHHmm.parseDateTime(time).getMillis());
	}
	
	public Time addDays(int i) {
		ms += (DateUtils.ONE_DAY_MS * i);
		return this;
	}

	public boolean equalsDate(int year, int month, int day) {
		return new JDateTime(year, month, day).equals(new JDateTime(getMs()));
	}

	public int getHour() {
		return getJodaDateTime().getHourOfDay();
	}
	
	public int getMinutes() {
		return getJodaDateTime().getMinuteOfHour();
	}

	public Time addTime(Time t) {
		ms += t.getMs();
		return this;
	}

	public static Time now() {
		return new Time(System.currentTimeMillis());
	}

}
