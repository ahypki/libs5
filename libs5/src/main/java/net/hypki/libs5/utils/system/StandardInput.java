package net.hypki.libs5.utils.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StandardInput {
	public static String readStringFromStandardInput() throws IOException {
		// open up standard input and read one line
		return new BufferedReader(new InputStreamReader(System.in)).readLine();
	}
}
