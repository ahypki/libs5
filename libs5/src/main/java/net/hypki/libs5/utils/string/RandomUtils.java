package net.hypki.libs5.utils.string;

import java.awt.Color;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.system.ExecutingLinuxCommand;

import org.fluttercode.datafactory.impl.DataFactory;



public class RandomUtils {
	
	private static Random random = new Random(System.currentTimeMillis());
	
	private static ArrayList<String> localCache = new ArrayList<String>();
	
	private static DataFactory dataFactory = new DataFactory();
	
	private static String[] animals = { "Cat", "Dog", "Goat", "Horse", "Sheep" };
	
	public static String randomAnimal() {
		// This example uses the array of animals and returns a value with a 20% chance of being the default value of "Cat"
		return dataFactory.getItem(animals, 80, "Cat");
	}
	
	public static Date dateBetween(Date minDate, Date maxDate) {
		return dataFactory.getDateBetween(minDate, maxDate);
	}
	
	public static String businessName() {
		return dataFactory.getBusinessName();
	}
	
	public static String city() {
		return dataFactory.getCity();
	}
	
	public static String firstName() {
		return dataFactory.getFirstName();
	}
	
	public static String lastName() {
		return dataFactory.getLastName();
	}
	
	public static String streetName() {
		return dataFactory.getStreetName();
	}
	
	public static List<String> firstNames(int count) {
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			names.add(dataFactory.getFirstName());
		}
		return names;
	}
	
	public static List<String> lastNames(int count) {
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			names.add(dataFactory.getLastName());
		}
		return names;
	}
	
	public static String nextPronounceableWordUsingAPG() {
		if (localCache.size() == 0) {
			try {
				String result = ExecutingLinuxCommand.executeCommand(new String[]{"apg", "-a", "0", "-m", "5", "-x", "5", "-M", "L", "-n", "50"}, null);
				String [] words = result.split("\\n");
				for (String string : words) {
					localCache.add(string);					
				}
			} catch (IOException e) {
				LibsLogger.error("Cannot execute apg command", e);
			}
		}
		
		String tmp = null;
		if (localCache.size() > 0) {
			tmp = localCache.get(0);
			localCache.remove(0);
		}
		return tmp;
	}
	
	public static String nextPronounceableText(int length) {
		return LoremIpsum.createSampleString(length);
//		StringBuilder sb = new StringBuilder();
//		while (true) {
//			String word = nextPronounceableWord();
//			if ((sb.length() + word.length()) > length)
//				break;
//			
//			sb.append(" " + word);
//		}
//		return sb.toString();
	}
	
	public static String nextPassword(int length) {
		String pass = "";
		for (int i = 0; i < length; i++) {
			pass += (char) nextInt(34, 124);
		}
		return pass; 
	}
	
	
	public static String nextPronounceableWord(int length) {
		return Gpw.generate(length);
	}
	
	public static String nextPronounceableWord() {
		return Gpw.generate(5);
	}
	
	public static String nextWordFromDigits(int length) {
		return dataFactory.getNumberText(length);
//		StringBuilder builder = new StringBuilder();
//		for (int i = 0; i < length; i++) {
//			builder.append(nextInt(10));
//		}
//		return builder.toString();
	}
	
	public static int nextInt() {
		return org.apache.commons.lang.math.RandomUtils.nextInt();
	}
	
	public static double nextDouble() {
		return random.nextDouble();
	}
	
	/**
	 * Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value (exclusive), from the Math.random() sequence.
	 * @param max
	 * @return
	 */
	public static int nextInt(int max) {
		return org.apache.commons.lang.math.RandomUtils.nextInt(max);
	}
	
	/**
	 * Returns a pseudorandom, uniformly distributed int value between min (inclusive) and the specified value max (exclusive), from the Math.random() sequence.
	 * @param min
	 * @param max
	 * @return
	 */
	public static int nextInt(int min, int max) {
		return min + org.apache.commons.lang.math.RandomUtils.nextInt(max - min);
	}
	
	public static void main(String[] args) throws IOException {
		for (int i = 0; i < 100; i++) {
			System.out.print(RandomUtils.nextInt(Integer.MAX_VALUE / 1000) + "; ");
		}
	}
}
