package net.hypki.libs5.utils;

import java.io.IOException;

import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;

public class LocalLogger {

	public static boolean debug(Object ... message) {
		try {
			FileUtils.appendToFile(SimpleDate.now().toStringHuman() + " DEBUG " + LibsLogger.toString(message) + "\n", 
					new FileExt("$HOME/.local.log").getAbsolutePath());
			
			return true;
		} catch (IOException e) {
			LibsLogger.error(LocalLogger.class, "Cannot log to local logger", e);
			return false;
		}
	} 
	
	public static boolean error(Object ... message) {
		try {
			FileUtils.appendToFile(SimpleDate.now().toStringHuman() + " ERROR " + LibsLogger.toString(message) + "\n", 
					new FileExt("$HOME/.local.log").getAbsolutePath());
			
			return true;
		} catch (IOException e) {
			LibsLogger.error(LocalLogger.class, "Cannot log to local logger", e);
			return false;
		}
	}
}
