package net.hypki.libs5.utils.collections;

import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class InfMapSerializer implements JsonSerializer<InfMap>, JsonDeserializer<InfMap> {
	
	@Override
	public JsonElement serialize(InfMap map, Type arg1, JsonSerializationContext arg2) {
		try {
			JsonObject jo = new JsonObject();
			jo.addProperty("infmap", StringUtilities.serialize(map));
			return jo;
		} catch (Exception e) {
			LibsLogger.error(InfMapSerializer.class, "Cannot serialize InfMap", e);
			return null;
		}
	}

	@Override
	public InfMap deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		try {
			return (InfMap) StringUtilities.deserialize(json.getAsJsonObject().get("infmap").getAsString());
		} catch (Throwable e) {
			LibsLogger.error(InfMap.class, "Cannot deserialize " + json, e);
			return null;
		}
	}
}
