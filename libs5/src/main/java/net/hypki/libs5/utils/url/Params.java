package net.hypki.libs5.utils.url;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class Params implements Serializable, Iterable<String> {

	@Expose
	private Map<String, Object> params = null;
	
	public Params() {
		
	}
	
	public Params(String keyValueParams) {
		try {
			if (keyValueParams != null)
				for (String keyValue : StringUtilities.split(keyValueParams, '&')) {
					keyValue = UrlUtilities.urlDecode(keyValue);
					int eqSign = keyValue.indexOf('=');
					String key = eqSign > 0 ? keyValue.substring(0, eqSign) : keyValue;
					String value = eqSign > 0 ? keyValue.substring(eqSign + 1) : null;
					
					if (getParams().get(key) != null) {
						if (isList(key)) {
							getList(key).add(value);
						} else {
							// there is already such a key, changing it to the list
							List<String> values = new ArrayList<>();
							values.add(getString(key));
							values.add(value);
							getParams().put(key, values);
						}
					} else {
						getParams().put(key, value);
					}
				}
		} catch (Exception e) {
			LibsLogger.error(Params.class, "Cannot parse key value params " + keyValueParams, e);
		}
	}
	
	public Params copy() {
		return JsonUtils.fromJson(getJson(), Params.class);
	}
	
	private JsonElement getJson() {
		return JsonUtils.toJson(this);
	}
	
	@Override
	public Iterator<String> iterator() {
		return getParams().keySet().iterator();
	}
	
	public List getList(String key) {
		if (isList(key))
			return (List) get(key);
		
		boolean allEmpty = true;
		List tmpList = new ArrayList<>();
		if (get(key) != null) {
			Object v = get(key);
			if (!nullOrEmpty(v))
				allEmpty = false;
			tmpList.add(v);
		}
		if (allEmpty) {
			tmpList = new ArrayList<>();
			add(key, tmpList);
			return tmpList;
		} else {
			add(key, tmpList);
			return tmpList;
		}
	}

	public boolean isList(String key) {
		return getParams().get(key) instanceof List;
	}

	@Override
	public String toString() {
		return toString(true);
	}
	
	public String toString(boolean urlEncode) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Object> entry : getParams().entrySet()) {
			if (entry.getValue() instanceof List) {
				for (Object value : (List) entry.getValue()) {
					if (sb.length() > 0)
						sb.append("&");
					sb.append(entry.getKey());
					sb.append("=");
					sb.append(urlEncode ? UrlUtilities.UrlEncode(String.valueOf(value)) : value);
				}
			} else {
				if (sb.length() > 0)
					sb.append("&");
				sb.append(entry.getKey());
				sb.append("=");
				sb.append(urlEncode ? 
						UrlUtilities.UrlEncode(entry.getValue() != null ? String.valueOf(entry.getValue()) : "") 
						: entry.getValue());
			}
		}
		return sb.toString();
	}
	
	/**
	 * Returns this object in form: key=value,key=value,...,key=value
	 * @return
	 */
	public String toStringOneLine() {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Object> entry : getParams().entrySet()) {
			if (sb.length() > 0)
				sb.append(",");
			sb.append(entry.getKey());
			sb.append("=");
			sb.append(entry.getValue());
		}
		return sb.toString();
	}
	
	public String toStringJson() {
		return JsonUtils.objectToStringPretty(this);
	}
	
	public boolean isEmpty() {
		return params == null || params.size() == 0;
	}
	
	public Object get(final String key) {
		return isEmpty() ? null : getParams().get(key);
	}
	
	public boolean contains(final String key) {
		return isEmpty() ? false : getParams().containsKey(key);
	}
	
	public Map<String, Object> getParams() {
		if (params == null)
			params = new HashMap<>();
		return params;
	}
	
	public Params add(boolean addIfTrue, String key, Object value) {
		if (addIfTrue)
			return add(key, value);
		return this;
	}
	
	public Params add(String key, Object value) {
		if (value != null)
			getParams().put(key, value);
		return this;
	}
	
	public int size() {
		return isEmpty() ? 0 : getParams().size();
	}

	public Object[] getValues() {
		if (isEmpty())
			return null;
		
		Object[] values = new Object[size()];
		int counter = 0;
		for (Object val : getParams().values()) {
			values[counter++] = val;
		}
		return values;
	}

	public Params remove(String key) {
		if (!isEmpty())
			getParams().remove(key);
		return this;
	}
	
	public String getString(String key, String defaultValue) {
		String tmp = getString(key);
		return tmp == null || tmp.trim().length() == 0 ? defaultValue : tmp;
	}
	
//	public UUID getUUID(String key) {
//		
//	}

	public String getString(String key) {
		Object o = get(key);
		if (o == null)
			return null;
		else if (o instanceof String)
			return (String) o;
		else
			return String.valueOf(o);
	}
	
	public int getInteger(String key, int defaultValue) {
		Object o = get(key);
		if (o == null)
			return defaultValue;
		else if (o instanceof Integer)
			return (Integer) o;
		else if (o instanceof String) {
			String s = (String) o;
			if (s.contains("/"));
				s = s.replace("/", "");
			return net.hypki.libs5.utils.utils.NumberUtils.toInt(s, defaultValue);
		} else
			return net.hypki.libs5.utils.utils.NumberUtils.toInt(String.valueOf(o), defaultValue);
	}
	
	public Number getNumber(String key, Number defaultValue) {
		Object o = get(key);
		if (o == null)
			return defaultValue;
		else if (RegexUtils.isInt((String) o))
			return NumberUtils.toInt((String) o);
		else
			return NumberUtils.toDouble(String.valueOf(o));
	}
	
	public double getDouble(String key, double defaultValue) {
		Object o = get(key);
		if (o == null)
			return defaultValue;
		else if (o instanceof Double)
			return (Double) o;
		else {
			String tmp = String.valueOf(o);
			if (tmp.length() > 0)
				return net.hypki.libs5.utils.utils.NumberUtils.toDouble(tmp);
			else
				return defaultValue;
		}
	}
	
	public SimpleDate getDate(String key, SimpleDate defaultValue) {
		String s = getString(key, null);
		if (s == null)
			return defaultValue;
		else if (s instanceof String)
			return SimpleDate.parse(s);
		else
			return SimpleDate.parse(String.valueOf(s));
	}
	
	public long getLong(String key, long defaultValue) {
		Object o = get(key);
		if (o == null)
			return defaultValue;
		else if (o instanceof Long)
			return (Long) o;
		else if (o instanceof Double)
			return (long) ((double) o);
		else
			return net.hypki.libs5.utils.utils.NumberUtils.toLong(String.valueOf(o), defaultValue);
	}
	
	public boolean getBoolean(String key) {
		return getBoolean(key, false);
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		Object o = get(key);
		if (o == null)
			return defaultValue;
		else
			return o.toString().equalsIgnoreCase("on") || o.toString().equalsIgnoreCase("true");
	}

	public boolean exists(String key) {
		return get(key) != null;
	}
	
	public Set<String> keys() {
		return getParams().keySet();
	}
	
	public String getStringByPrefix(String prefix) {
		List<String> prefixKeys = findKeysByPrefix(prefix);
		return prefixKeys != null && prefixKeys.size() == 1 ? getString(prefixKeys.get(0)) : null;
	}
	
	public List<String> findKeysByPrefix(String prefix) {
		List<String> keys = new ArrayList<String>();
		
		for (String key : keys()) {
			if (key.startsWith(prefix))
				keys.add(key);
		}
		
		return keys;
	}
	
	public List<String> findKeysBySuffix(String suffix) {
		List<String> keys = new ArrayList<String>();
		
		for (String key : keys()) {
			if (key.endsWith(suffix))
				keys.add(key);
		}
		
		return keys;
	}

	public Params subsetByRegex(String regex) {
		Params paramsCopy = new Params();
		
		for (Map.Entry<String, Object> entry : getParams().entrySet()) {
			if (entry.getKey().matches(regex))
				paramsCopy.add(entry.getKey(), entry.getValue());
		}
		
		return paramsCopy;
	}

	public Params filter(String prefix) {
		Params params = new Params();
		for (String key : this) {
			if (key.startsWith(prefix))
				params.add(key, get(key));
		}
		return params;
	}
	
	public Params removePrefix(String prefix) {
		Params params = new Params();
		for (String key : this) {
			if (key.startsWith(prefix))
				params.add(key.substring(prefix.length()), get(key));
		}
		return params;
	}

	public Params addAll(Params p) {
		for (String key : p) {
			add(key, p.get(key));
		}
		return this;
	}
}
