package net.hypki.libs5.utils.io;

import java.io.Console;
import java.io.IOException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.StandardInput;



public class UserIO {
	public static String getString(String text) {
		if (text != null)
			System.out.print(text);
		
		while (true) {
			try {
//				System.out.print(": ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (!StringUtilities.nullOrEmpty(tmp))
					return tmp;
			} catch (IOException e) {
				LibsLogger.error("Cannot get string from user", e);
			}
		}
	}
	
	public static String getPassword(String text, String defaultValue) {
		System.out.print(text);
		
		Console console = System.console();
		
	    if (console == null) {
	        System.out.println("Couldn't get Console instance. Creating plain console reader...");
	        return getString(text, defaultValue);
	    }

//	    console.printf("Testing password%n");
	    char passwordArray[] = console.readPassword();
	    return new String(passwordArray);
//	    console.printf("Password entered was: %s%n", new String(passwordArray));
	}
	
	public static String getString(String text, String defaultValue) {
		System.out.print(text);
		
		while (true) {
			try {
				System.out.print(" [" + (defaultValue != null ? defaultValue : "") + "]: ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (StringUtilities.nullOrEmpty(tmp))
					return defaultValue;
				else
					return tmp;
			} catch (IOException e) {
				LibsLogger.error("Cannot get string from user", e);
			}
		}
	}
	
	public static Boolean getBoolean(String text) {
		System.out.print(text);
		
		while (true) {
			try {
				System.out.print(": ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (StringUtilities.nullOrEmpty(tmp))
					continue;
				
				tmp = tmp.toLowerCase();
				if (tmp.startsWith("f") || tmp.startsWith("n"))
					return false;
				
				if (tmp.startsWith("t") || tmp.startsWith("y"))
					return true;

//				return Boolean.parseBoolean(tmp);
			} catch (IOException e) {
				LibsLogger.error("Cannot get boolean value from user", e);
			}
		}
	}
	
	public static Boolean getBoolean(String text, Boolean defaultValue) {
		System.out.print(text);
		
		while (true) {
			try {
				if (defaultValue != null) {
					if (defaultValue == true)
						System.out.print(" [TRUE/false]: ");
					else
						System.out.print(" [true/FALSE]: ");
				} else
					System.out.print(" [true/false]: ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (StringUtilities.nullOrEmpty(tmp))
					return defaultValue;

				return Boolean.parseBoolean(tmp);
			} catch (IOException e) {
				LibsLogger.error("Cannot get boolean value from user", e);
			}
		}
	}
	
	public static int getInteger(String text, Integer defaultValue) {
		System.out.print(text);
		
		while (true) {
			try {
				System.out.print(" [" + (defaultValue != null ? defaultValue : "") + "]: ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (StringUtilities.nullOrEmpty(tmp))
					return defaultValue;

				try {
					return Integer.parseInt(tmp);
				} catch (NumberFormatException e) {
					// wrong format, ask for input again
				}
			} catch (IOException e) {
				LibsLogger.error("Cannot get integer value from user", e);
			}
		}
	}
	
	public static Double getDouble(String text, Double defaultValue) {
		System.out.print(text);
		
		while (true) {
			try {
				System.out.print(" [" + (defaultValue != null ? defaultValue : "") + "]: ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (StringUtilities.nullOrEmpty(tmp))
					return defaultValue;

				try {
					return Double.parseDouble(tmp);
				} catch (NumberFormatException e) {
					// wrong format, ask for input again
				}
			} catch (IOException e) {
				LibsLogger.error("Cannot get double value from user", e);
			}
		}
	}

	public static SimpleDate getDate(String text, SimpleDate defaultValue) {
		while (true) {
			try {
				System.out.print(text + " [" + (defaultValue.toStringHuman() != null ? defaultValue.toStringHuman() : "") + "]: ");

				// read integer
				String tmp = StandardInput.readStringFromStandardInput();

				if (StringUtilities.nullOrEmpty(tmp))
					return defaultValue;

//				try {
				SimpleDate sd = SimpleDate.parse(tmp);
				
				if (sd != null)
					return sd;
				
//				} catch (Throwable t) {
//					// wrong format, ask for input again
//					LibsLogger.error(UserIO.class, "Cannot parse " + tmp, t);
//				}
			} catch (Exception e) {
				LibsLogger.error("Cannot parse date from user", e);
			}
		}
	}
}
