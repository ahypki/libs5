package net.hypki.libs5.utils.encryption;

import java.io.IOException;
import java.security.MessageDigest;

import net.hypki.libs5.utils.encryption.jbcrypt.BCrypt;
import net.hypki.libs5.utils.system.StandardInput;



public class HashUtils {
	private static MessageDigest md5Digest = Digest.getMD5MessageDigest();
	private static MessageDigest sha1Digest = Digest.getSHA1MessageDigest();
		
	public static String getBlowfishHash(String stringToHash) {
		// Hash a password for the first time
		return BCrypt.hashpw(stringToHash, BCrypt.gensalt());

		// gensalt's log_rounds parameter determines the complexity
		// the work factor is 2**log_rounds, and the default is 10
//		return BCrypt.hashpw(password, BCrypt.gensalt(12));
	}
	
	public static boolean checkBlowfishHash(String plainText, String hash) {
		return BCrypt.checkpw(plainText, hash);
	}
	
	public static void main(String[] args) throws IOException {
		System.out.print("Type string to hash: ");
		String toHash = StandardInput.readStringFromStandardInput();
		System.out.println("Hashed string: " + HashUtils.getBlowfishHash(toHash));
	}
}
