package net.hypki.libs5.utils.date;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import jodd.datetime.JDateTime;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.ByteUtils;
import net.hypki.libs5.utils.string.LocaleUtils;

import org.apache.commons.lang.NotImplementedException;

public class DateUtils {
	public static final String LANG_EN = "en";

	public static final long YEAR_2000 = new JDateTime(2000, 1, 1).getTimeInMillis();
	public static final long YEAR_2008 = new JDateTime(2008, 1, 1).getTimeInMillis();
	public static final long YEAR_2100 = new JDateTime(2100, 1, 1).getTimeInMillis();
	public static final byte[] YEAR_2008_BYTES = ByteUtils.toBytes(new JDateTime(2008, 1, 1).getTimeInMillis());
	public static final String YEAR_2008_STRING = String.valueOf(new JDateTime(2008, 1, 1).getTimeInMillis());
	
	public static final int 	FIFTHTEEN_SEC_MS = 15 * 1000;
	
	public static final int 	ONE_MINUTE_MS = 1 /*minute*/ * 60 * 1000;
	
	public static final int 	HALF_AN_HOUR_MS = 30 /*minutes*/ * 60 * 1000;
	
	public static final int 	ONE_HOUR_SEC = 1 /*hours*/ * 60 * 60;
	public static final int 	ONE_HOUR_MS = 1 /*hours*/ * 60 * 60 * 1000;
	
	public static final long 	ONE_DAY_MS = 1L /*days*/ * 24L /*hours*/ * 60L * 60L * 1000L;
	public static final long 	ONE_DAY_MINUTES = 1L /*days*/ * 24L /*hours*/ * 60L;
	
	public static final long 	ONE_WEEK_MS = 7L /*days*/ * 24L /*hours*/ * 60L * 60L * 1000L;
	public static final int 	ONE_WEEK_SEC = 7 /*days*/ * 24 /*hours*/ * 60 * 60;
	
	public static final long 	TWO_WEEK_MS = 14L /*days*/ * 24L /*hours*/ * 60L * 60L * 1000L;
	
	public static final long 	ONE_MONTH_MS;
	
	public static final long 	ONE_YEAR_MS;
	
	static {
		// one year in ms
		long now = System.currentTimeMillis();
		JDateTime next = new JDateTime(now);
		next.addYear(1);
		ONE_YEAR_MS = next.getTimeInMillis() - now;
		
		next.subYear(1);
		next.addMonth(1);
		ONE_MONTH_MS = next.getTimeInMillis() - now;
	}
	
	/**
	 * HH:mm
	 */
	private static final Format simpleDateFormat_HHmm = new SimpleDateFormat("HH:mm");
	
	/**
	 * yyyy-MM-dd
	 */
	private static final Format simpleDateFormat_yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * yyyy-MM-dd_HHmmss
	 */
	private static final Format simpleDateFormat_yyyyMMdd_HHmmss = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	
	/**
	 * yyyy-MM-dd HH:mm
	 */
	private static final Format simpleDateFormat_yyyyMMdd_space_HHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public static long oneYearInMs() {
		return ONE_YEAR_MS;
	}
	
	public static long oneMonthInMs() {
		return ONE_MONTH_MS;
	}
	
	/**
	 * Returns data in format: yyyy-MM-dd
	 * @return
	 */
	public static String dateToStringShort(Date date) {
		return simpleDateFormat_yyyyMMdd.format(date);
	}

	/**
	 * Returns data in format: yyyy-MM-dd_HHmmss
	 * @return
	 */
	public static String dateToStringLong(Date date) {
		return simpleDateFormat_yyyyMMdd_HHmmss.format(date);
	}
	
	/**
	 * Returns data in format: yyyy-MM-dd_HHmmss
	 * @return
	 */
	public static String dateToStringLongSpace(Date date) {
		return simpleDateFormat_yyyyMMdd_space_HHmm.format(date);
	}
	
	/**
	 * Returns data in format: yyyy-MM-dd_HHmmss
	 * @return
	 */
	public static String dateToStringLongSpace(long dateMs) {
		return simpleDateFormat_yyyyMMdd_space_HHmm.format(dateMs);
	}
	
	/**
	 * Returns date in format: yyyy-MM-dd
	 * @return
	 */
	public static String dateToStringShort(long date) {
		return simpleDateFormat_yyyyMMdd.format(date);
	}
	
	public static long thisWeekStartMs() {
		JDateTime jdt = new JDateTime();
		jdt.subDay(jdt.getDayOfWeek() -1);
		jdt.subTime(jdt.getHour(), jdt.getMinute(), jdt.getSecond(), jdt.getMillisecond());
		return jdt.getTimeInMillis();
	}
	
	public static long thisMonthStartMs() {
		JDateTime jdt = new JDateTime();
		jdt.subDay(jdt.getDayOfMonth() - 1);
		jdt.subTime(jdt.getHour(), jdt.getMinute(), jdt.getSecond(), jdt.getMillisecond());
		return jdt.getTimeInMillis();
	}
	
	public static SimpleDate nextMonth() {
		SimpleDate sd = SimpleDate.now().addMonths(1);
		sd.subtractDays(sd.getDayOfMonth() - 1);
		return sd.clearHour();
	}
	
	public static SimpleDate nextYear() {
		SimpleDate sd = SimpleDate.now().clearHour().addYears(1);
		return new SimpleDate(sd.getYear(), 1, 1);
	}
	
	public static SimpleDate nextMonday() {
		return nextMonday(null);
	}
	
	public static SimpleDate nextMonday(SimpleDate fromDate) {
		SimpleDate jdt = fromDate != null ? fromDate : SimpleDate.now().clearHour();
		jdt.addDays(8 - jdt.getDayOfWeek());
		return jdt;
	}
	
	public static SimpleDate nextTuesday() {
		return nextTuesday(null);
	}
	
	public static SimpleDate nextTuesday(SimpleDate fromDate) {
		return nextDayOfWeek(fromDate, 0);
	}
	
	public static SimpleDate nextWednesday() {
		return nextWednesday(null);
	}
	
	public static SimpleDate nextWednesday(SimpleDate fromDate) {
		return nextDayOfWeek(fromDate, 1);
	}
	
	public static SimpleDate nextThursday() {
		return nextThursday(null);
	}
	
	public static SimpleDate nextThursday(SimpleDate fromDate) {
		return nextDayOfWeek(fromDate, 2);
	}
	
	public static SimpleDate nextFriday() {
		return nextFriday(null);
	}
	
	public static SimpleDate nextFriday(SimpleDate fromDate) {
		return nextDayOfWeek(fromDate, 3);
	}
	
	public static SimpleDate nextSaturday() {
		return nextSaturday(null);
	}
	
	public static SimpleDate nextSaturday(SimpleDate fromDate) {
		return nextDayOfWeek(fromDate, 4);
	}
	
	public static SimpleDate nextSunday() {
		return nextSunday(null);
	}
	
	public static SimpleDate nextSunday(SimpleDate fromDate) {
		return nextDayOfWeek(fromDate, 5);
	}
	
	private static SimpleDate nextDayOfWeek(SimpleDate fromDate, int off) {
		SimpleDate jdt = fromDate != null ? fromDate : SimpleDate.now().clearHour();
		if (jdt.getDayOfWeek() < (2 + off))
			jdt.addDays(2 + off - jdt.getDayOfWeek());
		else
			jdt.addDays(9 + off - jdt.getDayOfWeek());
		return jdt;
	}
	
	public static long lastMonthStartMs() {
		JDateTime jdt = new JDateTime();
		jdt.subMonth(1);
		jdt.subDay(jdt.getDayOfMonth() - 1);
		jdt.subTime(jdt.getHour(), jdt.getMinute(), jdt.getSecond(), jdt.getMillisecond());
		return jdt.getTimeInMillis();
	}

	public static boolean isToday(Date toCheck) {
		JDateTime toCheckJDT = new JDateTime(toCheck);
		JDateTime today = new JDateTime();
		today.subMillisecond(today.getMillisOfDay());
		if (today.isBefore(toCheckJDT) == false)
			return false;
		
		today.addDay(1);
		return today.isAfter(toCheckJDT);
	}
	
	public static boolean isThisWeek(Date toCheck) {
		JDateTime toCheckJDT = new JDateTime(toCheck);
		JDateTime weekStart = new JDateTime();
		weekStart.subMillisecond(weekStart.getMillisOfDay());
		weekStart.subDay(weekStart.getDayOfWeek());
		if (weekStart.isBefore(toCheckJDT) == false)
			return false;
		
		weekStart.addDay(7);
		return weekStart.isAfter(toCheckJDT);
	}

	/**
	 * Parses date in format: yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static Time parseDateYYYYMMDD(String date) {
		return Time.parseDate(date);
	}
	
	public static Time parseHHss(String time) {
		return Time.parseTime(time);
	}

	public static String monthToString(int month, String lang) {
		return LocaleUtils.getMsg(DateUtils.class, lang, "month-" + month, String.valueOf(month));
	}

	public static boolean isBetween(SimpleDate from, SimpleDate to, SimpleDate testDate) {
		return testDate.getTimeInMillis() > from.getTimeInMillis() && testDate.getTimeInMillis() < to.getTimeInMillis();
	}
	
	public static XMLGregorianCalendar convert(SimpleDate date) {
		try {
			GregorianCalendar c = new GregorianCalendar();
			c.setTimeInMillis(date.getTimeInMillis());
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			LibsLogger.error(DateUtils.class, "Cannot convert to XMLGregorianCalendar", e);
			return null;
		}
	}
	
	public static String getDayOfWeekName(int dayOfWeek, String lang) {
		if (lang == null || lang.equalsIgnoreCase(LANG_EN)) {
			if (dayOfWeek == 1)
				return "Monday";
			else if (dayOfWeek == 2)
				return "Tuesday";
			else if (dayOfWeek == 3)
				return "Wednesday";
			else if (dayOfWeek == 4)
				return "Thursday";
			else if (dayOfWeek == 5)
				return "Friday";
			else if (dayOfWeek == 6)
				return "Saturday";
			else if (dayOfWeek == 7)
				return "Sunday";
			else
				throw new NotImplementedException("Uknown day of week " + dayOfWeek + ", expecting digit in between 1-7");
		} else
			throw new NotImplementedException("Uknown argument " + lang);
	}

	public static SimpleDate tomorrow() {
		return SimpleDate
				.now()
				.clearHour()
				.addDays(1);
	}

	public static SimpleDate nextDayOfWeek(DayOfWeek dayOfWeek) {
		if (dayOfWeek == DayOfWeek.MONDAY)
			return nextMonday();
		else if (dayOfWeek == DayOfWeek.TUESDAY)
			return nextTuesday();
		else if (dayOfWeek == DayOfWeek.WEDNESDAY)
			return nextWednesday();
		else if (dayOfWeek == DayOfWeek.THURSDAY)
			return nextThursday();
		else if (dayOfWeek == DayOfWeek.FRIDAY)
			return nextFriday();
		else if (dayOfWeek == DayOfWeek.SATURDAY)
			return nextSaturday();
		else if (dayOfWeek == DayOfWeek.SUNDAY)
			return nextSunday();
			
		return SimpleDate.now().clearHour();
	}
}
