package net.hypki.libs5.utils.string;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.constraint.AssertValid;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

public class MetaList implements Iterable<Meta>, Serializable {

	@Expose
	@AssertValid
	private Map<String, Meta> meta = null;
	
	private ArrayList<String> keysCached = null;
	
	public MetaList() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, Meta> e : getMeta().entrySet()) {
			sb.append(e.getValue());
			sb.append(", ");
		}
		return sb.toString();
	}

	private Map<String, Meta> getMeta() {
		if (meta == null)
			meta = new HashMap<>();
		return meta;
	}

	private void setMeta(Map<String, Meta> meta) {
		this.meta = meta;
	}
	
	public MetaList remove(String name) {
		if (notEmpty(name))
			getMeta().remove(name);
		return this;
	}
	
	public boolean containsMeta(final String metaName) {
		return getMeta().containsKey(metaName);
	}
	
	public MetaList add(final String name, final Object value) {
		this.keysCached = null;
		getMeta().put(name, new Meta(name, value));
		return this;
	}
	
	public MetaList add(final String name, final Object value, final String desc) {
		this.keysCached = null;
		getMeta().put(name, new Meta(name, value, desc));
		return this;
	}
	
	public boolean add(final Meta meta) {
		if (meta == null)
			return false;
		
		this.keysCached = null;
		return getMeta().put(meta.getName(), meta) == null;
	}
	
	public int size() {
		return this.meta != null ? this.meta.size() : 0;
	}
	
	public void clear() {
		if (this.meta != null)
			this.meta.clear();
		this.keysCached = null;
	}
	
	public List<String> getKeys() {
		if (keysCached != null)
			return keysCached;
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(getMeta().keySet());
		Collections.sort(keys);
		keysCached = keys;
		return keysCached;
	}
	
	public Meta get(final int index) {
		if (this.meta == null)
			return null;
		return getMeta().get(getKeys().get(index));
	}
	
	public Meta get(final String metaName) {
		return this.meta != null ? this.meta.get(metaName) : null;
	}
	
	public String getAsString(String metaName) {
		Meta meta = get(metaName);
		return meta != null ? meta.getAsString() : null;
	}
	
	public <T> T getAsObject(String metaName, Class<T> clazz) {
		String tmp = getAsString(metaName);
		return tmp != null ? JsonUtils.fromJson(tmp, clazz) : null;
	}

	public boolean getAsBoolean(String metaName, boolean defaultValue) {
		Meta m = get(metaName);
		if (m != null)
			return m.getAsBoolean();
		return defaultValue;
	}
	
	public int getAsInteger(String metaName, int defaultValue) {
		Meta m = get(metaName);
		if (m != null)
			return m.getAsInteger();
		return defaultValue;
	}

	@Override
	public Iterator<Meta> iterator() {
		return new Iterator<Meta>() {
			private Iterator<String> mapIterator = getMeta().keySet().iterator();
			
			@Override
			public boolean hasNext() {
				return mapIterator.hasNext();
			}

			@Override
			public Meta next() {
				return meta.get(mapIterator.next());
			}

			@Override
			public void remove() {
				throw new NotImplementedException();
			}
		};
	}

	public MetaList filterIn(String filter) {
		MetaList ml = new MetaList();
		
		for (Meta meta : this) {
			if (meta.isNameSpecified() && meta.getName().toLowerCase().contains(filter))
				ml.add(meta);
			else if (meta.isDescriptionSpecified() && meta.getDescription().toLowerCase().contains(filter))
				ml.add(meta);
		}
		
		return ml;
	}

	public boolean add(MetaList metaList) {
		boolean added = false;
		for (Meta meta : metaList) {
			if (add(meta))
				added = true;
		}
		return added;
	}

	// TODO remove this function later
	public void fixD() {
		for (Meta meta : this) {
			if (meta.isValueDouble()) {
				if (meta.getAsString().contains("d"))
					meta.setValue(meta.getAsString().replace('d', 'e'));
			}
		}
	}

	public boolean isNotEmpty(String name) {
		Meta m = get(name);
		return m != null && m.getValue() != null && notEmpty(m.getValue().toString());
	}

	public void removeWithPrefix(String prefix) {
		List<String> toRemove = new ArrayList<>();
		
		for (String key : getMeta().keySet()) {
			if (key.startsWith(prefix))
				toRemove.add(key);
		}
		
		for (String key : toRemove)
			getMeta().remove(key);
	}

	public MetaList copy(String metaNameSource, String metaNameDest) {
		add(get(metaNameSource)
				.cloneThis()
				.setName(metaNameDest));
		return this;
	}
}
