package net.hypki.libs5.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;


public class BigFileSplitted implements Iterable<Line> {
	private BufferedReader reader;
	private FileReader fileReader;
	
	public BigFileSplitted(File file) throws FileNotFoundException {
		reader = new BufferedReader(fileReader = new FileReader(file));
	}

	public BigFileSplitted(String filePath) throws FileNotFoundException {
		reader = new BufferedReader(fileReader = new FileReader(filePath));
		
//		FileInputStream fstream = new FileInputStream(filePath);
//	    DataInputStream in = new DataInputStream(fstream);
//	    reader = new BufferedReader(new InputStreamReader(in));
	}
	
	public BigFileSplitted(BufferedReader buffReader) throws FileNotFoundException {
		reader = buffReader;
	}
	
	public BigFileSplitted(InputStream is) throws FileNotFoundException {
		reader = new BufferedReader(new InputStreamReader(is));
	}

	public void close() throws IOException {
		reader.close();
	}

	public FileIterator iterator() {
		return new FileIterator();
	}

	public class FileIterator implements Iterator<Line>, Iterable<Line> {
		private String currentLine;
		private long currentSeek = 0L;
//		private Line line = new Line();

		public boolean hasNext() {
			try {
				currentSeek += currentLine != null ? currentLine.length() + 1 : 0;
				currentLine = reader.readLine();
			} catch (Exception ex) {
				currentLine = null;
			}

			return currentLine != null;
		}

		public Line next() {
//			line.setLine(currentLine);
//			return line;
			return new Line(currentLine);
		}

		public void remove() {
		}
		
		public long getCurrentSeek() {
			return currentSeek;
		}
		
		@Override
		public Iterator<Line> iterator() {
			return this;
		}
	}
}