package net.hypki.libs5.utils.math;

public class GaussianParams {

	private double norm = 0.0;
	
	private double mean = 0.0;
	
	private double sigma = 0.0;
	
	public GaussianParams() {
		
	}

	public double getNorm() {
		return norm;
	}

	public void setNorm(double norm) {
		this.norm = norm;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getSigma() {
		return sigma;
	}

	public void setSigma(double sigma) {
		this.sigma = sigma;
	}
	
}
