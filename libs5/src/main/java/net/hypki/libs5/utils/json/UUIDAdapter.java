package net.hypki.libs5.utils.json;

import java.lang.reflect.Type;
import java.util.UUID;

import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class UUIDAdapter implements InstanceCreator<UUID>, JsonDeserializer<UUID>, JsonSerializer<UUID> {

	@Override
	public JsonElement serialize(UUID arg0, Type arg1, JsonSerializationContext arg2) {
		return new JsonPrimitive(arg0.toString());
	}

	@Override
	public UUID deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		return UUID.fromString(arg0.getAsString());
	}

	@Override
	public UUID createInstance(Type arg0) {
		return UUID.randomUUID();
	}

}
