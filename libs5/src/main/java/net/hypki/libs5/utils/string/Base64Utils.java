package net.hypki.libs5.utils.string;

public class Base64Utils {

	public static String encode(String toEncode) {
		if (toEncode == null)
			return null;
		return jodd.util.Base64.encodeToString(toEncode);
	}
	
	public static String encode(byte[] toEncode) {
		if (toEncode == null)
			return null;
		return jodd.util.Base64.encodeToString(toEncode);
	}
	
	public static byte[] decodeToByte(String toDecode) {
		if (toDecode == null)
			return null;
		return jodd.util.Base64.decode(toDecode);
	}
	
	public static String decode(String toDecode) {
		if (toDecode == null)
			return null;
//		return new String(Base64.decode(toDecode));
		return new String(jodd.util.Base64.decode(toDecode));
	}
}
