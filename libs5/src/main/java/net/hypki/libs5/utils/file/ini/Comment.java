package net.hypki.libs5.utils.file.ini;

import com.google.gson.annotations.Expose;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Comment extends Line {
	
	@Expose
	@NotNull
	private String comment = null;

	public Comment() {
		
	}
	
	public Comment(String comment) {
		setComment(comment);
	}
	
	@Override
	public String toString() {
		return "#" + getComment();
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
