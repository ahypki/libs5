package net.hypki.libs5.utils.sha;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class SHAManager {
	public static final String MD5 = "MD5";
	public static final String SHA256 = "SHA-256";
	public static final String SHA512 = "SHA-512";
	
	public static String getSHA(String filename, String algorithm) throws IOException {
		try {
			/* The input stream. */
			FileInputStream inputStream = null;

			/* Used to hash the file. */
			MessageDigest messageDigest = null;
			
			/* Create the file hasher with the selected hash function. */
			messageDigest = MessageDigest.getInstance(algorithm);
			
			/* Open the file. */
			inputStream = new FileInputStream(filename);
			
			/* Used for reading chunks from the file. */
			byte[] bytes = new byte[1024];

			/* Stores the number of bytes read. */
			int bytesRead;
			
			/* Used for calculating the percent done. */
			long currentAmmountRead = 0;
			
			/* Read the file in chunks while the thread is still running. */
			while ((bytesRead = inputStream.read(bytes)) != -1) {

				/*
				 * Add the number of bytes read to the current amount of bytes
				 * read.
				 */
				currentAmmountRead += bytesRead;

				/* Update the message digest with the data read. */
				messageDigest.update(bytes, 0, bytesRead);
			}				
			
			/* close the input stream. */
			inputStream.close();			
			
			/* Convert the digest to a hex string. */
			return convertByteArrayToHexString(messageDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new IOException("Cannot calculate checksum of the file " + filename, e);
		} catch (FileNotFoundException e) {
			throw new IOException("Cannot calculate checksum of the file " + filename, e);
		}
	}
	
	public static String getSHA128(String data) throws IOException {
		return getSHA128(data != null ? data.getBytes() : null);
	}
	
	public static String getSHA256(String data) throws IOException {
		return getSHA(data != null ? data.getBytes() : null, SHA256);
	}
	
	public static String getSHA(String data) throws IOException {
		return getSHA(data != null ? data.getBytes() : null);
	}
	
	public static String getSHA(byte [] data) throws IOException {
		return getSHA(data, SHA512);
	}
	
	public static String getSHA128(byte [] data) throws IOException {
		return getSHA(data, MD5);
	}
	
	public static String getSHA(byte [] data, String algorithm) throws IOException {
		try {
			/* Used to hash the file. */
			MessageDigest messageDigest = null;
			
			/* Create the file hasher with the selected hash function. */
			messageDigest = MessageDigest.getInstance(algorithm);
			
			/* Update the message digest with the data read. */
			messageDigest.update(data);
			
			/* Convert the digest to a hex string. */
			return convertByteArrayToHexString(messageDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new IOException("Cannot calculate checksum of the byte[] data", e);
		}
	}
	
	/**
	 * Converts a byte array to a hex string.
	 * 
	 * @return A String holding the hex string of the byte array.
	 */
	private static String convertByteArrayToHexString(byte[] byteArray) {
		Formatter formatter = new Formatter();
		for (byte tempByte : byteArray) {
			formatter.format("%02x", tempByte);
		}
		String tmp = formatter.toString();
		formatter.close();
		return tmp;
	}
}
