package net.hypki.libs5.utils.system;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.collections.ListUtils;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;

/**
 * New version of the class ExecutingLinuxCommand - I need fresh look into
 * integration with bash
 * 
 * @author ahypki
 * 
 */
public class Basher {
	
	private List<String> args = null;
	
	public Basher() {
		
	}
	
	public void run() throws IOException {
		exec(ArrayUtils.toArray(getArgs()));
	}

	public static String executeCommand(String command, boolean waitForResponse) {
		String response = "";

		ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
		pb.redirectErrorStream(true);

//		pb
//			.redirectInput(Redirect.INHERIT)
//			.redirectOutput(Redirect.INHERIT)
//			.redirectError(Redirect.INHERIT);
		 
		// System.out.println("Linux command: " + command);

		try {
			Process shell = pb.start();

			if (waitForResponse) {

				// To capture output from the shell
				InputStream shellIn = shell.getInputStream();

				// Wait for the shell to finish and get the return code
				int shellExitStatus = shell.waitFor();
				// System.out.println("Exit status " + shellExitStatus);

				response = convertStreamToStr(shellIn);

				shellIn.close();
			}
			

		} catch (IOException | InterruptedException e) {
			System.out.println("Error occured while executing Linux command. Error Description: " + e.getMessage());
		}

		return response;
	}

	/*
	 * To convert the InputStream to String we use the Reader.read(char[]
	 * buffer) method. We iterate until the Reader return -1 which means there's
	 * no more data to read. We use the StringWriter class to produce the
	 * string.
	 */
	private static String convertStreamToStr(InputStream is) throws IOException {

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	public static void exec(String command) throws IOException {
		try {
			Process p = Runtime.getRuntime().exec(command);

			// StringBuffer output = new StringBuffer();
			// BufferedReader reader = new BufferedReader(new
			// InputStreamReader(p.getInputStream()));
			// String line = "";
			// while ((line = reader.readLine()) != null) {
			// output.append(line + "\n");
			// }
			// System.out.println("OUTPUT:" + output.toString());

			p.waitFor();
		} catch (InterruptedException e) {
			LibsLogger.error(Basher.class, "Process interrupted", e);
		}
	}

	public static void exec(String[] commandArray) throws IOException {
		try {
			Process p = Runtime.getRuntime().exec(commandArray);
			p.waitFor();
		} catch (InterruptedException e) {
			LibsLogger.error(Basher.class, "Process interrupted", e);
		}
	}
	
	public static List<String> exec(String[] commandArray, File workingDir) throws IOException {
//		try {
			ProcessBuilder pb = new ProcessBuilder(commandArray);
			pb.directory(workingDir);
			pb.redirectErrorStream(true);
//			pb.redirectOutput(Redirect.INHERIT);
//			pb.redirectError(Redirect.INHERIT);
			Process p = pb.start();
			
//			Process p = Runtime.getRuntime().exec(commandArray, null, workingDir);
//			System.out. println();
//			p.waitFor();
			
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
//			return input;
			List<String> output = new ArrayList<String>();
			String line = null;
			while((line = input.readLine()) != null)
				output.add(line);

			return output;
//		} catch (InterruptedException e) {
//			LibsLogger.error(Basher.class, "Process interrupted", e);
//			return null;
//		}
	}
	
	public static void exec(String[] commandArray, File workingDir, Writer out) throws IOException {
//		try {
			ProcessBuilder pb = new ProcessBuilder(commandArray);
			pb.directory(workingDir);
			pb.redirectErrorStream(true);
//			pb.redirectOutput(Redirect.INHERIT);
//			pb.redirectError(Redirect.INHERIT);
			Process p = pb.start();
			
//			Process p = Runtime.getRuntime().exec(commandArray, null, workingDir);
//			System.out. println();
//			p.waitFor();
			
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
//			return input;
//			List<String> output = new ArrayList<String>();
			String line = null;
			while((line = input.readLine()) != null) {
				out.append(line);
				out.append("\n");
			}

			out.close();
//			return output;
//		} catch (InterruptedException e) {
//			LibsLogger.error(Basher.class, "Process interrupted", e);
//			return null;
//		}
	}
	
	public static void execStreamApachePig(String[] commandArray, File workingDir, File stdOutput) throws IOException {
		ProcessBuilder pb = new ProcessBuilder(commandArray);
		pb.directory(workingDir);
		pb.redirectErrorStream(true);
		
		String javaHome = System.getenv("JAVA_HOME");
		String sparkHome = System.getenv("SPARK_HOME");
		String hadoopHome = System.getenv("HADOOP_HOME");
		
		assertTrue(notEmpty(javaHome), "JAVA_HOME is not set");
		assertTrue(notEmpty(sparkHome), "SPARK_HOME is not set");
		assertTrue(notEmpty(hadoopHome), "HADOOP_HOME is not set");
		
		pb.environment().put("JAVA_HOME", javaHome);
		pb.environment().put("SPARK_HOME", sparkHome);
		pb.environment().put("HADOOP_HOME", hadoopHome);
		Process p = pb.start();
		
		FileUtils.saveToFile(p.getInputStream(), stdOutput);
		
//		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
//		
//		FileWriter fw = null;
//		try {
//			fw = new FileWriter(stdOutput);
//			String line = null;
//			while((line = input.readLine()) != null){
//				fw.append(line);
//				fw.append("\n");
////				System.out.println(line);
////				fw.flush();
//			}
//			fw.close();
//		} catch (Exception e) {
//			LibsLogger.error(Basher.class, "Cannot write to log", e);
//		}
	}
	
	public static void execStreamOut(String[] commandArray, File workingDir, File stdOutput) throws IOException {
		ProcessBuilder pb = new ProcessBuilder(commandArray);
		pb.directory(workingDir);
		pb.redirectErrorStream(true);
		Process p = pb.start();
		
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		FileWriter fw = null;
		try {
			fw = new FileWriter(stdOutput);
			String line = null;
			while((line = input.readLine()) != null){
				fw.append(line);
				fw.append("\n");
				System.out.println(line);
			}
			fw.close();
		} catch (Exception e) {
			LibsLogger.error(Basher.class, "Cannot write to log", e);
		}
	}

	@Deprecated
	public static void execBindSysIO(String[] commandArray) throws IOException {
//		try {
			String line;
		    OutputStream stdin = null;
		    InputStream stderr = null;
		    InputStream stdout = null;

		      // launch EXE and grab stdin/stdout and stderr
		      Process process = Runtime.getRuntime ().exec (commandArray);
		      stdin = process.getOutputStream ();
		      stderr = process.getErrorStream ();
		      stdout = process.getInputStream ();

		      // "write" the parms into stdin
		      line = "param1" + "\n";
		      stdin.write(line.getBytes() );
		      stdin.flush();

		      line = "param2" + "\n";
		      stdin.write(line.getBytes() );
		      stdin.flush();

		      line = "param3" + "\n";
		      stdin.write(line.getBytes() );
		      stdin.flush();

		      stdin.close();

		      // clean up if any output in stdout
		      BufferedReader brCleanUp =
		        new BufferedReader (new InputStreamReader (stdout));
		      while ((line = brCleanUp.readLine ()) != null) {
		        //System.out.println ("[Stdout] " + line);
		      }
		      brCleanUp.close();

		      // clean up if any output in stderr
		      brCleanUp =
		        new BufferedReader (new InputStreamReader (stderr));
		      while ((line = brCleanUp.readLine ()) != null) {
		        //System.out.println ("[Stderr] " + line);
		      }
		      brCleanUp.close();
//		} catch (InterruptedException e) {
//			LibsLogger.error(Basher.class, "Process interrupted", e);
//		}
	}

	private List<String> getArgs() {
		if (args == null)
			args = new ArrayList<String>();
		return args;
	}

	private Basher setArgs(List<String> args) {
		this.args = args;
		return this;
	}
	
	public Basher add(String arg) {
		getArgs().add(arg);
		return this;
	}
	
	public Basher add(String param, String value) {
		getArgs().add(param);
		getArgs().add(value);
		return this;
	}
}
