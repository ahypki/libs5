package net.hypki.libs5.utils.url;

public class BrowserException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8668804199509303126L;

	public BrowserException() {
		
	}
	
	public BrowserException(String message) {
		super(message);
	}
	
	public BrowserException(String message, Throwable t) {
		super(message, t);
	}
}
