package net.hypki.libs5.utils.string;

import org.unbescape.html.HtmlEscape;

/**
 * Docs: http://www.unbescape.org/
 * 
 * @author ahypki
 *
 */
public class EscapeUnescapeUtils {
	
	public static String htmlUnescape(String escapedText) {
		return HtmlEscape.unescapeHtml(escapedText);
	}
	
	// INFO more for HTML, XML, CSS, JSON etc. on http://www.unbescape.org/
}
