package net.hypki.libs5.utils.date;

import org.apache.commons.lang.NotImplementedException;

public enum DayOfWeek {
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY,
	
	ANY;
	
	public String toStringHuman() {
		if (this.ordinal() == MONDAY.ordinal())
			return "Monday";
		else if (this.ordinal() == TUESDAY.ordinal())
			return "Tuesday";
		else if (this.ordinal() == WEDNESDAY.ordinal())
			return "Wednesday";
		else if (this.ordinal() == THURSDAY.ordinal())
			return "Thursday";
		else if (this.ordinal() == FRIDAY.ordinal())
			return "Friday";
		else if (this.ordinal() == SATURDAY.ordinal())
			return "Saturday";
		else if (this.ordinal() == SUNDAY.ordinal())
			return "Sunday";
		else if (this.ordinal() == ANY.ordinal())
			return "any day of week";
		else
			throw new NotImplementedException("Unimplemented case " + this.name());
	}
}
