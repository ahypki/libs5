package net.hypki.libs5.utils.math;

import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;

import com.udojava.evalex.Expression;

public class MathUtils {
	
	public static int min(int a, int b, int c) {
		return Math.min(a, Math.min(b, c));
	}

	public static BigDecimal eval(String mathExpression) {
		try {
			if (mathExpression.indexOf("==") > 0) {
				for (int i = 0; i < 100; i++) {
					String gr = RegexUtils.firstGroup("([\\w]+[\\s]*==[\\s]*[\\w]+)", mathExpression, Pattern.CASE_INSENSITIVE);
					if (gr == null)
						break;
					
					String [] parts = StringUtilities.split(gr, "==");
					if (parts[0].trim().equalsIgnoreCase(parts[1].trim()))
						mathExpression = mathExpression.replace(gr, "1");
					else
						mathExpression = mathExpression.replace(gr, "0");
				}
			}

			// TODO implement inline fi with '?'
			if (mathExpression.indexOf("?") > 0) {
				String inlineIfGroup = "[^\\\\?\\:\\(\\)]+";
				for (int i = 0; i < 100; i++) {
					List<String> gr = RegexUtils.allGroups("(" + inlineIfGroup + ")\\?(" 
							+ inlineIfGroup + "):(" + inlineIfGroup + ")", mathExpression, Pattern.CASE_INSENSITIVE);
					if (gr == null)
						break;
					
					mathExpression = mathExpression.replace(gr.get(0) + "?" 
							+ gr.get(1) + ":" + gr.get(2), 
							String.format("IF(%s, %s, %s)", gr.get(0), gr.get(1), gr.get(2)));
					
					if (mathExpression.indexOf("?") < 0)
						break;
				}
			}
			
			return new Expression(mathExpression).eval();
					
//			Expression e = new Expression(mathExpression);
//			
//			e.addOperator(new AbstractOperator("===", 30, true) {
//				@Override
//				public LazyNumber eval(LazyNumber v1, LazyNumber v2) {
//					return super.eval(v1, v2);
//				}
//				@Override
//				public BigDecimal eval(BigDecimal arg0, BigDecimal arg1) {
//					return null;
//				}
////			    @Override
////			    public BigDecimal eval(BigDecimal v1, BigDecimal v2) {
////			        return v1.movePointRight(v2.toBigInteger().intValue());
////			    }
//			});
			
//
//			e.addLazyFunction(new AbstractLazyFunction("STREQ", 2) {
//				private LazyNumber ZERO = new LazyNumber() {
//					public BigDecimal eval() {
//						return BigDecimal.ZERO;
//					}
//
//					public String getString() {
//						return "0";
//					}
//				};
//				private LazyNumber ONE = new LazyNumber() {
//					public BigDecimal eval() {
//						return BigDecimal.ONE;
//					}
//
//					public String getString() {
//						return null;
//					}
//				};
//
//				@Override
//				public LazyNumber lazyEval(List<LazyNumber> lazyParams) {
//					if (lazyParams.get(0).getString().equals(lazyParams.get(1).getString())) {
//						return ONE;
//					}
//					return ZERO;
//				}
//			});
//
//			return e.eval();
		} catch (Exception e) {
			// LibsLogger.error(MathUtils.class, "Cannot compute expr: ", mathExpression);
			return null;// new BigDecimal(0);
		}
	}

	public static PolynomialFunction fitPolinomial(final int degree, final List<WeightedObservedPoint> points) {
		PolynomialCurveFitter fitter = PolynomialCurveFitter.create(degree);
		return new PolynomialFunction(fitter.fit(points));

	}
	
	public static void main(String[] args) throws IOException {
		FileWriter fw = new FileWriter("radians.dat");
		
		fw.append("#  randRad  randDeg   randSin\n");
		
		for (int i = 0; i < 100000; i++) {
			double randRad = RandomUtils.nextDouble() * 2.0 * Math.PI;
			double randSin = sin(RandomUtils.nextDouble() * Math.PI);
			
			double phi = 2.0 * Math.PI * RandomUtils.nextDouble();
			double theta = acos(2.0 * RandomUtils.nextDouble() - 1.0);
			
			double vkickKMS_FB = 1.0;
			double v1 = vkickKMS_FB * sin(theta) * cos(phi);
			double v2 = vkickKMS_FB * sin(theta) * sin(phi);
			double v3 = vkickKMS_FB * cos(theta);
			
			fw.append("" + randRad + 					// 1
					" " + Math.toDegrees(randRad) + 	// 2
					" " + sin(randRad) + 				// 3
					" " + randSin +						// 4
					" " + theta + " " + phi +			// 5, 6
					" " + sin(theta) + " " + sin(phi) +	// 7, 8
					" " + cos(theta) + " " + cos(phi) +	// 9, 10
					" " + v1 + " " + v2 + " " + v3 +	// 11, 12, 13
					"\n");

//			LibsLogger.info(MathUtils.class, Math.sqrt(v1*v1 + v2*v2 + v3*v3));
		}
		
		fw.close();
		
		LibsLogger.info(MathUtils.class, "Finished");
	}
}
