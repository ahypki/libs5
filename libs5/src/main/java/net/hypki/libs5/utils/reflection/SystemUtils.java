package net.hypki.libs5.utils.reflection;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.system.Basher;
import net.hypki.libs5.utils.url.UrlUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

public class SystemUtils {
	
	private static UUID thisJvmId = UUID.randomUUID();
	
	public static UUID getThisJvmId() {
		return thisJvmId;
	}
	
	public static FileExt getWorkingDirectory() {
		return new FileExt(System.getProperty("user.dir"));
	}
	
	public static Iterable<Thread> iterateThreads() {
		final Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		final Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
		
		return new Iterable<Thread>() {
			@Override
			public Iterator<Thread> iterator() {
				return new Iterator<Thread>() {
					private int idx = 0;
					
					@Override
					public Thread next() {
						return threadArray[idx++];
					}
					
					@Override
					public boolean hasNext() {
						while (idx < threadArray.length) {
							if (threadArray[idx].isInterrupted() || threadArray[idx].isAlive() == false) {
								idx++;
								continue;
							}
							return true;
						}
						return false;
					}
				};
			}
		};
	}

	public static File getJarExecutionPath() throws UnsupportedEncodingException {
		FileExt ext = new FileExt(UrlUtilities.urlDecode(SystemUtils.class.getProtectionDomain().getCodeSource()
				.getLocation().getPath()));
		if (ext.getFilenameOnly().endsWith(".jar"))
			return ext.getParentFile();
		else
			return ext;
	}

	public static String getHomePath() {
		return System.getProperty("user.home");
	}

	public static boolean isWindows() {
		return (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (System.getProperty("os.name").toLowerCase().indexOf("nix") >= 0 
				|| System.getProperty("os.name").toLowerCase().indexOf("nux") >= 0 
				|| System.getProperty("os.name").toLowerCase().indexOf("aix") > 0);
	}

	public static InputStream getResourceAsStream(String filename) {
		if (SystemUtils.class.getClassLoader().getResource(filename) != null) {
			return SystemUtils.class.getClassLoader().getResourceAsStream(
					filename);
		} else if (SystemUtils.class.getResource(filename) != null) {
			return SystemUtils.class.getResourceAsStream(filename);
		} else
			return null;
	}
	
	public static InputStream getResourceAsStream(Class<?> clazz, String filename) {
		if (clazz.getClassLoader().getResource(filename) != null) {
			return clazz.getClassLoader().getResourceAsStream(
					filename);
		} else if (clazz.getResource(filename) != null) {
			return clazz.getResourceAsStream(filename);
		} else
			return null;
	}

	public static URL getResourceURL(String filename) {
		if (SystemUtils.class.getClassLoader().getResource(filename) != null) {
			return SystemUtils.class.getClassLoader().getResource(filename);
		} else if (SystemUtils.class.getResource(filename) != null) {
			return SystemUtils.class.getResource(filename);
		} else
			return null;
	}
	
	public static URL getResourceURL(Class<?> clazz, String filename) {
		if (clazz.getClassLoader().getResource(filename) != null) {
			return clazz.getClassLoader().getResource(filename);
		} else if (clazz.getResource(filename) != null) {
			return clazz.getResource(filename);
		} else
			return null;
	}

	// public static String readFileContent(Class clazz, String filename) {
	// try {
	// if (clazz.getResource(filename) != null) {
	// InputStream is = clazz.getResourceAsStream(filename);
	// BufferedReader br = new BufferedReader(new InputStreamReader(is));
	//
	// StringBuilder builder = new StringBuilder();
	// String line = null;
	// while ((line = br.readLine()) != null) {
	// builder.append(line);
	// builder.append("\n");
	// }
	// return builder.toString();
	// } else
	// return null;
	// } catch (IOException e) {
	// LibsLogger.error(SystemUtils.class, "Cannot read file", e);
	// return null;
	// }
	// }

	/**
	 * Search for file using ClassLoader. If it fails then in CLASSPATH. Returns
	 * content of the file
	 * 
	 * @param filename
	 * @return
	 */
	public static String readFileContent(String filename) {
		return readFileContent(SystemUtils.class, filename);
	}

	/**
	 * Search for file using ClassLoader. If it fails then in CLASSPATH. Returns
	 * content of the file
	 * 
	 * @param filename
	 * @return
	 */
	public static String readFileContent(Class<?> clazz, String filename) {
		try {
			if (clazz.getClassLoader().getResource(filename) != null) {
				InputStream is = clazz.getClassLoader().getResourceAsStream(
						filename);
				BufferedReader br = new BufferedReader(
						new InputStreamReader(is));

				StringBuilder builder = new StringBuilder();
				String line = null;
				while ((line = br.readLine()) != null) {
					builder.append(line);
					builder.append("\n");
				}
				return builder.toString();
			} else if (clazz.getResource(filename) != null) {
				InputStream is = clazz.getResourceAsStream(filename);
				BufferedReader br = new BufferedReader(
						new InputStreamReader(is));

				StringBuilder builder = new StringBuilder();
				String line = null;
				while ((line = br.readLine()) != null) {
					builder.append(line);
					builder.append("\n");
				}
				return builder.toString();
			} else
				return null;
		} catch (IOException e) {
			LibsLogger.error(SystemUtils.class, "Cannot read file", e);
			return null;
		}
	}

	public static List<String> getJvmArguments() {
		RuntimeMXBean RuntimemxBean = ManagementFactory.getRuntimeMXBean();
		return RuntimemxBean.getInputArguments();
	}
	
	public static int getMyuserId() {
		try {
			return NumberUtils.toInt(Basher.exec(new String[] {"id", "-u"}, null).get(0).trim());
		} catch (IOException e) {
			LibsLogger.error(SystemUtils.class, "Cannot check user ID", e);
			return -1;
		}
	}
	
	public static String getMyUsername() {
		try {
			return Basher.exec(new String[] {"whoami"}, null).get(0).trim();
		} catch (IOException e) {
			LibsLogger.error(SystemUtils.class, "Cannot check user ID", e);
			return null;
		}
	}

	public static boolean isRunningFromJar() {
		String res = SystemUtils.class.getResource(SystemUtils.class.getSimpleName() + ".class").toString();
		return res.startsWith("jar:");
	}

	public static String getHostname() {
		try {
			return Basher.executeCommand("/bin/hostname", true);
//			return InetAddress.getLocalHost().getHostName();
		} catch (Throwable e) {
			LibsLogger.error(SystemUtils.class, "Cannot find the hostname", e);
			return null;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(getHostname());
	}
}
