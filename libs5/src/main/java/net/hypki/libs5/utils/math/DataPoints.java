package net.hypki.libs5.utils.math;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;

import org.apache.commons.math3.analysis.function.Gaussian;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.fitting.GaussianCurveFitter;
import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;

public class DataPoints implements Iterable<DataPoints.DataPoint> {
	private static GaussianCurveFitter gaussianFitter = GaussianCurveFitter.create();
	private static PolynomialCurveFitter linearFitter = PolynomialCurveFitter.create(1);
	
	private List<DataPoint> dataPoints = null;

	public DataPoints() {
		
	}
	
	@Override
	public Iterator<DataPoint> iterator() {
		return getDataPoints().iterator();
	}
	
	public DataPoints add(double x, double y) {
		getDataPoints().add(new DataPoint(x, y));
		return this;
	}
	
	private List<DataPoint> getDataPoints() {
		if (dataPoints == null)
			dataPoints = new ArrayList<DataPoints.DataPoint>();
		return dataPoints;
	}
	
	private void clear() {
		getDataPoints().clear();
	}
	
	public class DataPoint {
		private double x = 0.0;
		private double y = 0.0;
		private double yWeight = 1.0;
		
		public DataPoint() {
			
		}
		
		public DataPoint(double x, double y) {
			setX(x);
			setY(y);
		}
		
		public DataPoint(double x, double y, double yWeight) {
			setX(x);
			setY(y);
			setyWeight(yWeight);
		}

		@Override
		public String toString() {
			return String.format("[%lf, %lf, weight=%lf]", getX(), getY(), getyWeight());
		}
		
		public double getX() {
			return x;
		}

		public void setX(double x) {
			this.x = x;
		}

		public double getY() {
			return y;
		}

		public void setY(double y) {
			this.y = y;
		}

		public double getyWeight() {
			return yWeight;
		}

		public void setyWeight(double yWeight) {
			this.yWeight = yWeight;
		}
		
	}

	public List getXAsList() {
		List<Double> xList = new ArrayList<Double>();
		for (DataPoint dp : this) {
			xList.add(dp.getX());
		}
		return xList;
	}
	
	public List getYAsList() {
		List<Double> yList = new ArrayList<Double>();
		for (DataPoint dp : this) {
			yList.add(dp.getY());
		}
		return yList;
	}
	
	public Gaussian fitGaussian(Double xMin, Double xMax, GaussianParams outGaussianParams) {
		List<WeightedObservedPoint> points = new ArrayList<WeightedObservedPoint>();
		for (DataPoints.DataPoint dp : this) {
			if (xMin != null && dp.getX() < xMin)
				continue;
			if (xMax != null && dp.getX() > xMax)
				continue;
			points.add(new WeightedObservedPoint(1.0, dp.getX(), dp.getY()));
		}
	
		Watch watch = new Watch();
		double [] gaussParams = gaussianFitter.fit(points);
		
		LibsLogger.debug(getClass(), "Gaussian params: Normalization=", gaussParams[0], " Mean=", gaussParams[1], " Sigma=", gaussParams[2], " found in ", watch);
		
		if (outGaussianParams != null) {
			outGaussianParams.setNorm(gaussParams[0]);
			outGaussianParams.setMean(gaussParams[1]);
			outGaussianParams.setSigma(gaussParams[2]);
		}
		
		return new Gaussian(gaussParams[0], gaussParams[1], gaussParams[2]);
	}
	
	public Gaussian fitGaussianWeighted(Double xMin, Double xMax, GaussianParams outGaussianParams) {
		DataPoints selectedDP = new DataPoints();
		
		for (DataPoints.DataPoint dp : this) {
			if (xMin != null && dp.getX() < xMin)
				continue;
			if (xMax != null && dp.getX() > xMax)
				continue;
			selectedDP.add(dp.getX(), dp.getY());
		}
		
		double meanDP = selectedDP.mean();
		List<WeightedObservedPoint> points = new ArrayList<WeightedObservedPoint>();
		for (DataPoints.DataPoint dp : selectedDP) {
			points.add(new WeightedObservedPoint(Math.max(1.0, 1.0 + (dp.getY() - meanDP) * 3.0), dp.getX(), dp.getY()));
		}
	
		Watch watch = new Watch();
		double [] gaussParams = gaussianFitter.fit(points);
		
		LibsLogger.debug(getClass(), "Gaussian params: Normalization=", gaussParams[0], " Mean=", gaussParams[1], " Sigma=", gaussParams[2], " found in ", watch);
		
		if (outGaussianParams != null) {
			outGaussianParams.setNorm(gaussParams[0]);
			outGaussianParams.setMean(gaussParams[1]);
			outGaussianParams.setSigma(gaussParams[2]);
		}
		
		return new Gaussian(gaussParams[0], gaussParams[1], gaussParams[2]);
	}
	
	private double mean() {
		double sum = 0.0;
		for (DataPoints.DataPoint dp : this)
			sum += dp.getY();
		return sum / (double) getDataPoints().size();
	}

	public PolynomialFunction fitLinear() {
		return fitLinear(null, null);
	}
	
	public PolynomialFunction fitLinear(Double xMin, Double xMax) {
		List<WeightedObservedPoint> points = new ArrayList<WeightedObservedPoint>();
		for (DataPoints.DataPoint dp : this) {
			if (xMin != null && dp.getX() < xMin)
				continue;
			if (xMax != null && dp.getX() > xMax)
				continue;
			points.add(new WeightedObservedPoint(1.0, dp.getX(), dp.getY()));
		}
		
		PolynomialFunction poly = new PolynomialFunction(linearFitter.fit(points));
		
		LibsLogger.debug(getClass(), "Fitted linear: ", poly);
		
		return poly;
	}
	
	public PolynomialFunction fitPolynomial(Double xMin, Double xMax, int degree) {
		List<WeightedObservedPoint> points = new ArrayList<WeightedObservedPoint>();
		for (DataPoints.DataPoint dp : this) {
			if (xMin != null && dp.getX() < xMin)
				continue;
			if (xMax != null && dp.getX() > xMax)
				continue;
			points.add(new WeightedObservedPoint(1.0, dp.getX(), dp.getY()));
		}
		
		PolynomialCurveFitter polynomialFitter = PolynomialCurveFitter.create(degree);
		PolynomialFunction poly = new PolynomialFunction(polynomialFitter.fit(points));
		
		LibsLogger.debug(getClass(), "Fitted polynomial: ", poly);
		
		return poly;
	}
	
	public PolynomialFunction fitPolynomial(Double xMin, Double xMax, Double xMin2, Double xMax2, int degree) {
		List<WeightedObservedPoint> points = new ArrayList<WeightedObservedPoint>();
		for (DataPoints.DataPoint dp : this) {
			if (xMin != null) {
				if (dp.getX() > xMin && dp.getX() < xMax) {
					points.add(new WeightedObservedPoint(1.0, dp.getX(), dp.getY()));
					continue;
				}
			}
			
			if (xMin2 != null) {
				if (dp.getX() > xMin2 && dp.getX() < xMax2) {
					points.add(new WeightedObservedPoint(1.0, dp.getX(), dp.getY()));
					continue;
				}
			}
			
			if (xMin == null && xMin2 == null)
				points.add(new WeightedObservedPoint(1.0, dp.getX(), dp.getY()));
		}
		
		PolynomialCurveFitter polynomialFitter = PolynomialCurveFitter.create(degree);
		PolynomialFunction poly = new PolynomialFunction(polynomialFitter.fit(points));
		
		LibsLogger.debug(getClass(), "Fitted polynomial: ", poly);
		
		return poly;
	}
	
	public Gaussian fitGaussian(GaussianParams outGaussianParams) {
		return fitGaussian(null, null, outGaussianParams);
	}

	public void saveToFile(File file) throws IOException {
		FileWriter fw = new FileWriter(new FileExt(file));
		fw.append("# x   y   weight\n");
		for (DataPoint dp : this) {
			fw.append(String.valueOf(dp.getX()));
			fw.append(" ");
			fw.append(String.valueOf(dp.getY()));
			fw.append("\n");
		}
		fw.close();
	}
}
