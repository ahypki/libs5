package net.hypki.libs5.utils.string;

import java.util.HashMap;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class LocaleUtils {
	
	private static TripleMap<String, String, String> cache = new TripleMap<>();
	private static boolean RELOAD_ALWAYS = false;
	
	static {
		RELOAD_ALWAYS = System.getProperty("RELOAD_ALWAYS") != null && System.getProperty("RELOAD_ALWAYS").equals("true");
	}
	
	public static String getMsg(Class clazz, String lang, String key, String defaultMsg) {
		String cacheKey = clazz.getName() + lang;
		HashMap<String, String> map = cache.get(cacheKey);
		
		// for developer mode always reload messages
		if (map == null || RELOAD_ALWAYS) {
			HashMap<String, String> newMap = readClass(clazz, lang);
			cache.put(cacheKey, newMap);
			map = newMap;
		}
		
		String tmp = cache.get(cacheKey, key);
		return tmp != null 
				? tmp 
						: defaultMsg;
	}

	private static HashMap<String, String> readClass(Class clazzToRead, String lang) {
		String filename = clazzToRead.getSimpleName() + "_" + lang.toString().toLowerCase() + ".properties";
		String content = SystemUtils.readFileContent(clazzToRead, filename);
		
		if (content == null) {
			filename = clazzToRead.getSimpleName() + "_" + lang.toString().toLowerCase() + ".lang";
			content = SystemUtils.readFileContent(clazzToRead, filename);
		}
		
		if (content == null) {
			LibsLogger.error(LocaleUtils.class, "Cannot find file " + filename + " for the class " + clazzToRead.getName() + ". Cannot read localized messages ", lang);
			return null;
		}
		
		HashMap<String, String> newMap = new HashMap<>();
		for (String line : content.split("\\n")) {
			int i = line.indexOf('=');
			if (i > 0)
				newMap.put(line.substring(0, i).trim(), line.substring(i + 1).trim());
		}
		return newMap;
	}
}
