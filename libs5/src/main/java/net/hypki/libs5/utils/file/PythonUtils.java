package net.hypki.libs5.utils.file;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.system.Basher2;

public class PythonUtils {
	public static void runPythonScript(final FileExt pyScript) throws Exception {
		for (String line : new Basher2()
				.setWorkingDir(new FileExt(pyScript.getParentFile()))
				.add("python")
				.add(pyScript.getAbsolutePath())
				.runIter()) {
				
			LibsLogger.debug(PythonUtils.class, line);
		}
	}
	
	public static void main(String[] args) throws Exception {
		runPythonScript(new FileExt("$HOME/projekty/wmi-inz-beans-ml/Inzynierka_Gwiazdy/machine_learning/split_files/train-learn.py"));
	}
}
