package net.hypki.libs5.utils.collections;

import java.util.HashMap;

public class QuadrupleMap<K1, K2, K3, V> extends HashMap<K1, HashMap<K2, HashMap<K3, V>>> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3392754298317601161L;
	
	private V returningValueIfNull = null;
	
	public QuadrupleMap() {
		
	}
	
	public QuadrupleMap(V returningValueIfNull) {
		this.setReturningValueIfNull(returningValueIfNull);
	}
	
	public V put(K1 key1, K2 key2, K3 key3, V value) {
		if (get(key1) == null)
			super.put(key1, new HashMap<K2, HashMap<K3, V>>());
		
		if (get(key1).get(key2) == null)
			get(key1).put(key2, new HashMap<K3, V>());
		
		return get(key1).get(key2).put(key3, value);
	};
	
	public V get(K1 key1, K2 key2, K3 key3) {
		if (get(key1) == null)
			return getReturningValueIfNull() != null ? getReturningValueIfNull() : null;
		
		if (get(key1).get(key2) == null)
				return getReturningValueIfNull() != null ? getReturningValueIfNull() : null;
		
		V v = get(key1).get(key2).get(key3);
		
		if (v != null)
			return v;
		else if (getReturningValueIfNull() != null)
			return getReturningValueIfNull();
		else
			return null;
	}

	public V getReturningValueIfNull() {
		return returningValueIfNull;
	}

	public void setReturningValueIfNull(V returningValueIfNull) {
		this.returningValueIfNull = returningValueIfNull;
	};
}
