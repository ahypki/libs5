package net.hypki.libs5.utils.system;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import jodd.util.StringUtil;
import net.hypki.libs5.utils.LibsLogger;


public class ExecutingLinuxCommand {
	
	/**
	 * Returns Pair with output and error stream respectively
	 * @param commands
	 * @param executingDir
	 * @return
	 */
	public static void executeInSeparateThread(final String [] commands, final File executingDir, final boolean printOut, final boolean printErr) {
		
		Thread t = new Thread() {
			@Override
			public void run() {
				Process process = null;
				try {
					process = Runtime.getRuntime().exec(commands, null, executingDir);
					
//					if (printErr) {
//						BufferedReader readerError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//						String line = null;
//						while ((line = readerError.readLine()) != null ) {
//							System.out.println(line);
//						}
//					}
//					
//					if (printOut) {
//						BufferedReader readerOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
//						String line = null;
//						while ((line = readerOutput. readLine()) != null ) {
//							System.out.println(line);
//						}
//					}
										
					// wait for the thread
					process.waitFor();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		
		t.start();
		try {
			t.join();
		} catch (InterruptedException e1) {
			LibsLogger.error("Cannot join() threads", e1);
		}
	}
	
	public static void executeInSeparateThread(final String [] commands) {
		new Thread() {
			@Override
			public void run() {
				Process process = null;
				try {
					process = Runtime.getRuntime().exec(commands);
					
//					String results = null;
//					InputStream is = null;
//					try {
//						is = process.getInputStream();
//						StringBuilder bfstr = new StringBuilder();
//						int ch = 0;
//
//						while ((ch = is.read()) != -1)
//							bfstr.append((char) ch);
//
//						results = new String(bfstr.toString());
//					} finally {
//						if (is != null)
//							is.close();
//					}
//					System.out.println(results);
					
					// wait for the thread
					process.waitFor();
					
					// run command which should be executed after thread was closed
//					System.out.println("Command " + commands + " finished");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	public static void executeInSeparateThread(final String [] commands, final String [] finalCommands) {
		new Thread() {
			@Override
			public void run() {
				Process process = null;
				try {
					process = Runtime.getRuntime().exec(commands);

//					String results = null;
//
//					InputStream is = process.getInputStream();
//					StringBuilder bfstr = new StringBuilder();
//					int ch = 0;
//
//					while ((ch = is.read()) != -1)
//						bfstr.append((char) ch);
//
//					results = new String(bfstr.toString());
//					is.close();
//
//					if (results.length() > 0)
//						System.out.println(results);
					
					// wait for the thread
					process.waitFor();
					
				} catch (Exception e) {
					LibsLogger.error(ExecutingLinuxCommand.class, "Command execution failed", e);
				} finally {
					try {
						// run command which should be executed after thread was closed
						Runtime.getRuntime().exec(finalCommands);
					} catch (IOException e) {
						LibsLogger.error(ExecutingLinuxCommand.class, "Final command execution failed", e);
					}					
				}
			}
		}.start();
	}
		
	public static String executeCommand(ArrayList<String> commands, File executingDir) throws IOException {
		if (commands == null || commands.size() == 0)
			return null;
		
		String [] cmdArray = new String [commands.size()];
		
		for (int i = 0; i < commands.size(); i++) {
			cmdArray[i] = commands.get(i);
		}
		
		return executeCommand(cmdArray, executingDir);
	}
	
	public static String executeCommand(String command, File executingDir) throws IOException {
		Process process = null;
		InputStream is = null;
		InputStream err = null;
		
		try {
//			StringBuilder wholeCmd = new StringBuilder();
//			for (String cmd : commands) {
//				wholeCmd.append(cmd);
//				wholeCmd.append(" ");
//			}
//			System.out.print("Executing: " + wholeCmd.toString());
			
//			process = Runtime.getRuntime().exec(wholeCmd.toString(), null, executingDir);
			if (executingDir != null)
				process = Runtime.getRuntime().exec(command, null, executingDir);
			else
				process = Runtime.getRuntime().exec(command);
			
			is = process.getInputStream();
			err = process.getErrorStream();
			StringBuilder bfstr = new StringBuilder();
			int ch = 0;
			
			StringBuilder line = new StringBuilder();
			while ((ch = is.read()) != -1) {
				bfstr.append((char) ch);
				
				line.append((char) ch);
				
				if ((char) ch == '\n') {
					System.out.print(line);
					line = new StringBuilder();
				}
			}
			
			while ((ch = err.read()) != -1) {
				bfstr.append((char) ch);
				
				line.append((char) ch);
				
				if ((char) ch == '\n') {
					System.out.print(line);
					line = new StringBuilder();
				}
			}
			
			return bfstr.toString();
		} finally {
			if (is != null)
				is.close();
		}
//		return executeCommand(StringUtil.splitc(command, ' '), executingDir);
	}
	
	public static String executeCommand(final String [] commands, File executingDir) throws IOException {
		Process process = null;
		InputStream is = null;
		InputStream err = null;
		
		try {
//			StringBuilder wholeCmd = new StringBuilder();
//			for (String cmd : commands) {
//				wholeCmd.append(cmd);
//				wholeCmd.append(" ");
//			}
//			System.out.print("Executing: " + wholeCmd.toString());
			
//			process = Runtime.getRuntime().exec(wholeCmd.toString(), null, executingDir);
			if (executingDir != null)
				process = Runtime.getRuntime().exec(commands, null, executingDir);
			else
				process = Runtime.getRuntime().exec(commands);
			
			is = process.getInputStream();
			err = process.getErrorStream();
			StringBuilder bfstr = new StringBuilder();
			int ch = 0;
			
			StringBuilder line = new StringBuilder();
			while ((ch = is.read()) != -1) {
				bfstr.append((char) ch);
				
				line.append((char) ch);
				
				if ((char) ch == '\n') {
					System.out.print(line);
					line = new StringBuilder();
				}
			}
			
			while ((ch = err.read()) != -1) {
				bfstr.append((char) ch);
				
				line.append((char) ch);
				
				if ((char) ch == '\n') {
					System.out.print(line);
					line = new StringBuilder();
				}
			}
			
			return bfstr.toString();
		} finally {
			if (is != null)
				is.close();
		}
	}
	
	public static ArrayList<String> executeCommand2(final String [] commands, File executingDir) throws IOException {
		Process process = null;
		InputStream is = null;
		InputStream err = null;
		
		try {
			StringBuilder wholeCmd = new StringBuilder();
			for (String cmd : commands) {
				wholeCmd.append(cmd);
				wholeCmd.append(" ");
			}
			LibsLogger.debug("Executing: " + wholeCmd.toString());
			
			process = Runtime.getRuntime().exec(wholeCmd.toString(), null, executingDir);
			
			is = process.getInputStream();
			err = process.getErrorStream();
			ArrayList<String> lines = new ArrayList<String>();
			int ch = 0;
			
			StringBuilder line = new StringBuilder();
			while ((ch = is.read()) != -1) {
				line.append((char)ch);
				
				if ((char) ch == '\n') {
					lines.add(line.toString());
					line = new StringBuilder();
				}
			}
			
			while ((ch = err.read()) != -1) {
				line.append((char) ch);
				
				if ((char) ch == '\n') {
					lines.add(line.toString());
					line = new StringBuilder();
				}
			}
			
			return lines;
		} finally {
			if (is != null)
				is.close();
		}
	}
}
