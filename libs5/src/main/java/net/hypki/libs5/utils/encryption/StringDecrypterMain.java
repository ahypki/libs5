package net.hypki.libs5.utils.encryption;

import net.hypki.libs5.utils.system.StandardInput;


public class StringDecrypterMain {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		PasswordsKeeper passwordKeeper = new PasswordsKeeper();
		
		System.out.print("Type text to decrypt: ");
		String toDecrypt = StandardInput.readStringFromStandardInput();
		
		System.out.print("Type password: ");
		String password1 = StandardInput.readStringFromStandardInput();
		
		passwordKeeper.setPassword(password1);
		StringEncrypter enc = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME, passwordKeeper.getPassword());
		
		System.out.println(enc.decrypt(toDecrypt));
	}

}
