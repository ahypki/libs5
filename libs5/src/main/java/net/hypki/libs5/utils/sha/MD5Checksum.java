package net.hypki.libs5.utils.sha;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.hypki.libs5.utils.string.ByteUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class MD5Checksum {
	
	MessageDigest messageDigest = null;

	public MD5Checksum() throws NoSuchAlgorithmException {
		messageDigest = MessageDigest.getInstance("MD5");//SHAManager.SHA512);
	}
	
	public MD5Checksum update(double d) {
		messageDigest.update(ByteUtils.toBytes(d), 0, 8);
		return this;
	}
	
	public MD5Checksum update(long l) {
		messageDigest.update(ByteUtils.toBytes(l), 0, 8);
		return this;
	}
	
	public MD5Checksum update(String s) throws UnsupportedEncodingException {
		byte[] b = s.getBytes("UTF-8");
		messageDigest.update(b, 0, b.length);
		return this;
	}
	
	public MD5Checksum update(byte[] b) throws UnsupportedEncodingException {
		messageDigest.update(b, 0, b.length);
		return this;
	}

	public MD5Checksum update(byte[] b, int offset, int length) throws UnsupportedEncodingException {
		messageDigest.update(b, offset, length);
		return this;
	}
	
	public String getChecksum() {
		return StringUtilities.getHex(messageDigest.digest());
	}
}
