package net.hypki.libs5.utils.file.ini;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;

import java.util.Iterator;

import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

/**
 * Key-value pair from an INI file
 * 
 * @author ahypki
 *
 */
public class Value extends Line implements Iterable<Object> {
	
	@Expose
	private String section = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private String key = null;
	
	@Expose
	@NotEmpty
	@NotNull
	private Object value = null;

	public Value() {
		
	}
	
	public Value(String section, String key, Object value) {
		setSection(section);
		setKey(key);
		setValue(value);
	}
	
	public Value(String key, Object value) {
		setKey(key);
		setValue(value);
	}
	
	@Override
	public Iterator<Object> iterator() {
		String [] parts = valueAsString().contains(",") ? split(valueAsString(), ",") : split(valueAsString());
		
		return new Iterator<Object>() {
			private int i = 0;
			
			@Override
			public Object next() {
				return parts[i++].trim();
			}
			
			@Override
			public boolean hasNext() {
				return i < parts.length;
			}
		};
	}
	
	@Override
	public String toString() {
		return !nullOrEmpty(getValue()) ? String.format("%s = %s", getKey(), getValue().toString()) : "";
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	public boolean isDoubleValue() {
		return this.value instanceof Double;
	}
	
	public boolean isLongValue() {
		return this.value instanceof Long;
	}
	
	public boolean isStringValue() {
		return this.value instanceof String;
	}
	
	public Double valueAsDouble() {
		return (Double) this.value;
	}
	
	public Long valueAsLong() {
		return (Long) this.value;
	}
	
	public String valueAsString() {
		return (String) this.value;
	}

	public boolean isList() {
		return isStringValue() 
				&& (valueAsString().contains(",") || valueAsString().contains(","));
	}
}
