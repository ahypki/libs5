package net.hypki.libs5.utils.api;

public enum APIArgType {
	STRING,
	INTEGER,
	BOOLEAN,
	LONG,
	DOUBLE,
	UUID,
	MAP,
	LIST,
	INPUTSTREAM
}
