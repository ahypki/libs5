package net.hypki.libs5.utils.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import jodd.datetime.JDateTime;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.JDateTimeInstanceCreator;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.SimpleDateInstanceCreator;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;

public class JsonUtils {
	
	private static GsonBuilder tmpBuilder = null;
	private static JsonParser jsonParser = null;
	private static Gson gson = null;
	private static Gson gsonPrettyPrinter = null;
	
	private static long regexCount = 0;
	
	public static JsonElement toJson(Object obj) {
		return getGson().toJsonTree(obj);
	}
	
	public static JsonElement readJsonFile(String path) throws FileNotFoundException {
		File file = new File(path);
		
		if (file.exists() == true) {
			Reader reader = new FileReader(file);
			return getJsonParser().parse(reader);
		} else {
			return new JsonObject();
		}
	}
	
	public static <T> T fromJson(String jsonData, Class<T> clazz) {
		try {
			return getGson().fromJson(jsonData, clazz);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static <T> T fromJson(JsonElement json, Class<T> clazz) {
		try {
			return getGson().fromJson(json, clazz);
		} catch (JsonSyntaxException e) {
			LibsLogger.error(JsonUtils.class, "Cannot deserialize object from JSON", e);
			return null;
		}
	}
	
	public static JsonElement parseJson(String jsonData) {
		return getJsonParser().parse(jsonData);
	}
	
	public static JsonElement readJsonElement(JsonElement json, String jsonsPath) {
		if (json == null)
			return null;
		
		String[] pathParts = jsonsPath == null ? new String[]{} : StringUtilities.split(jsonsPath, '/');
		JsonElement tmp = json;
		for (int i = 0; i < pathParts.length; i++) {
			if (tmp.getAsJsonObject().get(pathParts[i]) != null)
				tmp = tmp.getAsJsonObject().get(pathParts[i]);
			else
				return null;
		}
		return tmp;
	}
	
	/**
	 * 
	 * @param <T>
	 * @param clazz
	 * @param jsonsPath - separated by '/'
	 * @return
	 */
	public static int readInt(JsonElement json, String jsonsPath, int defaultValue) {
		String tmp = readString(json, jsonsPath);
		return tmp != null ? NumberUtils.toInt(tmp, defaultValue) : defaultValue;
//		String[] pathParts = StringUtilities.split(jsonsPath, '/');
//		JsonObject tmp = json.getAsJsonObject();
//		for (int i = 0; i < (pathParts.length - 1); i++) {
//			tmp = tmp.get(pathParts[i]).getAsJsonObject();
//		}
//		JsonElement element = tmp.get(pathParts[pathParts.length - 1]);
//		return element != null ? element.getAsInt() : defaultValue;
	}

	/**
	 * 
	 * @param <T>
	 * @param clazz
	 * @param jsonsPath - separated by '/'
	 * @return
	 */
	public static long readLong(JsonElement json, String jsonsPath, long defaultValue) {
		if (json == null)
			return defaultValue;
		
		String[] pathParts = StringUtilities.split(jsonsPath, '/');
		JsonObject tmp = json.getAsJsonObject();
		for (int i = 0; i < (pathParts.length - 1); i++) {
			tmp = tmp.get(pathParts[i]).getAsJsonObject();
			
			if (tmp == null)
				return defaultValue;
		}
		
		if (tmp == null)
			return defaultValue;
		
		JsonElement element = tmp.get(pathParts[pathParts.length - 1]);
		return element != null ? element.getAsLong() : defaultValue;
	}
	
	/**
	 * 
	 * @param <T>
	 * @param clazz
	 * @param path - separated by '/'
	 * @return
	 */
	public static String readString(JsonElement json, String path) {
		return readString(json, path, null);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param clazz
	 * @param path - separated by '/'
	 * @return
	 */
	public static String readString(JsonElement json, String path, String defaultValue) {
		if (json == null)
			return defaultValue;
		
		String[] pathParts = StringUtilities.split(path, '/');
		JsonObject tmp = json.getAsJsonObject();
		for (int i = 0; i < (pathParts.length - 1); i++) {
			JsonElement e = tmp.get(pathParts[i]);
			if (e != null) {
				if (!e.isJsonObject())
					return null;
				tmp = e.getAsJsonObject();
			} else
				return defaultValue;
		}
		JsonElement ele = tmp != null ? tmp.get(pathParts[pathParts.length - 1]) : null;
		if (ele != null && !ele.isJsonNull()) {
			if (ele.isJsonPrimitive())
				return ele.getAsString();
			else
				return ele.toString();
		} else
			return defaultValue;
	}
		
	public static float readFloat(JsonElement json, String path, float defaultValue) {
		JsonElement ele = readJsonElement(json, path);
		return ele != null ? ele.getAsFloat() : defaultValue;
	}
	
	public static JsonArray readArray(JsonElement json, String path) {
		JsonElement ele = readJsonElement(json, path);
		return ele != null ? ele.getAsJsonArray() : null;
	}
	
	public static boolean readBoolean(JsonElement json, String path, boolean defaultValue) {
		JsonElement ele = readJsonElement(json, path); //tmp.get(pathParts[pathParts.length - 1]);
		return ele != null ? ele.getAsBoolean() : defaultValue;
	}
	
	public static Boolean readBoolean(JsonElement json, String path) {
		JsonElement ele = readJsonElement(json, path);
		return ele != null ? ele.getAsBoolean() : null;
	}

	public static String objectToString(Object obj) {
		return getGson().toJson(obj);
	}
	
	public static String objectToStringPretty(Object obj) {
		return obj == null ? null : getGsonPrettyPrinter().toJson(obj);
	}
	
	private static void init() {
		long start = System.currentTimeMillis();
		
		tmpBuilder = new GsonBuilder()
//			.registerTypeAdapter(UniqueList.class, new GsonTagsListCreator())
//			.registerTypeAdapter(FieldList.class, new FieldMapInstanceCreator())
//			.registerTypeAdapter(Field.class, new FieldInstanceCreator())
//			.registerTypeAdapter(Job.class, new JobInstanceCreator())
//			.registerTypeAdapter(Select.class, new SelectInstanceCreator())
//			.registerTypeAdapter(SelectList.class, new SelectListInstanceCreator())
//			.registerTypeAdapter(Update.class, new UpdateInstanceCreator())
//			.registerTypeAdapter(UpdateList.class, new UpdateListInstanceCreator())
//			.registerTypeAdapter(Pair.class, new PairInstanceCreator())
			.serializeSpecialFloatingPointValues()
			.registerTypeAdapter(SimpleDate.class, new SimpleDateInstanceCreator())
			.registerTypeAdapter(UUID.class, new UUIDAdapter())
			.registerTypeAdapter(JDateTime.class, new JDateTimeInstanceCreator())
			.registerTypeAdapter(Object.class, new JsonDeserializer<Object>() {
				@Override
				public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
					JsonPrimitive value = json.getAsJsonPrimitive();
					if (value.isBoolean())
						return value.getAsBoolean();
					else if (value.isNumber()) {
						if (value.getAsNumber() instanceof Integer)
							return value.getAsNumber().intValue();
						else if (value.getAsNumber() instanceof Long)
							return value.getAsNumber().longValue();
						else if (value.getAsNumber() instanceof BigDecimal)
							return value.getAsNumber().doubleValue();
						else
							return value.getAsNumber().doubleValue();
					} else if (value.isString()) {
						regexCount += 2;
						if (regexCount % 1000 == 0)
							LibsLogger.error(JsonUtils.class, "Regex.matches fired " + regexCount + 
									" in JsonUtils already");
						
						if (value.getAsString().matches("[\\d]+\\.[\\deE\\+\\-]+"))
							return value.getAsNumber().doubleValue();
						else if (value.getAsString().matches("[\\d]+"))
							return value.getAsNumber().longValue();
						else
							return value.getAsString();
//					} else if (value.isNumber()) {
//						return value.getAsNumber();
					} else {
						return value.getAsString();
					}
				}
		    })
			.excludeFieldsWithoutExposeAnnotation();
		
//		if (Settings.getSettings().getAsJsonObject().get("Gson") != null) {
//			JsonArray arr = Settings.getSettings().getAsJsonObject().get("Gson").getAsJsonArray();
//			for (JsonElement jsonElement : arr) {
//				Class clazzType = ReflectionUtility.getClass(jsonElement.getAsJsonObject().get("type").getAsString());
//				Object adapter = ReflectionUtility.getInstance(jsonElement.getAsJsonObject().get("adapter").getAsString());
//				tmpBuilder = tmpBuilder.registerTypeAdapter(clazzType, adapter);
//			}
//		}
		
		String gsonAdaptersFile = "gson-adapters.json";
		if (System.getProperty("gsonAdapters") != null)
			gsonAdaptersFile = System.getProperty("gsonAdapters");
		
		String tmp = null;
//		try {
			if ((tmp = SystemUtils.readFileContent(gsonAdaptersFile)) != null) {
				JsonArray arr = JsonUtils.parseJson(tmp).getAsJsonArray();
				for (JsonElement jsonElement : arr) {
					try {
						Class clazzType = ReflectionUtility.getClass(jsonElement.getAsJsonObject().get("type").getAsString());
						Object adapter = ReflectionUtility.getInstance(jsonElement.getAsJsonObject().get("adapter").getAsString());
						tmpBuilder = tmpBuilder.registerTypeAdapter(clazzType, adapter);
					} catch (Exception e) {
						LibsLogger.error(JsonUtils.class, "Cannot find Gson adapter " + jsonElement.toString(), e);
					}
				}
			}
//		} catch (FileNotFoundException e) {
//			LibsLogger.error(JsonUtils.class, "Cannot find file", e);
//		}
		
		gson = tmpBuilder.create();
		gsonPrettyPrinter = tmpBuilder.setPrettyPrinting().create();
		LibsLogger.debug("gson started in " + (System.currentTimeMillis() - start));
	}
	
	public static void registerTypeAdapter(Type type, Object typeAdapter) {
		LibsLogger.debug(JsonUtils.class, "Registering in JsonUtils type ", type, " with an adapter ", typeAdapter.getClass());
		
		if (tmpBuilder == null)
			init();
		
		tmpBuilder.registerTypeAdapter(type, typeAdapter);
		
		gson = tmpBuilder.create();
		gsonPrettyPrinter = tmpBuilder.setPrettyPrinting().create();
	}
	
	public static Gson getGson() {
		if (gson == null) {
			init();
		}
		return gson;
	}
	
	public static Gson getGsonPrettyPrinter() {
		if (gsonPrettyPrinter == null) {
			init();
//			gsonPrettyPrinter = new GsonPrettyPrinter(getGson(), "\t");
		}
		return gsonPrettyPrinter;
	}
	
	private static JsonParser getJsonParser() {
		if (jsonParser == null) {
			jsonParser = new JsonParser();
		}
		return jsonParser;
	}

	public static void saveJson(JsonElement jsonElement, String pathname, boolean prettyString) throws IOException {
		if (prettyString == false) {
			FileUtils.saveToFile(getGson().toJson(jsonElement), new File(pathname), true);			
		} else {
			FileUtils.saveToFile(getGsonPrettyPrinter().toJson(jsonElement), new File(pathname), true);
		}
	}
	
	public static boolean isJsonPrimitive(Object o) {
		if (o == null)
			return false;
		else if (o instanceof String)
			return true;
		else if (o instanceof Number)
			return true;
		else if (o instanceof Boolean)
			return true;
		else
			return false;
	}
	
	public static JsonPrimitive toJsonPrimitive(Object o) {
		if (o == null)
			return null;
		else if (o instanceof String)
			return new JsonPrimitive((String) o);
		else if (o instanceof Number)
			return new JsonPrimitive((Number) o);
		else if (o instanceof Boolean)
			return new JsonPrimitive((Boolean) o);
		else
			return null;
	}

	public static boolean isJsonSyntax(String str) {
		try {
			JsonElement json = parseJson(str);
			return !(json instanceof JsonNull)
					&& !(json instanceof JsonPrimitive);
		} catch (Exception e) {
			return false;
		}
	}

	public static void putSetting(JsonElement json, String jsonPath, Object value) {
		if (json == null)
			return;
		
		String[] pathParts = StringUtilities.split(jsonPath, '/');
		JsonElement tmp = json;
		for (int i = 0; i < (pathParts.length - 1); i++) {
			if (tmp.getAsJsonObject().get(pathParts[i]) == null)
				tmp.getAsJsonObject().add(pathParts[i], new JsonObject());
			tmp = tmp.getAsJsonObject().get(pathParts[i]);
		}

		if (value == null) {
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], "");
		} else if (value instanceof String) {
			String s = (String) value;
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], s);
		} else if (value instanceof Boolean) {
			Boolean b = (Boolean) value;
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], b);
		} else if (value instanceof Double) {
			Double d = (Double) value;
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], d);
		} else if (value instanceof Integer) {
			Integer i = (Integer) value;
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], i);
		} else if (value instanceof Long) {
			Long l = (Long) value;
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], l);
		} else if (value instanceof Float) {
			Float f = (Float) value;
			tmp.getAsJsonObject().addProperty(pathParts[pathParts.length - 1], f);
		} else if (value instanceof JsonElement) {
			JsonElement je = (JsonElement) value;
			tmp.getAsJsonObject().add(pathParts[pathParts.length - 1], je);
		} else
			throw new RuntimeException("Unrecognized value type, cannot set value");
//			throw new IOException("Cannot save " + jsonPath + " with value " + value);
	}
	
	public static void add(JsonElement json, String jsonPath, Object value) {
		if (json == null)
			return;
		
		String[] pathParts = StringUtilities.split(jsonPath, '/');
		JsonElement found = json;
		for (int i = 0; i < pathParts.length; i++) {
			if (found.getAsJsonObject().get(pathParts[i]) == null) {
				if (isJsonPrimitive(value)) {
					found.getAsJsonObject().add(pathParts[i], toJsonPrimitive(value));
					return;
				} else
					found.getAsJsonObject().add(pathParts[i], (i <= (pathParts.length - 1)) ? new JsonObject() : new JsonArray());
			}
			found = found.getAsJsonObject().get(pathParts[i]);
		}
		
		if (!found.isJsonArray()) {
			throw new RuntimeException("Json in path " + jsonPath + " is not an array");
		}
		
		if (value instanceof JsonElement)
			found.getAsJsonArray().add((JsonElement) value);
		else
			found.getAsJsonArray().add(JsonUtils.toJson(value));
	}

	public static String readString(String json, String path) {
		return readString(parseJson(json), path);
	}
	
	public static <T> List<T> readList(String json, Class<T> clazz) {
		return readList(parseJson(json), "/", clazz);
	}

	public static <T> List<T> readList(JsonElement json, String path, Class<T> clazz) {
		List<T> objs = new ArrayList<T>();
		
		if (json instanceof JsonPrimitive
				&& path.equals("/")) {
			objs.add(JsonUtils.fromJson(json, clazz));
			return objs;
		}
		
		JsonArray ja = readArray(json, path);
		if (ja != null)
			for (JsonElement je : ja) {
				objs.add(JsonUtils.fromJson(je, clazz));
			}
		
		return objs;
	}
	
	public static JsonObject joinArrayIntoString(JsonObject jo, String jsonPath) {
		String[] pathParts = StringUtilities.split(jsonPath, '/');
		JsonElement tmp = jo;
		for (int i = 0; i < pathParts.length; i++) {
			String path = pathParts[i];
			
			if (tmp.getAsJsonObject().get(path) != null) {
				if (i == (pathParts.length - 1)) {
					// we have reached desired path
					if (tmp.getAsJsonObject().get(path).isJsonArray()) {
						StringBuilder sb = new StringBuilder();
						for (JsonElement line : tmp.getAsJsonObject().get(path).getAsJsonArray()) {
							sb.append(line.getAsString());
						}
						tmp.getAsJsonObject().remove(path);
						tmp.getAsJsonObject().add(path, new JsonPrimitive(sb.toString()));
					}
				} else {
					// go deeper
					jo = tmp.getAsJsonObject().get(path).getAsJsonObject();
					continue;
				}
			}
		}
		return jo;
	}

	public static boolean isJsonArray(String s) {
		if (!isJsonSyntax(s))
			return false;
		
		JsonElement je = parseJson(s);
		
		if (je.isJsonArray())
			return true;
		
		return false;
	}
}
