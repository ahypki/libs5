package net.hypki.libs5.utils.json;

import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class GsonStreamReader implements JsonStreamReader {

	private String filename = null;
	private JsonReader reader = null;
	
	public GsonStreamReader(String filename) {
		setFilename(filename);
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}

	@Override
	public void beginObject() throws IOException {
		getReader().beginObject();
	}

	@Override
	public void endObject() throws IOException {
		getReader().endObject();
	}
	
	@Override
	public void beginArray() throws IOException {
		getReader().beginArray();
	}
	
	@Override
	public void endArray() throws IOException {
		getReader().endArray();
	}

	@Override
	public boolean hasNext() throws IOException {
		return getReader().hasNext() && getReader().peek() != JsonToken.END_DOCUMENT;
	}

	@Override
	public String nextName() throws IOException {
		return getReader().nextName();
	}

	@Override
	public String nextString() throws IOException {
//		JsonToken peek = getReader().peek();
//		if (peek.equals(JsonToken.BEGIN_OBJECT)) {
//			getReader().beginObject();
//		}
		return getReader().nextString();
	}

	@Override
	public void close() throws IOException {
		getReader().close();
	}

	private JsonReader getReader() throws IOException {
		if (reader == null)
			reader = new JsonReader(new FileReader(getFilename()));
		return reader;
	}
	
	@Override
	public JsonElement next() throws IOException {
//		JsonElement je = null;
//		
//		JsonToken token = getReader().peek();
//		
//		if (token == JsonToken.BEGIN_ARRAY) {
//			je = new JsonArray();
//			getReader().beginArray();
//		} else if (token == JsonToken.BEGIN_OBJECT) {
//			je = new JsonObject();
//			getReader().beginObject();
//		} else
//			throw new NotImplementedException();
//		
//		while (true) {
//			token = getReader().peek();
//			
//			if (token == JsonToken.END_ARRAY) {
//				getReader().endArray();
//				break;
//			} else if (token == JsonToken.END_OBJECT) {
//				getReader().endObject();
//				break;
//			} else
//				read(je);
//		}
//		
//		return je;
		
		return read(null);
	}
	
	private JsonElement read(JsonElement jsonParent) throws IOException {
		String name = null;
		
		while (true) {
			JsonToken token = getReader().peek();

			if (token == JsonToken.BEGIN_ARRAY) {
				JsonArray jsonChild = new JsonArray();
				getReader().beginArray();
				jsonChild = (JsonArray) read(jsonChild);
				getReader().endArray();
				if (jsonParent == null) {
					jsonParent = jsonChild;
					break;
				} else if (name != null) {
					jsonParent.getAsJsonObject().add(name, jsonChild);
				}
			} else if (token == JsonToken.BEGIN_OBJECT) {
				JsonObject jsonChild = new JsonObject();
				getReader().beginObject();
				jsonChild = (JsonObject) read(jsonChild);
				if (jsonParent == null) {
					jsonParent = jsonChild;
					getReader().endObject();
					break;
				} else if (name != null) {
//					if (jsonParent == null)
//						jsonParent = new JsonObject();
					jsonParent.getAsJsonObject().add(name, jsonChild);
					name = null;
				} else {
//					if (jsonParent == null)
//						jsonParent = new JsonArray();
					jsonParent.getAsJsonArray().add(jsonChild);
				}
				getReader().endObject();
//				break;
			} else if (token == JsonToken.NAME) {
				name = getReader().nextName();
			} else if (token == JsonToken.STRING) {
				String str = getReader().nextString();
				if (name != null) {
					if (jsonParent == null)
						jsonParent = new JsonObject();
					jsonParent.getAsJsonObject().add(name, new JsonPrimitive(str));
					name = null;
				} else {
					if (jsonParent == null)
						jsonParent = new JsonArray();
					jsonParent.getAsJsonArray().add(new JsonPrimitive(str));
				}
//				break;
			} else if (token == JsonToken.END_OBJECT) {
				break;
			} else if (token == JsonToken.END_ARRAY) {
				break;
			} else
				throw new NotImplementedException();
		}
		
		return jsonParent;
	}
}
