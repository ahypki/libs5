package net.hypki.libs5.utils.api;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.annotations.Expose;

public class APICall {

	@Expose
	private String path = null;
	
	@Expose
	private List<APIArg> args = null;
	
	@Expose
	private APICallType type = null;
	
	@Expose
	private APIReturnType returnValue = null;
	
	public APICall() {
		
	}
	
	@Override
	public String toString() {
//		return JsonUtils.objectToStringPretty(this);
		StringBuilder sb = new StringBuilder();
		
		sb.append(getPath());
		for (APIArg arg : getArgs())
			sb.append(" --" + arg.getName());
		sb.append(" (" + getType() + ")");
		
		return sb.toString();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		if (path == null)
			return;
		
		if (path.startsWith("/"))
			path = path.substring(1);
		if (path.endsWith("/"))
			path = path.substring(0, path.length() - 1);
		
		this.path = path.replaceAll("//", "/");
	}

	public List<APIArg> getArgs() {
		if (args == null)
			args = new ArrayList<APIArg>();
		return args;
	}

	public void setArgs(List<APIArg> args) {
		this.args = args;
	}

	public APICallType getType() {
		return type;
	}

	public void setType(APICallType type) {
		this.type = type;
	}
	
	public boolean isGET() {
		return getType() == APICallType.GET;
	}
	
	public boolean isPOST() {
		return getType() == APICallType.POST;
	}
	
	public boolean isDELETE() {
		return getType() == APICallType.DELETE;
	}

	public APIReturnType getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(APIReturnType returnValue) {
		this.returnValue = returnValue;
	}
}
