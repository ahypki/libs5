package net.hypki.libs5.utils.file.ini;

import com.google.gson.annotations.Expose;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Section extends Line {
	
	@Expose
	@NotNull
	private String name = null;

	public Section() {
		
	}
	
	public Section(String name) {
		setName(name);
	}
	
	@Override
	public String toString() {
		return "[" + getName() + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
