package net.hypki.libs5.utils.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AssertUtils {

	public static void assertTrue(boolean condition, String message) throws ValidationException {
		if (condition == false)
			throw new ValidationException(message);
	}
	
	public static void assertFalse(boolean condition, String message) throws ValidationException {
		if (condition == true)
			throw new ValidationException(message);
	}
	
	public static void assertTrue(boolean condition, Object ... message) {
		if (condition == false) {
			StringBuilder b = new StringBuilder(200);
			for (Object obj : message) {
				b.append(obj);
				
				if (!(obj instanceof String))
					b.append(" ");
			}
			
			assertTrue(condition, b.toString());
		}
	}
}
