package net.hypki.libs5.utils.math;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Expression implements Serializable {

	@Expose
//	@NotEmpty
//	@NotNull
	private String field = null;
	
	@Expose
//	@NotEmpty
//	@NotNull
	private String mathExpr = null;
		
	public Expression() {
		
	}

	public Expression(String toParse) {
		toParse = toParse.trim();
		
		if (toParse.matches("\\w[\\w\\d]*"))
			setField(toParse);
		else
			setMathExpr(toParse);
	}
	
	@Override
	public String toString() {
		return isMathExpression() ? getMathExpr() : getField();
	}

	public String getField() {
		return field;
	}
	
	public boolean isField() {
		return field != null;
	}
	
	public boolean isMathExpression() {
		return mathExpr != null;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMathExpr() {
		return mathExpr;
	}

	public void setMathExpr(String mathExpr) {
		this.mathExpr = mathExpr;
	}
}
