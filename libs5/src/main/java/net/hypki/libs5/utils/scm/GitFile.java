package net.hypki.libs5.utils.scm;

import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.Tokenizer;

public class GitFile {
	
	private String path = null;
	
	private GitFileStatus status = null;

	public GitFile() {
		
	}
	
	public GitFile(String path, GitFileStatus status) {
		setPath(path);
		setStatus(status);
	}
	
	@Override
	public String toString() {
		return String.format("%s  %s", StringUtilities.padRight(getStatus().toString(), 12), getPath());
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		if (path.startsWith("\"") && path.endsWith("\""))
			this.path = path.substring(1, path.length() - 1);
		else
			this.path = path;
	}

	public GitFileStatus getStatus() {
		return status;
	}

	public void setStatus(GitFileStatus status) {
		this.status = status;
	}

}
