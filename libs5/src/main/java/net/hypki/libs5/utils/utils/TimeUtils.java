package net.hypki.libs5.utils.utils;

import net.hypki.libs5.utils.LibsLogger;

public class TimeUtils {

	public static void takeANap(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			LibsLogger.error(TimeUtils.class, "Cannot take a nap", e);
		}
	}
}
