package net.hypki.libs5.utils.utils;

public class ValidationException extends RuntimeException {

	public ValidationException() {
		
	}
	
	public ValidationException(String msg) {
		super(msg);
	}
	
	public ValidationException(Throwable t, String msg) {
		super(msg, t);
	}
	
	public ValidationException(String msg, Throwable t) {
		super(msg, t);
	}
}
