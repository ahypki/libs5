package net.hypki.libs5.utils.file;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import net.hypki.libs5.utils.LibsLogger;

/**
 * This class is Deprecated - to store properties use Json files instead
 * @author ahypki
 *
 */
@Deprecated
public class FileProperties {
	
	private String propertiesFileName;
	private HashMap<String, String> properties = new HashMap<String, String>();
	
	
	public FileProperties(String propertiesFilename) {
		setFilenameAndParse(propertiesFilename);
	}

	private void setFilenameAndParse(String propertiesFilename) {
		this.propertiesFileName = propertiesFilename;
		
		try {
			// Open the file
			FileInputStream fstream = new FileInputStream(propertiesFilename);
			
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			String strLine = null;
			
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// split line to key and value
				int index = strLine.indexOf('=');
				if (index > 0) {
					String key = strLine.substring(0, index);
					String value = strLine.substring(index + 1);
					
					properties.put(key, value);
				}
			}
			
			// Close the input stream
			in.close();
		} catch (FileNotFoundException e) {
			LibsLogger.error(FileProperties.class, "There is no file " + propertiesFilename, e);
		} catch (IOException e) {
			LibsLogger.error(FileProperties.class, "IOException occured when reading the file " + propertiesFilename, e);
		}
	}
	
	public HashMap<String, String> getProperties() {
		return properties;
	}
	
	public String getPropertyString(String key, String defaultValue) {
		if (getProperties() != null) {
			String ret = getProperties().get(key);
			if (ret != null)
				return ret;
			else
				return defaultValue;
		}
		return defaultValue;
	}
	
	public int getPropertyInt(String key, int defaultValue) {
		if (getProperties() != null) {
			String ret = getProperties().get(key);
			if (ret != null) {
				try {
					return Integer.parseInt(ret);
				} catch (NumberFormatException e) {
					return defaultValue;
				}
			}
			else
				return defaultValue;
		}
		return defaultValue;
	}

	public String getPropertiesFileName() {
		return propertiesFileName;
	}
}
