package net.hypki.libs5.utils.encryption;

public class EncryptionException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5197894505636328946L;

	public EncryptionException(String message) {
		super(message);
	}

	public EncryptionException(Throwable t) {
		super(t);
	}

	public EncryptionException(String message, Throwable t) {
		super(message, t);
	}
}
