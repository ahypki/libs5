package net.hypki.libs5.utils.reflection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.StringUtilities;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.NotImplementedException;

public class ReflectionUtility {
	
	// TODO the same as getClass()
	public static Class findClass(final String clazz) {
		try {
			if (StringUtilities.nullOrEmpty(clazz))
				return null;
			
			return Class.forName(clazz);
		} catch (ClassNotFoundException e) {
			LibsLogger.debug(ReflectionUtility.class, "Cannot find class " + clazz); // it is nothing special, so no error level
			return null;
		}
	}
	
	public static List<Class> getClasses(final String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    assert classLoader != null;
	    String path = packageName.replace('.', '/');
	    Enumeration<URL> resources = classLoader.getResources(path);
	    List<File> dirs = new ArrayList<File>();
	    while (resources.hasMoreElements()) {
	        URL resource = resources.nextElement();
	        dirs.add(new File(resource.getFile()));
	    }
	    ArrayList<Class> classes = new ArrayList<Class>();
	    for (File directory : dirs) {
	    	try {
	    		classes.addAll(findClasses(directory, packageName));
	    	} catch (ClassNotFoundException | NoClassDefFoundError e) {
	    		LibsLogger.error(ReflectionUtility.class, "Cannot search for classes in " + directory, e);
	    	}
	    }
	    return classes;
	}
	
	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 *
	 * @param directory   The base directory
	 * @param packageName The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
	    List<Class> classes = new ArrayList<Class>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    File[] files = directory.listFiles();
	    for (File file : files) {
	        if (file.isDirectory()) {
	            assert !file.getName().contains(".");
	            classes.addAll(findClasses(file, packageName + "." + file.getName()));
	        } else if (file.getName().endsWith(".class")) {
	            classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
	        }
	    }
	    return classes;
	}
	
	public static void printPathJar(Class<?> clazz) throws IllegalStateException {
		URL location = clazz.getResource('/' + clazz.getName().replace('.', '/') + ".class");
		LibsLogger.warn(ReflectionUtility.class, "Class ", clazz.getName(), " path: ", location.getPath());
		
		LibsLogger.warn(ReflectionUtility.class, "Class ", clazz.getName(), " path: ", clazz.getProtectionDomain().getCodeSource().getLocation());
	}
	
	public static void setFinalField(Class<?> clazz, final Field field, Object value) throws ReflectionException {
		try {
			field.setAccessible(true);

			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

			field.set(null, value);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new ReflectionException("Cannot set value to final field", e);
		}
	}

	public static void setFinalField(Class<?> clazz, final String fieldName, Object value) throws ReflectionException {
		try {
			setFinalField(clazz, clazz.getDeclaredField(fieldName), value);
		} catch (NoSuchFieldException | SecurityException e) {
			throw new ReflectionException("Cannot set value to final field", e);
		}
	}

	public static Object invokeMethod(Object invokingObject, String methodName, Object[] arguments) throws ReflectionException {
		return invokeMethod(invokingObject, methodName, arguments, false, false, false);
	}
	
	public static Object invokeMethod(Object invokingObject, String methodName, Object[] arguments, boolean matchArgs, 
			boolean searchInSuperclasses, boolean searchInPrivate) throws ReflectionException {
		try {
			Class clazz = invokingObject.getClass();
			
			while (true) {
				Method[] methods = searchInPrivate ? clazz.getDeclaredMethods() : clazz.getMethods();
	
				for (int i = 0; i < methods.length; i++) {
					if (methods[i].getName().equals(methodName)) {
						if (matchArgs) {
							Method methodToCall = methods[i];
							
							Object[] args = new Object[methodToCall.getGenericParameterTypes().length];
							int argsCounter = 0;
							
							for (Class methodArgClass : methodToCall.getParameterTypes()) {
								for (Object o : arguments) {
									if (methodArgClass.isAssignableFrom(o.getClass())) {
										args[argsCounter++] = o;
										break;
									}
								}
							}
							
							if (searchInPrivate)
								methodToCall.setAccessible(true);
							return methodToCall.invoke(invokingObject, args);
						} else {
							return methods[i].invoke(invokingObject, arguments);
						}
					}
				}
				
				if (searchInSuperclasses) {
					if (clazz.getSuperclass() == null)
						break;
					clazz = clazz.getSuperclass();
					continue;
				}
				
				break;
			}
			LibsLogger.error(ReflectionUtility.class, "Method ", methodName , " not found in class ", invokingObject.getClass().getSimpleName());
			return null;
		} catch (Exception ex) {
			throw new ReflectionException("Cannot invoke method " + methodName, ex.getCause());
		}
	}
	
//	public static Object invokeMethod(Object invokingObject, String methodName, Params arguments) throws ReflectionException {
//		try {
//			Method[] methods = invokingObject.getClass().getMethods();
//
//			for (int i = 0; i < methods.length; i++) {
//				if (methods[i].getName().equals(methodName)) {
//					Method m = methods[i];
//					
//					Object[] methodArguments = new Object[m.getTypeParameters().length];
//					int k = 0;
//					for (TypeVariable<Method> arg : m.getParameterAnnotations() ParameterTypes() TypeParameters()) {
//						methodArguments[k++] = arguments.get(arg.getName());
//					}
//					
//					return methods[i].invoke(invokingObject, methodArguments);
//				}
//			}
//			return null;
//		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
//			throw new ReflectionException("Cannot invoke method " + methodName, ex);
//		}
//	}

	public static int countMethodsWithAnnotation(Class<?> clazz, Class<? extends Annotation> annotation) {
		Method[] methods = clazz.getMethods();
		int counter = 0;

		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().startsWith("get") && methods[i].isAnnotationPresent(annotation)) {
				counter++;
			}
		}
		return counter;
	}

	public static boolean isSubclassOf(Class<?> clazz, Class<?> superClass) {
		Class<?> currentClass = clazz;
		while (true) {
			if (currentClass == null)
				return false;
			else if (currentClass.equals(superClass))
				return true;
			else if (currentClass.equals(Object.class))
				return false;
			else
				currentClass = currentClass.getSuperclass();
		}
	}

	public static ArrayList<Method> getMethodsRecursivelly(Class<?> clazz, Class<?> finalClass) {
		if (clazz == null)
			return null;

		if (isSubclassOf(finalClass, clazz))
			return null;

		ArrayList<Method> methods = new ArrayList<Method>();
		Class<?> currentClass = clazz;
		while (true) {
			for (Method m : currentClass.getDeclaredMethods()) {
				methods.add(m);
			}

			if (currentClass.equals(finalClass))
				break;
			else
				currentClass = currentClass.getSuperclass();
		}
		return methods;
	}

	public static ArrayList<Method> getMethodsRecursivelly(Class<?> clazz, Class<?> finalClass, Class<? extends Annotation> annotationPresent) {
		if (clazz == null)
			return null;

		if (isSubclassOf(finalClass, clazz))
			return null;

		ArrayList<Method> methods = new ArrayList<Method>();
		Class<?> currentClass = clazz;
		while (true) {
			for (Method m : currentClass.getDeclaredMethods()) {
				if (m.isAnnotationPresent(annotationPresent))
					methods.add(m);
			}

			if (currentClass.equals(finalClass))
				break;
			else
				currentClass = currentClass.getSuperclass();
			
			if (currentClass == null)
				break;
		}
		return methods;
	}

	public static List<Field> getFieldsRecursivelly(Class<?> clazz, Class<?> finalClass, Class<? extends Annotation> annotationPresent) {
		if (clazz == null)
			return null;

		if (!isSubclassOf(clazz, finalClass))
			return null;

		ArrayList<Field> fields = new ArrayList<Field>();
		Class<?> currentClass = clazz;
		while (true) {
			for (Field f : currentClass.getDeclaredFields()) {
				if (f.isAnnotationPresent(annotationPresent))
					fields.add(f);
			}

			if (currentClass.equals(finalClass))
				break;
			else
				currentClass = currentClass.getSuperclass();
		}
		return fields;
	}

	public static ArrayList<Field> getFieldsRecursivelly(Class<?> clazz, Class<?> finalClass) {
		if (clazz == null)
			return null;

		if (isSubclassOf(finalClass, clazz))
			return null;

		ArrayList<Field> fields = new ArrayList<Field>();
		Class<?> currentClass = clazz;
		while (true) {
			for (Field f : currentClass.getDeclaredFields()) {
				fields.add(f);
			}

			if (currentClass.equals(finalClass))
				break;
			else
				currentClass = currentClass.getSuperclass();
		}
		return fields;
	}
	
	public static Field getField(Class<?> clazz, String fieldName) {
		for (Field field : getFields(clazz)) {
			if (field.getName().equals(fieldName))
				return field;
		}
		return null;
	}

	public static ArrayList<Field> getFields(Class<?> clazz) {
		if (clazz == null)
			return null;

		// if (isSubclassOf(finalClass, clazz))
		// return null;

		ArrayList<Field> fields = new ArrayList<Field>();
		Class<?> currentClass = clazz;
		// while (true) {
		for (Field f : currentClass.getDeclaredFields()) {
			fields.add(f);
		}

		// if (currentClass.equals(finalClass))
		// break;
		// else
		// currentClass = currentClass.getSuperclass();
		// }
		return fields;
	}

	public static List<Class<?>> getClasses(Class<?> startingClass, boolean recursively) throws ClassNotFoundException {
		if (startingClass == null)
			return null;

		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		Class<?> temp = null;

		// // Get a File object for the package
		// File directory = null;
		// try {
		// ClassLoader cld = Thread.currentThread().getContextClassLoader();
		// if (cld == null) {
		// throw new ClassNotFoundException("Can't get class loader.");
		// }
		// String path = startingClass.getPackage().getName().replace('.', '/');
		//
		// URL resource = cld.getResource(path);
		// if (resource == null) {
		// throw new ClassNotFoundException("No resource for " + path);
		// }
		// directory = new File(resource.getFile());
		// } catch (NullPointerException x) {
		// throw new ClassNotFoundException(buildPackageName(startingClass,
		// directory)
		// + " does not appear to be a valid package");
		// }

		URL u = startingClass.getResource(startingClass.getSimpleName() + ".class");
		File directory = null;
		try {
			directory = new File(u.toURI());

			if (directory.exists()) {
				// Get the list of the files contained in the package
				List<File> files = FileUtils.getFiles(directory.getParentFile(), recursively);
				for (File f : files) {
					// we are only interested in .class files
					if (f.getName().endsWith(".class")) {
						String classPackage = buildPackageName(startingClass, f);

						try {
							temp = Class.forName(classPackage, false, startingClass.getClassLoader());
						} catch (ClassNotFoundException ex) {
							LibsLogger.error("Class not found", ex);
						}

						if (temp != null)
							classes.add(temp);
					}
				}
			} else {
				throw new ClassNotFoundException(buildPackageName(startingClass, directory) + " does not appear to be a valid package");
			}
		} catch (Exception e) {
			throw new ClassNotFoundException(String.format("Some problems with getting path to the %s class", startingClass.getSimpleName()), e);
		}

		return classes;
	}
	
	/*
	 * Adds the supplied Java Archive library to java.class.path. This is benign
	 * if the library is already loaded.
	 * Url: https://stackoverflow.com/questions/27187566/load-jar-dynamically-at-runtime
	 */
	public static synchronized void loadLibrary(java.io.File jar) throws RuntimeException {
		try {
			/*
			 * We are using reflection here to circumvent encapsulation; addURL
			 * is not public
			 */
			java.net.URLClassLoader loader = (java.net.URLClassLoader) ClassLoader
					.getSystemClassLoader();
			java.net.URL url = jar.toURI().toURL();
			/* Disallow if already loaded */
			for (java.net.URL it : java.util.Arrays.asList(loader.getURLs())) {
				if (it.equals(url)) {
					return;
				}
			}
			java.lang.reflect.Method method = java.net.URLClassLoader.class
					.getDeclaredMethod("addURL",
							new Class[] { java.net.URL.class });
			method.setAccessible(true); /* promote the method to public access */
			method.invoke(loader, new Object[] { url });
		} catch (final java.lang.NoSuchMethodException
				| java.lang.IllegalAccessException
				| java.net.MalformedURLException
				| java.lang.reflect.InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	public static List<Class<?>> getClassesFromJARFile(String jar, String packageName) throws IOException, ClassNotFoundException {
		// load the jar file
//		try {
//			loadLibrary(new File(jar));
//		} catch (Throwable t) {
//			LibsLogger.error(ReflectionUtility.class, "Problem with reading classes from jar", t.getMessage());
//		}
		
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		JarInputStream jarFile = new JarInputStream(new FileInputStream(jar));
		JarEntry jarEntry;

		packageName = packageName != null ? packageName.replaceAll("\\.", "/") : "";

		jarEntry = null;
		do {
			try {
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry != null) {
					String className = jarEntry.getName();
					if (className.endsWith(".class")) {
						className = className.substring(0, className.lastIndexOf('.'));
						className = className.lastIndexOf('$') >= 0 ? className.substring(0, className.lastIndexOf('$')) : className;
						if (className.startsWith(packageName))
							classes.add(Class.forName(className.replace('/', '.'), 
									false, 
									ReflectionUtility.class.getClassLoader()));
					}
				}
			} catch (Throwable t) {
				LibsLogger.error(ReflectionUtility.class, "Problem with reading a class", t.getMessage());
			}
		} while (jarEntry != null);

		if (jarFile != null)
			jarFile.close();

		return classes;
	}

	public static List<Class<?>> getClasses(Class<?> startingClass, Class<?> parentClass, boolean recursively) throws ClassNotFoundException {
		if (startingClass == null || parentClass == null)
			return null;

		List<Class<?>> allClasses = ReflectionUtility.getClasses(startingClass, recursively);
		ArrayList<Class<?>> selectedClasses = new ArrayList<Class<?>>();

		for (Class<?> clazz : allClasses) {
			if (ReflectionUtility.isSubclassOf(clazz, parentClass))
				selectedClasses.add(clazz);
		}

		return selectedClasses;
	}

	public static List<Class<?>> getClasses(Class<?> startingClass, Class<?> parentClass, Class<? extends Annotation> annotationPresent, boolean recursively)
			throws ClassNotFoundException {
		if (startingClass == null)
			return null;

		List<Class<?>> allClasses = ReflectionUtility.getClasses(startingClass, recursively);
		ArrayList<Class<?>> selectedClasses = new ArrayList<Class<?>>();

		for (Class<?> clazz : allClasses) {
			if (parentClass != null && !ReflectionUtility.isSubclassOf(clazz, parentClass))
				continue;
				
			if (clazz.getAnnotation(annotationPresent) != null)
				selectedClasses.add(clazz);
		}

		return selectedClasses;
	}

	private static String buildPackageName(Class<?> startingClass, File classFile) {
		String pathToStartingClass = startingClass.getResource(startingClass.getSimpleName() + ".class").getFile();
		if (SystemUtils.isUnix()) {
			String toRemove = startingClass.getPackage().getName().replace('.', '/');
			toRemove = pathToStartingClass.substring(0, pathToStartingClass.lastIndexOf(toRemove));
			String packageName = classFile.toString().substring(toRemove.length()).replace('/', '.');
			return packageName.substring(0, packageName.length() - ".class".length());
		} else if (SystemUtils.isWindows()) {
			String toRemove = startingClass.getPackage().getName().replace('.', '/');
			toRemove = pathToStartingClass.substring(0, pathToStartingClass.lastIndexOf(toRemove));
			String packageName = classFile.toString().substring(toRemove.length() - 1).replace('\\', '.').replace("$1", "");
			return packageName.substring(0, packageName.length() - ".class".length());
		} else
			throw new NotImplementedException();
	}

	/**
	 * Checks whether all classes from startingClass up to stoppingClass class contain only specified fields names in expectedFieldNames.
	 * 
	 * @param startingClass
	 * @param stoppingClass
	 * @param expectedFieldNames
	 */
	public static void expectedFieldsNames(Class<?> startingClass, Class<?> stoppingClass, Class<? extends Annotation> annotationPresent, List<String> expectedFieldNames)
			throws InvalidClassException {
		List<Field> classFields = ReflectionUtility.getFieldsRecursivelly(startingClass, stoppingClass, annotationPresent);
		for (Field classField : classFields) {
			if (expectedFieldNames.contains(classField.getName()) == false)
				throw new InvalidClassException("Unexpected class field " + classField.getName() + " in the class " + startingClass.getName());
		}
	}

	public static String getJarExecutionPath(Class<?> mainClass) {
		String tmp = mainClass.getProtectionDomain().getCodeSource().getLocation().getPath();
		try {
			return URLDecoder.decode(tmp, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot decode jar path ", e);
			return tmp;
		}
	}

	public static boolean hasInterface(Class<?> clazz, Class<?> interfaceClass) {
		for (int i = 0; i < clazz.getInterfaces().length; i++) {
			if (clazz.getInterfaces()[i].equals(interfaceClass))
				return true;
		}
		return false;
	}

	public static Class<?> getClass(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot find class " + className, e);
			return null;
		}
	}

	public static Object getInstance(String className) {
		try {
			return getClass(className).newInstance();
		} catch (InstantiationException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot create instance of " + className, e);
			return null;
		} catch (IllegalAccessException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot create instance of " + className, e);
			return null;
		}
	}

	public static String getStaticFieldValue(Class<?> clazz, String fieldName) {
		try {
			Field field = clazz.getField(fieldName);
			return (field != null ? (String) field.get(null) : null);
		} catch (SecurityException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot get value of static field " + fieldName, e);
			return null;
		} catch (IllegalArgumentException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot get value of static field " + fieldName, e);
			return null;
		} catch (NoSuchFieldException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot get value of static field " + fieldName, e);
			return null;
		} catch (IllegalAccessException e) {
			LibsLogger.error(ReflectionUtility.class, "Cannot get value of static field " + fieldName, e);
			return null;
		}
	}

	public static void copyProperties(Object destination, Object source) throws IOException {
		try {
			BeanUtils.copyProperties(destination, source);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new IOException("Cannot copy properties between two objects");
		}
	}
}
