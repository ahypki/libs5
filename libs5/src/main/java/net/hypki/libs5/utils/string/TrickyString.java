package net.hypki.libs5.utils.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Don't look at this class if you don't have to. 
 * 
 * @author ahypki
 *
 */
public class TrickyString {
	public static final String REGEX_VARIABLE = "^[\\s]*[\\w]+[\\w\\d_]*[\\s]*(\\(([\\s]*[\\d\\w\\*\\+\\-/]+[,]*[\\s]*)+\\))*";
//	public static final String REGEX_VARIABLE = "^[\\s]*[\\w]+[\\w\\d_]*[\\s]*[\\(]*";
	public static final String REGEX_VARIABLE_NAME = "^[\\s]*[a-zA-Z]+[\\w\\d_]*";
	public static final String REGEX_FUNCTION_NAME = "^[\\s]*[a-zA-Z]+[\\w\\d]*";
	public static final String REGEX_NUMBER = 
			"^[\\s]*[\\+\\-]*[\\d]*\\.[\\d]*[eEdD]*[\\-\\+]*[\\d]+|" + // real number
			"^[\\s]*[\\+\\-]*[\\d]+"   // integer number
//			"^[\\s]*[\\+\\-]*\\.[\\d]+[dD]*[\\-\\+]*[\\d]*|" +
//			"^[\\s]*[\\+\\-]*[\\d]+\\.[dD][\\-\\+]*[\\d]+|" +
////			"^[\\s]*[\\+\\-]*[\\d]+\\.[\\d]*|" +
//			"^[\\s]*[\\+\\-]*[\\d]+[dD][\\d]+|" +
//			"^[\\s]*[\\+\\-]*[\\d]+"
			;
//	public static final String REGEX_REAL_NUMBER_WITH_D = 
//		"^[\\s]*[\\+\\-]*[\\d]*[\\.]*[\\d]*[dD][\\-\\+]*[\\d]+|";
	public static final String REGEX_ALPHABETICAL_WORD = "^[\\s]*[a-zA-ZążźśęćńłóĄŻŹŚĘĆŃŁÓ]+";
	public static final String REGEX_ALPHANUMERIC_WORD = "^[\\s]*[a-zA-Z0-9ążźśęćńłóĄŻŹŚĘĆŃŁÓ]+";
	public static final String REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE = "^[\\s]*[a-zA-Z0-9_ążźśęćńłóĄŻŹŚĘĆŃŁÓ]+";
	public static final String REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS = "[a-zA-Z0-9\\_\\-ążźśęćńłóĄŻŹŚĘĆŃŁÓ]+";
	public static final String REGEX_ALPHANUMERIC_WORD_WITH_MINUS = "[a-zA-Z0-9\\-ążźśęćńłóĄŻŹŚĘĆŃŁÓ]+";
	public static final String REGEX_MATH_OPERATOR = "^[\\s]*[\\*\\+\\-/]+";
	public static final String REGEX_FORTRAN_TYPES = 
		"logical|" +
		"double[\\s]+precision|" +
		"integer|" +
		"character[\\s]*\\*[\\(]*[\\d]+[\\)]*|" +
		"character[\\s]*\\*\\([\\w]+\\)|" +
		"real\\*8|" +
		"real\\*4|" +
		"real[^\\(]";
	public static final String REGEX_COMPARISON_OPERATORS = "(eq|lt|le|gt|ge|ne|eqv)";
	public static final String REGEX_FILENAME = "[\\w\\d\\_\\-\\.\\=/]+";
	
	private StringBuilder text = null;
	private String currentLine = null;
	
	public TrickyString(String text) {
		this.setText(new StringBuilder(text));
	}
	
	public TrickyString(StringBuilder text) {
		this.setText(text);
	}
	
	public TrickyString(String[] args) {
		StringBuilder builder = new StringBuilder();
		for (String arg : args) {
			builder.append(arg);
			builder.append(" ");
		}
		this.setText(builder);
	}

	public boolean startsWith(String prefix) {
		String line = checkLine();
		return (line != null && line.trim().toLowerCase().startsWith(prefix));
	}
	
	public String checkLine() {
		if (currentLine != null)
			return currentLine;
		
		int i = 0, k = 0;
		while (true) {
			k = getText().indexOf("\n", i);
			
			if (k == -1) {
				// there is no '\n' characters - return whole text
				currentLine = getText().toString();
				return currentLine;
			}
			
			if (getText().length() < (k + 6 + 1) || getText().charAt(k + 6) != '&') {
				i = k + 1;
				break;
			} else
				i = k + 1;
		}
		currentLine = getText().substring(0, i).replaceAll("\\&", " ").replaceAll("\\n", " ").toLowerCase();
		return currentLine;
	}
	
	public String checkNextAlphabeticalWord() {
		return checkNextMatch(REGEX_ALPHABETICAL_WORD);
	}
	
	public String checkNextMatch(String match) {
		Pattern regex = Pattern.compile(match);
		Matcher m = regex.matcher(checkLine());
		if (m.find()) {
			return m.group().trim();
		}
		return null;
	}
	
	/**
	 * 
	 * @return Null if there is no next printable character.
	 */
	public String checkNextPrintableCharacter() {
		return checkNextMatch("^[\\s]*[\\S]");
	}
	
	public void expectedAlphanumericWord(String expectedWord) throws ParsingException {
		String temp = getNextAlphanumericWord().toLowerCase();
		if (temp.equals(expectedWord) == false) 
			throw new ParsingException("'" + expectedWord + "' expected");
	}
	
	public void expectedPrintableCharacter(String expectedChar) throws ParsingException {
		String c = this.getNextPrintableCharacter();
		if (c == null || !c.equals(expectedChar))
			throw new ParsingException("'" + expectedChar + "' expected but there is '" + c + "'");
	}
	
//	public void expectedPrintableCharacter(String ... oneOfExpectedChars) throws ParsingException {
//		String c = this.getNextPrintableCharacter();
//		if (c == null)
//			throw new ParsingException("'" + oneOfExpectedChars + "' expected but there is '" + c + "'");
//		else {
//			for (String oneExpectedChar : oneOfExpectedChars) {
//				if (c.equals(oneExpectedChar))
//					return;
//			}
//			throw new ParsingException("'" + oneOfExpectedChars + "' expected but there is '" + c + "'");
//		}
//	}
	
	public String moveToNextLine(boolean moveUntilNonEmptyLineAppears) throws ParsingException {
		String line = checkLine();
		
		while (line != null && line.matches("[\\s]*") && length() > 0) {
			moveToNextLine();
			line = checkLine();
		}
		
		return line;
	}
	
	public String moveToNextLine() throws ParsingException {
		String line = getText().substring(0, getText().indexOf("\n") + 1);
		if (!line.matches("[\\s]*"))
			throw new ParsingException("Cannot move to the next line because there are still printable " +
					"characters in the line");
		getText().delete(0, line.length());
		currentLine = null;
		return line;
	}
	
	/**
	 * Returns String which represents next word consisted of a-zA-Z characters! Method stops reading ff some other character than a-zA-Z will be 
	 * found.
	 *   
	 * @return
	 */
	public String getNextAlphabeticalWord() {
		return getNextMatch(REGEX_ALPHABETICAL_WORD);
	}

	public String getNextMatch(String regexMatch) {
		Pattern regex = Pattern.compile(regexMatch);
		Matcher m = regex.matcher(checkLine());
		if (m.find()) {
			String temp = m.group();
			getText().delete(0, getText().indexOf(temp) + temp.length());
			currentLine = currentLine.substring(m.end());
		    return temp.trim();
			
//			String temp = getText().substring(0, m.end());
//			getText().delete(0, m.end());
//			currentLine = currentLine.substring(temp.length());
//		    return temp.trim();
		}
		return null;
	}
	
	public String getNextNumber() {
		return getNextMatch(REGEX_NUMBER);
	}

	public String getNextPrintableCharacter() {
		return getNextMatch("^[\\s]*[\\S]");
	}
	
	public String getNextCharacter() {
		getText().deleteCharAt(getText().length() - 1);
		String c = currentLine.substring(0, 1);
		currentLine = currentLine.substring(1);
	    return c;
	}
	
	public StringBuilder getText() {
		return text;
	}

	public int length() {
		return getText().toString().trim().length();
	}

	private void setText(StringBuilder text) {
		this.text = text;
		this.currentLine = null;
	}
	
	@Override
	public String toString() {
		return getText().toString();
	}

	public String getLine() {
		String line = checkLine();
		if (line == null)
			return null;
		else {
			getText().delete(0, line.length());
			currentLine = null;
			return line;
		}
	}

	/**
	 * Returns null, if the 'untilStringRegex' is not found
	 * 
	 * @param untilStringRegex
	 * @return
	 */
	public String getNextStringUntil(String untilStringRegex) {
//		return getNextMatch(untilStringRegex);
		
		Pattern regex = Pattern.compile(untilStringRegex);
		Matcher m = regex.matcher(checkLine());
		if (m.find()) {
//			String temp = m.group();
			String ret = getText().substring(0, m.start());
			getText().delete(0, m.start());
			currentLine = currentLine.substring(m.start());
		    return ret;
		}
		return null;
	}
	
//	public String checkNextStringUntil(String untilStringRegex) {
//		return checkNextMatch(untilStringRegex);
//	}

	public String getNextStringUntil(String untilString, boolean withUntilString) {
		int index = checkLine().indexOf(untilString);
		
		if (index == -1)
			return null;
		
		int stopIndex = index + (withUntilString == true ? untilString.length() : 0);
		
		String result = checkLine().substring(0, stopIndex);
		getText().delete(0, stopIndex);
		
//		currentLine = null;
		currentLine = currentLine.substring(stopIndex);
		
		return result;
		
//		String temp = getNextMatch(String.format("^[^%s]*%s", until, until));
//		if (withUntilString == true)
//			return temp;
//		else {
//			return temp.substring(0, temp.length() - until.length());
//		}
	}

	public String getNextFunctionName() {
		return getNextMatch(REGEX_FUNCTION_NAME).trim();
	}
	
	public String checkNextAlphanumericWord() {
		return checkNextMatch("^[\\s]*" + REGEX_ALPHANUMERIC_WORD);
	}

	public String getNextAlphanumericWord() {
		return getNextMatch("^[\\s]*" + REGEX_ALPHANUMERIC_WORD);
	}

	public String getNextVariableName() {
		String temp = getNextMatch(REGEX_VARIABLE_NAME);
		return (temp == null ? null : temp.toLowerCase());
	}

	/**
	 * Reads statements like: 'some string'
	 * @return
	 * @throws ParsingException 
	 */
	public String getNextMessage() throws ParsingException {
		Pattern regex = Pattern.compile("^[\\s]*'[^\\']+'");
		Matcher m = regex.matcher(checkLine());
		if (m.find()) {
			currentLine = null;
			
			String temp = m.group().trim();
			getText().delete(m.start(), m.end());
			temp = temp.substring(1, temp.length() - 1);
			
			if (temp.endsWith("\\'")) {
				throw new ParsingException("Case with 'one_two\'three' not implemented");
			}
			
		    return temp;
		}
		return null;
	}

	public boolean startsWithRegex(String regex, boolean toLowerCase) {
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(toLowerCase ? checkLine().toLowerCase() : checkLine());
		return m.find();
	}

	public String checkNextVariableName() {
		String temp = checkNextMatch(REGEX_VARIABLE_NAME);
		return (temp == null ? null : temp.toLowerCase());
	}

	public String checkNextNumber() {
		return checkNextMatch(REGEX_NUMBER);
	}

	public String checkNextPrintableCharacter(int startIndex) {
		String linePart = checkLine().substring(startIndex);
		Pattern regex = Pattern.compile("^[\\s]*[\\S]");
		Matcher m = regex.matcher(linePart);
		if (m.find()) {
			return m.group().trim();
		}
		return null;
	}

	public String getNextAlphanumericWordWithUnderscore() {
		return getNextMatch("^[\\s]*" + REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE);
	}
	
	public String getNextAlphanumericWordWithMinus() {
		return getNextMatch("^[\\s]*" + REGEX_ALPHANUMERIC_WORD_WITH_MINUS);
	}

	public boolean isEmpty() {
		return text == null || text.toString().trim().length() == 0;
	}

	public void trim() {
		currentLine = currentLine.trim();
	}

//	public String substring(int start, int end) {
//		String temp = getText().substring(start, end);
//		getText().delete(0, end);
//		currentLine = currentLine.substring(end + 1);
//	    return temp.trim();
//	}
}
