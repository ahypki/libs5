package net.hypki.libs5.utils.date;

import java.lang.reflect.Type;

import jodd.datetime.JDateTime;

import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JDateTimeInstanceCreator implements InstanceCreator<JDateTime>, JsonDeserializer<JDateTime>, JsonSerializer<JDateTime> {

	@Override
	public JDateTime createInstance(Type type) {
		return new JDateTime();
	}
	
	@Override
	public JsonElement serialize(JDateTime src, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(src.getTimeInMillis());
	}
	
	@Override
	public JDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		return new JDateTime(json.getAsLong());
	}
}
