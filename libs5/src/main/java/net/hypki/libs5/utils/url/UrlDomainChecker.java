package net.hypki.libs5.utils.url;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.BigFile;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class UrlDomainChecker {
	
	private static HashMap<String, Boolean> domainExist = new HashMap<String, Boolean>();

	public static void main(String[] args) throws Exception {
//		checkDomain("http://www.onet.pl");
//		checkDomain("http://www.orecl.com");
//		save();
		
		read();
		
		int counter = 0;
		while (true) {
			String domain = "http://www." + RandomUtils.nextPronounceableWord(6) + ".com";
			
			if (domainExist.get(domain) != null)
				continue;
			
			checkDomain(domain);
			Thread.sleep(100);
			
			if ((counter++ % 20) == 0) {
				LibsLogger.debug(UrlDomainChecker.class, "Saving to file...");
				save();
			}
		}
	}
	
	private static void read() throws Exception {
		for (String line : new BigFile("domainChecker")) {
			String parts[] = StringUtilities.split(line, '=');
			domainExist.put(parts[0], parts[1].equals("true"));
		}
	}

	private static void save() {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter("domainChecker"));
			for (Entry<String, Boolean> domain : domainExist.entrySet()) {				
				bw.write(domain.toString() + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	private static void checkDomain(String domain) {
		try {
			boolean exists = UrlUtilities.domainExist(new URL(domain));
			LibsLogger.debug(UrlDomainChecker.class, domain);
			domainExist.put(domain, exists);
		} catch (MalformedURLException e) {
			LibsLogger.debug(UrlDomainChecker.class, domain);
			e.printStackTrace();
		}
	}
}
