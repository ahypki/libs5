package net.hypki.libs5.utils.string;

import static net.hypki.libs5.utils.string.RandomUtils.nextInt;

public class HtmlUtils {

	public String randomColor() {
		return String.format("rgb(%d, %d, %d)", nextInt(255), nextInt(255), nextInt(255));
	}
	
	public String randomColorGray() {
		int c = 10 + nextInt(240);
		return String.format("rgb(%d, %d, %d)", c, c, c);
	}
}
