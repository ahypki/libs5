package net.hypki.libs5.utils.args;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.utils.date.SimpleDate;

public class ArgsUtils {
	
	public static Iterable<String> iterateKeys(String [] args) {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {
					
					private int i = 0;
					private String nextKey = null;
					
					@Override
					public String next() {
						if (nextKey == null)
							hasNext();
						return nextKey;
					}
					
					@Override
					public boolean hasNext() {
						if (args != null) {
							for (; i < args.length; i++) {
								if (args[i].matches("--\\w[\\w\\d\\-]*")) {
									nextKey = args[i].substring(2);
									i++;
									return true;
								}
							}
						}
						return false;
					}
				};
			}
		};
	}

	public static String getString(String[] args, String key) {
		if (args != null)
			for (int i = 0; i < args.length; i++) {
				if (args[i] != null && args[i].equals("--" + key)) {
					
					if (args.length <= (i + 1))
						return null;
					
					String argValue = args[i + 1];
					
					if (argValue == null || argValue.startsWith("--"))
						return null;
					
					for (int j = i + 2; j < args.length; j++) {
						if (args[j] == null)
							break;
						else if (args[j].matches("--\\w[\\w\\d\\-]*"))
							break;
						else
							argValue += " " + args[j];
					}
					return argValue;
				}
			}
		return null;
	}
	
	public static List<String> getStrings(String[] args, String key) {
		List<String> values = new ArrayList<String>();
		
		if (args != null)
			for (int i = 0; i < args.length; i++) {
				if (args[i] != null && args[i].equals("--" + key)) {
					if (i + 1 < args.length) {
						String argValue = args[i + 1];
						String value = argValue;
						if (argValue != null && !argValue.startsWith("--")) {
							for (int j = i + 2; j < args.length; j++) {
								if (args[j].matches("--\\w[\\w\\d\\-]*"))
									// another --key -> break
									break;
								else
									value += " " + args[j];
							}
						}
						values.add(value);
					}
				}
			}
		return values;
	}
	
	public static String getString(String[] args, String key, String defaultValue) {
		String tmp = getString(args, key);
		return tmp != null ? tmp : defaultValue;
	}
	
	public static Double getDouble(String[] args, String key) {
		String tmp = getString(args, key);
		return tmp != null ? Double.parseDouble(tmp) : null;
	}
	
	public static Float getFloat(String[] args, String key) {
		String tmp = getString(args, key);
		return tmp != null ? Float.parseFloat(tmp) : null;
	}
	
	public static double getDouble(String[] args, String key, double defaultValue) {
		String tmp = getString(args, key);
		return tmp != null ? Double.parseDouble(tmp) : defaultValue;
	}
	
	public static Long getLong(String[] args, String key) {
		String tmp = getString(args, key);
		return tmp != null ? Long.parseLong(tmp) : null;
	}
	
	public static long getLong(String[] args, String key, long defaultValue) {
		String tmp = getString(args, key);
		return tmp != null ? Long.parseLong(tmp) : defaultValue;
	}
	
	public static Integer getInt(String[] args, String key) {
		String tmp = getString(args, key);
		return tmp != null ? Integer.parseInt(tmp) : null;
	}
	
	public static int getInt(String[] args, String key, int defaultValue) {
		String tmp = getString(args, key);
		return tmp != null ? Integer.parseInt(tmp) : defaultValue;
	}
	
	public static Boolean getBoolean(String[] args, String key) {
		String tmp = getString(args, key);
		return tmp != null ? Boolean.parseBoolean(tmp) : exists(args, key);
	}
	
	public static SimpleDate getDate(String[] args, String key) {
		String tmp = getString(args, key);
		return tmp != null ? SimpleDate.parse(tmp) : null;
	}
	
	public static boolean getBoolean(String[] args, String key, boolean defaultValue) {
		String tmp = getString(args, key);
		return tmp != null ? Boolean.parseBoolean(tmp) : defaultValue;
	}

	public static boolean exists(String[] args, String key) {
		if (args != null)
			for (int i = 0; i < args.length; i++) {
				if (args[i] != null && args[i].matches("[\\-]*" + key)) {
					return true;
				}
			}
		return false;
	}
}
