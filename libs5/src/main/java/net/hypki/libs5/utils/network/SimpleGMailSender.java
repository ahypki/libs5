package net.hypki.libs5.utils.network;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public final class SimpleGMailSender {
	public static final String HOST = "smtp.gmail.com";
	private final static int PORT = 587; // port for gmail
	
	public static void sendMail(String username, String pass, String recipient, String subject, 
			String messageText, boolean leaveOnServer) throws AddressException, MessagingException {
		
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true"); // added this line
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.user", username);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.port", PORT);
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props, null);
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(username));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
		if (leaveOnServer == true)
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(username));
		message.setSubject(subject);
		message.setText(messageText);
		
		Transport transport = session.getTransport("smtp");
		transport.connect(HOST, username, pass);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();

	}
}

