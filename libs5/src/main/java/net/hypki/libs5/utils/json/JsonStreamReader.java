package net.hypki.libs5.utils.json;

import java.io.IOException;

import com.google.gson.JsonElement;

public interface JsonStreamReader {
	
	public void beginObject() throws IOException;
	public void endObject() throws IOException;
	public void beginArray() throws IOException;
	public void endArray() throws IOException;
	public boolean hasNext() throws IOException;
	public String nextName() throws IOException;
	public String nextString() throws IOException;
	public void close() throws IOException;
	public JsonElement next() throws IOException;
}
