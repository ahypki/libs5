package net.hypki.libs5.utils.utils;

import java.awt.Color;
import java.util.Random;

public class ColorUtils {

	private static Random random = new Random(System.currentTimeMillis());

	public static Color randomPastelColor() {
		final float hue = random.nextFloat();
		// Saturation between 0.1 and 0.3
		final float saturation = (random.nextInt(2000) + 1000) / 10000f;
		final float luminance = 0.9f;
		return Color.getHSBColor(hue, saturation, luminance);
	}

	public static Color randomPastelColor2() {
		return new Color(random.nextInt(128) + 127, random.nextInt(128) + 127, random.nextInt(128) + 127);
	}

	public static Color randomColor() {
		int R = (int) (Math.random() * 256);
		int G = (int) (Math.random() * 256);
		int B = (int) (Math.random() * 256);
		return new Color(R, G, B); // random color, but can be bright or dull
	}

	public static Color randomPastelColor3() {
		// to get rainbow, pastel colors
		Random random = new Random();
		final float hue = random.nextFloat();
		final float saturation = 0.9f; // 1.0 for brilliant, 0.0 for dull
		final float luminance = 1.0f; // 1.0 for brighter, 0.0 for black
		return Color.getHSBColor(hue, saturation, luminance);
	}

	public static String colorToHex(Color color) {
		String rgb = Integer.toHexString(color.getRGB());
		return rgb.substring(2, rgb.length());
	}

	public static Color parseCssRgb(String cssRgb) {
		return Color.decode(cssRgb);
	}

	public static void main(String[] args) {
		System.out.println(colorToHex(randomPastelColor2()));
	}
}
