package net.hypki.libs5.utils.tests;

import java.io.File;
import java.io.IOException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.utils.system.ExecutingLinuxCommand;

import org.junit.Assert;
import org.junit.Ignore;


@Ignore
public class LibsTestCase {
		
	public static String createRandomWord() {
		return new RandomGUID().toString().split("-")[0].toLowerCase();
	}
	
	public static String createRandomPronounceableWord() {
		try {
			return ExecutingLinuxCommand.executeCommand(new String[] {"apg", "-a", "0", "-m", "5", "-x", "6", "-M", "L", "-n", "1"}, new File(".")).replaceAll("\n", "");
		} catch (IOException e) {
			LibsLogger.error("Cannot create random pronounceable word", e);
			return null;
		}
	}

	protected static void takeANap(int ms) {
		try {
			LibsLogger.debug(LibsTestCase.class, "Sleeping for " + ms + " [ms]...");
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			LibsLogger.error(LibsTestCase.class, "Cannot take a nap", e);
		}
	}
	
	public void assertTrue(boolean condition) {
		Assert.assertTrue(condition);
	}
	
	public void assertFalse(boolean condition) {
		Assert.assertTrue(!condition);
	}
	
	public void assertEquals(String left, String right) {
		Assert.assertTrue(left != null ? left.equals(right) : right == null);
	}
	
	public void assertTrue(boolean condition, String message) {
		Assert.assertTrue(message, condition);
	}
	
	public void assertTrue(String message, boolean condition) {
		Assert.assertTrue(message, condition);
	}
	
	public void info(String message) {
		LibsLogger.debug(message);
	}
	
	public void info(Class clazz, String message) {
		LibsLogger.debug(clazz, message);
	}
	
	public void info(Object message) {
		if (message != null)
			LibsLogger.debug(message.toString());
	}
	
	public void error(String message) {
		LibsLogger.error(message);
	}
	
	public void error(String message, Throwable t) {
		LibsLogger.error(message, t);
	}
}
