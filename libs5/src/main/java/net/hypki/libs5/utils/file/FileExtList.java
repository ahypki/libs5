package net.hypki.libs5.utils.file;

import java.util.ArrayList;
import java.util.List;

public class FileExtList {
	
	private List<FileExt> fileExtList = null;

	public FileExtList() {
		
	}
	
	public FileExtList add(FileExt f) {
		if (!getFileExtList().contains(f))
			getFileExtList().add(f);
		return this;
	}

	private List<FileExt> getFileExtList() {
		if (fileExtList == null)
			setFileExtList(new ArrayList<>());
		return fileExtList;
	}

	private void setFileExtList(List<FileExt> fileExtList) {
		this.fileExtList = fileExtList;
	}
}
