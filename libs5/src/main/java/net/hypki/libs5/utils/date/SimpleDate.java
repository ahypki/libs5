package net.hypki.libs5.utils.date;

import java.io.Serializable;
import java.time.YearMonth;

import jodd.datetime.JDateTime;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.NotNull;

import org.apache.log4j.helpers.ISO8601DateFormat;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.google.gson.annotations.Expose;

public class SimpleDate implements Serializable {
	
	private static final DateTimeFormatter parserISO = ISODateTimeFormat.dateTime();
//	private static final DateTimeFormatter parserISO8601 = ISO8601DateFormat. getDadateTime();
	private static final DateTimeFormatter parser = DateTimeFormat.forPattern("yyyy-MM-dd_HH:mm:ss.SSSS");
	private static final DateTimeFormatter parserShort = DateTimeFormat.forPattern("yyyy-MM-dd");
	private static final DateTimeFormatter parserHuman = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
	private static final DateTimeFormatter parserHour = DateTimeFormat.forPattern("HH:mm:ss");
	private static final DateTimeFormatter parserHourShort = DateTimeFormat.forPattern("HH:mm");

	@NotNull
	@Expose
	private JDateTime date = null;
	
	public SimpleDate() {
		setDate(new JDateTime());
	}
	
	public SimpleDate(int year, int month, int day) {
		setDate(new JDateTime(year, month, day));
	}
	
	public SimpleDate(long ms) {
		setDate(new JDateTime(ms));
	}

	private JDateTime getDate() {
		return date;
	}
	
	/**
	 * Sets hour and minutes, @hhmm in the format hh:mm
	 * @param hhmm
	 * @return
	 */
	public SimpleDate setHour(String hhmm) {
		if (hhmm.matches("\\d\\d\\:\\d\\d"))
			return setHour(NumberUtils.toInt(hhmm.substring(0, 2)), NumberUtils.toInt(hhmm.substring(3, 5)));
		return this;
	}
	
	public SimpleDate setHour(int hour, int minutes) {
		getDate().setHour(hour);
		getDate().setMinute(minutes);
		getDate().setSecond(0);
		getDate().setMillisecond(0);
		return this;
	}
	
	public int getHour() {
		return getDate().getHour();
	}
	
	public int getMinute() {
		return getDate().getMinute();
	}
	
	public boolean isHourDefined() {
		return getHour() != 0 || getMinute() != 0;
	}
	
	public SimpleDate clearHour() {
		return setHour(0, 0);
	}

	private void setDate(JDateTime date) {
		this.date = date;
	}

	public long getTimeInMillis() {
		return getDate().getTimeInMillis();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SimpleDate) {
			SimpleDate sd = (SimpleDate) obj;
			return this.getTimeInMillis() == sd.getTimeInMillis();
		}
		return super.equals(obj);
	}
	
	public SimpleDate clone() {
		return new SimpleDate(getTimeInMillis());
	}
	
	@Override
	public String toString() {
		return toStringISO();
//		return parser.print(getDate().getTimeInMillis());
	}
	
	/**
	 * Returns a formatter that combines a full date and time, separated 
	 * by a 'T' (yyyy-MM-dd'T'HH:mm:ss.SSSZZ). The time zone offset is 'Z' for zero, 
	 * and of the form '\u00b1HH:mm' for non-zero. The parser is strict by default, 
	 * thus time string 24:00 cannot be parsed.
	 * @return
	 */
	public String toStringISO() {
		return parserISO.print(getDate().getTimeInMillis());
	}
	
	public String toString(String format) {
		return DateTimeFormat.forPattern(format).print(getDate().getTimeInMillis());
	}
	
	public String toStringShort() {
		return parserShort.print(getDate().getTimeInMillis());
	}
	
	public String toStringHuman() {
		return parserHuman.print(getDate().getTimeInMillis());
	}
	
	public static SimpleDate parse(String date, String format) {
		try {
			if (StringUtilities.nullOrEmpty(date))
				return null;
			DateTimeFormatter parserTmp = DateTimeFormat.forPattern(format);
			return new SimpleDate(parserTmp.parseDateTime(date).getMillis());
		} catch (IllegalArgumentException e) {
			LibsLogger.error("Wrong format to parse date", e);
			return null;
		}
	}

	public static SimpleDate parse(String date) {
		try {
			if (StringUtilities.nullOrEmpty(date))
				return null;
			else if (date.length() == 10)
				return new SimpleDate(parserShort.parseDateTime(date).getMillis());
			else if (date.indexOf('T') == 10)
				return new SimpleDate(parserISO.parseDateTime(date).getMillis());
			else if (date.length() == 13)
				return new SimpleDate(NumberUtils.toLong(date));
			else
				return new SimpleDate(parser.parseDateTime(date).getMillis());
		} catch (IllegalArgumentException e) {
			LibsLogger.error("Wrong format to parse date", e);
			return null;
		}
	}

	public int getYear() {
		return getDate().getYear();
	}
	
	public int getMonth() {
		return getDate().getMonth();
	}
	
	public int getDay() {
		return getDate().getDay();
	}
	
	public SimpleDate subtractMonths(int months) {
		getDate().subMonth(months);
		return this;
	}
	
	public SimpleDate subtractYears(int years) {
		getDate().subYear(years);
		return this;
	}
	
	public SimpleDate subtractDays(int days) {
		getDate().subDay(days);
		return this;
	}
	
	public SimpleDate addMonths(int months) {
		getDate().addMonth(months);
		return this;
	}
	
	public SimpleDate addYears(int years) {
		getDate().addYear(years);
		return this;
	}
	
	public SimpleDate addDays(int days) {
		getDate().addDay(days);
		return this;
	}
	
	public SimpleDate addMillisecond(int ms) {
		getDate().addMillisecond(ms);
		return this;
	}
	
	public SimpleDate addWeeks(int weeks) {
		getDate().addDay(7 * weeks);
		return this;
	}
	
	public SimpleDate addHours(int hours) {
		getDate().addHour(hours);
		return this;
	}
	
	public SimpleDate addMinutes(int minutes) {
		getDate().addMinute(minutes);
		return this;
	}

	public static SimpleDate now() {
		return new SimpleDate();
	}

	public String toStringHour() {
		return parserHour.print(getDate().getTimeInMillis());
	}
	
	public String toStringHourShort() {
		return parserHourShort.print(getDate().getTimeInMillis());
	}

	public boolean isBefore(SimpleDate date) {
		return getTimeInMillis() < date.getTimeInMillis();
	}
	
	public boolean isLE(SimpleDate date) {
		return getTimeInMillis() <= date.getTimeInMillis();
	}
	
	public boolean isL(SimpleDate date) {
		return getTimeInMillis() < date.getTimeInMillis();
	}
	
	public boolean isG(SimpleDate date) {
		return getTimeInMillis() > date.getTimeInMillis();
	}
	
	public boolean isGE(SimpleDate date) {
		return getTimeInMillis() >= date.getTimeInMillis();
	}
	
	public int getDayOfWeek() {
		return getDate().getDayOfWeek();
	}
	
	public String getDayOfWeekName(String lang) {
		return DateUtils.getDayOfWeekName(getDate().getDayOfWeek(), lang);
	}
	
	public int getDayOfMonth() {
		return getDate().getDayOfMonth();
	}
	
	public int getNrOfDaysInMonth() {
		YearMonth yearMonthObject = YearMonth.of(getYear(), getMonth());
		return yearMonthObject.lengthOfMonth();
	}
	
	public int getNrOfPartialWeeks() {
		int nrWeeks = 0;
		int days = getNrOfDaysInMonth();
		int monthStartDayOfWeek = new SimpleDate(getYear(), getMonth(), 1).getDayOfWeek();
		if (monthStartDayOfWeek > 1) {
			days -=  (7 - monthStartDayOfWeek);
			nrWeeks++;
		}
		nrWeeks += days / 7;
		days -= days % 7;
		if (days > 0)
			nrWeeks++;
		return nrWeeks;
	}

	public boolean isTheSameDay(SimpleDate date) {
		if (getYear() != date.getYear())
			return false;
		if (getMonth() != date.getMonth())
			return false;
		if (getDay() != date.getDay())
			return false;
		return true;
	}
	
	public boolean isDayBefore(SimpleDate date) {
		return clone().clearHour().isBefore(date.clone().clearHour());
	}
	
	public int getDiffDays(SimpleDate to) {
		return date.getDayOfYear() - to.date.getDayOfYear();
	}

	public String getMonthHuman(String langEn) {
		if (langEn == null || langEn.equals(DateUtils.LANG_EN)) {
			if (getMonth() == 1)
				return "January";
			else if (getMonth() == 2)
				return "February";
			else if (getMonth() == 3)
				return "march";
			else if (getMonth() == 4)
				return "April";
			else if (getMonth() == 5)
				return "May";
			else if (getMonth() == 6)
				return "June";
			else if (getMonth() == 7)
				return "July";
			else if (getMonth() == 8)
				return "August";
			else if (getMonth() == 9)
				return "September";
			else if (getMonth() == 10)
				return "October";
			else if (getMonth() == 11)
				return "November";
			else
				return "December";
		}
		return null;
	}
}
