package net.hypki.libs5.utils.api;

import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.annotations.Expose;

public class APIArg {

	@Expose
	private String name = null;
	
	@Expose
	private APIArgType type = null;
	
	public APIArg() {
		
	}
	
	public APIArg(String name, APIArgType type) {
		setName(name);
		setType(type);
	}
	
	@Override
	public String toString() {
		return JsonUtils.objectToStringPretty(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public APIArgType getType() {
		return type;
	}

	public void setType(APIArgType type) {
		this.type = type;
	}
}
