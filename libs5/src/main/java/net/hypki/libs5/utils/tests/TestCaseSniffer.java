package net.hypki.libs5.utils.tests;

import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.LibsConst;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.reflection.ReflectionUtility;

import org.junit.Ignore;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


@Ignore
public class TestCaseSniffer {
	public static void testClasses(Class<? extends LibsTestCase> startingClass, Class<? extends LibsTestCase> superClass, 
			boolean shutdownAfterFirstFail) {
		try {
			// 1.
			List<Class<?>> classes = ReflectionUtility.getClasses(startingClass, true);
			ArrayList<String> failedClasses = new ArrayList<String>();
//	
//		ArrayList<Class> unitTestsClasses = new ArrayList<Class>(); 
//		
//		for (Class clazz : classes) {
//			if (ReflectionUtility.isSubclassOf(clazz, TestCaseSniffer.class)) {
//				if (!clazz.equals(TestCaseSniffer.class))
//					unitTestsClasses.add(clazz);
//			}
//		}
//		
//		Class [] classesArray = new Class[unitTestsClasses.size()];
//		for (int i = 0; i < unitTestsClasses.size(); i++) {
//			classesArray[i] = unitTestsClasses.get(i);
//		}
//		
//		Result result = JUnitCore.runClasses(classesArray);
//		if (!result.wasSuccessful()) {
//			for (Failure fail : result.getFailures()) {
//				System.out.println("######################################################################");
//				System.out.println("################################ FAILED ##############################");
//				System.out.println("######################################################################");
//				System.out.println(fail.toString());
//				System.out.println(fail.getTrace());
//				System.exit(WebLibsConst.ERROR_UNIT_TEST_FAILED);
//			}
//		}
		
			// 2.
			for (Class<?> clazz : classes) {
				if (ReflectionUtility.isSubclassOf(clazz, superClass) && !clazz.equals(superClass)) {
					System.out.println("############## " + clazz.getSimpleName() + " ######################");
					Result result = JUnitCore.runClasses(clazz);
					if (result.getFailures().size() > 0) {
						for (Failure fail : result.getFailures()) {
							System.out.println(fail.toString());
							System.err.println("ERROR in class: " + clazz.getSimpleName());
			                System.out.println(fail.getTrace());
			                if (shutdownAfterFirstFail) {
			                	LibsInit.shutdown();
			                	System.exit(LibsConst.ERROR_UNIT_TEST_FAILED);
			                } else
			                	failedClasses.add("Class " + clazz.getSimpleName() +  " failed, description: "+ fail.getDescription());
						}
					} else
						System.out.println("Done.\n");
				}
			}
			
			if (failedClasses.size() == 0) {
				System.out.println("__________ UNIT TESTS COMPLETED SUCCESSFULLY __________");
				System.out.println(String.format("Total %d classes were egzamined", classes.size()));
			} else {
				System.out.println("__________ UNIT TESTS COMPLETED __________");
				System.out.println(String.format("Total %d classes were egzamined", classes.size()));
				System.out.println(String.format("Total %d classes failed!", failedClasses.size()));
				for (String s : failedClasses) {
					System.out.println(s);
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			LibsInit.shutdown();			
		}
				
		// 3.
//		junit.textui.TestRunner.run(new UserExists_UnitTest());
//		new junit.textui.ResultPrinter(null).startTest(new DbPropertyCache_UnitTest());
	}

}
