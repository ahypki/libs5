package net.hypki.libs5.utils.system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;

public class Basher2 {

	private List<String> args = null;
	
	private Map<String, String> env = null;
	
	private FileExt workingDir = new FileExt();
	
	public Basher2() {
		
	}
	
	public void run() throws IOException {
		run(null);
	}
	
	public void run(List<String> output) throws IOException {
//		exec(ArrayUtils.toArray(getArgs()));
		
		ProcessBuilder pb = new ProcessBuilder(getArgs());
		pb.directory(getWorkingDir());
		pb.redirectErrorStream(true);
		
		for (String envKey : getEnv().keySet()) {
			pb.environment().put(envKey, getEnv().get(envKey));
		}
		
		Process p = pb.start();
		
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		try {
			String line = null;
			while((line = input.readLine()) != null){
//				System.out.println(line);
				
				if (output != null)
					output.add(line);
			}
		} catch (Exception e) {
			LibsLogger.error(Basher2.class, "Cannot write to log", e);
		}
	}
	
	public Iterable<String> runIter() throws IOException {
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {
					
					private String line = null;
					private BufferedReader input = null;
					
					@Override
					public String next() {
						if (input == null)
							hasNext();
						
						return line;
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (input == null) {
								ProcessBuilder pb = new ProcessBuilder(getArgs());
								pb.directory(getWorkingDir());
								pb.redirectErrorStream(true);
								
								for (String envKey : getEnv().keySet()) {
									pb.environment().put(envKey, getEnv().get(envKey));
								}
								
								Process p = pb.start();
								
								input = new BufferedReader(new InputStreamReader(p.getInputStream()));
							}
						
							line = input.readLine();
							
							if (line != null)
								return true;
						} catch (Exception e) {
							LibsLogger.error(Basher2.class, "Cannot get next output line", e);
						}
						
						return false;
					}
				};
			}
		};
		
//		exec(ArrayUtils.toArray(getArgs()));
		
		
		
		
		
		
	}

	private List<String> getArgs() {
		if (args == null)
			args = new ArrayList<String>();
		return args;
	}

	private Basher2 setArgs(List<String> args) {
		this.args = args;
		return this;
	}
	
	public Basher2 add(String arg) {
		getArgs().add(arg);
		return this;
	}
	
	public Basher2 add(String ... args) {
		for (String s : args)
			getArgs().add(s);
		return this;
	}
	
	public Basher2 add(String param, String value) {
		getArgs().add(param);
		getArgs().add(value);
		return this;
	}

	public FileExt getWorkingDir() {
		return workingDir;
	}

	public Basher2 setWorkingDir(FileExt workingDir) {
		this.workingDir = workingDir;
		return this;
	}

	public Map<String, String> getEnv() {
		if (env == null)
			env = new HashMap<String, String>();
		return env;
	}

	public Basher2 setEnv(Map<String, String> env) {
		this.env = env;
		return this;
	}
	
	public Basher2 addEnv(String key, String value) {
		getEnv().put(key, value);
		return this;
	}
}
