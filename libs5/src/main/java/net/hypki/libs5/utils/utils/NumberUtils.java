package net.hypki.libs5.utils.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RegexUtils;



public class NumberUtils {
	
	public static void main(String[] args) {
		LibsLogger.debug(NumberUtils.class, toStringNoExpForm(1.123));
		
		LibsLogger.debug(NumberUtils.class, NumberUtils.toInt("1.1", 3));
		LibsLogger.debug(NumberUtils.class, NumberUtils.toInt("7.1", 3));
		LibsLogger.debug(NumberUtils.class, NumberUtils.toInt("1", 3));
		LibsLogger.debug(NumberUtils.class, NumberUtils.toInt("7", 3));
	}
	
	public static boolean isExpForm(String number) {
		return !RegexUtils.isDoubleNonExpForm(number);
	}
	
	public static String toStringNoExpForm(double d) {
		DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		df.setMaximumFractionDigits(340); // 340 = DecimalFormat.DOUBLE_FRACTION_DIGITS
		df.setMinimumFractionDigits(1);
		return df.format((double) d);
	}
	
	public static long toLong(String input, long defaultValue) {
		try {
			return Long.parseLong(input);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public static long toLong(String input) {
		try {
			return Long.parseLong(input);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	public static float toFloat(String input) {
		try {
			return Float.parseFloat(input);
		} catch (NumberFormatException e) {
			return 0.0f;
		}
	}

	public static float toFloat(String input, float defaultValue) {
		try {
			return Float.parseFloat(input);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public static double toDouble(String input) {
		try {
			if (input.indexOf('d') >= 0) // TODO DEBUG
				return Double.parseDouble(input.replace('d', 'e'));
			
			return Double.parseDouble(input);
		} catch (NumberFormatException e) {
			return 0.0;
		}
	}
	
	public static double toDouble(String input, double defValue) {
		try {
			if (input.indexOf('d') >= 0) // TODO DEBUG
				return Double.parseDouble(input.replace('d', 'e'));
			
			return Double.parseDouble(input);
		} catch (NumberFormatException e) {
			return defValue;
		}
	}
	
//	public static int toInt(String s, int start, int stop, int defaultValue) {
////		return toInt(s.substring(start, stop));
//		int numL = 0;
//		char c = 0;
//		for (int i = stop - 1, times = 1; i >= start; i--, times *= 10) {
//			c = s.charAt(i);
//			if (c == '-')
//				return -numL;
//			numL += (c - '0') * times;
//		}
//		return numL;
//	}
//	
//	public static long toLong(String s, int start, int stop, long defaultValue) {
////		return toLong(s.substring(start, stop));
//		long numL = 0L;
//		char c = 0;
//		for (int i = stop - 1, times = 1; i >= start; i--, times *= 10) {
//			c = s.charAt(i);
//			if (c == '-')
//				return  - numL;
//			numL += (s.charAt(i) - '0') * times;
//		}
//		return numL;
//	}
//	
//	public static double toDouble(String s, int start, int stop, double defaultValue) {
////		return toDouble(s.substring(start, stop));
//		final int dot = s.indexOf('.', start);
//		if (dot >= 0) {
//			long numL = 0L;
//			double numR = 0L;
//			long mantis = 0L;
//			boolean mantisNeg = false;
//			boolean expFormat = false;
//			int expStart = -1;
//			boolean neg = false;
//			char c = 0;
//			
//			// left to 'dot'
//			for (int i = dot - 1, times = 1; i >= start; i--, times *= 10) {
//				c = s.charAt(i);
//				if (c == '-') {
//					neg = true;
//					break;
//				}
//				numL += (s.charAt(i) - '0') * times;
//			}
//			
//			// right to 'dot'
//			double timesD = 10.0;
//			for (int i = dot + 1; i <= stop; i++, timesD *= 10.0) {
//				c = s.charAt(i);
//				if (c == 'E' || c == 'e') {
//					expFormat = true;
//					expStart = i;
//					break;
//				}
//				numR += (c - '0') / timesD;
//			}
//			
//			// mantis
//			if (expFormat) {
//				for (int i = stop - 1, times = 1; i > expStart; i--, times *= 10) {
//					c = s.charAt(i);
//					if (c == '-')
//						mantisNeg = true;
//					else if (c == '+')
//						mantisNeg = false;
//					else
//						mantis += (c - '0') * times;
//				}
//			}
//			
//			double tmp = 0.0;
//			if (mantisNeg)
//				tmp = (neg ? -1.0 : 1.0) * (numL + numR) / Math.pow(10.0, mantis);
//			else
//				tmp = (neg ? -1.0 : 1.0) * (numL + numR) * Math.pow(10.0, mantis);
//			return tmp;
//		} else
//			throw new NotImplementedException();
//	}
	
	public static int toInt(Object input) {
		try {
			if (input != null)
				return Integer.parseInt(input.toString());
			else
				return 0;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public static int toInt(String input) {
		try {
			return Integer.parseInt(input);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	public static int toInt(String input, int defaultValue) {
		try {
			if (input == null)
				return defaultValue;
			
			return (int) Double.parseDouble(input); //Integer.parseInt(input);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static int[] sort(int[] a) {
		boolean changed = true;
		while (changed == true) {
			changed = false;
			for (int i = 0; i < (a.length - 1); i++) {
				if (a[i] > a[i+1]) {
					int tmp = a[i+1];
					a[i+1] = a[i];
					a[i] = tmp;
					changed = true;
				}
			}
		}
		return a;
	}


	public static Object autoParse(Object o) {
		if (o instanceof Double
				|| o instanceof Float
				|| o instanceof Long
				|| o instanceof Integer)
			return o;
		else if (o instanceof String) {
			String s = (String) o;

			if (RegexUtils.isInt(s))
				return toInt(s);
			else if (RegexUtils.isDouble(s))
				return toDouble(s);
			else
				return s;
		} else
			return o;
	}

	public static boolean isNumber(String s) {
		if (RegexUtils.isInt(s))
			return true;
		else if (RegexUtils.isDouble(s))
			return true;
		else if (RegexUtils.isDoubleNonExpForm(s))
			return true;
		return false;
	}
	
	public static boolean isDouble(String s) {
		if (RegexUtils.isDouble(s))
			return true;
		else if (RegexUtils.isDoubleNonExpForm(s))
			return true;
		return false;
	}
	
	public static boolean isInt(String s) {
		return RegexUtils.isInt(s);
	}
}
