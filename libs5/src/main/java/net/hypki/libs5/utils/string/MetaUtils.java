package net.hypki.libs5.utils.string;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.util.ArrayList;
import java.util.List;

public class MetaUtils {

	public static String applyMeta(final String s, final MetaList meta) {
		if (meta == null || meta.size() == 0 || nullOrEmpty(s))
			return s;
		
		List<String> groups = RegexUtils.allGroups("\\$([\\w\\d]+)", s);
		String newS = s;
		for (String group : groups) {
			Meta m = meta.get(group);
			if (m != null) {
				String newVal = m.getAsString();
				if (newVal != null)
					newS = newS.replaceAll("\\$" + group, newVal);
			}
		}
		return newS;
	}
}
