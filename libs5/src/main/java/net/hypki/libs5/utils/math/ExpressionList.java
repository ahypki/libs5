package net.hypki.libs5.utils.math;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.javatuples.Pair;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.string.TrickyString;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ExpressionList implements Serializable, Iterable<Expression> {
	
	@Expose
	@NotNull
	@AssertValid
	private List<Expression> expressions = null;

	public ExpressionList() {
		
	}
	
	public ExpressionList(String toParse) {
		TrickyString ts = new TrickyString(toParse);
		
		ts.expectedPrintableCharacter("(");
		
		// multiple join fields
		while (ts.checkNextPrintableCharacter() != null
				&& !ts.checkNextPrintableCharacter().equals(")")) {
			String expr = ts.getNextStringUntil(",");
			if (expr != null) {
				getExpressions().add(new Expression(expr));
				ts.expectedPrintableCharacter(",");
			} else {
				expr = ts.getNextStringUntil(")", false);
				
				if (expr == null)
					break; // nothing more to parse
				
				getExpressions().add(new Expression(expr));
				ts.expectedPrintableCharacter(")");
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Expression ex : getExpressions()) {
			sb.append(ex.toString());
			sb.append(", ");
		}
		return sb.toString();
	}
	
	@Override
	public Iterator<Expression> iterator() {
		return getExpressions().iterator();
	}

	public List<Expression> getExpressions() {
		if (expressions == null)
			expressions = new ArrayList<Expression>();
		return expressions;
	}

	public void setExpressions(List<Expression> expressions) {
		this.expressions = expressions;
	}

	public void add(Expression expression) {
		getExpressions().add(expression);
	}
}
