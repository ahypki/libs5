package net.hypki.libs5.utils.file;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.util.Iterator;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

import jodd.io.FileEx;
import jodd.io.FileUtil;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.sha.SHAManager;
import net.hypki.libs5.utils.string.StringUtilities;

public class FileExt extends FileEx implements Iterable<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5222569332780540742L;
	
	public FileExt() {
		super(".");
	}

	public FileExt(String pathname) {
		super(untangle(pathname));
	}

	public FileExt(URI uri) {
		super(uri);
	}

	public FileExt(File file) {
		super(untangle(file.getPath()));
	}
	
	public FileExt(String ... pathParts) {
		super(FileUtils.concatPaths(pathParts));
	}

	public FileExt(File parent, String child) {
		super(parent, child);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FileExt) {
			FileExt fExt = (FileExt) obj;
			return this.getAbsolutePath().equals(fExt.getAbsolutePath());			
		}
		return this.getAbsolutePath().equals(obj.toString());
	}
	
	public SimpleDate getLastEditDate() {
		return new SimpleDate(FileUtils.getLastEditDate(getAbsolutePath()));
	}
	
	public SimpleDate getCreateDate() {
		return new SimpleDate(FileUtils.getCreateDate(getAbsolutePath()));
	}
	
	public long getSizeBytes() {
		if (isFile())
			return length();
		else {
			long size = 0;
			for (FileExt f : new LazyFileIterator(this, true, true, false))
				size += f.length();
			return size;
		}
	}
	
	public InputStream openInputStream() throws IOException {
		return FileUtils.getInputStream(this);
	}
	
	public void remove(boolean quiet) throws IOException {
		if (quiet) {			
			try {
				super.remove();
			} catch (FileNotFoundException e) {
				LibsLogger.debug(FileExt.class, "File ", getAbsolutePath(), " does not exist, cannot remove, be quiet");
			}
		} else
			super.remove();
	}
	
	public boolean deleteIfExists() throws IOException {
		if (exists()) {
			if (isDirectory()) {
				deleteDir();
				return true;
			} else if (isFile()) {
				return delete();
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getAbsolutePath();
	}

	public String getRelativePathTo(String basePath) {
		String base = basePath.endsWith("/") ? basePath : basePath + "/";
//		System.out.println(getAbsolutePath() + "  " + base);
		if (getAbsolutePath().startsWith(base)) {
			return getAbsolutePath().substring(base.length());
		} else 
			return null;
	}
		
	public String getFilenameOnly() {
		return getName();
	}
	
	public String getFilenameOnlyNoExtension() {
		String tmp = getName();
		return tmp != null && tmp.indexOf('.') >= 0 ? tmp.substring(0, tmp.indexOf('.')) : tmp;
	}
	
	public String getDirectoryOnly() {
		if (isDirectory()) {
			return getAbsolutePath();
		} else {
			if (getParentFile() != null)
				return getParentFile().getAbsolutePath();
			else
				return ".";
		}
	}
	
	public String getExtensionOnly() {
//		if (isFile() == true) {
			int i = getName().lastIndexOf('.');
	        if (i > 0 &&  i < getName().length() - 1) 
	          return getName().substring(i + 1).toLowerCase();
//		}
		return null;
	}
	
	public long getNumOfLines(boolean hasToContain, String hasToContainStr) {
		try {
			BufferedReader reader = null;
			if (getFilenameOnly().endsWith("gz")) {
				FileInputStream fin = new FileInputStream(getAbsolutePath());
			    GZIPInputStream gzis = new GZIPInputStream(fin);
			    InputStreamReader xover = new InputStreamReader(gzis);
//				reader = new BufferedReader(xover); // INFO buffer size makes no differance, .., 8192 * 128, , 8192 * 32
			    reader = new BufferedReader(xover, 256000);
			} else {
				reader = new BufferedReader(new FileReader(getAbsolutePath()));
			}
			int lines = 0;
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (hasToContain) {
					if (line.contains(hasToContainStr))
						lines++;
				} else {
					if (!line.contains(hasToContainStr))
						lines++;
				}
			}
			reader.close();
			return lines;
		} catch (IOException e) {
			LibsLogger.error(FileExt.class, "Cannot read number of lines for file " + getAbsolutePath(), e);
			return 0;
		}
	}
	
	public long getNumOfLines() {
		try {
			BufferedReader reader = null;
			if (getFilenameOnly().endsWith("gz")) {
				FileInputStream fin = new FileInputStream(getAbsolutePath());
			    GZIPInputStream gzis = new GZIPInputStream(fin);
			    InputStreamReader xover = new InputStreamReader(gzis);
//				reader = new BufferedReader(xover); // INFO buffer size makes no differance, .., 8192 * 128, , 8192 * 32
			    reader = new BufferedReader(xover, 256000);
			} else {
				reader = new BufferedReader(new FileReader(getAbsolutePath()));
			}
			int lines = 0;
			while (reader.readLine() != null) lines++;
			reader.close();
			return lines;
		} catch (IOException e) {
			LibsLogger.error(FileExt.class, "Cannot read number of lines for file " + getAbsolutePath(), e);
			return 0;
		}
	}
	
	public String[] readLines() throws IOException {
		return FileUtil.readLines(this);
	}
	
	public String read() throws IOException {
		return FileUtil.readString(this);
	}
	
	private static String untangle(String absolutePath) {
		if (nullOrEmpty(absolutePath))
			return absolutePath;
		
		if (absolutePath.indexOf("$HOME") >= 0) {
			String home = System.getProperty("user.home");
			if (home.endsWith("/."))
				home = home.substring(0, home.length() - 2);
			absolutePath = absolutePath.replaceAll("\\$HOME", home);
		}
		
		if (absolutePath.endsWith("/."))
			absolutePath = absolutePath.substring(0, absolutePath.length() - 2);
		
		if (absolutePath.startsWith("./") && !absolutePath.startsWith(".."))
			return System.getProperty("user.dir") + "/" + absolutePath.substring(2);
		else if (absolutePath.trim().startsWith("~"))
			return System.getProperty("user.home") + absolutePath.substring(1);
		else
			return absolutePath;
	}

	@Override
	public String getAbsolutePath() {
		return untangle(super.getAbsolutePath());
//		String absolutePath = super.getAbsolutePath();
//
//		if (absolutePath.startsWith("."))
//			return System.getProperty("user.dir") + absolutePath.substring(1);
//		else if (absolutePath.trim().startsWith("~"))
//			return System.getProperty("user.home") + absolutePath.substring(1);
//		else
//			return absolutePath;
	}
	
	public String getMD5() throws IOException {
		return SHAManager.getSHA(getPath(), SHAManager.MD5);
	}

	public String getSHA256() throws IOException {
		return SHAManager.getSHA(getPath(), SHAManager.SHA256);
	}
	
	public String getSHA512() throws IOException {
		try {
			if (isSymlink())
				return null;
			return SHAManager.getSHA(getPath(), SHAManager.SHA512);
		} catch (Exception e) {
			LibsLogger.error(FileExt.class, "Cannot compute SHA512 for the file ", getPath());
			return null;
		}
	}

	@Override
	public Iterator<String> iterator() {
		return new TextFileIterator(getPath());
	}
	
	public boolean createSymlink(File dest, boolean replace) throws IOException {
		if (!replace)
			return createSymlink(dest);
		else {
			try {
				return createSymlink(dest);
			} catch (FileAlreadyExistsException e) {
				dest.delete();
				return createSymlink(dest);
			}
		}
	}
	
	public boolean createSymlink(File dest) throws IOException {
		return Files.createSymbolicLink(Paths.get(dest.getAbsolutePath()), Paths.get(getAbsolutePath())) != null;
	}
	
	public UserPrincipal getOwner() throws IOException {
		return java.nio.file.Files.getOwner(Paths.get(getAbsolutePath()));
	}

	public boolean isSymlink() {
		return Files.isSymbolicLink(toPath());
	}
	
	public boolean isSymlinkBroken() {
		return Files.isSymbolicLink(toPath());
	}
	
	public Path readSymbolikLink() throws IOException {
		return Files.readSymbolicLink(toPath());
	}

	public boolean isDirectoryWritable() {
		// INFO does not work
		return Files.isWritable(toPath());
//		String toCreate = ".test-" + UUID.randomUUID();
//		try {
//			return new FileExt(getDirectoryOnly(), toCreate).createNewFile();
//		} catch (Throwable t) {
//			// catch all
//			return false;
//		} finally {
//			try {
//				new FileExt(getDirectoryOnly(), toCreate).deleteIfExists();
//			} catch (Throwable e) {
//				LibsLogger.error(FileExt.class, "Cannot remove tmp file", e);
//			}
//		}
	}
	
	public boolean isPlainText() {
		try {
			String mimeType = Files.probeContentType(toPath());
			return mimeType != null && mimeType.contains("text/plain");
		} catch (IOException e) {
			LibsLogger.error(FileExt.class, "Cannot check if file is a plain file", e);
			return false;
		}
	}

	public boolean isRoot() {
		return getAbsolutePath().equals("/");
	}
	
	public boolean isInSubfolder(FileExt folder) {
		return getAbsolutePath().startsWith(folder.getAbsolutePath());
	}
	
//	public FileExt getSubpath(String path) {
//		String relPath = getRelativePathTo(path);
//		return relPath != null ? new FileExt(relPath) : null;
//	}

//	public FileExt getTopParent() {
//		FileExt parent = new FileExt(this.getParentFile());
//		while (parent.getParentFile() != null)
//			parent = new FileExt(this.getParentFile());
//		return parent;
//	}
}
