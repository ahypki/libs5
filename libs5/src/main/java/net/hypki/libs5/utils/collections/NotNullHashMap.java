package net.hypki.libs5.utils.collections;

import java.util.HashMap;

public class NotNullHashMap<K, V> extends HashMap<K, V> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2709925507831334237L;
	
	private V returningValueIfNull = null;
	
	public NotNullHashMap(V returningValueIfNull) {
		this.returningValueIfNull = returningValueIfNull;
	}
	
	public V get(Object key) {
		return (super.get(key) == null ? returningValueIfNull : super.get(key));
	};
}
