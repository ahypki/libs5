package net.hypki.libs5.utils.system;

import java.io.IOException;
import java.util.jar.Manifest;

import net.hypki.libs5.utils.LibsLogger;

public class ManifestUtils {

	public static String getValue(Class<?> clazz, final String key) {
		try {
			Manifest manifest = new Manifest(clazz.getResourceAsStream("/META-INF/MANIFEST.MF"));
			return manifest.getMainAttributes().getValue(key);
		} catch (IOException e) {
			LibsLogger.error(ManifestUtils.class, "Cannot read manifest key ", key);
			return null;
		}
	}
}
