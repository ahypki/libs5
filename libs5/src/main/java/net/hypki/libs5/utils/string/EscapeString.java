package net.hypki.libs5.utils.string;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.swing.text.AbstractDocument.LeafElement;

import net.hypki.libs5.utils.LibsLogger;

public class EscapeString {
	
	private String inputString = null;
	
	private String escapedText = null;
	
	private Map<String, String> id2sub = null;
	
	private List<String> escapedChars = new ArrayList<String>();
	
//	private List<String> escapeRegex = new ArrayList<String>();

	public EscapeString() {
		
	}
	
	public EscapeString(String inputString) {
		setInputString(inputString);
	}
	
	public String getInputString() {
		return inputString;
	}

	public EscapeString setInputString(String inputString) {
		this.inputString = inputString;
		
		escapeInputString();
		
		return this;
	}
	
	public EscapeString escape(char toEscape) {
		getEscapeChars().add(String.valueOf(toEscape));
		
		escapeInputString();
		
		return this;
	}
	
	public EscapeString escape(String toEscape) {
		getEscapeChars().add(toEscape);
		
		escapeInputString();
		
		return this;
	}
	
	public EscapeString escapeRegex(String regexToEscape) {
		String lastEscapedText = getEscapedText();
		
		while (true) {
			String toEscape = RegexUtils.firstGroup(regexToEscape, getEscapedText());
			
			if (nullOrEmpty(toEscape))
				break;
			
			// check for internal escapes
			while (true) {
				String toEscapeInternal = RegexUtils.firstGroup(regexToEscape, toEscape.substring(1));
				if (toEscapeInternal != null)
					toEscape = toEscapeInternal;
				else
					break;
			}
			
			UUID uuid = UUID.randomUUID();
			getEscapers().put(uuid.toString(), toEscape);
			setEscapedText(getEscapedText().replace(toEscape, uuid.toString()));
			
			if (lastEscapedText.equals(getEscapedText()))
				break;
			lastEscapedText = getEscapedText();
		}
//		
		return this;
	}

	private void escapeInputString() {
		getEscapers().clear();
		
		// parsing
		setEscapedText(getInputString());
		for (String toEscape : getEscapeChars()) {
			if (getEscapedText().contains(toEscape)) {
				UUID uuid = UUID.randomUUID();
				getEscapers().put(uuid.toString(), toEscape);
				setEscapedText(getEscapedText().replace(toEscape, uuid.toString()));
			}
		}
//		for (String toEscape : getEscapeRegex()) {
//			UUID uuid = UUID.randomUUID();
//			setEscapedText(getMainText().replaceAll(toEscape, uuid.toString()));
//		}
	}

	private Map<String, String> getEscapers() {
		if (id2sub == null)
			id2sub = new HashMap<String, String>();
		return id2sub;
	}

	private void setEscapers(Map<String, String> id2sub) {
		this.id2sub = id2sub;
	}

	private List<String> getEscapeChars() {
		if (escapedChars == null)
			escapedChars = new ArrayList<String>();
		return escapedChars;
	}

	private void setEscapeChars(List<String> escapeChars) {
		this.escapedChars = escapeChars;
	}

	public String getEscapedText() {
		return escapedText;
	}

	public void setEscapedText(String escapedText) {
		this.escapedText = escapedText;
	}

//	private List<String> getEscapeRegex() {
//		if (escapeRegex == null)
//			escapeRegex = new ArrayList<String>();
//		return escapeRegex;
//	}
//
//	private void setEscapeRegex(List<String> escapeRegex) {
//		this.escapeRegex = escapeRegex;
//	}

	public String deescapeString(String stringToDeescape) {
		int safetyValve = 1_000_000;
		while (true) {
			String old = stringToDeescape;
			
			for (String key : getEscapers().keySet()) {
				stringToDeescape = stringToDeescape.replace(key, getEscapers().get(key));
			}
			
			if (old.equals(stringToDeescape))
				break;
			
			if (safetyValve-- <= 0) {
				LibsLogger.error(EscapeString.class, "Inf loop in deescapeString, safety valve kicked in");
				break;
			}
		}
		return stringToDeescape;
	}
	
	public void escapeBetween(int left, int right) {
		int safetyValve = 1_000_000;
		while (true) {
			int leftIdx = getEscapedText().indexOf(left);
			
			if (leftIdx == -1)
				break;
			
			int rightIdx = getEscapedText().indexOf(right, leftIdx);
			
			UUID uuid = UUID.randomUUID();
			String toEscape = getEscapedText().substring(leftIdx, rightIdx + 1);
			
			getEscapers().put(uuid.toString(), toEscape);
			setEscapedText(getEscapedText().replace(toEscape, uuid.toString()));
			
			if (safetyValve-- <= 0) {
				LibsLogger.error(EscapeString.class, "Inf loop in escapeBetween, safety valve kicked in");
				break;
			}
		}
	}

	public void escapeAll(String regex) {
		int safetyValve = 1_000_000;
		
		while (RegexUtils.contains(regex, getEscapedText())) {
			escapeRegex(regex);
			
			if (safetyValve-- <= 0) {
				LibsLogger.error(EscapeString.class, "Inf loop in deescapeString, safety valve kicked in");
				break;
			}
		}
	}
}
