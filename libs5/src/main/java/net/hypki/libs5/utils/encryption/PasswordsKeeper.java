package net.hypki.libs5.utils.encryption;


public class PasswordsKeeper {
	public String MAIN_PASSWORD_KEY = null;
	
	public PasswordsKeeper() {
		
	}
	
	public PasswordsKeeper(String password) throws IncorrectMainPasswordException {
		setPassword(password);
	}
	
	public void setPassword(String password) throws IncorrectMainPasswordException {
		if (password.length() < 24)
			MAIN_PASSWORD_KEY = password + "000000000000000000000000".substring(password.length());
		else
			MAIN_PASSWORD_KEY = password;
	}
	
	public String getPassword() {
		return MAIN_PASSWORD_KEY;
	}
	
	public boolean isMainPasswordSpecified() {
		if (getPassword() == null || getPassword().trim().length() == 0)
			return false;
		return true;
	}
}
