package net.hypki.libs5.utils.api;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.reflection.SystemUtils;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

public class API {
	
	@Expose
	private List<APICall> calls = null;

	public API() {
		
	}
	
	@Override
	public String toString() {
		return JsonUtils.objectToStringPretty(this);
	}
	
	public static API buildAPI(String apiJson) {
		return JsonUtils.fromJson(apiJson, API.class);
	}
	
	public static API buildAPI(Class startingClass) {
		try {
			API api = new API();
			
			List<Class<?>> classes = null;
			
			if (SystemUtils.isRunningFromJar()) {
				String jarPath = ReflectionUtility.getJarExecutionPath(startingClass);
				classes = ReflectionUtility.getClassesFromJARFile(jarPath, startingClass.getPackage().getName());
			} else
				classes = ReflectionUtility.getClasses(startingClass, null, Path.class, true);
			
			for (Class javarxClass : classes) {
				for (Method method : ReflectionUtility.getMethodsRecursivelly(javarxClass, null, Path.class)) {
					APICall call = new APICall();
					
					// path
					call.setPath(((Path) javarxClass.getAnnotation(Path.class)).value() + 
							"/" + ((Path) method.getAnnotation(Path.class)).value());
					
					// GET, POST, DELETE
					if (method.getAnnotation(GET.class) != null)
						call.setType(APICallType.GET);
					else if (method.getAnnotation(POST.class) != null)
						call.setType(APICallType.POST);
					else if (method.getAnnotation(DELETE.class) != null)
						call.setType(APICallType.DELETE);
					else
						throw new RuntimeException("Unknown case for API call type");
					
					// return value
					if (method.getReturnType().equals(String.class))
						call.setReturnValue(APIReturnType.STRING);
					else if (method.getReturnType().equals(void.class))
						call.setReturnValue(APIReturnType.VOID);
					else if (method.getReturnType().equals(boolean.class))
						call.setReturnValue(APIReturnType.BOOLEAN);
					else if (method.getReturnType().equals(long.class))
						call.setReturnValue(APIReturnType.LONG);
					else if (method.getReturnType().equals(Response.class))
						call.setReturnValue(APIReturnType.RESPONSE);
					else if (method.getReturnType().equals(StreamingOutput.class))
						call.setReturnValue(APIReturnType.OUTPUTSTREAM);
					else
						throw new NotImplementedException("Return type " + method.getReturnType() + " not implemented " 
								+ "for " + call.getPath());
					
					// arguments
					for (Parameter param : method.getParameters()) {

						if (param.getParameterizedType().equals(SecurityContext.class))
							continue;
						else if (param.getParameterizedType().equals(UriInfo.class))
							continue;
						else if (param.getParameterizedType().getTypeName().equals("org.glassfish.jersey.media.multipart.FormDataContentDisposition"))
							continue;
						
						APIArg arg = new APIArg();
						
						Annotation paramAnn = null;
						String paramName = null;
						if (call.isGET()) {
							paramAnn = param.getAnnotation(QueryParam.class);
							paramName = paramAnn != null ? ((QueryParam) paramAnn).value() : "";
						} else if (call.isPOST()) {
							paramAnn = param.getAnnotation(FormParam.class);
//							assertTrue(param.getAnnotation(QueryParam.class) == null, 
//									"POST method " + call.getPath() + " cannot have " + QueryParam.class.getSimpleName() + " arguments");
							paramName = paramAnn != null ? ((FormParam) paramAnn).value() : "";
						} else if (call.isDELETE()) {
							paramAnn = param.getAnnotation(QueryParam.class);
							paramName = ((QueryParam) paramAnn).value();
						} else
							throw new RuntimeException("Unknown case " + call.getType());
						
						
						if (param.getParameterizedType().equals(String.class)) {

							arg.setName(paramName);
							arg.setType(APIArgType.STRING);
						
						} else if (param.getParameterizedType().equals(int.class)
								|| param.getParameterizedType().equals(Integer.class)) {

							arg.setName(paramName);
							arg.setType(APIArgType.INTEGER);
							
						} else if (param.getParameterizedType().equals(long.class)
								|| param.getParameterizedType().equals(Long.class)) {

							arg.setName(paramName);
							arg.setType(APIArgType.LONG);
							
						} else if (param.getParameterizedType().equals(double.class)
								|| param.getParameterizedType().equals(Double.class)) {

							arg.setName(paramName);
							arg.setType(APIArgType.DOUBLE);
							
						} else if (param.getParameterizedType().equals(boolean.class)
								|| param.getParameterizedType().equals(Boolean.class)) {

							arg.setName(paramName);
							arg.setType(APIArgType.BOOLEAN);
							
						} else if (param.getParameterizedType().equals(InputStream.class)) {

							arg.setName(paramName);
							arg.setType(APIArgType.INPUTSTREAM);
							
						} else if (param.getParameterizedType().getTypeName().equals("net.hypki.libs5.db.db.weblibs.utils.UUID")) {

							arg.setName(paramName);
							arg.setType(APIArgType.STRING);
							
						} else if (param.getParameterizedType().getTypeName().equals("javax.ws.rs.core.MultivaluedMap<java.lang.String, java.lang.String>")) {

							arg.setName(paramName);
							arg.setType(APIArgType.MAP);
							
						} else if (param.getParameterizedType().getTypeName().startsWith("java.util.List<")) {

							arg.setName(paramName);
							arg.setType(APIArgType.LIST);
							
						} else
							throw new RuntimeException("Unknown case " + call.getPath() + ", " + param.getParameterizedType());
						
						call.getArgs().add(arg);
					}
					
					api.getCalls().add(call);
				}
			}
			
			return api;
		} catch (Throwable e) {
			LibsLogger.error(API.class, "Cannot build API", e);
			return null;
		}
	}

	public List<APICall> getCalls() {
		if (calls == null)
			calls = new ArrayList<APICall>();
		return calls;
	}

	public void setCalls(List<APICall> calls) {
		this.calls = calls;
	}

	public APICall getCall(String apiPath, APICallType apiCallType) {
//		if (apiPath == null)
//			return null;
		
		String right = apiPath;
		if (right != null && right.endsWith("/"))
			right = right.substring(0, right.length() - 1);
		
		for (APICall apiCall : getCalls()) {
			// check api path
			String left = apiCall.getPath();
			if (left.endsWith("/"))
				left = left.substring(0, left.length() - 1);
			if (left.equals(right)) {
				if (apiCallType == null)
					return apiCall;
				else if (apiCallType != null && apiCall.getType().equals(apiCallType))
					return apiCall;
			}
		}
		return null;
	}
}
