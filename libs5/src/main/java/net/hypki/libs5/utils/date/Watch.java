package net.hypki.libs5.utils.date;

public class Watch {
	
	private long sum = 0;
	
	private long start = 0;

	public Watch() {
		reset();
	}
	
	public Watch(long start) {
		this.start = start;
	}
	
	public static Watch fromNow() {
		return new Watch();
	}
	
	public static Watch from(long start) {
		return new Watch(start);
	}
	
	public long getStart() {
		return start;
	}
	
	public SimpleDate getStartDate() {
		return new SimpleDate(getStart());
	}
	
	public void pause() {
		sum += (System.currentTimeMillis() - start);
	}
	
	public void unpause() {
		start = System.currentTimeMillis();
	}

	public void reset() {
		start = System.currentTimeMillis();
	}
	
	public long ms() {
		return System.currentTimeMillis() - start + sum;
	}
	
	public long sec() {
		return (System.currentTimeMillis() - start + sum) / 1000;
	}
	
	public double mins() {
		return sec() / 60.0;
	}
	
	public double hours() {
		return mins() / 60.0;
	}
	
	public double days() {
		return hours() / 24.0;
	}
	
	@Override
	public String toString() {
		long tmp = ms();
		double units = tmp / (double) DateUtils.ONE_DAY_MS;
		if (units > 1.0)
			return String.format("%.2f days", units);
		units = units * 24.0;
		if (units > 1.0)
			return String.format("%.2f hours", units);
		units = units * 60.0;
		return units > 1.0 ? String.format("%.2f mins", units) : String.format("%.2f sec", units * 60.0);
	}
}
