package net.hypki.libs5.utils.utils;

public class PhysicalConstants {

	/**
	 * G used for binaries periods calculations to get results in years
	 */
	public static final double G = 0.39187189344e9;

}
