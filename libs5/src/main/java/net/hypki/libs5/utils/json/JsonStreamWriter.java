package net.hypki.libs5.utils.json;

import java.io.IOException;

public interface JsonStreamWriter {

	public void beginObject() throws IOException;
	public void endObject() throws IOException;
	public void beginArray() throws IOException;
	public void endArray() throws IOException;
	public void close() throws IOException;
	public void append(String name, String value) throws IOException;
	public void append(String name, long value) throws IOException;
	public void name(String name) throws IOException;
	public void value(String value) throws IOException;
}
