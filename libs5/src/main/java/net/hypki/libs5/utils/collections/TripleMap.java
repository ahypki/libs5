package net.hypki.libs5.utils.collections;

import java.util.HashMap;

public class TripleMap<K, K2, V> extends HashMap<K, HashMap<K2, V>> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3392704298017601961L;
	
	private V returningValueIfNull = null;
	
	public TripleMap() {
		
	}
	
	public TripleMap(V returningValueIfNull) {
		this.setReturningValueIfNull(returningValueIfNull);
	}
	
	public V put(K key, K2 key2, V value) {
		if (get(key) == null) {
			super.put(key, new HashMap<K2, V>());
		}
		return get(key).put(key2, value);
	};
	
	public V get(K key, K2 key2) {
		if (get(key) == null) {
			return getReturningValueIfNull() != null ? getReturningValueIfNull() : null;
		}
		
		if (get(key).get(key2) != null)
			return get(key).get(key2);
		else if (getReturningValueIfNull() != null)
			return getReturningValueIfNull();
		else
			return null;
	}

	public V getReturningValueIfNull() {
		return returningValueIfNull;
	}

	public void setReturningValueIfNull(V returningValueIfNull) {
		this.returningValueIfNull = returningValueIfNull;
	};
}
