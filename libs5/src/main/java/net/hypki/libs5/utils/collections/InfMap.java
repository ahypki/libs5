package net.hypki.libs5.utils.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.json.JsonUtils;
import net.sf.oval.constraint.AssertValid;

public class InfMap implements Serializable {

	@Expose
	@AssertValid
	private Map<Object, Object> map = null;
	
	static {
		JsonUtils.registerTypeAdapter(InfMap.class, new InfMapSerializer());
	}
	
	public InfMap() {
		
	}
	
	public Iterable<List> iterateKeys() {
		return new Iterable<List>() {
			@Override
			public Iterator<List> iterator() {
				return new Iterator<List>() {
					private Iterator<Object>[] keysIter = null;
					private Object[] nextKeys = null;
					private List<Object> next = null;
					// TODO add safety valve to compute whether all keys were taken into account
					
					@Override
					public List next() {
						try {
							if (keysIter == null)
								hasNext();
							
							return next;
						} finally {
							next = null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (next != null)
							return true;
						
						boolean movedToNext = false;
						
						if (keysIter == null) {
							// init
							keysIter = new Iterator[getDepth()];
							nextKeys = new Object[getDepth()];
							
							for (int i = 0; i < keysIter.length; i++) {
								keysIter[i] = getKeysIterator(nextKeys);
								nextKeys[i] = keysIter[i].hasNext() ? keysIter[i].next() : null;
							}
							movedToNext = true;
						}
						
						// iterate next key
						while (!movedToNext) {
							// if the first iterator does not have more elements then it is over
							if (keysIter[0] == null)
								return false; 
							
							// check if one has to open some new iterators
							for (int i = 1; i < keysIter.length; i++) {
								if (keysIter[i] == null) {
									keysIter[i] = getKeysIterator(nextKeys);
									nextKeys[i] = keysIter[i].next();
									movedToNext = true;
								}
							}
							
							// going to the next element for the first possible iterator which has
							// more elements (starting from the most nested one)
							if (!movedToNext) {
								for (int i = keysIter.length - 1; i >= 0; i--) {
//									if (keysIter[i] == null)
//										break;
//									else {
										// iterator is not null, trying to get next elements
										if (keysIter[i].hasNext()) {
											nextKeys[i] = keysIter[i].next();
											
											// check if one has to open some new iterators
											for (int k = i + 1; k < keysIter.length; k++) {
												if (keysIter[k] == null) {
													keysIter[k] = getKeysIterator(nextKeys);
													nextKeys[k] = keysIter[k].next();
												}
											}
											
											movedToNext = true;
											break;
										} else {
											// no more elements in this iterator, setting null to it
											keysIter[i] = null;
											nextKeys[i] = null;
											movedToNext = false;
//											break;
										}
//									}
								}
							}
						}
						
						// build the next list of keys
						next = new ArrayList<>();
						for (int i = 0; i < nextKeys.length; i++) {
							if (nextKeys[i] != null)
								next.add(nextKeys[i]);
							else {
								// there is no more elements
								next = null;
								break;
							}
						}
						
						return next != null;
					}
				};
			}
		};
	}
	
	private Iterator<Object> getKeysIterator(Object ... keys) {
		Map<Object, Object> currentMap = getMap();
		for (int i = 0; i < (keys.length - 1); i++) {
			Object key = keys[i];
			if (key == null)
				break;
			Object nestedMap = currentMap.get(key);
			if (nestedMap == null) {
				return null;
			}
			currentMap = (Map<Object, Object>) nestedMap;
		}
		
		if (currentMap != null)
			return currentMap.keySet().iterator();
		return null;
	}
	
	public int getDepth() {
		int depth = 1;
		Map<Object, Object> currentMap = getMap();
		while (true) {
			Iterator<Object> ii = currentMap.keySet().iterator();
			if (!ii.hasNext()) {
				break;
			}
			
			Object key = ii.next();
			Object nestedMap = currentMap.get(key);
			if (nestedMap == null) {
				break;
			}
			
			if (nestedMap instanceof Map)
				currentMap = (Map<Object, Object>) nestedMap;
			else
				break;
		}
		
		return depth;
	}
	
	public void put(Object value, Object ... keys) {
		Map<Object, Object> currentMap = getMap();
		for (int i = 0; i < (keys.length - 1); i++) {
			Object key = keys[i];
			Object nestedMap = currentMap.get(key);
			if (nestedMap == null) {
				nestedMap = new HashMap<Object, Object>();
				currentMap.put(key, nestedMap);
			}
			currentMap = (Map<Object, Object>) nestedMap;
		}
		
		if (currentMap != null)
			currentMap.put(keys[keys.length - 1], value);
	}
	
	public Object get(Object ... keys) {
		Map<Object, Object> currentMap = getMap();
		for (int i = 0; i < (keys.length - 1); i++) {
			Object key = keys[i];
			Object nestedMap = currentMap.get(key);
			if (nestedMap == null) {
				return null;
			}
			currentMap = (Map<Object, Object>) nestedMap;
		}
		
		if (currentMap != null)
			return currentMap.get(keys[keys.length - 1]);
		return null;
	}

	private Map<Object, Object> getMap() {
		if (map == null)
			map = new HashMap<>();
		return map;
	}

	private void setMap(Map<Object, Object> map) {
		this.map = map;
	}
}
