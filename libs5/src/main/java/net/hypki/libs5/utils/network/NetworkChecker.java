package net.hypki.libs5.utils.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class NetworkChecker {
	
	public static boolean isOnline() {
		try {
			InetAddress address = InetAddress.getByName("google.com");
//			System.out.println("Name: " + address.getHostName());
//			System.out.println("Addr: " + address.getHostAddress());
//			System.out.println("Reach: " + address.isReachable(5000));
			
			return address.getHostAddress() != null;
		} catch (UnknownHostException e) {
//			System.err.println("Unable to lookup web.mit.edu");
			return false;
		}
	}
}
