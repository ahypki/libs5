package net.hypki.libs5.utils.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

public class BigFile implements Iterable<String> {
	private BufferedReader reader;

	public BigFile(String filePath) throws IOException {
		reader = new BufferedReader(new FileReader(filePath));
	}

	public BigFile(InputStream is) throws IOException {
		reader = new BufferedReader(new InputStreamReader(is));
	}
	
	public BigFile(BufferedReader reader) throws IOException {
		this.reader = reader;
	}

	public void Close() {
		try {
			reader.close();
		} catch (Exception ex) {
		}
	}

	public Iterator<String> iterator() {
		return new FileIterator();
	}

	private class FileIterator implements Iterator<String> {
		private String currentLine;

		public boolean hasNext() {
			try {
				currentLine = reader.readLine();
			} catch (Exception ex) {
				currentLine = null;
				ex.printStackTrace();
			}

			return currentLine != null;
		}

		public String next() {
			return currentLine;
		}

		public void remove() {
		}
	}
}