package net.hypki.libs5.utils.scm;

public enum GitFileStatus {

	UNTRACKED,
	NEW,
	MODIFIED,
	REMOVED,
	MOVED
}
