package net.hypki.libs5.utils.string;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class HexUtils {

	static final String HEXES = "0123456789ABCDEF";
	
	public static String toHex(Serializable o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(o);
		oos.close();
		
		return new String(Hex.encodeHex(baos.toByteArray()));

//		byte[] raw = baos.toByteArray();
//		final StringBuilder hex = new StringBuilder(2 * raw.length);
//		for (final byte b : raw)
//			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
//		return hex.toString();
	}

	public static Object toObject(String hex) throws IOException, ClassNotFoundException, DecoderException {
		byte[] bytes = Hex.decodeHex(hex.toCharArray());
		ByteArrayInputStream bytesIn = new ByteArrayInputStream(bytes);
		ObjectInputStream ois = new ObjectInputStream(bytesIn);
		Object obj = ois.readObject();
		bytesIn.close();
		ois.close();
		return obj;
	}
}
