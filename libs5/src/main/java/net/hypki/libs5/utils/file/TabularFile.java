package net.hypki.libs5.utils.file;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Iterator;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.KMP;



public class TabularFile implements Iterable<Line>, Iterator<Line> {
	private Line line = null;
	private RandomAccessFile raf = null;
	private byte [] buf = null;
	private int lastNewlineIndex = -1;
	private int oldIndex = -1;
	private KMP kmp = null;
	private int lenRead = -1;
	private int linesRead = -1;
	
	public TabularFile(String filepath) throws IOException {
		raf = new RandomAccessFile(filepath, "r");
		raf.seek(0);
		line = new Line();
		buf = new byte[500000];
		kmp = new KMP("\n".getBytes());
	}
	
	public void close() {
		
	}

	@Override
	public Iterator<Line> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		try {
			return raf.getFilePointer() < raf.length() || !(oldIndex > 0 && lastNewlineIndex == -1);
		} catch (IOException e) {
			LibsLogger.error("Cannot change file pointer", e);
			return false;
		}
	}

	@Override
	public Line next() {
		try {
			if (lenRead == -1)
				lenRead = raf.read(buf, 0, 500000);
			
			oldIndex = lastNewlineIndex + 1;
			lastNewlineIndex = kmp.search(buf, oldIndex, lenRead - lastNewlineIndex - 1);
			
			if (lastNewlineIndex == -1) {
				// read next buffer
				lenRead = raf.read(buf, 0, 500000);
				
				if (lenRead > 0)
					oldIndex = 0;
				
				return line;
			} else {
				linesRead++;
				return line;
			}
			
		} catch (IOException e) {
			LibsLogger.error("Cannot get next line", e);
			return null;
		}
	}
	
	public int getReadLines() {
		return linesRead;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
