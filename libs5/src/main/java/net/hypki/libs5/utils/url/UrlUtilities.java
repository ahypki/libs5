package net.hypki.libs5.utils.url;

import static java.lang.String.format;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;
import javax.ws.rs.core.MediaType;

import jodd.servlet.UrlEncoder;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.json.UUIDAdapter;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.IOUtils;

import org.apache.commons.lang.NotImplementedException;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpDelete;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.google.gson.JsonElement;

//import org.apache.http.conn.ssl.SSLSocketFactory;

public class UrlUtilities {
	
	private static final String USER_AGENT = "Mozilla/5.0";
	
	private static boolean defaultSSLFactorySet = false;
	
	static {
		if (defaultSSLFactorySet == false) {
			try {
				// https://stackoverflow.com/questions/2642777/trusting-all-certificates-using-httpclient-over-https
				HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				});
				SSLContext context = SSLContext.getInstance("TLS");
				context.init(null, new X509TrustManager[] { new X509TrustManager() {
					public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					}
	
					public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					}
	
					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
							throws java.security.cert.CertificateException {
						
					}
	
					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
							throws java.security.cert.CertificateException {
						
					}
	
					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new java.security.cert.X509Certificate[0];
					}
				} }, new SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				LibsLogger.error(UrlUtilities.class, "Cannot change default SSL Factory", e);
			}
			
			defaultSSLFactorySet = true;
		}
	}
		
	public static String UrlEncode(String toEncode) {
		return toEncode != null ? UrlEncoder.encode(toEncode, "UTF-8") : null;
	}

	public static String urlDecode(String toDecode) throws UnsupportedEncodingException {
		return URLDecoder.decode(toDecode, "UTF-8");
	}
	
	public static String toQueryString(Object ... keyValueParams) {
		StringBuilder qs = new StringBuilder();
		if (keyValueParams != null && keyValueParams.length > 1)
			for (int i = 0; i < keyValueParams.length; i = i + 2) {
				if (keyValueParams[i + 1] instanceof Boolean) {
					if (((Boolean) keyValueParams[i + 1]) == true) {
						if (qs.length() > 0)
							qs.append("&");
						qs.append(UrlEncode(keyValueParams[i].toString()));
						qs.append("=");
						qs.append("true");
					}
				} else {
					if (keyValueParams[i] != null && keyValueParams[i + 1] != null) {
						if (qs.length() > 0)
							qs.append("&");
						qs.append(UrlEncode(keyValueParams[i].toString()));
						qs.append("=");
						qs.append(UrlEncode(keyValueParams[i + 1].toString()));
					}
				}
			}
		return qs.toString();
	}
	
	public static boolean domainExist(URL url) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("HEAD");
			con.setConnectTimeout(30000);
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (UnknownHostException e) {
			LibsLogger.error(UrlUtilities.class, "Url does not exist", e);
			return false;
		} catch (IOException e) {
			LibsLogger.error(UrlUtilities.class, "Url does not exist probably", e);
			return false;
		}
	}
	
	public static void openUrlAlternative(String url) throws BrowserException {
		String os = System.getProperty("os.name").toLowerCase();
		Runtime rt = Runtime.getRuntime();
		try {
			if (os.indexOf("win") >= 0) {
				
				// this doesn't support showing urls in the form of "page.html#nameLink"
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
				
			} else if (os.indexOf("mac") >= 0) {
				
				rt.exec("open " + url);
				
			} else if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {
				
				// Do a best guess on unix until we get a platform independent
				// way Build a list of browsers to try, in this order.
				String[] browsers = { "epiphany", "firefox", "mozilla",
						"konqueror", "netscape", "opera", "links", "lynx" };

				// Build a command string which looks like "browser1 "url" || browser2 "url" ||..."
				StringBuilder cmd = new StringBuilder();
				for (int i = 0; i < browsers.length; i++)
					cmd.append((i == 0 ? "" : " || ") + browsers[i] + " \""	+ url + "\" ");

				rt.exec(new String[] { "sh", "-c", cmd.toString() });
				
			} else {
				throw new BrowserException("Cannot open url in browser, unknown operating system");
			}
		} catch (Exception e) {
			throw new BrowserException("Cannot open url in browser, some exception occured", e);
		}	
	}

	public static void openURL(String url) throws BrowserException {
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Mac OS")) {
				
				Class<?> fileMgr = Class.forName("com.apple.eio.FileManager");
				Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[] { String.class });
				openURL.invoke(null, new Object[] { url });
				
			} else if (osName.startsWith("Windows")) {
				
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
			
			} else { 
				// assume Unix or Linux
				String[] browsers = { "firefox", "opera", "konqueror", "epiphany", "mozilla", "netscape" };
				String browser = null;
				for (int count = 0; count < browsers.length && browser == null; count++)
					if (Runtime.getRuntime().exec(new String[] { "which", browsers[count] }).waitFor() == 0)
						browser = browsers[count];
				if (browser == null)
					throw new BrowserException("Could not find web browser", null);
				else
					Runtime.getRuntime().exec(new String[] { browser, url });
			}
		
		} catch (ClassNotFoundException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (SecurityException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (NoSuchMethodException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (IOException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (InterruptedException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (IllegalArgumentException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (IllegalAccessException e) {
			throw new BrowserException("Could not open web browser", e);
		} catch (InvocationTargetException e) {
			throw new BrowserException("Could not open web browser", e);
		}
	}

	public static boolean checkUrlAvailability(String url) {
		try {
			URL testURL = new URL(url);
			URLConnection testConnection = testURL.openConnection();
			testConnection.connect();
			return true;
		} catch (MalformedURLException e) { // new URL() failed
			LibsLogger.error("Cannot check availability for url: " + url, e);
		} catch (IOException e) { // openConnection() failed
			LibsLogger.error("Cannot check availability for url: " + url, e);
		}
		return false;
	}

	public static String getSubdomainFromUrl(String url) {
		return url != null && url.length() > 2 ? url.substring(url.indexOf('/') + 2, url.indexOf('.')) : null;
	}
	
	public static void downloadContent(URL url, FileWriter fw) throws IOException {
		InputStream is = null;
		DataInputStream dis;
		String line;

		try {
			is = url.openStream(); // throws an IOException
			dis = new DataInputStream(new BufferedInputStream(is));

			while ((line = dis.readLine()) != null) {
//				System.out.println(line);
				fw.append(line);
			}
			
			
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (fw != null)
				fw.close();
			
			try {
				is.close();
			} catch (IOException ioe) {
				// nothing to see here
			}
		}
	}

	public static String downloadContent(URL url) {
//		return Jsoup.connect(url).get().html(); // TODO add jspup and switch that on
		InputStream is = null;
		DataInputStream dis;
		String line;
		StringBuilder builder = new StringBuilder();

		try {
			is = url.openStream(); // throws an IOException
			dis = new DataInputStream(new BufferedInputStream(is));

			while ((line = dis.readLine()) != null) {
//				System.out.println(line);
				builder.append(line);
			}
			
			return builder.toString();
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
			return null;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		} finally {
			try {
				is.close();
			} catch (IOException ioe) {
				// nothing to see here
			}
		}
	}

	public static boolean isUrlValid(String url) {
//		return url != null && url.matches(".*\\..*");
		return url != null && url.matches("^(https?://|ftp://|file://|/).+$");
//		return true;
	}
	
	public static net.hypki.libs5.utils.url.HttpResponse post(final String url, final String [] cookies, final File [] files, 
			final String [] params) throws IOException {
		InputStream [] filesStreams = new InputStream[files.length];
		String [] filesName = new String[files.length];
		
		int i = 0;
		for (File file : files) {
			filesStreams[i] = new FileExt(file).openInputStream();
			filesName[i] = file.getName();
		}
		
		Params par = new Params();
		if (params != null && params.length > 0) {
			for (int j = 0; j < params.length; j += 2) {
				par.add(params[j], params[j + 1]);
			}
		}
		
		return post(url,
				cookies,
				filesStreams,
				filesName,
				par);
	}
	
	public static net.hypki.libs5.utils.url.HttpResponse post(final String url, final String [] cookies, final InputStream [] files, 
			final String[] fileNames, final Params params) throws IOException {
//		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		 
		// add header
		post.setHeader("User-Agent", USER_AGENT);
//        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
		
		addCookies(post, cookies);

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		int fileNameCounter = 0;
		for (InputStream is : files) {
			builder.addBinaryBody("file", is, ContentType.DEFAULT_BINARY, fileNames[fileNameCounter++]);
		}
		
		if (params != null)
			for (Map.Entry<String, Object> entry : params.getParams().entrySet()) {
				builder.addTextBody(entry.getKey(), String.valueOf(entry.getValue()), ContentType.TEXT_PLAIN);
			}
		post.setEntity(builder.build());
		
		LibsLogger.debug(UrlUtilities.class, "Sending 'POST' request to URL : " + url);
		LibsLogger.debug(UrlUtilities.class, "Post parameters : " + params.toString());//EntityUtils.toString(post.getEntity()));
 
//		HttpResponse response = client.execute(post);
		String result = null;
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(post)) {
//            	System.out.println(response.getVersion()); // HTTP/1.1
//                System.out.println(response.getCode()); // 200
//                System.out.println(response.getReasonPhrase()); // OK

                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
                // Ensure that the stream is fully consumed
                EntityUtils.consume(entity);
                
                LibsLogger.debug(UrlUtilities.class, "Response Code : " + response.getCode());
            }
        } catch (Exception e) {
        	LibsLogger.error(UrlUtilities.class, "Cannot POST " + url, e);
        }
		
 
//		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
 
//		StringBuffer result = new StringBuffer();
//		String line = "";
//		while ((line = rd.readLine()) != null) {
//			result.append(line);
//		}
		
		// dealing with obsolete net.hypki.libs5.utils.url.HttpResponse response from API
		JsonElement je = JsonUtils.parseJson(result.toString());
		if (je.isJsonObject() && je.getAsJsonObject().get("body") == null) {
			return new net.hypki.libs5.utils.url.HttpResponse(200, je.toString());
		} else if (je.isJsonArray()) {
			return new net.hypki.libs5.utils.url.HttpResponse(200, je.toString());
		}
		
//		LibsLogger.debug(UrlUtilities.class, "Http response " + result);
		return JsonUtils.fromJson(result.toString(), net.hypki.libs5.utils.url.HttpResponse.class);
	}
	
	public static net.hypki.libs5.utils.url.HttpResponse post(final String url, final Params params) throws IOException {
		return post(url, null, params);
	}
	
	public static net.hypki.libs5.utils.url.HttpResponse post(final String url, final String[] cookies, final Params params) throws IOException {
//		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		 
		// add header
		post.setHeader("User-Agent", USER_AGENT);
//        post.setHeader("Content-Type", "text/plain; charset=UTF-8");
		
		addCookies(post, cookies);
 
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		if (params != null)
			for (Map.Entry<String, Object> entry : params.getParams().entrySet()) {
				if (entry.getValue() instanceof List) {
					List list = (List) entry.getValue();
					for (Object o : list) {
						urlParameters.add(new BasicNameValuePair(entry.getKey(), String.valueOf(o)));
					}
				} else
					urlParameters.add(new BasicNameValuePair(entry.getKey(), String.valueOf(entry.getValue())));
			}
		 
		post.setEntity(new UrlEncodedFormEntity(urlParameters, Charset.forName("UTF-8")));
		
		LibsLogger.debug(UrlUtilities.class, "Sending 'POST' request to URL : " + url);
		try {
			LibsLogger.debug(UrlUtilities.class, "Post parameters : " + EntityUtils.toString(post.getEntity()));
		} catch (Exception e) {
			LibsLogger.error(UrlUtilities.class, "Cannot print POST parameters", e);
		}
 
//		HttpResponse response = client.execute(post);
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(post)) {
//            	System.out.println(response.getVersion()); // HTTP/1.1
//                System.out.println(response.getCode()); // 200
//                System.out.println(response.getReasonPhrase()); // OK

                
                LibsLogger.debug(UrlUtilities.class, "Response Code : " + response.getCode());
                
                if (response.getEntity() != null && response.getEntity().getContent() != null) {
                	HttpEntity entity = response.getEntity();
                	String result = EntityUtils.toString(entity);
                	// Ensure that the stream is fully consumed
                	EntityUtils.consume(entity);
                	
//                	BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                	
                	// dealing with obsolete net.hypki.libs5.utils.url.HttpResponse response from API
                	JsonElement je = JsonUtils.parseJson(result.toString());
                	if (je.isJsonObject() && je.getAsJsonObject().get("body") == null) {
                		return new net.hypki.libs5.utils.url.HttpResponse(200, je.toString());
                	} else if (je.isJsonArray()) {
                		return new net.hypki.libs5.utils.url.HttpResponse(200, je.toString());
                	}
                	
                	return JsonUtils.fromJson(result.toString(), net.hypki.libs5.utils.url.HttpResponse.class);
                } else
                	return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), null);
            }
        } catch (Exception e) {
        	LibsLogger.error(UrlUtilities.class, "Cannot POST " + url, e);
        }
		
		return new net.hypki.libs5.utils.url.HttpResponse(HttpStatus.SC_BAD_REQUEST, null);
//		LibsLogger.debug(UrlUtilities.class, "Http response " + result);
	}
	
	public static net.hypki.libs5.utils.url.HttpResponse post(final String url, final String[] cookies, final String body) throws IOException {
//		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		 
		// add header
		post.setHeader("User-Agent", USER_AGENT);
        post.setHeader("Content-Type", "application/json");
		
		addCookies(post, cookies);
 		 
		post.setEntity(new StringEntity(body));
		
		LibsLogger.debug(UrlUtilities.class, "Sending 'POST' request to URL : " + url);
		try {
			LibsLogger.debug(UrlUtilities.class, "Post parameters : " + EntityUtils.toString(post.getEntity()));
		} catch (Exception e) {
			LibsLogger.error(UrlUtilities.class, "Cannot print POST params to the screen", e);
		}
 
//		HttpResponse response = client.execute(post);
		String result = null;
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(post)) {
//            	System.out.println(response.getVersion()); // HTTP/1.1
//                System.out.println(response.getCode()); // 200
//                System.out.println(response.getReasonPhrase()); // OK

                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
                // Ensure that the stream is fully consumed
                EntityUtils.consume(entity);
                
                LibsLogger.debug(UrlUtilities.class, "Response Code : " + response.getCode());
                
                if (response.getEntity() != null && response.getEntity().getContent() != null) {
                	BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                	
                	if (JsonUtils.isJsonSyntax(result.toString())) {
                		JsonElement e = JsonUtils.parseJson(result.toString());
                		if (e.isJsonObject()) {
                			if (e.getAsJsonObject().get("code") != null)
                				return JsonUtils.fromJson(e, net.hypki.libs5.utils.url.HttpResponse.class);
                			else
                				return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), 
                						result.toString());
                		} else
                			throw new NotImplementedException();
                	} else {
                		return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), 
                				result.toString());
                	}
                	
//			return JsonUtils.fromJson(result.toString(), net.hypki.libs5.utils.url.HttpResponse.class);
                } else
                	return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), null);
            }
        } catch (Exception e) {
        	LibsLogger.error(UrlUtilities.class, "Cannot POST " + url, e);
        }
		
		return new net.hypki.libs5.utils.url.HttpResponse(HttpStatus.SC_BAD_REQUEST, null);
//		LibsLogger.debug(UrlUtilities.class, "Http response " + result);
	}
	
	public static net.hypki.libs5.utils.url.HttpResponse get(final String url, final String[] cookies, 
			final Params params, MediaType mediaType) throws IOException {
//		HttpParams http = new BasicHttpParams();
//		if (params != null)
//			for (Map.Entry<String, Object> entry : params.getParams().entrySet()) {
//				http.setParameter(entry.getKey(), entry.getValue());
//			}
				
//		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url + "?" + params);
		 
		// add header
//		get.setHeader("User-Agent", USER_AGENT);
//		if (mediaType != null)
//			get.setHeader("Accept", MediaType.TEXT_PLAIN);
		
		addCookies(get, cookies);
 		
		LibsLogger.debug(UrlUtilities.class, "Sending 'GET' request to URL : " + url);
//		LibsLogger.debug(UrlUtilities.class, "Post parameters : " + post.getEntity());
 
//		HttpResponse response = client.execute(get);
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                // Get status code
//                System.out.println(response.getVersion()); // HTTP/1.1
//                System.out.println(response.getCode()); // 200
//                System.out.println(response.getReasonPhrase()); // OK
                HttpEntity entity = response.getEntity();
                // Get response information
                
                LibsLogger.debug(UrlUtilities.class, "Response Code : " + response.getCode());
                
                if (response.getEntity() != null) {
                	String resultContent = EntityUtils.toString(entity);
//                	BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                	
//                	StringBuffer result = new StringBuffer();
//                	String line = "";
//                	while ((line = rd.readLine()) != null) {
//                		result.append(line);
//                	}
                	
//			if (StringUtilities.nullOrEmpty(result.toString().trim()))
                	if (StringUtilities.nullOrEmpty(resultContent))
                		return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), null);
                	
//			LibsLogger.debug(UrlUtilities.class, "Http response " + result);
                	
                	// dealing with obsolete net.hypki.libs5.utils.url.HttpResponse response from API
                	JsonElement je = JsonUtils.parseJson(resultContent);
                	if (je.isJsonObject() && je.getAsJsonObject().get("body") == null) {
                		return new net.hypki.libs5.utils.url.HttpResponse(200, je.toString());
                	} else if (je.isJsonArray()) {
                		return new net.hypki.libs5.utils.url.HttpResponse(200, je.toString());
                	}
                	
                	return JsonUtils.fromJson(resultContent, net.hypki.libs5.utils.url.HttpResponse.class);
                } else
                	return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), null);
            } catch (Exception e) {
            	LibsLogger.error(UrlUtilities.class, "Cannot GET " + url, e);
            }
        } catch (Exception e) {
        	LibsLogger.error(UrlUtilities.class, "Cannot GET " + url, e);
        }
		
		return new net.hypki.libs5.utils.url.HttpResponse(HttpStatus.SC_BAD_REQUEST, null);
	}
	
	public static void getStream(final String url, final String[] cookies, final Params params, OutputStream out) throws IOException {
//		HttpParams http = new BasicHttpParams();
//		if (params != null)
//			for (Map.Entry<String, Object> entry : params.getParams().entrySet()) {
//				http.setParameter(entry.getKey(), entry.getValue());
//			}
		
//		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url + "?" + params);
		 
		// add header
		get.setHeader("User-Agent", USER_AGENT);
		
		addCookies(get, cookies);
 		
		LibsLogger.debug(UrlUtilities.class, "Sending 'GET' request to URL : " + url);
//		LibsLogger.debug(UrlUtilities.class, "Post parameters : " + post.getEntity());
		
		UUID fileId = UUID.randomUUID();
		InputStream inputStream = null;
		
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                // Get status code
//                System.out.println(response.getVersion()); // HTTP/1.1
//                System.out.println(response.getCode()); // 200
//                System.out.println(response.getReasonPhrase()); // OK
                
            	IOUtils.pipe(response.getEntity().getContent(), out);
            } catch (Exception e) {
            	LibsLogger.error(UrlUtilities.class, "Cannot GET " + url, e);
            }
        } catch (Exception e) {
        	LibsLogger.error(UrlUtilities.class, "Cannot GET " + url, e);
        }
	}

	public static net.hypki.libs5.utils.url.HttpResponse delete(String url, String[] cookies, Params params) throws IOException {
//		HttpClient client = new DefaultHttpClient();
		HttpDelete delete = new HttpDelete(url + "?" + params);
		 
		// add header
		delete.setHeader("User-Agent", USER_AGENT);
		
		addCookies(delete, cookies);
 		
		LibsLogger.debug(UrlUtilities.class, "Sending 'DELETE' request to URL : " + url);
//		LibsLogger.debug(UrlUtilities.class, "Post parameters : " + post.getEntity());
 
//		HttpResponse response = client.execute(delete);
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = httpclient.execute(delete)) {
//            	System.out.println(response.getVersion()); // HTTP/1.1
//                System.out.println(response.getCode()); // 200
//                System.out.println(response.getReasonPhrase()); // OK

                HttpEntity entity = response.getEntity();
                if (entity != null) {
	                String result = EntityUtils.toString(entity);
	                // Ensure that the stream is fully consumed
	                EntityUtils.consume(entity);
	                
	                LibsLogger.debug(UrlUtilities.class, "Response Code : " + response.getCode());
	                if (response.getEntity() != null && response.getEntity().getContent() != null) {
	                	BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	                	
	                	//		LibsLogger.debug(UrlUtilities.class, "Http response " + result);
	                	return JsonUtils.fromJson(result.toString(), net.hypki.libs5.utils.url.HttpResponse.class);
	                } else
	                	return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), null);
                } else
                	return new net.hypki.libs5.utils.url.HttpResponse(response.getCode(), null);
            }
        } catch (Exception e) {
        	LibsLogger.error(UrlUtilities.class, "Cannot POST " + url, e);
        }
		
		return new net.hypki.libs5.utils.url.HttpResponse(HttpStatus.SC_BAD_REQUEST, null);
	}
	
	private static void addCookies(HttpUriRequestBase http, String[] cookies) {
		// build cookie string
		if (cookies != null && cookies.length > 0) {
			StringBuilder cookiesStr = new StringBuilder();
			for (int i = 0; i < cookies.length; i = i + 2) {
				cookiesStr.append(cookies[i]);
				cookiesStr.append("=");
				cookiesStr.append(cookies[i + 1]);
				cookiesStr.append("; ");
			}
			http.addHeader("Cookie", cookiesStr.toString());
		}
	}
}
