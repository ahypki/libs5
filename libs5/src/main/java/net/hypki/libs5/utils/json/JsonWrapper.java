package net.hypki.libs5.utils.json;

import java.io.IOException;

import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonWrapper {

	private JsonElement json = null;
	
	public JsonWrapper() {
		
	}
	
	public JsonWrapper(JsonElement json) {
		this.json = json;
	}
	
	public JsonWrapper(String json) {
		this.json = JsonUtils.parseJson(json);
	}
	
	@Override
	public String toString() {
		return getJson().toString();
	}
	
	public JsonElement getJson() {
		if (json == null)
			json = new JsonObject();
		return json;
	}
	
	private JsonObject getJsonObject() {
		if (getJson() instanceof JsonObject)
			return (JsonObject) getJson();
		throw new NotImplementedException();
	}
	
	private JsonArray getJsonArray() {
		if (json == null) {
			json = new JsonArray();
			return (JsonArray) json;
		}
		
		if (getJson() instanceof JsonArray)
			return (JsonArray) getJson();
		throw new NotImplementedException();
	}
	
	public JsonWrapper set(String jsonPath, Object value) {
		JsonUtils.putSetting(getJson(), jsonPath, value);
		return this;
	}
	
	public boolean isArray() {
		return json != null && json.isJsonArray();
	}

	public int size() {
		if (isArray())
			return ((JsonArray) json).size();
		return -1;
	}

	public JsonWrapper get(int i) {
		AssertUtils.assertTrue(isArray(), "Json is not an array, cannot get object at index ", i);
		return new JsonWrapper(getJsonArray().get(i));
	}

	public String get(String name) {
		if (name.indexOf('/') >= 0)
			return JsonUtils.readString(getJsonObject(), name);
		JsonElement e = getJsonObject().get(name);
		return e != null ? e.getAsString() : null;
	}

	public JsonWrapper add(JsonWrapper element) {
		getJsonArray().add(element.getJson());
//		AssertUtils.assertTrue(isArray(), "This json wrapper is not an array, cannot add elements to it");
		return this;
	}

	public void delete(String path) {
		getJsonObject().remove(path);
	}
}
