package net.hypki.libs5.unittests.json;

import com.google.gson.annotations.Expose;

public class Person {
	@Expose
	private String name = "John Lock";
	
	public Person() {
		
	}
	
	public Person(String name) {
		this.setName(name);
	}
	
	@Override
	public String toString() {
		return "inner: " + getName();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
