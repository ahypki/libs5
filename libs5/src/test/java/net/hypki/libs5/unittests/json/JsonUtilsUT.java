package net.hypki.libs5.unittests.json;

import java.io.IOException;

import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;

import org.junit.Test;

public class JsonUtilsUT {

	@Test
	public void testDeser() throws IOException {
		Person p = new Person("john snow");
		
		String pJson = JsonUtils.objectToStringPretty(p);
		
		System.out.println(pJson);
		
		Person newPerson = new Person();

		ReflectionUtility.copyProperties(newPerson, p);
		
		System.out.println(JsonUtils.objectToStringPretty(newPerson));
		
		System.out.println("I know something afterall");
	}
}
