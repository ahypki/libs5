package net.hypki.libs5.unittests.xml;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class XmlReadWriteUnitTest extends LibsTestCase {
	
	@Test
	public void testRead() throws ParserConfigurationException, SAXException, IOException {
		// The two lines below are just for getting an instance of DocumentBuilder which we use
		// for parsing XML data
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Here we do the actual parsing
		Document doc = builder.parse(SystemUtils.readFileContent("SampleXML.xml"));

		// Here we get the root element of XML and print out the value of its 'testAttr' attribute
		Element rootElement = doc.getDocumentElement();
		System.out.println("testAttr for root element: " + rootElement.getAttribute("testAttr"));

		// Here we get a list of all elements named 'child'
		NodeList list = rootElement.getElementsByTagName("child");

		// Traversing all the elements from the list and printing out its data
		for (int i = 0; i < list.getLength(); i++) {
			// Getting one node from the list. BTW, we used method getElementsByTagName so every entry
			// in the list is effectively of type 'Element', so you could cast it directly to 'Element' if 
			// you needed to.
			Node childNode = list.item(i);
			System.out.println("data in child number " + i + ": " + childNode.getNodeValue());
			
			assertTrue(childNode.getNodeValue().equals("data" + (i + 1)));
		}

		assertTrue(rootElement.getAttribute("testAttr").equals("testValue"));
	}
	
	@Test
	public void testWrite() throws ParserConfigurationException, FileNotFoundException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Here instead of parsing an existing document we want to create a new one.
		Document testDoc = builder.newDocument();

		// This creates a new tag named 'testElem' inside the document and sets its data to 'TestContent'
		Element el = testDoc.createElement("testElem");
		el.setNodeValue("TestContent");
		testDoc.appendChild(el);

		// The XML document we created above is still in memory so we have to output it to a real file.
		// In order to do it we first have to create an instance of DOMSource
		DOMSource source = new DOMSource(testDoc);

		// PrintStream will be responsible for writing the text data to the file
		PrintStream ps = new PrintStream("test2.xml");
		StreamResult result = new StreamResult(ps);

		// Once again we are using a factory of some sort, this time for getting a Transformer instance,
		// which we use to output the XML
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		// The actual output to a file goes here
		transformer.transform(source, result);

	}
}
