package net.hypki.libs5.unittests.io;

import static net.hypki.libs5.utils.string.RandomUtils.nextInt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.LoremIpsum;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class FileUtilitiesUnitTest extends LibsTestCase {
	
	@Test
	public void testAbsoluteNames() {
		assertTrue(new FileExt("~").getAbsolutePath().equals(System.getenv("HOME")));
	}

	@Test
	public void testFolderName() {
		FileExt file = new FileExt("/home/user/file.pdf");
		
		assertTrue(file.getParent().equals("/home/user"));
		assertTrue(file.getFilenameOnly().equals("file.pdf"));
	}
	
	@Test
	public void testValidDirectory() {
		FileExt file = new FileExt("/home/user/file.pdf");
		assertTrue("Wrong extension", file.getExtensionOnly().equals("pdf"));
		
		file = new FileExt("/home/user/file.test");
		assertTrue("Wrong extension", file.getExtensionOnly().equals("test"));
		
		file = new FileExt("/home/user/file_pdf");
		assertTrue("Wrong extension", file.getExtensionOnly() == null);
	}
	
	@Test
	public void testTailMaxBytes() throws IOException {
		List<String> lines = new ArrayList<String>();
		for (int i = 0; i < 1000; i++) {
			lines.add(LoremIpsum.createSampleString(nextInt(10, 400)));
		}
		
		File f = new File("test-tail");
		
		FileUtils.saveToFile(lines, f);
		
		System.out.println(FileUtils.tail(f, 600));
	}
}
