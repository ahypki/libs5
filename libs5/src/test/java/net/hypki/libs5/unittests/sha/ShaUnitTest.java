package net.hypki.libs5.unittests.sha;

import java.io.File;
import java.io.IOException;

import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.sha.SHAManager;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class ShaUnitTest extends LibsTestCase {
	
	@Test
	public void testSHA512Sum() throws IOException {
		String fileToHash = ShaUnitTest.class.getClassLoader().getResource("test_file_for_SHA512.txt").getPath();
		
		// sha256sum test_file_for_SHA512.txt
		String sha256FromLinux = "9a4616c1caacb21fab42e2483892404920742d207e979c20b190084d61f790da";
		String sha256FromSHAManager = SHAManager.getSHA(fileToHash, SHAManager.SHA256);
		assertTrue(sha256FromLinux.equals(sha256FromSHAManager));
		
		// sha512sum test_file_for_SHA512.txt
		String sha512FromLinux = "e5e7009ea931881a589159727b7068a16081f289f1d8d42628b61edd9a90380b1da9adfcbb082e53cfc17b86b2d05c158b6c31d351b3b1212c6a4ad505ac13b7";
		String sha512FromSHAManager = SHAManager.getSHA(fileToHash, SHAManager.SHA512);
		assertTrue(sha512FromLinux.equals(sha512FromSHAManager));
		
		// read data from file as byte[]
		byte[] data = FileUtils.getBytesFromFile(new File(fileToHash));
		String sha256FromSHAManagerAsByteArray = SHAManager.getSHA(data, SHAManager.SHA256);
		assertTrue(sha256FromLinux.equals(sha256FromSHAManagerAsByteArray));
	}
}
