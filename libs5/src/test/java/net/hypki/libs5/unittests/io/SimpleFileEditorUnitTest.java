package net.hypki.libs5.unittests.io;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import net.hypki.libs5.utils.file.SimpleFileEditor;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Assert;
import org.junit.Test;


public class SimpleFileEditorUnitTest extends LibsTestCase {
	
	@Test
	public void testSimpleNewFile() throws IOException {
		// create new file
		String path = "test-file-SimpleFileEditor";
		SimpleFileEditor sfe = new SimpleFileEditor(path);
		sfe.open(false);
		String toWrite = "one two three four";
		sfe.write(toWrite);
		sfe.close();
		
		
		// read file normally and compare results
		FileInputStream fstream = new FileInputStream(path);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			Assert.assertTrue(strLine.equals(toWrite));
		}
		in.close();
		
		// remove file
		Assert.assertTrue(new File(path).delete() == true);
	}
	
	@Test
	public void testSimpleAppend() throws IOException {
		// create new file
		String path = "test-file-SimpleFileEditor-2";
		SimpleFileEditor sfe = new SimpleFileEditor(path);
		sfe.open(false);
		String toWrite = "one two ";
		sfe.write(toWrite);
		sfe.close();
		
		// append something to file
		sfe = new SimpleFileEditor(path);
		sfe.open(true);
		toWrite = "three four";
		sfe.write(toWrite);
		sfe.close();
		
		
		// read file normally and compare results
		FileInputStream fstream = new FileInputStream(path);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		while ((strLine = br.readLine()) != null) {
			Assert.assertTrue(strLine.equals("one two three four"));
		}
		in.close();
		
		// remove file
		Assert.assertTrue(new File(path).delete() == true);
	}
}
