package net.hypki.libs5.unittests.file;

import static net.hypki.libs5.utils.string.StringUtilities.split;
import static net.hypki.libs5.utils.utils.NumberUtils.toDouble;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Test;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.BigFile;
import net.hypki.libs5.utils.file.BigFilePartial;

public class BigFilePartialTests {

	@Test
	public void testRead() throws IOException {
		final String filePlain = "/home/ahypki/temp/mocca/mocca-beans-tests/Survey5/n=40000_15000/conc_pop=0.05/plain/snapshot.dat";
		final String fileGzip = "/home/ahypki/temp/mocca/mocca-beans-tests/Survey5/n=40000_15000/conc_pop=0.05/gzip/snapshot.dat.gz";
		
		BigDecimal sum1 = new BigDecimal(0.0);		
		for (String line : new BigFile(filePlain))
			sum1 = sum1.add(new BigDecimal(toDouble(split(line)[0])));
		LibsLogger.info(BigFilePartialTests.class, "Sum 1: " + sum1);
		
		BigDecimal sum2 = new BigDecimal(0.0);
		for (String line : new BigFilePartial(filePlain))
			sum2 = sum2.add(new BigDecimal(toDouble(split(line)[0])));
		LibsLogger.info(BigFilePartialTests.class, "Sum 2: " + sum2);
		assertTrue("Sum differs", sum2.equals(sum1));
		
		BigDecimal sum3 = new BigDecimal(0.0);
		for (int partsCount = 1; partsCount <= 10; partsCount++) {
			sum3 = new BigDecimal(0.0);
			for (int partNum = 0; partNum < partsCount; partNum++) {			
				for (String line : new BigFilePartial(filePlain).iterator(partNum, partsCount))
					sum3 = sum3.add(new BigDecimal(toDouble(split(line)[0])));
			}
			LibsLogger.info(BigFilePartialTests.class, "Sum 3: " + sum3 + ", parts count " + partsCount);
			assertTrue("Sum differs", sum3.equals(sum1));
		}
		
		BigDecimal sum4 = new BigDecimal(0.0);
		for (String line : new BigFilePartial(fileGzip))
			sum4 = sum4.add(new BigDecimal(toDouble(split(line)[0])));
		LibsLogger.info(BigFilePartialTests.class, "Sum 4: " + sum4);
		assertTrue("Sum differs", sum4.equals(sum1));
		
		BigDecimal sum5 = new BigDecimal(0.0);
		for (int partsCount = 1; partsCount <= 10; partsCount++) {
			sum5 = new BigDecimal(0.0);
			for (int partNum = 0; partNum < partsCount; partNum++) {			
				for (String line : new BigFilePartial(filePlain).iterator(partNum, partsCount))
					sum5 = sum5.add(new BigDecimal(toDouble(split(line)[0])));
			}
			LibsLogger.info(BigFilePartialTests.class, "Sum 5: " + sum5 + ", parts count " + partsCount);
			assertTrue("Sum differs", sum5.equals(sum1));
		}
	}
}
