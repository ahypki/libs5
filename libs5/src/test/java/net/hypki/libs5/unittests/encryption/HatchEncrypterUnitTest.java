package net.hypki.libs5.unittests.encryption;


import net.hypki.libs5.utils.encryption.EncryptionException;
import net.hypki.libs5.utils.encryption.HashUtils;
import net.hypki.libs5.utils.encryption.IncorrectMainPasswordException;
import net.hypki.libs5.utils.encryption.PasswordsKeeper;
import net.hypki.libs5.utils.encryption.StringEncrypter;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class HatchEncrypterUnitTest extends LibsTestCase {

	@Test
	public void testEncryptString() throws EncryptionException, IncorrectMainPasswordException {
		PasswordsKeeper passwordKeeper = new PasswordsKeeper("testpassword");
		StringEncrypter enc = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME, passwordKeeper.getPassword());
		
		System.out.println(HashUtils.getBlowfishHash(passwordKeeper.getPassword()));
		
		String test1 = "Test";
		String test1Enc = enc.encrypt(test1);
		System.out.println(test1Enc);

		String something1 = "AAABBB";
		String something1Enc = enc.encrypt(something1);
		System.out.println(something1Enc);
		
//		passwordKeeper.setMainPassword("^&*^*&^adminadminadminadminadminadminadminadmin");
		
		String test2 = "Test";
		String test2Enc = enc.encrypt(test2); 
		System.out.println(test2Enc);

		String something2 = "AAABBB";
		String something2Enc = enc.encrypt(something2);
		System.out.println(something2Enc);
		
		assertTrue("ERROR: encryption doesn't work", test1Enc.equals(test2Enc));
		assertTrue("ERROR: encryption doesn't work", something1Enc.equals(something2Enc));
	}
	
	@Test
	public void testLongTextEncryption() throws EncryptionException, IncorrectMainPasswordException {
		PasswordsKeeper passwordKeeper = new PasswordsKeeper("testpassword");
		StringEncrypter enc = new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME, passwordKeeper.getPassword());
		
		System.out.println(HashUtils.getBlowfishHash(passwordKeeper.getPassword()));
		
		String test100chars = "TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00";
		String test100Encrypted = enc.encrypt(test100chars);
		System.out.println(test100Encrypted);
		
		String verification100 = enc.decrypt(test100Encrypted);
		assertTrue(verification100.equals(test100chars));
		
		String test400chars = "TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00TestTest00";
		String test400Encrypted = enc.encrypt(test400chars);
		System.out.println(test400Encrypted);
		
		String verification400 = enc.decrypt(test400Encrypted);
		assertTrue(verification400.equals(test400chars));
	}

//	public void testDecryptString() {
//		fail("Not yet implemented");
//	}

}