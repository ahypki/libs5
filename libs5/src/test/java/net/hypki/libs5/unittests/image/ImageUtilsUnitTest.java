package net.hypki.libs5.unittests.image;

import java.awt.image.BufferedImage;
import java.io.IOException;

import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.image.ImageUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class ImageUtilsUnitTest extends LibsTestCase {
	
	@Test
	public void testResize() throws IOException, InterruptedException {
		BufferedImage img = ImageUtils.fileToBufferedImage("test-img.png");
		BufferedImage out = ImageUtils.getThumbnail(img, 279, 0, true);
		ImageUtils.bufferedImageToFile(out, "observatory_La_Palma-COPY.jpg");
		
		out = ImageUtils.cropImage(out, 50, 50, 50, 50);
		ImageUtils.bufferedImageToFile(out, "observatory_La_Palma-COPY-CROP.jpg");
		assertTrue(new FileExt("observatory_La_Palma-COPY.jpg").exists());
		
		out = ImageUtils.getThumbnailAndCrop(img, 620, 620, true, 620, 320);
		ImageUtils.bufferedImageToFile(out, "observatory_La_Palma-COPY-CROP2.jpg");
		assertTrue(new FileExt("observatory_La_Palma-COPY-CROP2.jpg").exists());
	}
}
