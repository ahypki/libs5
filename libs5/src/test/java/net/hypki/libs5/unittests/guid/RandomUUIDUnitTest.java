package net.hypki.libs5.unittests.guid;

import java.util.ArrayList;

import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class RandomUUIDUnitTest extends LibsTestCase {
	
	@Test
	public void testUUIDsUniquity() {
		long start = System.currentTimeMillis();
		ArrayList<String> uuids = new ArrayList<String>();
		
		for (int i = 0; i < 1000; i++) {
			String newUUID = new RandomGUID().toString();
			assertTrue("UUID is no unique for 1000 random draws", uuids.contains(newUUID) == false);
			uuids.add(newUUID);
		}
		
		System.out.println("new RandomGUID().toString() x 1000 times in " + 
				((System.currentTimeMillis() - start)/1000.0) + " seconds");
		
		System.out.println(new RandomGUID().toString());
		System.out.println(new RandomGUID().toString());
		
		RandomGUID guid = new RandomGUID();
		System.out.println(guid.toRawString());
		System.out.println(guid.toRawString());
		System.out.println(guid.toRawString().length());
	}
}
