package net.hypki.libs5.unittests.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DecimalFormatSymbols;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Vector;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.junit.Test;

public class TracsXml2Csv {

	@Test
	public void testXml2Csv() throws XMLStreamException, FactoryConfigurationError, IOException {

		final File DIR_ENTRADA = new File("./temp/tracks"), DIR_SALIDA = new File("./temp/tracks");
		final char CVS_SEPARADOR = new DecimalFormatSymbols().getPatternSeparator();

		FilenameFilter fnf;
		XMLStreamReader xml;
		LinkedHashMap<String, Integer> columnas;
		Vector<Vector<String>> filas;
		String s;
		Integer i;
		Vector<String> v;
		FileWriter fw;
		Enumeration<String> en;

		DIR_ENTRADA.mkdir();
		DIR_SALIDA.mkdir();

		fnf = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".xml");
			}
		};
		for (File f : DIR_ENTRADA.listFiles(fnf)) {
			// Procesa el XML
			columnas = new LinkedHashMap<String, Integer>();
			filas = new Vector<Vector<String>>();
			xml = XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(f));
			for (int ev = xml.next(); ev != XMLStreamReader.START_ELEMENT && ev != XMLStreamReader.END_DOCUMENT; ev = xml.next())
				;
			if (xml.getEventType() == XMLStreamReader.START_ELEMENT) {
				while (xml.nextTag() != XMLStreamReader.END_ELEMENT) {
					v = new Vector<String>();
					while (xml.nextTag() != XMLStreamReader.END_ELEMENT) {
						s = (xml.getAttributeCount() > 0 ? xml.getAttributeValue(0) : xml.getLocalName()).toUpperCase();
						i = columnas.get(s);
						if (i == null) {
							i = columnas.size();
							columnas.put(s, i);
						}
						v.setSize(Math.max(i + 1, v.size()));
						v.set(i, xml.getElementText());
					}
					filas.add(v);
				}
			}
			xml.close();
			// Genera el archivo CVS
			fw = new FileWriter(new File(DIR_SALIDA, f.getName().substring(0, f.getName().lastIndexOf('.')) + ".csv"));
			en = new Vector<String>(columnas.keySet()).elements();
			while (en.hasMoreElements()) {
				imprimir(fw, en.nextElement());
				if (en.hasMoreElements()) {
					imprimir(fw, CVS_SEPARADOR);
				}
			}
			imprimir(fw, "\r\n");
			for (Vector<String> vi : filas) {
				en = vi.elements();
				while (en.hasMoreElements()) {
					imprimir(fw, en.nextElement());
					if (en.hasMoreElements()) {
						imprimir(fw, CVS_SEPARADOR);
					}
				}
				imprimir(fw, "\r\n");
			}
			fw.close();
		}
		// }

		// }
	}

	private static void imprimir(FileWriter fw, char datos) throws IOException {
		imprimir(fw, String.valueOf(datos));
	}

	private static void imprimir(FileWriter fw, String datos) throws IOException {
		System.out.print(datos);
		fw.append(datos);
	}
}
