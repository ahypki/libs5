package net.hypki.libs5.unittests.json;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

public class Pets {
	@Expose
	private ArrayList<Pet> pets = new ArrayList<Pet>();
	
	public Pets() {
		
	}

	public void setPets(ArrayList<Pet> pets) {
		this.pets = pets;
	}

	public ArrayList<Pet> getPets() {
		return pets;
	}
}
