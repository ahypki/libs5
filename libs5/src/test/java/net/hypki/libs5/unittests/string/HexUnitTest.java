package net.hypki.libs5.unittests.string;

import java.io.IOException;
import java.util.HashMap;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.string.HexUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.apache.commons.codec.DecoderException;
import org.junit.Test;

public class HexUnitTest extends LibsTestCase {

	@Test
	public void hexDeser() throws IOException, ClassNotFoundException, DecoderException {
		String hex = HexUtils.toHex("abc");
		assertTrue(HexUtils.toObject(hex).equals("abc"));
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("c1", "abcd");
		map.put("c2", 2.0);
		hex = HexUtils.toHex(map);
		HashMap<String, Object> mapBack = (HashMap<String, Object>) HexUtils.toObject(hex);
		assertTrue(((Double) map.get("c2")).equals(((Double) mapBack.get("c2"))));
		
		Watch w = new Watch();
		for (int i = 0; i < 100000; i++) {
			map = new HashMap<>();
			map.put("c1", "abcd");
			map.put("c2", 2.0);
			hex = HexUtils.toHex(map);
			mapBack = (HashMap<String, Object>) HexUtils.toObject(hex);
			assertTrue(((Double) map.get("c2")).equals(((Double) mapBack.get("c2"))));
		}
		LibsLogger.debug(HexUnitTest.class, "100k hex conversions in ", w);
	}
}
