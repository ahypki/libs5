package net.hypki.libs5.unittests.collections;

import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class TripleMapUnitTest extends LibsTestCase {
	
	@Test
	public void testTripleMap() {
		TripleMap<String, Integer, Boolean> triple = new TripleMap<String, Integer, Boolean>();
		
		triple.put("jeden", 2, true);
		triple.put("dwa", 3, false);
		
		assertTrue(triple.get("jeden", 1) == null);
		assertTrue(triple.get("jeden", 2) == true);
		assertTrue(triple.get("jeden", 3) == null);
		
		assertTrue(triple.get("dwa", 1) == null);
		assertTrue(triple.get("dwa", 2) == null);
		assertTrue(triple.get("dwa", 3) == false);
		assertTrue(triple.get("dwa", 4) == null);
		
		assertTrue(triple.get("trzy", 3) == null);
	}
}
