package net.hypki.libs5.unittests.utils;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class NumberUtilsUnitTest extends LibsTestCase {

	@Test
	public void testConversion() {
		int max = 18000000;
		
		String dStr = "456789321.654";
		
		Watch watch = new Watch();
		for (int i = 0; i < max; i++) {
			double d = Double.parseDouble(dStr);
		}
		LibsLogger.debug(NumberUtilsUnitTest.class, "Double.parseDouble for " + max + " objects took " + watch);
	}
}