package net.hypki.libs5.unittests.io;

import java.io.File;
import java.util.ArrayList;

import jodd.io.findfile.ClasspathScanner;
import jodd.io.findfile.FilepathScanner;
import jodd.io.findfile.FindFile;
import jodd.io.findfile.WildcardFindFile;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Assert;
import org.junit.Test;

/**
 * More documentation about Jodd library one can find here: http://jodd.org/doc/findfile.html 
 * @author ahypki
 *
 */
public class FindFilesAndClassesJoddUnitTest extends LibsTestCase {
	
	@Test
	public void testFindFilesUsingWildcards() {
		FindFile ff = new WildcardFindFile("*")
			.recursive(true)
			.includeDirs(true)
			.searchPath(".");

		File f;
		while ((f = ff.nextFile()) != null) {
			if (f.isDirectory() == true) {
				System.out.println("Dir: " + f.getPath());
			} else {
				System.out.println("File: " + f.getPath());
			}
		}
	}
	
	@Test
	public void testFindFilesUsingScanner() {
		FilepathScanner fs = new FilepathScanner() {
			@Override
			protected void onFile(File file) {
				System.out.println("Do something with: " + file.getPath());
			}
		}.includeDirs(true).recursive(true).includeFiles(false);
		
		fs.scan(".");
	}
	
	@Test
	public void testFindClassesInJarUsingClasspathScanner() {
		String jarPath = "./target/libs5-0.1.0.jar";
		final ArrayList<String> foundClasses = new ArrayList<String>();
		
		System.out.println("Following classes were found in jar " + jarPath);
		
		ClasspathScanner cs = new ClasspathScanner() {
			@Override
			protected void onEntry(EntryData entryData) throws Exception {
//				InputStream inputStream = entryData.openInputStream();
//				byte[] bytes = StreamUtil.readAvailableBytes(inputStream);
//				System.out.println("Found class " + entryData.getName());
				foundClasses.add(entryData.getName());
			}
		};
		cs.scan(new File(jarPath));

		Assert.assertTrue(foundClasses.size() > 0);
	}
}
