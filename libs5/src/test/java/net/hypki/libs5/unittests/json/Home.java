package net.hypki.libs5.unittests.json;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.annotations.Expose;

public class Home {
	@Expose
	private String name = "palace";
	
	@Expose
	private Person person = new Person();
	
	@Expose
	private ArrayList<Person> persons = new ArrayList<Person>();
	
	@Expose
	private HashMap<Integer, Person> guests = new HashMap<Integer, Person>();
	
	@Expose
	private ArrayList<String> attrs = new ArrayList<String>();
	
	@Expose
	private SpecialList<Pet> pets = new SpecialList<Pet>();
	
	@Expose
	private SpecialList<Person> persons2 = new SpecialList<Person>();
	
//	@Expose
//	private PersonsList persons3 = new PersonsList();
	
	public Home() {
		
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("this: " + name + "\n");
		builder.append("person: " + person.toString() + "\n");
		for (Person in : persons) {
			builder.append("persons " + in.toString() + "\n");
		}
		return builder.toString();
	}

	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}

	public void setAttrs(ArrayList<String> attrs) {
		this.attrs = attrs;
	}

	public ArrayList<String> getAttrs() {
		return attrs;
	}

	public void setGuests(HashMap<Integer, Person> guests) {
		this.guests = guests;
	}

	public HashMap<Integer, Person> getGuests() {
		return guests;
	}

	public void setPets(SpecialList<Pet> pets) {
		this.pets = pets;
	}

	public SpecialList<Pet> getPets() {
		return pets;
	}

	public void setPersons2(SpecialList<Person> persons2) {
		this.persons2 = persons2;
	}

	public SpecialList<Person> getPersons2() {
		return persons2;
	}

//	public void setPersons3(PersonsList persons3) {
//		this.persons3 = persons3;
//	}
//
//	public PersonsList getPersons3() {
//		return persons3;
//	}
}
