package net.hypki.libs5.unittests.date;

import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class DateUtilsUnitTest extends LibsTestCase {

	@Test
	public void testParse() {
		assertTrue(DateUtils.parseDateYYYYMMDD("2010-09-08").getYear() == 2010);
		assertTrue(DateUtils.parseDateYYYYMMDD("2010-09-08").getMonth() == 9);
		assertTrue(DateUtils.parseDateYYYYMMDD("2010-09-08").getDay() == 8);
	}
	
	@Test
	public void testParseHour() {
		assertTrue(DateUtils.parseHHss("1:20").getHour() == 1);
		assertTrue(DateUtils.parseHHss("1:20").getMinutes() == 20);
	}
}
