package net.hypki.libs5.unittests.collections;

import java.util.UUID;

import org.junit.Test;

import net.hypki.libs5.utils.collections.LRUWeightedMap;
import net.hypki.libs5.utils.tests.LibsTestCase;

public class LRUWeightedMapUT extends LibsTestCase {

	@Test
	public void testLRU() {
		LRUWeightedMap lru = new LRUWeightedMap(100.0, null);
		
		for (int i = 0; i < 200000; i++) {
			lru.put(UUID.randomUUID().toString(), 1.0);
		}
		
		assertTrue(lru.size() == 90, "Expected 90, but size is " + lru.size());
	}
}
