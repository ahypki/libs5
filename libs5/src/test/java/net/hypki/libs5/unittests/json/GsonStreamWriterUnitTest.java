package net.hypki.libs5.unittests.json;

import java.io.IOException;

import net.hypki.libs5.utils.json.GsonStreamReader;
import net.hypki.libs5.utils.json.GsonStreamWriter;
import net.hypki.libs5.utils.json.JsonStreamReader;
import net.hypki.libs5.utils.json.JsonStreamWriter;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class GsonStreamWriterUnitTest extends LibsTestCase {

	@Test
	public void testWriterReader() throws IOException {
		String filename = "test-gson-writer";
		
		JsonStreamWriter writer = new GsonStreamWriter(filename);
		writer.beginObject();
		writer.append("name1", "value1");
		writer.append("name2", "value2");
		writer.endObject();
		writer.close();
		
		JsonStreamReader reader = new GsonStreamReader(filename);
		reader.beginObject();
		assertTrue(reader.nextName().equals("name1"));
		assertTrue(reader.nextString().equals("value1"));
		assertTrue(reader.nextName().equals("name2"));
		assertTrue(reader.nextString().equals("value2"));
		reader.endObject();
		reader.close();
	}
	
	@Test
	public void testWriterReaderNested() throws IOException {
		String filename = "test-gson-writer";
		
		JsonStreamWriter writer = new GsonStreamWriter(filename);
		
		writer.beginObject();
		writer.name("name1");
		
		writer.beginObject();
		writer.append("name2", "value2");
		writer.endObject();
		
		writer.endObject();
		writer.close();
		
		JsonStreamReader reader = new GsonStreamReader(filename);
		reader.beginObject();
		assertTrue(reader.nextName().equals("name1"));
		
		reader.beginObject();
		assertTrue(reader.nextName().equals("name2"));
		assertTrue(reader.nextString().equals("value2"));
		reader.endObject();
		
		reader.endObject();
		reader.close();
	}
}
