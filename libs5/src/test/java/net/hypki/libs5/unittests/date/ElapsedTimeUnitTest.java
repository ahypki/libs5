package net.hypki.libs5.unittests.date;

import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class ElapsedTimeUnitTest extends LibsTestCase {

	@Test
	public void testElapsedTime() {
		System.out.println(Watch.fromNow());
		System.out.println(Watch.from(new SimpleDate(2014, 3, 11).getTimeInMillis()));
		System.out.println(Watch.from(new SimpleDate(2014, 2, 11).getTimeInMillis()));
	}
}
