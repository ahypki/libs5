package net.hypki.libs5.unittests.url;

import net.hypki.libs5.utils.tests.LibsTestCase;
import net.hypki.libs5.utils.url.UrlUtilities;

import org.junit.Test;

public class UrlUtilitiesUnitTest extends LibsTestCase {

	@Test
	public void testIsUrlValid() {
		assertTrue(UrlUtilities.isUrlValid("http://www.onet.pl"));
		assertTrue(UrlUtilities.isUrlValid("www.onet.pl"));
		assertTrue(UrlUtilities.isUrlValid("onet.pl"));
		assertTrue(UrlUtilities.isUrlValid("t.pl"));
		
		assertTrue(UrlUtilities.isUrlValid("tpl") == false);
		assertTrue(UrlUtilities.isUrlValid("/tpl") == false);
	}
}
