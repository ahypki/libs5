package net.hypki.libs5.unittests.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import jodd.datetime.JDateTime;
import jodd.datetime.format.DefaultFormatter;
import jodd.datetime.format.JdtFormat;
import jodd.datetime.format.JdtFormatter;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


/**
 * Sample codes from: http://jodd.org/doc/jdatetime.html 
 * @author ahypki
 *
 */
public class JDateTimeUnitTest extends LibsTestCase {
	
	@Test
	public void testDateOfWeek() {
		assertTrue(new JDateTime(2012, 1, 3).getDayOfWeek() == 2);
		assertTrue(new JDateTime(2012, 1, 6).getDayOfWeek() == 5);
	}
	
	@Test
	public void testCreate() {
		JDateTime jdt = new JDateTime();		// set current date and time
		jdt = new JDateTime(2012, 12, 21);		// set 21st December 2012, midnight
		System.out.println(jdt.toString());
		System.out.println(jdt.toString("YYYY - MM - DD"));
		
		jdt = new JDateTime(System.currentTimeMillis());
		jdt = new JDateTime(2012, 12, 21, 11, 54, 22, 124);	// set 21st December 2012, 11:54:22.124
		jdt = new JDateTime("2012-12-21 11:54:22.124");		// -//-
		jdt = new JDateTime("12/21/2012", "MM/DD/YYYY");	// set 21st December 2012, midnight
	}
	
	@Test
	public void testThisWeekThisMonthEtc() {
		System.out.println("Playing with it....");
		
		JDateTime jdt = new JDateTime();
		System.out.println(jdt.toString("Today YYYY - MM - DD  DL"));
		System.out.println("Day of week: " + jdt.getDayOfWeek());
		
		jdt.subDay(jdt.getDayOfWeek() -1);
		System.out.println(jdt.toString("Beginning of this week YYYY - MM - DD  DL"));
		System.out.println("Day of week: " + jdt.getDayOfWeek());
		
		jdt.subDay(7);
		System.out.println(jdt.toString("Beginning of the last week YYYY - MM - DD  DL  -> ms " + jdt.getTimeInMillis()));
		System.out.println(DateUtils.thisWeekStartMs());
		System.out.println("Day of week: " + jdt.getDayOfWeek());
		
		
		jdt = new JDateTime();
		jdt.subMonth(1);
		jdt.subDay(jdt.getDayOfMonth() - 1);
		System.out.println(jdt.toString("Beginning of the last month YYYY - MM - DD  DL"));
		System.out.println(jdt.toString("Beginning of the last month YYYY - MM - DD  DL  -> ms " + jdt.getTimeInMillis()));
		System.out.println(DateUtils.lastMonthStartMs());
		
		System.out.println("That was fun!");
	}
	
	@Test
	public void testDateUtils() {
		JDateTime jdt = new JDateTime(DateUtils.thisWeekStartMs());
		System.out.println(jdt.toString("Beginning of this week YYYY - MM - DD  DL hh mm ss mss"));
		jdt = new JDateTime(DateUtils.thisMonthStartMs());
		System.out.println(jdt.toString("Beginning of this month YYYY - MM - DD  DL hh mm ss mss"));
		jdt = new JDateTime(DateUtils.lastMonthStartMs());
		System.out.println(jdt.toString("Beginning of the last month YYYY - MM - DD  DL hh mm ss mss"));
	}
	
	@Test
	public void changingDate() {
		JDateTime jdt = new JDateTime();			// set current date and time
		jdt.set(2012, 12, 21, 11, 54, 22, 124);		// set 21st December 2012, 11:54:22.124
		System.out.println(jdt);
		
		jdt.set(2012, 12, 21);						// set 21st December 2012, midnight
		jdt.setDate(2012, 12, 21);					// change date to 21st December 2012, do not change te time
		jdt.setCurrentTime();						// set current date and time
		jdt.setYear(1973);							// change the year
		jdt.setHour(22);							// change the hour
		jdt.setTime(18, 00, 12, 853);				// change just time, date remains the same	
		
		jdt.add(1, 2, 3, 4, 5, 6, 7);    // add 1 year, 2 months, 3 days, 4 hours...
		jdt.add(4, 2, 0);			// add 4 years and 2 months
		jdt.addMonth(-120);			// go back 120 months
		jdt.subYear(1);				// go back one year
		jdt.addHour(1234);			// add 1234 hours
//		Adding months is dubious because of variable month length. There are 
//		two ways how months may be considered during adding, what will be shown 
//		in following example: what is one month from 2003-01-31?
//
//		JDateTime has a monthFix flag that defines the month adding behavior. 
//		When this flag is off, months are approximated to 31 days. So the result 
//		is: 2003-01-31 + 0-1-0 = 2003-03-03.
//
//		When monthFix is on (what is the default), month length is ignored. In 
//		that case, adding one month will always give the very next month: 
//			2003-01-31 + 0-1-0 = 2003-02-28.
//
//		Since adding months is dubious because of variable month length it is a 
//		good practice to add days instead.
	}
	
	@Test
	public void formatting() {
//		pattern  	parsing?  	value
//		YYYY 	yes 	year
//		MM 	yes 	month
//		DD 	yes 	day of month
//		D 	  	day of week
//		MML 	  	month name long
//		MMS 	  	month name short
//		DL 	  	day of week name long
//		DS 	  	day of week name short
//		hh 	yes 	hour
//		mm 	yes 	minute
//		ss 	yes 	seconds
//		mss 	yes 	milliseconds
//		DDD 	  	day of year
//		WW 	  	week of year
//		WWW 	  	week of year with 'W' prefix
//		W 	  	week of month
//		E 	  	era (AD or BC)
//		TZL 	  	time zone name long
//		TZS 	  	time zone name short
		
		JDateTime jdt = new JDateTime(1975, 1, 1);
		jdt.toString();						// "1975-01-01 00:00:00.000"
		jdt.toString("YYYY.MM.DD");			// "1975.01.01"
		jdt.toString("MM: MML (MMS)");		// "01: January (Jan)"
		jdt.toString("DD is D: DL (DS)");	// "01 is 3: Wednesday (Wed)"
		jdt = new JDateTime(1968, 9, 30);
		jdt.toString("'''' is a sign, W is a week number and 'W' is a letter");
		// "' is a sign, 5 is a week number and W is a letter"

		jdt.parse("2003-11-24 23:18:38.173");
		jdt.parse("2003-11-23");				// 2003-11-23 00:00:00.000
		jdt.parse("01.01.1975", "DD.MM.YYYY");	// 1975-01-01
		jdt.parse("2001-01-31", "YYYY-MM-***");	// 2001-01-01, since day is not parsed

		jdt = new JDateTime();
		JdtFormatter fmt = new DefaultFormatter();
		fmt.convert(jdt, "YYYY-MM.DD");			// external conversion

		JdtFormat format = new JdtFormat(new DefaultFormatter(), "YYYY+DD+MM");
		jdt.toString(format);
		format.convert(jdt);

		DateFormat df = new SimpleDateFormat();
		df.format(jdt.convertToDate());			// date formatter
	}

	@Test
	public void timezone() {
		JDateTime jdt = new JDateTime();
		System.out.println("date in default zone: " + jdt);
		
		jdt.setTimeZone(TimeZone.getTimeZone("Warsaw"));
		System.out.println("date in Warsaw: " + jdt);

	}
	
	@Test
	public void playingWithTimestamps() {
		JDateTime jdt = new JDateTime(2009, 11, 11);
		System.out.println("ms for 2009-11-11: " + jdt.getTimeInMillis());
		
		jdt = new JDateTime(2009, 11, 11, 0, 0, 0, 123);
		System.out.println("ms for 2009-11-11 + 123 ms: " + jdt.getTimeInMillis());
		
		long ms = (2009 - 1970) * 365 * 24 * 60 * 60 * 1000;
		ms += (11 - 1) * (365 - 30) * 24 * 60 * 60 * 1000;
		ms += (11 - 1) * 24 * 60 * 60 * 1000;
		System.out.println("ms for 2009-11-11 + 123 ms: " + ms);
		
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		System.out.println(new JDateTime());
	}
}
