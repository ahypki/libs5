package net.hypki.libs5.unittests.string;

import java.util.List;

import org.junit.Test;

import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.tests.LibsTestCase;

public class SplitByRegexUnitTest extends LibsTestCase {

	@Test
	public void testSplitByRegex() {
		String s = "plot 'datasets=\"mocca\"    tables=\"system\"' u 2:3, \n"
				+ "'' u 4:5;";
		
		List<String> splits = StringUtilities.splitByRegex(s, ",[\\s]*");
		assertTrue(splits.size() == 2);
		assertTrue(splits.get(0).equals("plot 'datasets=\"mocca\"    tables=\"system\"' u 2:3"));
		assertTrue(splits.get(1).equals("'' u 4:5;"));
	}
}
