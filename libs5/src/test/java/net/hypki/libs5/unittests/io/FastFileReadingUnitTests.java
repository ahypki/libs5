package net.hypki.libs5.unittests.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import jodd.util.StringUtil;
import net.hypki.libs5.utils.file.BigFile;

import org.junit.Test;


public class FastFileReadingUnitTests {
	
	@Test
	public void testSumFirstColumn() throws IOException {
		String name = "./snapshot-0.dat";
		int bufferSize = 32000;
		long start, stop;
		double checksum;
		
		start = System.currentTimeMillis();
		checksum = testFileChannelFirstColumnSum(name, bufferSize);
		stop = System.currentTimeMillis();
		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");
	}

	public double testFileChannelFirstColumnSum(String name, int bufferSize) throws IOException {

		FileInputStream f = new FileInputStream(name);
		FileChannel ch = f.getChannel();
		MappedByteBuffer mb = ch.map(FileChannel.MapMode.READ_ONLY, 0L, ch.size());
		byte[] barray = new byte[bufferSize];
//		long checkSum = 0L;
		double sum = 0.0;
		int nGet, start, end, sw, white, col2 = 0;
		int [] n = new int[100];
		n[0] = 0;
		String [] parts = null;
		while (mb.hasRemaining()) {
			nGet = Math.min(mb.remaining(), bufferSize);
			mb.get(barray, 0, nGet);

			// System.out.println(new String(barray));
			start = 0;
			end = 0;
			for (int i = 0; i < nGet; i++) {
//				checkSum += barray[i];
				if (barray[i] == '\n') {
					end = i;
					
					sw = 0;
					white = 1;
					for (int j = start; j < end; j++) {
						if (barray[j] == ' ') {
							if (white == 0) {
								white = 1;
							}
						} else {
							if (white == 1) {
								white = 0;
								n[sw++] = j;
								
								if (sw == 3) {
									sum += Double.parseDouble(new String(barray, n[1], (n[2] - n[1])));
									break;
								}
							}
						}
						
					}
					
//					parts = StringUtil.splitc(new String(barray, start, (end - start)), ' ');
					start = end;
				}
			}
		}

		return sum;
	}

	@Test
	public void testPerformance() throws Exception {
		String name = "./snapshot-0.dat";
		int size = 8000;

		long start = System.currentTimeMillis();
		long checksum = testFileChannel(name, size);
		long stop = System.currentTimeMillis();
		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");

//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 16000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 32000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 64000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");
//
		start = System.currentTimeMillis();
		checksum = testFileChannel(name, 128000);
		stop = System.currentTimeMillis();
		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");
		start = System.currentTimeMillis();
		checksum = testFileChannel(name, 256000);
		stop = System.currentTimeMillis();
		System.out.println("FileChannel (checksum " + checksum + ")" + (stop - start) + " ms");
//		
//		
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, size);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannelByteBufferByteArrayGets (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 16000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannelByteBufferByteArrayGets (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 32000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannelByteBufferByteArrayGets (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 64000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannelByteBufferByteArrayGets (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 128000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannelByteBufferByteArrayGets (checksum " + checksum + ")" + (stop - start) + " ms");
//		
//		start = System.currentTimeMillis();
//		checksum = testFileChannel(name, 256000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannelByteBufferByteArrayGets (checksum " + checksum + ")" + (stop - start) + " ms");
//		
//		
//		start = System.currentTimeMillis();
//		checksum = testFileChannelBuffered(name, 16000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannel buffered (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testFileChannelBuffered(name, 32000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannel buffered (checksum " + checksum + ")" + (stop - start) + " ms");
//		
//
//		
//		start = System.currentTimeMillis();
//		checksum = testBufferedInputStream(name, 16000);
//		stop = System.currentTimeMillis();
//		System.out.println("BufferedInputStream (checksum " + checksum + ")" + (stop - start) + " ms");
//
//		start = System.currentTimeMillis();
//		checksum = testBufferedInputStream(name, 32000);
//		stop = System.currentTimeMillis();
//		System.out.println("BufferedInputStream (checksum " + checksum + ")" + (stop - start) + " ms");
//		
//		
		start = System.currentTimeMillis();
		checksum = testLineByLine(name, 0);
		stop = System.currentTimeMillis();
		System.out.println("Line by line (checksum " + checksum + ")" + (stop - start) + " ms");
//		
		start = System.currentTimeMillis();
		double sum = testBigFile(name, 0);
		stop = System.currentTimeMillis();
		System.out.println("BigFile (checksum " + sum + ")" + (stop - start) + " ms");
		
//		
//		start = System.currentTimeMillis();
//		float sum1Column = testFileChannelByteBufferByteArrayGetsWithSplittingToLines(name, 16000);
//		stop = System.currentTimeMillis();
//		System.out.println("FileChannel line by line (checksum " + sum1Column + ")" + (stop - start) + " ms");
	}
	
	public double testBigFile(String name, int bufferSize) throws Exception {
		BigFile file = new BigFile(name);

		double sum = 0.0f;
		for (String line : file) {
			String [] parts = StringUtil.splitc(line, " ");
			if (parts[0].length() != 0)
				sum += Float.parseFloat(parts[0]);
			else if (parts[1].length() != 0)
				sum += Float.parseFloat(parts[1]);
			else
				throw new RuntimeException();
				
//			System.out.println(line);
		}
		return sum;
	}
	
	public long testLineByLine(String name, int bufferSize) throws IOException {
		FileInputStream fstream = new FileInputStream(name);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		long checkSum = 0;
		float sum = 0.0f;
		byte [] bytes;
		while ((strLine = br.readLine()) != null) {

			String [] parts = StringUtil.splitc(strLine, " ");
			sum += Float.parseFloat(parts[1]);
		}
		in.close();
		return checkSum;
	}
	
	public float getColumnAsFloat(byte[] bytes, int bytesStart, int column) {
		boolean white = true;
		int index = 0;
		int start = 0;
		for (int i = bytesStart; i < bytes.length; i++) {
			if (bytes[i] == ' ' || bytes[i] == '\n') {
				if (index == column)
					return Float.parseFloat(new String(bytes, start, i - start));
			} else {
				white = false;
				index++;
				start = i;
			}
		}
		return 0.0f;
	}
	
	public float testFileChannelByteBufferByteArrayGetsWithSplittingToLines(String name, int bufferSize) throws IOException {
		FileInputStream f = new FileInputStream(name);
		FileChannel ch = f.getChannel();
		ByteBuffer bb = ByteBuffer.allocateDirect(bufferSize * 100);
		byte[] barray = new byte[bufferSize];
		float checkSum = 0.0f;
		int nRead, nGet;
		String line = null;
		String rest = null;
		int lineStart = 0;
		while ((nRead = ch.read(bb)) != -1) {
			if (nRead == 0)
				continue;
			bb.position(0);
			bb.limit(nRead);
			while (bb.hasRemaining()) {
				lineStart = 0;
				
				nGet = Math.min(bb.remaining(), bufferSize);
				bb.get(barray, 0, nGet);
				
//				line = new String(barray);
				
//				System.out.println(line);
				
				for (int i = 0; i < nGet; i++) {
					if (barray[i] == '\n') {
						checkSum += getColumnAsFloat(barray, lineStart, 1);
						
//						line = new String(barray, lineStart, i - lineStart);
//						System.out.println(rest != null ? rest + line : line);
						lineStart = i + 1;
						rest = null;
						
					}
				}
				
//				rest = new String(barray, lineStart, nGet - lineStart);
			}
			bb.clear();
		}
		return checkSum;
	}
	
	public long testFileChannelByteBufferByteArrayGets(String name, int bufferSize) throws IOException {
		FileInputStream f = new FileInputStream(name);
		FileChannel ch = f.getChannel();
		ByteBuffer bb = ByteBuffer.allocateDirect(bufferSize * 100);
		byte[] barray = new byte[bufferSize];
		long checkSum = 0L;
		int nRead, nGet;
		while ((nRead = ch.read(bb)) != -1) {
			if (nRead == 0)
				continue;
			bb.position(0);
			bb.limit(nRead);
			while (bb.hasRemaining()) {
				nGet = Math.min(bb.remaining(), bufferSize);
				bb.get(barray, 0, nGet);
				for (int i = 0; i < nGet; i++)
					checkSum += barray[i];
			}
			bb.clear();
		}
		return checkSum;
	}
	
	public long testBufferedInputStream(String name, int bufferSize) throws IOException {
		BufferedInputStream f = new BufferedInputStream(new FileInputStream(
				name));
		byte[] barray = new byte[bufferSize];
		long checkSum = 0L;
		int nRead;
		while ((nRead = f.read(barray, 0, bufferSize)) != -1)
			for (int i = 0; i < nRead; i++)
				checkSum += barray[i];
		return checkSum;
	}
	
	public long testFileChannel(String name, int bufferSize) throws IOException {

		FileInputStream f = new FileInputStream(name);
		FileChannel ch = f.getChannel();
		MappedByteBuffer mb = ch.map(FileChannel.MapMode.READ_ONLY, 0L,
				ch.size());
		byte[] barray = new byte[bufferSize];
		long checkSum = 0L;
		int nGet;
		while (mb.hasRemaining()) {
			nGet = Math.min(mb.remaining(), bufferSize);
			mb.get(barray, 0, nGet);

			// System.out.println(new String(barray));

			for (int i = 0; i < nGet; i++) {
				checkSum += barray[i];
			}
		}

		return checkSum;
	}

	public long testFileChannelBuffered(String name, int bufferSize) throws IOException {
		FileInputStream f = new FileInputStream(name);
		FileChannel ch = f.getChannel();
		MappedByteBuffer mb = ch.map(FileChannel.MapMode.READ_ONLY, 0L, ch.size());
		long checkSum = 0L;
		while (mb.hasRemaining())
			checkSum += mb.get();
		return checkSum;
	}
}
