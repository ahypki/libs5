package net.hypki.libs5.unittests.string;

import static net.hypki.libs5.utils.string.StringUtilities.lastWord;
import static net.hypki.libs5.utils.string.StringUtilities.removeLastWord;
import jodd.util.StringUtil;
import jodd.util.Wildcard;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class StringUtilitiesUnitTest extends LibsTestCase {
	
	@Test
	public void testReplace() {
		assertTrue("ab cd".replaceAll("[^\\w\\d]+", "\\_").equals("ab_cd"));
		assertTrue("ab cd#90".replaceAll("[^\\w\\d]+", "\\_").equals("ab_cd_90"));
	}
	
	@Test
	public void testSplitPerformance() {
		int max = 100000;
		int count = 200;
		
		String numStr = " 456789321.654 ";
		String toSplit = "";
		for (int i = 0; i < count; i++) {
			toSplit += numStr;
		}
		
		long start = System.currentTimeMillis();
		for (int i = 0; i < max; i++) {
			String [] parts = StringUtilities.split(toSplit);
			assertTrue(parts.length == count);
		}
		LibsLogger.debug(StringUtilitiesUnitTest.class, "Split performance [ms sec] " + (System.currentTimeMillis() - start));
	}
	
	@Test
	public void testSubstringMax() {
		assertTrue(StringUtilities.substringMax("0123456789", 0, 10).equals("0123456789"));
		assertTrue(StringUtilities.substringMax("0123456789", 1, 10).equals("123456789"));
		assertTrue(StringUtilities.substringMax("0123456789", 2, 10).equals("23456789"));
		assertTrue(StringUtilities.substringMax("0123456789", 9, 10).equals("9"), StringUtilities.substringMax("0123456789", 9, 10));
		assertTrue(StringUtilities.substringMax("0123456789", 10, 10).equals(""));
	}

	@Test
	public void testWithoutLastWord() {
		assertTrue(removeLastWord("one").equals(""));
		assertTrue(removeLastWord("one ").equals(""));
		
		assertTrue(removeLastWord("one two").equals("one"));
		assertTrue(removeLastWord("one two ").equals("one"));
		assertTrue(removeLastWord("one two\t").equals("one"));
		
		assertTrue(removeLastWord("one łół").equals("one"));
		assertTrue(removeLastWord("one ążśźćęłńó ").equals("one"));
	}
	
	@Test
	public void testLastWord() {
		assertTrue(lastWord("one").equals("one"));
		assertTrue(lastWord("one ").equals("one"));
		
		assertTrue(lastWord("one two").equals("two"));
		assertTrue(lastWord("one two ").equals("two"));
		assertTrue(lastWord("one two\t").equals("two"));
	}
	
	@Test
	public void testStringToBytesASCII() {
		String temp = "one two three";
		
		byte [] bytes = StringUtilities.stringToBytesASCII(temp);
		assertTrue(temp.equals(new String(bytes)));
		
		bytes = StringUtilities.stringToBytesUTFCustom(temp);
		assertTrue(temp.equals(StringUtilities.bytesToStringUTFCustom(bytes)));
	}
	
	@Test
	public void testCommonPrefix() {
		String[] strings = new String[] {"switch worskspace", "switch user"};
		
		assertTrue(StringUtilities.findCommonPrefix(strings).equals("switch "), StringUtilities.findCommonPrefix(strings));
	}
	
	@Test
	public void testIsAlphanumerical() {
		assertTrue(StringUtilities.isAlphanumerical("abc"));
		assertTrue(StringUtilities.isAlphanumerical("abcPOI"));
		assertTrue(StringUtilities.isAlphanumerical("abcPOI987"));
		assertTrue(!StringUtilities.isAlphanumerical("abc def"));
		assertTrue(!StringUtilities.isAlphanumerical("ab#cdef"));
		assertTrue(StringUtilities.isAlphanumerical(""));
		assertTrue(!StringUtilities.isAlphanumerical(" "));
	}
	
	@Test
	public void wildcardMatches() {
		Wildcard.match("CfgOptions.class", "*C*g*cl*");     	// true   
		Wildcard.match("CfgOptions.class", "*g*c**s");      	// true!   
		Wildcard.match("CfgOptions.class", "??gOpti*c?ass");    // true   
		Wildcard.match("CfgOpti*class", "*gOpti\\*class");  	// true   
		Wildcard.match("CfgOptions.class", "C*ti*c?a?*");   	// true
	}
	
	@Test
	public void usernameRegEx() {
		String usernameRegex = "^[a-z0-9_-]{3,16}$";
		
		assertTrue("abc".matches(usernameRegex));
		assertTrue("_abc".matches(usernameRegex));
		assertTrue("abc_".matches(usernameRegex));
		assertTrue("abc_def-gh".matches(usernameRegex));
		assertTrue(!"ab".matches(usernameRegex));
		assertTrue(!"".matches(usernameRegex));
	}
	
	@Test
	public void emailRegEx() {
		String emailRegex = "^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$";
		
		assertTrue("abc@two.pl".matches(emailRegex));
		assertTrue(!"abc@two.PL".matches(emailRegex));
		assertTrue(!"abc@two.toLongDomain".matches(emailRegex));
	}
	
	@Test
	public void urlRegEx() {
		String urlRegex = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$";
		
		assertTrue("http://net.tutsplus.com/about".matches(urlRegex));
		assertTrue(!"http://google.com/some/file!.html".matches(urlRegex));
	}
	
	@Test
	public void testRandomPronounceableWords() {
		System.out.println(createRandomPronounceableWord());
		System.out.println(createRandomPronounceableWord());
		System.out.println(createRandomPronounceableWord());
		
		assertTrue(createRandomPronounceableWord() != null);
		assertTrue(createRandomPronounceableWord() != null);
	}
	
	@Test
	public void testJoddStringUtil() {
		System.out.println("Jodd testing...");
		
		String path = "/one/two";
		System.out.println(StringUtilities.toString(StringUtil.split(path, "/")));
		System.out.println(StringUtilities.toString(StringUtil.splitc(path, "/")));
		System.out.println(StringUtilities.toString(StringUtil.splitc(path, '/')));
	}
}
