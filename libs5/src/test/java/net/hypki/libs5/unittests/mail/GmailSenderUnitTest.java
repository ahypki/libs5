package net.hypki.libs5.unittests.mail;

import java.io.IOException;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import jodd.datetime.JDateTime;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.network.NetworkChecker;
import net.hypki.libs5.utils.network.SimpleGMailSender;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Ignore;
import org.junit.Test;


@Ignore
public class GmailSenderUnitTest extends LibsTestCase {
	

	@Test(expected=AuthenticationFailedException.class)
	public void testSend() throws AddressException, MessagingException, IOException {
		
		if (NetworkChecker.isOnline() == true) {
			LibsLogger.debug("Computer is online. Trying to send an email");
			
			String username = FileUtils.readString(new FileExt("$HOME/.gmail-user"));
			String email 	= FileUtils.readString(new FileExt("$HOME/.gmail-user"));
			String pass 	= FileUtils.readString(new FileExt("$HOME/.gmail-pass"));
			
			String subject = LibsTestCase.createRandomWord();
			SimpleGMailSender.sendMail(username, pass, email, subject, new JDateTime().toString() + " - UnitTest", true);
			
			LibsLogger.debug("Mail with subject: '" + subject + "' sent successfully");
		}
	}

	
}
