package net.hypki.libs5.unittests.json;

import com.google.gson.annotations.Expose;

public class Pet {
	@Expose
	private String name = "dog";
	
	public Pet() {
		
	}
	
	public Pet(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
