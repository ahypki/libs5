package net.hypki.libs5.unittests.sha;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.sha.MD5Checksum;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class MD5ChecksumUnitTest extends LibsTestCase {

	@Test
	public void testMD5() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MD5Checksum crc = new MD5Checksum();
		
//		System.out.println(crc.update(1.0).update(2.0).getChecksum());
		
		String a = crc.update("aabb").getChecksum();
		String b = crc.update("aa").update("bb").getChecksum();
		assertTrue(a.equals(b));
		
		a = crc.update("aabb").update(5.0).update(3L).getChecksum();
		b = crc.update("aa").update("bb").update(5.0).update(3L).getChecksum();
		assertTrue(a.equals(b));
		
		a = crc.update("").getChecksum();
		b = crc.update("").update("").getChecksum();
		assertTrue(a.equals(b));
		LibsLogger.debug(MD5ChecksumUnitTest.class, "a == " + a);
	}
}
