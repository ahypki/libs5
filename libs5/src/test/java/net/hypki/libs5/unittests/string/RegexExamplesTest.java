package net.hypki.libs5.unittests.string;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class RegexExamplesTest extends LibsTestCase {
	
	@Test
	public void testGroups() {
		List<String> groups = RegexUtils.allGroups("([\\w]+[\\s]*(?:[\\w]*))", "abc def56", Pattern.CASE_INSENSITIVE);
		
		for (String group : groups) {
			LibsLogger.debug(RegexExamplesTest.class, "Group: ", group);
		}
		LibsLogger.debug(RegexExamplesTest.class, "Groups finished");
	}
	
	@Test
	public void testDoubles() {
		assertTrue(RegexUtils.isDouble("1.1"));
		assertTrue(RegexUtils.isDouble("1.1e2"));
		assertTrue(RegexUtils.isDouble("1.1e-2"));
	}
	
	@Test
	public void testUsernames() {
//		Description:
//		We begin by telling the parser to find the beginning of the string (^), 
//		followed by any lowercase letter (a-z), number (0-9), an underscore, or a 
//		hyphen. Next, {3,16} makes sure that are at least 3 of those characters, 
//		but no more than 16. Finally, we want the end of the string ($).
		String usernameRegex = "^[a-zA-Z0-9_-]{3,16}$";
		
		assertTrue("abc".matches(usernameRegex));
		assertTrue("ABC".matches(usernameRegex));
		assertTrue("abc12".matches(usernameRegex));
		assertTrue("23abc12".matches(usernameRegex));
		
		assertTrue(!"ab".matches(usernameRegex));
		
	}
	
	@Test
	public void testReplaceRegexWithGroups() {
		String input = "2:($3+2.0)";
		
		Pattern pattern = Pattern.compile("\\$([\\d]+)");
	    Matcher matcher = pattern.matcher(input);
	    String result = matcher.replaceAll("\\#{var$1}");
	    System.out.println(result);
	    
	    assertTrue(result.equals("2:(#{var3}+2.0)"));
	}
}
