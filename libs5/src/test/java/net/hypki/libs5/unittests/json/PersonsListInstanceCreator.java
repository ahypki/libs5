package net.hypki.libs5.unittests.json;

import java.lang.reflect.Type;

import com.google.gson.InstanceCreator;

public class PersonsListInstanceCreator implements InstanceCreator<PersonsList> {

	public PersonsList createInstance(Type type) {
		// No need to use a parameterized list since the actual instance will
		// have the raw type anyway.
		return new PersonsList();
	}

}