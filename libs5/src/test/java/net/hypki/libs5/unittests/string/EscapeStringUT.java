package net.hypki.libs5.unittests.string;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.EscapeString;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class EscapeStringUT extends LibsTestCase {

	@Test
	public void testEscapeString() {
		EscapeString es = new EscapeString("one two");
		
		assertTrue(es.getInputString().equals("one two"));
		assertTrue(es.getEscapedText().equals("one two"));
	}
	
	@Test
	public void testEscapeString2() {
		EscapeString es = new EscapeString("one 'two' three");
		
		es
			.escape('\'');
		
//		assertTrue(es.getMainText().length() == "one  two".length() + 40);
		LibsLogger.info(EscapeStringUT.class, "Escaped ", es.getEscapedText());
	}
	
	@Test
	public void testEscapeString3() {
		EscapeString es = new EscapeString("one 'TTT\\'WWWOOO' three");
		
		es
			.escape("\\'");
		
//		assertTrue(es.getMainText().length() == "one  two".length() + 40);
		LibsLogger.info(EscapeStringUT.class, "Escaped ", es.getEscapedText());
		
		LibsLogger.info(EscapeStringUT.class, "Applied ", es.deescapeString(es.getEscapedText().replace("three", "__THREE__")));
	}
}
