package net.hypki.libs5.unittests.encryption;

import net.hypki.libs5.utils.encryption.HashUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class HashUtilsUnitTest extends LibsTestCase {
//	@Test
//	public void testMD5Hash() {
//		String password = "some secret password";
//		String password2 = "some another secret password";
//		
//		String passwordHashed = HashUtils.getHashMD5(password);
//		String passwordHashedCopy = HashUtils.getHashMD5(password);
//		String password2Hashed = HashUtils.getHashMD5(password2);
//		String password2HashedCopy = HashUtils.getHashMD5(password2);
//		
//		assertTrue(passwordHashed.equals(passwordHashedCopy));
//		assertTrue(password2Hashed.equals(password2HashedCopy));
//		
//		assertTrue(passwordHashed.equals(password2Hashed) == false);
//	}
//	
//	@Test
//	public void testSHA1Hash() {
//		String password = "some secret password";
//		String password2 = "some another secret password";
//		
//		String passwordHashed = HashUtils.getHashSHA1(password);
//		String passwordHashedCopy = HashUtils.getHashSHA1(password);
//		String password2Hashed = HashUtils.getHashSHA1(password2);
//		String password2HashedCopy = HashUtils.getHashSHA1(password2);
//		
//		assertTrue(passwordHashed.equals(passwordHashedCopy));
//		assertTrue(password2Hashed.equals(password2HashedCopy));
//		
//		assertTrue(passwordHashed.equals(password2Hashed) == false);
//	}
	
	@Test
	public void testBlowfishHash() {
		String password = "some secret password";
		String password2 = "some another secret password";
		
		String passwordHashed = HashUtils.getBlowfishHash(password);
		String passwordHashedCopy = HashUtils.getBlowfishHash(password);
		String password2Hashed = HashUtils.getBlowfishHash(password2);
		String password2HashedCopy = HashUtils.getBlowfishHash(password2);
		
		assertTrue(HashUtils.checkBlowfishHash(password, passwordHashedCopy));
		assertTrue(HashUtils.checkBlowfishHash(password2, password2HashedCopy));
		
		assertTrue(passwordHashed.equals(password2Hashed) == false);
	}
}
