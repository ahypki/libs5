package net.hypki.libs5.unittests.threads;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class ThreadsUniTest extends LibsTestCase {

	@Test
	public void testThreads() {
		ExecutorService executorService = Executors.newFixedThreadPool(10);

		CountDownLatch latch = new CountDownLatch(5);
		
		for (int i = 0; i < 5; i++) {
			executorService.execute(new Runnable() {
			    public void run() {
			    	int ms = RandomUtils.nextInt(3000, 5000);
			    	LibsLogger.info(ThreadsUniTest.class, "Waiting ", ms, " ms...");
			    	try {
				        TimeUnit.MILLISECONDS.sleep(ms);
				    } catch (InterruptedException e) {
				        e.printStackTrace();
				    }
			    	latch.countDown();
			    	LibsLogger.info(ThreadsUniTest.class, "Task finished");
			    }
			});
		}
		
		// wait for the latch to be decremented by the two remaining threads
		try {
			latch.await();
			executorService.shutdown();
			LibsLogger.info(ThreadsUniTest.class, "Executor finished");
		} catch (InterruptedException e) {
			LibsLogger.error(ThreadsUniTest.class, "Interruption failed", e);
		}
	}
}
