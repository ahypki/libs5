package net.hypki.libs5.unittests.file;

import java.io.File;
import java.io.FileFilter;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class LazyFileIteratorTest extends LibsTestCase {

	@Test
	public void testIterator() {
		LazyFileIterator fIter = new LazyFileIterator(new File("."), true, true, true, new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				if (pathname.isDirectory() && pathname.getName().equals("target"))
					return false;
				
				if (pathname.isDirectory() && pathname.getName().equals("bin"))
					return false;
				
				return true;
			}
		});
		
		for (File file : fIter) {
			LibsLogger.debug(LazyFileIteratorTest.class, file.getAbsolutePath());
		}
	}
	
	
}
