package net.hypki.libs5.unittests.string;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.LoremIpsum;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class Base64UnitTest extends LibsTestCase {

	@Test
	public void testToString2() throws IOException {
		String tmp = "jeden dwa";
		System.out.println(tmp + " in base64 is: " + Base64Utils.encode(tmp));
	}
	
	@Test
	public void testToString() throws IOException {
		long start = System.currentTimeMillis();
		String sha256 = new FileExt("test-files/observatory_La_Palma.jpg").getSHA256();
		
		LibsLogger.debug("Calculating 20 hashes of test file...");
		for (int i = 0; i < 20; i++) {
			String sha256OnceAgain = new FileExt("test-files/observatory_La_Palma.jpg").getSHA256();
			assertTrue(sha256.equals(sha256OnceAgain));
		}
		System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");
	}
	
	@Test
	public void testEscapingCharactersInCookieValue() throws UnsupportedEncodingException {
		String toEscape = "test@email.pl:65eae4557ff2fb8c403ad3cf0cc161c8łążśćęð“ó";
		String escaped = jodd.util.Base64.encodeToString(toEscape);
		String reversedUTF8 = new String(jodd.util.Base64.decode(escaped), "UTF-8");
		String reversedDefault = new String(jodd.util.Base64.decode(escaped), "UTF-8");
		
		assertTrue(reversedDefault.equals(toEscape));
		assertTrue(reversedUTF8.equals(toEscape));
	}
	
	@Test
	public void testEscaping2() throws UnsupportedEncodingException {
		String toEscape = LoremIpsum.createSampleString(10000);
		String escaped = jodd.util.Base64.encodeToString(toEscape);

		LibsLogger.info(Base64UnitTest.class, "toEscaoe length " + toEscape.length());
		LibsLogger.info(Base64UnitTest.class, "escaped " + escaped.length());
	}
}
