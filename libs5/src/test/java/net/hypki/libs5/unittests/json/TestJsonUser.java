package net.hypki.libs5.unittests.json;

import org.junit.Ignore;

@Ignore
public class TestJsonUser {
	private Gender _gender;
	private boolean _isVerified;
	private byte[] _userImage;
	private String _first, _last;
	
	public enum Gender {
		MALE, FEMALE
	}

	public String getFirst() {
		return _first;
	}

	public String getLast() {
		return _last;
	}

	public void setFirst(String s) {
		_first = s;
	}

	public void setLast(String s) {
		_last = s;
	}

	public boolean isVerified() {
		return _isVerified;
	}

	public Gender getGender() {
		return _gender;
	}

	public byte[] getUserImage() {
		return _userImage;
	}

	public void setVerified(boolean b) {
		_isVerified = b;
	}

	public void setGender(Gender g) {
		_gender = g;
	}

	public void setUserImage(byte[] b) {
		_userImage = b;
	}

}
