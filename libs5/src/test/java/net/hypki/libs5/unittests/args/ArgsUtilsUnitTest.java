package net.hypki.libs5.unittests.args;

import static net.hypki.libs5.utils.args.ArgsUtils.getString;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class ArgsUtilsUnitTest extends LibsTestCase {
	
	@Test
	public void testArgsWithSpaces() {
		String [] args = new String[] {"--one", "two", "--three", "four", "five"};
		
		assertTrue(getString(args, "one").equals("two"));
		assertTrue(getString(args, "three").equals("four five"));
	}

	@Test
	public void testArgs() {
		String [] args = new String[] {"hist1D", "--w", "0.1", "--h1", "\"one", "two\"", "--h2", "\"one", "two", "\\\">\\\"", "three\""};
		
		String tmp = getString(args, "h1");
		assertTrue(tmp.equals("\"one two\""), tmp);
		
		tmp = getString(args, "h2");
		assertTrue(tmp.equals("\"one two \\\">\\\" three\""), tmp);
	}
}
