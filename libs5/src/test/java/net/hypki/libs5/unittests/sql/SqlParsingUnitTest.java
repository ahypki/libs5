package net.hypki.libs5.unittests.sql;

import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

import com.akiban.sql.StandardException;
import com.akiban.sql.parser.SQLParser;
import com.akiban.sql.parser.StatementNode;

public class SqlParsingUnitTest extends LibsTestCase {

	@Test
	public void testParse() throws StandardException {
		SQLParser parser = new SQLParser();
        StatementNode q = parser.parseStatement("select t.time from ds.tab as t");
	}
	
	@Test
	public void testParsers() throws StandardException {
//		Select select = SqlUtils.parse("select m1 from tab");
//		assertTrue(select.getColumns().get(0).getName().equals("m1"));
//		assertTrue(select.getColumns().get(0).getTable().equals("tab"));
//		
//		select = SqlUtils.parse("select tab.m1 from tab");
//		assertTrue(select.getColumns().get(0).getName().equals("m1"));
//		assertTrue(select.getColumns().get(0).getTable().getOriginalName().equals("tab"));
//		
//		select = SqlUtils.parse("select tab.m1, tab2.m2 from tab, tab2");
//		assertTrue(select.getColumns().get(0).getName().equals("m1"));
//		assertTrue(select.getColumns().get(0).getTable().getOriginalName().equals("tab"));
//		assertTrue(select.getColumns().get(1).getName().equals("m2"));
//		assertTrue(select.getColumns().get(1).getTable().getOriginalName().equals("tab2"));
//		
//		select = SqlUtils.parse("select tab.m1, tab2.m2, m3 from tab, tab2");
//		assertTrue(select.exist("tab", "m1"));
//		assertTrue(select.exist("tab2", "m2"));
//		assertTrue(select.exist("tab", "m3"));
//		assertTrue(select.exist("tab2", "m3"));
//		
//		select = SqlUtils.parse("SELECT m1 FROM tab WHERE m1 > 0.5");
//		info(select);
//		assertTrue(select.exist("tab", "m1"));
//		
//		select = SqlUtils.parse("SELECT m1 FROM tab, tab2 WHERE tab3.m1 > 0.5 AND (tab1.m1 > 0.6 OR tab1.m2 < 0.2)");
//		info(select);
//		assertTrue(select.exist("tab", "m1"));
		
	}
}
