package net.hypki.libs5.unittests.json;

import java.util.ArrayList;

import net.hypki.libs5.utils.json.GsonArrayListCreator;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUnitTest extends LibsTestCase {
	
	@Test
	public void testInnerOuterClassSerialization() {
		Home obj = new Home();
		
		Gson gson = new GsonBuilder()
			.registerTypeAdapter(ArrayList.class, new GsonArrayListPersonInstanceCreator())
			.registerTypeAdapter(SpecialList.class, new GsonSpecialListInstanceCreator())
			.excludeFieldsWithoutExposeAnnotation()
			.create();
		String json = gson.toJson(obj);
		System.out.println(json);
		
		Home fromString = gson.fromJson(json, Home.class);
		System.out.println(fromString);
		assertTrue(fromString.getPersons().size() == 0);
	}
	
	@Test
	public void testInnerOuterClassSerializationWithArraylist() {
		
		Gson gson = new GsonBuilder()
			.registerTypeAdapter(ArrayList.class, new GsonArrayListPersonInstanceCreator())
			.registerTypeAdapter(SpecialList.class, new GsonSpecialListInstanceCreator())
			.registerTypeAdapter(PersonsList.class, new PersonsListInstanceCreator())
			.excludeFieldsWithoutExposeAnnotation()
			.create();
		
		Home obj = new Home();
		obj.getPersons().add(new Person("jeden"));
		obj.getPersons().add(new Person("dwa"));
		String json = gson.toJson(obj);
		System.out.println(json);
		
		Home fromString = gson.fromJson(json, Home.class);
		System.out.println(fromString);
		assertTrue(fromString.getPersons().get(0).getName().equals("jeden"));
		assertTrue(fromString.getPersons().get(1).getName().equals("dwa"));
		
		
		obj.getAttrs().add("jeden attr");
		obj.getAttrs().add("dwa attr");
		json = gson.toJson(obj);
		System.out.println(json);
		
		fromString = gson.fromJson(json, Home.class);
		System.out.println(fromString);
		assertTrue(fromString.getPersons().get(0).getName().equals("jeden"));
		assertTrue(fromString.getPersons().get(1).getName().equals("dwa"));
		assertTrue(fromString.getAttrs().get(0).equals("jeden attr"));
		assertTrue(fromString.getAttrs().get(1).equals("dwa attr"));
		
		
		obj.getGuests().put(1, new Person("jeden guest"));
		obj.getGuests().put(2, new Person("dwa guest"));
		json = gson.toJson(obj);
		System.out.println(json);
		
		fromString = gson.fromJson(json, Home.class);
		System.out.println(fromString);
		assertTrue(fromString.getPersons().get(0).getName().equals("jeden"));
		assertTrue(fromString.getPersons().get(1).getName().equals("dwa"));
		assertTrue(fromString.getAttrs().get(0).equals("jeden attr"));
		assertTrue(fromString.getAttrs().get(1).equals("dwa attr"));
		assertTrue(fromString.getGuests().get(1).getName().equals("jeden guest"));
		assertTrue(fromString.getGuests().get(2).getName().equals("dwa guest"));
		
		
		obj.getPets().add(new Pet("szarik"));
		obj.getPets().add(new Pet("burek"));
		json = gson.toJson(obj);
		System.out.println(json);
		
		fromString = gson.fromJson(json, Home.class);
		System.out.println(fromString);
		assertTrue(fromString.getPersons().get(0).getName().equals("jeden"));
		assertTrue(fromString.getPersons().get(1).getName().equals("dwa"));
		assertTrue(fromString.getAttrs().get(0).equals("jeden attr"));
		assertTrue(fromString.getAttrs().get(1).equals("dwa attr"));
		assertTrue(fromString.getGuests().get(1).getName().equals("jeden guest"));
		assertTrue(fromString.getGuests().get(2).getName().equals("dwa guest"));
		assertTrue(fromString.getPets().get(0).getName().equals("szarik"));
		assertTrue(fromString.getPets().get(1).getName().equals("burek"));
		
		
		obj.getPersons2().add(new Person("Jan Zamek"));
		obj.getPersons2().add(new Person("Jacek Pasterz"));
		json = gson.toJson(obj);
		System.out.println(json);
		
		fromString = gson.fromJson(json, Home.class);
		System.out.println(fromString);
		assertTrue(fromString.getPersons().get(0).getName().equals("jeden"));
		assertTrue(fromString.getPersons().get(1).getName().equals("dwa"));
		assertTrue(fromString.getAttrs().get(0).equals("jeden attr"));
		assertTrue(fromString.getAttrs().get(1).equals("dwa attr"));
		assertTrue(fromString.getGuests().get(1).getName().equals("jeden guest"));
		assertTrue(fromString.getGuests().get(2).getName().equals("dwa guest"));
		assertTrue(fromString.getPets().get(0).getName().equals("szarik"));
		assertTrue(fromString.getPets().get(1).getName().equals("burek"));
		assertTrue(fromString.getPersons2().get(0).getName().equals("Jan Zamek"));
		assertTrue(fromString.getPersons2().get(1).getName().equals("Jacek Pasterz"));
		
	}
	
	@Test
	public void testSpecialList() throws Exception {
		
		GsonBuilder gsonBuilder = new GsonBuilder()
			.registerTypeAdapter(ArrayList.class, new GsonArrayListPersonInstanceCreator())
			.registerTypeAdapter(SpecialList.class, new GsonSpecialListInstanceCreator())
			.registerTypeAdapter(PersonsList.class, new PersonsListInstanceCreator())
			.excludeFieldsWithoutExposeAnnotation();
//		gsonBuilder.
		Gson gson = gsonBuilder.create();
	
		SpecialList<Person> obj = new SpecialList<Person>();
		obj.add(new Person("jeden"));
		obj.add(new Person("dwa"));
		String json = gson.toJson(obj);
		System.out.println(json);
		
//		SpecialList<Person> fromString = gson.fromJson(json, SpecialList.class);
//		System.out.println(fromString);
//		assertTrue(fromString.get(0).getName().equals("jeden"));
//		assertTrue(fromString.get(1).getName().equals("dwa"));
	}
	
	@Test
	public void testPersonsList() {
		Gson gson = new GsonBuilder()
//			.registerTypeAdapter(ArrayList.class, new GsonArrayListPersonInstanceCreator())
//			.registerTypeAdapter(SpecialList.class, new GsonSpecialListInstanceCreator())
//			.registerTypeAdapter(PersonsList.class, new PersonsListInstanceCreator())
			.registerTypeAdapter(PersonsList.class, new PersonsListInstanceCreator())
			.excludeFieldsWithoutExposeAnnotation()
			.create();
	
		PersonsList obj = new PersonsList();
		obj.add(new Person("jeden"));
		obj.add(new Person("dwa"));
		String json = gson.toJson(obj);
		System.out.println(json);
		
//		PersonsList fromString = gson.fromJson(json, PersonsList.class);
//		System.out.println(fromString);
//		assertTrue(fromString.get(0).getName().equals("jeden"));
//		assertTrue(fromString.get(1).getName().equals("dwa"));
	}
	
	@Test
	public void testPersons() {
		Gson gson = new GsonBuilder()
//			.registerTypeAdapter(ArrayList.class, new GsonArrayListPersonInstanceCreator())
			.registerTypeAdapter(ArrayList.class, new GsonArrayListCreator())
//			.registerTypeAdapter(SpecialList.class, new GsonSpecialListInstanceCreator())
//			.registerTypeAdapter(PersonsList.class, new PersonsListInstanceCreator())
			.excludeFieldsWithoutExposeAnnotation()
			.create();
		
		Pets persons = new Pets();
		persons.getPets().add(new Pet("raz"));
		String json = gson.toJson(persons);
		System.out.println(json);
		
		Pets fromString = gson.fromJson(json, Pets.class);
		System.out.println(fromString);
		assertTrue(fromString.getPets().get(0).getName().equals("raz"));
	}
}
