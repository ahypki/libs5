package net.hypki.libs5.unittests.collections;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.InfMap;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;


public class InfMapUnitTest extends LibsTestCase {
	
	@Test
	public void testTripleMap() throws IOException, ClassNotFoundException {
		InfMap triple = new InfMap();
		
		triple.put(true, "jeden", 2);
		triple.put(false, "dwa", 3);
		// it is working
//		String tmp = StringUtilities.serialize(triple);
//		triple = (InfMap) StringUtilities.deserialize(tmp);
		
		String tmp = JsonUtils.objectToString(triple);
		triple = JsonUtils.fromJson(tmp, InfMap.class);		
		
		assertTrue(triple.get("jeden", 1) == null);
		assertTrue((Boolean) triple.get("jeden", 2) == true);
		assertTrue(triple.get("jeden", 3) == null);
		
		assertTrue(triple.get("dwa", 1) == null);
		assertTrue(triple.get("dwa", 2) == null);
		assertTrue((Boolean) triple.get("dwa", 3) == false);
		assertTrue(triple.get("dwa", 4) == null);
		
		assertTrue(triple.get("trzy", 3) == null);
		
		for (List keys : triple.iterateKeys()) {
			LibsLogger.info(InfMapUnitTest.class, "KEY " + keys);
		}
	}
}
