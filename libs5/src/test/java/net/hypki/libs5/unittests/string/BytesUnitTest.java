package net.hypki.libs5.unittests.string;

import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class BytesUnitTest extends LibsTestCase {
	
	@Test
	public void testBytesCodes() {
		String test = "abwzABWZ";
		for (int i = 0; i < test.length(); ++i) {
			byte c = test.getBytes()[i];
			System.out.println(test.substring(i, i + 1) + " -> " + (int) c);
		}
	}
	
	@Test
	public void testByteConversions() {
		String temp = "temp12";
		System.out.println("String: " + temp);
		
		byte[] tempBytes = temp.getBytes();
		System.out.println("byte[]: " + tempBytes);
		System.out.println("byte[].length: " + tempBytes.length);
		
		String temp2 = new String(tempBytes);
		assertTrue(temp.equals(temp2));
		System.out.println("String from bytes: " + temp2);
	}
}
