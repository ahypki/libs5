package net.hypki.libs5.unittests.date;

import java.io.FileNotFoundException;

import jodd.datetime.JDateTime;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

import com.google.gson.annotations.Expose;

public class SimpleDateUnitTest extends LibsTestCase {
	
	@Test
	public void testMsSerialization() throws FileNotFoundException {
		SimpleDate sd = new SimpleDate(2000, 9, 8);
		String sdJson = JsonUtils.objectToString(sd);
		
		System.out.println("JSON: " + sdJson);
		
		SimpleDate sdFromJson = JsonUtils.fromJson(sdJson, SimpleDate.class);
		System.out.println(sdFromJson.toString());
		
		assertTrue(sd.getTimeInMillis() == sdFromJson.getTimeInMillis());
	}
	
	@Test
	public void testSerialization2() throws FileNotFoundException {
		System.out.println("JDateTime " + new JDateTime(2013, 9, 2));
		System.out.println("JDateTime ISO " + new SimpleDate(2013, 9, 2).toStringISO());
		
		assertTrue(SimpleDate.parse("2013-09-02_00:00:00.000").getDay() == 2);
		assertTrue(SimpleDate.parse("2013-09-02T00:00:00.000+02:00").getDay() == 2);
	}

	@Test
	public void testSerialization() throws FileNotFoundException {
		SimpleDate kd = new SimpleDate(2000, 9, 8);
		System.out.println("JSON: " + JsonUtils.objectToString(kd));
		
		String json = JsonUtils.objectToString(kd);
		SimpleDate jd = JsonUtils.fromJson(json, SimpleDate.class);
		System.out.println(jd.toString());
		
		assertTrue(jd.getYear() == 2000);
		assertTrue(jd.getMonth() == 9);
		assertTrue(jd.getDay() == 8);
		
		Bla bla = new Bla();
		bla.setSimpleDate(new SimpleDate(2000, 9, 8));
		
		json = JsonUtils.objectToString(bla);
		Bla bla2 = JsonUtils.fromJson(json, Bla.class);
		
		assertTrue(bla2.getSimpleDate().getYear() == 2000);
		assertTrue(bla2.getSimpleDate().getMonth() == 9);
		assertTrue(bla2.getSimpleDate().getDay() == 8);
		
		kd = SimpleDate.now();
		final long nowMs = kd.getTimeInMillis();
		String jsonSer = JsonUtils.objectToString(kd);
		SimpleDate jdDes = JsonUtils.fromJson(jsonSer, SimpleDate.class);
		assertTrue(nowMs == jdDes.getTimeInMillis(), "Ms differ");
	}
}

class Bla {
	@Expose
	private SimpleDate simpleDate = null;

	public SimpleDate getSimpleDate() {
		return simpleDate;
	}

	public void setSimpleDate(SimpleDate simpleDate) {
		this.simpleDate = simpleDate;
	}
	
}