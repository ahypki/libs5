package net.hypki.libs5.unittests.string;

import net.hypki.libs5.utils.string.RandomUtils;

import org.fluttercode.datafactory.impl.DataFactory;
import org.junit.Test;

public class DataFactoryUnitTest {

	@Test
	public void testCreateNames() {
		DataFactory df = new DataFactory();
		for (int i = 0; i < 10; i++) {
			String name = df.getFirstName() + " " + df.getLastName();
			System.out.println(name);
		}
		
		for (int i = 0; i < 10; i++) {
			String address = df.getAddress()+","+df.getCity()+","+df.getNumberText(5);
			String business = df.getBusinessName();
			System.out.println(business + " located at " + address);
		}

		for (int i = 0; i < 10; i++) {
			System.out.println("animal: " + RandomUtils.randomAnimal());
		}
	}
}
