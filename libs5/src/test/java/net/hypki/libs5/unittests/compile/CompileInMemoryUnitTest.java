package net.hypki.libs5.unittests.compile;

import java.io.IOException;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.junit.Test;


public class CompileInMemoryUnitTest {
	
	@Test
	public void testCompile() throws IOException {
		String path = CompileInMemoryUnitTest.class.getClassLoader().getResource("HelloWorld.java").getPath();

		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		int compilationResult = compiler.run(null, null, null, path);
		if (compilationResult == 0) {
			System.out.println("Compilation is successful");
		} else {
			System.out.println("Compilation Failed");
		}
	}
}
