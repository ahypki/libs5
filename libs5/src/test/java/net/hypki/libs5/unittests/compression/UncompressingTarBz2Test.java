package net.hypki.libs5.unittests.compression;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import net.hypki.libs5.utils.compression.GzipCompressor;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class UncompressingTarBz2Test extends LibsTestCase {
	@Test
	public void testUncompress() throws FileNotFoundException, IOException {
	
		GZIPInputStream is = new GZIPInputStream(new FileInputStream(UncompressingTarBz2Test.class.getClassLoader().getResource("oneTwo.gz").getPath()));
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		
		assertTrue(br.readLine().equals("one"));
		assertTrue(br.readLine().equals("two"));
		assertTrue(br.readLine().equals("three"));
		
        br.close();
	}
	
	@Test
	public void testGzipCompressFolder() throws IOException {
		FileExt toCompress = new FileExt("/opt/project_data");
		
		GzipCompressor gzip = new GzipCompressor(new FileExt("opt.gzip"));
		for (File file : new LazyFileIterator(toCompress, true, true, false)) {
			gzip.addFile(new FileExt(file), toCompress);
		}
		gzip.close();
	}
}
