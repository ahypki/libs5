package net.hypki.libs5.unittests.utils;

import net.hypki.libs5.utils.string.ByteUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;
import net.hypki.libs5.utils.utils.NumberUtils;

import org.junit.Test;


public class ConverterUnitTest extends LibsTestCase {
	
	@Test
	public void testSimple() {
		assertTrue(NumberUtils.toLong("text") == 0);
		assertTrue(NumberUtils.toLong((String) null) == 0);
		assertTrue(NumberUtils.toLong("") == 0);
		
		assertTrue(NumberUtils.toLong("0") == 0);
		assertTrue(NumberUtils.toLong("1") == 1);
		assertTrue(NumberUtils.toLong("10") == 10);
		assertTrue(NumberUtils.toLong("-1") == -1);
	}
	
	@Test
	public void testDefaultValue() {
		assertTrue(NumberUtils.toInt("text", 11) == 11);
		assertTrue(NumberUtils.toInt(null, 11) == 11);
		assertTrue(NumberUtils.toInt("", 11) == 11);
		
		assertTrue(NumberUtils.toInt("0", 11) == 0);
		assertTrue(NumberUtils.toInt("1") == 1);
		assertTrue(NumberUtils.toInt("10") == 10);
		assertTrue(NumberUtils.toInt("-1") == -1);
	}
	
	@Test
	public void testLongToBytesAndOposite() {
		byte[] temp = ByteUtils.toBytes(1234);
		assertTrue(1234 == ByteUtils.toLong(temp));
		
		temp = ByteUtils.toBytes(-5202550769794180169L);
		assertTrue(-5202550769794180169L == ByteUtils.toLong(temp));
	}
}
