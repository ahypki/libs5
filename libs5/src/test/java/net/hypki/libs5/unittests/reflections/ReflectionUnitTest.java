package net.hypki.libs5.unittests.reflections;

import java.util.List;

import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class ReflectionUnitTest extends LibsTestCase {
		
	@Test
	public void testGetUnitTestClasses() throws ClassNotFoundException {
		List<Class<?>> classes = ReflectionUtility.getClasses(LibsTestCase.class, true);
		
		for (Class<?> clazz : classes) {
			System.out.println(clazz.getSimpleName());
		}
		
		assertTrue(classes.size() > 0);
	}
}
