package net.hypki.libs5.unittests.json;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.InstanceCreator;

public class GsonArrayListPersonInstanceCreator implements InstanceCreator<ArrayList<Person>> {
	
	public ArrayList<Person> createInstance(Type type) {
		// No need to use a parameterized list since the actual instance will
		// have the raw type anyway.
		return new ArrayList<Person>();
	}

}
