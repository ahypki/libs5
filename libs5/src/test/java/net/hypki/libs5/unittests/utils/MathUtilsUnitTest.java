package net.hypki.libs5.unittests.utils;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.math.MathUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class MathUtilsUnitTest extends LibsTestCase {

	@Test
	public void testmathExpressions() {
		LibsLogger.debug(MathUtilsUnitTest.class, "2 + 3 = ", MathUtils.eval("2 + 3"));
		LibsLogger.debug(MathUtilsUnitTest.class, "LOG10(10) = ", MathUtils.eval("LOG10(10)"));
		
		String tmp = "IF(1<2, 3, 4)";
		LibsLogger.debug(MathUtilsUnitTest.class, tmp + " = ", MathUtils.eval(tmp));
		
//		tmp = "IF(STREQ(\"ab\", \"ab\"), 5, 6)";
		tmp = "IF(ab== ab, 5, 6)";
		LibsLogger.debug(MathUtilsUnitTest.class, tmp + " = ", MathUtils.eval(tmp));
		
		tmp = "IF(ab== ab && 2>1, 5, 6)";
		LibsLogger.debug(MathUtilsUnitTest.class, tmp + " = ", MathUtils.eval(tmp));
		
		tmp = "IF(ab== ab && 2>11, 5, 6)";
		LibsLogger.debug(MathUtilsUnitTest.class, tmp + " = ", MathUtils.eval(tmp));
		
		tmp = "(7 == 8) || (7+1 > 0+7 || (8 < 10))";
		LibsLogger.debug(MathUtilsUnitTest.class, tmp + " = ", MathUtils.eval(tmp));
		
//		tmp = "(7 == 8) OR (7+1 > 0+7 OR (8 < 10))";
//		LibsLogger.debug(MathUtilsUnitTest.class, tmp + " = ", MathUtils.eval(tmp));
	}
}
