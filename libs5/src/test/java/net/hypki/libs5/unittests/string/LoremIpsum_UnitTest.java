package net.hypki.libs5.unittests.string;

import net.hypki.libs5.utils.string.LoremIpsum;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;


public class LoremIpsum_UnitTest extends LibsTestCase {
	@Test
	public void testLengthOfTestText() {
		assertTrue(LoremIpsum.createSampleString(0).length() == 0);
		assertTrue(LoremIpsum.createSampleString(1).length() == 1);
		assertTrue(LoremIpsum.createSampleString(10).length() == 10);
		assertTrue(LoremIpsum.createSampleString(100).length() == 100);
		assertTrue(LoremIpsum.createSampleString(1000).length() == 1000);
		assertTrue(LoremIpsum.createSampleString(10000).length() == 10000);
		assertTrue(LoremIpsum.createSampleString(2).length() == 2);
		assertTrue(LoremIpsum.createSampleString(57).length() == 57);
	}
}
