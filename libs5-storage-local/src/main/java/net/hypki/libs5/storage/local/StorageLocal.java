package net.hypki.libs5.storage.local;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.storage.StorageProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.MetaList;

public class StorageLocal implements StorageProvider {
	
	public static final String META_STORAGE_PATH = "libs5-storage-local";
	
	private static String STORAGE = "common3-storage-local";
	
	private boolean storageChecked = false;
	
	private Set<String> setsCreated = new HashSet<String>();

	@Override
	public void shutdown() {
		// do nothing
	}
	
	private void checkStorage(String set) {
		if (!storageChecked) {
			new File(STORAGE).mkdirs();
			storageChecked = true;
		}
		
		if (set != null && setsCreated.contains(set) == false) {
			// creating subfolder for a given set name
			new File(STORAGE + "/" + set + "/").mkdirs();
			setsCreated.add(set);
		}
	}
	
	@Override
	public void init(Map<String, Object> params) throws IOException {
		// do nothing
		if (params.containsKey(META_STORAGE_PATH))
			STORAGE = String.valueOf(params.get(META_STORAGE_PATH));
	}

	@Override
	public long save(String set, UUID fileId, InputStream dataStream, MetaList metaList) throws IOException {
		checkStorage(set);
		
		long bytes = FileUtils.saveToFile(dataStream, buildPath(set, fileId));
		LibsLogger.debug(StorageLocal.class, "File ", fileId, " saved");
		
		if (metaList != null)
			FileUtils.saveToFile(JsonUtils.objectToStringPretty(metaList), buildPathMeta(set, fileId));
		
		return bytes;
	}

	@Override
	public boolean remove(String set, UUID fileId) throws IOException {
		boolean removed = buildPath(set, fileId).delete();
		LibsLogger.debug(StorageLocal.class, "File ", fileId, " removed");
		return removed;
	}

	@Override
	public InputStream read(String set, UUID fileId) throws IOException {
		return FileUtils.getInputStream(getPath(set, fileId));
	}
	
	@Override
	public long size(String set, UUID fileId) throws IOException {
		return getPath(set, fileId).length();
	}

	@Override
	public boolean exist(String set, UUID fileId) throws IOException {
		return getPath(set, fileId).exists();
	}
	
	private File buildPath(String set, UUID fileId) {
		return new File(STORAGE + "/" + set + "/" + fileId.getId());
	}
	
	private File buildPathMeta(String set, UUID fileId) {
		return new File(STORAGE + "/" + set + "/" + fileId.getId() + ".meta");
	}
	
	private File getPath(String set, UUID fileId) {
		File f = new File(STORAGE + "/" + fileId.getId());
		if (f.exists())
			return f;
		// TODO old paths, update files at some point
		return new File(STORAGE + "/" + set + "/" + fileId.getId());
	}
}
