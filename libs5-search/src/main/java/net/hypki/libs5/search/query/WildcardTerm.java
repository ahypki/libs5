package net.hypki.libs5.search.query;

import java.io.Serializable;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class WildcardTerm extends Term implements Serializable {
	
	private String term = null;
	
	private boolean toLowerCase = false;

	public WildcardTerm(String field, String term) {
		setField(field);
		setTerm(term);
	}
	
	public WildcardTerm(String field, String term, TermRequirement requirement) {
		setField(field);
		setTerm(term);
		setRequirement(requirement);
	}

	public WildcardTerm setTerm(String term) {
		this.term = term;
		return this;
	}

	public String getTerm() {
		return term;
	}
	
	@Override
	public void toLowerCase() {
		this.term = term != null ? term.toLowerCase() : term;
		this.toLowerCase = true;
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Object value = row.get(getField());
		if (value == null)
			return false;
		else if (value instanceof String)
			return StringUtilities.wildcardMatches(getTerm(), 
					toLowerCase ? ((String) value).toLowerCase() : (String) value);
		else
			return false;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s:*%s*", getRequirement(), getField(), getTerm());
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append(" ");
		builder.append(getRequirement());
		builder.append(" ");
		builder.append(getField());
		builder.append(":*");
		builder.append(getTerm());
		builder.append("* ");
	}
}
