package net.hypki.libs5.search.query;

import java.io.Serializable;

import org.apache.commons.lang.NotImplementedException;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.utils.NumberUtils;

public class LongTerm extends Term implements Serializable {
	
	private long term = 0;

	private TermComparator comparator = TermComparator.E;
	
	public LongTerm(String field, long term) {
		setField(field);
		setTerm(term);
	}
	
	public LongTerm(String field, long term, TermRequirement requirement) {
		setField(field);
		setTerm(term);
		setRequirement(requirement);
	}
	
	public LongTerm(String field, long term, TermComparator comparator, TermRequirement requirement) {
		setField(field);
		setTerm(term);
		setRequirement(requirement);
		setComparator(comparator);
	}
	
	public LongTerm(String field, long term, String comparator, TermRequirement requirement) {
		setField(field);
		setTerm(term);
		setRequirement(requirement);
		setComparator(TermComparator.fromString(comparator));
	}

	public LongTerm setTerm(long term) {
		this.term = term;
		return this;
	}

	public long getTerm() {
		return term;
	}
	
	@Override
	public void toLowerCase() {
		
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Object value = row.get(getField());
		if (value == null)
			return false;
		else if (value instanceof Long) {
			long d = (long) value;
			if (comparator == TermComparator.E)
				return d == getTerm();
			else if (comparator == TermComparator.G)
				return d > getTerm();
			else if (comparator == TermComparator.GE)
				return d >= getTerm();
			else if (comparator == TermComparator.L)
				return d < getTerm();
			else if (comparator == TermComparator.LE)
				return d <= getTerm();
			else
				throw new NotImplementedException();
		} else if (value instanceof Integer) {
			int d = (int) value;
			if (comparator == TermComparator.E)
				return d == getTerm();
			else if (comparator == TermComparator.G)
				return d > getTerm();
			else if (comparator == TermComparator.GE)
				return d >= getTerm();
			else if (comparator == TermComparator.L)
				return d < getTerm();
			else if (comparator == TermComparator.LE)
				return d <= getTerm();
			else
				throw new NotImplementedException();
		} else if (value instanceof Double) {
			double d = (double) value;
			if (comparator == TermComparator.E)
				return d == getTerm();
			else if (comparator == TermComparator.G)
				return d > getTerm();
			else if (comparator == TermComparator.GE)
				return d >= getTerm();
			else if (comparator == TermComparator.L)
				return d < getTerm();
			else if (comparator == TermComparator.LE)
				return d <= getTerm();
			else
				throw new NotImplementedException();
		} else if (value instanceof Float) {
			float d = (float) value;
			if (comparator == TermComparator.E)
				return d == getTerm();
			else if (comparator == TermComparator.G)
				return d > getTerm();
			else if (comparator == TermComparator.GE)
				return d >= getTerm();
			else if (comparator == TermComparator.L)
				return d < getTerm();
			else if (comparator == TermComparator.LE)
				return d <= getTerm();
			else
				throw new NotImplementedException();
		} else if (value instanceof String)
			return NumberUtils.toLong((String) getField(), 0) == getTerm();
		else
			return false;
	}

	@Override
	public String toString() {
		return String.format("%s %s %s %s", getRequirement(), getField(), getComparator(), getTerm());
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append(" ");
		builder.append(getRequirement());
		builder.append(" ");
		builder.append(getField());
		builder.append(" ");
		builder.append(getComparator().toString());
		builder.append(" ");
		builder.append(getTerm());
		builder.append(" ");
	}

	public TermComparator getComparator() {
		return comparator;
	}

	public void setComparator(TermComparator comparator) {
		this.comparator = comparator;
	}
}
