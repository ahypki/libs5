package net.hypki.libs5.search.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.hypki.libs5.db.db.Row;

public class Bracket extends Term implements Serializable {

	private List<Term> terms = null;
	
	public Bracket() {
		setTerms(new ArrayList<Term>());
	}
	
	public Bracket(TermRequirement requirement) {
		setTerms(new ArrayList<Term>());
		setRequirement(requirement);
	}
	
	public Bracket addTerm(Term term) {
		getTerms().add(term);
		return this;
	}
	
	public Bracket addTerms(List<Term> terms) {
		getTerms().addAll(terms);
		return this;
	}
	
	@Override
	public void addPrefixNames(String prefix) {
		for (Term term : getTerms()) {
			term.addPrefixNames(prefix);
		}
	}
	
	@Override
	public void toLowerCase() {
		for (Term term : getTerms()) {
			term.toLowerCase();
		}
	}

	private void setTerms(List<Term> terms) {
		this.terms = terms;
	}

	public List<Term> getTerms() {
		return terms;
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Boolean oneShouldFulfilled = null;
		for (Term term : getTerms()) {
			if (term.getRequirement() == TermRequirement.MUST
					&& term.isFulfilled(row) == false)
					return false;
			else if (term.getRequirement() == TermRequirement.MUST_NOT 
					&& term.isFulfilled(row))
				return false;
			else if (term.getRequirement() == TermRequirement.SHOULD) {
				if (oneShouldFulfilled == null)
					oneShouldFulfilled = false;
				if (term.isFulfilled(row) == true)
					oneShouldFulfilled = true;
			} 
		}
		
		if (oneShouldFulfilled != null)
			return oneShouldFulfilled;
		
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getRequirement());
		builder.append(" (");
		for (Term t : getTerms()) {
			t.toString(builder);
		}
		builder.append(")");
		return builder.toString();
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append(getRequirement());
		builder.append(" (");
		for (Term t : getTerms()) {
			t.toString(builder);			
		}
		builder.append(")");
	}

	public boolean containsField(String name) {
		for (Term term : getTerms()) {
			if (term instanceof Bracket) {
				if (((Bracket) term).containsField(name) == false)
					return true;
			} else if (term.getField().equals(name))
				return true;
//			else
//				throw new NotImplementedException();
					
		}
		return false;
	}
	
	public void removeTerm(String withName) {
		ArrayList<Term> newTerms = new ArrayList<Term>();
		
		for (Term term : getTerms()) {
			if (term instanceof Bracket) {
				Bracket bracket = ((Bracket) term);
				bracket.removeTerm(withName);
				if (bracket.getTerms().size() > 0)
					newTerms.add(term);
			} else if (term.getField().equals(withName)) {
				// do nothing
			} else
				newTerms.add(term);
		}
		
		setTerms(newTerms);
	}
}
