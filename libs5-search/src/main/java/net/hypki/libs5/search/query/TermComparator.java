package net.hypki.libs5.search.query;

import java.io.Serializable;

public enum TermComparator implements Serializable {
	L,
	LE,
	E,
	GE,
	G;

	public static TermComparator fromString(String comparator) {
		if (comparator == null)
			throw new IllegalArgumentException("Comparator not specified");
		else if (comparator.equals("<"))
			return L;
		else if (comparator.equals("<="))
			return LE;
		else if (comparator.equals("=="))
			return E;
		else if (comparator.equals(">="))
			return GE;
		else if (comparator.equals(">"))
			return G;
		else
			throw new IllegalArgumentException("Comparator " + comparator + " unknown");
	}
	
	public String toStringHuman() {
		if (this == L)
			return "<";
		else if (this == LE)
			return "<=";
		else if (this == E)
			return "==";
		else if (this == GE)
			return ">=";
		else if (this == G)
			return ">";
		else
			throw new IllegalArgumentException("Comparator " + this + " unknown");
	}
}
