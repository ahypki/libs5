package net.hypki.libs5.search.query;

import java.io.Serializable;

import net.hypki.libs5.db.db.Row;

public class BooleanTerm extends Term implements Serializable {
	
	private boolean term = false;

	public BooleanTerm(String field, boolean term) {
		setField(field);
		setTerm(term);
	}
	
	public BooleanTerm(String field, boolean term, TermRequirement requirement) {
		setField(field);
		setTerm(term);
		setRequirement(requirement);
	}

	public BooleanTerm setTerm(boolean term) {
		this.term = term;
		return this;
	}

	public boolean getTerm() {
		return term;
	}
	
	@Override
	public void toLowerCase() {
		
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Object value = row.get(getField());
		if (value == null)
			return false;
		else if (value instanceof Boolean)
			return (boolean) value == getTerm();
		else if (value instanceof String)
			return Boolean.parseBoolean((String) getField()) == getTerm();
		else
			return false;
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append(" ");
		builder.append(getRequirement());
		builder.append(" ");
		builder.append(getField());
		builder.append(":");
		builder.append(getTerm());
	}
}
