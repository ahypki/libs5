package net.hypki.libs5.search;

import java.io.IOException;
import java.util.Iterator;

import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;

public class SearchIter<T> implements Iterable<T> {
	
	private Class<T> clazz;
	
	private String index;
	
	private String type;
	
	private Query query;
	
//	private int from;
//	
//	private int size;
	
	private SearchEngineProvider searchProvider = null;

	public SearchIter() {
		
	}
	
	public SearchIter(SearchEngineProvider searchProvider, Class<T> clazz, String index, String type, Query query) {
		setSearchProvider(searchProvider);
		setClazz(clazz);
		setIndex(index);
		setType(type);
		setQuery(query);
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			private SearchResults<T> page = null;
			
			private int pageIdx = 0;
			
			private int from = 0;
			
			private int size = 500; // TODO conf
			
			private T next = null;

			@Override
			public boolean hasNext() {
				if (next == null) {
					if (page == null) {
						// read first page
						try {
							// TODO this class does not work if the number of items are changing in the meantime
							page = getSearchProvider().search(getClazz(), getIndex(), getType(), getQuery(), from, size);
							pageIdx = 0;
							next = null;
						} catch (IOException e) {
							LibsLogger.error(SearchIter.class, "Cannot search for the next page, closing Iterator", e);
							return false;
						}						
					} else if (page != null && pageIdx >= page.size()) {
						// read next page
						try {
							from += size;
							page = getSearchProvider().search(getClazz(), getIndex(), getType(), getQuery(), from, size);
							pageIdx = 0;
							next = null;
						} catch (IOException e) {
							LibsLogger.error(SearchIter.class, "Cannot search for the next page, closing Iterator", e);
							return false;
						}
					} 
				} else {
					return true;
				}
				
				if (pageIdx < page.size()) {
					next = page.getObjects().get(pageIdx++);
					return true;
				} else {
					next = null;
				}
				
				return next != null;
			}

			@Override
			public T next() {
				if (hasNext() == false)
					return null;
				
				T t = next;
				next = null;
				return t;
			}
			
		};
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}

//	private int getFrom() {
//		return from;
//	}
//
//	private void setFrom(int from) {
//		this.from = from;
//	}
//
//	private int getSize() {
//		return size;
//	}
//
//	private void setSize(int size) {
//		this.size = size;
//	}

	private SearchEngineProvider getSearchProvider() {
		return searchProvider;
	}

	private void setSearchProvider(SearchEngineProvider searchProvider) {
		this.searchProvider = searchProvider;
	}
}
