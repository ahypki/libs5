package net.hypki.libs5.search.query;

import java.io.Serializable;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.utils.NumberUtils;

public class IntTerm extends Term implements Serializable {
	
	private int term = 0;

	public IntTerm(String field, int term) {
		setField(field);
		setTerm(term);
	}
	
	public IntTerm(String field, int term, TermRequirement requirement) {
		setField(field);
		setTerm(term);
		setRequirement(requirement);
	}

	public IntTerm setTerm(int term) {
		this.term = term;
		return this;
	}

	public int getTerm() {
		return term;
	}
	
	@Override
	public void toLowerCase() {
		
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Object value = row.get(getField());
		if (value == null)
			return false;
		else if (value instanceof Integer) {
			return (int) value == getTerm();
		} else if (value instanceof Long)
			return (long) value == getTerm();
		else if (value instanceof String)
			return NumberUtils.toInt((String) getField(), 0) == getTerm();
		else
			return false;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s:*%s*", getRequirement(), getField(), getTerm());
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append(" ");
		builder.append(getRequirement());
		builder.append(" ");
		builder.append(getField());
		builder.append(":");
		builder.append(getTerm());
	}
}
