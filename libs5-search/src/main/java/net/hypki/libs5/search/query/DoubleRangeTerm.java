package net.hypki.libs5.search.query;

import java.io.Serializable;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.utils.NumberUtils;

public class DoubleRangeTerm extends Term implements Serializable {
	
	private double from = 0;
	private double to = 0;

	public DoubleRangeTerm(String field, double from, double to) {
		setField(field);
		setFrom(from);
		setTo(to);
	}
	
	public DoubleRangeTerm(String field, double from, double to, TermRequirement requirement) {
		setField(field);
		setFrom(from);
		setTo(to);
		setRequirement(requirement);
	}

	public DoubleRangeTerm setFrom(double from) {
		this.from = from;
		return this;
	}

	public double getFrom() {
		return from;
	}

	public DoubleRangeTerm setTo(double to) {
		this.to = to;
		return this;
	}

	public double getTo() {
		return to;
	}
	
	@Override
	public void toLowerCase() {
		
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Object value = row.get(getField());
		if (value == null)
			return false;
		else if (value instanceof Double) {
			double v = (double) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof Float) {
			float v = (float) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof Integer) {
			int v = (int) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof Long){
			long v = (long) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof String) {
			double v = NumberUtils.toDouble((String) getField());
			return v >= getFrom() && v <= getTo();
		} else
			return false;
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append("[");
		builder.append(getFrom());
		builder.append(" to ");
		builder.append(getTo());
		builder.append("]");
	}
}
