package net.hypki.libs5.search.query;

import java.io.Serializable;

public enum SortOrder implements Serializable {
	ASCENDING,
	DESCENDING
}
