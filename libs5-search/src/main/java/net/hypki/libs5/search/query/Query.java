package net.hypki.libs5.search.query;

import static java.lang.String.*;
import static net.hypki.libs5.search.query.TermRequirement.*;
import static net.hypki.libs5.utils.string.RegexUtils.*;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.NumberUtils.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.TrickyString;
import net.hypki.libs5.utils.utils.NumberUtils;

import org.apache.commons.lang.NotImplementedException;

public class Query implements Serializable {
	
	public static final String CONJUCTION = "(AND|OR)";
	public static final String TERM = "\\w[\\w\\d\\_]*";
	public static final String COMPARATOR = "(<=|<|==|>=|>)";
	public static final String VALUE = "(" + DOUBLE + "|" + INTEGER + "|" + TERM + "|[\\d]+)";
	
	private String sortBy = null;
	
	private SortOrder sortOrder = SortOrder.ASCENDING;
	
	private SortType sortType = SortType.LONG;
	
	private List<Term> terms = null;

	public Query() {
		setTerms(new ArrayList<Term>());
	}
	
	public Query cloneQuery() {
//		return clone();
//	}
	
//	@Override
//	protected Object clone() throws CloneNotSupportedException {
		try {
			return (Query) StringUtilities.deserialize(StringUtilities.serialize(this));
		} catch (ClassNotFoundException | IOException e) {
			LibsLogger.error(Query.class, "Cannot clone Query: " + toString(), e);
			return null;
		}
	}
	
	public Query addTerm(Term term) {
		getTerms().add(term);
		return this;
	}

	public boolean isFulfilled(Row row) {
		Boolean oneShouldFulfilled = null;
		for (Term term : getTerms()) {
			if (term.getRequirement() == TermRequirement.MUST
					&& term.isFulfilled(row) == false)
					return false;
			else if (term.getRequirement() == TermRequirement.MUST_NOT 
					&& term.isFulfilled(row))
				return false;
			else if (term.getRequirement() == TermRequirement.SHOULD) {
				if (oneShouldFulfilled == null)
					oneShouldFulfilled = false;
				if (term.isFulfilled(row) == true)
					oneShouldFulfilled = true;
			} 
		}
		
		if (oneShouldFulfilled != null)
			return oneShouldFulfilled;
		
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(50);
		for (Term term : getTerms()) {
			term.toString(builder);
			builder.append("\n");
		}
		if (getSortBy() != null) {
			builder.append(" SORTBY ");
			builder.append(getSortBy());
			builder.append(" ");
			builder.append(getSortOrder().toString());
		}
		return builder.toString();
	}

	private void setTerms(List<Term> terms) {
		this.terms = terms;
	}

	public List<Term> getTerms() {
		return terms;
	}

	public Query setSortBy(String sortBy) {
		this.sortBy = sortBy;
		return this;
	}
	
	public Query setSortBy(String sortBy, SortOrder sortOrder) {
		this.sortBy = sortBy;
		setSortOrder(sortOrder);
		return this;
	}

	public String getSortBy() {
		return sortBy;
	}
	
	public boolean isSortByDefined() {
		return sortBy != null;
	}
	
	private static Term parseTerm(TermRequirement termReq, String term, String comparator, String value) {
		if (RegexUtils.isDouble(value))
			return new DoubleTerm(term, toDouble(value), TermComparator.fromString(comparator), termReq);
		else if (RegexUtils.isInt(value))
			return new LongTerm(term, toLong(value), TermComparator.fromString(comparator), termReq);
		else if (value.matches(TERM))
			return new WildcardTerm(term, "*" + value + "*", termReq);
		else if (value.matches("[\\w\\d\\_/=-]+"))
			return new WildcardTerm(term, "*" + value + "*", termReq);
		else
			return new WildcardTerm(term, "*" + value + "*", termReq);
	}
	
	private static Term parseTerm(String conjunction, String term, String comparator, String value) {
		TermRequirement termReq = conjunction == null || conjunction.equalsIgnoreCase("and") ? MUST : SHOULD;
		return parseTerm(termReq, term, comparator, value);
	}
	
	public static Bracket parseBracket(String bracketString) {
		Bracket bracket = new Bracket();
		TermRequirement termRequirement = MUST;
		
		TrickyString tsBracket = new TrickyString(bracketString);
		while (!tsBracket.isEmpty()) {
			String word = tsBracket.getNextAlphanumericWord();
			
			if (word == null) {
				if (tsBracket.checkNextPrintableCharacter().equals("(")) {
					tsBracket.trim();
					int closingBracket = StringUtilities.findClosingBracket(tsBracket.checkLine(), 0);
					String bracketStr = tsBracket.checkLine().substring(0, closingBracket + 1);
					tsBracket = new TrickyString(tsBracket.checkLine().substring(closingBracket + 1));// ts.getNextStringUntil(")", true); 
					Bracket bracket2 = parseBracket(bracketStr.trim().substring(1, bracketStr.length() - 1));
					bracket2.setRequirement(termRequirement);
					bracket.addTerm(bracket2);
					continue;
				}
			}
			
			if (word.equalsIgnoreCase("and"))
				termRequirement = MUST;
			else if (word.equalsIgnoreCase("or")) {
				termRequirement = SHOULD;
				
				if (bracket.getTerms().size() > 0)
					bracket.getTerms().get(bracket.getTerms().size() - 1).setRequirement(termRequirement);
			} else if (word.equalsIgnoreCase("NOT"))
				termRequirement = MUST_NOT;
			else {
				// term comparator value
				String comparator = tsBracket.getNextMatch(COMPARATOR);
				if (comparator != null) {
					tsBracket = new TrickyString(tsBracket.getLine().trim());
					String value = tsBracket.getNextStringUntil(" ", true);
					if (value == null)
						value = tsBracket.getLine();
					value = value.trim();
					if (value.endsWith(")"))
						value = value.substring(0, value.length() - 1);
				
					bracket.addTerm(parseTerm(termRequirement, word, comparator, value));
				} else {
					bracket.addTerm(new WildcardTerm(null, /*t*/word.toLowerCase(), TermRequirement.MUST));
				}
			}
		}
		
		return bracket;
	}
	
	public static Query parseQuery(String query) {
		// TODO implement parsing quries from scratch 
		
		if (nullOrEmpty(query))
			return new Query();
		
		String toParse = query;
		LibsLogger.debug(Query.class, "Parsing query : " + toParse);
		
		Query q = new Query();
		
		try {
			TrickyString ts = new TrickyString(toParse);
			TermRequirement termRequirement = MUST;
			
			
			while (!ts.isEmpty()) {
				String prevLine = ts.checkLine();
				
				if (ts.checkNextPrintableCharacter().equals("(")) {
					ts.trim();
					int closingBracket = StringUtilities.findClosingBracket(ts.checkLine(), 0);
//					ts = new TrickyString(ts.checkLine())
					String bracketStr = ts.checkLine().substring(0, closingBracket + 1);
					ts = new TrickyString(ts.checkLine().substring(closingBracket + 1));// ts.getNextStringUntil(")", true); 
					Bracket bracket = parseBracket(bracketStr.trim().substring(1, bracketStr.length() - 1));
					bracket.setRequirement(termRequirement);
					q.addTerm(bracket);
					continue;
				}
				
				String word = null;//ts.getNextAlphanumericWordWithUnderscore();//StringUntil("(", false);
//				termRequirement = MUST;
//				if (word == null && ts.getLine().trim().indexOf(' ') == -1)
//					word = ts.getLine();
				
				ts.trim();
				if (word == null)
					word = ts.getNextMatch(TrickyString.REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS);
				
				if (word == null)
					word = ts.getLine();
				
				if (word != null)
					word = word.trim();

				termRequirement = MUST; // default
				if (word.equalsIgnoreCase("and"))
					termRequirement = MUST;
				else if (word.equalsIgnoreCase("or")) {
					termRequirement = SHOULD;
					
					if (q.getTerms().size() > 0)
						q.getTerms().get(q.getTerms().size() - 1).setRequirement(termRequirement);
//				} else if (word.equalsIgnoreCase("NOT")) {
//					termRequirement = MUST_NOT;
				} else if (word.startsWith("-")) {
					word = word.substring(1);
					termRequirement = MUST_NOT;
					q.addTerm(new WildcardTerm(null, word, termRequirement));
				} else {
					String comparator = ts.checkNextMatch(COMPARATOR);
					if (comparator != null 
							&& ts.toString().trim().startsWith(comparator)) {
						comparator = ts.getNextMatch(COMPARATOR);
						ts = new TrickyString(ts.getLine().trim());
						String value = ts.getNextStringUntil(" ", true);
						if (value == null)
							value = ts.getLine();
						value = value.trim();
					
						q.addTerm(parseTerm(termRequirement, word, comparator, value.toLowerCase()));
//						Bracket b = parseBracket(word);
//						b.setRequirement(termRequirement);
//						q.addTerm(b.getTerms().get(0));
					} else if (word.matches(TrickyString.REGEX_ALPHANUMERIC_WORD_WITH_UNDERSCORE_WITH_MINUS + RegexUtils.SPACES + COMPARATOR + RegexUtils.SPACES + RegexUtils.ANY)) {
						TrickyString wordTricky = new TrickyString(word);
						word = wordTricky.getNextAlphanumericWordWithUnderscore();
						comparator = wordTricky.getNextMatch(COMPARATOR);
						String value = wordTricky.getLine();
						
						word = word.trim();
						comparator = comparator.trim();
						value = value.trim();
					
						q.addTerm(parseTerm(termRequirement, word, comparator, value.toLowerCase()));
					} else {
						// replacing special characters with spaces
//						word = word.replaceAll("[^\\w\\d]", " ");
						
						for (String wordPart : StringUtilities.split(word))
							q.addTerm(new WildcardTerm(null, /*t*/wordPart.toLowerCase(), termRequirement));
					}
				}
				
				if (ts.checkLine().equals(prevLine)) {
					LibsLogger.error(Query.class, "Something is wrong, cannot parse query, no advance");
					break;
				}
			}
		} catch (Exception e) {
			LibsLogger.error(Query.class, "Cannot parse " + query, e);
			return q;
		}
		
		if (q.getTerms().size() > 0)
			return q;
		
		String term = null;
		
		// first search for: AND|OR term comparator value 
		while ((term = firstGroup("(" + CONJUCTION + SPACES + TERM +  SPACES + COMPARATOR + SPACES + "(" + DOUBLE + "|" + TERM + "|[\\d]+)" + ")", toParse, Pattern.CASE_INSENSITIVE)) != null) {
			List<String> parts = allGroups(format("%s%s(%s)%s%s%s%s", CONJUCTION, SPACES, TERM, SPACES, COMPARATOR, SPACES, VALUE), term, Pattern.CASE_INSENSITIVE);
			q.addTerm(parseTerm(parts.get(0), parts.get(1), parts.get(2), parts.get(3).toLowerCase()));
			toParse = toParse.replace(term, ""); 
		}
		
		while ((term = firstGroup("[^\\w](" + CONJUCTION + "[^\\w]" + SPACES + TERM +  SPACES + ")", toParse, Pattern.CASE_INSENSITIVE)) != null) {
			List<String> parts = allGroups(CONJUCTION + SPACES + "(" + TERM + ")" +  SPACES, term, Pattern.CASE_INSENSITIVE);
			TermRequirement termReq = parts.get(0).equalsIgnoreCase("or") ? TermRequirement.SHOULD : MUST;
			q.addTerm(new WildcardTerm(null, parts.get(1).toLowerCase(),  termReq));
			toParse = toParse.replace(term, ""); 
			
			if (toParse.trim().matches(TERM)) {
				q.addTerm(new WildcardTerm(null, toParse.trim(), termReq));
				toParse = "";
			}
		}
		
		// search for term (without AND|OR)
//		if ((term = firstGroup("(" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + VALUE + ")", query)) != null) {
//			List<String> parts = allGroups(format("%s(%s)%s%s%s%s", SPACES, TERM, SPACES, COMPARATOR, SPACES, VALUE), term);
//			q.addTerm(parseTerm("and", parts.get(0), parts.get(1), parts.get(2).toLowerCase()));
//			query = query.replace(term, "");
//		}
		
		// now search for: term comparator value
		if (toParse == null) {
			// nothing more to do
		} else {
			while (StringUtilities.notEmpty(toParse.trim())) {
				if (toParse.matches("^" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + VALUE + SPACES + ANY)) {
					List<String> parts = allGroups(format("%s(%s)%s%s%s%s%s", SPACES, TERM, SPACES, COMPARATOR, SPACES, VALUE, SPACES), toParse);
					q.addTerm(parseTerm("and", parts.get(0), parts.get(1), parts.get(2).toLowerCase()));
					toParse = toParse.substring(firstGroup("(" + SPACES + TERM +  SPACES + COMPARATOR + SPACES + VALUE + SPACES + ")", toParse).length());
				} else {
					// otherwise -> wildcard queries
//					for (String t : toParse.split("[\\s]+")) {
//						if (t.length() > 0)
					String parts[] = StringUtilities.split(toParse);
					String part = parts.length > 0 ? parts[0] : null;
					if (part != null) {
						q.addTerm(new WildcardTerm(null, /*t*/part.toLowerCase(), TermRequirement.MUST));
						toParse = (part.length() + 1 < toParse.length()) ? toParse.substring(part.length() + 1) : "";
					}
//					}
				}
			}
		}
			
			
		
		LibsLogger.debug(Query.class, "Parsed query " + q);
		return q;
	}

	public SortOrder getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public void addPrefixNames(String prefix) {
		for (Term term : getTerms()) {
			term.addPrefixNames(prefix);
		}
	}

	public boolean containsField(String name) {
		for (Term term : getTerms()) {
			if (term instanceof Bracket) {
				if (((Bracket) term).containsField(name))
					return true;
			} else if (term != null
					&& term.getField() != null
					&& term.getField().equals(name))
				return true;
//			else
//				throw new NotImplementedException();
					
		}
		return false;
	}
	
	private void getFieldNames(Bracket bracket, List<String> names) {
		for (Term term : bracket.getTerms()) {
			if (term instanceof Bracket)
				getFieldNames((Bracket) term, names);
			else {
				if (!names.contains(term.getField()))
					names.add(term.getField());
			}
		}
	}
	
	public List<String> getFieldNames() {
		List<String> names = new ArrayList<String>();
		
		for (Term term : getTerms()) {
			if (term instanceof Bracket)
				getFieldNames((Bracket) term, names);
			else {
				if (!names.contains(term.getField()))
					names.add(term.getField());
			}
		}
		
		return names;
	}

	public Query addTermEQ(String fieldName, Object v) {
		if (v == null)
			return this;
		else if (v instanceof String)
			addTerm(new StringTerm(fieldName, (String) v, TermRequirement.MUST));
		else if (v instanceof Double)
			addTerm(new DoubleTerm(fieldName, (Double) v, TermRequirement.MUST));
		else if (v instanceof Integer)
			addTerm(new IntTerm(fieldName, (Integer) v, TermRequirement.MUST));
		else if (v instanceof Long)
			addTerm(new LongTerm(fieldName, (Long) v, TermRequirement.MUST));
		else
			throw new NotImplementedException("Not implemented case for addTermEQ for " + v);
		return this;
	}

//	public static C3Where toC3Where(Query q) {
//		C3Where where = new C3Where();
//		
//		for (Term term : q.getTerms()) {
//			where
//				.addClause(new C3Clause(term.getField(), clauseType, value))
//		}
//		
//		return where;
//	}
	
	public static void main(String[] args) {
		System.out.println(parseQuery("sm1"));
		System.out.println(parseQuery("sm2>0.0"));
		System.out.println(parseQuery("(sm1<0.1 AND sm2<0.1)"));
		System.out.println(parseQuery("(sm1<0.1 OR sm2<0.1)"));
		System.out.println(parseQuery("sm1<0.1 AND sm2<0.1"));
		System.out.println(parseQuery("(sm1<0.1 AND sm2<0.1) OR (sm2>0.0)"));
		System.out.println(parseQuery("(sm1<0.1 sm2<0.1) OR (sm2>0.0)"));
		System.out.println(parseQuery("tphys > 12000.0 AND tphys < 12030.0 AND sm1 > 0.0 AND sm1 < 0.1 AND sm2 > 0.0001 AND sm2 < 0.1"));
		System.out.println(parseQuery("(tphys > 1000.0 AND tphys < 1030.0) OR (tphys > 12000.0 AND tphys < 12030.0)"));
		System.out.println(parseQuery("MOCCA n_1>10"));
		System.out.println(parseQuery("(ik1i == 10 OR ik1i == 11 OR ik1i == 12) AND (ik2i == 10 OR ik2i == 11 OR ik2i == 12)"));
		System.out.println(parseQuery("tbid==abc"));
		System.out.println(parseQuery("tphys < 200.0 AND (ik1 == 14 OR ik2 == 14)"));
	}

	public void removeTerm(String withName) {
		ArrayList<Term> newTerms = new ArrayList<Term>();
		
		for (Term term : getTerms()) {
			if (term instanceof Bracket) {
				Bracket bracket = ((Bracket) term);
				bracket.removeTerm(withName);
				if (bracket.getTerms().size() > 0)
					newTerms.add(term);
			} else if (term.getField().equals(withName)) {
				// do nothing
			} else
				newTerms.add(term);
		}
		
		setTerms(newTerms);
	}

	public SortType getSortType() {
		return sortType;
	}

	public Query setSortType(SortType sortType) {
		this.sortType = sortType;
		return this;
	}

	public void toLowerCase() {
		for (Term term : terms) {
			term.toLowerCase();
		}
	}

	public boolean isShouldAll() {
		for (Term term : terms) {
			if (term.getRequirement() != SHOULD)
				return false;
		}
		return true;
	}
}
