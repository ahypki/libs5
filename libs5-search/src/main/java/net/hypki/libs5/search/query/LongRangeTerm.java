package net.hypki.libs5.search.query;

import java.io.Serializable;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.utils.NumberUtils;

public class LongRangeTerm extends Term implements Serializable {
	
	private long from = 0;
	private long to = 0;

	public LongRangeTerm(String field, long from, long to) {
		setField(field);
		setFrom(from);
		setTo(to);
	}
	
	public LongRangeTerm(String field, long from, long to, TermRequirement requirement) {
		setField(field);
		setFrom(from);
		setTo(to);
		setRequirement(requirement);
	}

	public LongRangeTerm setFrom(long from) {
		this.from = from;
		return this;
	}

	public long getFrom() {
		return from;
	}

	public LongRangeTerm setTo(long to) {
		this.to = to;
		return this;
	}

	public long getTo() {
		return to;
	}
	
	@Override
	public void toLowerCase() {
		
	}
	
	@Override
	public boolean isFulfilled(Row row) {
		Object value = row.get(getField());
		if (value == null)
			return false;
		else if (value instanceof Long) {
			long v = (long) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof Integer) {
			int v = (int) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof Double) {
			double v = (double) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof Float) {
			float v = (float) value;
			return v >= getFrom() && v <= getTo();
		} else if (value instanceof String) {
			double v = NumberUtils.toDouble((String) getField());
			return v >= getFrom() && v <= getTo();
		} else
			return false;
	}
	
	@Override
	public void toString(StringBuilder builder) {
		builder.append("[");
		builder.append(getFrom());
		builder.append(" to ");
		builder.append(getTo());
		builder.append("]");
	}
}
