package net.hypki.libs5.search;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.IModule;

import org.javatuples.Pair;

import com.google.gson.JsonElement;

public interface SearchEngineProvider extends IModule {
	
	public final static String JVM_SEARCH_ENGINE_PROVIDER = "SearchEngineProvider";
	
	public void init(JsonElement settings) throws IOException;
	
	public void indexDocument(String index, String type, String id, String data) throws IOException;
	
	public void indexDocument(String index, String type, Map<String, String> idData) throws IOException;
	
	public void removeDocument(String index, String type, String id) throws IOException;
	
	public void remove(String index, String type, Query query) throws IOException;
	
	public <T> T getDocument(Class<T> clazz, String index, String type, String id) throws IOException;
	
	public void clearIndexType(String index, String type) throws IOException;
	public void clearIndex(String index) throws IOException;
	
	public boolean isIndexTypeEmpty(String index, String type) throws IOException;
	
	public SearchResults<String> searchIds(String index, String type, Query query, int from, int size) throws IOException;
	public <T> SearchResults<T> search(Class<T> clazz, String index, String type, Query query, int from, int size) throws IOException;
	
	public void close();
}
