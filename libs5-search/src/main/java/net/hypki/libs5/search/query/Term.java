package net.hypki.libs5.search.query;

import java.io.Serializable;
import java.util.List;

import net.hypki.libs5.db.db.Row;

public abstract class Term implements Serializable {
	
	private String field = null;
		
	private TermRequirement requirement = TermRequirement.MUST;
	
	public abstract void toString(StringBuilder builder);
	
	public abstract boolean isFulfilled(Row row);
	
	public abstract void toLowerCase();
	
	public Term() {
		
	}
	
	public Term(String field) {
		setField(field);
	}
	
	public Term(String field, TermRequirement requirement) {
		setField(field);
		setRequirement(requirement);
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s", getClass().getSimpleName(), getField(), getRequirement().toString());
	}
	
	public void addPrefixNames(String prefix) {
		setField(prefix + getField());
	}

	public Term setField(String field) {
		this.field = field;
		return this;
	}

	public String getField() {
		return field;
	}

	public Term setRequirement(TermRequirement requirement) {
		this.requirement = requirement;
		return this;
	}

	public TermRequirement getRequirement() {
		return requirement;
	}
}
