package net.hypki.libs5.search.query;

import java.io.Serializable;

public enum TermRequirement implements Serializable {
	MUST,
	MUST_NOT,
	SHOULD;
	

	
	public String toStringHuman() {
		if (this == MUST)
			return "AND";
		else if (this == MUST_NOT)
			return "NOT";
		else
			return "OR";
	}
}
