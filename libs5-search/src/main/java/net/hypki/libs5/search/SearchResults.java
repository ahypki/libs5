package net.hypki.libs5.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.Expose;

import mjson.Json;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.json.JsonUtils;

public class SearchResults<T> implements Iterable<T> {
	
	@Expose
	private long maxHits = 0;

	@Expose
	private List<T> objects = null;
	
	public SearchResults() {
		
	}
	
	public java.util.Iterator<T> iterator() {
		return getObjects().iterator();
	};
	
	public SearchResults(Collection<T> results, long maxHits) {
		this.objects = new ArrayList<T>();
		this.maxHits = maxHits;
		
		if (results != null)
			for (T t : results) {
				this.objects.add(t);
			}
	}
	
	public List<T> getObjects() {
		if (objects == null)
			objects = new ArrayList<T>();
		return objects;
	}
	
	public Json getObjectsAsJson() {
		Json rows = Json.array();
		for (T row : this) {
			rows.add(Json.read(JsonUtils.objectToString(row)));
		}
		return rows;
	}
	
	public long size() {
		return objects != null ? objects.size() : 0;
	}
	
	public long maxHits() {
		return maxHits;
	}
	
	public void setMaxHits(long maxHits) {
		this.maxHits = maxHits;
	}
}
