package net.hypki.libs5.storage.google;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.storage.StorageProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.MetaList;

public class StorageLocal implements StorageProvider {
	
	private static final String STORAGE = "common3-storage-local";
	
	private boolean storageChecked = false;

	@Override
	public void shutdown() {
		// do nothing
	}
	
	private void checkStorage() {
		if (!storageChecked) {
			new File(STORAGE).mkdirs();
			storageChecked = true;
		}
	}

	public void save(UUID fileId, InputStream dataStream) throws IOException {
		checkStorage();
		
		FileUtils.saveToFile(dataStream, new File(STORAGE + "/" + fileId.getId()));
		LibsLogger.debug(StorageLocal.class, "File ", fileId, " saved");
	}

	public boolean remove(UUID fileId) throws IOException {
		boolean removed = new File(STORAGE + "/" + fileId.getId()).delete();
		LibsLogger.debug(StorageLocal.class, "File ", fileId, " removed");
		return removed;
	}

	public InputStream read(UUID fileId) throws IOException {
		return FileUtils.getInputStream(new File(STORAGE + "/" + fileId.getId()));
	}

	public boolean exist(UUID fileId) throws IOException {
		return new File(STORAGE + "/" + fileId.getId()).exists();
	}

	@Override
	public void init(Map<String, Object> params) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long save(String set, UUID fileId, InputStream dataStream, MetaList metaList) throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean remove(String set, UUID fileId) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public InputStream read(String set, UUID fileId) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exist(String set, UUID fileId) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long size(String set, UUID fileId) throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}
}
