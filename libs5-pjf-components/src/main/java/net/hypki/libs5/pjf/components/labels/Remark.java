package net.hypki.libs5.pjf.components.labels;

import net.hypki.libs5.utils.string.StringUtilities;

public class Remark {
	
	private MessageType messageType = MessageType.DEFAULT;
	
	private String caption = null;
	
	private String text = null;
	
	public Remark(final String name) {
		setText(name);
	}

	public Remark(final MessageType labelType, final String name) {
		setMessageType(labelType);
		setText(name);
	}
	
	public Remark(final MessageType labelType, final String caption, final String name) {
		setMessageType(labelType);
		setCaption(caption);
		setText(name);
	}
	
	@Override
	public String toString() {
		return String.format("<div class=\"alert alert-%s\">%s%s</div>", getMessageType().toString().toLowerCase(), 
				StringUtilities.notEmpty(getCaption()) ? "<strong>" + getCaption() +"</strong>" : "",
					getText());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
}
