package net.hypki.libs5.pjf.components.labels;

public enum MessageType {
	DEFAULT,
	INFO,
	SUCCESS,
	WARNING,
	DANGER
}
