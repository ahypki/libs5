package net.hypki.libs5.pjf.components.ops;

import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;

public class OpDialogOpen extends net.hypki.libs5.pjf.op.OpDialogOpen {

	public OpDialogOpen(final Component component, final String methodToCall, final String title, final String html, final int width, final int height) {
		super(Component.CLICK_URL, 
				new Params()
					.add("componentClass", component.getClass().getName())
					.add("componentId", component.getId())
					.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
					.add("method", methodToCall),
				RestMethod.POST.toString(),
				title, 
				html, 
				width,
				height);
	}
	
	public OpDialogOpen(final Component component, final String methodToCall, final String title, final String html) {
		super(Component.CLICK_URL, 
				new Params()
					.add("componentClass", component.getClass().getName())
					.add("componentId", component.getId())
					.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
					.add("method", methodToCall),
				RestMethod.POST.toString(),
				title, 
				html, 
				480,
				480);
	}
	
	public OpDialogOpen(final String title, final String html) {
		super(null, 
				null,
				RestMethod.POST.toString(),
				title, 
				html, 
				480,
				480);
	}
	
	public OpDialogOpen(final String title, final String html, final int width, final int height) {
		super(null, 
				null,
				RestMethod.POST.toString(),
				title, 
				html, 
				width,
				height);
	}
}
