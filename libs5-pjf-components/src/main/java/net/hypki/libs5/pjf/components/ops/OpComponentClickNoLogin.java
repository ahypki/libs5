package net.hypki.libs5.pjf.components.ops;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class OpComponentClickNoLogin extends Op {

	public OpComponentClickNoLogin(final Component component, final String methodToCall, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL_NOLOGIN);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
	
	public OpComponentClickNoLogin(final Class componentClass, final String componentIdToSerialize, 
			final String methodToCall, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentIdToSerialize)
//			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL_NOLOGIN);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentIdToSerialize + " :input').serialize()")); 
	}
}
