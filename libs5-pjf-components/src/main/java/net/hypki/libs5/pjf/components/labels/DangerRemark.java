package net.hypki.libs5.pjf.components.labels;

public class DangerRemark extends Remark {

	public DangerRemark(String caption, String msg) {
		super(MessageType.DANGER, caption, msg);
	}
}
