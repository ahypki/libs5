package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.onClick;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

public class ComboComponent extends Component {
	
	@Expose
	private String name = null;
	
	@Expose
	private String selectedValue = null;
	
	@Expose
	private List<String> values = null;
	
	@Expose
	private boolean editMode = false;
	
	@Expose
	private Map<String, OpList> onEdit = null;
	
	@Expose
	private OpList onEditAlways = null;
	
	@Expose
	private String additionalCssClass = null;
	
	private boolean visible = true;
	
	public ComboComponent() {
		
	}

	public ComboComponent(String inputName) {
		setName(inputName);
	}
	
	public OpList changeValue() throws IOException {
		final String newValue = getParams().getString("newValue");
		
		setSelectedValue(newValue);
		
		return new OpList()
			.add(new OpReplace("#" + getId(), toHtml()))
			.add(getOnEditAlways())
			.add(getOnEdit().get(newValue))
			;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_(" pjf-combo " + getAdditionalCssClassNotNull())
					.style(isVisible() ? "" : "display: none;")
					.onClick(new OpList()
								.add(new OpHide("#" + getId() + " .selected"))
								.add(new OpShow("#" + getId() + " .possible"))
								.add(new OpSetAttr("#" + getId() + ".combo", "onclick", ""))
								.toStringOnclick()))
				.input(type("hidden")
						.name(getName())
						.id(getName())
						.value(getSelectedValue()))
		
				.span(class_("selected")
						.add("style", isEditMode() ? "display: none;" : ""))
					.content(getSelectedValue())

				.span(class_("possible")
						.add("style", !isEditMode() ? "display: none;" : ""));
		
		for (String v : getValues()) {
			html
				.a(class_(" ")
						.onClick(new OpComponentClick(this, "changeValue", new Params().add("newValue", v))
						.toStringOnclick()))
					.content(v);
		}
			
		html
				._span()
			._div();
	}

	public String getName() {
		return name;
	}

	public ComboComponent setName(String name) {
		this.name = name;
		return this;
	}

	public List<String> getValues() {
		if (this.values == null)
			this.values = new ArrayList<String>();
		return values;
	}

	public ComboComponent setValues(List<String> values) {
		this.values = values;
		return this;
	}
	
	public ComboComponent setValues(String ... values) {
		this.values = new ArrayList<String>();
		for (String v : values) {
			this.values.add(v);
		}
		return this;
	}
	
	public ComboComponent addValue(boolean addIfTrue, String value) {
		if (addIfTrue)
			addValue(value);
		return this;
	}
	
	public ComboComponent addValue(String value) {
		if (!getValues().contains(value))
			getValues().add(value);
		getSelectedValue();
		return this;
	}
	
	public ComboComponent addValue(String[] value) {
		for (String s : value)
			addValue(s);
		getSelectedValue();
		return this;
	}

	public String getSelectedValue() {
		if (selectedValue == null) {
			if (getValues() != null && getValues().size() > 0)
				this.selectedValue = getValues().get(0);
		}
		return selectedValue;
	}

	public ComboComponent setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
		return this;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public Map<String, OpList> getOnEdit() {
		if (onEdit == null)
			onEdit = new HashMap<>();
		return onEdit;
	}

	public ComboComponent setOnEdit(Map<String, OpList> onEdit) {
		this.onEdit = onEdit;
		return this;
	}
	
	public ComboComponent addOp(String value, Op op) {
		OpList oplist = getOnEdit().get(value);
		if (oplist == null)
			oplist = new OpList();
		oplist.add(op);
		getOnEdit().put(value, oplist);
		return this;
	}
	
	public ComboComponent addOp(Op op) {
		getOnEditAlways().add(op);
		return this;
	}
	
	public ComboComponent addOp(OpList op) {
		getOnEditAlways().add(op);
		return this;
	}

	public String getAdditionalCssClass() {
		return additionalCssClass;
	}
	
	public String getAdditionalCssClassNotNull() {
		return additionalCssClass != null ? additionalCssClass : "";
	}

	public ComboComponent setAdditionalCssClass(String additionalCssClass) {
		this.additionalCssClass = additionalCssClass;
		return this;
	}

	public boolean isVisible() {
		return visible;
	}

	public ComboComponent setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public OpList getOnEditAlways() {
		if (onEditAlways == null)
			onEditAlways = new OpList();
		return onEditAlways;
	}

	public void setOnEditAlways(OpList onEditAlways) {
		this.onEditAlways = onEditAlways;
	}

	public ComboComponent addValue(List<String> possibleValues) {
		for (String v : possibleValues) {
			addValue(v);
		}
		return this;
	}
}
