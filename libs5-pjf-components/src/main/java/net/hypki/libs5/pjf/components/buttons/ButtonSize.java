package net.hypki.libs5.pjf.components.buttons;

public enum ButtonSize {
	EXTRA_SMALL,
	SMALL,
	NORMAL
}
