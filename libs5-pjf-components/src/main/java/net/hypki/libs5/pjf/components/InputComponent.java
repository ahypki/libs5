package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.*;

import java.io.IOException;

import net.hypki.libs5.pjf.HtmlSpecial;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class InputComponent extends Component {
	
	private String value = null;
	
	@Expose
	private OpList opOnSave = null;
	
	@Expose
	private OpList opOnCancel = null;
	
	@Expose
	private boolean readonly = false;
	
	@Expose
	private String valueName = "text";

	public InputComponent() {
		
	}
	
	public InputComponent(String value, OpList opOnSave, OpList opOnCancel) {
		setValue(value);
		setOpOnSave(opOnSave);
		setOpOnCancel(opOnCancel);
	}
	
	private OpList edit(Params params) throws IOException {
		final String text = params.getString(getValueName());
		
		setValue(text);
		
		return new OpList()
			.add(new OpReplace("#" + getId(), renderEdit(new HtmlCanvas()).toHtml()));
	}
	
	private OpList view(Params params) throws IOException {
		final String text = params.getString(getValueName());
		
		setValue(text);
		
		HtmlCanvas html = new HtmlCanvas();
		renderOn(html);
		
		return new OpList()
			.add(new OpReplace("#" + getId(), html.toHtml()));
	}

	protected HtmlCanvas renderEdit(HtmlCanvas html) throws IOException {
		return html
//				.div(class_("card card-block sameheight-item"))
			.div(id(getId())
					.class_("input-component form-group")
					.onKeypress(HtmlSpecial.ifKeyCode(HtmlSpecial.KEYCODE_ENTER, getOpListForSave()))
					.onKeyup(HtmlSpecial.ifKeyCode(HtmlSpecial.KEYCODE_ESCAPE, getOpListForCancel()))
					)
//					.form()
				.input(class_("text form-control boxed")
						.type("text")
						.name(getValueName())
						.value(getValue() != null ? getValue() : ""))
//				._form()
				.render(new ButtonGroup()
						.setAdditionalCssClasses("input-component-buttons ")
						.addButton(new Button()
									.setHtmlClass("btn btn-primary-outline")
									.setGlyphicon("file")
									.setName("Save")
									.add(getOpListForSave())
									)
						.addButton(new Button()
									.setHtmlClass("btn btn-primary-outline")
									.setGlyphicon("ban-circle")
									.setName("Cancel")
									.add(getOpListForCancel())
									)
						)
			._div()
//			._div()
			;
	}
	
	protected HtmlCanvas renderView(HtmlCanvas html) throws IOException {
		return html
			.span()
				.content(getValue() != null ? getValue() : "");
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("input-component")
					.onClick(isReadonly() ? "" : new OpComponentClick(this, "edit", new Params().add(getValueName(), getValue() != null ? getValue() : "")).toStringOnclick()));
		
		renderView(html);
		
		html
			._div();
	};
	
	private OpList getOpListForSave() {
		return new OpList()
				.add(getOpOnSave())
				.add(new OpComponentClick(this, "view"));
	}
	
	private OpList getOpListForCancel() {
		return new OpList()
				.add(new OpSetVal("#" + getId() + " .text", getValue() != null ? getValue() : ""))
				.add(getOpOnCancel())
				.add(new OpComponentClick(this, "view"));
	}

	public OpList getOpOnSave() {
		return opOnSave;
	}

	public void setOpOnSave(OpList opOnSave) {
		this.opOnSave = opOnSave;
	}

	public OpList getOpOnCancel() {
		return opOnCancel;
	}

	public void setOpOnCancel(OpList opOnCancel) {
		this.opOnCancel = opOnCancel;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public boolean isReadonly() {
		return readonly;
	}

	public InputComponent setReadonly(boolean readonly) {
		this.readonly = readonly;
		return this;
	}

	public String getValueName() {
		return valueName;
	}

	public InputComponent setValueName(String valueName) {
		this.valueName = valueName;
		return this;
	}
}
