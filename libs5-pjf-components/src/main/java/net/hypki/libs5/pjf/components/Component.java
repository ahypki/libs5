package net.hypki.libs5.pjf.components;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpGET;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;
import net.hypki.libs5.weblibs.user.Allowed;

import org.apache.commons.lang.NotImplementedException;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

@Path(Component.PATH)
public class Component implements Renderable {

	// TODO DEBUG
	public static final String PATH = "view/component";
	
	public static final String CLICK = "click";
	public static String CLICK_URL = PATH + "/" + CLICK;
	
	public static final String CLICKNOLOGIN = "clicknologin";
	public static String CLICK_URL_NOLOGIN = PATH + "/" + CLICKNOLOGIN;
	
	private String id = null;
	
	private SessionBean sb = null;
	
	private SecurityContext securityContext = null;
	
	private Params params = null;
	
	private OpList opList = null;
	
	private UriInfo uriInfo = null;
	
	public Component() {
		setId(UUID.random().getId());
	}
	
	public Component(Component initializingComponent) {
		setId(UUID.random().getId());
		if (initializingComponent != null) {
			setParams(initializingComponent.getParams());
	//		setSessionBean(initializingComponent.getSessionBean());
			setSecurityContext(initializingComponent.getSecurityContext());
		}
	}
	
	public Component(SecurityContext securityContext, Params params) {
		setId(UUID.random().getId());
		setSecurityContext(securityContext);
		setParams(params);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		throw new NotImplementedException("Subclass of " + getClass().getSimpleName() + " must implement method renderOn()");
	}
	
	@POST
	@Path(CLICKNOLOGIN)
//	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String clickNoLogin(@Context SecurityContext sc,
			@Context UriInfo uriInfo,
			@FormParam("componentClass") String componentClass,
			@FormParam("componentId") String componentId,
			@FormParam("componentData") String componentData,
			@FormParam("method") String method,
			MultivaluedMap<String, String> formParams) throws IOException, ValidationException {
		return click(sc, uriInfo, componentClass, componentId, componentData, method, formParams);
	}
	
	@POST
	@Path(CLICK)
//	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String click(@Context SecurityContext sc,
			@Context UriInfo uriInfo,
			@FormParam("componentClass") String componentClass,
			@FormParam("componentId") String componentId,
			@FormParam("componentData") String componentData,
			@FormParam("method") String method,
			MultivaluedMap<String, String> formParams) throws IOException, ValidationException {
		
		LibsLogger.debug(Component.class, "Click on component ", componentClass, " with ID ", componentId);//, " with query params ", formParams);
		
		Watch totalTime = new Watch();
		Component component = null;
		
		try {			
			final Class componentClazz = Class.forName(componentClass);
			if (StringUtilities.nullOrEmpty(componentData))
//				component = (Component) componentClazz.getDeclaredConstructors()[0].newInstance(new Object[]{null});
				component = (Component) componentClazz.newInstance();
			else
				component = (Component) JsonUtils.fromJson(Base64Utils.decode(componentData), componentClazz);
			component.setId(componentId);
			
			component.sb = (SessionBean) sc.getUserPrincipal();
			component.securityContext = sc;
			this.sb = component.sb;
			this.securityContext = sc;
			setUriInfo(uriInfo);
			
			Params params = new Params();
			if (formParams != null) {
				for (Entry<String, List<String>> e : formParams.entrySet()) {
					List<String> values = new ArrayList<String>();
					for (String s : e.getValue()) {
						try {
							values.add(UrlUtilities.urlDecode(s));
						} catch (IllegalArgumentException e2) {
							values.add(s);
						}
					}
					if (values.size() == 1)
						params.add(e.getKey(), values.get(0));
					else
						params.add(e.getKey(), values);
				}
			}
			if (sb != null)
				params.add("userId", sb.getUserId());
			
			component.setParams(params);
						
			Object ret = ReflectionUtility.invokeMethod(component, method, new Object[] {sc, params, uriInfo}, true, true, true);
			
			// TODO write error if it returnes too much data
			
			if (ret instanceof Op)
				return new OpList().add((Op) ret).toString();
			else if (ret instanceof OpList)
				return ((OpList) ret).toString();
			else if (ret instanceof String)
				return (String) ret;
			else
				return new OpList().toString();
			
		} catch (Exception e) {
			LibsLogger.error(Component.class, "Cannot call " + method + " on component " + componentClass, e);
			if (component != null)
				return component.onError(e).toString();
			else
				return onError(e).toString();
		} finally {
			if (totalTime.ms() > 250)
				LibsLogger.warn(Component.class, "Method ", method, " executed for component ", 
						component.getClass().getSimpleName(), " in ", totalTime.toString());
			else
				LibsLogger.debug(Component.class, "Method ", method, " executed for component ", 
						component.getClass().getSimpleName(), " in ", totalTime.toString());			
		}
	}
	
	protected OpList onError(Exception e) throws IOException {
		if (e instanceof ValidationException)
			return new OpList().add(new OpError(e.getMessage()));
		else if (e.getCause() instanceof ValidationException)
			return new OpList().add(new OpError(e.getCause().toString()));
		else if (e.getCause() instanceof net.hypki.libs5.utils.utils.ValidationException)
			return new OpList().add(new OpError(e.getCause().getMessage()));
		else
			return new OpList();
	}
	
	public SessionBean getSessionBean() {
		if (this.sb == null) {
			if (this.securityContext != null)
				this.sb = (SessionBean) this.securityContext.getUserPrincipal();
		}
		return this.sb;
	}
	
	public String getUserLC() {
		return getSessionBean() != null ? getSessionBean().getSessionId() : null;
	}
	
	public UUID getUserId() {
		return getSessionBean() != null ? getSessionBean().getUserId() : null;
	}
	
	protected String callMethod(final String methodName, final Params params) throws IOException {
		return opsCall(new OpGET(CLICK_URL, "componentClass", getClass().getName(), "componentId", getId(), "method", methodName, "params", params.toString()));
	}
	
	public String toHtml() throws IOException {
		return new HtmlCanvas().render(this).toHtml();
	}

	public String getId() {
		if (id == null)
			id = UUID.random().getId();
		return id;
	}

	public Component setId(String id) {
		this.id = id;
		return this;
	}
	
	public Component setSessionBean(final SecurityContext sc) {
		this.securityContext = sc;
		this.sb = (SessionBean) sc.getUserPrincipal();
		return this;
	}
	
	public Component setSessionBean(final SessionBean sb) {
		this.sb = sb;
		return this;
	}
	
	public SessionBean getSessionBean(final SecurityContext sc) {
		return (SessionBean) sc.getUserPrincipal();
	}

	public Params getParams() {
		if (params == null)
			params = new Params();
		return params;
	}

	public Component setParams(Params params) {
		this.params = params;
		return this;
	}
	
	public boolean hasRight(final String right) {
		if (getSessionBean() == null)
			return false;
		
		return Allowed.isAllowed(getSessionBean().getUserId(), right);
	}
	
	protected boolean isOpListDefined() {
		return opList != null && opList.size() > 0;
	}

	public OpList getOpList() {
		if (opList == null)
			opList = new OpList();
		return opList;
	}

	private void setOpList(OpList opList) {
		this.opList = opList;
	}

	public SecurityContext getSecurityContext() {
		return securityContext;
	}

	protected void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}

	public UriInfo getUriInfo() {
		return uriInfo;
	}

	private void setUriInfo(UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
}
