package net.hypki.libs5.pjf.components.ops;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class OpComponentClick extends Op {

	public OpComponentClick(final Component component, final String methodToCall) {
		super("post");
		
		Params params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
				
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Component component, final String methodToCall, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}

	public OpComponentClick(final Class<? extends Component> componentClass, final String methodToCall) {
		super("post");
		
		final String componentId = UUID.random().getId();
		Params params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClass, final String methodToCall, final Params params) {
		super("post");
		
		final String componentId = UUID.random().getId();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClass, final UUID componentId, final String methodToCall) {
		super("post");
		
		Params params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId.getId())
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClassToCall, 
			final String componentIdToSerialize, 
			final String componentMethodToCall) {
		super("post");
		
		Params params = new Params();
		
		params
			.add("componentClass", componentClassToCall.getName())
			.add("componentId", componentIdToSerialize)
			.add("method", componentMethodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentIdToSerialize + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClass, final UUID componentId, 
			final String methodToCall, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId.getId())
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final String componentClass, final String componentId, 
			final String methodToCall, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass)
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClass, final String componentId, final String methodToCall, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Component component, final String methodToCall, final JsonElement confirmation, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		if (confirmation != null)
			set("confirmation", confirmation);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClass, final String componentId, final String methodToCall, final JsonElement confirmation, Params params) {
		super("post");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		if (confirmation != null)
			set("confirmation", confirmation);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
}
