package net.hypki.libs5.pjf.components.buttons;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class Button implements Renderable {
	
	private boolean separator = false;
	
	private boolean separatorVertical = false;

	private String name = null;
	
	private String glyphicon = null;
	
	private String awesomeicon = null;
	
	private OpList opList = null;
	
	private List<Button> buttons = null;
	
	private boolean visible = true;
	
	private boolean enabled = true;
	
	private String htmlClass = null;
	
	private String replacedHtmlClass = null;
	
	private boolean stopPropagation = false;
	
	private ButtonSize buttonSize = ButtonSize.NORMAL;
	
	private String tooltip = null;
	
	public Button() {
		
	}
	
	public Button(String name) {
		setName(name);
	}
	
	public Button(String name, OpList opList) {
		setName(name);
		setOpList(opList);
	}
	
	public Button(String name, Op op) {
		setName(name);
		getOpList().add(op);
	}
	
	@Override
	public String toString() {
		return getName();
	}

	public String getName() {
		return name;
	}
	
	public String getNameNotNull() {
		return name != null ? name : "";
	}

	public Button setName(String name) {
		this.name = name;
		return this;
	}

	public OpList getOpList() {
		if (opList == null)
			opList = new OpList();
		return opList;
	}

	public Button setOpList(OpList opList) {
		this.opList = opList;
		return this;
	}
	
	public Button add(Op op) {
		getOpList().add(op);
		return this;
	}
	
	public Button add(boolean addIfTrue, Op op) {
		if (addIfTrue)
			getOpList().add(op);
		return this;
	}
	
	public Button add(OpList op) {
		getOpList().add(op);
		return this;
	}
	
	public Button add(boolean addIfTrue, OpList op) {
		if (addIfTrue)
			getOpList().add(op);
		return this;
	}

	public List<Button> getButtons() {
		if (buttons == null)
			buttons = new ArrayList<Button>();
		return buttons;
	}

	public Button setButtons(List<Button> buttons) {
		this.buttons = buttons;
		return this;
	}
	
	public Button addButton(Button button) {
		if (button != null)
			getButtons().add(button);
		return this;
	}

	public boolean isSeparator() {
		return separator;
	}

	public Button setSeparator(boolean separator) {
		this.separator = separator;
		return this;
	}

	public String getGlyphicon() {
		return glyphicon;
	}

	public Button setGlyphicon(String glyphicon) {
		this.glyphicon = glyphicon;
		return this;
	}

	public String getAwesomeicon() {
		return awesomeicon;
	}

	public Button setAwesomeicon(String awesomeicon) {
		this.awesomeicon = awesomeicon;
		return this;
	}

	public boolean isHierarchical() {
//		if (this.buttons == null || this.buttons.size() <= 1)
//			return false;
//		for (Button button : buttons) {
//			if (button.getButtons().size() > 0)
//				return true;
//		}
//		return false;//
		return this.buttons != null && this.buttons.size() > 0;
	}
	
	public boolean isInputButton() {
		return this instanceof InputButton;
	}

	public boolean isIconDefined() {
		return isGlyphiconDefined() || isAwesomeDefined();
	}
	
	public boolean isNameDefined() {
		return getName() != null && getName().length() > 0;
	}

	public boolean isGlyphiconDefined() {
		return getGlyphicon() != null;
	}
	
	public boolean isAwesomeDefined() {
		return getAwesomeicon() != null;
	}

	public boolean isVisible() {
		return visible;
	}

	public Button setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public String getHtmlClass() {
		return htmlClass;
	}

	public Button setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
		return this;
	}
	
	public boolean isHtmlClassDefined() {
		return notEmpty(getHtmlClass());
	}

	public boolean isStopPropagation() {
		return stopPropagation;
	}

	public Button setStopPropagation(boolean stopPropagation) {
		this.stopPropagation = stopPropagation;
		return this;
	}
	
	public String getActualHtmlClasses() {
		if (StringUtilities.notEmpty(getReplacedHtmlClass()))
			return getReplacedHtmlClass();
		else
			return "btn btn-default " + ButtonGroup.getBootstrapSize(getButtonSize()) + " " 
    				+ (isHtmlClassDefined() ? getHtmlClass() : "");
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
//		if (!isVisible())
//			return;
		
		html
			.button(type("button")
					.title(notEmpty(getTooltip()) ? getTooltip() : "")
	        		.class_(getActualHtmlClasses())
//					.class_(getHtmlClass())
//	        		.data("toggle", "dropdown")
	        		.add("style", !isVisible() ? "display: none;" : "")
	        		.onClick(getOpList().toStringOnclick() + (isStopPropagation() ? "; event.stopPropagation();" : ""))
//	        		.onClick(getOpList().toStringOnclick() + (isStopPropagation() ? "; event.stopPropagation();" : ""))
	        		)
	        	.i(class_((isIconDefined() ? (isGlyphiconDefined() ? "glyphicon glyphicon-" + getGlyphicon() : "fa fa-" + getAwesomeicon()) : "")))
	        		.content(isNameDefined() ? " " + getName() : "", false)
	        ._button()
	        ;
	}

	public ButtonSize getButtonSize() {
		return buttonSize;
	}

	public Button setButtonSize(ButtonSize buttonSize) {
		this.buttonSize = buttonSize;
		return this;
	}

	public String getTooltip() {
		return tooltip;
	}
	
	public String getTooltipNotNull() {
		return tooltip != null ? tooltip : "";
	}

	public Button setTooltip(String tooltip) {
		this.tooltip = tooltip;
		return this;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	public boolean isDisabled() {
		return !enabled;
	}

	public Button setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public boolean isSeparatorVertical() {
		return separatorVertical;
	}

	public Button setSeparatorVertical(boolean separatorVertical) {
		this.separatorVertical = separatorVertical;
		return this;
	}

	public String getReplacedHtmlClass() {
		return replacedHtmlClass;
	}

	public Button setReplacedHtmlClass(String replacedHtmlClass) {
		this.replacedHtmlClass = replacedHtmlClass;
		return this;
	}
}
