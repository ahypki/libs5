package net.hypki.libs5.pjf.components.labels;

public class InfoRemark extends Remark {

	public InfoRemark(String caption, String msg) {
		super(MessageType.INFO, caption, msg);
	}
}
