package net.hypki.libs5.pjf.components.ops;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;

public class OpComponentTimer extends Op {

	public OpComponentTimer(final Component component, final String methodToCall, final int delayMs) {
		super("timer");
		
		set("url", Component.CLICK_URL);
		set("ms", delayMs);
		set("restMethod", RestMethod.POST.toString());
		set("jQuerySelector", "#" + component.getId());
		set("timerId", UUID.random().getId());
		
		Params params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
//		set("params", UrlUtilities.toQueryString("componentClass", component.getClass().getName(),
//				"componentId", component.getId(),
//				.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
//				"method", methodToCall,
//				"params", "")
//			);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
	
	public OpComponentTimer(final Class<? extends Component> componentClass, final UUID componentId, final String methodToCall, final Params params, final int delayMs) {
		super("timer");
		
		set("url", Component.CLICK_URL);
		set("ms", delayMs);
		set("restMethod", RestMethod.POST.toString());
		set("jQuerySelector", "#" + componentId.getId());
		set("timerId", UUID.random().getId());
		
		Params paramsLoc = params != null ? params : new Params();
		
		paramsLoc
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId.getId())
//			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
//		set("params", UrlUtilities.toQueryString("componentClass", component.getClass().getName(),
//				"componentId", component.getId(),
//				.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
//				"method", methodToCall,
//				"params", "")
//			);
		set("params", paramsLoc.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId.getId() + " :input').serialize()")); 
	}
	
	public OpComponentTimer(final Class<? extends Component> componentClass, final UUID componentId, final String methodToCall, final int delayMs) {
		super("timer");
		
		set("url", Component.CLICK_URL);
		set("ms", delayMs);
		set("restMethod", RestMethod.POST.toString());
		set("jQuerySelector", "#" + componentId.getId());
		set("timerId", UUID.random().getId());
		
		Params paramsLoc = new Params();
		
		paramsLoc
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId.getId())
//			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
//		set("params", UrlUtilities.toQueryString("componentClass", component.getClass().getName(),
//				"componentId", component.getId(),
//				.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
//				"method", methodToCall,
//				"params", "")
//			);
		set("params", paramsLoc.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId.getId() + " :input').serialize()")); 
	}
	
	public OpComponentTimer(final Component component, final String methodToCall, final int delayMs, final Params params) {
		super("timer");
		
		set("url", Component.CLICK_URL);
		set("ms", delayMs);
		set("restMethod", RestMethod.POST.toString());
		set("jQuerySelector", "#" + component.getId());
		set("timerId", UUID.random().getId());
		
		Params paramsLocal = params != null ? params : new Params();
		
		paramsLocal
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
			.add("method", methodToCall);
		
//		set("params", UrlUtilities.toQueryString("componentClass", component.getClass().getName(),
//				"componentId", component.getId(),
//				.add("componentData", Base64Utils.encode(JsonUtils.objectToString(component)))
//				"method", methodToCall,
//				"params", "")
//			);
		set("params", paramsLocal.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
}
