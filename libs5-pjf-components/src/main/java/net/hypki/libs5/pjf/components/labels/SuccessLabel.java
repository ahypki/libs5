package net.hypki.libs5.pjf.components.labels;

public class SuccessLabel extends Label {

	public SuccessLabel(String msg) {
		super(MessageType.SUCCESS, msg);
	}
}
