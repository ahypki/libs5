package net.hypki.libs5.pjf.components;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class PagerRenderable implements Renderable {
	
	private Component component = null;
	private String methodToCall = null;
	private int currentPage = 0;
	private int perPage = 0;
	private long maxElements = 0;
	private String htmlClass = "";
	private boolean firstNextBtnsOnly = false;
	private ButtonSize buttonSize = ButtonSize.NORMAL;

	public PagerRenderable(Component component, String methodToCall, int currentPage, int perPage, long maxElements,
			ButtonSize buttonSize) {
		this.component = component;
		this.methodToCall = methodToCall;
		this.currentPage = currentPage;
		this.perPage = perPage;
		this.maxElements = maxElements;
		this.buttonSize = buttonSize;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		final int toPage = min(max(0, currentPage - 2) + 5, ((int)(maxElements / perPage)) + (maxElements % perPage == 0 ? 0 : 1));
		final int fromPage = max(0, toPage - 5);
		final int maxPage = ((int)(maxElements / perPage)) + (maxElements % perPage == 0 ? 0 : 1) - 1;
		
		ButtonGroup gr = new ButtonGroup();
		
		gr
			.setButtonSize(buttonSize)
			.addButton(new Button(isFirstNextBtnsOnly() ? "First" : "&laquo;")
					.setHtmlClass(getHtmlClass())
					.add(new OpComponentClick(component, methodToCall, new Params().add("page", 0))));
		
//		if (maxElements >= 0 && maxElements != Integer.MAX_VALUE)
		if (!isFirstNextBtnsOnly())
			for (int page = fromPage; page < toPage; page++) {
				
						if (page == currentPage)
							gr
								.addButton(new Button(String.valueOf(page + 1))
										.setHtmlClass("selected " + getHtmlClass()));
						else
							gr
								.addButton(new Button(String.valueOf(page + 1))
										.setHtmlClass(getHtmlClass())
										.add(new OpComponentClick(component, methodToCall, new Params().add("page", page))))
					;
			}
		
		gr
			.addButton(new Button(isFirstNextBtnsOnly() ? "Next" : "&raquo;")
				.setHtmlClass(getHtmlClass())
				.add(new OpComponentClick(component, methodToCall, 
						new Params().add("page", isFirstNextBtnsOnly() ? currentPage + 1 : maxPage))));
		
		html
			.div(class_("pager"))
				.render(gr)
			._div()
			;
	}

	public String getHtmlClass() {
		return htmlClass;
	}

	public PagerRenderable setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
		return this;
	}

	public boolean isFirstNextBtnsOnly() {
		return firstNextBtnsOnly;
	}

	public PagerRenderable setFirstNextBtnsOnly(boolean firstNextBtnsOnly) {
		this.firstNextBtnsOnly = firstNextBtnsOnly;
		return this;
	}
}
