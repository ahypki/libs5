package net.hypki.libs5.pjf.components.buttons;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.onClick;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class ButtonGroup extends Component {
	
	private ButtonSize buttonSize = ButtonSize.EXTRA_SMALL;
	
	private List<Button> buttons = null;
	
	private boolean vertical = false;
	
	private boolean visible = true;
	
	private String additionalCssClasses = null;
	
	public ButtonGroup() {
		
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
		    .div(class_("button-group " + getAdditionalCssClassesNotNull() + (isVertical() ? " btn-group-vertical" : " btn-group"))
		    		.role("group")
		    		.add("style", !isVisible() ? "display: none;" : "")
		    		.id(getId()));
		
		if (buttons != null)
			for (Button button : buttons) {
				addButtonHtml(button, html);
			}
		
		html
		    ._div();
	}
	
	private void addButtonHtml(Button button, HtmlCanvas html) throws IOException {
		if (button.isHierarchical()) {
			html
				.div(class_("btn-group " + (button.isHtmlClassDefined() ? button.getHtmlClass() : ""))
						.role("group"))
		            .button(type("button")
							.title(notEmpty(button.getTooltip()) ? button.getTooltip() : "")
		            		.class_("btn btn-default dropdown-toggle " + getBootstrapSize(getButtonSize()) + " "
		            				+ (button.isHtmlClassDefined() ? button.getHtmlClass() : ""))
		            		.data("toggle", "dropdown")
		            		.add("style", !button.isVisible() ? "display: none;" : "")
		            		)
		            	.i(class_(button.isIconDefined() ? (button.isGlyphiconDefined() ? "glyphicon glyphicon-" + button.getGlyphicon() : "fa fa-" + button.getAwesomeicon()) : ""))
		            	._i()
            			.write(button.isNameDefined() ? " " + button.getName() : "", false)
		            ._button()
		            .ul(class_("dropdown-menu")
		            		.role("menu"));
			
			for (Button button2 : button.getButtons()) {
				if (button2.isSeparator()) {
					html
						.li(class_(button2.isSeparatorVertical() ? "divider-vertical" : "divider")
								.role("separator"))
							.content("");
				} else {
					html
						.li()
			                .a(onClick(opsCall(button2.getOpList()) + (button.isStopPropagation() ? "; event.stopPropagation();" : ""))
			                		.class_(getBootstrapSize(getButtonSize()) + " " + 
			                				(button2.isHtmlClassDefined() ? button2.getHtmlClass() : ""))
		                			.add("style", !button2.isVisible() ? "display: none;" : "")
		                			.disabled_if(button2.isDisabled()))
	                			.i(class_(button2.isIconDefined() ? (button2.isGlyphiconDefined() ? "glyphicon glyphicon-" + button2.getGlyphicon() : "fa fa-" + button2.getAwesomeicon()) : ""))
	                			._i()
	                    		.write(" " + button2.getName(), false)
	                    	._a()
			            ._li();
				}
			}
		                
		     html
		            ._ul()
		        ._div();
		} else if (button.isInputButton()) {
			html
				.input(name(button.getName())
						.type("text")
						.class_("" + button.getName())
						.add("style", !isVisible() ? "display: none;" : "")
						.onKeyup(button.getOpList().toStringOnclick()));
		} else {
			html
				.button(type("button")
						.title(notEmpty(button.getTooltip()) ? button.getTooltip() : "")
		        		.class_(button.getActualHtmlClasses())// "btn btn-default " + getBootstrapSize(getButtonSize()) + (button.isHtmlClassDefined() ? " " + button.getHtmlClass() : ""))
                		.add("style", !button.isVisible() ? "display: none;" : "")
		        		.onClick(opsCall(button.getOpList()) + (button.isStopPropagation() ? "; event.stopPropagation();" : ""))
		        		.disabled_if(button.isDisabled())
		        		)
		        	.if_(button.isIconDefined())
		        		.i(class_(button.isIconDefined() ? (button.isGlyphiconDefined() ? "glyphicon glyphicon-" + button.getGlyphicon() : "fa fa-" + button.getAwesomeicon()) : ""))
		            		.content(button.isNameDefined() ? " " + button.getName() : "", false)
		            ._if()
		            .if_(!button.isIconDefined())
		            	.span()
		            		.content(button.getName(), false)
		            ._if()
	           ._button();
		}
	}

	public static String getBootstrapSize(ButtonSize size) {
		switch (size) {
		case EXTRA_SMALL:
			return "btn-xs";
		case SMALL:
			return "btn-sm";
		case NORMAL:
			return "";
		default:
			return "btn-sm";
		}
	}

	public ButtonSize getButtonSize() {
		return buttonSize;
	}

	public ButtonGroup setButtonSize(ButtonSize buttonSize) {
		this.buttonSize = buttonSize;
		return this;
	}

	public List<Button> getButtons() {
		if (buttons == null)
			buttons = new ArrayList<>();
		return buttons;
	}

	public ButtonGroup setButtons(List<Button> buttons) {
		this.buttons = buttons;
		return this;
	}
	
	public ButtonGroup addButton(boolean addIfTrue, Button button) {
		if (addIfTrue)
			addButton(button);
		return this;
	}

	public ButtonGroup addButton(Button button) {
		if (button != null)
			getButtons().add(button);
		return this;
	}
	
	public ButtonGroup addButton(String location, Button button) {
		if (button != null) {
			String[] locationParts = split(location,  '/');
			
			if (locationParts.length < 1) {
				getButtons().add(button);
				return this;
			} else  {
				for (Button btn : getButtons()) {
					if (btn.getName().equalsIgnoreCase(locationParts[0])) {						
						addButton(locationParts, 1, button, btn);
						return this;
					}
				}
				getButtons().add(new Button().setName(locationParts[0]));
				addButton(locationParts, 1, button, getButtons().get(getButtons().size() - 1));
				return this;
			}
		}
		return this;
	}
	
	private void addButton(String [] location, int locationIdex, Button buttonToAdd, Button currentButton) {
		if (location.length == locationIdex) {
			currentButton.getButtons().add(buttonToAdd);
			return;
		}
		
		final String currentName = location[locationIdex];
		for (Button btn : currentButton.getButtons()) {
			if (btn.getName().equalsIgnoreCase(currentName)) {
				addButton(location, locationIdex + 1, buttonToAdd, btn);
				return;
			}
		}
		currentButton.getButtons().add(new Button().setName(currentName));
		addButton(location, locationIdex + 1, buttonToAdd, currentButton.getButtons().get(currentButton.getButtons().size() - 1));
	}
	
	public ButtonGroup addButton(List<Button> button) {
		if (button != null) {
			for (Button button2 : button) {				
				getButtons().add(button2);
			}
		}
		return this;
	}

	public boolean isVertical() {
		return vertical;
	}

	public ButtonGroup setVertical(boolean vertical) {
		this.vertical = vertical;
		return this;
	}

	public boolean isVisible() {
		return visible;
	}

	public ButtonGroup setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public String getAdditionalCssClasses() {
		return additionalCssClasses;
	}
	
	public String getAdditionalCssClassesNotNull() {
		return getAdditionalCssClasses() != null ? getAdditionalCssClasses() : "";
	}

	public ButtonGroup setAdditionalCssClasses(String additionalCssClasses) {
		this.additionalCssClasses = additionalCssClasses;
		return this;
	}
}
