package net.hypki.libs5.pjf.components.jersey;

import java.io.Serializable;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.user.Allowed;

    
public class SessionBean implements Serializable, java.security.Principal {
    private static final long serialVersionUID = -7483270672993892282L;
     
    private String sessionId;
    
    private UUID userId;
    
    private String userName;
    
    public SessionBean() {
		
	}
    
    public SessionBean(String userLC, UUID userId) {
		setSessionId(userLC);
		setUserId(userId);
	}
    
    @Override
    public String toString() {
    	return "sessionId " + sessionId + " userId " + userId + " userName " + userName;
    }

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = new UUID(userId);
	}
	
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getName() {
		return getUserId().getId();
	}
}
