package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.*;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class IntComponent extends Component {
	
	private String name = null;
	
	private int value = 0;
	
	private String additionalClass = null;
	
	private boolean visible = true;

	public IntComponent() {
		
	}
	
	public IntComponent(String formName, int value) {
		setName(formName);
		setValue(value);
	}

	public int getValue() {
		return value;
	}

	public IntComponent setValue(int value) {
		this.value = value;
		return this;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("input-group int-component " + getAdditionalClassNotNull())
					.style(isVisible() ? "" : "display: none;"))
				.input(type("text")
						.class_("value form-control boxed")
						.name(getName())
						.add("placeholder", "value")
						.value(String.valueOf(getValue())))
				.span(class_("input-group-btn"))
					.button(class_("btn btn-primary-outline")
							.type("button")
							.onClick("$('#" + getId() + " .value').val(parseInt($('#" + getId() + " .value').val(), 10) + 1);"))
//						.i(class_("glyphicon glyphicon-plus"))
							.content("+")
//						._i()
//					._button()
					.button(class_("btn btn-primary-outline")
							.type("button")
							.onClick("$('#" + getId() + " .value').val(parseInt($('#" + getId() + " .value').val(), 10) - 1);"))
						.content("-")
				._span()
			._div();
	}

	public String getName() {
		return name;
	}

	public IntComponent setName(String name) {
		this.name = name;
		return this;
	}

	public String getAdditionalClass() {
		return additionalClass;
	}
	
	public String getAdditionalClassNotNull() {
		return additionalClass != null ? additionalClass : "";
	}

	public IntComponent setAdditionalClass(String additionalClass) {
		this.additionalClass = additionalClass;
		return this;
	}

	public boolean isVisible() {
		return visible;
	}

	public IntComponent setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}
}
