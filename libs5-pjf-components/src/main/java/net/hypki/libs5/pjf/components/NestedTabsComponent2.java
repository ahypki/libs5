package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.collections.NestedKeyMapList;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

public class NestedTabsComponent2 extends Component {
	
	private NestedKeyMapList<Component> tabs = new NestedKeyMapList<>("_");
	
	private Map<String, OpList> ops = new HashMap<>();
	
	private String defaultKey = null;

	public NestedTabsComponent2() {
		
	}
	
	public NestedTabsComponent2 add(String path, Component component) {
		tabs.add(path, component);
		return this;
	}
	
	public NestedTabsComponent2 add(String path, Component component, boolean isDefault) {
		String fullpath = tabs.add(path, component);
		if (isDefault)
			setDefaultKey(fullpath);
		return this;
	}
	
	public NestedTabsComponent2 add(String path, Component component, OpList ops) {
		String fullpath = tabs.add(path, component);
		this.ops.put(fullpath, ops);
		return this;
	}
	
	public NestedTabsComponent2 add(String path, Component component, OpList ops, boolean isDefault) {
		String fullpath = tabs.add(path, component);
		this.ops.put(fullpath, ops);
		if (isDefault)
			setDefaultKey(fullpath);
		return this;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html.div(id(getId())
				.class_("pjf-tabs"));
		
		// rendering breadcrumbs
		html
			.div(class_("pjf-tabs-breadcrumb"))
//				.content("");
				.span(class_(""))
					.write(" <i class=\"glyphicon glyphicon-stop\" > </i>", false)
				._span();
		if (getDefaultKey() != null) {
			String thisKey = tabs.getSeparator();
			String thisKeyEncoded = tabs.getSeparator();
			int currentLevel = 0;
			for (String keyTmp : StringUtilities.split(getDefaultKey(), tabs.getSeparator())) {
				thisKey = thisKey + keyTmp + tabs.getSeparator();
				String parentKeyEncoded = thisKeyEncoded;
				thisKeyEncoded = thisKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator();
				
				OpList ops = new OpList()
					.add(new OpHide("#" + getId() + " .pjf-tabs-comp"))
					.add(new OpShow("#" + getId() + " .pjf-tabs-comp-" + thisKeyEncoded))
					.add(new OpAddClass("#" + getId() + " .pjf-tabs-this-" + thisKeyEncoded, "selected"))
					.add(new OpHide("#" + getId() + " .pjf-tabs-header"))
					;
				
				// adding breadcrumb
				OpList breadOps = new OpList();
				for (int i = currentLevel; i <= tabs.getMaxLevel(); i++)
					breadOps.add(new OpRemove("#" + getId() + " .pjf-tabs-breadcrumb-level" + i));
				breadOps
					.add(ops)
					.add(new OpShow("#" + getId() + " .pjf-tabs-parent-" + parentKeyEncoded));
//				ops
//					.add(new OpAppend("#" + getId() + " .pjf-tabs-breadcrumb", breadcrumb.toHtml()))
//					;
//				if ( && getDefaultKey().startsWith(thisKey))
				
				HtmlCanvas breadcrumb = new HtmlCanvas()
						.span(class_("pjf-tabs-breadcrumb-key pjf-tabs-breadcrumb-level" + currentLevel)
								.onClick(breadOps.toStringOnclick()))
							.write(keyTmp)
						._span()
						.if_(tabs.hasChildrenKeys(thisKey))
							.span(class_("pjf-tabs-breadcrumb-level" + currentLevel))
								.write(" <i class=\"glyphicon glyphicon-chevron-right\"  > </i> ", false)
							._span()
						._if()
						;
				
				html
					.write(breadcrumb.toHtml(), false);
				
//				ops
//					.add(new OpShow("#" + getId() + " .pjf-tabs-parent-" + thisKeyEncoded));
//				
//				if (this.ops.get(thisKey) != null)
//					ops.add(this.ops.get(thisKey));
//				
//				html
//					.span(class_("pjf-tabs-header pjf-tabs-parent-" + parentKeyEncoded)
//							.style(currentLevel > 0 ? "display: none;" : "")
//							.onClick(ops.toStringOnclick()))
//						.content(keyTmp);
				
				currentLevel++;
			}
		}
		html
			._div()
		;
		
		renderKeys(html, null, tabs.getSeparator(), 0, tabs.getSeparator());
		
		renderComponents(html, null, tabs.getSeparator(), 0, tabs.getSeparator());
		
		html._div();
	}
	
	private boolean renderKeys(HtmlCanvas html, String currentKey, String parentKey, int currentLevel, String parentKeyEncoded) throws IOException {
		if (!tabs.hasChildrenKeys(parentKey))
			return false;
		
		html
			.div(class_("pjf-tabs-header" + currentLevel)
//				.style(currentLevel > 0 ? "display: none;" : "")
				);
		
		for (String keyTmp : tabs.getKeys(parentKey)) {
			String thisKey = parentKey + keyTmp + tabs.getSeparator();
			String thisKeyEncoded = parentKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator();
			
			OpList ops = new OpList()
				.add(new OpHide("#" + getId() + " .pjf-tabs-comp"))
				.add(new OpShow("#" + getId() + " .pjf-tabs-comp-" + thisKeyEncoded))
				.add(new OpAddClass("#" + getId() + " .pjf-tabs-this-" + thisKeyEncoded, "selected"))
				.add(new OpHide("#" + getId() + " .pjf-tabs-header"))
				;
			
			// adding breadcrumb
			OpList breadOps = new OpList();
			for (int i = currentLevel; i <= tabs.getMaxLevel(); i++)
				breadOps.add(new OpRemove("#" + getId() + " .pjf-tabs-breadcrumb-level" + i));
			breadOps
				.add(ops)
				.add(new OpShow("#" + getId() + " .pjf-tabs-parent-" + parentKeyEncoded));
			ops
				.add(new OpAppend("#" + getId() + " .pjf-tabs-breadcrumb", new HtmlCanvas()
						.span(class_("pjf-tabs-breadcrumb-key pjf-tabs-breadcrumb-level" + currentLevel)
								.onClick(breadOps.toStringOnclick()))
							.content(keyTmp)
						.if_(tabs.hasChildrenKeys(thisKey))
							.span(class_("pjf-tabs-breadcrumb-level" + currentLevel))
								.content(" <i class=\"glyphicon glyphicon-chevron-right\" /> ", false)
						._if()
						.toHtml()))
				;
			
			ops
				.add(new OpShow("#" + getId() + " .pjf-tabs-parent-" + thisKeyEncoded));
			
			if (this.ops.get(thisKey) != null)
				ops.add(this.ops.get(thisKey));
			
			html
				.span(class_("pjf-tabs-header pjf-tabs-parent-" + parentKeyEncoded)
						.style(currentLevel > 0 ? "display: none;" : getDefaultKey() != null ? "display: none;" : "")
						.onClick(ops.toStringOnclick()))
					.content(keyTmp);
		}
		
		html
			._div();
						
		for (String keyTmp : tabs.getKeys(parentKey)) {
			String thisKey = parentKey + keyTmp + tabs.getSeparator();
			String thisKeyEncoded = parentKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator();
			renderKeys(html, keyTmp, thisKey, currentLevel + 1, thisKeyEncoded);
		}
		
		return true;
	}
	
	private boolean renderComponents(HtmlCanvas html, String currentKey, String currentFullKey, int currentLevel, String currentFullKeyEncoded) throws IOException {
		if (!tabs.hasChildrenKeys(currentFullKey))
			return false;
				
		for (String keyTmp : tabs.getKeys(currentFullKey)) {
			String thisKey = currentFullKey + keyTmp + tabs.getSeparator();
			if (!tabs.isEmpty(thisKey)) {
				boolean isDefault = getDefaultKey() != null && getDefaultKey().equals(thisKey);
				html
					.div(class_("pjf-tabs-comp pjf-tabs-comp-" + currentFullKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator())
							.style(isDefault ? "" : "display: none;"));
				for (Component comp : tabs.get(thisKey)) {
					html.render(comp);
				}
				html
					._div();
			}
		}
		
		for (String keyTmp : tabs.getKeys(currentFullKey)) {
			renderComponents(html, 
					keyTmp, 
					currentFullKey + keyTmp + tabs.getSeparator(), 
					currentLevel + 1, 
					currentFullKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator());
		}
		
		return true;
	}

	private Map<String, OpList> getOps() {
		return ops;
	}

	private void setOps(Map<String, OpList> ops) {
		this.ops = ops;
	}

	public String getDefaultKey() {
		return defaultKey;
	}

	public void setDefaultKey(String defaultKey) {
		this.defaultKey = defaultKey;
	}
}
