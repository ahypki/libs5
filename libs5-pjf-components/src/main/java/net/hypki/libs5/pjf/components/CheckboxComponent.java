package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.*;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class CheckboxComponent extends Component {
	
	private String name = null;
	
	private String caption = null;

	public CheckboxComponent() {
		
	}
	
	public CheckboxComponent(String name, String caption) {
		setName(name);
		setCaption(caption);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("custom-control custom-checkbox"))
				.input(class_("custom-control-input")
						.name(getName())
						.id(getId() + "-" + getName())
						.type("checkbox"))
				.label(class_("custom-control-label")
						.for_(getId() + "-" + getName()))
					.content(getCaption())
			._div();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
}
