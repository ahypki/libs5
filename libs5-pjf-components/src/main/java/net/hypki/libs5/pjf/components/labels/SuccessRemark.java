package net.hypki.libs5.pjf.components.labels;

public class SuccessRemark extends Remark {

	public SuccessRemark(String caption, String msg) {
		super(MessageType.SUCCESS, caption, msg);
	}
}
