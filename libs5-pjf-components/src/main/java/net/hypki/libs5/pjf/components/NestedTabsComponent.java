package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.collections.NestedKeyMapList;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

public class NestedTabsComponent extends Component {
	
	private NestedKeyMapList<Component> tabs = new NestedKeyMapList<>("_");
	
	private Map<String, OpList> ops = new HashMap<>();

	public NestedTabsComponent() {
		
	}
	
	public NestedTabsComponent add(String path, Component component) {
		tabs.add(path, component);
		return this;
	}
	
	public NestedTabsComponent add(String path, Component component, OpList ops) {
		String fullpath = tabs.add(path, component);
		this.ops.put(fullpath, ops);
		return this;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html.div(id(getId())
				.class_("pjf-tabs"));
		
//		html
//			.div(class_("pjf-tabs-header pjf-tabs-header0"))
//				.content(" / ");
		
		renderOn(html, null, tabs.getSeparator(), 0, tabs.getSeparator());
		
		html._div();
	}
	
	private boolean renderOn(HtmlCanvas html, String currentKey, String currentFullKey, int currentLevel, String currentFullKeyEncoded) throws IOException {
		if (!tabs.hasChildrenKeys(currentFullKey))
			return false;
		
		html
			.div(class_("pjf-tabs-header" + currentLevel + " pjf-tabs-parent-" + currentFullKeyEncoded)
					.style(currentLevel > 0 ? "display: none;" : ""));
		
		for (String keyTmp : tabs.getKeys(currentFullKey)) {
			String thisKeyNotEnc = currentFullKey+ keyTmp + tabs.getSeparator();
			String thisKey = currentFullKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator();
			
			OpList ops = new OpList();
			for (int i = currentLevel + 1; i <= tabs.getMaxLevel(); i++) {
				ops.add(new OpHide("#" + getId() + " .pjf-tabs-header" + i));
			}
			ops
				.add(new OpShow("#" + getId() + " .pjf-tabs-parent-" + thisKey))
				.add(new OpHide("#" + getId() + " .pjf-tabs-comp"))
				.add(new OpShow("#" + getId() + " .pjf-tabs-comp-" + thisKey))
				.add(new OpRemoveClass("#" + getId() + " .pjf-tabs-header" + currentLevel + " span", "selected"))
				.add(new OpAddClass("#" + getId() + " .pjf-tabs-this-" + thisKey, "selected"));
			
			if (this.ops.get(thisKeyNotEnc) != null)
				ops.add(this.ops.get(thisKeyNotEnc));
			
			html
				.span(class_("pjf-tabs-header pjf-tabs-this-" + thisKey)
						.onClick(ops.toStringOnclick()))
				.content(keyTmp);
		}
		
		html
			._div();
		
		for (String keyTmp : tabs.getKeys(currentFullKey)) {
			String thisKey = currentFullKey + keyTmp + tabs.getSeparator();
			if (!tabs.isEmpty(thisKey)) {
				html
					.div(class_("pjf-tabs-comp pjf-tabs-comp-" + currentFullKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator())
							.style("display: none;"));
				for (Component comp : tabs.get(thisKey)) {
					html.render(comp);
				}
				html
					._div();
			}
		}
		
		for (String keyTmp : tabs.getKeys(currentFullKey)) {
			renderOn(html, 
					keyTmp, 
					currentFullKey + keyTmp + tabs.getSeparator(), 
					currentLevel + 1, 
					currentFullKeyEncoded + keyTmp.replaceAll("[\\s]+", "") + tabs.getSeparator());
		}
		
		return true;
	}

	private Map<String, OpList> getOps() {
		return ops;
	}

	private void setOps(Map<String, OpList> ops) {
		this.ops = ops;
	}
}
