package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.UrlUtilities;

public class ChooseComponent extends Component {
	
	@Expose
	private List<String> options = null;
	
	@Expose
	private String selectedOption = null;
	
	private OpList opsOnChange = null;

	public ChooseComponent() {
		
	}
	
	public ChooseComponent(String ... options) {
		if (options != null)
			for (String opt : options) {
				getOptions().add(opt);
			}
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("choose-component")
					.id(getId()))
				.input(type("hidden")
						.class_("selected-option-id")
						.name("selected-option-id"))
				.input(type("hidden")
						.class_("selected-option-text")
						.name("selected-option-text"));
		
		int counter = 0;
		for (String option : getOptions()) {
			String option64 = option.replaceAll("[\\s]+", "_");
			
			html
				.div(class_("custom-control custom-radio")
						.onClick(new OpList()
//									.add(new OpRemoveClass("#" + getId() + " .choose-option", "choose-selected"))
//									.add(new OpAddClass("#" + getId() + " .choose-" + option64, "choose-selected"))
									.add(new OpSetVal("#" + getId() + " .selected-option-id", counter++))
									.add(new OpSetVal("#" + getId() + " .selected-option-text", option))
									.add(getOpsOnChange()
											.addParamParam("choose-selected", option))
									.toStringOnclick()))
					.input(class_("custom-control-input")
							.name("name-" + getId())
							.id(getId() + "-" + option64)
							.type("radio"))
					.label(class_("custom-control-label")
							.for_(getId() + "-" + option64))
						.content(option)
				._div();
			
//			html
//				.span(class_("choose-option " + " choose-" + option64 + " " + (getSelectedOption().equals(option) ? "choose-selected" : ""))
//						.onClick(new OpList()
//									.add(new OpRemoveClass("#" + getId() + " .choose-option", "choose-selected"))
//									.add(new OpAddClass("#" + getId() + " .choose-" + option64, "choose-selected"))
//									.add(new OpSetVal("#" + getId() + " .selected-option-id", counter++))
//									.add(new OpSetVal("#" + getId() + " .selected-option-text", option))
//									.add(getOpsOnChange()
//											.addParamParam("choose-selected", option))
//									.toStringOnclick()))
//					.content(option);
		}
		
		html
			._div();
	}

	public List<String> getOptions() {
		if (options == null)
			options = new ArrayList<>();
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public String getSelectedOption() {
		if (selectedOption == null)
			selectedOption = getOptions().get(0);
		return selectedOption;
	}

	public ChooseComponent setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
		return this;
	}

	public OpList getOpsOnChange() {
		if (opsOnChange == null)
			opsOnChange = new OpList();
		return opsOnChange;
	}

	public ChooseComponent setOpsOnChange(OpList opsOnChange) {
		this.opsOnChange = opsOnChange;
		return this;
	}
	
	public ChooseComponent addOpsOnChange(OpList opsOnChange) {
		getOpsOnChange().add(opsOnChange);
		return this;
	}
	
	public ChooseComponent addOpsOnChange(Op opOnChange) {
		getOpsOnChange().add(opOnChange);
		return this;
	}
}
