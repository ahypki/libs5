package net.hypki.libs5.pjf.components.labels;

public class WarningRemark extends Remark {

	public WarningRemark(String caption, String msg) {
		super(MessageType.WARNING, caption, msg);
	}
}
