package net.hypki.libs5.pjf.components.buttons;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.onClick;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class MultiLevelButtonGroup extends Component {
	
	@Expose
	private List<String> pathSorted = new ArrayList<String>();
	
	@Expose
	private Map<String, OpList> pathToOpList = new HashMap<String, OpList>();
	
	@Expose
	private Map<String, OpList> pathToAdditionalOpList = new HashMap<String, OpList>();
	
	@Expose
	private Map<String, String> pathToCssClass = new HashMap<String, String>();
	
	@Expose
	private String currentPath = "/";
	
	@Expose
	private String startingText = null;
	
	@Expose
	private boolean collapsed = false;
	
	@Expose
	private boolean movePageOnUncollapsed = true;
	
	@Expose
	private String cssDivClasses = "";
	
	@Expose
	private String cssFoldersClasses = "";
	
	@Expose
	private String cssLeafsClasses = "";
	
	@Expose
	private String cssBreadcrumbsClasses = "";
	
	@Expose
	private OpList additionalOpList = new OpList();
	
	public MultiLevelButtonGroup() {
		
	}
	
	public MultiLevelButtonGroup add(String path, OpList ops) {
		return add(path, ops, null);
	}
	
	public MultiLevelButtonGroup addAdditionalOpList(String path, OpList ops) {
		if (pathToAdditionalOpList.get(path) != null)
			pathToAdditionalOpList.get(path).add(ops);
		else
			pathToAdditionalOpList.put(path, ops);
		return this;
	}
	
	public MultiLevelButtonGroup addAdditionalOpList(String path, Op op) {
		if (pathToAdditionalOpList.get(path) == null)
			pathToAdditionalOpList.put(path, new OpList());
		pathToAdditionalOpList.get(path).add(op);
		return this;
	}
	
	public MultiLevelButtonGroup add(String path, OpList ops, String cssClasses) {
		path = path != null ? path.trim() : null;
		
		if (nullOrEmpty(path))
			path = "/";
		if (path.startsWith("/") == false)
			path = "/" + path;
		
		path = path.replaceAll("//", "/");

		path = new FileExt(path).getAbsolutePath();
		
		getPathSorted().add(path);
		getPathToOpList().put(path, ops);
		if (notEmpty(cssClasses))
			getPathToCssClass().put(path, cssClasses);
		return this;
	}
	
	public MultiLevelButtonGroup sortAlphabetically() {
		Collections.sort(getPathSorted());
		
		return this;
	}
	
	public MultiLevelButtonGroup add(String path, Op op) {
		return add(path, new OpList().add(op));
	}
	
	private String escapePath(String path) {
//		return SHAManager.getSHA(path);
		return path.replaceAll("[^a-zA-Z0-9]+", "\\_");
	}
	
	public OpList onUncollapse(HtmlCanvas breadCrumb) {
		return new OpList()
			.add(new OpHide("#" + getId() + " .root"))
			.add(new OpShow("#" + getId() + " ." + escapePath("/")))
			.add(new OpSet("#" + getId() + " .mlb-breadcrumb", breadCrumb.toHtml()))
			.add(isMovePageOnUncollapsed() ? new OpJs("window.location.hash = '#" + getId() + "';") : null);
			
	}
	
	public OpList onCollapse() throws IOException {
		return new OpList()
		.add(new OpHide("#" + getId() + " .mlb"))
			.add(new OpShow("#" + getId() + " .root"))
//			.add(new OpHide("#" + getId() + " .leaf"))
			.add(new OpHide("#" + getId() + " ." + escapePath("/")))
//			.add(new OpSet("#" + getId() + " .breadcrumb", ""))
//			.add(isMovePageOnUncollapsed() ? new OpJs("window.location.hash = '#" + getId() + "';") : null)
			;
			
	}
		
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		Set<String> pathsDone = new HashSet<String>();
		Map<String, String> pathsIDs = new HashMap<>();
		
		html
			.div(id(getId())
					.class_("multilevel-button-group " + getCssDivClasses()))
				;
		
		// creating starting button
		String pathStr = "/";
		pathsIDs.put(pathStr, UUID.random().getId());
		html
			.a(class_(getCssBreadcrumbsClasses() + " mlb root ")
				.add("style", isCollapsed() ? "" : "display: none;")
				.onClick(new OpList()
								.add(getAdditionalOpList())
								.add(new OpHide("#" + getId() + " .mlb"))
								.add(new OpShow("#" + getId() + " ." + pathsIDs.get(pathStr)))
								.toStringOnclick()))
				.content(getStartingText(), false);

		// create breadcrumbs
		for (int j = 0; j < getPathSorted().size(); j++) {
			
			String crumb = "/";
			for (String partCrumb : StringUtilities.split(getPathSorted().get(j), '/')) {
				crumb += partCrumb + "/";
				
				if (pathsDone.contains(crumb))
					continue;
					
				if (pathsIDs.containsKey(crumb) == false)
					pathsIDs.put(crumb, UUID.random().getId());
			
				html
					.div(class_(" mlb mlb-breadcrumb " + pathsIDs.get(crumb))
							.add("style", "display: none;"))
						.a(class_(getCssBreadcrumbsClasses())
								.onClick(new OpList()
										.add(getAdditionalOpList())
										.add(new OpHide("#" + getId() + " .mlb"))
										.add(new OpShow("#" + getId() + " ." + pathsIDs.get("/")))
										.toStringOnclick()))
							.content("/");
				
				String partCrumb2 = "/";
				for (String tmpCrumb : StringUtilities.split(crumb, '/')) {
					partCrumb2 += tmpCrumb + "/";
					html
						.a(class_(getCssBreadcrumbsClasses())
								.onClick(new OpList()
											.add(getAdditionalOpList())
											.add(new OpHide("#" + getId() + " .mlb"))
											.add(new OpShow("#" + getId() + " ." + pathsIDs.get(partCrumb2)))
											.toStringOnclick()))
							.content(tmpCrumb)
						.span()
							.content("/");
				}
				html
					._div(); // end breadcrumb
				
				pathsDone.add(crumb);
			}
		}
		
		// creating menu for all paths
		pathsDone.clear();
		// first only folders
		for (int j = 0; j < getPathSorted().size(); j++) {
			pathStr = getPathSorted().get(j);
			String[] pathStrParts = StringUtilities.split(pathStr, '/');
			
			String currentPath = "/";
			
			if (pathsIDs.containsKey(pathStr) == false)
				pathsIDs.put(pathStr, UUID.random().getId());
			
			// create menu buttons for this path
			currentPath = "/";
			for (String part : pathStrParts) {
				for (int k = 0; k < getPathSorted().size(); k++) {
					
					if (getPathSorted().get(k).startsWith(currentPath)) {
						String menu = getPathSorted().get(k).substring(currentPath.length());
						menu = menu.indexOf('/') > 0 ? menu.substring(0, menu.indexOf('/')) : menu;
						
						if (pathsDone.contains(currentPath + menu + "/"))
							continue;
						
						if (pathsIDs.containsKey(currentPath) == false)
							pathsIDs.put(currentPath, UUID.random().getId());
						if (pathsIDs.containsKey(currentPath + menu + "/") == false)
							pathsIDs.put(currentPath + menu + "/", UUID.random().getId());
						
						if (getPathToOpList().get(currentPath + menu) != null) {
							// leaf
//							html
//								.a(class_(getCssLeafsClasses() + " mlb leaf " + pathsIDs.get(currentPath) + 
//										(getPathToCssClass().get(currentPath + menu) != null ? 
//												" " + getPathToCssClass().get(currentPath + menu) : ""))
//									.add("style", !isCollapsed() && currentPath.equals("/") ? "" : "display: none;" )
//									.onClick(getPathToOpList()
//												.get(currentPath + menu)
//												.add(getAdditionalOpList())
//												.toStringOnclick()))
//									.content(menu)
//							;
						} else {
							// folder
							html
								.a(class_(getCssFoldersClasses() + " mlb " + pathsIDs.get(currentPath))
										.add("style", !isCollapsed() && currentPath.equals("/") ? "" : "display: none;")
										.onClick(new OpList()
													.add(getAdditionalOpList())
													.add(pathToAdditionalOpList.get(currentPath + menu + "/"))
													.add(new OpHide("#" + getId() + " .mlb"))
													.add(new OpShow("#" + getId() + " ." + pathsIDs.get(currentPath + menu + "/")))
													.toStringOnclick()))
									.b()
										.content(menu)
									.i(class_("glyphicon glyphicon-arrow-right right"))
									._i()
								._a();
						}
						
						pathsDone.add(currentPath + menu + "/");
					}
				}
				
				pathsDone.add(currentPath);
				currentPath += part + "/";
			}
		}
		// and now leafs
		pathsDone.clear();
		for (int j = 0; j < getPathSorted().size(); j++) {
			pathStr = getPathSorted().get(j);
			String[] pathStrParts = StringUtilities.split(pathStr, '/');
			
			String currentPath = "/";
			
			if (pathsIDs.containsKey(pathStr) == false)
				pathsIDs.put(pathStr, UUID.random().getId());
			
			// create menu buttons for this path
			currentPath = "/";
			for (String part : pathStrParts) {
				for (int k = 0; k < getPathSorted().size(); k++) {
					
					if (getPathSorted().get(k).startsWith(currentPath)) {
						String menu = getPathSorted().get(k).substring(currentPath.length());
						menu = menu.indexOf('/') > 0 ? menu.substring(0, menu.indexOf('/')) : menu;
						
						if (pathsDone.contains(currentPath + menu + "/"))
							continue;
						
						if (pathsIDs.containsKey(currentPath) == false)
							pathsIDs.put(currentPath, UUID.random().getId());
						if (pathsIDs.containsKey(currentPath + menu + "/") == false)
							pathsIDs.put(currentPath + menu + "/", UUID.random().getId());
						
						if (getPathToOpList().get(currentPath + menu) != null) {
							// leaf
							html
								.a(class_(getCssLeafsClasses() + " mlb leaf " + pathsIDs.get(currentPath) + 
										(getPathToCssClass().get(currentPath + menu) != null ? 
												" " + getPathToCssClass().get(currentPath + menu) : ""))
									.add("style", !isCollapsed() && currentPath.equals("/") ? "" : "display: none;" )
									.onClick(getPathToOpList()
												.get(currentPath + menu)
												.add(getAdditionalOpList())
												.toStringOnclick()))
									.content(menu)
							;
						} else {
							// folder
//							html
//								.a(class_(getCssFoldersClasses() + " mlb " + pathsIDs.get(currentPath))
//										.add("style", !isCollapsed() && currentPath.equals("/") ? "" : "display: none;")
//										.onClick(new OpList()
//													.add(getAdditionalOpList())
//													.add(pathToAdditionalOpList.get(currentPath + menu + "/"))
//													.add(new OpHide("#" + getId() + " .mlb"))
//													.add(new OpShow("#" + getId() + " ." + pathsIDs.get(currentPath + menu + "/")))
//													.toStringOnclick()))
//									.b()
//										.content(menu)
//									.i(class_("glyphicon glyphicon-arrow-right right"))
//									._i()
//								._a();
						}
						
						pathsDone.add(currentPath + menu + "/");
					}
				}
				
				pathsDone.add(currentPath);
				currentPath += part + "/";
			}
		}
		
		html
			._div();
	}
	
	private Iterable<String> iterateChildren(String path) {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {
					private Iterator<String> allKeys = getPathToOpList().keySet().iterator();
					private String key = null;
					private Set<String> keyDone = new HashSet<String>();
					
					@Override
					public String next() {
						return key;
					}
					
					@Override
					public boolean hasNext() {
						key = null;
						
						while (allKeys.hasNext()) {
							key = allKeys.next();
							
							FileExt thiz = new FileExt(key);
							String subPath = thiz.getRelativePathTo(path);
							
							if (StringUtilities.notEmpty(subPath)) {
								int idx = subPath.indexOf("/");
								key = idx > 0 ? subPath.substring(0, idx) : subPath;
							}
														
							if (keyDone.contains(key))
								continue;
							
							keyDone.add(key);
							return true;
						}
						
						return false;
					}
				};
			}
		};
	}

	private Map<String, OpList> getPathToOpList() {
		return pathToOpList;
	}

	private void setPathToOpList(Map<String, OpList> paths) {
		this.pathToOpList = paths;
	}

	private String getCurrentPath() {
		return currentPath;
	}

	private void setCurrentPath(String currentPath) {
		this.currentPath = currentPath;
	}

	public boolean isCollapsed() {
		return collapsed;
	}

	public MultiLevelButtonGroup setCollapsed(boolean collapsed) {
		this.collapsed = collapsed;
		return this;
	}

	private Map<String, String> getPathToCssClass() {
		if (pathToCssClass == null)
			pathToCssClass = new HashMap<String, String>();
		return pathToCssClass;
	}

	private void setPathToCssClass(Map<String, String> pathToCssClass) {
		this.pathToCssClass = pathToCssClass;
	}

	private List<String> getPathSorted() {
		return pathSorted;
	}

	private void setPathSorted(List<String> pathSorted) {
		this.pathSorted = pathSorted;
	}

	public boolean isMovePageOnUncollapsed() {
		return movePageOnUncollapsed;
	}

	public MultiLevelButtonGroup setMovePageOnUncollapsed(boolean movePageOnUncollapsed) {
		this.movePageOnUncollapsed = movePageOnUncollapsed;
		return this;
	}

	public String getStartingText() {
		if (startingText == null)
			try {
				startingText = new HtmlCanvas()
								.i(class_("glyphicon glyphicon-list"))
								._i()
								.toHtml();
			} catch (IOException e) {
				LibsLogger.error(MultiLevelButtonGroup.class, "Cannot create starting text", e);
				startingText = "...";
			}
		return startingText;
	}

	public MultiLevelButtonGroup setStartingText(String startingText) {
		this.startingText = startingText;
		return this;
	}

	public String getCssDivClasses() {
		return cssDivClasses;
	}

	public MultiLevelButtonGroup setCssDivClasses(String cssDivClasses) {
		this.cssDivClasses = cssDivClasses;
		return this;
	}

	public String getCssFoldersClasses() {
		return cssFoldersClasses;
	}

	public MultiLevelButtonGroup setCssFoldersClasses(String cssFoldersClasses) {
		this.cssFoldersClasses = cssFoldersClasses;
		return this;
	}

	public String getCssLeafsClasses() {
		return cssLeafsClasses;
	}

	public MultiLevelButtonGroup setCssLeafsClasses(String cssLeafsClasses) {
		this.cssLeafsClasses = cssLeafsClasses;
		return this;
	}

	public String getCssBreadcrumbsClasses() {
		return cssBreadcrumbsClasses;
	}

	public MultiLevelButtonGroup setCssBreadcrumbsClasses(String cssBreadcrumbsClasses) {
		this.cssBreadcrumbsClasses = cssBreadcrumbsClasses;
		return this;
	}

	public OpList getAdditionalOpList() {
		return additionalOpList;
	}

	public void setAdditionalOpList(OpList additionalOpList) {
		this.additionalOpList = additionalOpList;
	}

}
