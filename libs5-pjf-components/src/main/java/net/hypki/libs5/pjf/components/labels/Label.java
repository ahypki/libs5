package net.hypki.libs5.pjf.components.labels;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.hypki.libs5.pjf.components.Component;

public class Label extends Component {
	
	private String additionalCssClass = null;
	
	private MessageType messageType = MessageType.DEFAULT;
	
	private String name = null;

	public Label(final MessageType messageType, final String name) {
		setMessageType(messageType);
		setName(name);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.span(HtmlAttributesFactory.class_("label label-" + getMessageType().toString().toLowerCase() + " " + (getAdditionalCssClass() != null ? getAdditionalCssClass() : "")))
				.content(getName());
	}
	
	@Override
	public String toString() {
		return String.format("<span class=\"label label-%s %s\">%s</span>", 
				getMessageType().toString().toLowerCase(),
				getAdditionalCssClass() != null ? getAdditionalCssClass() : "",
				getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getAdditionalCssClass() {
		return additionalCssClass;
	}

	public Label setAdditionalCssClass(String additionalCssClass) {
		this.additionalCssClass = additionalCssClass;
		return this;
	}
}
