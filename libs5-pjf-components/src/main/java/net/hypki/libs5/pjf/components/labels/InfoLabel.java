package net.hypki.libs5.pjf.components.labels;

public class InfoLabel extends Label {

	public InfoLabel(String msg) {
		super(MessageType.INFO, msg);
	}
}
