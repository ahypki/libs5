package net.hypki.libs5.pjf.components.labels;

public class WarningLabel extends Label {

	public WarningLabel(String msg) {
		super(MessageType.WARNING, msg);
	}
}
