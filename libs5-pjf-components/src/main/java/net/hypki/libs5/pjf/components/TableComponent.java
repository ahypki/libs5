package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.onClick;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public abstract class TableComponent extends Component {
	
	@Expose
	private int currentPage = 0;
	
	@Expose
	private int perPage = 20;
	
	@Expose
	private String filter = null;
	
	@Expose
	private boolean perPageVisible = true;
	
	@Expose 
	private ButtonSize buttonSize = ButtonSize.SMALL;
	
	@Expose
	private boolean initialRefresh = true;
	
	@Expose
	private boolean menuVisible = true;
	
	@Expose
	private String refreshButtonText = "Refresh";
	
	protected abstract long totalRows();
	protected abstract int getColumnCount();
	protected abstract void prepareRow(HtmlCanvas html, int row) throws IOException;
	protected abstract String getMessageIfNoRows();
	protected abstract boolean isFilterVisible();
	protected abstract List<Button> getMenu();

	public TableComponent() {
		
	}
	
	public TableComponent(Component parent) {
		super(parent);
	}
	
	public TableComponent(SecurityContext sc, Params params) {
		super(sc, params);
	}
	
	protected String getHeaderName(int column) {
		return "";
	}
	
	protected int getRefreshDelayMs() {
		return 0;
	}
	
	protected boolean isHeaderVisible() {
		return false;
	}
	
	public boolean isPerPageVisible() {
		return perPageVisible;
	}
	
	public TableComponent setPerPageVisible(boolean perPageVisible) {
		this.perPageVisible = perPageVisible;
		return this;
	}
	
	/**
	 * If true pager is like: <<, 1, 2, 3, 4, 5, >>
	 * If false pager is like: First, Next
	 * @return
	 */
	protected boolean isPagerWithNumbers() {
		return true;
	}
	
	public TableComponent setMenuVisible(boolean menuVisible) {
		this.menuVisible = menuVisible;
		return this;
	}
	
	public boolean isMenuVisible() {
		return this.menuVisible;
	}
	
	protected String getPanelHtmlClasses() {
		return "";
	}
	
	protected String getTableHtmlClasses() {
		return "";
	}
	
	protected void renderHeader(HtmlCanvas html) throws IOException {
		
	}
	
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	
	protected void renderBeforePager(HtmlCanvas html) throws IOException {
		
	}
	
	protected String getButtonGroupAdditionalClasses() {
		return "";
	}
	
	protected String getButtonAdditionalClasses() {
		return "";
	}
	
	public ButtonSize getButtonSize() {
		return buttonSize;
	}
	
	protected String getRefreshButtonText() {
		return refreshButtonText;
	}
	
	public TableComponent setRefreshButtonText(String refreshButtonText) {
		this.refreshButtonText = refreshButtonText;
		return this;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		setFilter(getInititalFilter());
		
		html.div(id(getId())
				.class_("table-component " + getPanelHtmlClasses()));
		
		renderHeader(html);
		
		ButtonGroup buttons = new ButtonGroup()
			.setButtonSize(getButtonSize())
			.setAdditionalCssClasses(getButtonGroupAdditionalClasses())
			.addButton(new Button()
					.setName(getRefreshButtonText())
					.setHtmlClass(getButtonAdditionalClasses())
					.setAwesomeicon("sync")
					.setVisible(isFilterVisible())
					.add(new OpComponentClick(this, "onRefresh"))
					);
		
		if (isMenuVisible())
			buttons
				.addButton(getMenu());
		
		html
				.if_(isFilterVisible())
					.input(type("text")
							.name("table-filter")
							.class_("table-filter")
							.add("placeholder", getFilterPlaceholder())
							.value(getInititalFilter())
							.onKeyup(new OpComponentTimer(this, "onRefresh", 300).toStringOnclick())
							.onChange(new OpComponentTimer(this, "onRefresh", 300).toStringOnclick())
							)
				._if()
				
				.render(buttons);
		
		
		html
				.div(class_("table-data"));
		
		if (getRefreshDelayMs() <= 0)
				renderTable(html);
//		else
		
		if (isInitialRefresh())
			html
				.write(new OpComponentTimer(this, "onRefresh", getRefreshDelayMs()).toStringAdhock(), false);
		
		html
				._div()
			
				.if_(isPerPageVisible())
					.div(class_("table-perpage"))
						.write(renderPerPage(), false)
					._div()
				._if()
			._div();
		
		renderFooter(html);
	}
	
	private String renderPerPage() throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		
		html
			.span()
				.content("Per page: ");
		
		for (Integer perPageTmp : new int[] {5, 10, 20, 50}) {
			if (perPageTmp.equals(getPerPage()))
				html
					.span()
						.content(perPageTmp);
			else
				html
					.a(onClick(new OpComponentClick(this, "onPerPageChange", new Params().add("perPage", perPageTmp)).toStringOnclick()))
						.content(perPageTmp);
		}
		
		return html.toHtml();
	}
	
	private OpList onPerPageChange() throws IOException {
		setPerPage(getParams().getInteger("perPage", 5));
		
		return onRefresh();
	}
	
	protected String getInititalFilter() {
		return "";
	}
	
	protected String getFilterPlaceholder() {
		return "";
	}
	
	protected OpList onRefresh() throws IOException {
		setCurrentPage(getParams().getInteger("page", 0));
		setFilter(getParams().getString("table-filter"));
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .table-data", renderTable(new HtmlCanvas()).toHtml()))
			.add(new OpSet("#" + getId() + " .table-perpage", renderPerPage()));
	}
	
	private HtmlCanvas renderTable(HtmlCanvas html) throws IOException {
//		HtmlCanvas html = new HtmlCanvas();
		
		if (totalRows() == 0) {
			if (getMessageIfNoRows() != null)
				html
					.i(class_("no-data-yet"))
						.content(getMessageIfNoRows());
		} else {
			html.table(class_(getTableHtmlClasses()));
			
			if (isHeaderVisible()) {
				html.thead();
				for (int i = 0; i < getColumnCount(); i++) {
						html
							.th()
								.content(getHeaderName(i), false);
				}
				html._thead();
			}
			
			for (int i = (getCurrentPage() * getPerPage()), k = 0; k < (Math.min(totalRows(), getPerPage())) && i < totalRows(); i++, k++)
				prepareRow(html, i);
			
			html._table();
			
			renderBeforePager(html);
			
			html
				.render_if(new PagerRenderable(this, 
						"onRefresh", 
						getCurrentPage(), 
						getPerPage(), 
						totalRows(),
						getButtonSize())
							.setFirstNextBtnsOnly(!isPagerWithNumbers())
							.setHtmlClass(getButtonAdditionalClasses()),
						totalRows() >= getPerPage() && getCurrentPage() * getPerPage() <= totalRows());
			
		}
		
		return html;
	}
	
	protected int getCurrentPage() {
		return currentPage;
	}
	
	private void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	protected int getPerPage() {
		return perPage;
	}
	
	protected int getPage() {
		return getParams().getInteger("page", 0);
	}
	
	public TableComponent setPerPage(int perPage) {
		this.perPage = perPage;
		return this;
	}
	
	protected String getFilter() {
		if (filter == null)
			filter = getParams().getString("table-filter", null);
		return filter;
	}
	
	protected void setFilter(String filter) {
		this.filter = filter;
	}
	
	public TableComponent setButtonSize(ButtonSize buttonSize) {
		this.buttonSize = buttonSize;
		return this;
	}
	
	public boolean isInitialRefresh() {
		return initialRefresh;
	}
	
	public TableComponent setInitialRefresh(boolean initialRefresh) {
		this.initialRefresh = initialRefresh;
		return this;
	}
}
