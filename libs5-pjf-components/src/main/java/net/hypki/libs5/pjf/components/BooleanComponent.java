package net.hypki.libs5.pjf.components;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class BooleanComponent extends Component {
	
	private String title = null;
	
	private boolean value = false;
	
	private OpList opOnYes = null;
	
	private OpList opOnNo = null;

	public BooleanComponent() {
		
	}
	
	public BooleanComponent(String title, boolean value, OpList opOnYes, OpList opOnNo) {
		setTitle(title);
		setValue(value);
		setOpOnYes(opOnYes);
		setOpOnNo(opOnNo);
	}
	
	public OpList change(Params params) {
		final boolean yes = params.getBoolean("yes");
		
		return new OpList()
			.add(yes ? new OpAddClass("#" + getId() + " .yes", "selected") : new OpAddClass("#" + getId() + " .no", "selected"))
			.add(yes ? new OpRemoveClass("#" + getId() + " .no", "selected") : new OpRemoveClass("#" + getId() + " .yes", "selected"));
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()).class_("boolean-comp"))
				.span(class_("title"))
					.content(getTitle() != null ? getTitle() : "")
				.a(class_("yes " + (isValue() ? "selected" : ""))
						.onClick(new OpList()
							.add(new OpComponentClick(this, "change", new Params().add("yes", "on")))
							.add(getOpOnYes())
							.toStringOnclick()))
					.content("Yes")
				.a(class_("no " + (!isValue() ? "selected" : ""))
						.onClick(new OpList()
							.add(new OpComponentClick(this, "change", new Params().add("yes", "off")))
							.add(getOpOnNo())
							.toStringOnclick()))
					.content("No")
			._div();
	};

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public OpList getOpOnYes() {
		return opOnYes;
	}

	public void setOpOnYes(OpList opOnYes) {
		this.opOnYes = opOnYes;
	}

	public OpList getOpOnNo() {
		return opOnNo;
	}

	public void setOpOnNo(OpList opOnNo) {
		this.opOnNo = opOnNo;
	}
}
