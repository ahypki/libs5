package net.hypki.libs5.pjf.components.labels;

public class DangerLabel extends Label {

	public DangerLabel(String msg) {
		super(MessageType.DANGER, msg);
	}
}
