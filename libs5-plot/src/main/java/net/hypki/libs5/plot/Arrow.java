package net.hypki.libs5.plot;

import com.google.gson.annotations.Expose;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Arrow {

	@Expose
	@NotNull
	@NotEmpty
	private double x1 = 0.0;
	
	@Expose
	@NotNull
	@NotEmpty
	private double y1 = 0.0;
	
	@Expose
	@NotNull
	@NotEmpty
	private double x2 = 0.0;
	
	@Expose
	@NotNull
	@NotEmpty
	private double y2 = 0.0;
	
	@Expose
	private String label = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private int labelRotation = 0;
	
	@Expose
	@NotNull
	@NotEmpty
	private int fontsize = 12;
	
	public Arrow() {
		
	}
	
	public Arrow(double x1, double y1, double x2, double y2, String label, int labelRotation) {
		setX1(x1);
		setY1(y1);
		setX2(x2);
		setY2(y2);
		setLabel(label);
		setLabelRotation(labelRotation);
	}
	
	public Arrow(double x1, double y1, double x2, double y2, String label) {
		setX1(x1);
		setY1(y1);
		setX2(x2);
		setY2(y2);
		setLabel(label);
	}

	public double getX1() {
		return x1;
	}

	public void setX1(double x1) {
		this.x1 = x1;
	}

	public double getY1() {
		return y1;
	}

	public void setY1(double y1) {
		this.y1 = y1;
	}

	public double getX2() {
		return x2;
	}

	public void setX2(double x2) {
		this.x2 = x2;
	}

	public double getY2() {
		return y2;
	}

	public void setY2(double y2) {
		this.y2 = y2;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getLabelRotation() {
		return labelRotation;
	}

	public void setLabelRotation(int labelRotation) {
		this.labelRotation = labelRotation;
	}

	public int getFontsize() {
		return fontsize;
	}

	public Arrow setFontsize(int fontsize) {
		this.fontsize = fontsize;
		return this;
	}
}
