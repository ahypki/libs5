package net.hypki.libs5.plot;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.math.DataPoints;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Plot {
	
	/**
	 * Data source for this particular Plot, it can be e.g. path to a file, 
	 * datasets and tables queries for BEANS etc. 
	 */
	@Expose
	@NotNull
	@NotEmpty
	private String dataSource = null;
	
	/**
	 * Column name based on which data source is sorted before it is used by this Plot
	 */
	@Expose
	private String sortBy = null;
	
	/**
	 * Column name based on which data source will be split
	 */
	@Expose
	private String splitBy = null;
	
	/**
	 * Column name based on which data source will be colored by (e.g. different lines)
	 */
	@Expose
	private String colorBy = null;
	
	/**
	 * Legend for this one Plot
	 */
	@Expose
	private String legend = null;
	
	/**
	 * Color for this one Plot
	 */
	@Expose
	private String color = null;
	
	/**
	 * If true then the legend will be plotted
	 */
	@Expose
	private boolean legendOn = true;
	
	/**
	 * Plot type: POINTS, LINES, STEPS, or BOXES
	 */
	@Expose
	@NotNull
	@NotEmpty
	private PlotType plotType = null;
	
//	private InputStream inputStream = null;
	
//	private File inputFile = null;
	
	/**
	 * X column (it can be field name or a mathematical expression)
	 */
	@Expose
	private String x = null;
	
	/**
	 * Y column (it can be field name or a mathematical expression)
	 */
	@Expose
	@NotNull
	@NotEmpty
	private String y = null;
	
	/**
	 * Label (e.g. field name) which denotes any given point for this Plot 
	 */
	@Expose
	private String labelColumn = null;
	
	/**
	 * Error (e.g. field name) which corresponds to any given point for this Plot
	 */
	@Expose
	private String errorColumn = null;
	
//	private List<Filter> filters = null;
	
	/**
	 * Point types for this Plot: PLUS, MINUS, STAR, SQUARE, DOT, TRIANGLE, CIRCLE
	 */
	@Expose
	private PointType pointType = null;
	
	/**
	 * Point size for the points in this plot. Default size is denoted with 0.0, 
	 * any value larger than 1.0 increases the size, and the value below 1.0 decreases
	 * the point size. Applicable for the plot of a type POINTS. 
	 */
	@Expose
	private double pointSize = 0.0;
	
	/**
	 * Line type for this plot (if the type of the plot is LINES).
	 */
	@Expose
	private LineType lineType = null;
	
	/**
	 * Line width for this plot. Default size is denoted with 0.0.
	 */
	@Expose
	private double lineWidth = 0.0;
	
	/**
	 * Box width if such type of the plot was selected (i.e. BOXES, or STEPS)
	 */
	@Expose
	private double boxWidth = 0.0;
	
	/**
	 * Sample determines a fraction of the points to be taken into account. Useful, 
	 * if the number of points can be huge, then by setting `sample' to low value, 
	 * like 0.01 will plot only 1% of all points. 
	 */
	@Expose
	@NotNull
	@NotEmpty
	private double sample = 1.0;
	
	public Plot() {
		setPlotType(PlotType.POINTS);
	}
	
	public String getLegend() {
		return legend;
	}
	
	public String getLegend(String defaultLegend) {
		return legend != null ? legend : defaultLegend;
	}

	public Plot setLegend(String legend) {
		this.legend = legend;
		return this;
	}
	
	public boolean isLegendSet() {
		return notEmpty(getLegend()) && isLegendOn();
	}

	public PlotType getPlotType() {
		return plotType;
	}

	public Plot setPlotType(PlotType plotType) {
		this.plotType = plotType;
		return this;
	}
	
	public Plot setPlotType(String plotType) {
		this.plotType = PlotType.parse(plotType, PlotType.POINTS);
		return this;
	}

//	public InputStream getInputStream() {
//		return inputStream;
//	}
//
//	public Plot setInputStream(InputStream inputStream) {
//		this.inputStream = inputStream;
//		return this;
//	}
	
	public Plot setInputData(List<Object> x, List<Object> y) throws IOException {
		File tmpFile = FileUtils.createTmpFile();
		
		FileWriter fw = new FileWriter(tmpFile);
		for (int i = 0; i < x.size(); i++) {
			fw.append(x.get(i).toString());
			fw.append(" ");
			fw.append(y.get(i).toString());
			fw.append("\n");
		}
		fw.close();
		
		setDataSource(tmpFile.getAbsolutePath());
		
		return this;
	}
	
	public Plot setInputData(DataPoints dataPoints) throws IOException {
		File tmpFile = FileUtils.createTmpFile();
		
		FileWriter fw = new FileWriter(tmpFile);
		for (DataPoints.DataPoint dp : dataPoints) {
			fw.append(String.valueOf(dp.getX()));
			fw.append(" ");
			fw.append(String.valueOf(dp.getY()));
			fw.append("\n");
		}
		fw.close();
		
		setDataSource(tmpFile.getAbsolutePath());
		
		return this;
	}
	
//	public Plot setInputFile(File inputFile) {
//		this.inputFile = inputFile;
//		return this;
//	}
//	
//	public File getInputFile() {
//		return this.inputFile;
//	}

	public String getX() {
		return x;
	}

	public Plot setX(String x) {
		this.x = x;
		return this;
	}
	
	public String getY() {
		return y;
	}

	public Plot setY(String y) {
		this.y = y;
		return this;
	}
	
//	public boolean areFiltersDefined() {
//		return filters != null && filters.size() > 0;
//	}
//
//	public List<Filter> getFilters() {
//		if (filters == null)
//			filters = new ArrayList<>();
//		return filters;
//	}
//
//	public Plot setFilters(List<Filter> filters) {
//		this.filters = filters;
//		return this;
//	}
	
//	public Plot addFilter(Filter filter) {
//		getFilters().add(filter);
//		return this;
//	}

	public PointType getPointType() {
		return pointType;
	}

	public Plot setPointType(PointType pointType) {
		this.pointType = pointType;
		return this;
	}

	public String getLabelColumn() {
		return labelColumn;
	}
	
	public boolean isLabelColumnDefined() {
		return notEmpty(getErrorColumn());
	}

	public Plot setLabelColumn(String labelColumn) {
		this.labelColumn = labelColumn;
		return this;
	}

	public String getErrorColumn() {
		return errorColumn;
	}
	
	public boolean isErrorColumnDefined() {
		return notEmpty(getErrorColumn());
	}

	public Plot setErrorColumn(String errorColumn) {
		this.errorColumn = errorColumn;
		return this;
	}

	public boolean isLegendOn() {
		return legendOn;
	}

	public Plot setLegendOn(boolean legendOn) {
		this.legendOn = legendOn;
		return this;
	}

	public double getLineWidth() {
		return lineWidth;
	}

	public Plot setLineWidth(double lineWidth) {
		this.lineWidth = lineWidth;
		return this;
	}

	public String getColor() {
		return color;
	}

	public Plot setColor(String color) {
		this.color = color;
		return this;
	}

	public boolean isColorSet() {
		return StringUtilities.notEmpty(getColor());
	}

	public double getBoxWidth() {
		return boxWidth;
	}

	public Plot setBoxWidth(double boxWidth) {
		this.boxWidth = boxWidth;
		return this;
	}

	public String getDataSource() {
		return dataSource;
	}

	public Plot setDataSource(String dataSource) {
		this.dataSource = dataSource;
		return this;
	}

	public LineType getLineType() {
		return lineType;
	}

	public void setLineType(LineType lineType) {
		this.lineType = lineType;
	}

	public double getPointSize() {
		return pointSize;
	}

	public Plot setPointSize(double pointSize) {
		this.pointSize = pointSize;
		return this;
	}

	public double getSample() {
		return sample;
	}

	public void setSample(double sample) {
		this.sample = sample;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSplitBy() {
		return splitBy;
	}

	public void setSplitBy(String splitBy) {
		this.splitBy = splitBy;
	}

	public String getColorBy() {
		return colorBy;
	}

	public void setColorBy(String colorBy) {
		this.colorBy = colorBy;
	}
}
