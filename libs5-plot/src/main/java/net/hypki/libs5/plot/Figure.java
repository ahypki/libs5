package net.hypki.libs5.plot;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Figure {
	
	/**
	 * Title for the entire Figure
	 */
	@Expose
	private String title = null;
	
	/**
	 * X label (on the bottom) for the entire Figure
	 */
	@Expose
	private String xLabel = null;
	
	/**
	 * Y label (on the left) for the entire Figure
	 */
	@Expose
	private String yLabel = null;
	
	/**
	 * X range for the entire Figure
	 */
	@Expose
	private Range xRange = null;
	
	/**
	 * Y range for the entire Figure
	 */
	@Expose
	private Range yRange = null;
	
	/**
	 * List of separate plots which this Figure consists of
	 */
	@Expose
	@NotNull
	@NotEmpty
	private List<Plot> plots = null;
	
	/**
	 * List of arrows which should be plotted on this Figure 
	 */
	@Expose
	private List<Arrow> arrows = null;
	
	/**
	 * Font size for all texts in the figure
	 */
	@Expose
	private int fontsize = 0;
	
	/**
	 * Font size only for the legends of the plots
	 */
	@Expose
	private int fontsizeLegend = 0;
	
	/**
	 * Position on this Figure of the legends of the Plots
	 */
	@Expose
	private LegendPosition legendPosition = null;
	
	/**
	 * Width in px of the entire Figure
	 */
	@Expose
	private double width = 0.0;
	
	/**
	 * Height in px of the entire Figure
	 */
	private double height = 0.0;
	
	@Expose
	@AssertValid
	private MetaList meta = null;
	
	/**
	 * Log scale on X axis
	 */
	@Expose
	private boolean xLogscale = false;
	
	/**
	 * Log scale on Y axis
	 */
	@Expose
	private boolean yLogscale = false;
	
	public Figure() {
		
	}
	
	public Figure plot() throws IOException {
		throw new IOException("not implemented");
	}

	public String getTitle() {
		return title;
	}

	public Figure setTitle(String title) {
		this.title = title;
		return this;
	}

	public List<Plot> getPlots() {
		if (plots == null)
			plots = new ArrayList<>();
		return plots;
	}

	private void setPlots(List<Plot> plots) {
		this.plots = plots;
	}

	public Figure addPlot(Plot plot) {
		getPlots().add(plot);
		return this;
	}

	public Range getXRange() {
		return xRange;
	}

	public Figure setXRange(Range xRange) {
		this.xRange = xRange;
		return this;
	}

	public Range getYRange() {
		return yRange;
	}

	public Figure setYRange(Range yRange) {
		this.yRange = yRange;
		return this;
	}
	
	public Figure addArrow(Arrow a) {
		getArrows().add(a);
		return this;
	}

	public List<Arrow> getArrows() {
		if (arrows == null)
			arrows = new ArrayList<>();
		return arrows;
	}

	private void setArrows(List<Arrow> arrows) {
		this.arrows = arrows;
	}

	public String getXLabel() {
		return xLabel;
	}
	
	public String getXLabel(String defaultLabel) {
		return xLabel != null ? xLabel : defaultLabel;
	}

	public Figure setXLabel(String xLabel) {
		this.xLabel = xLabel;
		return this;
	}

	public String getYLabel() {
		return yLabel;
	}
	
	public String getYLabel(String defaultLabel) {
		return yLabel != null ? yLabel : defaultLabel;
	}

	public Figure setYLabel(String yLabel) {
		this.yLabel = yLabel;
		return this;
	}

	public int getFontsize() {
		return fontsize;
	}

	public Figure setFontsize(int fontsize) {
		this.fontsize = fontsize;
		return this;
	}
	
	public boolean isFontsizeSet() {
		return getFontsize() != 0;
	}
	
	public boolean isFontsizeLegendSet() {
		return getFontsizeLegend() != 0;
	}

	public int getFontsizeLegend() {
		return fontsizeLegend;
	}

	public Figure setFontsizeLegend(int fontsizeKey) {
		this.fontsizeLegend = fontsizeKey;
		return this;
	}

	public LegendPosition getLegendPosition() {
		return legendPosition;
	}

	public Figure setLegendPosition(LegendPosition legendPosition) {
		this.legendPosition = legendPosition;
		return this;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getFigureSizeRatio() {
		if (getWidth() > 0.0 && getHeight() > 0.0)
			return getWidth() / getHeight();
		return 0.0;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}

	public boolean isxLogscale() {
		return xLogscale;
	}

	public void setxLogscale(boolean xLogscale) {
		this.xLogscale = xLogscale;
	}

	public boolean isyLogscale() {
		return yLogscale;
	}

	public void setyLogscale(boolean yLogscale) {
		this.yLogscale = yLogscale;
	}

	public void clearSortBy() {
		for (Plot p : getPlots())
			p.setSortBy(null);
	}

	public void clearLabels() {
		setXLabel(null);
		setYLabel(null);
//		for (Plot p : getPlots())
//			p.set LaSortBy(null);
	}

	public String getTitle(String defaultTitle) {
		return notEmpty(getTitle()) ? getTitle() : defaultTitle;
	}
}
