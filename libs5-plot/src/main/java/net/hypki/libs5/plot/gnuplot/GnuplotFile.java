package net.hypki.libs5.plot.gnuplot;

import java.io.File;
import java.io.IOException;

import net.hypki.libs5.plot.Figure;

public class GnuplotFile extends Gnuplot {
	
	private File outputFile = null;
	

	public GnuplotFile(File fileOutput) {
		setOutputFile(fileOutput);
	}
	
	@Override
	public Figure plot() throws IOException {
		return gnuplot(getOutputFile());
	}

	public File getOutputFile() {
		return outputFile;
	}

	public GnuplotFile setOutputFile(File outputFile) {
		this.outputFile = outputFile;
		return this;
	}
}
