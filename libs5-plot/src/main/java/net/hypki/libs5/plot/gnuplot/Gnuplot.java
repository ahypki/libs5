package net.hypki.libs5.plot.gnuplot;

import static java.lang.String.format;
import static java.util.Locale.ENGLISH;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import net.hypki.libs5.additions.PdfUtils;
import net.hypki.libs5.plot.Arrow;
import net.hypki.libs5.plot.Figure;
import net.hypki.libs5.plot.Filter;
import net.hypki.libs5.plot.LegendPosition;
import net.hypki.libs5.plot.Plot;
import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.system.Basher;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.apache.commons.lang.NotImplementedException;

public abstract class Gnuplot extends Figure {

	private int imageWidth = 640;
	
	private int imageHeight = 480;
	
	private boolean smoothUnique = false;

	public Gnuplot() {
		
	}
	
	protected Figure gnuplot(File fileOutput) throws IOException {
		ArrayList<File> toRemove = new ArrayList<>();
		
		try {
			Watch start = new Watch();
			
			File fScript = FileUtils.createTmpFile();
			toRemove.add(fScript);
			
			FileWriter fw = new FileWriter(fScript);
			double pointScale = 1.3;
			
			// title
			if (notEmpty(getTitle()))
				fw.write(format(ENGLISH, "set title \"%s\" %s;\n", getTitle(), isFontsizeSet() ? "font \"," + (int) getFontsize() + "\"" : ""));
			
			// legend outside
//			fw.write("set key outside vert; set key left top; \n");
			
			// term
			if (fileOutput != null) {
				if (fileOutput.getAbsolutePath().endsWith(".pdf")) {
					fw.write("set term pdf enhanced color;\n");
					pointScale = 0.7;
				} else if (fileOutput.getAbsolutePath().endsWith(".ps"))
					fw.write("set term postscript enhanced color;\n");
				else if (fileOutput.getAbsolutePath().endsWith(".jpg"))
					fw.write("set term jpg color enhanced;\n");
				else if (fileOutput.getAbsolutePath().endsWith(".png"))
					fw.write("set term png size " + getImageWidth() + "," + getImageHeight() + " enhanced;\n");
				else
					throw new NotImplementedException("Don't know how to plot to " + fileOutput);
				fw.write(format(ENGLISH, "set output \"%s\";\n", fileOutput.getAbsolutePath()));
			} else {
				fw.write("set term wxt;\n");
			}
			
			// x range
			if (getXRange() != null)
				fw.write(format("set xrange[%f:%f];", getXRange().getFrom(), getXRange().getTo()));
			if (getYRange() != null)
				fw.write(format("set yrange[%f:%f];", getYRange().getFrom(), getYRange().getTo()));
			
			// box width
			if (getPlots().get(0).getBoxWidth() > 0.0)
				fw.write(format("set boxwidth %f;", getPlots().get(0).getBoxWidth()));
			
			// plot ratio
			if (getFigureSizeRatio() != 0.0)
				fw.write(format("set clip two; set size ratio %f;", getFigureSizeRatio()));
			
			// arrows
			for (Arrow arrow : getArrows()) {
				fw.write(format("set style arrow 9 head filled size screen 0.03,15,45 ls 1 lw 1;\n"
						+ "set arrow from %f,%s to %f,%f as 9;\n"
						+ "set label '%s' at %f,%f rotate by %d font ',%d';\n", 
						arrow.getX1(), arrow.getY1(), arrow.getX2(), arrow.getY2(), 
						arrow.getLabel(), arrow.getX2(), arrow.getY2(), arrow.getLabelRotation(), arrow.getFontsize()));
			}
			
			// labels
			if (notEmpty(getXLabel()))
				fw.write(format("set xlabel \"%s\" %s;\n", getXLabel(), isFontsizeSet() ? "font \"," + (int) getFontsize() + "\"" : "")); 
			if (notEmpty(getYLabel()))
				fw.write(format("set ylabel \"%s\" %s;\n", getYLabel(), isFontsizeSet() ? "font \"," + (int) getFontsize() + "\"" : ""));
			
			// key size
			if (isFontsizeLegendSet())
				fw.write(format("set key font \",%d\"; set key spacing 0.8; \n", getFontsizeLegend()));
			
			// legend's position
			if (getLegendPosition() == LegendPosition.BOTTOM_RIGHT)
				fw.write("set key right bottom;\n");
			else if (getLegendPosition() == LegendPosition.TOP_LEFT)
				fw.write("set key left top;\n");
			else if (getLegendPosition() == LegendPosition.BOTTOM_LEFT)
				fw.write("set key left bottom;\n");
			
			// density map
//			if (getPlots() != null
//					&& getPlots().size() > 0
//					&& getPlots().get(0).getPlotType() == PlotType.MAP) {
//				fw.write("set palette rgbformulae 34,35,0;\n");
//				fw.write("set size square;\n");
//				fw.write("set pm3d map;\n");
//				fw.write("splot ");
//			} else {
				fw.write("plot ");
//			}
			
			for (int i = 0; i < getPlots().size(); i++) {
				Plot plot = getPlots().get(i);
				File plotData = null;
				
//				if (plot.getInputFile() != null)
					plotData = new File(plot.getDataSource());// plot.getInputFile();
//				else {
//					plotData = FileUtils.createTmpFile();
//					FileUtils.saveToFile(plot.getInputStream(), plotData);
//					toRemove.add(plotData);
//				}
				
				// file name to read the data from
				fw.write(format(" \"%s\" ", plotData.getAbsolutePath()));
				
				fw.write(" u ");
				
				// columns
//				if (plot.areFiltersDefined()) {
//					// additional filters are defined
//					Filter f = plot.getFilters().get(0);
//					fw.write(String.format("($%d %s %s ? $%s : NaN)", f.getColumnNumber() + 1, f.getComparator(), f.getValue(), String.valueOf(plot.getX() + 1)));
//				} else
					fw.write(String.format("(%s)", plot.getX() != null ? plot.getX() : "$1"));
				fw.write(":");
				fw.write(String.format("(%s)", plot.getY() != null ? plot.getY() : "$2"));
				
//				if (plot.getPlotType() == PlotType.MAP) {
//					fw.write(":");
//					fw.write(String.valueOf(plot.getLabelColumn()));
//				}
				
				// with labels? errorbars?
				if (plot.isLabelColumnDefined()) {// && plot.getPlotType() != PlotType.MAP) {
					fw.write(":");
					fw.write(String.valueOf(plot.getLabelColumn() + 1));
					fw.write(" with labels ");
				} else if (plot.isErrorColumnDefined()) {
					fw.write(":");
					fw.write(String.valueOf(plot.getErrorColumn() + 1));
					fw.write(" with errorbars ");
				} else if (plot.getPlotType() == PlotType.LINES)
					fw.write(" with lines ");
				else if (plot.getPlotType() == PlotType.POINTS)
					fw.write(" with points ");
				else if (plot.getPlotType() == PlotType.BOXES)
					fw.write(" with boxes lw 1.9 ");
				else if (plot.getPlotType() == PlotType.STEPS)
					fw.write(" with steps " + (plot.getLineWidth() > 0.0 ? " lw " + plot.getLineWidth() : ""));
//				else if (plot.getPlotType() == PlotType.MAP) {
					// do nothing
				else
					throw new NotImplementedException();
				
				if (plot.getPlotType() == PlotType.POINTS)
					fw.write(format(ENGLISH, " ps %.2f ", plot.getPointSize() * pointScale));
				
				// manual color
				if (plot.isColorSet()) {
//					if (plot.getColor().startsWith("#"))
						fw.write(format(ENGLISH, " lc rgb \"%s\" ", plot.getColor()));
//					else
//						fw.write(format(ENGLISH, " lc rgb \"%s\" ", plot.getColor()));
				}
				
				// point type
				if (plot.getPointType() != null && !plot.isLabelColumnDefined())
					switch (plot.getPointType()) {
						case CIRCLE:
							fw.write(" pt 7"); break;
						case DOT:
							fw.write(" pt 0"); break;
						case SQUARE:
							fw.write(" pt 5"); break;
						case STAR:
							fw.write(" pt 3"); break;
						case TRIANGLE:
							fw.write(" pt 9"); break;
					}
				
				fw.write(format(ENGLISH, " title \"%s\" ", plot.isLegendSet() ? plot.getLegend() : ""));
				
				if (isSmoothUnique())
					fw.write(" smooth unique ");
				
				if (i < (getPlots().size() - 1))
					fw.write(", ");
			}
			
			fw.write(";");
			fw.close();
			
			LibsLogger.debug(Gnuplot.class, "Script: \n" + FileUtils.readString(fScript));
			LibsLogger.debug(Gnuplot.class, "gnuplot -p ", fScript.getAbsolutePath());
			
			Basher.exec(new String[]{"gnuplot", "-p", fScript.getAbsolutePath()});
			
			AssertUtils.assertTrue(fileOutput == null || fileOutput.exists(), "File " + (fileOutput != null ? fileOutput.getAbsolutePath() : "") + " does not exist");
			
			LibsLogger.debug(Gnuplot.class, "Plot generated in " + start);
			
			if (fileOutput != null && fileOutput.getAbsolutePath().endsWith(".pdf"))
				PdfUtils.addMetadata(fileOutput, 
						"Plot made with libs5-plot", 
						"Script: \n" + FileUtils.readString(fScript), 
						"John Smith", 
						"http://url.net/", 
						"http://url.net/");

		} finally {
			for (File fileToRemove : toRemove) {
				if (fileToRemove.exists())
					fileToRemove.delete();
			}
		}
		
		return this;
	}

	public int getImageWidth() {
		return imageWidth;
	}

	public Gnuplot setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
		return this;
	}

	public int getImageHeight() {
		return imageHeight;
	}

	public Gnuplot setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
		return this;
	}

	public boolean isSmoothUnique() {
		return smoothUnique;
	}

	public Gnuplot setSmoothUnique(boolean smoothUnique) {
		this.smoothUnique = smoothUnique;
		return this;
	}
}
