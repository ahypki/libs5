package net.hypki.libs5.plot;

import static net.hypki.libs5.utils.string.RandomUtils.nextInt;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;

import net.hypki.libs5.utils.string.RandomUtils;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotEmpty;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class Color implements Serializable {
	
	public static final int MAX = 255;
	public static final Color RED = new Color(MAX, 0, 0);
	public static final Color GREEN = new Color(0, MAX, 0);
	public static final Color BLUE = new Color(0, 0, MAX);
	public static final Color GRAY = new Color(202, 202, 202);
	public static final Color LIGHTGRAY = new Color(144, 144, 144);
	
	@Expose
	@Min(value = 0)
	private int red = 255;
	
	@Expose
	@Min(value = 0)
	private int green = 0;
	
	@Expose
	@Min(value = 0)
	private int blue = 0;
	
	
	private static HashMap<String, Color> nameToColor = new HashMap<>();
	private static HashMap<Integer, String> colorToName = new HashMap<>();
	
	static {
		nameToColor.put("red", RED);
		nameToColor.put("green", GREEN);
		nameToColor.put("blue", BLUE);
		nameToColor.put("lightgray", LIGHTGRAY);
		nameToColor.put("gray", GRAY);
		
		for (Entry<String, Color> color : nameToColor.entrySet()) {
			colorToName.put(color.getValue().hashCode(), color.getKey());
		}
	}
	
	public Color() {
		
	}
		
	public Color(int red, int green, int blue) {
		setRed(red);
		setGreen(green);
		setBlue(blue);
	}
	
	@Override
	public String toString() {
		return String.format("%s[r:%d g:%d b:%d]", getName() != null ? getName() + " " : "", getRed(), getGreen(), getBlue());
	}
	
	@Override
	public int hashCode() {
		return getRed() * 1000000 + getGreen() * 1000 + getBlue();
	}
	
	public static Color getColorByName(String colorName) {
		return nameToColor.get(colorName);
	}
	
	public static Color getColorByName(String colorName, Color defaultColor) {
		Color c = nameToColor.get(colorName);
		return c != null ? c : defaultColor;
	}
	
	public static Color random() {
		return new Color(nextInt(256), nextInt(256), nextInt(256));
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public String getName() {
		return colorToName.get(this.hashCode());
	}
}
