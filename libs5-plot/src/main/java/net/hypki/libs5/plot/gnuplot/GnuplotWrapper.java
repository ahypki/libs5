package net.hypki.libs5.plot.gnuplot;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.system.Basher;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class GnuplotWrapper {
	
	@Expose
//	@NotNull
//	@NotEmpty
	private String gnuplotScript = null;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private String workingFolder = null;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private String gnuplotFilename = null;

	public GnuplotWrapper() {
		
	}
	
	public GnuplotWrapper(String gnuplotScript) {
		setGnuplotScript(gnuplotScript);
	}

	public String getGnuplotScript() {
		return gnuplotScript;
	}

	public GnuplotWrapper setGnuplotScript(String gnuplotScript) {
		this.gnuplotScript = gnuplotScript;
		return this;
	}
	
	public GnuplotWrapper run() throws IOException {
		try {
			final FileExt gplFile = getGnuplotFile();
			
			// create folder 
			getWorkingFolder().mkdirs();

			// save gnuplot file
			FileUtils.saveToFile(getGnuplotScript(), gplFile, true);
			
			// run gnuplot
			Basher.exec(new String[]{"gnuplot", "-p", gplFile.getAbsolutePath()},
					getWorkingFolder());
			
			return this;
		} catch (Throwable t) {
			throw new IOException("Gnuplot wrapper failed", t);
		}
	}
	
	public GnuplotWrapper clone() {
		return JsonUtils.fromJson(JsonUtils.objectToString(this), GnuplotWrapper.class);
	}
	
	public void delete() throws IOException {
		getWorkingFolder().remove(true);
	}
	
	public void clear() throws IOException {
		getWorkingFolder().cleanDir();
	}
	
	public Iterable<FileExt> iterateOutputFiles() throws IOException {
		final FileExt f = getWorkingFolder();
		
		return new Iterable<FileExt>() {
			@Override
			public Iterator<FileExt> iterator() {
				return new LazyFileIterator(f, true, true, false, new FileFilter() {
						@Override
						public boolean accept(File pathname) {
							String tmp = pathname.getName();
							return tmp.equals(getGnuplotFilename()) == false
									&& tmp.startsWith("input-data-") == false
									;
						}
					});
			}
		};
	}
	
	public FileExt getWorkingFolder() throws IOException {
		return new FileExt(getWorkingFolderPath());
	}

	public String getWorkingFolderPath() throws IOException {
		if (workingFolder == null) {
			workingFolder = FileUtils.createTmpRandomDir().getAbsolutePath();			
		}
		return workingFolder;
	}

	public GnuplotWrapper setWorkingFolderPath(String workingFolder) {
		this.workingFolder = workingFolder;
		return this;
	}
	
	public FileExt getGnuplotFile() throws IOException {
		return new FileExt(getWorkingFolderPath(), getGnuplotFilename());
	}

	public String getGnuplotFilename() {
		if (this.gnuplotFilename == null) {
			this.gnuplotFilename = "gnuplot-wrapper-" + UUID.randomUUID().toString().replaceAll("\\s", "") +  ".gpl";
		}
		return gnuplotFilename;
	}

	public GnuplotWrapper setGnuplotFilename(String gnuplotFilename) {
		this.gnuplotFilename = gnuplotFilename;
		return this;
	}
}
