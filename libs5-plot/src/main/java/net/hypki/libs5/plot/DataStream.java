package net.hypki.libs5.plot;

import java.io.InputStream;
import java.util.Iterator;

public class DataStream {
	
	private InputStream inputStream = null;

	public DataStream(InputStream is) {
		setInputStream(is);
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream is) {
		this.inputStream = is;
	}
}
