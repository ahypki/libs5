package net.hypki.libs5.plot;

public class Filter {

	private int columnNumber = 0;
	
	private String comparator = null;
	
	private String value = null;
	
	public Filter() {
		
	}
	
	public Filter(int columnNumber, String comparator, String value) {
		setColumnNumber(columnNumber);
		setComparator(comparator);
		setValue(value);
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public Filter setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
		return this;
	}

	public String getComparator() {
		return comparator;
	}

	public Filter setComparator(String comparator) {
		this.comparator = comparator;
		return this;
	}

	public String getValue() {
		return value;
	}

	public Filter setValue(String value) {
		this.value = value;
		return this;
	}
}
