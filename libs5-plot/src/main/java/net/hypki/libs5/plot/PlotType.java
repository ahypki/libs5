package net.hypki.libs5.plot;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import net.hypki.libs5.utils.string.StringUtilities;

public enum PlotType {
	POINTS,
	LINES,
	STEPS,
	BOXES,
//	MAP,
//	PARALLEL_COORDINATES,
//	SCATTERPLOT_MATRIX
	;
	
	public static PlotType parse(String toParse, PlotType defaultType) {
		if (nullOrEmpty(toParse))
			return defaultType;
		else if (toParse.equalsIgnoreCase("lines"))
			return LINES;
		else if (toParse.equalsIgnoreCase("points"))
			return POINTS;
		else if (toParse.equalsIgnoreCase("boxes"))
			return BOXES;
//		else if (toParse.equalsIgnoreCase("matrix"))
//			return SCATTERPLOT_MATRIX;
//		else if (toParse.equalsIgnoreCase(SCATTERPLOT_MATRIX.toString()))
//			return SCATTERPLOT_MATRIX;
//		else if (toParse.equalsIgnoreCase("parallel"))
//			return PARALLEL_COORDINATES;
//		else if (toParse.equalsIgnoreCase(PARALLEL_COORDINATES.toString()))
//			return PARALLEL_COORDINATES;
		else
			return defaultType;
	}
}
