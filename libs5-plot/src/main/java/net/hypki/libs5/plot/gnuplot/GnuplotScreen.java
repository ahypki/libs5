package net.hypki.libs5.plot.gnuplot;

import java.io.IOException;

import net.hypki.libs5.plot.Figure;

public class GnuplotScreen extends Gnuplot {

	public GnuplotScreen() {
		
	}
	
	@Override
	public Figure plot() throws IOException {
		return gnuplot(null);
	}
}
