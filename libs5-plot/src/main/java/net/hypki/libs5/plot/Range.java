package net.hypki.libs5.plot;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class Range implements Serializable {

	@Expose
	private Double from = null;
	
	@Expose
	private Double to = null;
	
	public Range() {
		
	}
	
	public Range(double from, double to) {
		setFrom(from);
		setTo(to);
	}

	public Double getFrom() {
		return from;
	}

	public void setFrom(Double from) {
		this.from = from;
	}

	public Double getTo() {
		return to;
	}

	public void setTo(Double to) {
		this.to = to;
	}
}
