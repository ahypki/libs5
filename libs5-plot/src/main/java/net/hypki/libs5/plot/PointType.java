package net.hypki.libs5.plot;

public enum PointType {
	PLUS,
	MINUX,
	STAR,
	SQUARE,
	DOT,
	TRIANGLE,
	CIRCLE
}
