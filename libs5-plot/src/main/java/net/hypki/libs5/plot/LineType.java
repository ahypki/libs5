package net.hypki.libs5.plot;

public enum LineType {
	SOLID,
	DASHED,
	DOTTED
}
