package net.hypki.libs5.plot.unittests;

import java.io.IOException;

import net.hypki.libs5.plot.gnuplot.GnuplotWrapper;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;

import org.junit.Test;

public class GnuplotWrapperUT {


	@Test
	public void testGnuplotWrapper() throws IOException {
		GnuplotWrapper gnu = new GnuplotWrapper("set term pdf enhanced color;"
				+ "set output \"test.pdf\"; "
				+ "plot "
				+ "		x title \"a,,\\\",,bc\", "
				+ "		x title \"dd\" w p;")
			.run();
		
		for (FileExt f : gnu.iterateOutputFiles()) {
			LibsLogger.info(GnuplotWrapperUT.class, "Output " + f.getAbsolutePath());
		}
	}
}
