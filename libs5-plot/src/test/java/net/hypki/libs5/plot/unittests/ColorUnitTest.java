package net.hypki.libs5.plot.unittests;

import net.hypki.libs5.plot.Color;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class ColorUnitTest extends LibsTestCase {

	@Test
	public void testColors() {
		Color lg = new Color(212, 212, 212);
		System.out.println(lg.hashCode());
		System.out.println(lg);
		
		lg = new Color(Color.MAX, 0, 0);
		System.out.println(lg.hashCode());
		System.out.println(lg);
		
		System.out.println(Color.RED);
		System.out.println(Color.GREEN);
		System.out.println(Color.BLUE);
	}
}
