package net.hypki.libs5.plot.unittests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import net.hypki.libs5.plot.Plot;
import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.plot.gnuplot.Gnuplot;
import net.hypki.libs5.plot.gnuplot.GnuplotFile;
import net.hypki.libs5.plot.gnuplot.GnuplotScreen;
import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class GnuplotDensityMapUnitTest extends LibsTestCase {
	
	private File createTestMap(int size) throws IOException {
		int middleX = size / 2 + RandomUtils.nextInt(size / 4);
		int middleY = size / 2 + RandomUtils.nextInt(size / 4);
		
		TripleMap<Integer, Integer, Integer> points = new TripleMap<>(0);
		for (int i = 0; i < size * 15000; i++) {
			int signX = RandomUtils.nextInt(2) == 0 ? -1 : 1; 
			int signY = RandomUtils.nextInt(2) == 0 ? -1 : 1;
			int x = middleX + signX * (int) (Math.sin(RandomUtils.nextDouble()) * (size / 4.0));
			int y = middleY + signY * (int) (Math.sin(RandomUtils.nextDouble()) * (size / 4.0));
			points.put(x, y, points.get(x, y) + 1);
		}
		
		File f = new File("test-map.dat");
		FileWriter fw = new FileWriter("test-map.dat");
		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				fw.append("" + row + " " + col + " " + points.get(row, col) + "\n");
			}
			fw.append("\n");
		}
		fw.close();
		
		return f;
	}

	@Test
	public void testGnuplot() throws IOException {
		
		File testMap = createTestMap(500);
		
		
		for (int i = 0; i < 3; i++) {
			Gnuplot gnuplot = null;
			if (i == 0)
				gnuplot = new GnuplotScreen();
			else if (i == 1)
				gnuplot = new GnuplotFile(new File("ut-gnuplot-map.pdf"));
			else
				gnuplot = new GnuplotFile(new File("ut-gnuplot-map.png"));
			
			gnuplot
				.setTitle("Test density map")
				
				.addPlot(new Plot()
								.setLegend("Density map")
//								.setPlotType(PlotType.MAP)// TODO
								.setLabelColumn("3")
								.setDataSource(testMap.getAbsolutePath()))
				
				.plot();
		}
	}
}
