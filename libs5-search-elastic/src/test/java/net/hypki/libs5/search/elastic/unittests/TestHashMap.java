package net.hypki.libs5.search.elastic.unittests;
import java.util.HashMap;

import com.google.gson.annotations.Expose;

public class TestHashMap {
	
	@Expose
	private String s = null;
	
	@Expose
	private int i = 0;
	
	@Expose
	private HashMap<String, String> map = null;
	
	public TestHashMap() {
		
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public HashMap<String, String> getMap() {
		if (map == null)
			map = new HashMap<String, String>();
		return map;
	}

	public void setMap(HashMap<String, String> map) {
		this.map = map;
	}
}
