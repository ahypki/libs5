package net.hypki.libs5.search.elastic.unittests;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.utils.json.JsonUtils;

public class Person {

	@Expose
	private String id = null;
	
	@Expose
	private String name = null;
	
	@Expose
	private long age = 0;
	
	public Person() {
		
	}
	
	public Person(String id, String name, long age) {
		setId(id);
		setName(name);
		setAge(age);
	}
	
	@Override
	public String toString() {
		return getData();
	}
	
	public String getData() {
		return JsonUtils.objectToStringPretty(this);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getAge() {
		return age;
	}

	public void setAge(long age) {
		this.age = age;
	}
}
