package net.hypki.libs5.search.elastic.unittests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class HashMapIndexingUnitTest extends LibsTestCase {

	@Test
	public void testHashMap() throws FileNotFoundException, IOException {
//		System.setProperty("hazelcast.logging.type", "log4j");
//		System.setProperty(Settings.WEBLIBS_SETTINGS, "settingsUnitTests.json");
		Settings.init(new String[]{"--settings", "settingsUnitTests.json"});
		
		final String index = "testindex4";
		final String type = TestHashMap.class.getSimpleName().toLowerCase();
		final String id = "test-id2";
		
		TestHashMap t = new TestHashMap();
		t.setI(15002901);
		t.setS("tysiąc dziewięćset");
		t.getMap().put("jeden", "dwa");
		t.getMap().put("trzy", "cztery");
		t.getMap().put("data", SimpleDate.now().toStringHuman());
		
		ElasticSearchProvider elas = new ElasticSearchProvider();
		elas.init(Settings.getSettings());
		
		System.out.println("Indexing " + JsonUtils.objectToString(t));
		elas.indexDocument(index, type, id, JsonUtils.objectToString(t));
		
		Query q = new Query();
		q.addTerm(new StringTerm("s", "tysiąc"));
		
		takeANap(1000);
		
		List<TestHashMap> hashs = elas.search(TestHashMap.class, index, type, q, 0, 10).getObjects();
		
		for (TestHashMap testHashMap : hashs) {
			System.out.println("Found " + JsonUtils.objectToString(testHashMap));
		}
		
		elas.removeDocument(index, type, id);
		
		assertTrue(hashs.size() == 1);
	}
}
