package net.hypki.libs5.search.elastic.unittests;

import java.io.IOException;

import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;

public class ElasticTestCase extends LibsTestCase {
	
	private SearchEngineProvider searchInstance = null;
	
	public ElasticTestCase() {
		
	}

	protected SearchEngineProvider searchInstance() throws IOException {
		if (searchInstance == null) {
			searchInstance = new ElasticSearchProvider();
			searchInstance.init(JsonUtils.parseJson("{\"ElasticSearch\" : { \"host\" : \"localhost\", \"port\" : 9300}}"));
		}
		return searchInstance;
	}
}
