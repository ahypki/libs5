package net.hypki.libs5.search.elastic.unittests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.LongTerm;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermComparator;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonWrapper;

import org.junit.Test;

public class LongRangeUnitTest extends ElasticTestCase {

	@Test
	public void testLongSearch() throws IOException {
		indexPerson("1", "one", 10);
		indexPerson("2", "two", 100);
		indexPerson("3", "three", 1000);
		
		takeANap(1500);
		
		assertTrue(new Query().addTerm(new StringTerm("id", "1")), "1");
		
		assertTrue(new Query().addTerm(new LongTerm("age", 10, TermComparator.LE, TermRequirement.MUST)), "1");
		assertTrue(new Query().addTerm(new LongTerm("age", 100, TermComparator.LE, TermRequirement.MUST)), "1", "2");
		assertTrue(new Query().addTerm(new LongTerm("age", 101, TermComparator.LE, TermRequirement.MUST)), "1", "2");
		assertTrue(new Query().addTerm(new LongTerm("age", 200, TermComparator.LE, TermRequirement.MUST)), "1", "2");
	}
	
	private void assertTrue(Query query, String ... expectedPersonsIds) throws IOException {
		List<Person> persons = searchPersons(query);
		
		List<String> expectedPersonsList = new ArrayList<String>();
		for (String id : expectedPersonsIds) {
			expectedPersonsList.add(id);
		}
		
		assertTrue(expectedPersonsList.size() == persons.size(), "Expected to find " + expectedPersonsList.size() + " persons, but found " + persons.size());
		for (Person person : persons) {
			assertTrue(expectedPersonsList.contains(person.getId()), "Person with id " + person.getId() + " was not expected to find");
		}
	}
	
	private List<Person> searchPersons(Query query) throws IOException {
		SearchResults<Person> res = searchInstance().search(Person.class, "unittest", "person", query, 0, 100);
		return res.getObjects();
	}
	
	private void indexPerson(String id, String name, long age) throws IOException {
		searchInstance().indexDocument("unittest", "person", id, new Person(id, name, age).getData());
	}
}
