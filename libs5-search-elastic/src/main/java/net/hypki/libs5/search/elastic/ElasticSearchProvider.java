package net.hypki.libs5.search.elastic;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.elasticsearch.client.Requests.deleteRequest;
import static org.elasticsearch.client.Requests.indexRequest;
import static org.elasticsearch.client.Requests.searchRequest;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.index.query.QueryBuilders.wildcardQuery;
import static org.elasticsearch.search.builder.SearchSourceBuilder.searchSource;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.commons.lang.NotImplementedException;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.admin.indices.refresh.RefreshResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchPhaseExecutionException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.indices.TypeMissingException;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.carrotsearch.hppc.ObjectLookupContainer;
import com.carrotsearch.hppc.cursors.ObjectCursor;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.BooleanTerm;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.DoubleTerm;
import net.hypki.libs5.search.query.IntTerm;
import net.hypki.libs5.search.query.LongRangeTerm;
import net.hypki.libs5.search.query.LongTerm;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermComparator;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.QuadrupleMap;
import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.Basher2;

public class ElasticSearchProvider implements SearchEngineProvider {
	
	private static Client elasticClient = null;
//	private static Client elasticClient2 = null;
//	private static Client elasticClient3 = null;
	private static int elasticPort = 0;
	private static String elasticHost = null;
//	private static int round = 0;
	
	private static boolean CACHE_ENABLED = true;
	private static QuadrupleMap<String, String, String, String> toIndexCached = new QuadrupleMap<String, String, String, String>();
	private static TripleMap<String, String, List<String>> toRemoveCached = new TripleMap<String, String, List<String>>();
	
	private static boolean increaseFieldsCountBeansTable = false;
	
	private static boolean shutdownRequested = false;
	
	private static long CACHE_SLEEP = 1000;

	@Override
	public void init(JsonElement settings) throws IOException {
		if (elasticClient == null) {
			elasticHost = settings.getAsJsonObject().get("ElasticSearch").getAsJsonObject().get("host").getAsString();
			elasticPort = settings.getAsJsonObject().get("ElasticSearch").getAsJsonObject().get("port").getAsInt();
			LibsLogger.debug(ElasticSearchProvider.class, "ElasticSearch connection details ", elasticHost, ":", elasticPort);
		}
		
		new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						flushCache(null, null, null, null);	
					} catch (Throwable e) {
						LibsLogger.error(ElasticSearchProvider.class, "Something wrong in Elastic flush Thread", e);
					}
					
					if (shutdownRequested)
						break;
					
					try {
						Thread.sleep(CACHE_SLEEP);
					} catch (Throwable e) {
						LibsLogger.error(ElasticSearchProvider.class, "Cannot put to sleep the Thread", e);
					}
				}
			}
		}.start();
	}

	private static Client getElasticSearch() throws UnknownHostException {
		if (elasticClient == null) {
//			elasticClient = new TransportClient().addTransportAddress(new InetSocketTransportAddress(elasticHost, elasticPort));
//			elasticClient.admin().indices().preparePutTemplate(name)
			Settings settings = Settings.builder()
//			        .put("cluster.name", "prod")
					.put("client.transport.sniff", true)
			        .build();

			for (int i = 0; i < 5; i++) {
				try {
					elasticClient = new PreBuiltTransportClient(settings)
					        .addTransportAddress(new TransportAddress(InetAddress.getByName(elasticHost), elasticPort));
				} catch (Throwable e) {
					if (e instanceof IllegalStateException
							&& e.getMessage().contains("availableProcessors is already set to"))
						LibsLogger.warn(ElasticSearchProvider.class, e.getMessage());
					else
						LibsLogger.error(ElasticSearchProvider.class, "1st attempt to get Elastic failed, sleeping 1 s", e);
					
					try {
						Thread.sleep(CACHE_SLEEP);
					} catch (Throwable e1) {
						LibsLogger.error(ElasticSearchProvider.class, "Cannot sleep", e);
					}
				}
				
				if (elasticClient != null) {
					LibsLogger.debug(ElasticSearchProvider.class, "Elastic search provider created successfully");
					break;
				}
			}
			
//			elasticClient2 = new PreBuiltTransportClient(settings)
//		        .addTransportAddress(new TransportAddress(InetAddress.getByName(elasticHost), elasticPort + 1));
//			
//			elasticClient3 = new PreBuiltTransportClient(settings)
//		        .addTransportAddress(new TransportAddress(InetAddress.getByName(elasticHost), elasticPort + 2));
		}
//		round = ++round % 3;
//		if (round == 0)
			return elasticClient;
//		else if (round == 1)
//			return elasticClient2;
//		else
//			return elasticClient3;
	}

	@Override
	public void shutdown() {
		if (elasticClient != null) {
			elasticClient.close();
			elasticClient = null;
		}
		shutdownRequested = true;
	}
	
	private synchronized void flushCache(String index, String type, String id, String data) {
		if (data != null) {
			toIndexCached.put(index, type, id, data);
			return;
		} else if (index != null) {
			List<String> tmp = toRemoveCached.get(index, type);
			if (tmp == null) {
				tmp = new ArrayList<String>();
				toRemoveCached.put(index, type, tmp);
			}
			tmp.add(id);
			return;
		}
		
		if (toIndexCached.size() == 0
				&& toRemoveCached.size() == 0)
			return;
		
		try {
			for (String index1 : toIndexCached.keySet()) {
				for (String type1 : toIndexCached.get(index1).keySet()) {
					if (toIndexCached.get(index1) != null)
						indexDocument(index1, type1, toIndexCached.get(index1).get(type1));
				}
			}
			
			for (String index1 : toRemoveCached.keySet()) {
				for (String type1 : toRemoveCached.get(index1).keySet()) {
					removeDocument(index1, type1, toRemoveCached.get(index1).get(type1));
				}
			}
			
			toIndexCached.clear();
			toRemoveCached.clear();
		} catch (IOException e) {
			LibsLogger.error(ElasticSearchProvider.class, "Cannot flush cache", e);
			
			try {
				Thread.sleep(CACHE_SLEEP);
			} catch (Throwable e1) {
				LibsLogger.error(ElasticSearchProvider.class, "Cannot sleep", e);
			}
		}
	}
	
//	@Override
//	public void isIndexEmpty(String index) throws IOException {
//		getElasticSearch().
//	}
	
	@Override
	public boolean isIndexTypeEmpty(String index, String type) throws IOException {
		SearchSourceBuilder search = searchSource()
//				.query("*")
				.from(0)
				.size(10);
		
		SearchResponse response = getElasticSearch()
				.search(searchRequest(index)
						.types(type)
						.source(search)
				)
				.actionGet();
			
		return response.getHits().getTotalHits() == 0;
	}
	
	@Override
	public void clearIndex(String index) throws IOException {
//		try {
		ObjectLookupContainer<String> keys = getElasticSearch().admin().cluster() .prepareState().execute() .actionGet().getState() .getMetaData().indices().keys();
		
		Iterator<ObjectCursor<String>> iter = keys.iterator();
		while(iter.hasNext()) {
			ObjectCursor<String> s = iter.next();
			int idx = s.value.indexOf(':');
			if (idx > 0) {
				String typeTmp = s.value.substring(idx + 1);
				clearIndexType(index, typeTmp);
			}
		}

		
//		for (String key : keys.) {
//			clearIndexType(index, type);
//		}
		
//			getElasticSearch()
//				.admin()
//				.indices()
//				.delete(new DeleteIndexRequest(index))
//				.actionGet();
//		} catch (IndexMissingException e) {
//			LibsLogger.warn(ElasticSearchProvider.class, "Index ", index, " does not exist yet, ignoring exception");
//		}
		LibsLogger.debug(ElasticSearchProvider.class, String.format("Index %s cleared for all types", index));
	}
	
	@Override
	public void clearIndexType(String index, String type) throws IOException {
		try {
			LibsLogger.debug(ElasticSearchProvider.class, "Clearing index ", index, " type ", type);
			getElasticSearch()
				.admin()
				.indices()
				.delete(new DeleteIndexRequest(index + ":" + type)).actionGet();
//			getElasticSearch().delete(deleteRequest(index + ":" + type)
//					.type(type)
////					.id("*")
//					).actionGet();
//			getElasticSearch().admin().indices().delete(new DeleteIndexRequest(index).) Mapping(new DeleteIndexRequest(index).types(type)).actionGet();
//		} catch (IndexMissingException e) {
//			LibsLogger.warn(ElasticSearchProvider.class, "Index ", index, " does not exist yet, ignoring exception");
		} catch (Exception e) {
			if (e instanceof TypeMissingException
					|| e instanceof IndexNotFoundException)
				LibsLogger.warn(ElasticSearchProvider.class, "Type ", type, " in index is not present yet, ignoring exception");
			else
				throw e;
		}
		LibsLogger.debug(ElasticSearchProvider.class, String.format("Index %s cleared for type %s", index, type));
	}

	@Override
	public void indexDocument(String index, String type, String id, String data) throws IOException {
		if (CACHE_ENABLED) {
			flushCache(index, type, id, data);
			return;
		}
		
		IndexResponse resp = getElasticSearch().index(
				indexRequest(index + ":" + type)
					.type(type)
					.id(id)
					.source(data, XContentType.JSON)
					).actionGet();
//		LibsLogger.debug(ElasticSearchProvider.class, String.format("Document with id %s from type %s added to index %s", id, type, index));
	}
	
	@Override
	public void indexDocument(String index, String type, Map<String, String> idData) throws IOException {
		if (idData == null || idData.size() == 0)
			return;
		
		BulkRequest bulk = new BulkRequest();
		for (String key : idData.keySet()) {
			
			if (nullOrEmpty(key))
				continue;
			
			if (key.length() > 510) {
				LibsLogger.error(ElasticSearchProvider.class, "Key is longer than 510 in ES: " + key);
			}
			
//			JsonObject obj = JsonUtils.parseJson(idData.get(key)).getAsJsonObject(); 
//			obj.remove("pk");
//			String tmp = obj.toString();
			bulk.add(indexRequest(index + ":" + type)
					.type(type)
					.id(key)
					.source(idData.get(key), XContentType.JSON)
					);
		}
		
		BulkResponse resp = getElasticSearch().bulk(bulk).actionGet();
		
		if (resp.hasFailures())
			throw new IOException(resp.buildFailureMessage());
		
		LibsLogger.debug(ElasticSearchProvider.class, "Bulk done for ", idData.size(), " objects");
	}
	
	public void removeDocument(String index, String type, List<String> ids) throws IOException {
		BulkRequest bulk = new BulkRequest();
		for (String id : ids) {
			bulk.add(deleteRequest(index + ":" + type)
					.type(type)
					.id(id)
					);
		}
		
		BulkResponse resp = getElasticSearch().bulk(bulk).actionGet();
		
		if (resp.hasFailures())
			throw new IOException(resp.buildFailureMessage());
		
		LibsLogger.debug(ElasticSearchProvider.class, "Bulk remove done for ", ids.size(), " objects");
	}

	@Override
	public void removeDocument(String index, String type, String id) throws IOException {
		if (CACHE_ENABLED) {
			flushCache(index, type, id, null);
			return;
		}
		
		/*DeleteResponse deleteResponse = */getElasticSearch().delete(deleteRequest(index + ":" + type)
				.type(type)
				.id(id)
				).actionGet();
		LibsLogger.debug(ElasticSearchProvider.class, String.format("Document with id %s from type %s removed from index %s", id, type, index));
	}
	
	@Override
	public void remove(String index, String type, Query query) throws IOException {
		try {
			for (int i = 0; i < 500; i++) {
				SearchHits hits = searchHits(index, type, query, 0, 1000);
				
				if (hits.getTotalHits() == 0)
					break;
				
				if (i == 499) {
					LibsLogger.error("Something is wrong, cannot remove all hist");
				}
				
				for (SearchHit hit : hits) {
					removeDocument(index, type, hit.getId());
				}
				
				if (hits.getTotalHits() < 1000)
					break;
			}
		} catch (IndexNotFoundException e) {
			LibsLogger.warn(ElasticSearchProvider.class, "Type ", type, " in index is not present yet, ignoring exception");
		}
		
		
//		BoolQueryBuilder elasticQuery = boolQuery();
//		addTerms(query.getTerms(), elasticQuery);
//		
//		QuerySourceBuilder search = new QuerySourceBuilder().setQuery(elasticQuery);
//		
//		getElasticSearch().deleteByQuery(deleteByQueryRequest(index)
//				.types(type)
//				.source(search)
//				).actionGet();
//		LibsLogger.debug(ElasticSearchProvider.class, String.format("Documents with query %s of type %s removed from index %s", query, type, index));
	}
	
	private static QueryBuilder toElasticTerm(Term t) {
		if (t instanceof StringTerm)
			return termQuery(t.getField(), ((StringTerm) t).getTerm());
//			return termQuery(t.getField(), ((StringTerm) t).getTerm());
		else if (t instanceof BooleanTerm)
			return termQuery(t.getField(), ((BooleanTerm) t).getTerm());
		else if (t instanceof WildcardTerm)
			return wildcardQuery(t.getField(), "*" + ((WildcardTerm)t).getTerm() + "*");
		else if (t instanceof Bracket) {
			Bracket bracket = (Bracket) t;
			BoolQueryBuilder bracketQuery = boolQuery();
			addTerms(bracket.getTerms(), bracketQuery);
			return bracketQuery;
		} else if (t instanceof LongRangeTerm) {
			LongRangeTerm range = (LongRangeTerm) t;
			return rangeQuery(t.getField()).from(range.getFrom()).to(range.getTo());
		} else if (t instanceof IntTerm) {
			return termQuery(t.getField(), ((IntTerm) t).getTerm());
		} else if (t instanceof DoubleTerm) {
			DoubleTerm dt = (DoubleTerm) t;
			if (dt.getComparator() == TermComparator.G)
				return rangeQuery(t.getField()).gt(dt.getTerm());
			else if (dt.getComparator() == TermComparator.GE)
				return rangeQuery(t.getField()).gte(dt.getTerm());
			else if (dt.getComparator() == TermComparator.L)
				return rangeQuery(t.getField()).lt(dt.getTerm());
			else if (dt.getComparator() == TermComparator.LE)
				return rangeQuery(t.getField()).lte(dt.getTerm());
			else
				return termQuery(t.getField(), dt.getTerm());
		} else if (t instanceof LongTerm) {
			LongTerm longTerm = (LongTerm) t;
			if (longTerm.getComparator() == TermComparator.G)
				return rangeQuery(t.getField()).gt(longTerm.getTerm());
			else if (longTerm.getComparator() == TermComparator.GE)
				return rangeQuery(t.getField()).gte(longTerm.getTerm());
			else if (longTerm.getComparator() == TermComparator.L)
				return rangeQuery(t.getField()).lt(longTerm.getTerm());
			else if (longTerm.getComparator() == TermComparator.LE)
				return rangeQuery(t.getField()).lte(longTerm.getTerm());
			else
				return termQuery(t.getField(), longTerm.getTerm());
		} else
			throw new NotImplementedException("Unimplemented case " + t.getClass().getName());
	}
	
	private static void addTerms(List<Term> terms, BoolQueryBuilder elasticQuery) {
		for (Term t : terms) {
			if (t.getRequirement() == TermRequirement.SHOULD) {
				elasticQuery.should(toElasticTerm(t));
//				if (t instanceof StringTerm)
//					elasticQuery.should(termQuery(t.getField(), ((StringTerm) t).getTerm()));
//				else if (t instanceof BooleanTerm)
//					elasticQuery.should(termQuery(t.getField(), ((BooleanTerm) t).getTerm()));
//				else if (t instanceof WildcardTerm)
//					elasticQuery.should(wildcardQuery(t.getField(), "*" + ((WildcardTerm) t).getTerm() + "*"));
//				else if (t instanceof Bracket) {
//					Bracket bracket = (Bracket) t;
//					BoolQueryBuilder bracketQuery = boolQuery();
//					elasticQuery.should(bracketQuery);
//					addTerms(bracket.getTerms(), bracketQuery);
//				} else if (t instanceof IntTerm) {
//					elasticQuery.should(termQuery(t.getField(), ((IntTerm) t).getTerm()));
//				} else
//					throw new NotImplementedException();
			} else if (t.getRequirement() == TermRequirement.MUST_NOT) {
				if (t instanceof StringTerm)
					elasticQuery.mustNot(termQuery(t.getField(), ((StringTerm) t).getTerm()));
				else if (t instanceof BooleanTerm)
					elasticQuery.mustNot(termQuery(t.getField(), ((BooleanTerm) t).getTerm()));
				else if (t instanceof WildcardTerm)
					elasticQuery.mustNot(wildcardQuery(t.getField(), "*" + ((WildcardTerm) t).getTerm() + "*"));
				else if (t instanceof Bracket) {
					Bracket bracket = (Bracket) t;
					BoolQueryBuilder bracketQuery = boolQuery();
					elasticQuery.mustNot(bracketQuery);
					addTerms(bracket.getTerms(), bracketQuery);
				} else if (t instanceof IntTerm) {
					elasticQuery.mustNot(termQuery(t.getField(), ((IntTerm) t).getTerm()));
				} else
					throw new NotImplementedException();
			} else if (t.getRequirement() == TermRequirement.MUST) {
				elasticQuery.must(toElasticTerm(t));
//				if (t instanceof StringTerm)
//					elasticQuery.must(termQuery(t.getField(), ((StringTerm) t).getTerm()));
//				else if (t instanceof BooleanTerm)
//					elasticQuery.must(termQuery(t.getField(), ((BooleanTerm) t).getTerm()));
//				else if (t instanceof WildcardTerm)
//					elasticQuery.must(wildcardQuery(t.getField(), "*" + ((WildcardTerm)t).getTerm() + "*"));
//				else if (t instanceof Bracket) {
//					Bracket bracket = (Bracket) t;
//					BoolQueryBuilder bracketQuery = boolQuery();
//					elasticQuery.must(bracketQuery);
//					addTerms(bracket.getTerms(), bracketQuery);
//				} else if (t instanceof LongRangeTerm) {
//					LongRangeTerm range = (LongRangeTerm) t;
//					elasticQuery.must(rangeQuery(t.getField()).from(range.getFrom()).to(range.getTo()));
//				} else if (t instanceof IntTerm) {
//					elasticQuery.must(termQuery(t.getField(), ((IntTerm) t).getTerm()));
//				} else if (t instanceof DoubleTerm) {
//				} else
//					throw new NotImplementedException("Unimplemented case " + t.getClass().getName());
			} else
				throw new NotImplementedException();
		}
	}
	
	@Override
	public <T> T getDocument(Class<T> clazz, String index, String type, String id) throws IOException {
		GetResponse response = getElasticSearch()
				.prepareGet(index, type, id)
				.execute()
				.actionGet();
		return response.isExists() ? JsonUtils.fromJson(response.getSourceAsString(), clazz) : null;
	}
	
	private SearchHits searchHits(String index, String type, Query query, int from, int size) throws IOException {
		long start = System.currentTimeMillis();
		try {
			
			if (query == null)
				return null;
			
			// convert Query to ElasticSearch query
			BoolQueryBuilder elasticQuery = boolQuery();
			addTerms(query.getTerms(), elasticQuery);
			
	//		.should(wildcardQuery("email.email", "*" + searchQuery + "*"))
	//		.should(wildcardQuery("login", "*" + searchQuery + "*"));
			
			SearchSourceBuilder search = searchSource()
				.query(elasticQuery)
				.from(from)
				.size(size);
			
			if (query.getSortBy() != null)
				search = search.sort(query.getSortBy(), query.getSortOrder() == net.hypki.libs5.search.query.SortOrder.ASCENDING ? SortOrder.ASC : SortOrder.DESC);
			
			LibsLogger.trace(ElasticSearchProvider.class, "ES search ", search);
			
			SearchResponse response = getElasticSearch()
				.search(searchRequest(index + ":" + type)
						.types(type)
						.source(search)
				).actionGet();
			
			LibsLogger.debug(ElasticSearchProvider.class, "Search done in [ms] " + (System.currentTimeMillis() - start), " ", response.getHits().totalHits, " hits");
			return response.getHits();
		} catch (SearchPhaseExecutionException ex) {
			if (ex.getMessage().contains("all shards failed;"))
				LibsLogger.warn(ElasticSearchProvider.class, "There is most likely no index of the type ", type);
			else
				throw ex;
			return null;
		} finally {
		}
	}
	
	@Override
	public SearchResults<String> searchIds(String index, String type, Query query, int from, int size) throws IOException {
		SearchHits searchHits = searchHits(index, type, query, from, size);
		
		List<String> objs = new ArrayList<>();
		
		if (searchHits == null)
			return new SearchResults<String>(objs, searchHits.getTotalHits());
		
		for (SearchHit hit : searchHits.getHits()) {
			try {
				objs.add(hit.getId());
			} catch (Exception e) {
				LibsLogger.error(ElasticSearchProvider.class, "Cannot deserialize " + hit.getSourceAsString(), e);
			}
		}
		LibsLogger.debug(ElasticSearchProvider.class, String.format("Query %s executed for type %s for index %s, found %d objects", query.toString(), type, index, searchHits.getTotalHits()));
		
		return new SearchResults<String>(objs, searchHits.getTotalHits());
	}
	
	@Override
	public <T> SearchResults<T> search(Class<T> clazz, String index, String type, Query query, int from, int size) throws IOException {
		try {
			SearchHits searchHits = searchHits(index, type, query, from, size);
			
			List<T> objs = new ArrayList<T>();
			
			if (searchHits == null)
				return new SearchResults<T>(objs, searchHits.getTotalHits());
			
			for (SearchHit hit : searchHits.getHits()) {
				try {
					objs.add(JsonUtils.fromJson(hit.getSourceAsString(), clazz));
				} catch (Exception e) {
					LibsLogger.error(ElasticSearchProvider.class, "Cannot deserialize " + hit.getSourceAsString(), e);
				}
			}
			LibsLogger.debug(ElasticSearchProvider.class, String.format("Query %s executed for type %s for index %s, found %d objects", query.toString(), type, index, searchHits.getTotalHits()));
			
			return new SearchResults<T>(objs, searchHits.getTotalHits());
		} catch (IndexNotFoundException e) {
			LibsLogger.warn(ElasticSearchProvider.class, "Index " + index + " of type " + type + " does not exist yet");
			return new SearchResults<T>(null, 0);
		}
	}
	
	@Override
	public void close() {
//		try {
			shutdown();
//		} catch (UnknownHostException e) {
//			LibsLogger.error(ElasticSearchProvider.class, "Cannot close Elastic", e);
//		}
	}
	
	public static boolean increaseFieldsCount(String index, String type) {
		try {
			StringBuilder sb = new StringBuilder();
			for (String line : new Basher2()
								.add("/usr/bin/curl")
								.add("--silent")
								.add("-XPUT", "localhost:9200/" + index + ":" + type + "/_settings")
								.add("-H", "Content-Type: application/json")
								.add("-d {\"index\" : {\"mapping\" : {\"total_fields\" : {\"limit\" : \"100000\"}}}}")
								.runIter()) {
				sb.append(line);
			}
			
			if (JsonUtils.isJsonSyntax(sb.toString())) {
				JsonElement json = JsonUtils.parseJson(sb.toString());
				if (json.isJsonObject()
						&& json.getAsJsonObject().get("acknowledged") != null)
					return json.getAsJsonObject().get("acknowledged").getAsBoolean();
			}
			return false;
		} catch (IOException e) {
			LibsLogger.error(ElasticSearchProvider.class, "Cannot increase the fields count for " + index + ":" + type, e);
			return false;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(increaseFieldsCount("beansdev3", "notebookentry"));
	}
}
