package net.hypki.libs5.weblibs.unittests.scheduling;

import java.io.IOException;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.ScheduleManager;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

import org.junit.Test;


public class SchedulingUnitTest extends WeblibsTestCase {

	@Test
	public void testJobs() throws InterruptedException, IOException {
		// schedule a job
		ScheduleManager.schedule(SimplePrintJob.class, 1);
		
		// wait long enough so that the scheduler has an opportunity to run the job!
		LibsLogger.debug("------- Waiting 65 seconds... -------------");
//		takeANap(65 * 1000);
		
		LibsLogger.debug("------- UnitTest finished -------------");
	}

	@Test
	public void testRecuringJobs() throws InterruptedException, IOException {
		// schedule a job
		ScheduleManager.scheduleRecursively(SimplePrintJob.class, 1);
		
		// wait long enough so that the scheduler has an opportunity to run the job!
		LibsLogger.debug("------- Waiting 125 seconds... -------------");
//		takeANap(125 * 1000);
		
		LibsLogger.debug("------- UnitTest finished -------------");
	}
}
