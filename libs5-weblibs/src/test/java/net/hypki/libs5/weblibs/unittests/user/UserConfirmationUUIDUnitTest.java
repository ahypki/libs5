package net.hypki.libs5.weblibs.unittests.user;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.UserConfirmationUUID;

import org.junit.Test;


public class UserConfirmationUUIDUnitTest extends WeblibsTestCase {
	@Test
	public void testCreateRemove() throws IOException, ValidationException {
		UserConfirmationUUID u = new UserConfirmationUUID();
		u.setConfirmUuid(new UUID(RandomGUID.getRawRandomGUID()));
		u.setUserUUID(new UUID(RandomGUID.getRawRandomGUID()));
		u.save();
		
		addToRemove(u);
		
		UserConfirmationUUID uConfirmFromDb = UserConfirmationUUID.getUserConfirmationUUID(u.getUserUUID());
		assertTrue(u.getUserUUID().equals(uConfirmFromDb.getUserUUID()));
		
		u.setClicked(true);
		u.save();
		
		uConfirmFromDb = UserConfirmationUUID.getUserConfirmationUUID(u.getUserUUID());
		assertTrue(u.getUserUUID().equals(uConfirmFromDb.getUserUUID()));
		assertTrue(u.getClicked() == true);
	}
}
