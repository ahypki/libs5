package net.hypki.libs5.weblibs.unittests.user;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.image.ImageUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserData;

import org.junit.Test;

public class UserDataUnitTest extends WeblibsTestCase {
	
	@Test
	public void testLogoCrop() throws IOException, ValidationException, InterruptedException {
		User user = testUser();
		
		byte[] logo = FileUtils.getBytesFromFile(new File(WeblibsConst.class.getClassLoader().getResource("testLogo.png").getPath()));
		BufferedImage logoScaled = ImageUtils.getThumbnailAndCrop(ImageUtils.byteArrayToBufferedImage(logo), 90, 90, true, 90, 90);
		
		ImageUtils.bufferedImageToFile(logoScaled, "test-logo-scaled.png");
		
		UserData ud = new UserData();
		ud.setUserId(user.getUserId());
		ud.setLogo(ImageUtils.bufferedImageToByteArray(logoScaled));
		ud.save();
		
		addToRemove(ud);
		
		UserData udFromDb = UserData.getUserData(user.getUserId());
		assertTrue(udFromDb != null);
		assertTrue(udFromDb.getLogo().length == ImageUtils.bufferedImageToByteArray(logoScaled).length);
	}

	@Test
	public void testCreate() throws IOException, ValidationException {
		User user = testUser();
		
		byte[] logo = FileUtils.getBytesFromFile(new File(WeblibsConst.class.getClassLoader().getResource("testLogo.png").getPath()));
		
		UserData ud = new UserData();
		ud.setUserId(user.getUserId());
		ud.setLogo(logo);
		ud.save();
		
		addToRemove(ud);
		
		UserData udFromDb = UserData.getUserData(user.getUserId());
		assertTrue(udFromDb != null);
		assertTrue(udFromDb.getLogo().length == logo.length);
	}
}
