package net.hypki.libs5.weblibs.unittests.backup;

import java.util.List;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.TxObject;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.CookieUUID;
import net.hypki.libs5.weblibs.user.CookieUUIDAppId;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserConfirmationUUID;
import net.hypki.libs5.weblibs.user.UserUUID;

import org.junit.Test;


public class BackupUserUnitTest extends WeblibsTestCase {
	
//	@Test
//	public void testBackup() throws IOException, ValidationException {
//		User user = createTestUser();
//		createTestItem(user);
//		createTestItem(user);
//		
//		BackupUtils.backupUser(user, new File("test-backup"));
//	}

	/**
	 * @throws ClassNotFoundException 
	 * 
	 * Checks whether new classes (extending {@link CassandraObject}) appeared in the code - of true then one has
	 * to add this class to Backup
	 */
	@Test
	public void testNewClassesForBackup() throws ClassNotFoundException {
		List<Class<?>> list = ReflectionUtility.getClasses(WeblibsObject.class, DbObject.class, true);
		
		// removing all known classes from the list 
		list.remove(DbObject.class);
		list.remove(WeblibsObject.class);
		list.remove(CookieUUIDAppId.class);
		list.remove(CookieUUIDAppId.class);
		list.remove(UserConfirmationUUID.class);
		list.remove(Allowed.class);
		list.remove(User.class);
		list.remove(UserUUID.class);
		list.remove(CookieUUID.class);
		list.remove(Property.class);
		list.remove(TxObject.class);
		list.remove(Job.class);
		
		// if the list not empty then there are some new classes which have to added to backup files
		StringBuilder builder = new StringBuilder();
		for (Class<?> clazz : list) {
			builder.append(clazz.getSimpleName() + ", ");
		}
		assertTrue(list.size() == 0, "Classes which have to be included in 'user backup' command: " + builder.toString());
	}
}
