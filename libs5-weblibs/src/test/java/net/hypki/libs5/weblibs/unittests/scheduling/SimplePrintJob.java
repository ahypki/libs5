package net.hypki.libs5.weblibs.unittests.scheduling;

import net.hypki.libs5.utils.LibsLogger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class SimplePrintJob implements Job {
	
//	public static final String CACHE_PRINTS_COUNT = "CACHE_PRINTS_COUNT"; 
	
	public SimplePrintJob() {
		
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LibsLogger.debug("Simple print job says hallo!");
		
//		cacheInstance().put(CACHE_PRINTS_COUNT, key, obj)
	}
}
