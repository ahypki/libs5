package net.hypki.libs5.weblibs.unittests.user;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.UserUUID;

import org.junit.Test;


public class UserUUIDUnitTest extends WeblibsTestCase {
	
	@Test
	public void testCreateRemove() throws IOException, ValidationException {
		UUID userId = new UUID(RandomGUID.getRawRandomGUID());
		Email email = new Email(RandomGUID.getRawRandomGUID() + "@test.com");
		
		UserUUID u = new UserUUID();
		u.setEmail(email);
		u.setUserUUID(userId);
		u.save();
		
		addToRemove(u);
		
		Email emailFromDB = UserUUID.getUserEmail(userId);
		assertTrue(email.equals(emailFromDB));
	}
	
	@Test(expected = ValidationException.class)
	public void testValidationShouldFail() throws IOException, ValidationException {
		UserUUID u = new UserUUID();
		u.setEmail(new Email("@")); // Validation exception
		u.setUserUUID(new UUID(RandomGUID.getRawRandomGUID()));
		u.save();
		
		addToRemove(u);
	}
}
