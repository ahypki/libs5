package net.hypki.libs5.weblibs.unittests.user;

import java.io.IOException;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.encryption.HashUtils;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserConfirmationUUID;
import net.hypki.libs5.weblibs.user.UserStatus;
import net.hypki.libs5.weblibs.user.UserUUID;

import org.junit.Test;


public class UserUnitTest extends WeblibsTestCase {
	
	@Test
	public void testCreateAndSearch() throws IOException, ValidationException, InterruptedException {
		Email email = new Email("test-mail@test.pl");
		String login = createRandomPronounceableWord();
		
		User user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID().toLowerCase());
		user.setEmail(email);
		user.setPlainPasswordForHashing("aaa");
		user.setLogin(login);
		user.save();
		
		System.out.println("Email: " + user.getEmail() + ", SHA1: " + user.getPasswordHash());
		
		addToRemove(user);
		
		SearchManager.index(user);
		
		takeANap(1500);
		
		// search the user
		List<User> objs = SearchManager.searchUsers(login, 0, 10).getObjects();
		for (WeblibsObject weblibsObject : objs) {
			System.out.println("Found in elastic: " + weblibsObject.getCombinedKey());
		}
		assertTrue(objs.size() == 1);
		
		objs = SearchManager.searchUsers(email.toString(), 0, 10).getObjects();
		for (WeblibsObject weblibsObject : objs) {
			System.out.println("Found in elastic: " + weblibsObject.getCombinedKey());
		}
		assertTrue(objs.size() == 1);
		
		objs = SearchManager.searchUsers("test", 0, 10).getObjects();
		for (WeblibsObject weblibsObject : objs) {
			System.out.println("Found in elastic: " + weblibsObject.getCombinedKey());
		}
		assertTrue(objs.size() == 1, "size is " + objs.size());
	}

	@Test
	public void testCreateAndTestRemove() throws IOException, ValidationException {
		Email email = new Email("test-mail@test.pl");
		
		User user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID().toLowerCase());
		user.setEmail(email);
		user.setPlainPasswordForHashing("aaa");
		user.setLogin("login");
		user.save();
		
		System.out.println("Login: " + user.getEmail() + ", SHA1: " + user.getPasswordHash());
		
		addToRemove(user);
		
		User userFromDB = User.getUser(email);
		assertTrue(user.getEmail().equals(userFromDB.getEmail()));
		assertTrue(user.getPasswordHash().equals(userFromDB.getPasswordHash()));
		
		// removing user
		user = User.getUser(email);
		if (user != null)
			user.remove();
		
		// check whether all other objects were removed
		assertTrue(User.getUser(user.getEmail()) == null);
		assertTrue(UserUUID.getUserUUID(user.getUserId()) == null);
		assertTrue(UserConfirmationUUID.getUserConfirmationUUID(user.getUserId()) == null);
	}
	
	@Test
	public void testCreate() throws IOException, ValidationException {
		Email email = new Email("test-mail2@test.pl");
		
		User user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID().toLowerCase());
		user.setEmail(email);
		user.setPlainPasswordForHashing("aaa");
		user.setLogin("login");
		user.save();
		
		System.out.println("Login: " + user.getEmail() + ", SHA1: " + user.getPasswordHash());
		
		addToRemove(user);
		
		User userFromDB = User.getUser(email);
		
		assertTrue(user.getEmail().equals(userFromDB.getEmail()));
		assertTrue(user.getPasswordHash().equals(userFromDB.getPasswordHash()));
	}

	@Test
	public void testUserStatus() throws Exception {
		
		Email email = new Email("test-email@bla.pl");
		
		User user = User.getUser(email);
		if (user != null)
			user.remove();
		
		user = new User();
		user.setUserUUID(new UUID(RandomGUID.getRawRandomGUID()));
		user.setEmail(email);
		user.setPlainPasswordForHashing("test-password");
//		user.setLogo(User.getDefaultLogo());
		user.setLogin("login");
		user.save();
		
		addToRemove(user);
		
		LibsLogger.debug("User login: " + user.getEmail().getOnlyLogin());
		
//		addToRemove(user);
		
		user.setStatus(UserStatus.NORMAL);
		user.save();
		
		User userFromDB = User.getUser(user.getEmail());
			
		assertTrue(userFromDB.getStatus() == UserStatus.NORMAL);
	}
	
//	@Test
//	public void testUserCreationWithImage() throws Exception {
//		
//		Email email = new Email("test-email2@bla.pl");
//		
//		User user = User.findUser(getAppId(), email);
//		if (user != null)
//			user.remove();
//		
//		File logo = new File(User.class.getResource("logo.png").getPath());
//		
//		user = new User();
//		user.setUserUUID(UUID.random());
//		user.setAppId(getAppId());
//		user.setEmail(email);
//		user.setPlainPasswordForHashing("test-password");
////		user.setLogo(FileUtilities.getBytesFromFile(logo));
//		user.setLogin("login");
//		user.save();
//		
//		addToRemove(user);
//		
//		UUID userUUID = user.getUserUUID();
//		User userFromDB = User.getUser(user.getAppId(), user.getEmail());
//			
//		assertTrue(user.getUserUUID().equals(userFromDB.getUserUUID()));
//		assertTrue(user.getAppId().equals(getAppId()));
//		assertTrue(user.getEmail().equals("test-email2@bla.pl"));
//		assertTrue(user.getUserUUID().equals(userUUID));
//		assertTrue(HashUtils.checkBlowfishHash("test-password", user.getPasswordHash()));
//		assertTrue(user.getStatus().equals(userFromDB.getStatus()));
//		assertTrue(userFromDB.getStatus() == UserStatus.NEW);
////		assertTrue(user.isAdmin() == false);
//		
//		// saving user's logo to the file and comparing with the original logo.png file
////		FileUtilities.saveToFile(user.getLogo(), new File("/tmp/users_logo.png"));
//		
//		String temp = ExecutingLinuxCommand.executeCommand(new String[] {"diff", logo.getAbsolutePath(), "/tmp/users_logo.png"}, new File("."));
//		System.out.println("diff result: " + temp);
//		assertTrue(temp.length() == 0, "User's logo file is not saved properly");
//	}

	@Test
	public void testUserCreation() throws Exception {

		Email email = new Email("test-email3@bla.pl");
		
		User user = User.getUser(email);
		if (user != null)
			user.remove();
		
		user = new User();
		user.setUserUUID(new UUID(RandomGUID.getRawRandomGUID()));
		user.setEmail(email);
		user.setPlainPasswordForHashing("test-password");
		user.setLogin("login");
		user.save();
		
		addToRemove(user);
		
		UUID userUUID = user.getUserId();
		User userFromDB = User.getUser(user.getEmail());
			
		assertTrue(user.getUserId().equals(userFromDB.getUserId()));
		assertTrue(user.getEmail().equals("test-email3@bla.pl"));
		assertTrue(user.getUserId().equals(userUUID));
		assertTrue(HashUtils.checkBlowfishHash("test-password", user.getPasswordHash()));
//		assertTrue(user.isAdmin() == false);
	}
	
	@Test
	public void testUserCreationAdminAndSuperAdmin() throws Exception {

		Email email = new Email("test-email5@bla.pl");
		
		User user = User.getUser(email);
		if (user != null)
			user.remove();
		
		user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID());
		user.setEmail(email);
		user.setPlainPasswordForHashing("test-password");
		user.setLogin("login");
		user.save();
		
		addToRemove(user);
		
		UUID userUUID = user.getUserId();
		User userFromDB = User.getUser(user.getEmail());
			
		assertTrue(user.getUserId().equals(userFromDB.getUserId()));
		assertTrue(user.getEmail().equals(email));
		assertTrue(user.getUserId().equals(userUUID));
		assertTrue(HashUtils.checkBlowfishHash("test-password", user.getPasswordHash()));
	}
	
	@Test
	public void testUserCreationAdminAndNotSuperAdmin() throws Exception {

		Email email = new Email("test-email4@bla.pl");
		
		User user = User.getUser(email);
		if (user != null)
			user.remove();
		
		user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID());
		user.setEmail(email);
		user.setPlainPasswordForHashing("test-password");
		user.setLogin("login");
		user.save();
		
		addToRemove(user);
		
		UUID userUUID = user.getUserId();
		User userFromDB = User.getUser(user.getEmail());
			
		assertTrue(user.getUserId().equals(userFromDB.getUserId()));
		assertTrue(user.getEmail().equals(email));
		assertTrue(user.getUserId().equals(userUUID));
		assertTrue(HashUtils.checkBlowfishHash("test-password", user.getPasswordHash()));
	}
	
	@Test
	public void testUserIdEmailRelation() throws IOException, ValidationException {
		String testEmail = RandomGUID.getRawRandomGUID() + "@test.pl";
		
		User user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID().toLowerCase());
		user.setEmail(testEmail);
		user.setPlainPasswordForHashing("aaa");
		user.setLogin("login");
		user.save();
		
		System.out.println("Login: " + user.getEmail() + ", SHA1: " + user.getPasswordHash());
		
		addToRemove(user);
		
		User userFromDB = User.getUser(new Email(testEmail));
		assertTrue(user.getEmail().equals(userFromDB.getEmail()));
		assertTrue(user.getPasswordHash().equals(userFromDB.getPasswordHash()));
		
		assertTrue(user.getEmail().equals(UserUUID.getUserEmail(user.getUserId())));
	}
	
	@Test(expected = ValidationException.class)
	public void testValidationEmail() throws IOException, ValidationException {
		User user = new User();
		user.setUserUUID(RandomGUID.getRawRandomGUID().toLowerCase());
		user.setEmail(new Email("a"));
		user.setPlainPasswordForHashing("aaa");
		user.setLogin("login");
		user.save();
		
		addToRemove(user);
		assertTrue(false, ValidationException.class.getSimpleName() + " should be thrown");
	}
	
	@Test
	public void testUserUniquity() throws IOException, ValidationException {
		User user = new User();
		user.setEmail("test-uniquity@test.com");
		user.setPlainPasswordForHashing("pass");
		user.setUserUUID(RandomGUID.getRawRandomGUID());
		user.setLogin("login");
		user.save();
		
		addToRemove(user);
		
		try {
			// create user with the same name and appId but with different userId - it shouldn't be created
			user = new User();
//			user.setAdmin(false);
			user.setEmail("test-uniquity@test.com");
			user.setPlainPasswordForHashing("pass-different");
			user.setUserUUID(RandomGUID.getRawRandomGUID());
			user.save();
			
			addToRemove(user);
			
			assertTrue("ValidationException was expected", false);
		} catch (ValidationException e) {
			// expected exception
		}
	}
}
