package net.hypki.libs5.weblibs.unittests.file;

import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class PathUnitTest extends LibsTestCase {
		
	@Test
	public void testPathNameValidation() {
		assertTrue(WeblibsPath.isNameValid("aabb"));
		assertTrue(WeblibsPath.isNameValid("aaBB"));
		assertTrue(WeblibsPath.isNameValid("aBc"));
		assertTrue(WeblibsPath.isNameValid("a"));
		assertTrue(WeblibsPath.isNameValid("a-a"));
		assertTrue(WeblibsPath.isNameValid("a_ab-b"));
		
		assertTrue(!WeblibsPath.isNameValid(""));
		assertTrue(!WeblibsPath.isNameValid(null));
		assertTrue(!WeblibsPath.isNameValid("a;b"));
	}
	
	@Test
	public void testPathNameToString() {
		assertTrue(new WeblibsPath("one").toString().equals("/one/"));
		assertTrue(new WeblibsPath("/one").toString().equals("/one/"));
		assertTrue(new WeblibsPath("/one/").toString().equals("/one/"));
		assertTrue(new WeblibsPath("//one").toString().equals("/one/"));
		assertTrue(new WeblibsPath("//one//").toString().equals("/one/"));
		
		assertTrue(new WeblibsPath("/one/two/").toString().equals("/one/two/"));
		assertTrue(new WeblibsPath("/one/two").toString().equals("/one/two/"));
		assertTrue(new WeblibsPath("/one//two//").toString().equals("/one/two/"));
		assertTrue(new WeblibsPath("/one/two//").toString().equals("/one/two/"));
		assertTrue(new WeblibsPath("one/two").toString().equals("/one/two/"));
		assertTrue(new WeblibsPath("one//two").toString().equals("/one/two/"));
		
		assertTrue(new WeblibsPath("one /two").toString().equals("/one/two/") == false);
	}
}
