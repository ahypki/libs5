package net.hypki.libs5.weblibs.unittests.jobs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.jobs.JobFactory;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

import org.junit.Test;

public class JobsUnitTest extends WeblibsTestCase {
	
//	@Test
//	public void testManyJobs() throws ValidationException, IOException {
//		
//		DbObject.getDatabaseProvider().removeTable(WeblibsConst.KEYSPACE, UTJob.COLUMN_FAMILY);
//		DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE, new UTJob(null, null).getTableSchema());
//		DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE, new TxObject().getTableSchema());
//		
//		final String NORMAL = "NORMAL";
//		final String FAST = "FAST";
//		
//		for (int i = 0; i < 1000; i++) {
//			new UTJob(RandomUtils.nextDouble() > 0.5 ? NORMAL : FAST, "name-" + i).save();			
//		}
//		
//		takeANap(150);
//		
//		for (Job job : Job.getOldestJobs(NORMAL, 100, DateUtils.YEAR_2008)) {
//			LibsLogger.debug(JobsUnitTest.class, "Oldest jobs from  ", NORMAL, " ", ((UTJob) job).getMsg());
////			job.remove();
//		}
//		
//		for (Job job : Job.getOldestJobs(FAST, 100, DateUtils.YEAR_2008)) {
//			LibsLogger.debug(JobsUnitTest.class, "Oldest jobs from  ", FAST, " ", ((UTJob) job).getMsg());
////			job.remove();
//		}
//		
//		runAllJobs(new String[] {NORMAL, FAST});
//		
//		assertTrue(CassandraUtils.getRowsCount(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY) == 0, "Job table expected to be empty");
//		
//		LibsLogger.debug(JobsUnitTest.class, "Finished!");
//	}

	@Test
	public void testJobsOrdering() throws ValidationException, IOException {
		
		DbObject.getDatabaseProvider().removeTable(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY);
		DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE, new Job() {
			@Override
			public void run() throws IOException, ValidationException {}
		}.getTableSchema());
		
		final String NORMAL = "NORMAL";
		final String FAST = "FAST";
		
		new UTJob(NORMAL, "normal1").save();
		takeANap(50);
		new UTJob(FAST, "fast1").save();
		takeANap(50);
		new UTJob(FAST, "fast2").save();
		takeANap(50);
		new UTJob(NORMAL, "normal2").save();
		takeANap(50);
		new UTJob(FAST, "fast3").save();
		takeANap(50);
		
		for (Job job : JobFactory.getOldestJobs(NORMAL, 100, DateUtils.YEAR_2008)) {
			LibsLogger.debug(JobsUnitTest.class, "Oldest jobs from  ", NORMAL, " ", ((UTJob) job).getMsg());
//			job.remove();
		}
		
		for (Job job : JobFactory.getOldestJobs(FAST, 100, DateUtils.YEAR_2008)) {
			LibsLogger.debug(JobsUnitTest.class, "Oldest jobs from  ", FAST, " ", ((UTJob) job).getMsg());
//			job.remove();
		}
		
		runAllJobs(new String[] {NORMAL, FAST});
		
		assertTrue(DbObject.getDatabaseProvider().countRows(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY, null) == 0, "Job table expected to be empty");
		
		LibsLogger.debug(JobsUnitTest.class, "Finished!");
	}
	
	@Test
	public void testManyJobsOrdering() throws ValidationException, IOException {
		DbObject.getDatabaseProvider().removeTable(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY);
		DbObject.getDatabaseProvider().createTable(WeblibsConst.KEYSPACE, new Job() {
			@Override
			public void run() throws IOException, ValidationException {}
		}.getTableSchema());
		
		final String NORMAL = "NORMAL";
		final String FAST = "FAST";
		
		List<DbObject> toSave = new ArrayList<DbObject>();
		
		final int maxJobs = 300000;
		
		for (int i = 0; i < maxJobs; i++) {
			
			toSave.add(new UTJob(NORMAL, "normal" + i));
			toSave.add(new UTJob(FAST, "fast" + i));
			
			if (i % 100 == 0) {
				DbObject.saveBulk(toSave);
				toSave.clear();
				LibsLogger.debug(JobsUnitTest.class, "Saved " + i + " jobs");
			}
		}
		
		LibsLogger.info(JobsUnitTest.class, "Saved " + maxJobs + " jobs");
		
		Watch w = new Watch();
		
		for (Job job : JobFactory.getOldestJobs(NORMAL, 100, DateUtils.YEAR_2008)) {
			LibsLogger.debug(JobsUnitTest.class, "Oldest jobs from  ", NORMAL, " ", ((UTJob) job).getMsg());
		}
		
		for (Job job : JobFactory.getOldestJobs(FAST, 100, DateUtils.YEAR_2008)) {
			LibsLogger.debug(JobsUnitTest.class, "Oldest jobs from  ", FAST, " ", ((UTJob) job).getMsg());
		}
		
		LibsLogger.info(JobsUnitTest.class, "100 jobs from both channels received in " + w);
		
		runAllJobs(new String[] {NORMAL, FAST});
		
		assertTrue(DbObject.getDatabaseProvider().countRows(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY, null) == 0, "Job table expected to be empty");
		
		LibsLogger.debug(JobsUnitTest.class, "Finished!");
	}
}
