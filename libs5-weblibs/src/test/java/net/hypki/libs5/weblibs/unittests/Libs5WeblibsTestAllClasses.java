package net.hypki.libs5.weblibs.unittests;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.tests.TestCaseSniffer;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

public class Libs5WeblibsTestAllClasses {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		System.setProperty("hazelcast.logging.type", "log4j");
		
		args = (String[]) ArrayUtils.concat(args, new String[] {"--setings", "settingsUnitTests.json"});
						
		DbObject.getDatabaseProvider().clearKeyspace(WeblibsConst.KEYSPACE);
		
		TestCaseSniffer.testClasses(WeblibsTestCase.class, WeblibsTestCase.class, false);
	}

}
