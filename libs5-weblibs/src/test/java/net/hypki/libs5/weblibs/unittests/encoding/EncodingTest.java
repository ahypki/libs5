package net.hypki.libs5.weblibs.unittests.encoding;

import org.junit.Assert;
import org.junit.Test;

public class EncodingTest {
	@Test
	public void testDefaultEncoding() {
		Assert.assertTrue("Default encoding should be UTF-8", java.nio.charset.Charset.defaultCharset().name().equals("UTF-8"));
	}
}
