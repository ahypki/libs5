package net.hypki.libs5.weblibs.unittests.user;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;


public class AllowedUnitTest extends WeblibsTestCase {
	
	@Test
	public void testCreate() throws IOException, ValidationException {
		User user = testUser();
		
		String right = "TEST-RIGHT";
		
		Allowed all = new Allowed();
		all.setRight(right);
		all.setUserUUID(user.getUserId());
		all.setAllowed(true);
		all.save();
		
		addToRemove(all);
		
		Allowed fromDb = Allowed.getAllowed(user.getUserId(), right);
		assertTrue(all.getRight().equals(fromDb.getRight()));
		
		assertTrue(Allowed.isAllowed(user.getUserId(), right));
	}
}
