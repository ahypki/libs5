package net.hypki.libs5.weblibs.unittests.concurrency;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import net.hypki.libs5.weblibs.HazelcastManager;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

import org.junit.Test;

public class BlockingQueueUnitTest extends WeblibsTestCase {

	@Test
	public void testSimpleBlockingQueue2() throws InterruptedException {
		BlockingQueue<String> q = HazelcastManager.getInstance().getQueue("tasks");
		q.put("test-job");
		String task = q.poll(2, TimeUnit.SECONDS);
		assertTrue(task.equals("test-job"));
	}
	
	@Test
	public void testSimpleBlockingQueue() throws InterruptedException {
		BlockingQueue<String> q = HazelcastManager.getInstance().getQueue("tasks");
		q.put("test-job");
		String task = q.take();
		assertTrue(task.equals("test-job"));
	}
}
