package net.hypki.libs5.weblibs.unittests.search;

import java.io.IOException;
import java.util.StringTokenizer;

import jodd.util.StringUtil;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

import org.junit.Test;


public class QueryParsing extends WeblibsTestCase {

	@Test
	public void testQueryParsing() throws IOException, ValidationException, InterruptedException {
		StringTokenizer tokenizer = new StringTokenizer("+jeden dwa -trzy tag: cztery tag:-pięć", "\r\n\t :", true);
		while (tokenizer.hasMoreElements()) {
			String t = tokenizer.nextToken();
			System.out.println("token: " + t);
		}
		
		String [] tmp = StringUtil.split("+jeden dwa -trzy tag:cztery -tag:pięć", " ");
		for (String string : tmp) {
			System.out.println("jodd: " + string);
		}
	}
}
