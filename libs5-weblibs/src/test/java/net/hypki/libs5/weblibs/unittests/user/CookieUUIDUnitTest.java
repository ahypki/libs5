package net.hypki.libs5.weblibs.unittests.user;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.CookieUUID;
import net.hypki.libs5.weblibs.user.CookieUUIDFactory;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;


public class CookieUUIDUnitTest extends WeblibsTestCase {
	@Test
	public void testCreate() throws IOException, ValidationException {
		User user = testUser();
		UUID uuid = new UUID(RandomGUID.getRawRandomGUID());
		
		CookieUUID cookieUUID = new CookieUUID();
		cookieUUID.setUserUUID(user.getUserId());
		cookieUUID.setUuid(uuid);
		cookieUUID.setExpirationDate(System.currentTimeMillis());
		cookieUUID.setClientDomain("127.0.0.1");
		cookieUUID.setLastLogin(System.currentTimeMillis());
		cookieUUID.save();
		
		System.out.println("Login: " + cookieUUID + ", uuid: " + uuid);
		
		addToRemove(cookieUUID);
		
		CookieUUID cookieUUIDFromDB = CookieUUIDFactory.getByUuid(uuid);
		
		assertTrue(cookieUUIDFromDB != null);
		assertTrue(cookieUUID.getUserUUID().equals(cookieUUIDFromDB.getUserUUID()));
		assertTrue(cookieUUID.getUuid().equals(cookieUUIDFromDB.getUuid()));
	}
	
	@Test
	public void testValidationNullDate() throws IOException, ValidationException {
		User user = testUser();
		UUID uuid = new UUID(RandomGUID.getRawRandomGUID());
		
		// create sample CookieUUID 
		CookieUUID c = new CookieUUID();
		c.setUserUUID(user.getUserId());
		c.setUuid(uuid);
		c.setExpirationDate(System.currentTimeMillis());
		c.setClientDomain("127.0.0.1");
		c.setLastLogin(System.currentTimeMillis());
		c.save();
		
		addToRemove(c);
		
		CookieUUID cookieFromDb = CookieUUIDFactory.getByUuid(uuid);
		
		assertTrue(cookieFromDb != null);
		assertTrue(c.getUserUUID().equals(cookieFromDb.getUserUUID()));
	}
	
//	@Test
//	public void testValidationUserIdInvalid() throws DbException {
//		try {
//			long userId = 56;
//			assertTrue("User with id " + userId + " should not exists",
//					HibernateObject.exists(User.class, userId) == false);
//			
//			// create sample CookieUUIDAppId 
//			CookieUUIDAppId c = new CookieUUIDAppId();
//			c.setUserId(userId); // user should exist
//			c.setUserUUID()CookieUUIDAppId.getNewUuid());
//			c.setValidDateInDays(7.0);
//			c.save(null, null);
//			
//			addToRemove(c);
//			
//			assertTrue("Error code 200 expected", false);
//		} catch (ValidationException e) {
//			// do nothing - expected exception
//			assertTrue("Error code 200 expected", e.isErrorOccured(200));
//		} 
//	}
//	
//	@Test
//	public void testValidationUuidInvalidLength() throws DbException {
//		try {
//			long userId = 1;
//			assertTrue("User with id " + userId + " should not exists",
//					HibernateObject.exists(User.class, userId) == true);
//			
//			// create sample CookieUUIDAppId 
//			CookieUUIDAppId c = new CookieUUIDAppId();
//			c.setUserId(userId); // user should exist
//			c.setUserUUID()"ABCD-EFG");
//			c.setValidDateInDays(7.0);
//			c.save(null, null);
//			
//			addToRemove(c);
//			
//			assertTrue("Error code 219 expected", false);
//		} catch (ValidationException e) {
//			// do nothing - expected exception
//			assertTrue("Error code 219 expected", e.isErrorOccured(219));
//		} 
//	}
//	
//	@Test
//	public void testValidationNullUUID() throws ValidationException {
//		try {
//			long userId = 1;
//			assertTrue("User with id " + userId + " should not exists",
//					HibernateObject.exists(User.class, userId) == true);
//			
//			// create sample CookieUUIDAppId 
//			CookieUUIDAppId c = new CookieUUIDAppId();
//			c.setUserId(userId); // user should exist
////		c.setUserUUID()null);
//			c.setValidDateInDays(7.0);
//			c.save(null, null);
//			
//			addToRemove(c);
//			
//			assertTrue("Error DbException was expected", false);
//		} catch (DbException e) {
//			// do nothing - expected exception
//		} 
//	}
//	
//	@Test
//	public void testRemoveFromDbAllUuids() throws Exception {
//		User user = createSampleUnitTestUser();
//		long userId = user.getId();
//		String uuid1 = null;
//		String uuid2 = null;
//		
//		// create sample CookieUUIDAppId 
//		CookieUUIDAppId c = new CookieUUIDAppId();
//		c.setUserId(userId); // user should exist
//		c.setUserUUID()CookieUUIDAppId.getNewUuid());
//		c.setValidDateInDays(7.0);
//		c.setAppId(getUnitTestAppId());
//		c.save(null, null);
//		uuid1 = c.getUuid();
//		assertTrue("CookieUUIDAppId should be valid", 
//				CookieUUIDAppId.isValid(c.getUuid(), getUnitTestAppId(), userId));
//
//		// create another CookieUUIDAppId object (valid for 14 days)  
//		c = new CookieUUIDAppId();
//		c.setUserId(userId); // user should exist
//		c.setUserUUID()CookieUUIDAppId.getNewUuid());
//		c.setValidDateInDays(14.0);
//		c.setAppId(getUnitTestAppId());
//		c.save(null, null);
//		uuid2 = c.getUuid();
//		assertTrue("CookieUUIDAppId should be valid", 
//				CookieUUIDAppId.isValid(c.getUuid(), getUnitTestAppId(), userId));
//		
//		// remove all CookieUUIDAppId for user and then check whether CookieUUIDAppId were really removed
//		CookieUUIDAppId.removeFromDbAllUuids(getUnitTestAppId(), userId);
//		assertTrue("CookieUUIDAppId should be invalid", 
//				CookieUUIDAppId.isValid(uuid1, getUnitTestAppId(), userId) == false);
//		assertTrue("CookieUUIDAppId should be invalid", 
//				CookieUUIDAppId.isValid(uuid2, getUnitTestAppId(), userId) == false);
//	}
//	
//	@Test
//	public void testCookieIsValid() throws Exception {
//		String uuid = null;
//		User user = createSampleUnitTestUser();
//		long userId = user.getId();
//		try {
//			
//			assertTrue("User with id " + userId + " should not exists",
//					HibernateObject.exists(User.class, userId) == true);
//			
//			CookieUUIDAppId c = new CookieUUIDAppId();
//			c.setUserId(userId); // user should exist
//			c.setUserUUID()CookieUUIDAppId.getNewUuid());
//			c.setValidDateInDays(7.0);
//			c.setAppId(getUnitTestAppId());
//			c.save(null, null);
//			
//			uuid = c.getUuid();
//			
//			assertTrue("CookieUUIDAppId should be valid", 
//					CookieUUIDAppId.isValid(uuid, getUnitTestAppId(), userId));
//	
//		} finally {
//			if (uuid != null) {
//				// remove CookieUUIDAppId and then check whether CookieUUIDAppId was really removed
//				CookieUUIDAppId.removeFromDb(getUnitTestAppId(), userId, uuid);
//				assertTrue("CookieUUIDAppId should be invalid", 
//						CookieUUIDAppId.isValid(uuid, getUnitTestAppId(), userId) == false);
//			}
//		}
//	}
//	
//	@Test
//	public void testCookieUUIDAppIdRemove() throws Exception {
//		long userId = 1;
//		
//		assertTrue("User with id " + userId + " should not exists",
//				HibernateObject.exists(User.class, userId) == true);
//		
//		CookieUUIDAppId c = new CookieUUIDAppId();
//		c.setUserId(userId); // user should exist
//		c.setUserUUID()CookieUUIDAppId.getNewUuid());
//		c.setValidDate(new Date());
//		c.setAppId(getUnitTestAppId());
//		c.save(null, null);
//
//		String toRemove = c.getUuid();
//		CookieUUIDAppId.removeFromDb(getUnitTestAppId(), userId, toRemove);
//
//		// check whether CookieUUIDAppId was removed
//		assertTrue(CookieUUIDAppId.isValid(toRemove, getUnitTestAppId(), userId) == false);
//	}
//
//	@Test
//	public void testUserIdAnnotation() throws DbException {
//		try {
//			long userId = 999;
//			
//			assertTrue("User with id " + userId + " should not exists",
//					HibernateObject.exists(User.class, userId) == false);
//			
//			
//			CookieUUIDAppId c = new CookieUUIDAppId();
//			c.setUserId(userId); // user should NOT exist
//			c.setUserUUID()CookieUUIDAppId.getNewUuid());
//			c.setValidDate(new Date());
//			
//			c.save(null, null);
//			
//			// it should be thrown InvalidStateException
//			String toRemove = c.getUuid();
//			CookieUUIDAppId.removeFromDb(getUnitTestAppId(), 1, c.getUuid());
//			assertTrue(CookieUUIDAppId.isValid(toRemove, getUnitTestAppId(), userId));
//			
//			assertTrue("CookieUUIDAppId with unexisted user was saved to DB. InvalidStateException was expected!", false);
//		} catch (ValidationException e) {
//			// do nothing
//		}		
//	}
}
