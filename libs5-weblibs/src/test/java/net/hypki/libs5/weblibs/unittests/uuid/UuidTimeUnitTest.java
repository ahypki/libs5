package net.hypki.libs5.weblibs.unittests.uuid;

import java.util.concurrent.TimeUnit;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.db.uuid.UUIDTime;
import net.hypki.libs5.utils.tests.LibsTestCase;

import org.junit.Test;

public class UuidTimeUnitTest extends LibsTestCase {
	
	@Test
	public void testTimeUnitConversion() {
		TimeUnit ms = TimeUnit.MILLISECONDS;
		TimeUnit sec = TimeUnit.SECONDS;
		
		assertTrue(ms.convert(100, TimeUnit.SECONDS) == 100000);
		assertTrue(sec.convert(100, TimeUnit.SECONDS) == 100);
		assertTrue(sec.convert(1, TimeUnit.MINUTES) == 60);
	}
	
	@Test
	public void testOlderThan() {
		UUIDTime t = new UUIDTime();
		assertTrue(t.isOlderThan(5, TimeUnit.MINUTES) == false);
		
		takeANap(200);
		assertTrue(t.isOlderThan(100, TimeUnit.MILLISECONDS) == true);
		assertTrue(t.isOlderThan(900, TimeUnit.MILLISECONDS) == false);
	}

	@Test
	public void testCreate() {
		UUIDTime t = new UUIDTime();
		assertTrue(t.getTime().getMs() > 0);
		assertTrue(t.getUuid().toString().equals(UUID.ZERO) == false);
	}
}
