package net.hypki.libs5.weblibs.unittests.time;

import jodd.datetime.JDateTime;
import net.hypki.libs5.utils.date.Time;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

import org.junit.Test;

public class TimeUnitTest extends WeblibsTestCase {

	@Test
	public void testSerialization() {
		Time t = new Time();
		String json = JsonUtils.toJson(t).toString();
		Time tFromJson = JsonUtils.fromJson(json, Time.class);
		assertTrue(t.getMs() == tFromJson.getMs());
		
		// JDateTime cannot convert to json, there is no Expose annotation
		JDateTime jdt = new JDateTime(System.currentTimeMillis());
		json = JsonUtils.toJson(jdt).toString();
		JDateTime jdtFromJson = JsonUtils.fromJson(json, JDateTime.class);
		assertTrue(jdt.getTimeInMillis() == jdtFromJson.getTimeInMillis());
		
	}
}
