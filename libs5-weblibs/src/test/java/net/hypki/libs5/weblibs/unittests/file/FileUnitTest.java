package net.hypki.libs5.weblibs.unittests.file;

import java.io.IOException;

import jodd.util.MimeTypes;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.file.BlobFile;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class FileUnitTest extends WeblibsTestCase {

	@Test
	public void testRename() throws IOException, ValidationException {
		User user = testUser();
		BlobFile file = new BlobFile(user.getUserId(), null, "two.pdf", MimeTypes.MIME_APPLICATION_PDF);
		file.save();
		addToRemove(file);
		
		BlobFile fileDb = BlobFile.getFile(user.getUserId(), file.getFileId());
		assertTrue(fileDb != null);
		
		// rename
		fileDb.setFilename("two2.pdf");
		fileDb.save();
		
		fileDb = BlobFile.getFile(user.getUserId(), file.getFileId());
		assertTrue(fileDb == null);
		
		fileDb = BlobFile.getFile(user.getUserId(), file.getFileId());
		assertTrue(fileDb != null);
		assertTrue(fileDb.getFilename().equals("/one/two2.pdf"));
		
		addToRemove(fileDb);
	}
}
