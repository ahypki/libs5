package net.hypki.libs5.weblibs.unittests.jobs;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class UTJob extends Job {
	
	@Expose
	@NotEmpty
	@NotNull
	private String msg = null;
	
	public UTJob() {
		
	}
	
	public UTJob(String channel, String msg) {
		setChannel(channel);
		setMsg(msg);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.debug(UTJob.class, "Running job ", getMsg());
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
