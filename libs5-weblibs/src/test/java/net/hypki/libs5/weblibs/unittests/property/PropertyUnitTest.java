package net.hypki.libs5.weblibs.unittests.property;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.language.LanguageIds;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.property.PropertyFactory;
import net.hypki.libs5.weblibs.unittest.WeblibsTestCase;

import org.junit.Test;


public class PropertyUnitTest extends WeblibsTestCase {
	
	@Test
	public void testProperty() throws IOException, ValidationException {
		Property prop = new Property();
		prop.setLangId(LanguageIds.PL);
		prop.setMsg("Nieprawidłowy katalog");
		prop.setPropKey("FileBrowser.Error.InvalidPath");
		prop.save();
		
		addToRemove(prop);
		
		Property fromDB = PropertyFactory.getProperty(LanguageIds.PL, "FileBrowser.Error.InvalidPath");
		assertTrue(fromDB != null);
	}
	
	@Test
	public void testAddRemoveProperty() throws IOException, ValidationException {
		Property prop = new Property();
		prop.setLangId(LanguageIds.PL);
		prop.setMsg("test-msg");
		prop.setPropKey("test-key");
		prop.save();
		
		// adding object to remove
		addToRemove(prop);
		
		Property fromDB = PropertyFactory.getProperty(LanguageIds.PL, prop.getKey());
		assertTrue(prop.getMsg().equals(fromDB.getMsg()));
		assertTrue(prop.getPropKey().equals(fromDB.getPropKey()));
		assertTrue(prop.getLangId().equals(fromDB.getLangId()));
		
		fromDB = PropertyFactory.getProperty(LanguageIds.PL, "test-key");
		assertTrue(prop.getMsg().equals(fromDB.getMsg()));
		assertTrue(prop.getPropKey().equals(fromDB.getPropKey()));
		assertTrue(prop.getLangId().equals(fromDB.getLangId()));
		
		assertTrue(PropertyFactory.getPropertyValue(LanguageIds.PL, "test-key").equals("test-msg"));
		
//		Property fromDB = Property. initByKey(Property.class, prop.getKeyspace(), prop.getColumnFamily(), prop.getKey(), prop.getSuperColumn(), prop.getColumn());
//		
//		assertTrue(prop.getMsg().equals(fromDB.getMsg()));
//		assertTrue(prop.getPropKey().equals(fromDB.getPropKey()));
//		assertTrue(prop.getLangId().equals(fromDB.getLangId()));
	}
}
