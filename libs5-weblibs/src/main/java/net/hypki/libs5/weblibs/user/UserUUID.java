package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

/**
 * Cassandra: Keyspace.User[{userUuid}]."uuidmapping"
 * @author ahypki
 *
 */
public class UserUUID extends WeblibsObject {
	public static final String COLUMN_FAMILY = "useruuid";
		
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userUUID = null;
	
	@AssertValid
	@Expose
	private Email email = null;
	
		
	public UserUUID() {
		
	}
	
	public UserUUID(User user) {
		setEmail(user.getEmail());
		setUserUUID(user.getUserId());
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getUserUUID().toString();
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Email getEmail() {
		return email;
	}

	public static UserUUID getUserUUID(UUID userId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, userId.getId())), 
				DbObject.COLUMN_DATA, 
				UserUUID.class);
//		return initByKey(UserUUID.class, true, WeblibsConst.KEYSPACE, COLUMN_FAMILY, appId.getAppId(), userId.getId().getBytes(), COLUMN);
//		return get(UserUUID.class, WeblibsConst.KEYSPACE, COLUMN_FAMILY, userId.toString(), DbObject.COLUMN_PK, DbObject.COLUMN_DATA);
	}

	public static Email getUserEmail(UUID userId) throws IOException {
		UserUUID userUUID = getUserUUID(userId);
		return (userUUID != null ? userUUID.getEmail() : null);
	}

	public static void remove(UUID userId) {
		removeMutation(WeblibsConst.KEYSPACE, COLUMN_FAMILY, userId.getId(), DbObject.COLUMN_DATA);
	}
	
	public static Mutation removeMutation(UUID userId) {
		return removeMutation(WeblibsConst.KEYSPACE, COLUMN_FAMILY, userId.getId(), DbObject.COLUMN_DATA);
	}

	public void setUserUUID(UUID userUUID) {
		this.userUUID = userUUID;
	}

	public UUID getUserUUID() {
		return userUUID;
	}
}
