package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;

import com.google.gson.annotations.Expose;

/**
 * Cassandra: Keyspace.User[{userUuid}]."confirm"
 * @author ahypki
 *
 */
public class UserConfirmationUUID extends WeblibsObject {
	public static final String COLUMN_FAMILY = "userconfuuid";
		
	@AssertValid
	@Expose
	private UUID confirmUuid = null;
	
	@AssertValid
	@Expose
	private UUID userUuid = null;
	
	@Expose
	private Boolean clicked = null;
		
	public UserConfirmationUUID() {
		
	}
	
	public UserConfirmationUUID(User user) {
		setConfirmUuid(UUID.random());
		setUserUUID(user.getUserId());
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getUserUUID().toString();
	}

	public void setConfirmUuid(UUID confirmUuid) {
		this.confirmUuid = confirmUuid;
	}

	public UUID getConfirmUuid() {
		return confirmUuid;
	}
	
	public void setUserUUID(UUID userUuid) {
		this.userUuid = userUuid;
	}

	public UUID getUserUUID() {
		return userUuid;
	}

	public static UserConfirmationUUID getUserConfirmationUUID(UUID userId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, userId.getId())), 
				DbObject.COLUMN_DATA, 
				UserConfirmationUUID.class);
	}

	public static void removeUserConfirmationUUID(UUID userId) {
		removeMutation(WeblibsConst.KEYSPACE, COLUMN_FAMILY, userId.getId(), DbObject.COLUMN_DATA);
	}
	
	public static Mutation removeMutation(UUID userId) {
		return removeMutation(WeblibsConst.KEYSPACE, COLUMN_FAMILY, userId.getId(), DbObject.COLUMN_DATA);
	}

	public Boolean getClicked() {
		return clicked;
	}

	public void setClicked(Boolean clicked) {
		this.clicked = clicked;
	}
}
