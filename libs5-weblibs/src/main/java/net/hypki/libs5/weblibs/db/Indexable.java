package net.hypki.libs5.weblibs.db;

import java.io.IOException;

public interface Indexable {
	public void index() throws IOException;
	public void deindex() throws IOException;
}
