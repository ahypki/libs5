package net.hypki.libs5.weblibs.language;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;

public class LangValidator extends AbstractAnnotationCheck<LangValidatorAnnotation> {

	@Override
	public boolean isSatisfied(Object validatedObject, Object valueToValidate, OValContext context, Validator validator) throws OValException {
		if (valueToValidate instanceof LanguageIds)
			return true;
		else
			return false;
	}

}
