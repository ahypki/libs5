package net.hypki.libs5.weblibs.user;


@Deprecated
public class CookieUUIDAppId extends CookieUUID {

//    private static Cache uuidCache = CacheUtils.getCache(WebLibsConst.CACHE_UUID_VALIDATION);
//    
//    private long appId = 0;
//    
//	public CookieUUIDAppId() {
//		
//	}
//
//	public void setAppId(long appId) {
//		this.appId = appId;
//	}
//
//	public long getAppId() {
//		return appId;
//	}
//	
//    public static boolean removeFromDb(long appId, long userId, String uuid) {
//        HashMap<String, Object> args = new HashMap<String, Object>();
//        args.put("userId", userId);
//        args.put("appId", appId);
//        args.put("uuid", uuid);
//
////        if (HibernateFactory.executeQuery(null, null, 
////        		"delete from CookieUUID " +
////        		"where userId=:userId and uuid=:uuid and appId=:appId", args) == 1) {
////        	uuidCache.remove(uuid);
////            return true;
////        }
//        
//        return false;
//    }
//    
//    public static boolean removeFromDbAllUuids(long appId, long userId) {
//    	try {
//			ArrayList<String> list = (ArrayList<String>) HibernateFactory.getList(
//					"SELECT uuid FROM CookieUUID " +
//					"WHERE userId=:userId AND appId=:appId", 
//					"userId", userId, "appId", appId);
//			
//			for (String uuid : list)
//				// after some time old uuids will be removed from cache, but for now
//				// setting all uuid as invalid
//				uuidCache.remove(uuid);
//		} finally {			
////			HibernateFactory.executeQuery(null, null, 
////					"delete from CookieUUID where userId=:userId AND appId=:appId", 
////					"userId", userId, "appId", appId);
//		}
//        return true;
//    }
//    
//    /**
//     * Returns true if uuid is valid for specified application and user ids
//     * 
//     * @param uuid
//     * @return User ID is returned or 0 if uuid is invalid
//     */
//    public static boolean isValid(String uuid, long appId, long userId) {
//    	// search first in cache
//    	Element element = uuidCache.get(uuid);
//    	if (element != null) {
//    		return element.getObjectValue().toString().equals(String.format("%d_%d", appId, userId));
//    	} else {
//    		// search in database 	
//    		Object o = HibernateFactory.uniqueResult("SELECT userId FROM CookieUUID " +
//	        		"where uuid=:uuid AND appId=:appId AND userId=:userId AND current_date() < validDate", 
//	        		"uuid", uuid, "appId", appId, "userId", userId);
//	        
//    		if (o != null) {
//    			long id = Long.parseLong(o.toString());
//    				
//    			if (id == userId) {
//	    			// given uuid is valid, add to cache
//		        	uuidCache.put(new Element(uuid, String.format("%d_%d", appId, userId)));
//	    			
//		        	return true;
//    			}
//    		}
//    		
//    		return false;
//    	}
//    }
//    
//    public static long getUserId(String uuid, long appId) {
//    	// search first in cache
//    	Element element = uuidCache.get(uuid);
//    	if (element != null) {
//    		String temp = element.getObjectValue().toString();
//    		return Long.parseLong(temp.substring(temp.lastIndexOf('_') + 1));
//    	} else {
//    		// search in database 	
//    		Object o = HibernateFactory.uniqueResult("SELECT userId FROM CookieUUID " +
//	        		"where uuid=:uuid AND appId=:appId AND current_date() < validDate", 
//	        		"uuid", uuid, "appId", appId);
//	        
//    		if (o != null) {
//    			return Long.parseLong(o.toString());
//    		} else
//    			return 0;
//    	}
//    }
}
