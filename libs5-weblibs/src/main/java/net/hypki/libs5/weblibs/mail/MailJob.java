package net.hypki.libs5.weblibs.mail;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.Email;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class MailJob extends Job {
	
	@NotNull
	@AssertValid
	@Expose
	private Email to = null;
	
//	@NotNull
	@AssertValid
	@Expose
	private Email cc = null;
	
//	@NotNull
	@AssertValid
	@Expose
	private Email bcc = null;
	
	@NotNull
	@Expose
	private String title = null;
	
	@NotNull
	@Expose
	private String body = null;
	
	@NotNull
	@Expose
	private boolean html = false;
	
	@NotNull
	@Expose
	private MailCredentials mailCredentials = null;
	
	@NotNull
	@Expose
	private int retries = 0;

	public MailJob() {
		
	}
	
	public MailJob(String to, String title, String body) {
		setTo(to);
		setTitle(title);
		setBody(body);
	}
	
	public MailJob(Email to, String title, String body) {
		setTo(to);
		setTitle(title);
		setBody(body);
	}
	
	public MailJob(Email to, Email cc, Email bcc, String title, String body) {
		setTo(to);
		setTitle(title);
		setBody(body);
		setCc(cc);
		setBcc(bcc);
	}
	
	public MailJob(Email to, String title, String body, boolean isHtml) {
		setTo(to);
		setTitle(title);
		setBody(body);
		setHtml(isHtml);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			Mailer.sendMail(getMailCredentials(),
					getTo().getEmail(), 
					getCc() != null ? getCc().getEmail() : null, 
					getBcc() != null ? getBcc().getEmail() : null, 
					getTitle(), 
					getBody(), 
					isHtml(), 
					null);
		} catch (Exception e) {
			Throwable nested = e.getCause();
			for (int i = 0; i < 10; i++) {
				if (nested instanceof com.sun.mail.smtp.SMTPAddressFailedException) {
					// consuming job
					LibsLogger.error(MailJob.class, "Invalid recipient " + getTo().getEmail() + ". Consuming Job...", e);
					return;
				}
				
				nested = nested != null ? nested.getCause() : null;
				if (nested == null)
					break;
			}
			
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			if (errors.toString().contains("550 Invalid recipient")) {
				// consuming job
				LibsLogger.error(MailJob.class, "Invalid recipient " + getTo().getEmail() + ". Consuming Job...", e);
				return;
			}
			
			if (getRetries() < 5) {
				setRetries(getRetries() + 1);
				setPostponeMs(DateUtils.ONE_MINUTE_MS);
			} else {
				LibsLogger.error(MailJob.class, "Cannot send message '" + getTitle() + "' to " 
						+ getTo().toString() + ". Tried " + getRetries() + " times. Consuming job.", e);
			}
		}
	}

	public MailJob setTo(Email to) {
		this.to = to;
		return this;
	}
	
	public void setTo(String to) {
		this.to = new Email(to);
	}

	public Email getTo() {
		return to;
	}

	public MailJob setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public MailJob setBody(String body) {
		this.body = body;
		return this;
	}

	public String getBody() {
		return body;
	}

	public boolean isHtml() {
		return html;
	}

	public void setHtml(boolean html) {
		this.html = html;
	}

	public Email getCc() {
		return cc;
	}

	public void setCc(Email cc) {
		this.cc = cc;
	}

	public Email getBcc() {
		return bcc;
	}

	public void setBcc(Email bcc) {
		this.bcc = bcc;
	}

	public MailCredentials getMailCredentials() {
		return mailCredentials;
	}

	public MailJob setMailCredentials(MailCredentials mailCredentials) {
		this.mailCredentials = mailCredentials;
		return this;
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}
}
