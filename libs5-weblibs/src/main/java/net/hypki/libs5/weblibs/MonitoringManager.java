package net.hypki.libs5.weblibs;


public class MonitoringManager {
	
	public static final String CHANNEL_JOB_WORKER_ALIVE 	= "CHANNEL_JOB_WORKER_ALIVE";
	public static final String CHANNEL_JOB_PRODUCER_ALIVE 	= "CHANNEL_JOB_PRODUCER_ALIVE";
	
//	private static MonitoringProvider monitoringProvider = null;

	private MonitoringManager() {
		
	}
	
//	public static MonitoringProvider monitoringInstance() {
//		if (monitoringProvider == null) {
//			try {
//				monitoringProvider = (MonitoringProvider) Class.forName(JsonUtils.readString(Settings.getSettings(), "MonitoringProvider/class")).newInstance();
//			} catch (InstantiationException e) {
//				throw new RuntimeException("Cannot create instance of MonitoringProvider", e);
//			} catch (IllegalAccessException e) {
//				throw new RuntimeException("Cannot create instance of MonitoringProvider", e);
//			} catch (ClassNotFoundException e) {
//				throw new RuntimeException("Cannot create instance of MonitoringProvider", e);
//			}
//		}
//		return monitoringProvider;
//	}
}
