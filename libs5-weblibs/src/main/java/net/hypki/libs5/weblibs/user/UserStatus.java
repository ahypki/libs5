package net.hypki.libs5.weblibs.user;

public enum UserStatus {
	/**
	 * Normal, registered, unblocked for any reason user
	 */
	NORMAL,
	
	/**
	 * Registered, but NOT confirmed user. Email for confirmation is not send yet.
	 */
	NEW,
	
	/**
	 * Registered, but NOT confirmed user. Email for confirmation sent, waiting for user to click on the confirmation link.
	 */
	UNCONFIRMED,
	
	/**
	 * Blocked user, cannot login or do anything
	 */
	BLOCKED,
	
	/**
	 * Removed user, cannot login or do anything
	 */
	REMOVED
}
