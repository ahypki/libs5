package net.hypki.libs5.weblibs.search;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class RemoveFromIndex extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private String objectCombinedKey = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String objectColumnFamily = null;
	
	public RemoveFromIndex() {
		
	}
	
	public RemoveFromIndex(String combinedKey, String cf) {
		setObjectCombinedKey(combinedKey);
		setObjectColumnFamily(cf);
	}
	
	public RemoveFromIndex(WeblibsObject o) {
		setObjectCombinedKey(o.getCombinedKey());
		setObjectColumnFamily(o.getColumnFamily());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		SearchManager.removeObject(getObjectCombinedKey(), getObjectColumnFamily());
		LibsLogger.debug(RemoveFromIndex.class, String.format("Removed %s from index %s", getObjectCombinedKey(), getObjectColumnFamily()));
	}

	public String getObjectCombinedKey() {
		return objectCombinedKey;
	}

	public void setObjectCombinedKey(String combKey) {
		this.objectCombinedKey = combKey;
	}

	public String getObjectColumnFamily() {
		return objectColumnFamily;
	}

	public void setObjectColumnFamily(String cf) {
		this.objectColumnFamily = cf;
	}

}
