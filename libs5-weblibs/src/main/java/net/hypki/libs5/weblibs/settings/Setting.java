package net.hypki.libs5.weblibs.settings;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.db.WeblibsObjectIndexable;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

public class Setting extends WeblibsObjectIndexable<Setting> implements Indexable {

	public final static String COLUMN_FAMILY = "setting";
	
	@Expose
	@NotNull
//	@AssertValid
	private String id = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String name = null;
	
	@Expose
	private String description = null;
	
	@Expose
	@AssertValid
	private List<SettingValue> values = null;
	
	@Expose
	private SimpleDate lastEdit = null;
	
	@Expose
	private boolean system = true;
	
	public Setting() {
		setId(UUID.random().getId());
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		if (!isSystem())
			assertTrue(getUserId() != null, "For non-system settings it is expected to define userId");
	}
	
	@Override
	public MutationList getSaveMutations() {
		setLastEdit(SimpleDate.now());
		
		return super.getSaveMutations();
	}
	
	@Override
	public Setting save() throws ValidationException, IOException {
		super.save();
		
		SettingFactory.updateCache(this);
		
		return this;
	}

	@Override
	public void index() throws IOException {
		JsonObject je = (JsonObject) JsonUtils.toJson(this);
		if (je.get("values") != null)
			for (JsonElement jo : je.get("values").getAsJsonArray()) {
				if (jo.getAsJsonObject().get("vall") != null)
//					jo.getAsJsonObject().remove("vall");
					jo.getAsJsonObject().addProperty("vall", jo.getAsJsonObject().get("vall").getAsString());
			}
		SearchManager.index(this, je.toString());
	}

	@Override
	public void deindex() throws IOException {
		SearchManager.removeObject(getCombinedKey(), getColumnFamily());
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getKey() {
		if (getUserId() == null)
			return getId();
		else
			return getId() + "_" + getUserId();
	}

	public String getId() {
		return id;
	}

	public Setting setId(String id) {
		this.id = id;
		return this;
	}

	public UUID getUserId() {
		return userId;
	}

	public Setting setUserId(UUID userId) {
		this.userId = userId;
		if (userId != null)
			setSystem(false);
		return this;
	}

	public String getName() {
		return name;
	}

	public Setting setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Setting setDescription(String description) {
		this.description = description;
		return this;
	}

	public List<SettingValue> getValues() {
		if (values == null)
			values = new ArrayList<SettingValue>();
		return values;
	}

	private void setValues(List<SettingValue> values) {
		this.values = values;
	}
	
	public SettingValue getValue(String name) {
		for (SettingValue val : getValues()) {
			if (val.getName().equals(name))
				return val;
		}
		return null;
	}
	
	public Setting addValue(SettingValue val) {
		getValues().add(val);
		return this;
	}
	
	public Setting removeValue(String name) {
		int toRemove = -1;
		for (int i = 0; i < getValues().size(); i++) {
			if (getValues().get(i).getName().equals(name)) {
				toRemove = i;
				break;
			}
		}
		if (toRemove >= 0)
			getValues().remove(toRemove);
		return this;
	}
	
	public Setting clearValues() {
		getValues().clear();
		return this;
	}

	public SimpleDate getLastEdit() {
		return lastEdit;
	}

	public Setting setLastEdit(SimpleDate lastEdit) {
		this.lastEdit = lastEdit;
		return this;
	}
	
	public static void main(String[] args) {
		Setting s = new Setting()
			.setId("ID")
			.setName("Name")
			.setDescription("Description")
			.setLastEdit(SimpleDate.now())
			.setUserId(UUID.random())
			.addValue(new SettingValue("val1", "v").setDescription("desc1"))
			.addValue(new SettingValue("val2", true).setDescription("desc2"))
			;
		System.out.println(JsonUtils.objectToStringPretty(s));
	}

	public boolean isSystem() {
		return system;
	}

	public Setting setSystem(boolean system) {
		this.system = system;
		return this;
	}

	public void setValue(String name, Object value) {
		setValue(name, value, null);
	}
	
	public void setValue(String name, Object value, SettingType valueType) {
		SettingValue settValue = getValue(name);
		
		if (settValue == null) {
			settValue = new SettingValue();
			settValue.setName(name);
			addValue(settValue);
		}
		
		settValue.setValue(value);
		
		if (valueType == null)
			settValue.determineType();
		else
			settValue.setType(valueType);
	}
}
