package net.hypki.libs5.weblibs.file;

import static net.hypki.libs5.db.db.weblibs.MutationType.INSERT;
import static net.hypki.libs5.weblibs.WeblibsConst.KEYSPACE;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import jodd.util.MimeTypes;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.weblibs.StorageManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.user.User;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;
import com.mchange.lang.ByteUtils;

/**
 * Cassandra: Keyspace.Files[{userId}].null.{path}
 * @author ahypki
 *
 */
public class BlobFile extends WeblibsObject<BlobFile> implements Serializable {
	private static final long serialVersionUID = 5804909329701234648L;
//	private static final byte[] MIN = new byte[] {0};
//	private static final byte[] MAX = new byte[] {127};
	
	public static final String PDF_MIME_TYPE = MimeTypes.MIME_APPLICATION_PDF;
	public static final String BINARY_MIME_TYPE = MimeTypes.MIME_APPLICATION_OCTET_STREAM;
	
	public static final String COLUMN_FAMILY = "Files";
		
	@NotNull(message = "user id cannot be null")
	@AssertValid
	@Expose
	private UUID userId = null;
	
	@NotNull(message = "path cannot be null")
	@AssertValid
	@Expose
	private UUID folderId = null;
	
	@NotNull
	@AssertValid
	@Expose
	private UUID fileId = null;

	@NotNull
	@NotEmpty
	@Length(min = 1, max = 256)
	@Expose
	private String mimeType = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String filename = null;
	
//	private byte[] dataToSave = null;
	
	private InputStream dataStreamToSave = null;
	
	private boolean readFailed = false;
	
	public BlobFile() {
		setFileId(UUID.random());
	}
	
	public BlobFile(UUID userId, UUID folderId, String filename, String mimetype) {
		setUserId(userId);
		setFolderId(folderId);
		setFileId(UUID.random());
		setFilename(filename);
		setMimeType(mimetype);
	}

	public BlobFile(UUID userId, UUID folderId, String mimetype, InputStream dataStream) {
		setUserId(userId);
		setFolderId(folderId);
		setFileId(UUID.random());
		setFilename(filename);
		setMimeType(mimetype);
		setDataStream(dataStream);
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public String getKey() {
		return getUserUUID().toString();
	}

	@Override
	public String getColumn() {
		return getFileId().toString();
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addMutation(new Mutation(INSERT, KEYSPACE, COLUMN_FAMILY, getUserUUID().toString() + "_" + getFolderIdNotNull(), 
						DbObject.COLUMN_PK, getFileId().toString(), getData()));
	}
			
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addInsertMutations(new RemoveFileJob(this));
	}
	
	@Override
	public BlobFile save() throws ValidationException, IOException {
		// saving data to storage system
		saveFileToStorage();
				
		return super.save();
	}
		
	public void setUser(User user) {
		this.userId = user != null ? user.getUserId() : null;
	}
	
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getUserUUID() {
		return userId;
	}

	public static BlobFile getFile(UUID userId, UUID fileId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, fileId.getId())), 
				DbObject.COLUMN_DATA, 
				BlobFile.class);
	}
	
	public void saveBlobToFile(java.io.File file) throws IOException {
		FileUtils.saveToFile(getBlobData(), file);
	}

	public static List<BlobFile> getFiles(UUID userId, UUID folderId) throws IOException {
		List<BlobFile> results = new ArrayList<>();
		getList(BlobFile.class, WeblibsConst.KEYSPACE, COLUMN_FAMILY, folderId.getId(), null, WeblibsConst.FILE_BROWSER_MAX_SUBELEMENTS_PER_FOLDER, null, results);
		return results;
//		return (List<BlobFile>) DbObject.getDatabaseProvider().getList(BlobFile.class, WeblibsConst.KEYSPACE, COLUMN_FAMILY, 
//				userId.toString() + "_" + (folderId != null ? folderId.toString() : ""),
////				MIN, MAX,
//				"", 
//				null,
//				false, WeblibsConst.FILE_BROWSER_MAX_SUBELEMENTS_PER_FOLDER);
	}

	public static boolean isFilePresent(UUID userId, UUID fileId) throws IOException {
		return DbObject.getDatabaseProvider().exist(WeblibsConst.KEYSPACE, COLUMN_FAMILY, 
				new C3Where().addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, userId.getId())), 
				fileId.getId());
	}
	
	public UUID getFileId() {
		return fileId;
	}

	public void setFileId(UUID fileId) {
		this.fileId = fileId;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public UUID getFolderId() {
		return folderId;
	}
	
	private String getFolderIdNotNull() {
		return folderId != null ? folderId.getId() : "";
	}

	public void setFolderId(UUID folderId) {
		this.folderId = folderId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public InputStream getDataStream() {
		return dataStreamToSave;
	}

	public void setDataStream(InputStream dataStream) {
		this.dataStreamToSave = dataStream;
	}

	public void saveFileToStorage() throws IOException {
		if (getDataStream() != null) {
			StorageManager.save(null, getFileId(), getDataStream());
			getDataStream().close();
			setDataStream(null);
			LibsLogger.debug(BlobFile.class, "File " + getStorageWeblibsPath().toString() + " saved to storage");
		} else if (getData() != null) {
			StorageManager.save(null, getFileId(), getBlobData());
			this.dataStreamToSave = null;
			LibsLogger.debug(BlobFile.class, "File " + getStorageWeblibsPath().toString() + " saved to storage");
		}
	}
	
	public void setBlobData(java.io.File file) {
		try {
			setBlobData(FileUtils.getBytesFromFile(file));
			setMimeType(new MimetypesFileTypeMap().getContentType(file));
		} catch (IOException e) {
			LibsLogger.error(BlobFile.class, "Cannot read file content", e);
		}
	}
	
	public void setBlobData(byte[] data) {
		this.dataStreamToSave = net.hypki.libs5.utils.string.ByteUtils.toStream(data);
		setMimeType(MimeTypes.MIME_APPLICATION_OCTET_STREAM);
	}
	
	public InputStream getBlobData() {
		if (readFailed == false) {
			try {
				this.dataStreamToSave = StorageManager.read(null, getFileId());
			} catch (IOException e) {
				LibsLogger.error(getClass(), "Cannot read blob data", e);
				readFailed = true;
			}
		}
		return this.dataStreamToSave;
	}

	public void removeFileFromStorage() {
		try {
			StorageManager.remove(null, getFileId());
		} catch (IOException e) {
			LibsLogger.error(getClass(), "Cannot read blob data", e);
			readFailed = true;
		}
	}

	private WeblibsPath getStorageWeblibsPath() {
		return new WeblibsPath(null, getFileId().getId());
	}
}
