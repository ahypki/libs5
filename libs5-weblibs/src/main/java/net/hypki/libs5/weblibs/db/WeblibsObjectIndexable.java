package net.hypki.libs5.weblibs.db;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.search.DeindexJob;
import net.hypki.libs5.weblibs.search.IndexJob;

public abstract class WeblibsObjectIndexable<T> extends WeblibsObject<T> implements Indexable {

	public WeblibsObjectIndexable() {
		
	}

	@Override
	public MutationList getSaveMutations() {
		MutationList tmp = super.getSaveMutations();
		
		if (this instanceof Indexable)
			tmp.addInsertMutations(new IndexJob(this));
		
		return tmp;
	}
	
	@Override
	public MutationList getRemoveMutations() {
		MutationList tmp = super.getRemoveMutations();
		
		if (this instanceof Indexable)
			tmp.addInsertMutations(new DeindexJob(this));
		
		return tmp;
	}
	
	@Override
	public T save() throws ValidationException, IOException {
		T tmp = super.save();
		
		if (this instanceof Indexable) {
			try {
				((Indexable) this).index();
			} catch (Exception e) {
				LibsLogger.error(getClass(), "Cannot index ad-hoc ", getClass().getSimpleName(), " in save() method", e);
			}
		}
		
		return tmp;
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
		
		if (this instanceof Indexable) {
			try {
				SearchManager.removeObject(getCombinedKey(), getColumnFamily());
			} catch (Exception e) {
				LibsLogger.error(getClass(), "Cannot remove from index ad-hoc ", getClass().getSimpleName(), 
						" in remove() method", e);
			}
		}
	}
	
	public static <T extends WeblibsObjectIndexable> T getWeblibsObjectIndexable(UUID objectId, String objectClass) throws IOException {
		try {
			Class<T> clazz = (Class<T>) Class.forName(objectClass);
			
			return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE,
					ReflectionUtility.getStaticFieldValue(clazz, "COLUMN_FAMILY"), 
					new C3Where()
							.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, objectId.getId())), 
					DbObject.COLUMN_DATA, 
					clazz);
		} catch (ClassNotFoundException e) {
			throw new IOException("Cannot get BeansObject from DB", e);
		}
	}
}
