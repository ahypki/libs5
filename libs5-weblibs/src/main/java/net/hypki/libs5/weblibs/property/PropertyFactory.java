package net.hypki.libs5.weblibs.property;

import static net.hypki.libs5.weblibs.CacheManager.cacheInstance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.language.LanguageIds;

public class PropertyFactory {

	public static Property getProperty(LanguageIds languageId, String propKey) throws IOException {
		return PropertyFactory.getPropertyCached(null, languageId, propKey);
	}

	public static Property getProperty(UUID userId, String propKey) throws IOException {
		return PropertyFactory.getProperty(userId, LanguageIds.__, propKey);
	}

	public static Property getProperty(String propKey) throws IOException {
		return PropertyFactory.getProperty(null, LanguageIds.__, propKey);
	}
	
	public static Property getPropertyNoCache(String propKey) throws IOException {
		return PropertyFactory.getPropertyNoCache(null, LanguageIds.__, propKey);
	}

	public static String getPropertyValue(String propKey) {
		return PropertyFactory.getPropertyValue(LanguageIds.__, propKey);
	}
	
	public static int getPropertyValueAsInt(String propKey, int defaultValue) {		
		String tmp = PropertyFactory.getPropertyValue(LanguageIds.__, propKey);
		return tmp != null ? NumberUtils.toInt(tmp) : defaultValue;
	}
	
	public static int getPropertyValueAsInt(String propKey) {
		return getPropertyValueAsInt(propKey, 0);
	}

	public static String getPropertyValue(LanguageIds languageId, String propKey) {
		Property prop = PropertyFactory.getPropertyCached(null, languageId, propKey);
		return prop != null ? prop.getMsg() : null;
	}

	public static String getPropertyValue(LanguageIds languageId, String propKey, String defaultValue) {
		Property prop = PropertyFactory.getPropertyCached(null, languageId, propKey);
		return prop != null ? prop.getMsg() : defaultValue;
	}

	public static void saveProperty(String propKey, String value) throws ValidationException, IOException {
			Property prop = new Property();
			prop.setLangId(LanguageIds.__);
			prop.setMsg(value);
			prop.setPropKey(propKey);
	//		prop.setSystem(true);
			prop.save();
			cacheInstance().put(Property.class.getSimpleName(), Property.buildKey(null, LanguageIds.__, propKey), prop);
		}

	public static void saveProperty(UUID userId, String propKey, String value) throws ValidationException, IOException {
		Property prop = new Property();
		prop.setLangId(LanguageIds.__);
		prop.setMsg(value);
		prop.setPropKey(propKey);
		prop.setUserId(userId);
//		prop.setSystem(userId != null ? true : false);
		prop.save();//DbObject.get(Property.class, WeblibsConst.KEYSPACE, COLUMN_FAMILY, buildKey(prop.isSystem() ? null : userId, LanguageIds.__, propKey), DbObject.COLUMN_DATA)
		cacheInstance().put(Property.class.getSimpleName(), Property.buildKey(userId, LanguageIds.__, propKey), prop);
	}
	
	public static void updateCache(Property prop) {
		cacheInstance().put(Property.class.getSimpleName(), prop.getKey(), prop);
	}

	/**
	 * If property is system then uuidId is null
	 * 
	 * @param userId
	 * @param propKey
	 * @param column
	 * @return
	 */
	static Property getPropertyCached(UUID userId, LanguageIds lang, String propKey) {
		final String key = Property.buildKey(userId, lang, propKey);
		Property prop = cacheInstance().get(Property.class, Property.class.getSimpleName(), key);
		if (prop != null)
			return prop;
		else {
			try {
				prop = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, Property.COLUMN_FAMILY, new C3Where().addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, key)), DbObject.COLUMN_DATA, Property.class);
				
				cacheInstance().put(Property.class.getSimpleName(), key, prop);
			} catch (IOException e) {
				LibsLogger.error(Property.class, "Cannot check for property ", key, e);
			}
			return prop;
		}
	}
	
	/**
	 * If property is system then uuidId is null
	 * 
	 * @param userId
	 * @param propKey
	 * @param column
	 * @return
	 */
	static Property getPropertyNoCache(UUID userId, LanguageIds lang, String propKey) {
		final String key = Property.buildKey(userId, lang, propKey);
		
		Property prop = null;
		
		try {
			prop = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
					Property.COLUMN_FAMILY, 
					new C3Where().addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, key)), 
					DbObject.COLUMN_DATA, 
					Property.class);
		} catch (IOException e) {
			LibsLogger.error(Property.class, "Cannot check for property ", key, e);
		}
				
		return prop;
	}

	public static Property getProperty(UUID userId, LanguageIds languageId, String propKey) throws IOException {
		return getPropertyCached(userId, languageId, propKey);
	}

	public static Iterable<Property> iterateProperty(String filter) {
		final String filterLowerCase = filter != null ? filter.toLowerCase() : filter;
		return new Iterable<Property>() {
			@Override
			public Iterator<Property> iterator() {
				return new Iterator<Property>() {
					Iterator<Row> rowIter = DbObject.getDatabaseProvider()
							.tableIterable(WeblibsConst.KEYSPACE_LOWERCASE, Property.COLUMN_FAMILY.toLowerCase())
							.iterator();
					Property nextProp = null;
					
					@Override
					public Property next() {
						return nextProp;
					}
					
					@Override
					public boolean hasNext() {
						while (rowIter.hasNext()) {
							Row row = rowIter.next();
							Property prop = row.getAsObject(DbObject.COLUMN_DATA, Property.class);
							
							if (filterLowerCase == null 
									|| prop.getKey().toLowerCase().contains(filterLowerCase)
									|| (prop.getUserId() != null 
											&& prop.getUserId().getId().toLowerCase().contains(filterLowerCase))) {
								nextProp = prop;
								return true;
							}
						}
						return false;
					}
				};
			}
		};
	}

	public static Iterable<Property> iterateProperty() {
		return iterateProperty(null);
	}

	public static long getAllCount(String filter) {
		int counter = 0;
		for (Property prop : iterateProperty(filter)) {
			counter++;
		}
		return counter;
	}

	@Deprecated
	// TODO use search engine
	public static SearchResults<Property> search(final String filter, final int from, final int size) {
		List<Property> props = new ArrayList<Property>();
		int i = 0;
		for (Property prop : iterateProperty(filter)) {
			if (i >= from && props.size() < size)
				props.add(prop);
			i++;
		}
		return new SearchResults<Property>(props, i);
	}

	public static long getAllCount() {
		try {
			return DbObject.getDatabaseProvider().countRows(WeblibsConst.KEYSPACE_LOWERCASE, Property.COLUMN_FAMILY.toLowerCase(), null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

}
