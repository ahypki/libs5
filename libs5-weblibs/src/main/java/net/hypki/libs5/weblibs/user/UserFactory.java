package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class UserFactory {
	
	public static User getUserFromCache(UUID userId) {
		return CacheManager.cacheInstance().get(User.class, User.class.getSimpleName(), userId.getId());
	}
	
	public static void updateCache(User user) {
		CacheManager
			.cacheInstance()
			.put(User.class.getSimpleName(), user.getUserId().getId(), user);
	}

	public static SearchResults<User> searchUsers(String keywords, int from, int size) throws IOException {
		Query q = Query.parseQuery(keywords);
		Query esQuery = new Query();
		
		for (Term term : q.getTerms()) {
			if (term.getField() == null) {
				if (term instanceof WildcardTerm) {
					WildcardTerm wild = (WildcardTerm) term;
					esQuery.addTerm(new Bracket(TermRequirement.MUST)
										.addTerm(new WildcardTerm("login", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm("email.email", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm("id.id", wild.getTerm(), TermRequirement.SHOULD))
//										.addTerm(new WildcardTerm("taskId.id", wild.getTerm(), TermRequirement.SHOULD))
//										.addTerm(new WildcardTerm("description", wild.getTerm(), TermRequirement.SHOULD))
							);
				}
			} else {
				term.setField("meta.meta." + term.getField() + ".value");
				esQuery.addTerm(term);
			}
		}
		
		if (esQuery.getTerms().size() == 0)
			esQuery.addTerm(new WildcardTerm("email.email", "*", TermRequirement.SHOULD));
		
		esQuery.setSortBy("creationDate");
	
		return SearchManager.searchInstance().search(User.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				User.COLUMN_FAMILY.toLowerCase(), 
				esQuery, 
				from, 
				size);
	}
	
	public static Iterable<User> iterateUsers() {
		return DbObject
				.iterateTable(User.class, 
						WeblibsConst.KEYSPACE, 
						User.COLUMN_FAMILY, 
						DbObject.COLUMN_DATA);
	}
	
	public static User getUser(String name) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				User.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, name)), 
				DbObject.COLUMN_DATA, 
				User.class);
	}

	public static User getUser(UUID userId) throws IOException {
		if (userId == null || userId.isValid() == false)
			return null;
		
		User userCached = getUserFromCache(userId);
		
		if (userCached != null)
			return userCached;
		
		Email email = UserUUID.getUserEmail(userId);
		if (email != null) {
			User user = User.getUser(email);
			
			updateCache(user);
			
			return user;
		} else
			return null;
	}
}
