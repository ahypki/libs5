package net.hypki.libs5.weblibs.db;

import java.io.IOException;
import java.io.Serializable;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public abstract class WeblibsObject<T> extends DbObject<T> implements Serializable {
	private static final long serialVersionUID = -8619413946307946267L;

	
	@Override
	public String getKeyspace() {
		return WeblibsConst.KEYSPACE;
	}
	
	public <T extends WeblibsObject> T copy() {
		return (T) JsonUtils.fromJson(getData(), getClass());
	}
	
	public static <T extends WeblibsObject> T initByKey(Class<T> clazz,  
			String keyspace, String columnFamily, String key, String column) throws IOException {
		return initByKey(clazz, false, keyspace, columnFamily, key, column);
	}
	
	public static <T extends WeblibsObject> T initByKey(Class<T> clazz, boolean useCache, 
			String keyspace, String columnFamily, String key, String column) throws IOException {
		if (useCache) {
			if (WeblibsConst.CACHE_GLOBALLY_ENABLED) {
				// first search in cache
				T obj = CacheManager.cacheInstance().get(clazz, clazz.getSimpleName(), DbObject.buildCombinedKey(keyspace, columnFamily, key, column));
//				if (cache == null) {
//					LibsLogger.warn(clazz, "Cache for class " + clazz.getSimpleName() + " doesn't exist, creating default one");
//					cache = new Cache(clazz.getSimpleName(), 15000, false, true, 600, 600);
//					getCacheManager().addCache(cache);
//				}
				
//				Element myObject = cache.get();
				if (obj != null)
					return obj;//(T) myObject.getObjectValue();
		
				// read object from database
				obj = get(keyspace, columnFamily, key, DbObject.COLUMN_PK, column, clazz);
			
				// put to cache this object
				if (obj != null)
					CacheManager.cacheInstance().put(clazz.getSimpleName(), DbObject.buildCombinedKey(keyspace, columnFamily, key, column), obj);
				
				return obj;
			} else {
				// read object from database
				T obj = get(keyspace, columnFamily, key, DbObject.COLUMN_PK, column, clazz);

				return obj;
			}
		} else 
			return get(keyspace, columnFamily, key, DbObject.COLUMN_PK, column, clazz);
	}
	
	private static <T extends WeblibsObject> T get(String keyspace, String columnFamily, String key, String keyColName, String column, Class<T> clazz) throws IOException {
		return DbObject.getDatabaseProvider().get(keyspace, columnFamily, 
				new C3Where()
					.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, key)), column, clazz);
	}
}
