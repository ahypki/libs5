package net.hypki.libs5.weblibs.jobs;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class StopWorkerJob extends Job {
	
	private static UUID jvmId = UUID.random();
	
	@Expose
	@NotNull
	@AssertValid
	private UUID thisJvmId = jvmId;

	public StopWorkerJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		// do nothing
	}
	
	public boolean isThisJvm() {
		return getThisJvmId().equals(jvmId);
	}

	private UUID getThisJvmId() {
		return thisJvmId;
	}

	private void setThisJvmId(UUID thisJvmId) {
		this.thisJvmId = thisJvmId;
	}
}
