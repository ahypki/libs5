package net.hypki.libs5.weblibs.jobs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.tests.LibsTestCase;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.HazelcastManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class JobsManager {
	
	public static List<JobsWorker> getJobsWorkersThreads() {
		List<JobsWorker> workers = new ArrayList<JobsWorker>();
		
		for (Thread tread : SystemUtils.iterateThreads()) {
			if (tread instanceof JobsWorker) {
				workers.add((JobsWorker) tread);
			}
		}
		
		return workers;
	}
	
	public static boolean stop(final String jobName) {
		for (JobsWorker worker : getJobsWorkersThreads()) {
			if (worker.getCurrentJobName() != null 
					&& worker.getCurrentJobName().equals(jobName)) {
				try {
					CacheManager.cacheInstance().put(JobsWorker.JOBS_DONE_CACHE, worker.getCurrentJob().getDuplicationId(), "true");
//					CacheManager.cacheInstance().remove(JobsWorker.JOBS_DONE_CACHE, worker.getCurrentJob().getDuplicationId());
					
					HazelcastManager.getInstance().getMap("JOBS_MAP_" + worker.getChannel()).remove(worker.getCurrentJob().getDuplicationId());
					
					worker.getCurrentJob().remove();
					
					worker.shutdown();// stop(); interrupt();// stop();//shutdown();
					
					JobsManager.init(worker.getChannel());
				} catch (Throwable t) {
					LibsLogger.error(JobsManager.class, "Stopping thread exception", t);
				}
				return true;
			}
		}
		return false;
	}
	
	public static boolean stop(final long jobId) {
		for (JobsWorker worker : getJobsWorkersThreads()) {
			if (worker.getId() == jobId) {
				try {
					worker.shutdown();// stop(); interrupt();// stop();//shutdown();
				} catch (Throwable t) {
					LibsLogger.error(JobsManager.class, "Stopping thread exception", t);
				}
				return true;
			}
		}
		return false;
	}
	
	public static void init(String ... channelNames) {
		init((List<String>) ArrayUtils.toList(channelNames));
	}
	
	public static void init(List<String> channelNames) {
		long start = System.currentTimeMillis();
		
		for (String channelName : channelNames)
			new JobsWorker(channelName).start();
		
		LibsLogger.debug("Jobs workers started in " + (System.currentTimeMillis() - start));
	}
	
	// TODO these functions are messy
	public static void runAllJobs(final List<String> channelNames) throws IOException, ValidationException {
		runAllJobs(channelNames, 0);
	}
	
	public static void runAllJobs(final List<String> channelNames, int sleepBetweenJobs) throws IOException, ValidationException {
		// TODO fix this function, it will not work if there are multiple channels, and one channels will have some jobs but the jobs will depend on another channels' jobs
		for (String channelName : channelNames) {
			runAllJobs(channelName, sleepBetweenJobs);
		}
	}
	
	public static void runAllJobs(final String channelName) throws IOException, ValidationException {
		runAllJobs(channelName, 0);
	}
	
	public static void stopJobs(final Class<? extends Job> jobsToStop) throws IOException, ValidationException {
		LibsLogger.debug(JobsManager.class, "Stopping jobs of the class ", jobsToStop, "...");
		
		for (Job job : Job.iterateTable(Job.class, WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY, DbObject.COLUMN_DATA)) {
			if (job.getClass().equals(jobsToStop)) {
				job.remove();
				LibsLogger.debug(JobsManager.class, "Job " + job.getClazz() + " " + job.getCombinedKey() + " stopped");
			}
		}
	}
	
	public static void runAllJobs(final String channelName, int sleepBetweenJobsMS) throws IOException, ValidationException {
		LibsLogger.debug(JobsManager.class, "Running all jobs for channel ", channelName, "...");
		while (true) {
			// getting jobs from DB
			List<Job> jobs = JobFactory.getOldestJobs(channelName, 5000, DateUtils.YEAR_2008);
			
			if (jobs.size() == 0) {
				break;
			} else { 
				// run jobs
				for (Job job : jobs) {
					try {
						job.run();
						
						if (job.getPostponeMs() > 0) {
							job
								.copy()
								.save();
							job.setPostponeMs(0);
						} 
						
						job.remove();
						LibsLogger.debug(JobsManager.class, "Job " + job.getClazz() + " " + job.getCombinedKey() + " done");
					} catch (Exception e) {
						LibsLogger.error(JobsManager.class, "Job " + job.getClazz() + " " + job.getId() + " failed. ", e);
//						job.remove();
					}
					
					if (sleepBetweenJobsMS > 0) {
						try {
							LibsLogger.debug(LibsTestCase.class, "Sleeping for " + sleepBetweenJobsMS + " [ms]...");
							Thread.sleep(sleepBetweenJobsMS);
						} catch (InterruptedException e) {
							LibsLogger.error(LibsTestCase.class, "Cannot take a nap", e);
						}
					}
				}
			}
		}
	}
	
	public static long getJobsCount(String channelName) throws IOException {
		return JobFactory.getJobsCount(channelName);
	}

	public static long getJobsCount(List<String> channelNames) {
		long allJobs = 0;
		
		for (String channelName : channelNames) {
			try {
				allJobs += getJobsCount(channelName);
			} catch (IOException e) {
				LibsLogger.error(JobsManager.class, "Cannot check " + channelName + " for the number of jobs", e);
			}
		}
		
		return allJobs;
	}

	public static void runAllJobs() {
		LibsLogger.debug(Job.class, "Running all jobs...");
		for (Row jobRow : DbObject.getDatabaseProvider().tableIterable(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY)) {
			try {
				JsonElement json = JsonUtils.parseJson(jobRow.getAsString(DbObject.COLUMN_DATA));
				Job job = (Job) JsonUtils.fromJson(json, Class.forName(JsonUtils.readString(json, "clazz")));
				job.run();
			} catch (Exception e) {
				LibsLogger.error(Job.class, "Job " + jobRow.getAsString(DbObject.COLUMN_DATA) + " failed", e);
			}
		}
	}
}
