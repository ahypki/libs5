package net.hypki.libs5.weblibs;

import static net.hypki.libs5.search.query.TermRequirement.MUST;
import static net.hypki.libs5.search.query.TermRequirement.MUST_NOT;
import static net.hypki.libs5.search.query.TermRequirement.SHOULD;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.user.User;

public class SearchManager {

//	public static final String QUERYSTRING_SEARCH_TERM = "s";
	private static SearchEngineProvider searchProvider = null;
	
	private SearchManager() {
		
	}
	
	public static void init(final String searchClass) {
//		if (searchProvider == null) {
			try {
				long start = System.currentTimeMillis();

				LibsLogger.debug(SearchManager.class, "Creating new instance of ", searchClass);
				
				searchProvider = (SearchEngineProvider) Class.forName(searchClass).newInstance();
				searchProvider.init(Settings.getSettings());
				
				LibsInit.addToRunningModules(searchProvider);
				
				LibsLogger.debug("SearchObject started in " + (System.currentTimeMillis() - start));
			} catch (Exception e) {
				throw new RuntimeException("Cannot create instance of SearchEngineProvider", e);
			}
//		}
//		return searchProvider;
	}
	
	public static SearchEngineProvider searchInstance() {
		if (searchProvider == null)
			init(Settings.getString("SearchEngineProvider/class"));
		return searchProvider;	
	}

	public static void index(DbObject cassObj) throws IOException {
		searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, 
				cassObj.getColumnFamily().toLowerCase(),
				cassObj.getCombinedKey().toLowerCase(),
				cassObj.getData());
	}
	
	public static void index(DbObject cassObj, String data) throws IOException {
		searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, 
				cassObj.getColumnFamily().toLowerCase(),
				cassObj.getCombinedKey().toLowerCase(),
				data);
	}
	
	public static void index(List<DbObject> cassObjs) throws IOException {
		if (cassObjs == null || cassObjs.size() == 0)
			return;
		
		Map<String, String> map = new HashMap<>();
		
		for (DbObject dbObject : cassObjs) {
			map.put(dbObject.getCombinedKey().toLowerCase(), dbObject.getData());
		}
		
		searchInstance().indexDocument(WeblibsConst.KEYSPACE_LOWERCASE, 
				cassObjs.get(0).getColumnFamily().toLowerCase(),
				map
//				cassObj.getCombinedKey().toLowerCase(),
//				cassObj.getData()
				);
	}
	
//	private static BoolQueryBuilder itemAllFields(String searchQuery) {
//		return boolQuery().should(wildcardQuery("title", searchQuery))
//			.should(wildcardQuery("tags", searchQuery))
//			.should(wildcardQuery("fields.name", searchQuery))
//			.should(wildcardQuery("fields.value", searchQuery));
//	}
//	
//	private static BoolQueryBuilder userAllFields(String searchQuery) {
//		return boolQuery()
//			.should(wildcardQuery("email.email", "*" + searchQuery + "*"))
//			.should(wildcardQuery("login", "*" + searchQuery + "*"));
//	}
//	
//	private static BoolQueryBuilder groupAllFields(String searchQuery) {
//		return boolQuery()
//			.should(wildcardQuery("name", searchQuery));
//	}
	
	private static boolean isTermNameValid(String term) {
		return term.equalsIgnoreCase("title") || term.equalsIgnoreCase("tags") || term.startsWith("fields.");
	}
	
	public static Query parse(String query) {
		Query q = new Query();
		StringTokenizer st = new StringTokenizer(query, " ");
		 
    	while(st.hasMoreElements()){
    		String term = st.nextToken();

    		if (term.charAt(0) == '+') {
    			q.addTerm(new StringTerm("tags", term.substring(1), MUST));
    		} else if (term.charAt(0) == '!') {
    			q.addTerm(new StringTerm("tags", term.substring(1), MUST_NOT)); 
    		} else {
    			q.addTerm(new Bracket(term.charAt(0) == '-' ? MUST_NOT : MUST)
							.addTerm(new WildcardTerm("title", term, SHOULD))
							.addTerm(new WildcardTerm("tags", term, SHOULD))
							.addTerm(new WildcardTerm("fields.name", term, SHOULD))
							.addTerm(new WildcardTerm("fields.str", term, SHOULD))
    					);
    		}
    	}
    	return q;
		
//		int index = 0;
//		String part = null;
//		String term = null;
//		
//		String [] parts = StringUtil.split(searchQuery, " ");
//		for (int i = 0; i < parts.length; i++) {
//			part = parts[i];
//			
//			if (part.length() == 0)
//				continue;
//			
//			if (part.charAt(0) == '-') {
//				if ((index = part.indexOf(':')) >= 0) {
//					// -fieldname:value
//					term = part.substring(1, index);
//					if (isTermNameValid(term)) {
//						if (term.startsWith("fields.")) {
//							queryExplain.append(" NOT (fields.name:" + term.substring(7) + " AND fields.value:" + part.substring(index + 1) + ")");
//							boolQuery = boolQuery.mustNot(boolQuery().mustNot(termQuery("fields.name", term.substring(7)))
//																	 .mustNot(wildcardQuery("fields.value", part.substring(index + 1))));
//						} else {
//							queryExplain.append(" NOT " + term + ":" + part.substring(index + 1));
//							boolQuery = boolQuery.mustNot(termQuery(term, part.substring(index + 1)));
//						}
//					}
//				} else {
//					// -word
//					queryExplain.append(String.format(" NOT (title:%s OR tags:%s OR fields.name:%s OR fields.value:%s)", 
//							part.substring(1), part.substring(1), part.substring(1), part.substring(1)));
//					boolQuery = boolQuery.mustNot(itemAllFields(part.substring(1)));
//				}
//			} else {
//				if ((index = part.indexOf(':')) >= 0) {
//					// [+]fieldname:value
//					term = part.substring(0, index);
//					if (isTermNameValid(term)) {
//						if (term.startsWith("fields.")) {
//							queryExplain.append(" AND (fields.name:" + term.substring(7) + " AND fields.value:" + part.substring(index + 1) + ")");
//							boolQuery = boolQuery.must(boolQuery().must(termQuery("fields.name", term.substring(7)))
//																  .must(wildcardQuery("fields.value", part.substring(index + 1))));
//						} else {
//							queryExplain.append(" AND " + term + ":" + part.substring(index + 1));
//							boolQuery = boolQuery.must(termQuery(term, part.substring(index + 1)));
//						}
//					}
//				} else {// [+]word
//					if (part.charAt(0) == '+') {
//						term = part.substring(1);
//						queryExplain.append(String.format(" AND (tags:%s)", term));
//						boolQuery = boolQuery.must(wildcardQuery("tags", term));
//					} else {
//						queryExplain.append(String.format(" AND (title:%s OR tags:%s OR fields.name:%s OR fields.value:%s)", part, part, part, part));
//						boolQuery = boolQuery.must(itemAllFields(part));
//					}
//				}
//			}
//		}
//		
//		return boolQuery;
	}
	
	
	public static void removeObject(String id, String columnFamily) throws IOException {
		searchInstance().removeDocument(WeblibsConst.KEYSPACE_LOWERCASE, columnFamily.toLowerCase(), id.toLowerCase());
	}

	public static SearchResults<User> searchUsers(String searchTerm, int from, int size) throws IOException {
		Query q = new Query();
		if (notEmpty(searchTerm)) {
			String terms[] = searchTerm.split("[^\\w\\d]+");
			for (String term : terms) {
				if (term.length() > 0) {
					q.addTerm(new Bracket(TermRequirement.MUST)
						.addTerm(new WildcardTerm("email.email", term, TermRequirement.SHOULD))
						.addTerm(new WildcardTerm("login", term, TermRequirement.SHOULD)));
				}
			}
		} else
			q.addTerm(new WildcardTerm("email.email", "*", TermRequirement.SHOULD));
		q.setSortBy("creationDate", SortOrder.DESCENDING);
		return searchInstance().search(User.class, WeblibsConst.KEYSPACE_LOWERCASE, User.COLUMN_FAMILY.toLowerCase(), q, from, size);
	}

}
