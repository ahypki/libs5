package net.hypki.libs5.weblibs.jobs;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;

public class RunAllJobsJob extends Job {

	public RunAllJobsJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		JobsWorker.checkForAllNewJobs();
	}
}
