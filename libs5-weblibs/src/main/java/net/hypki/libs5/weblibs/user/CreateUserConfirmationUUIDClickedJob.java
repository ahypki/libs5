package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class CreateUserConfirmationUUIDClickedJob extends Job {
	
	@NotNull
	@AssertValid
	@Expose
	private UUID userId = null;
	
	public CreateUserConfirmationUUIDClickedJob() {
		
	}
	
	public CreateUserConfirmationUUIDClickedJob(UUID userId) {
		setUserId(userId);
	}

	@Override
	public void run() throws IOException, ValidationException {
		try {
			if (getUserId() != null) {
				LibsLogger.debug(CreateUserConfirmationUUIDClickedJob.class, "UserId is not null " + getUserId().getId());
				User user = UserFactory.getUser(getUserId());
				if (user != null) {
					LibsLogger.debug(CreateUserConfirmationUUIDClickedJob.class, "Found user " + user.getEmail());
					if (user.getStatus() == UserStatus.NORMAL) {
						LibsLogger.debug(CreateUserConfirmationUUIDClickedJob.class, "User has status " + UserStatus.NORMAL);
						UserConfirmationUUID confirm = new UserConfirmationUUID();
						confirm.setUserUUID(getUserId());
						confirm.setConfirmUuid(UUID.random());
						confirm.setClicked(true);
						confirm.save();
						LibsLogger.debug(getClass(), "Confirmation click saved " + confirm.toString());
						return;
					}
				}
			}
			LibsLogger.error(getClass(), "Something is wrong, tried to create confirmation click for the user user " + getUserId() + " but failed");
		} catch (Exception e) {
			LibsLogger.error(getClass(), "Something is wrong, tried to create confirmation click for the user user " + getUserId() + " but failed", e);
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
