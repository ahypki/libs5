package net.hypki.libs5.weblibs.search;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObjectIndexable;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class DeindexJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private String objectData = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String objectClass = null;
	
	public DeindexJob() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}
	
	public DeindexJob(WeblibsObjectIndexable o) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setObjectData(o.getData());
		setObjectClass(o.getClass().getName());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			WeblibsObjectIndexable bo = (WeblibsObjectIndexable) JsonUtils.fromJson(getObjectData(), Class.forName(getObjectClass()));
			
			((Indexable) bo).deindex();
			LibsLogger.debug(DeindexJob.class, "Object of the class ", getObjectClass(), " indexed successfully");
		} catch (Throwable e) {
			LibsLogger.error(DeindexJob.class, "Cannot index object of the class " + getObjectClass(), e);
		}
	}

	public String getObjectData() {
		return objectData;
	}

	public void setObjectData(String objectData) {
		this.objectData = objectData;
	}

	public String getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}

}
