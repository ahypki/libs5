package net.hypki.libs5.weblibs.user;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.mail.Mailer;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import org.antlr.stringtemplate.StringTemplate;

import com.google.gson.annotations.Expose;



public class ConfirmationEmailOperation extends Job {
	
	@NotNull
	@AssertValid
	@Expose
	private User user = null;
	
	public ConfirmationEmailOperation() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}
	
	public ConfirmationEmailOperation(User user) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setUser(user);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
//		if (WeblibsConst.REGISTRATION_ENABLED == false) {
//			LibsLogger.warn(ConfirmationEmailOperation.class, "Registration is off. Not sending any email. Consuming job.");
//			return;
//		}
//		
//		final User user = getUser();
//		if (user != null && user.getStatus() == UserStatus.NEW) {
//			String to = user.getEmail().toString();
//			
//			try {
//				UserConfirmationUUID uConfirmFromDb = UserConfirmationUUID.getUserConfirmationUUID(user.getUserId());
//				
//				if (uConfirmFromDb != null) {
//					String subject = Settings.getString("Registration/ConfirmationLinkSubject");
//					String bodyPath = Settings.getString("Registration/ConfirmationLinkBody");
//					
//					if (nullOrEmpty(subject))
//						subject = "Mail confirmation";
//					
//					String mailIntegrationOK = null;
//					if (nullOrEmpty(bodyPath))
//						mailIntegrationOK = "Dear $user$,\nan account at $domain$ project has been created for you.\nConfirm the account creation by clicking to this link:\n$domain$/confirm/$confirmation$\n\nRegards,";
//					else
//						mailIntegrationOK = SystemUtils.readFileContent(bodyPath);
//					
//					StringTemplate template = new StringTemplate(mailIntegrationOK);
//					Map<String, String> params = new HashMap<String, String>();
//					params.put("user", user.getUserId().getId());
//					params.put("domain", WeblibsConst.DOMAIN);
//					params.put("confirmation", uConfirmFromDb.getConfirmUuid().getId());
//					template.setAttributes(params);
//					
//					Mailer.sendMail(to, subject, template.toString(), true);
//				
//					LibsLogger.debug(ConfirmationEmailOperation.class, "User ", to, " setting to ", UserStatus.UNCONFIRMED);
//					user.setStatus(UserStatus.UNCONFIRMED);
//					
//					LibsLogger.debug(ConfirmationEmailOperation.class, "User ", to, " saving..");
//					user.save();
//				}
//			} catch (Exception e) {
//				throw new IOException("Cannot send confirmation email to " + user.getEmail(), e);
//			}
//		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
