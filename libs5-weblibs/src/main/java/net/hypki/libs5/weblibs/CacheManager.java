package net.hypki.libs5.weblibs;

import net.hypki.libs5.cache.CacheProvider;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.json.JsonUtils;

public class CacheManager {

	private static CacheProvider cacheProvider = null;
	
	private CacheManager() {
		
	}
	
	public static CacheProvider cacheInstance() {
		if (cacheProvider == null) {
			try {
				cacheProvider = (CacheProvider) Class.forName(JsonUtils.readString(Settings.getSettings(), "CacheProvider/class")).newInstance();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				throw new RuntimeException("Cannot create instance of CacheProvider", e);
			}
		}
		return cacheProvider;
	}
}
