package net.hypki.libs5.weblibs.mail;

import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;

import net.hypki.libs5.utils.LibsLogger;

import com.google.gson.annotations.Expose;
import com.sun.mail.util.MailSSLSocketFactory;

public class MailCredentials implements Serializable {

	@Expose
	private String host = null;
	
	@Expose
	private int port = 0;
	
	@Expose
	private String from = null;
	
	@Expose
	private String pass = null;
	
	@Expose
	private boolean ssl = false;
	
	@Expose
	private int timeoutMs = 25000;
	
	private Session session = null;
	private Transport transport = null;
	
	public MailCredentials() {
		
	}
	
	public MailCredentials(String host, int port, String from, String pass, boolean ssl) {
		setHost(host);
		setPort(port);
		setFrom(from);
		setPass(pass);
		setSsl(ssl);
	}
	
	public MailCredentials(String host, int port, String from, String pass, boolean ssl, int timeoutMs) {
		setHost(host);
		setPort(port);
		setFrom(from);
		setPass(pass);
		setSsl(ssl);
		setTimeoutMs(timeoutMs);
	}
	
	@Override
	public String toString() {
		return String.format("Host=%s Port=%d Username=%s", getHost(), getPort(), getFrom());
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean isSsl() {
		return ssl;
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}
	
	public Session getSession() {
		if (session == null) {
			try {
				if (isSsl()) {
					//create Authenticator object to pass in Session.getInstance argument
					Authenticator auth = new Authenticator() {
						//override the getPasswordAuthentication method
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(getFrom(), getPass());
						}
					};
					session = Session.getInstance(getProps(), auth);
				} else {
					session = Session.getInstance(getProps());
				}
			} catch (GeneralSecurityException e) {
				LibsLogger.error(MailCredentials.class, "Cannot create mail session", e);
			}
		}
		return session;
	}
	
	private Properties getProps() throws GeneralSecurityException {
		Properties props = null;
		
		if (isSsl()) {
			props = System.getProperties();
			
			MailSSLSocketFactory sf = new MailSSLSocketFactory();
			sf.setTrustAllHosts(true); 
			
			props.put("mail.imap.ssl.trust", "*");
			props.put("mail.imap.ssl.socketFactory", sf);
			props.put("mail.smtp.ssl.trust", "*");
			props.put("mail.smtp.ssl.socketFactory", sf);
			props.put("mail.smtp.host", getHost()); //SMTP Host
			props.put("mail.smtp.port", "" + getPort()); //TLS Port
			props.put("mail.smtp.auth", "true"); //enable authentication
			props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
			
			return props;
		} else {
			props = System.getProperties();
			props.put("mail.smtp.socketFactory.port", port);
//			if (isSsl())
//				props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "true");
			
			props.put("mail.smtp.timeout", String.valueOf(getTimeoutMs()));
		}
		return props; 
	}
	
	public Transport getTransport() throws MessagingException {
		if (transport != null)
			return transport;
		
		transport = createTransport();
		
		if (transport == null) {
			LibsLogger.debug(Mailer.class, "Trying to create transport again");
			transport = createTransport();
		}
		
		return transport;
	}
	
	public void closeTransport() {
		if (transport != null) {
			try {
				transport.close();
			} catch (MessagingException e) {
				LibsLogger.warn(Mailer.class, "Cannot close mail transport", e);
			}
			transport = null;
		}
	}

	public Transport createTransport() throws MessagingException {
		if (getSession() != null) {
			try {
				closeTransport();
				
				if (isSsl())
					transport = getSession().getTransport("smtps");
				else
					transport = getSession().getTransport("smtp");
				transport.connect(getHost(), getPort(), getFrom(), getPass());
				
				return transport;
			} catch (AddressException e) {
				LibsLogger.error(Mailer.class, "Cannot create mail transport", e);
				closeTransport();
			} catch (NoSuchProviderException e) {
				LibsLogger.error(Mailer.class, "Cannot create mail transport", e);
				closeTransport();
			}
		}
		return null;
	}

	public int getTimeoutMs() {
		return timeoutMs;
	}

	public void setTimeoutMs(int timeoutMs) {
		this.timeoutMs = timeoutMs;
	}

	public boolean isValid() {
		try {
			createTransport();
			return true;
		} catch (Exception e) {
			LibsLogger.error(MailCredentials.class, "Cannot create email transport", e);
			return false;
		}
	}
}
