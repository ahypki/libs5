package net.hypki.libs5.weblibs.jobs;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.IModule;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.TripleMap;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.HazelcastManager;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

import com.google.gson.annotations.Expose;
import com.hazelcast.core.HazelcastException;
import com.hazelcast.core.HazelcastInstanceNotActiveException;
import com.hazelcast.map.IMap;

public class JobsWorker extends Thread implements IModule {

//	private static final String JOBS_QUEUE_PREFIX = "JOBS_QUEUE_PREFIX_";
	private static final String JOB_LOCK_PREFIX = "JOB_LOCK_";
	private static final String JOB_LOCK_NEW_JOBS = "JOB_LOCK_NEW_JOBS";
	
	public static final String JOBS_DONE_CACHE = "JOBS_DONE_CACHE";
	
	private static final long RESTART_JOBS_INTERVAL_MS = 120 * 1000; // 2 minutes
	private static final int CHECK_FOR_NEW_JOBS_INTERVAL_MS = 5 * 1000;
	
	public static boolean PRINT_DEBUG = false;
		
	private boolean stopRequested = false;
	private boolean finished = false;
	
	private long lastTime = DateUtils.YEAR_2008;
	
//	private BlockingQueue<Job> jobsBlockingQueue = null;
	private IMap<String, Job> jobsMap = null;
	private long lastRestartTimeMs = 0;
	
	@Expose
	private String channel = WeblibsConst.CHANNEL_NORMAL;
	
	private Job currentJob = null;
	
	private long currentJobStartTime = 0;
	
	private static Map<String, Map<String, Job>> jobsNotDistributed = new HashMap<String, Map<String,Job>>();

	public JobsWorker() {
		super("[" + WeblibsConst.CHANNEL_NORMAL + "]");
		
		setChannel(WeblibsConst.CHANNEL_NORMAL);
		
//		LibsInit.addToRunningModules(this);
//		LibsLogger.debug(JobsWorker.class, getName() + " started");
	}
	
	public JobsWorker(String channel) {
		super("[" + channel + "]");
		
		setChannel(channel);
		
//		LibsInit.addToRunningModules(this);
//		LibsLogger.debug(JobsWorker.class, getName() + " started");
	}
	
	@Override
	public synchronized void start() {
		LibsInit.addToRunningModules(this);
		LibsLogger.debug(JobsWorker.class, getName() + " started");
		
		super.start();
	}
	
	@Override
	public boolean isInterrupted() {
		return super.isInterrupted() || this.stopRequested;
	}
	
	@Override
	public void shutdown() {
		int tries = 10;
		LibsLogger.debug(JobsWorker.class, "Trying to stop worker: " + getName());
		stopRequested = true;
		
		// waiting to thread to stop
		while (finished == false) {
			takeANap(200);
			
			if (tries-- <= 0) {
				interrupt();
				break;
			}
		}
	}
	
	@Override
	public void run() {
		List<Mutation> mutsCached = new ArrayList<Mutation>();
		
		while (stopRequested == false) {
			try {
				for (String jobDuplicationId : getJobsMap().keySet()) {
					Job job = getJobsMap().get(jobDuplicationId);
					
					if (job == null)
						continue; // the job is already done by someone else
					
					if (job instanceof StopWorkerJob) {
						// stopping this thread
						StopWorkerJob stopJob = (StopWorkerJob) job;
						if (stopJob.isThisJvm()) {
							// stop this JobsWorker
							return;
						} else {
							// remove old StopWorkerJob
							job.remove();
						}
					}
					
					Lock lockJobId = null;
					boolean lockedHere = false;
					try {
						// try to get a lock
//						if (HazelcastManager.ENABLED)
						lockJobId = HazelcastManager.getInstance().getCPSubsystem().getLock(JOB_LOCK_PREFIX + jobDuplicationId);
						if (lockJobId.tryLock(1000, MILLISECONDS) == true) {
							lockedHere = true;
							
							// check again if the job is still present in the jobsMap
							if (getJobsMap().get(jobDuplicationId) != null) {
								
								// check if job was done already
								if (CacheManager.cacheInstance().get(String.class, JOBS_DONE_CACHE, jobDuplicationId) == null) {
									Watch w = new Watch();
									LibsLogger.debug(JobsWorker.class, "Starting Job ", job.getId(), " ", job.getClazz());
									
									this.currentJob = job;
									this.setCurrentJobStartTime(System.currentTimeMillis());
									
									if (job.getLogSeparately() != null) {
										String logName = job.getLogSeparately();
										Thread.currentThread().setName("LogSeparately:" + logName);
										
										FileAppender fa = new FileAppender(new org.apache.log4j.PatternLayout("BEANS %d %-5p %c - %m%n"), logName, false);
										fa.setName(logName);
										Logger
//											.getRootLogger()
											.getLogger("00")
											.addAppender(fa);
									}
									
									try {
										job.run();
									} catch (Throwable t) {
										LibsLogger.error(JobsWorker.class, t, "Job ", job.toString(), " has failed");
										job.setPostponeMs(RESTART_JOBS_INTERVAL_MS);
									} finally {
										if (job.getLogSeparately() != null) {
											Thread.currentThread().setName("[" + getChannel() + "]");
											
											String logName = job.getLogSeparately();
											if (Logger.getLogger("00").getAppender(logName) != null) {
												Logger.getLogger("00").getAppender(logName).clearFilters();
												Logger.getLogger("00").getAppender(logName).close();
												Logger.getLogger("00").removeAppender(logName);
											}
										}
									}
									
									this.currentJob = null;
									this.setCurrentJobStartTime(0);
									
									if (job.getPostponeMs() > 0) {
										((Job) job.copy())
//											.setId(UUID.random()) // TODO this should be on
//											.setPostponeMs(0)
											.setCreationMs(SimpleDate.now().getTimeInMillis() + job.getPostponeMs())
											.save();
										job.setPostponeMs(0);
									} 
									
									CacheManager.cacheInstance().put(JOBS_DONE_CACHE, jobDuplicationId, "true");
//									job.remove();
									mutsCached.addAll(job.getRemoveMutations());
									if (mutsCached.size() > 500) {
										DbObject.getDatabaseProvider().save(mutsCached);
										mutsCached.clear();
									}
									
									getJobsMap().remove(jobDuplicationId);
									
									LibsLogger.debug(JobsWorker.class, "Job ID=", job.getKey(), " of type ", job.getClazz(), " done in ", w.toString());
								} else {
									LibsLogger.debug(JobsWorker.class, "Job ", job.getClass().getSimpleName(), " ", jobDuplicationId, " was already done by some other thread");
									
									getJobsMap().remove(jobDuplicationId);
								}
							}
						}
					} catch (Throwable e) {
						LibsLogger.error(JobsWorker.class, "JobData: " + job.getClass().getSimpleName() + " with ID= " + job.getId().getId() + " failed! Moving to the next job.", e);
					} finally {
						if (lockJobId != null && lockedHere == true)
							lockJobId.unlock();
					}
				}
				
				if (mutsCached.size() > 0) {
					DbObject.getDatabaseProvider().save(mutsCached);
					mutsCached.clear();
				}
				
				checkForNewJobs();
				
			} catch (HazelcastException e) {
				LibsLogger.error(JobsWorker.class, "Hazelcast stopped", e);
				HazelcastManager.resetInstance();
			} catch (Exception e) {
				LibsLogger.error(JobsWorker.class, "Cannot get next jobs from DB. Waiting...", e);
				takeANap(5000);
			}
		}
		
		LibsLogger.debug(JobsWorker.class, getName() + " finished");
		finished = true;
	}
	
	public static boolean isDone(String channel, long createMs, UUID jobId) throws IOException {
		final String duplicationId = Job.buildKey(channel, createMs, jobId.getId());
		
		String doneFromCache = CacheManager.cacheInstance().get(String.class, JOBS_DONE_CACHE, duplicationId);
		if (doneFromCache != null && doneFromCache.equals("true"))
			return true;
		
		if (doneFromCache != null)
			return false;
		
		Job fromDb = JobFactory.getJobData(channel, createMs, jobId);
		
		if (fromDb == null)
			return true;
		
		return false;
	}
	
	public static void markAsDone(Job job) {
		CacheManager.cacheInstance().put(JOBS_DONE_CACHE, job.getDuplicationId(), "true");
	
		String channel = "JOBS_MAP_" + job.getChannel();
//		if (HazelcastManager.ENABLED)
			HazelcastManager.getInstance().getMap(channel).remove(job.getDuplicationId());
//		else
//			getLocalMap(channel).remove(job.getDuplicationId());
	}
	
	public static void checkForAllNewJobs() {
		try {
			int jobsAdded = 0;
			long now = System.currentTimeMillis();
						
			for (Job job : JobFactory.iterateJobs()) {
				
				if (job.getCreationMs() > now)
					continue; // these are jobs for the future
				
				if (job instanceof RunAllJobsJob)
					continue;
				
				if (CacheManager.cacheInstance().get(String.class, JOBS_DONE_CACHE, job.getDuplicationId()) != null) {
					try {//Job.getJobData(job.getChannel(), job.getCreationMs(), job.getId())
						job.remove();
						CacheManager.cacheInstance().remove(JOBS_DONE_CACHE, job.getDuplicationId());
					} catch (Throwable t) {
						LibsLogger.debug(JobsWorker.class, "Cannot remove job ", job);
					}
					continue;
				}
				
				try {
					IMap<String, Job> jobsMap = HazelcastManager.getInstance().getMap("JOBS_MAP_" + job.getChannel());
					if (!jobsMap.containsKey(job.getDuplicationId())) {
						jobsMap.put(job.getDuplicationId(), job);
						jobsAdded++;
						LibsLogger.debug(JobsWorker.class, "Job ", job.getClass().getSimpleName(), " ", job.getDuplicationId(), 
								" added to jobs map ", job.getChannel());
					}
				} catch (Exception e) {
					LibsLogger.error(JobsWorker.class, "Cannot add job " + job + " to the queue " + job.getChannel(), e);
				}
				
				if (jobsAdded < 5 || jobsAdded % 100 == 0)
					LibsLogger.info(JobsWorker.class, "checkForAllNewJobs added "+  jobsAdded);
			}
		} catch (Throwable e) {
			LibsLogger.error(JobsWorker.class, "Cannot check for new jobs", e);
		} finally {
			
		}
	}
	
	private void checkForNewJobs() throws IOException, InterruptedException {
		Lock lockJobId = null;
		boolean lockedHere = false;
		try {
			// try to get a lock
//			if (HazelcastManager.ENABLED)
			lockJobId = HazelcastManager.getInstance().getCPSubsystem().getLock(JOB_LOCK_NEW_JOBS + getChannel());
			if (lockJobId.tryLock(1000, MILLISECONDS) == true) {
				lockedHere = true;
				
				if (PRINT_DEBUG)
					LibsLogger.debug(JobsWorker.class, "Checking for ", WeblibsConst.JOB_WORKER_NEW_JOBS_COUNT, " new jobs...");
				
				List<Job> jobs = JobFactory.getOldestJobs(getChannel(), WeblibsConst.JOB_WORKER_NEW_JOBS_COUNT, lastTime);
				
				if (jobs != null && jobs.size() > 0 && jobs.get(0).getCreationMs() == lastTime)
					jobs.remove(0);
				
				LibsLogger.trace(JobsWorker.class, "Total number of jobs in channel ", getChannel(), " is ", JobsManager.getJobsCount(getChannel()));
				
				int jobsAdded = 0;
				long now = System.currentTimeMillis();
				for (Job job : jobs) {
					if (job.getCreationMs() > now)
						continue; // these are jobs for the future
					
					if (CacheManager.cacheInstance().get(String.class, JOBS_DONE_CACHE, job.getDuplicationId()) != null) {
						try {//Job.getJobData(job.getChannel(), job.getCreationMs(), job.getId())
							job.remove();
							CacheManager.cacheInstance().remove(JOBS_DONE_CACHE, job.getDuplicationId());
						} catch (Throwable t) {
							LibsLogger.debug(JobsWorker.class, "Cannot remove job ", job);
						}
						continue;
					}
					
					try {
						if (!getJobsMap().containsKey(job.getDuplicationId())) {
							getJobsMap().put(job.getDuplicationId(), job);
							jobsAdded++;
							LibsLogger.debug(JobsWorker.class, "Job ", job.getClass().getSimpleName(), " ", job.getDuplicationId(), " added to jobs map ", getChannel());
						}
					} catch (Exception e) {
						LibsLogger.error(JobsWorker.class, "Cannot add job " + job + " to the queue " + getChannel(), e);
					}
				}
				
				// update last job's time
				if (jobsAdded > 0 && jobs.size() > 0) {
					lastTime = jobs.get(jobs.size() - 1).getCreationMs();
					LibsLogger.debug(JobsWorker.class, "Added " + jobsAdded + " new jobs to queue");
				} else {
					LibsLogger.trace(JobsWorker.class, "There is no new jobs, sleeping for ", CHECK_FOR_NEW_JOBS_INTERVAL_MS, " [ms]. Total ", getJobsMap().size(), " jobs in ", getChannel());
					takeANap(CHECK_FOR_NEW_JOBS_INTERVAL_MS);
				}
				
				// restart jobs after some time
				if ((System.currentTimeMillis() - lastRestartTimeMs) > RESTART_JOBS_INTERVAL_MS) {
					LibsLogger.trace(JobsWorker.class, "Restarting older jobs");
					lastTime = DateUtils.YEAR_2008;
					lastRestartTimeMs = System.currentTimeMillis();
				}
			
			}
		} catch (com.hazelcast.core.HazelcastInstanceNotActiveException e) {
			LibsLogger.debug(JobsWorker.class, "Hazelcast is closed, consuming error...");
			
			takeANap(1000);
		} catch (Throwable e) {
			LibsLogger.error(JobsWorker.class, "Cannot check for new jobs", e);
			
			takeANap(1000);
		} finally {
			if (lockJobId != null && lockedHere == true)
				lockJobId.unlock();
		}
	}
	
	private void takeANap(int ms) {
		try {
			LibsLogger.trace(JobsWorker.class, getChannel(), " sleeping for ", ms, " [ms]");
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			LibsLogger.error(JobsWorker.class, getName() + " cannot take a nap", e);
		}
	}
	
	private IMap<String, Job> getJobsMap() {
		if (jobsMap == null) {
//			if (HazelcastManager.ENABLED)
				this.jobsMap = HazelcastManager.getInstance().getMap("JOBS_MAP_" + getChannel());
//			else
//				this.jobsMap = getLocalMap("JOBS_MAP_" + getChannel());
		}
		return jobsMap;
	}
	
//	private static IMap<String, Job> getLocalMap(String channel) {
//		Map<String, Job> tmp = jobsNotDistributed.get(channel);
//		
//		if (tmp == null) {
//			tmp = new HashMap<String, Job>();
//			jobsNotDistributed.put(channel, tmp);
//		}
//		
//		return tmp;
//	}

	public static boolean isJobsQueueEmpty(String channel) throws IOException {
		IMap<String, Job> jobsMap = null;
		
//		if (HazelcastManager.ENABLED)
			jobsMap = HazelcastManager.getInstance().getMap("JOBS_MAP_" + channel);
//		else
//			jobsMap = getLocalMap("JOBS_MAP_" + channel);
		
		if (jobsMap.isEmpty() == false)
			return false;
		
		List<Job> jobs = JobFactory.getOldestJobs(channel, 1, DateUtils.YEAR_2008);
		return (jobs == null || jobs.size() == 0);
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	public Job getCurrentJob() {
		return this.currentJob;
	}

	public String getCurrentJobName() {
		try {
			return currentJob != null ? currentJob.getKey() : null;
		} catch (Throwable t) {
			LibsLogger.error(JobsWorker.class, "Ohh snap, there is no current job anymore", t);
			return null;
		}
	}

//	public long getCurrentJobStartTime() {
//		return currentJobStartTime;
//	}
	
	public SimpleDate getCurrentJobStartTime() {
		if (this.currentJobStartTime > 0)
			return new SimpleDate(this.currentJobStartTime);
		else
			return null;
	}

	public void setCurrentJobStartTime(long currentJobStartTime) {
		this.currentJobStartTime = currentJobStartTime;
	}
}
