package net.hypki.libs5.weblibs.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.LocaleUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.mail.MailCredentials;
import net.hypki.libs5.weblibs.mail.Mailer;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import org.antlr.stringtemplate.StringTemplate;

import com.google.gson.annotations.Expose;

public class ResetPassRequestedJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private ResetPassUUID resetPassId = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String mailSubject = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String domain = null;
	
	public ResetPassRequestedJob() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}

	public ResetPassRequestedJob(ResetPassUUID resetId) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setResetPassId(resetId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		User user = UserFactory.getUser(getResetPassId().getUserId());
		
		if (user == null) {
			LibsLogger.error(ResetPassRequestedJob.class, "User is not defined in job reseting password");
			return;
		}
		
		try {
			StringTemplate template = new StringTemplate(SystemUtils.readFileContent("newPassRequestedMailTemplate.html"));
			
			Map<String, String> params = new HashMap<String, String>();
			params.put("login", user.getEmail().toString());
			params.put("url", getDomain());
			params.put("userId", user.getUserId().toString());
			params.put("resetId", getResetPassId().getResetId().toString());
			
			template.setAttributes(params);
			
			String mailSubject = getMailSubject();
			
			if (StringUtilities.nullOrEmpty(mailSubject)) {
				StringTemplate templateSubject = new StringTemplate(LocaleUtils.getMsg(getClass(), "en", 
						"MailSubject", "Password reset request"));
				
				Map<String, String> paramsSubject = new HashMap<String, String>();
				paramsSubject.put("domain", WeblibsConst.DOMAIN);
				
				templateSubject.setAttributes(paramsSubject);
				
				mailSubject = templateSubject.toString();
			}
							
			Mailer.sendMail(getResetPassId().getMailCredentials(), user.getEmail().toString(), null, null, 
					mailSubject, template.toString(), true, null);
			
			LibsLogger.debug(getClass(), "Link for new password sent to user " + user.getEmail());
		} catch (MessagingException e) {
			LibsLogger.error(getClass(), "Cannot send email with new password for user " + user.getEmail() + ". Consuming job...", e);
		}
		
	}

	public void setResetPassId(ResetPassUUID resetPassId) {
		this.resetPassId = resetPassId;
	}

	public ResetPassUUID getResetPassId() {
		return resetPassId;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public ResetPassRequestedJob setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
		return this;
	}

	public String getDomain() {
		return domain;
	}

	public ResetPassRequestedJob setDomain(String domain) {
		this.domain = domain;
		return this;
	}
}
