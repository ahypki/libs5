package net.hypki.libs5.weblibs.user;

import java.io.Serializable;

import net.hypki.libs5.utils.url.EmailUtilities;
import net.sf.oval.constraint.CheckWith;
import net.sf.oval.constraint.CheckWithCheck.SimpleCheck;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Email implements SimpleCheck, Serializable {
	private static final long serialVersionUID = 9046692476554613100L;
	
	@NotNull
	@NotEmpty
	@Length(min = 5, max = 512)
	@CheckWith(Email.class)
	@Expose
	private String email = null;

	public Email() {

	}

	public Email(String email) {
		setEmail(email);
	}

	public void setEmail(String email) {
		this.email = (email != null ? email.toLowerCase() : null);
	}

	public String getEmail() {
		return email;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Email) {
			Email inputEmail = (Email) obj;
			return getEmail().equalsIgnoreCase(inputEmail.getEmail());
		} else if (obj instanceof String) {
			return ((String) obj).equalsIgnoreCase(getEmail());
		} else
			return false;
	}

	public byte[] getBytes() {
		return getEmail() != null ? getEmail().getBytes() : null;
	}
	
	@Override
	public String toString() {
		return getEmail() != null ? getEmail().toString() : null;
	}
	
	public boolean isValid() {
		return EmailUtilities.isValidEmail(getEmail());
	}
	
	public String toLowercaseString() {
		return getEmail() != null ? getEmail().toLowerCase() : null;
	}

	@Override
	public boolean isSatisfied(Object validatedObject, Object value) {
		if (value instanceof Email) {
			Email email = (Email) value;
			return EmailUtilities.isValidEmail(email.getEmail());
		} else if (value instanceof String) {
			return EmailUtilities.isValidEmail((String) value);
		} else
			return false;
	}

	public boolean isNullOrEmpty() {
		return email == null || email.trim().length() == 0;
	}

	public String getOnlyLogin() {
		return getEmail().substring(0, getEmail().indexOf('@'));
	}
}
