package net.hypki.libs5.weblibs.uuid;

import net.hypki.libs5.db.db.weblibs.utils.UUID;

public class TimeUUIDUtils {

	public static String createTimeUUID(long timestamp, UUID uuid) {
		return String.format("%d%s", timestamp, uuid.toString());
	}

}
