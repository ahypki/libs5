package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

/**
 * Cassandra: Keyspace.Allowed[{userId}]."allowedRight"
 * @author ahypki
 *
 */
public class Allowed extends WeblibsObject {
	public static final String COLUMN_FAMILY = "allowed";
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userUUID = null;
		
	@NotEmpty
	@NotNull
	@Expose
	private String right = null;
	
	@NotEmpty
	@NotNull
	@Expose
	private Boolean allowed = null;
	
	public Allowed() {
		
	}
	
	public Allowed(UUID userId, String right, boolean isAllowed) {
		setUserUUID(userId);
		setRight(right);
		setAllowed(isAllowed);
	}
	
	public static boolean isAllowed(UUID userUuid, String right) {
		try {
			Allowed a = initByKey(Allowed.class, 
					true, 
					WeblibsConst.KEYSPACE, 
					COLUMN_FAMILY, 
					userUuid.getId() + right.toString(), 
					DbObject.COLUMN_DATA);
			return a != null && a.isAllowed();
		} catch (IOException e) {
			LibsLogger.error(Allowed.class, "Cannot check it user is allowed with the right ", right);
			return false;
		}
	}

	public static Allowed getAllowed(UUID userUuid, String right) {
		try {
			return initByKey(Allowed.class, true, WeblibsConst.KEYSPACE, COLUMN_FAMILY, userUuid.getId() + right.toString(), DbObject.COLUMN_DATA);
		} catch (IOException e) {
			LibsLogger.error(Allowed.class, "Cannot check it user is allowed with the right ", right);
			return null;
		}
	}
	
	@Override
	public Object save() throws ValidationException, IOException {
		LibsLogger.debug(Allowed.class, "Right " + getRight() + " to user " + getUserUUID() + (isAllowed() ? " is allowed" : " is NOT allowed"));
		return super.save();
	}

	@Override
	public String getKey() {
		return getUserUUID().toString() + getRight().toString();
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	public void setUserUUID(UUID userUUID) {
		this.userUUID = userUUID;
	}

	public UUID getUserUUID() {
		return userUUID;
	}

	public String getRight() {
		return right;
	}

	public Allowed setRight(String right) {
		this.right = right;
		return this;
	}

	public Boolean getAllowed() {
		return allowed;
	}
	
	public boolean isAllowed() {
		return getAllowed() != null && getAllowed() == true;
	}

	public Allowed setAllowed(Boolean allowed) {
		this.allowed = allowed;
		return this;
	}
}
