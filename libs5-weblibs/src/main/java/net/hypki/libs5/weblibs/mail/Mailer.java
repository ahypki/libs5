package net.hypki.libs5.weblibs.mail;

import static java.lang.String.format;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.javatuples.Pair;

import com.google.gson.JsonElement;

public class Mailer {
	
//	private static String host = null;
//	private static int port = 0;
//	private static String from = null;
//	private static String pass = null;
//	private static boolean ssl = false;
//	
//	private static Session session = null;
//	private static Transport transport = null;
	
	private static MailCredentials defaultMailCredentials = null;
	
	private static MailCredentials getDefaultMailCredentials() {
		if (defaultMailCredentials == null) {
			JsonElement settings = Settings.getSettings();
			
			String host = settings.getAsJsonObject().get("Mail").getAsJsonObject().get("host").getAsString();
			int port = settings.getAsJsonObject().get("Mail").getAsJsonObject().get("port").getAsInt();
			String from = JsonUtils.readString(settings, "Mail/from");
			String pass = JsonUtils.readString(settings, "Mail/pass");
			boolean ssl = false;
			if (settings.getAsJsonObject().get("Mail").getAsJsonObject().get("SSL") != null)
				ssl = settings.getAsJsonObject().get("Mail").getAsJsonObject().get("SSL").getAsBoolean();
			
			defaultMailCredentials = new MailCredentials(host, port, from, pass, ssl);
		}
		return defaultMailCredentials;
	}
	
	public static void sendMail(String to, String subject, String messageText) throws MessagingException {
		try {
			sendMailPriv(getDefaultMailCredentials(), to, null, null, subject, messageText, false, null);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(getDefaultMailCredentials(), to, null, null, subject, messageText, false, null);
		}
	}

	public static void sendMail(String to, String cc, String subject, String messageText) throws MessagingException {
		try {
			sendMailPriv(getDefaultMailCredentials(), to, cc, null, subject, messageText, false, null);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(getDefaultMailCredentials(), to, cc, null, subject, messageText, false, null);
		}
	}
	
//	private static void sendMailPriv(String to, String cc, String subject, String messageText) throws MessagingException {
//		try {
//			Transport transport = getTransport();
//
//			if (transport == null)
//				throw new MessagingException("Cannot initilize java mail transport");
//			
//			if (WeblibsConst.DEVELOPERS_MODE_REDIRECT_EMAILS != null) {
//				to = WeblibsConst.DEVELOPERS_MODE_REDIRECT_EMAILS;
//				if (cc != null)
//					cc = WeblibsConst.DEVELOPERS_MODE_REDIRECT_EMAILS;
//			}
//			
//			MimeMessage message = new MimeMessage(session);
//			message.setFrom(new InternetAddress(from));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//			if (nullOrEmpty(cc) == false)
//				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc)); // make a CC copy
//			message.setSubject(subject);
//			message.setText(messageText);
//			
//			transport.sendMessage(message, message.getAllRecipients());
//			
//			LibsLogger.debug(Mailer.class, String.format("Mail '%s' send to %s", subject, to));
//		} catch (MessagingException e) {
//			closeTransport();
//			throw e;
//		} catch (Exception e) {
//			closeTransport();
//			throw new MessagingException("Cannot send email. Some General Exception.", e);
//		}
//	}
	
	public static void sendMail(String to, String subject, String messageText, boolean isMessageInHtml) throws MessagingException {
		try {
			sendMailPriv(getDefaultMailCredentials(), to, null, null, subject, messageText, isMessageInHtml, null);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(getDefaultMailCredentials(), to, null, null, subject, messageText, isMessageInHtml, null);
		}
	}
	
	public static void sendMail(String to, String subject, String messageText, File attachement, boolean isMessageInHtml) throws MessagingException {
		ArrayList<Pair<? extends File, String>> attachements = new ArrayList<Pair<? extends File, String>>();
		attachements.add(new Pair<File, String>(attachement, attachement.getName()));
		try {
			sendMailPriv(getDefaultMailCredentials(), to, null, null, subject, messageText, isMessageInHtml, attachements);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(getDefaultMailCredentials(), to, null, null, subject, messageText, isMessageInHtml, attachements);
		}
	}
	
	public static void sendMail(String to, String cc, String bcc, String subject, String messageText, List<Pair<? extends File, String>> attachements) throws MessagingException {
		try {
			sendMailPriv(getDefaultMailCredentials(), to, cc, bcc, subject, messageText, false, attachements);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(getDefaultMailCredentials(), to, cc, bcc, subject, messageText, false, attachements);
		}
	}
	
	public static void sendMail(String to, String cc, String bcc, String subject, String messageText, boolean isMessageInHtml, List<Pair<? extends File, String>> attachements) throws MessagingException {
		try {
			sendMailPriv(getDefaultMailCredentials(), to, cc, bcc, subject, messageText, isMessageInHtml, attachements);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(getDefaultMailCredentials(), to, cc, bcc, subject, messageText, isMessageInHtml, attachements);
		}
	}
	
	public static void sendMail(MailCredentials credentials, String to, String cc, String bcc, String subject, String messageText, boolean isMessageInHtml, List<Pair<? extends File, String>> attachements) throws MessagingException {
		try {
			if (credentials == null)
				credentials = getDefaultMailCredentials();
			sendMailPriv(credentials, to, cc, bcc, subject, messageText, isMessageInHtml, attachements);
		} catch (MessagingException e) {
			LibsLogger.warn(Mailer.class, "Cannot send mail. Recreating mail transport", e);
			sendMailPriv(credentials, to, cc, bcc, subject, messageText, isMessageInHtml, attachements);
		}
	}
	
	private static void sendMailPriv(MailCredentials credentials, String to, String cc, String bcc, String subject, String messageText, 
			boolean isMessageInHtml, List<Pair<? extends File, String>> attachements) throws MessagingException {
//		Transport transport = credentials.getTransport();
		
//		if (transport == null)
//			throw new MessagingException("Cannot initilize java mail transport");
		
		LibsLogger.info(Mailer.class, "Preparing mail to ", to, ": ", subject);
			
		try {
			MimeMessage message = new MimeMessage(credentials.getSession());
			
			// from
			message.setFrom(new InternetAddress(credentials.getFrom()));
			
			// recipients
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			if (cc != null)
				message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
			if (bcc != null)
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
			
			// subject
			message.setSubject(subject);
			
			if (attachements != null && attachements.size() > 0) {
				// there are attachements
				Multipart multipart = new MimeMultipart();
				
				// create the message part
				MimeBodyPart messageBodyPart = new MimeBodyPart();
//				messageBodyPart.setHeader("Content-Type", "text/html");
				if (isMessageInHtml) {
//					messageBodyPart.addHeader("Content-Type", "text/html; charset=\"UTF-8\"");
					messageBodyPart.setContent(messageText, "text/html; charset=\"UTF-8\"");
				} else
					messageBodyPart.setText(messageText);
				multipart.addBodyPart(messageBodyPart);
	
				// attachments
				for (Pair<? extends File, String> pair : attachements) {				
					messageBodyPart = new MimeBodyPart();
					messageBodyPart.setDataHandler(new DataHandler(pair.getValue0().toURL()));
					messageBodyPart.setFileName(pair.getValue1());
					multipart.addBodyPart(messageBodyPart);
				}
				
				// put parts in message
//				message.saveChanges();
				message.setContent(multipart);
			} else {
				// there are NO attachements
				if (isMessageInHtml)
					message.setContent(messageText, "text/html; charset=\"UTF-8\"");
				else
					message.setText(messageText);
			}

			// send it
//			credentials.getSession();
			
			Transport.send(message, message.getAllRecipients());
			
			LibsLogger.debug(Mailer.class, format("Mail '%s' really sent to %s", subject, ArrayUtils.toString(message.getAllRecipients())));
		} catch (MalformedURLException e) {
			credentials.closeTransport();
			throw new MessagingException("URL is wrong", e);
		} catch (Exception e) {
			credentials.closeTransport();
			throw new MessagingException("Cannot send email. Some General Exception.", e);
		}
	}
	
	
}
