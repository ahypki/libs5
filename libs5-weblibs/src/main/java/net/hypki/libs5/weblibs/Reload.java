package net.hypki.libs5.weblibs;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.language.LanguageIds;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.hypki.libs5.weblibs.user.UserStatus;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Reload {
	

//	public static void reloadItems(JsonElement items) throws IOException, ValidationException {
//		Set<Entry<String, JsonElement>>	allItems = items.getAsJsonObject().entrySet();
//		
//		for (Entry<String, JsonElement> entry : allItems) {
//			try {
//				JsonObject obj = entry.getValue().getAsJsonObject();
//				
//				User user = User.getUser(new Email(obj.get("ownerEmail").getAsString()));
//							
//				Item item = null;
//				if (obj.get("uuid") != null)
//					item = Item.getById(user.getUserId(), new UUID(obj.get("uuid").getAsString()));
//				
//				if (item == null) {
//					item = new Item();
//					item.setId(obj.get("uuid") != null ? new UUID(obj.get("uuid").getAsString()) : UUID.random());
//					item.setCreateMs(System.currentTimeMillis());
//					item.setLastEdit(System.currentTimeMillis());
//				}
//				item.setTitle(entry.getKey());
//				item.setComment(obj.get("comment").getAsBoolean());
//				item.setLanguage(LanguageIds.valueOf(obj.get("lang").getAsString()));
//				item.setNewFieldsAllowed(obj.get("newFieldsAllowed").getAsBoolean());
//				item.setUserId(user.getUserId());
//				item.setPrivate(obj.get("private").getAsBoolean());
//				item.setPrivateChangeable(obj.get("privateChangeable").getAsBoolean());
//				item.setRemoveable(obj.get("removeable").getAsBoolean());
//				item.getTags().clear();
//				for (JsonElement tag : obj.get("tags").getAsJsonArray()) {				
//					item.getTags().add(tag.getAsString());
//				}
//				item.getFields().clear();
//				for (JsonElement fieldEntry : obj.get("fields").getAsJsonArray()) {
//					Class<? extends Field> clazz = (Class<? extends Field>) Class.forName(fieldEntry.getAsJsonObject().get("class").getAsString());
//					item.getFields().add(JsonUtils.fromJson(fieldEntry, clazz));
//				}
//				item.save();
//				
//				if (obj.get("property") != null) {
//					String key = obj.get("property").getAsJsonObject().get("key").getAsString();
//					Property prop = new Property();
//					prop.setLangId(LanguageIds.valueOf(obj.get("lang").getAsString()));
//					prop.setMsg(user.getUserId() + ":" + item.getId().toString());
//					prop.setPropKey(key);
//					prop.setSystem(obj.get("property").getAsJsonObject().get("system").getAsBoolean());
//					prop.save();
//				}
//			} catch (ClassNotFoundException e) {
//				LibsLogger.error(Reload.class, "Cannot find class", e);
//			}
//		}
//	}

	public static void reloadAllowed(JsonElement allowedRights) throws IOException, ValidationException {
		JsonArray allItems = allowedRights.getAsJsonArray();
		
		for (JsonElement entry : allItems) {
			JsonObject obj = entry.getAsJsonObject();
			
			User user = null;
			
			// searching for the user
			if (JsonUtils.readString(obj, "userUUID/id") != null) {
				user = UserFactory.getUser(new UUID(JsonUtils.readString(obj, "userUUID/id")));
			} else if (JsonUtils.readString(obj, "userUUID") != null) {
				user = UserFactory.getUser(new UUID(JsonUtils.readString(obj, "userUUID")));
			} else {
				user = User.getUser(new Email(JsonUtils.readString(obj, "userEmail")));
			}
			
			if (user == null) {
				LibsLogger.error(Reload.class, "No user found for the entry " + obj.toString());
				continue;
			}
						
			Allowed allowed = new Allowed();
			allowed.setRight(obj.get("allowedRight").getAsString());
			allowed.setUserUUID(user.getUserId());
			allowed.setAllowed(obj.get("allowed").getAsBoolean());
			allowed.save();
			
//			Allowed allowed = CassandraObject.getGson().fromJson(obj, Allowed.class);
			
			
			
//			allowed.save();
			LibsLogger.debug(Reload.class, "User " + user.getEmail().toString() + ", right " + allowed.getRight(), ": ", allowed.isAllowed());
		}
	}

	public static void reloadProperties(JsonElement propertiesPaths) throws IOException, ValidationException {
		Set<Entry<String, JsonElement>>	allItems = propertiesPaths.getAsJsonObject().entrySet();
		
		for (Entry<String, JsonElement> entry : allItems) {
			JsonObject obj = entry.getValue().getAsJsonObject();
			
			Property prop = new Property();
			prop.setPropKey(entry.getKey());
			prop.setLangId(LanguageIds.valueOf(obj.get("lang").getAsString()));
			prop.setMsg(obj.get("value").getAsString());
//			prop.setSystem(obj.get("isSystem").getAsBoolean());
			prop.save();
		}
	}

	public static void reloadUsers(JsonElement users) throws IOException, ValidationException {
		Set<Entry<String, JsonElement>>	allEntries = users.getAsJsonObject().entrySet();
		
		HashSet<String> usersDone = new HashSet<String>();
		
		for (Entry<String, JsonElement> entry : allEntries) {
			JsonObject obj = entry.getValue().getAsJsonObject();
			
			Email email = new Email(entry.getKey());
			
			if (usersDone.contains(email.toString())) {
				throw new IOException("User with email " + email.toString() + " was already reloaded");
			}
			
			User user = User.getUser(email);
			if (user != null) {
				if (usersDone.contains(user.getUserId().toString())) {
					throw new IOException("User with id " + user.getUserId().toString() + " was already reloaded");
				}
				user.remove();
			}
			
			user = createUser(email, obj);
			
			usersDone.add(email.toString());
			usersDone.add(user.getUserId().toString());
		}
	}

	public static void reloadLanguageProperties(JsonElement settings) throws IOException, ValidationException {
//		Reader reader = new FileReader(new File(langJsonFile));
//		JsonElement settings = new JsonParser().parse(reader);
		
		Set<Entry<String, JsonElement>>	allEntries = settings.getAsJsonObject().entrySet();
		
		for (Entry<String, JsonElement> entry : allEntries) {
			String field = entry.getKey();
			LanguageIds langId = LanguageIds.valueOf(field.substring(0, 2));
			String key = field.substring(3);
			
			Property lang = new Property();
			lang.setPropKey(key);
			lang.setMsg(entry.getValue().getAsString());
			lang.setLangId(langId);			
			lang.save();
		}
	}

	public static void createMissingUsers(JsonElement usersJson) throws ValidationException, IOException {
		Set<Entry<String, JsonElement>>	allEntries = usersJson.getAsJsonObject().entrySet();
	
		for (Entry<String, JsonElement> entry : allEntries) {
			JsonObject oneUserJson = entry.getValue().getAsJsonObject();
						
			Email email = new Email(entry.getKey());
			User user = User.getUser(email);
			if (user != null)
				continue;
			
			createUser(email, oneUserJson);
		}
	}
	
	public static User createUser(Email email, JsonObject oneUserJson) throws ValidationException, IOException {
		User user = new User();

		user.setEmail(email);
		user.setLogin(oneUserJson.get("login").getAsString());
		user.setCreationDate(System.currentTimeMillis());
		
		if (oneUserJson.get("passPlain") != null)
			user.setPlainPasswordForHashing(oneUserJson.get("passPlain").getAsString());
		else if (oneUserJson.get("pass") != null)
			user.setPasswordHash(oneUserJson.get("pass").getAsString());
		
		if (oneUserJson.get("uuid") != null)
			user.setUserUUID(oneUserJson.get("uuid").getAsString());
		else
			user.setUserUUID(UUID.random());
		user.save();
		
		String statusStr = oneUserJson.get("status") != null ? oneUserJson.get("status").getAsString() : "UNCONFIRMED";
		if (UserStatus.NORMAL.toString().toLowerCase().equalsIgnoreCase(statusStr))
			user.setStatus(UserStatus.NORMAL);
		else if (UserStatus.BLOCKED.toString().toLowerCase().equalsIgnoreCase(statusStr))
			user.setStatus(UserStatus.BLOCKED);
		else if (UserStatus.REMOVED.toString().toLowerCase().equalsIgnoreCase(statusStr))
			user.setStatus(UserStatus.REMOVED);
		else if (UserStatus.UNCONFIRMED.toString().toLowerCase().equalsIgnoreCase(statusStr))
			user.setStatus(UserStatus.UNCONFIRMED);
		else
			user.setStatus(UserStatus.NEW);
		
		if (user.getStatus() != UserStatus.NEW)
			user.save();
		return user;
	}
}
