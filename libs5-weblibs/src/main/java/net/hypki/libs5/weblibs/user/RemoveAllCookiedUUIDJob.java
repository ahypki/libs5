package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class RemoveAllCookiedUUIDJob extends Job {
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userId = null;

	public RemoveAllCookiedUUIDJob() {
		
	}
	
	public RemoveAllCookiedUUIDJob(UUID userId) {
		setUserId(userId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		for (CookieUUID c : CookieUUIDFactory.iterateCookieUUID(getUserId())) {
			c.remove();
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
