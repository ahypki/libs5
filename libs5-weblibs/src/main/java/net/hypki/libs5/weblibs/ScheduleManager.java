package net.hypki.libs5.weblibs;

import static org.quartz.DateBuilder.evenMinuteDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.IModule;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.url.Params;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class ScheduleManager {
	
	private static Scheduler scheduler = null;

	private ScheduleManager() {
		
	}
	
	private static Scheduler getScheduler() throws IOException {
		if (scheduler == null) {
			try {
				long start = System.currentTimeMillis();
				
				// First we must get a reference to a scheduler
				SchedulerFactory sf = new StdSchedulerFactory();
				scheduler = sf.getScheduler();
				
				// Start up the scheduler (nothing can actually run until the 
				// scheduler has been started)
				scheduler.start();
				
				// register shutdown method
				LibsInit.addToRunningModules(new IModule() {
					@Override
					public void shutdown() {						
						try {
							// shuting down the scheduler
							scheduler.shutdown(true);
							
							LibsLogger.debug(getClass(), "Scheduler shutdown successfully");
						} catch (SchedulerException e) {
							LibsLogger.error(getClass(), "Cannot start scheduler", e);
						}
					}
				});
				
				LibsLogger.debug("Schedule manager started in " + (System.currentTimeMillis() - start));
			} catch (SchedulerException e) {
				throw new RuntimeException("Cannot start scheduler", e);
			}
		}
		return scheduler;
	}
	
	public static void stop(JobKey jobKey) throws SchedulerException, IOException {
		getScheduler().deleteJob(jobKey);
	}
	
	public static Iterable<JobDetail> iterateJobs() {
		return new Iterable<JobDetail>() {
			@Override
			public Iterator<JobDetail> iterator() {
				return new Iterator<JobDetail>() {
					private boolean initiated = false;
					private Iterator<JobKey> keys = null;
					private Iterator<String> groupNames = null;
					
					private void init() {
						try {
							groupNames = ScheduleManager.getScheduler().getJobGroupNames().iterator();
							
							nextKeys();
							
							initiated = true;
						} catch (SchedulerException | IOException e) {
							throw new RuntimeException("Cannot create iterator for Jobs in SchedulerManager");
						}
					}
					
					private void nextKeys() {
						while (groupNames.hasNext()) {
							String groupName = groupNames.next();
							
							try {
								keys = ScheduleManager.getScheduler().getJobKeys(org.quartz.impl.matchers.GroupMatcher.groupEquals(groupName)).iterator();
								
								if (keys.hasNext())
									return;
							} catch (SchedulerException | IOException e) {
								LibsLogger.error(ScheduleManager.class, "Cannot check for keys for group " + groupName, e);
							}
						}
					}
					
					@Override
					public JobDetail next() {
						if (initiated == false)
							init();
						
						try {
							JobKey k = keys.next();
							return ScheduleManager.getScheduler().getJobDetail(k);
						} catch (SchedulerException | IOException e) {
							LibsLogger.error(ScheduleManager.class, "Cannot get more keys from ScheduleManager", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (initiated == false)
							init();
						
						if (keys != null && keys.hasNext())
							return true;
						
						nextKeys();
						
						if (keys != null && keys.hasNext())
							return true;
						
						return false;
					}
				};
			}
		};
	}
	
	public static void schedule(Class<? extends Job> jobClass, int inXMinutes, Params params) throws IOException {
		try {
			// compute a time that is on the next round minute
			Date runTime = evenMinuteDate(new Date());

			LibsLogger.debug("------- Scheduling Job  -------------------");
			
			final UUID id = UUID.random();

			// define the job and tie it to our Job class
			JobBuilder jobBuilder = newJob(jobClass)
			    .withIdentity("job_" + id.getId(), "group1");
			
			// adding params if needed
			if (params != null) {
				for (String key : params.keys()) {
					if (params.get(key) instanceof String)
						jobBuilder.usingJobData(key, (String) params.get(key));
					else if (params.get(key) instanceof Double)
						jobBuilder.usingJobData(key, (Double) params.get(key));
					else if (params.get(key) instanceof Integer)
						jobBuilder.usingJobData(key, (Integer) params.get(key));
					else if (params.get(key) instanceof Long)
						jobBuilder.usingJobData(key, (Long) params.get(key));
					else
						throw new IOException("Unimplemented case for " + params.get(key));
				}
			}
			
			JobDetail job = jobBuilder.build();
			
			// Trigger the job to run on the next round minute
			Trigger trigger = newTrigger()
			    .withIdentity("trigger_" + id.getId(), "group1")
			    .startAt(runTime)
			    .build();
			
			// Tell quartz to schedule the job using our trigger
			getScheduler().scheduleJob(job, trigger);
			LibsLogger.debug(job.getKey() + " will run at: " + runTime);
		} catch (SchedulerException e) {
			throw new IOException("Cannot add job to scheduler", e);
		}
	}
	
	public static void schedule(Class<? extends Job> jobClass, int inXMinutes) throws IOException {
		schedule(jobClass, inXMinutes, null);
	}
	
	public static void scheduleDaily(Class<? extends Job> jobClass, int hour, int minute) throws IOException {
		try {
			LibsLogger.debug("------- Scheduling Job  -------------------");
			
			final UUID id = UUID.random();

			// define the job and tie it to our Job class
			JobDetail job = newJob(jobClass)
			    .withIdentity("job_" + id.getId(), "group1")
			    .build();
			
			// Trigger the job to run on the next round minute
			CronTrigger trigger = newTrigger()
			    .withIdentity("trigger_" + id.getId(), "group1")
//			    .withSchedule(SimpleScheduleBuilder.)
			    .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(hour, minute))
			    .build();
			
			// Tell quartz to schedule the job using our trigger
			getScheduler().scheduleJob(job, trigger);
			LibsLogger.debug(job.getKey() + " will run all at " + hour + ":" + minute);
		} catch (SchedulerException e) {
			throw new IOException("Cannot add job to scheduler", e);
		}
	}
	
	public static void scheduleRecursively(Class<? extends Job> jobClass, int inEachXMinutes, Params params) throws IOException {
		try {
			LibsLogger.debug("------- Scheduling Job  -------------------");

			final UUID id = UUID.random();

			// define the job and tie it to our Job class
			JobBuilder jobBuilder = newJob(jobClass)
			    .withIdentity("job_" + id.getId(), "group1");

			// adding params if needed
			if (params != null) {
				for (String key : params.keys()) {
					if (params.get(key) instanceof String)
						jobBuilder.usingJobData(key, (String) params.get(key));
					else if (params.get(key) instanceof Double)
						jobBuilder.usingJobData(key, (Double) params.get(key));
					else if (params.get(key) instanceof Integer)
						jobBuilder.usingJobData(key, (Integer) params.get(key));
					else if (params.get(key) instanceof Long)
						jobBuilder.usingJobData(key, (Long) params.get(key));
					else
						throw new IOException("Unimplemented case for " + params.get(key));
				}
			}
			
			JobDetail job = jobBuilder.build();
			
			// Trigger the job to run on the next round minute
			Trigger trigger = newTrigger()
				.startAt(new Date(SimpleDate.now().addHours(inEachXMinutes / 60).getTimeInMillis()))
			    .withIdentity("trigger_" + id.getId(), "group1")
			    .withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(inEachXMinutes))// CronScheduleBuilder.cronSchedule("0 0/" + inEachXMinutes + " * * * ?"))
			    .build();
			
			// Tell quartz to schedule the job using our trigger
			getScheduler().scheduleJob(job, trigger);
			LibsLogger.debug(job.getKey() + " will run in each " + inEachXMinutes + " minutes");
		} catch (SchedulerException e) {
			throw new IOException("Cannot add job to scheduler", e);
		} 
//		catch (ParseException e) {
//			throw new IOException("Cannot add job to scheduler", e);
//		}
	}
	
	public static void scheduleRecursively(Class<? extends Job> jobClass, int inEachXMinutes) throws IOException {
		scheduleRecursively(jobClass, inEachXMinutes, null);
	}

	public static SimpleDate getNextFireAlarm(JobDetail jobDetail) {
		try {
			List<? extends Trigger> triggerList = getScheduler().getTriggersOfJob(jobDetail.getKey());
			return triggerList != null && triggerList.size() > 0 ? new SimpleDate(triggerList.get(0).getNextFireTime().getTime()) : null;
		} catch (SchedulerException | IOException e) {
			LibsLogger.error(ScheduleManager.class, "Cannot find out what is the next trigger time for the job " + jobDetail.getKey().getName(), e);
			return null;
		}
	}
}
