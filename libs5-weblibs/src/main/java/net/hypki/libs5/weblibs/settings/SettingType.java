package net.hypki.libs5.weblibs.settings;

public enum SettingType {
	STRING,
	MULTILINE,
	INTEGER,
	FLOAT,
	DOUBLE,
	BOOLEAN,
	DATE,
	PASS
}
