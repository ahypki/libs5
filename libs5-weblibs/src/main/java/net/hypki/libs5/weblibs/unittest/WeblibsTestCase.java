package net.hypki.libs5.weblibs.unittest;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.db.weblibs.db.DbTestCase;
import net.hypki.libs5.utils.guid.RandomGUID;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.User;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;

@Ignore
public class WeblibsTestCase extends DbTestCase {
	
	private static boolean init = false;
	
	public WeblibsTestCase() {
//		init();
	}
	
	private static void init() {
		if (init == false) {
			init = true;
			
			Settings.init(new String[] {"--settings", "settingsUnitTests.json", 
					"--db", "cassandra", "--verbose"});
			
//			CassandraProvider cassProv = new CassandraProvider();
			
//			DbObject.init(cassProv);
			
			getChannelNames().add(WeblibsConst.CHANNEL_NORMAL);
		}
	}
	
	@BeforeClass
	public static void beforeUnitTest() throws IOException, ValidationException {
		
	}
	
	
	@AfterClass
	public static void afterUnitTest() throws IOException, ValidationException {
		// run all pending jobs
		JobsManager.runAllJobs(getChannelNames());
	}
	
	protected void runAllJobs() throws IOException, ValidationException {
		JobsManager.runAllJobs(getChannelNames());
	}
	
	protected void runAllJobs(String[] additionalChannelNames) throws IOException, ValidationException {
		for (String channelName : additionalChannelNames) {
			getChannelNames().add(channelName);
		}
		JobsManager.runAllJobs(getChannelNames());
	}
	
	protected void runAllJobs(int sleepMs) throws IOException, ValidationException {
		JobsManager.runAllJobs(getChannelNames());
		takeANap(sleepMs);
	}
	
	protected void runAllJobs(int sleepMs, int sleepBetweenJobsMs) throws IOException, ValidationException {
		JobsManager.runAllJobs(getChannelNames(), sleepBetweenJobsMs);
		takeANap(sleepMs);
	}
	
	protected long getJobsCount() {
		return JobsManager.getJobsCount(getChannelNames());
	}
	
	protected User createTestUser(String login) throws IOException, ValidationException {		
		User user = new User();
		user.setEmail(RandomGUID.getRandomGUID().split("-")[0] + "@blablabla.net");
		user.setLogin(login);
		user.setUserUUID(RandomGUID.getRawRandomGUID());
		user.setPasswordHash("test-pass-hashed");
		user.save();
		addToRemove(user);
		return user;
	}
	
	protected User testUser() throws IOException, ValidationException {
		return createTestUser(RandomGUID.getRandomGUID().split("-")[0]);
	}
	
}