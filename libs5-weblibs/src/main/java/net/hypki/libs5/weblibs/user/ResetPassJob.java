package net.hypki.libs5.weblibs.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.LocaleUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.mail.Mailer;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import org.antlr.stringtemplate.StringTemplate;

import com.google.gson.annotations.Expose;

public class ResetPassJob extends Job {
	
	@NotNull
	@AssertValid
	@Expose
	private UUID userId = null;
	
	public ResetPassJob() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}

	public ResetPassJob(UUID userId) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setUserId(userId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		User user = UserFactory.getUser(userId);
		
		if (user != null) {
			try {
				String newPass = UUID.random().toString().toLowerCase().substring(1, 10);
				
				if (user.getStatus() == UserStatus.UNCONFIRMED)
					user.setStatus(UserStatus.NORMAL);
				else if (user.getStatus() == UserStatus.BLOCKED) {
					// do nothing, consume the job
					return;
				}
				
				user.setPlainPasswordForHashing(newPass);
				user.save();
								
				StringTemplate template = new StringTemplate(SystemUtils.readFileContent("newPassMailTemplate.html"));
				Map<String, String> params = new HashMap<String, String>();
				params.put("pass", newPass);
				template.setAttributes(params);
				
				StringTemplate templateSubject = new StringTemplate(LocaleUtils.getMsg(getClass(), "en", "MailSubject", "New password"));
				Map<String, String> paramsSubject = new HashMap<String, String>();
				paramsSubject.put("domain", WeblibsConst.DOMAIN);
				templateSubject.setAttributes(paramsSubject);
				
				ResetPassUUID resetPass = ResetPassUUID.getResetPassId(userId);
				
				if (resetPass == null) {
					LibsLogger.error(ResetPassJob.class, "Cannot reset password, cannot find reset ID. Consuming job...");
					return;
				}
				
				Mailer.sendMail(resetPass.getMailCredentials(), user.getEmail().toString(), null, null, 
						templateSubject.toString(), template.toString(), true, null);
				
				resetPass.remove();
				
				LibsLogger.debug(getClass(), "New password sent to user " + user.getEmail());
			} catch (ValidationException | MessagingException e) {
				LibsLogger.error(getClass(), "Cannot send email with new password to the user " + user.getEmail(), e);
			}
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
