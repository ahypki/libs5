package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Time;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.mail.MailCredentials;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ResetPassUUID extends WeblibsObject<ResetPassUUID> {
	
	public static final String COLUMN_FAMILY = "resetpass";
	
	@NotNull
	@AssertValid
	@Expose
	private UUID userId = null;
	
	@NotNull
	@AssertValid
	@Expose
	private UUID resetId = null;
	
	@NotNull
	@AssertValid
	@Min(value = 1)
	@Expose
	private long validUntilMs = 0;
	
	@Expose
//	@NotNull
	@AssertValid
	private MailCredentials mailCredentials = null;
	
	@Expose
//	@NotNull
	@AssertValid
	private String mailSubject = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String domain = null;
	
	public ResetPassUUID() {
		setValidUntilMs(new Time().addHours(2).getMs());
	}

	public ResetPassUUID(UUID userId, MailCredentials mc) {
		setUserId(userId);
		setResetId(UUID.random());
		setValidUntilMs(new Time().addHours(2).getMs());
		setMailCredentials(mc);
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getUserId().toString();
	}
	
	public boolean isValid(UUID resetId) {
		if (resetId != null
				&& resetId.equals(getResetId())
				&& SimpleDate.now().isBefore(new SimpleDate(getValidUntilMs())))
			return true;
		return false;
	}
	
	public static ResetPassUUID getResetPassId(UUID userId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, userId.getId())), 
				DbObject.COLUMN_DATA, 
				ResetPassUUID.class);
	}
	
	@Override
	public MutationList getSaveMutations() {
		return super.getSaveMutations()
				.addInsertMutations(getMailCredentials() != null ?
						new ResetPassRequestedJob(this)
							.setMailSubject(getMailSubject())
							.setDomain(getDomain()): null);
	}
	
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setResetId(UUID resetId) {
		this.resetId = resetId;
	}

	public UUID getResetId() {
		return resetId;
	}

	public long getValidUntilMs() {
		return validUntilMs;
	}

	public void setValidUntilMs(long validUntilMs) {
		this.validUntilMs = validUntilMs;
	}

	public MailCredentials getMailCredentials() {
		return mailCredentials;
	}

	public void setMailCredentials(MailCredentials mailCredentials) {
		this.mailCredentials = mailCredentials;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public ResetPassUUID setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
		return this;
	}

	public String getDomain() {
		return domain;
	}

	public ResetPassUUID setDomain(String domain) {
		this.domain = domain;
		return this;
	}
}
