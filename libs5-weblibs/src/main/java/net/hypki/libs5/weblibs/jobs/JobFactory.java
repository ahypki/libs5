package net.hypki.libs5.weblibs.jobs;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Order;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.search.IndexJob;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JobFactory {
	
	public static Iterable<Job> iterateJobs() {

		return new Iterable<Job>() {
			@Override
			public Iterator<Job> iterator() {
				return new Iterator<Job>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private Job nextJob = null;
					
					@Override
					public Job next() {
						if (!initiated)
							hasNext();
						return nextJob;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY, 
									new String[]{DbObject.COLUMN_DATA}, null);
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row != null && row.contains(DbObject.COLUMN_DATA)) {
								nextJob = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), Job.class);
								
								return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}

	public static List<Job> getOldestJobs(String channel, int count, long fromMilisec) throws IOException {
//		LibsLogger.info(JobFactory.class, "Channel ", channel, " count ", getJobsCount(channel));
		
		Watch watch = new Watch();

		int jobsCounter = 0;
		int missedHits = 0;
		List<Job> results = new ArrayList<>();
		Job last = null;
		
		Results oldestJobs = DbObject.getDatabaseProvider().getList(WeblibsConst.KEYSPACE, 
				Job.COLUMN_FAMILY, 
				new String[] {DbObject.COLUMN_DATA}, 
				new Order(Job.COLUMN_TIME, true), 
				count, 
				null, 
				new C3Where()
					.addClause(new C3Clause(Job.COLUMN_CHANNEL, ClauseType.EQ, channel))
					.addClause(new C3Clause(Job.COLUMN_TIME, ClauseType.GE, fromMilisec))
					.addClause(new C3Clause(Job.COLUMN_TIME, ClauseType.LT, System.currentTimeMillis()))
					);
		
		for (net.hypki.libs5.db.db.Row row : oldestJobs) {
		
			if (row == null)
				continue;
			
			try {
				String tmp = row.getAsString(DbObject.COLUMN_DATA);
				if (notEmpty(tmp)) {
					JsonElement json = JsonUtils.parseJson(tmp);
					
					String clazzToCast = JsonUtils.readString(json, "clazz");
					
					// TODO DEBUG 
					if (clazzToCast.equals("net.beanscode.model.search.IndexJob"))
						clazzToCast = IndexJob.class.getName();
					
					if (notEmpty(clazzToCast)) {
		//				LibsLogger.debug(Job.class, "Class to cast ", clazzToCast);
						Job job = null;
						
						try {
							job = (Job) JsonUtils.fromJson(json, Class.forName(clazzToCast));
						} catch (ClassNotFoundException e) {
							LibsLogger.error(JobFactory.class, "Cannot find class " + clazzToCast + ", consuming Job..", e);
							
							LibsLogger.debug(JobFactory.class, "Removing manually job " + json);
							
							// removing by building the key manually
							JsonObject jo = (JsonObject) json;
							String channel_ = jo.get("channel").getAsString();
							String key = Job.buildKey(jo.get("channel").getAsString(), 
									jo.get("time").getAsLong(), 
									jo.get("id").getAsJsonObject().get("id").getAsString());
							DbObject
								.getDatabaseProvider()
								.remove(WeblibsConst.KEYSPACE_LOWERCASE, 
										Job.COLUMN_FAMILY,
										new C3Where()
											.addClause(new C3Clause(Job.COLUMN_CHANNEL, ClauseType.EQ, channel_))
											.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, key))
										);
							continue;
						}
						
						if (last != null && job.getCreationMs() < last.getCreationMs()) {
							LibsLogger.error(JobFactory.class, "Problem with Jobs sorting by date");
						}
						last = job;
						
		//				if (job.getCreationMs() < watch.getStart())
						// TODO OPTY
						if (job.getChannel().equals(channel)) {
							if (job.getCreationMs() >= fromMilisec) {
								results.add(job);
						
								if (++jobsCounter >= count)
									break;
							}
						} else {
							if (++missedHits > (count * 3))
								break;
						}
					}
				} else {
//					DbObject.getDatabaseProvider().remove(WeblibsConst.KEYSPACE, Job.COLUMN_FAMILY, 
//							new C3Where()
//								.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, row.getPk().toString()))
//							);
//					LibsLogger.debug(Job.class, "Removed empty row in Job table ", row);
				}
			} catch (Exception e) {
				LibsLogger.error(JobFactory.class, "Cannot deserialize Job", e);
			}
		
//		    if (--remaining == 0)
//		        break;
		}
		
		if (results.size() > 0)
			LibsLogger.debug(JobFactory.class, "Getting ", results.size(), " jobs for channel ", channel, " (with iterator) in ", watch);
		
		return results;
	}

	public static long getJobsCount(String channel) throws IOException {
		return DbObject.getDatabaseProvider().countRows(WeblibsConst.KEYSPACE, 
				Job.COLUMN_FAMILY, 
				new C3Where()
					.addClause(new C3Clause(Job.COLUMN_CHANNEL, ClauseType.EQ, channel)));
	}
	
	public static Job getJob(String key) throws IOException {
		Object[] splits = Job.splitKey(key);
		return getJobData((String) splits[0], (Long) splits[1], new UUID((String) splits[2]));
	}

	public static Job getJobData(String channel, long creationDate, UUID jobId) throws IOException {
			return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
					Job.COLUMN_FAMILY,
					new C3Where()
						.addClause(new C3Clause(Job.COLUMN_CHANNEL, ClauseType.EQ, channel))
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, Job.buildKey(channel, creationDate, jobId.getId()))),
	//					.addClause(new C3Clause(COLUMN_TIME, ClauseType.EQ, createTimeUUID(creationDate, jobId))), 
					DbObject.COLUMN_DATA, 
					Job.class);
		}

}
