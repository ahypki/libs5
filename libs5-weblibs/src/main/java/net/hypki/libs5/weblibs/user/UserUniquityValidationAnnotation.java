package net.hypki.libs5.weblibs.user;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import net.sf.oval.configuration.annotation.Constraint;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Constraint(checkWith = UserUniquityValidator.class)
public @interface UserUniquityValidationAnnotation {
	String errorKey() default "ErrorCode.UserExists";
	String message() default "ErrorCode.UserExists";
}
