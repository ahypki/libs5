package net.hypki.libs5.weblibs;

import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.JsonElement;
import com.hazelcast.config.Config;
import com.hazelcast.config.SerializerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.nio.serialization.Serializer;

public class HazelcastManager {
	
	private static HazelcastInstance hazelcastInstance = null;
	
	private static Config config = null;
	
	static {
		
		
		
//		SerializerConfig sc = new SerializerConfig().
//		        setImplementation(new SimpleDateSerializer()).
//		        setTypeClass(SimpleDate.class);
//		
//		config.getSerializationConfig().addSerializerConfig(sc);
//		
//		sc = new SerializerConfig().
//		        setImplementation(new JDateTimeSerializer()).
//		        setTypeClass(JDateTime.class);
//		
//		config.getSerializationConfig().addSerializerConfig(sc);
		
//		SerializerConfig sc = new SerializerConfig().setImplementation(new ClearTableJobSerializer()).setTypeClass(
//				ClearTableJob.class);
//		HazelcastManager.getInstance().getConfig().getSerializationConfig().addSerializerConfig(sc);
		
	}
	
	private synchronized static Config getConfig() {
		if (config == null) {
			config = new Config();
			config.setProperty("hazelcast.logging.type", "log4j");
			config.setProperty("hazelcast.logging.level", "WARN");
//			config.setProperty("hazelcast.initial.min.cluster.size", "3");
			
			try {
				JsonElement hazelcast = JsonUtils.readJsonElement(Settings.getSettings(), "Hazelcast/Serializers");
				
				if (hazelcast != null) {
					for (JsonElement oneSerializer : hazelcast.getAsJsonArray()) {						
						SerializerConfig sc = new SerializerConfig().
								setImplementation((Serializer) Class.forName(oneSerializer.getAsJsonObject().get("impl").getAsString()).newInstance()).
								setTypeClass(Class.forName(oneSerializer.getAsJsonObject().get("type").getAsString()));
						
						config.getSerializationConfig(). addSerializerConfig(sc);
					}
				}
				
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				throw new RuntimeException("Cannot register Hazelcast serializers", e);
			}
		}
		return config;
	}
	
	public static void resetInstance() {
		hazelcastInstance = null;
	}

	public static HazelcastInstance getInstance() {		
		if (hazelcastInstance == null) {
			hazelcastInstance = Hazelcast.newHazelcastInstance(getConfig());
		}
		return hazelcastInstance;
	}
//
//	public static void addSerializerConfig(SerializerConfig sc) {
////		getConfig().getSerializationConfig().addSerializerConfig(sc);
////		hazelcastInstance.getConfig().getSerializationConfig().addSerializerConfig(sc);
////		shutdown();
////		hazelcastInstance = null;
////		hazelcastInstance = getInstance();
//	}
}
