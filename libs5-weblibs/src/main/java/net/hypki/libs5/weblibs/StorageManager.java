package net.hypki.libs5.weblibs;

import java.io.IOException;
import java.io.InputStream;

import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.weblibs.Settings;
import net.hypki.libs5.storage.StorageProvider;
import net.hypki.libs5.utils.IModule;
import net.hypki.libs5.utils.LibsInit;
import net.hypki.libs5.utils.json.JsonUtils;


public class StorageManager {
	
	private static StorageProvider storageProvider = null;

	private StorageManager() {
		
	}
	
	private static StorageProvider storageInstance() {		
		if (storageProvider == null) {
			try {
				storageProvider = (StorageProvider) Class.forName(JsonUtils.readString(Settings.getSettings(), "StorageProvider/class")).newInstance();
				
				LibsInit.addToRunningModules(new IModule() {
					
					@Override
					public void shutdown() {
						if (storageProvider != null) {
							storageProvider.shutdown();
						}
					}
				});
			} catch (InstantiationException e) {
				throw new RuntimeException("Cannot create instance of StorageProvider", e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException("Cannot create instance of StorageProvider", e);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Cannot create instance of StorageProvider", e);
			}
		}
		return storageProvider;
	}
//	
//	public static void save(UUID fileId, WeblibsPath path, byte[] blobdata) throws IOException {
//		if (path.isFile() == false)
//			throw new IOException("Weblibs describes no file, cannot save data to storage");
//		if (blobdata == null)
//			throw new IOException("Blob data cannot be null");
//		storageInstance().saveFile(userId, path, blobdata);
//	}
//	
	public static void save(String set, UUID fileId, InputStream dataStream) throws IOException {
		if (dataStream == null)
			throw new IOException("Blob data cannot be null");
		storageInstance().save(set, fileId, dataStream, null);
	}
	
	public static boolean remove(String set, UUID fileId) throws IOException {
		return storageInstance().remove(set, fileId);
	}
	
	public static InputStream read(String set, UUID fileId) throws IOException {
		return storageInstance().read(set, fileId);
	}
}
