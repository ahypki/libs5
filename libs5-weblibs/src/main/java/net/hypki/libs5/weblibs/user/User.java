package net.hypki.libs5.weblibs.user;

import static net.hypki.libs5.db.weblibs.Settings.getSettings;
import static net.hypki.libs5.utils.json.JsonUtils.readBoolean;

import java.io.FileNotFoundException;
import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.encryption.HashUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.Reload;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.search.IndexUserOperation;
import net.hypki.libs5.weblibs.search.RemoveFromIndex;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

/**
 * Cassandra: Keyspace.User[{userEmail}].{appId}."data"
 * @author ahypki
 *
 */
@UserUniquityValidationAnnotation
public class User extends WeblibsObject<User> {
	
	public static final String COLUMN_FAMILY = "user";
		
	static {
//		init();
	} 
	
//	public static void init() {
//		init(false);
//	}
	
	public static void init(boolean force) {
		// check if one has to recreating users
		try {
			boolean recreate = readBoolean(getSettings(), "Users/Recreate", false);
			boolean create = readBoolean(getSettings(), "Users/Create", false);
			
			if (recreate || create || force) {
				if (DbObject.getDatabaseProvider().getTableSchema(WeblibsConst.KEYSPACE, COLUMN_FAMILY) != null) {
					String usersContent = SystemUtils.readFileContent("User.json");
					if (usersContent == null)
						throw new RuntimeException("Cannot find User.json");
					
					if (recreate || force)
						Reload.reloadUsers(JsonUtils.parseJson(usersContent));
					else if (create)
						Reload.createMissingUsers(JsonUtils.parseJson(usersContent));
				}
			}
		} catch (FileNotFoundException e) {
			LibsLogger.error(WeblibsConst.class, "Cannot find User.json file", e);
		} catch (IOException e) {
			LibsLogger.error(WeblibsConst.class, "Cannot find User.json file", e);
		} catch (ValidationException e) {
			LibsLogger.error(WeblibsConst.class, "Cannot create users", e);
		}
	}
		
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userUUID = null;
	
	@NotNull
	@NotEmpty
//	@AssertValid
	@Expose
	private Email email = null;
	
	@NotNull
	@NotEmpty
	@Length(min = 6, max = 256, message="Login.PassToShort")
	@Expose
	private String passwordHash = null;
	
	@NotNull
	@NotEmpty
	@Length(min = 1, max = 256)
	@Expose
	private String login = null;
	
	@Min(value = 1)
	@Expose
	private long creationDate = 0;
	
	@Expose
	private UserStatus status = UserStatus.NEW;
	
	private boolean alreadyStatusNormal = false;
	
	public User() {
		setCreationDate(System.currentTimeMillis());
	}
	
	@Override
	public String toString() {
		return String.format("%s [%s, %s]", getEmail(), getStatus(), getUserId());
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getEmail().toString();
	}
	
	@Override
	public TableSchema getTableSchema() {
		return new TableSchema(COLUMN_FAMILY)
				.addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true)
				.addColumn(DbObject.COLUMN_DATA, ColumnType.STRING)
				;
	}
	
	@Override
	public User save() throws ValidationException, IOException {
		super.save();

		try {
			index();
			
			UserFactory.updateCache(this);
		} catch (Throwable e) {
			LibsLogger.error(User.class, "Cannot ad-hoc add to index", e);
		}
		
		return this;
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
		
		try {
			SearchManager.removeObject(getCombinedKey(), getColumnFamily());
		} catch (Throwable e) {
			LibsLogger.error(User.class, "Cannot ad-hoc remove from index", e);
		}
	}
	
	@Override
	public void additionalValidation() throws ValidationException {
		super.additionalValidation();
		
		if (!getEmail().getEmail().equalsIgnoreCase("admin"))
			AssertUtils.assertTrue(getEmail().isValid(), "Email is not valid");
	}

	public void setEmail(String email) {
		this.email = new Email(email);
	}
	
	public void setEmail(Email email) {
		this.email = email;
	}

	public Email getEmail() {
		return email;
	}
	
	public void setPlainPasswordForHashing(String plainPassword) {
		setPasswordHash(HashUtils.getBlowfishHash(plainPassword));
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getPasswordHash() {
		return passwordHash;
	}
	
	public boolean isPasswordCorrect(String plainPass) {
		return HashUtils.checkBlowfishHash(plainPass, getPasswordHash());
	}
	
	public String setRandomPassword() {
		String tmp = UUID.random().toString().substring(0, 10);
		setPlainPasswordForHashing(tmp);
		return tmp;
	}
	
	@Override
	public MutationList getSaveMutations() {
		if (getStatus() == UserStatus.NEW && isAlreadyStatusNormal() == false) {
			if (getCreationDate() == 0)
				// set creation time only if it is a new user
				setCreationDate(System.currentTimeMillis());
						
			return super.getSaveMutations()
					.addMutation(insertMutation(new UserUUID(this))) 
					.addMutation(insertMutation(new UserConfirmationUUID(this))) 
					.addInsertMutations(new ConfirmationEmailOperation(this)) 
					.addInsertMutations(new IndexUserOperation(this)) 
					;
		} else if (isAlreadyStatusNormal()) {
//			AssertUtils.assertTrue(getStatus() == UserStatus.NEW, "Making user already " + UserStatus.NORMAL + " allowed only for newly created users.");
			
//			setStatus(UserStatus.NORMAL);
//			setCreationDate(System.currentTimeMillis());
						
			return super.getSaveMutations()
					.addMutation(insertMutation(new UserUUID(this)))
					.addInsertMutations(new IndexUserOperation(this))
					;
		} else {
			return super.getSaveMutations()
					.addInsertMutations(new IndexUserOperation(this));
		}
	}
	
	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	public long getCreationDate() {
		return creationDate;
	}
	
	public String getCreationDateAsString() {
		return DateUtils.dateToStringShort(getCreationDate());
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setUserUUID(UUID userUUID) {
		this.userUUID = userUUID;
	}
	
	public void setUserUUID(String userUUID) {
		this.userUUID = new UUID(userUUID);
	}
	
	public UUID getUserId() {
		return userUUID;
	}
	
	@Override
	public MutationList getRemoveMutations() {
		return super.getRemoveMutations()
				.addMutation(UserUUID.removeMutation(this.getUserId()))
				.addMutation(UserConfirmationUUID.removeMutation(this.getUserId()))
				.addInsertMutations(new RemoveFromIndex(this));
	}

	public static User getUser(Email email) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, email.toString())), 
				DbObject.COLUMN_DATA, 
				User.class);
	}

	public static boolean comparePasswords(String plainPass, String hashedPass) {
		return HashUtils.checkBlowfishHash(plainPass, hashedPass);
	}

	public static UUID getUserId(Email userEmail) throws IOException {
		User user = getUser(userEmail);
		return user != null ? user.getUserId() : null;
	}

	public static boolean isAllowed(User loggedUser, String right) {
		return (loggedUser != null ? Allowed.getAllowed(loggedUser.getUserId(), right) != null : false);
	}

	public boolean isAlreadyStatusNormal() {
		return getStatus() != null
				&& getStatus().equals(UserStatus.NORMAL);
	}

	public void setAlreadyStatusNormal(boolean alreadyStatusNormal) {
		this.alreadyStatusNormal = alreadyStatusNormal;
	}

	public boolean isBlocked() {
		return getStatus() == UserStatus.BLOCKED;
	}

	public static Results getUsers(int count) throws IOException {
		return DbObject.getDatabaseProvider().getList(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new String[] {COLUMN_DATA}, 
				null, 
				count, 
				null, 
				null);
	}

	public void index() throws IOException {
		SearchManager.index(this);
	}

	public boolean hasRight(String right) {
		return Allowed.isAllowed(getUserId(), right);
	}

	public void block() throws ValidationException, IOException {
		// remove all the login cookies
		new RemoveAllCookiedUUIDJob(getUserId())
			.save();
		
		setStatus(UserStatus.BLOCKED);
		save();
	}
	
	public void unblock() throws ValidationException, IOException {
		setStatus(UserStatus.NORMAL);
		save();
	}

}
