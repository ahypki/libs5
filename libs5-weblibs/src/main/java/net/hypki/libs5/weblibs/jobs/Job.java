package net.hypki.libs5.weblibs.jobs;

import static net.hypki.libs5.db.db.weblibs.MutationType.INSERT;
import static net.hypki.libs5.db.db.weblibs.MutationType.REMOVE;
import static net.hypki.libs5.weblibs.uuid.TimeUUIDUtils.createTimeUUID;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationList;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;

/**
 * Cassandra: Keyspace.Job[Job].null.{UUIDTime}
 * @author ahypki
 *
 */
public abstract class Job extends WeblibsObject {
	
	public static final String COLUMN_FAMILY = "job";
	
	public static final String COLUMN_CHANNEL = "channel";
	
	public static final String COLUMN_TIME = "time";
	
	@NotNull
	@NotEmpty
	@Expose
	private String channel = null;
	
	/**
	 * Starting date for this job
	 */
	@Min(value = 0)
	@Expose
	private long time = 0;
	
	@Expose
	private long postponeMs = 0;
	
	@AssertValid
	@Expose
	private UUID id = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String clazz = null;
	
	private String logSeparately = null;
	
	public Job() {
		setClazz(getClass().getName());
		setChannel(WeblibsConst.CHANNEL_NORMAL);
		setCreationMs(System.currentTimeMillis());
		setId(UUID.random());
	}
		
	public Job(String channel) {
		setClazz(getClass().getName());
		setChannel(channel);
		setCreationMs(System.currentTimeMillis());
		setId(UUID.random());
	}
	
	public abstract void run() throws IOException, ValidationException;

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}

	@Override
	public TableSchema getTableSchema() {
		return new TableSchema(getColumnFamily())
				.addColumn(COLUMN_CHANNEL, 	ColumnType.STRING, 	true, true)
				.addColumn(COLUMN_PK, 		ColumnType.STRING, 	true, true)
				.addColumn(COLUMN_TIME, 	ColumnType.LONG, 	true)
				.addColumn(COLUMN_DATA, 	ColumnType.STRING)
				;
	}
	
	public static String buildKey(String channel, long createMs, String jobId) {
		return channel + "_" + createMs + "_" + jobId;
	}
	
	public static Object[] splitKey(String key) {
		Object[] ret = new Object[3];
		String[] splits = StringUtilities.split(key, '_');
		
		ret[0] = splits[0];
		ret[1] = NumberUtils.toLong(splits[1]);
		ret[2] = splits[2];
		
		return ret;
	}

	@Override
	public String getKey() {
		return buildKey(getChannel(), getCreationMs(), getId().getId());
//		return getChannel() + "_" + getCreationMs() + "_" + getId();
	}
	
	@Override
	public String toString() {
		return getKey();// String.format("%s %s", getClass().getName(), getCombinedKey());
	}
	
	public String getDescription() {
		return getClazz();
	}
	
	/**
	 * This ID defines if two different jobs (with different getId()) actually want to perfom the same job. If the getDuplicationId()
	 * ID is the same for them only one such a job will be executed. 
	 * @return
	 */
	public String getDuplicationId() {
		return getKey();// getId().getId() + "-" + getCreationMs();
	}
	
	@Override
	public boolean equals(Object obj) {
		return ((Job) obj).getId().equals(this.getId());
	}
	
	public Job setCreationMs(long creationMs) {
		this.time = creationMs;
		return this;
	}

	public long getCreationMs() {
		return time;
	}

	public Job setId(UUID id) {
		this.id = id;
		return this;
	}

	public UUID getId() {
		return id;
	}
	
	@Override
	public MutationList getSaveMutations() {
		MutationList muts = new MutationList();
		
		if (getPostponeMs() > 0) {
			muts.add(getRemoveMutations());
			setCreationMs(getCreationMs() + getPostponeMs());
			setPostponeMs(0);
		}
		
		muts
			.addMutation(new Mutation(INSERT, 
					WeblibsConst.KEYSPACE, 
					COLUMN_FAMILY, 
					new C3Where()
						.addClause(new C3Clause(COLUMN_CHANNEL, ClauseType.EQ, getChannel()))
						.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, getKey()))
						.addClause(new C3Clause(COLUMN_TIME, ClauseType.EQ, getCreationMs())),
					DbObject.COLUMN_DATA, 
					getData()
					));
//		muts
//			.addMutation(new Mutation(INSERT, 
//					WeblibsConst.KEYSPACE, 
//					COLUMN_FAMILY, 
//					new C3Where()
//						.addClause(new C3Clause(COLUMN_CHANNEL, ClauseType.EQ, getChannel()))
//						.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, getKey())),
//					COLUMN_CHANNEL, 
//					getChannel()
//					));
		return muts;
	}
		
	@Override
	public MutationList getRemoveMutations() {
		return new MutationList()
				.addMutation(new Mutation(REMOVE, 
						WeblibsConst.KEYSPACE, 
						COLUMN_FAMILY, 
						new C3Where()
							.addClause(new C3Clause(COLUMN_CHANNEL, ClauseType.EQ, getChannel()))
							.addClause(new C3Clause(COLUMN_PK, ClauseType.EQ, getKey())),
						DbObject.COLUMN_DATA, 
						getData()
						));
	}
		
	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannel() {
		return channel;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public long getPostponeMs() {
		return postponeMs;
	}

	public Job setPostponeMs(long postponeMs) {
		this.postponeMs = postponeMs;
		return this;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(getCreationMs());
	}
	
	@Override
	public Object save() throws ValidationException, IOException {
		if (getPostponeMs() > 0) {
			setCreationMs(getCreationMs() + getPostponeMs());
			setPostponeMs(0);
		}
		return super.save();
	}

	protected String getLogSeparately() {
		return logSeparately;
	}

	protected void setLogSeparately(String logSeparately) {
		this.logSeparately = logSeparately;
	}
}
