package net.hypki.libs5.weblibs;

import static net.hypki.libs5.db.weblibs.Settings.getSettings;
import static net.hypki.libs5.utils.json.JsonUtils.readBoolean;
import static net.hypki.libs5.utils.json.JsonUtils.readString;

import java.util.ArrayList;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.language.LanguageIds;
import net.hypki.libs5.weblibs.user.Email;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class WeblibsConst {
	
	public static 		String 				KEYSPACE = null;
	public static 		String 				KEYSPACE_LOWERCASE = null;
	
	public static 		String 				ROOTCONTEXT = null;
	
	public static 		String 				DOMAIN = null;
	
	public static final String 				VERSION = "0.2.1.3";
	
	public static 		String 				COOKIE_LOGIN = "lc";
	public static final String 				COOKIE_MSG_ERROR = "msgError";
	public static final String 				COOKIE_MSG_INFO = "msgInfo";

	public static final String 				QUERYSTRING_PAGE = "page";
	public static final String 				QUERYSTRING_FROM_MILISEC = "fromMS";
	public static final String 				QUERYSTRING_PARAM_LANGUAGE = "lg";
	public static final String 				QUERYSTRING_PARAM_IMG_ID = "imgId";
	public static final String 				PARAM_ITEM_ID = "itemId";
	public static final String 				PARAM_FIELD_ID = "fieldId";
	public static final String 				PARAM_FIELDS_IDS = "fieldsIds";
	public static final String 				PARAM_IS_TEMPLATE = "isTemplate";
	public static final String 				PARAM_USER_ID = "userId";
	public static final String 				PARAM_FRIEND_ID = "friendId";
	public static final String 				PARAM_FOLLOWER_ID = "followerId";
	public static final String 				QUERYSTRING_GROUP_NAME = "groupName";
	public static final String 				PARAM_TAGS = "tags";
	public static final String 				PARAM_TEMPLATE_ID = "templateId";
	public static final String 				PARAM_OLDER = "older";
	public static final String 				PARAM_NEWER = "newer";
	public static final String 				PARAM_NEWEST = "newest";
	public static final String 				PARAM_TO_USER = "to";
	public static final String 				PARAM_CONV_ID = "convId";
	public static final String 				PARAM_FILE_PATH = "filePath";
	public static final String 				PARAM_FILE_NAME = "fileName";
	public static final String 				PARAM_FOLDER_NAME = "folderName";
	public static final String 				PARAM_FOLDER_ID = "folderId";
	public static final String 				PARAM_FILE_ID = "fileId";
	public static final String 				PARAM_FUNCTION_ID = "functionId";
	public static final String 				PARAM_FUNCTION_NAME = "functionName";
	public static final String 				PARAM_SEARCH = "search";
	public static final String 				PARAM_SEARCH_ID = "searchId";
	public static final String 				PARAM_SEARCH_NAME = "searchName";
	public static final String 				PARAM_SEARCH_TYPE = "searchType";
	public static final String 				PARAM_PATH = "path";
	public static final String 				PARAM_REMINDER_DATE = "remDate";
	public static final String 				PARAM_REMINDER_HOUR = "remHour";
	public static final String 				PARAM_REMINDER_HOUR_ENABLED = "remHourEnabled";
	public static final String 				PARAM_REMINDER_BEFORE = "remBefore";
	public static final String 				PARAM_REMINDER_BEFORE_TYPE = "remBeforeType";
	public static final String 				PARAM_REMINDER_REPEAT_ENABLED = "remRepeatEnabled";
	public static final String 				PARAM_REMINDER_REPEAT = "remRepeat";
	public static final String 				PARAM_REMINDER_REPEAT_TYPE = "remRepeatType";

	public static final int 				FILE_BROWSER_MAX_SUBELEMENTS_PER_FOLDER = 1000;
		
	public static final boolean 			CACHE_GLOBALLY_ENABLED = false;
	
	public static final int			 		ITEM_MAX_FIELDS = 40;
	public static final int			 		ITEM_MAX_TAGS = 40;
	
	public static final LanguageIds 		DEFAULT_LANG_IF_EVERYTHING_FAILS = LanguageIds.PL;
	
	public static final int 				CASSANDRA_MIN_RELOAD_INTERVALS_MS = 5000;

	public static final String 				SEARCHABLE_APP_ID = "appId";
	public static final String 				SEARCHABLE_USER_ID = "userId";

	public static final int					TAGS_POPULAR_TAGS_TO_SHOW = 45;

	public static 		String				OPENID_RETURN_URL = null;
	public static 		boolean				OPENID_ENABLED = false;
	
	public static 		boolean 			REGISTRATION_ENABLED = false;
	
	public static final int 				JOB_PRODUCER_WAIT_FOR_NEW_JOBS_MS = 500;
	public static final int 				JOB_WORKER_NEW_JOBS_COUNT = 10000;

	
	public static final String CHANNEL_NORMAL = "NORMAL";
	
	public static 		boolean HTTPS_REDIRECT = false;
	
	public static final String WEBLIBS_SETTINGS_NAME = "weblibsSettings";
	
		
	static {
		long start = System.currentTimeMillis();
		
		// TODO all of this, or almost all of this read from Property
		
//		if (getSettings() != null) {
			KEYSPACE = readString(getSettings(), "Settings/Keyspace");
			KEYSPACE_LOWERCASE = KEYSPACE.toLowerCase();
			
			DOMAIN = readString(getSettings(), "Settings/Domain", "localhost:25001");
			
			REGISTRATION_ENABLED = readBoolean(getSettings(), "Settings/RegistrationEnabled", false);
			
			OPENID_RETURN_URL = readString(getSettings(), "Settings/OpenIDReturnURL");
			OPENID_ENABLED = readBoolean(getSettings(), "Settings/OpenIDEnabled", false);
			
	//		START_BROWSER = readBoolean(getSettings(), "Settings/StartBrowser", false);
			
			HTTPS_REDIRECT = readBoolean(getSettings(), "Settings/HttpsRedirect", false);
//		}
				
		LibsLogger.debug("WeblibsConst started in " + (System.currentTimeMillis() - start));
	}
}
