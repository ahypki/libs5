package net.hypki.libs5.weblibs.property;

import static net.hypki.libs5.weblibs.CacheManager.cacheInstance;

import java.io.IOException;
import java.io.Serializable;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.hypki.libs5.weblibs.language.LangValidatorAnnotation;
import net.hypki.libs5.weblibs.language.LanguageIds;
import net.hypki.libs5.weblibs.user.User;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

/**
 * Cassandra: {Keyspace}."Prop"[{appId}].{language}.{property_key}
 * @author ahypki
 *
 */
public class Property extends WeblibsObject implements Serializable {
	
	public static final String COLUMN_FAMILY = "prop";
	public static final String KEY_SYSTEM = "sys";
	
	public static final String NONE_BYTES = LanguageIds.__.toString();
		
	@LangValidatorAnnotation
	@Expose
	private LanguageIds lang = null;

	@NotNull
	@NotEmpty
	@Length(min = 1, max = 100000)
	@Expose
	private String propKey = null;
	
//	@NotNull
//	@NotEmpty
//	@Length(min = 1, max = 1000000)
	@Expose
	private String msg = null;
	
//	@NotNull
//	@NotEmpty
	@AssertValid
	@Expose
	private UUID userId = null;
	
	@Expose
	private String description = null;
	
//	@Expose
//	private boolean isSystem = false;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private SimpleDate lastUpdate = null;
	
	public Property() {
		
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return buildKey(getUserId(), getLangId(), getPropKey());
	}
	
//	@Override
//	public String getColumn() {
//		return buildColumn(getLangId(), getPropKey());
//	}
	
	public static String buildKey(UUID userId, LanguageIds lang, String propKey) {
		return String.format("%s_%s_%s", userId != null ? userId.getId() : KEY_SYSTEM, lang, propKey);
//		return String.format("%s%s", lang != null ? lang.toString(): null, propKey);
	}
	
	public Property setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public String getMsg() {
		return msg;
	}

	public Property setLangId(LanguageIds langId) {
		this.lang = langId;
		return this;
	}

	public LanguageIds getLangId() {
		return lang;
	}

	public Property setPropKey(String propKey) {
		this.propKey = propKey;
		return this;
	}

	public String getPropKey() {
		return propKey;
	}
		
//	public void setSystem(boolean isSystem) {
//		this.isSystem = isSystem;
//	}
//	
//	public boolean isSystem() {
//		return isSystem;
//	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		final String key = buildKey(userId, lang, propKey);
		cacheInstance().remove(Property.class.getSimpleName(), key);

		try {
			SearchManager.removeObject(getCombinedKey(), getColumnFamily());
		} catch (Throwable e) {
			LibsLogger.error(User.class, "Cannot ad-hoc remove from index", e);
		}
		
		super.remove();
	}
	
	@Override
	public Property save() throws ValidationException, IOException {
		setLastUpdate(SimpleDate.now());
		
		super.save();

		try {
			PropertyFactory.updateCache(this);
			
			index();
		} catch (Throwable e) {
			LibsLogger.error(User.class, "Cannot ad-hoc add to index", e);
		}
		
		return this;
	}
	
	public void index() throws IOException {
		SearchManager.index(this);
	}
	
	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public Property setDescription(String description) {
		this.description = description;
		return this;
	}

	public static void main(String[] args) {
		Property p = new Property();
		System.out.println(JsonUtils.objectToStringPretty(p));
	}

	public SimpleDate getLastUpdate() {
		return lastUpdate;
	}

	public Property setLastUpdate(SimpleDate lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}
}
