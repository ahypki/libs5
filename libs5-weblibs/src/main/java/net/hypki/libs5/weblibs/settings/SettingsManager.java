package net.hypki.libs5.weblibs.settings;

import java.util.HashSet;
import java.util.Set;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

import com.google.gson.JsonElement;

public class SettingsManager {
	public static boolean reloadSettings(JsonElement jsonSettings, boolean force, SimpleDate lastReloadDate) {
		boolean reloaded = false;
		Set<String> uniqueSettingsIDs = new HashSet<String>();
		
		for (JsonElement je : jsonSettings.getAsJsonArray()) {
			try {
				// changing automatically "description" from array into one string (if needed)
				je = JsonUtils.joinArrayIntoString(je.getAsJsonObject(), "description");
				
				Setting s = JsonUtils.fromJson(je, Setting.class);
				Setting fromDb = SettingFactory.getSetting(s.getId());
				
				if (uniqueSettingsIDs.contains(s.getId())) {
					LibsLogger.error(SettingsManager.class, "In settings file the ID=" + s.getId() + 
							" is duplicated, skipping this setting...");
					continue;
				}
				
				// TODO DEBUG
				if (fromDb != null)
					fromDb.index();
				
				if (force 
						|| fromDb == null
						|| (s != null && s.getLastEdit() == null)
						|| (s != null && s.getLastEdit().isG(lastReloadDate))) {
					if (s.getUserId() == null) {
						// system setting
						s.save();
					} else {
						if (s.getUserId().equals(UUID.ZERO)) {
							// reloading for every user
							for (User user : UserFactory.iterateUsers()) {
								s
									.setUserId(user.getUserId())
									.save();
							}
						} else {
							// reloading for one user only
							s.save();
						}
					}
					reloaded = true;
				}
				
			} catch (Exception e) {
				LibsLogger.error(SettingsManager.class, "Cannot reload settings", e, "for object ", je.toString());
				return reloaded;
			}
		}
		return reloaded;
	}
}
