package net.hypki.libs5.weblibs.user;

import static net.hypki.libs5.weblibs.CacheManager.cacheInstance;

import java.io.IOException;
import java.util.Iterator;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.WeblibsConst;

public class CookieUUIDFactory {

	public static CookieUUID getByUuid(UUID cookieUUID) throws IOException {
		// first search in cache
		CookieUUID c = null;
		if ((c = cacheInstance().get(CookieUUID.class, CookieUUID.class.getSimpleName(), cookieUUID.getId())) != null)
			return c;
	
		// get from DB
		return DbObject
					.getDatabaseProvider()
					.get(WeblibsConst.KEYSPACE, 
							CookieUUID.COLUMN_FAMILY, 
							new C3Where()
								.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, cookieUUID.getId())), 
							DbObject.COLUMN_DATA, 
							CookieUUID.class);
	}

	public static CookieUUID getCookieID(String cookieUUID) throws IOException {
		return getByUuid(new UUID(cookieUUID));
	}
	
	public static Iterable<CookieUUID> iterateCookieUUID(final UUID userId) {
		return new Iterable<CookieUUID>() {
			@Override
			public Iterator<CookieUUID> iterator() {
				return new Iterator<CookieUUID>() {
					private Iterator<CookieUUID> cookieUUIDIter = null;
					private CookieUUID next = null;
					
					private void init() {
						
					}
					
					@Override
					public CookieUUID next() {
						try {
							if (cookieUUIDIter == null)
								init();
							
							if (next == null)
								hasNext();
							
							return next;
						} finally {
							next = null;
						}
					}
					
					@Override
					public boolean hasNext() {
						if (cookieUUIDIter == null)
							init();
						
						while (cookieUUIDIter.hasNext()) {
							CookieUUID c = cookieUUIDIter.next();
							if (c.getUserUUID().equals(userId)) {
								next = c;
								return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}

	public static Iterable<CookieUUID> iterateCookieUUID() {
		return DbObject
				.iterateTable(CookieUUID.class, 
						WeblibsConst.KEYSPACE, 
						CookieUUID.COLUMN_FAMILY, 
						DbObject.COLUMN_DATA);
	}
}
