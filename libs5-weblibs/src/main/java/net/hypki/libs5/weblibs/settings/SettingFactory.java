package net.hypki.libs5.weblibs.settings;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.TableIterator;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.BooleanTerm;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class SettingFactory {
	
	public static String getSettingValueString(String settingKey, String valueName, String defaultValue) {
		try {
			SettingValue val = getSettingValue(settingKey, valueName);
			if (val != null)
				return val.getValueAsString() != null ? val.getValueAsString() : defaultValue;
			return defaultValue;
		} catch (IOException e) {
			LibsLogger.error(SettingFactory.class, "Cannot get " + settingKey + " " + valueName + 
					", returning default value", e);
			return defaultValue;
		}
	}
	
	public static int getSettingValueInt(String settingKey, String valueName, int defaultValue) {
		try {
			SettingValue val = getSettingValue(settingKey, valueName);
			if (val != null)
				return val.getValueAsInteger() != null ? val.getValueAsInteger() : defaultValue;
			return defaultValue;
		} catch (IOException e) {
			LibsLogger.error(SettingFactory.class, "Cannot get " + settingKey + " " + valueName + 
					", returning default value", e);
			return defaultValue;
		}
	}
	
	public static SettingValue getSettingValue(String settingKey, String valueName) throws IOException {
		final Setting sett = SettingFactory.getSetting(settingKey);
		if (sett != null) {
			SettingValue val = sett.getValue(valueName);
			if (val != null)
				return val;
		}
		return null;
	}
	
	public static Setting getSetting(String settingKey) throws IOException {
		return getSetting(null, settingKey);
	}
	
	public static void updateCache(Setting setting) {
		if (setting == null)
			return;
		
		CacheManager
			.cacheInstance()
			.put("setting-" + (setting.getUserId() != null ? setting.getUserId().getId() : ""), 
					setting.getKey(), 
					setting);
	}

	public static Setting getSetting(UUID userId, String settingKey) throws IOException {
		Setting settingCache = CacheManager
								.cacheInstance()
								.get(Setting.class, 
										"setting-" + (userId != null ? userId.getId() : ""), 
										userId != null ? settingKey + "_" + userId : settingKey);
		
		if (settingCache != null)
			return settingCache;
		
		C3Where where = new C3Where()
				.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, 
						userId != null ? settingKey + "_" + userId.getId() : settingKey));
			
		// TODO cache
		Setting sett = DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				Setting.COLUMN_FAMILY, 
				where, 
				DbObject.COLUMN_DATA, 
				Setting.class);
		
		// double check that the setting actually belongs to the expected user
		if (userId != null
				&& sett != null
				&& sett.getUserId().equals(userId) == false) {
			AssertUtils.assertTrue(false, "Setting was expected for the user ", userId, ", but it got the "
					+ "setting for the user ", sett.getUserId());
			return null;
		}
		
		if (sett != null)
			updateCache(sett);
		
		return sett;
	}
	
	public static Iterable<Setting> iterateSettings() {
		return new TableIterator<Setting>(WeblibsConst.KEYSPACE, 
				Setting.COLUMN_FAMILY, 
				DbObject.COLUMN_DATA, 
				null,
				Setting.class);
	}
	
	public static SearchResults<Setting> searchSettings(UUID userId, final String queryToParse, 
			Boolean system, int from, int size) throws IOException {
		return searchSettings(parseSettingsQuery(queryToParse, system, userId), from, size);
	}
	
	private static Query parseSettingsQuery(final String queryToParse, final Boolean system, final UUID userId) {
		Query q = Query.parseQuery(queryToParse);
		Query esQuery = new Query();
		
		for (Term term : q.getTerms()) {
			if (term.getField() == null) {
				if (term instanceof WildcardTerm) {
					WildcardTerm wild = (WildcardTerm) term;
					esQuery.addTerm(new Bracket(TermRequirement.MUST)
										.addTerm(new WildcardTerm("id", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm("name", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm("description", wild.getTerm(), TermRequirement.SHOULD))
							);
				}
			} else {
//				term.setField(datasetQueryPrefix + "meta.meta." + term.getField() + ".value");
//				esQuery.addTerm(term);
			}
		}
		
		if (q.getTerms().size() == 0)
			esQuery.addTerm(new WildcardTerm("name", "*", TermRequirement.SHOULD));
		
		if (userId != null)
			esQuery.addTerm(new StringTerm("userId.id", userId.getId(), TermRequirement.MUST));
		
		if (system != null)
			esQuery.addTerm(new BooleanTerm("system", system, TermRequirement.MUST));
		
//		esQuery.setSortBy("creationMs", SortOrder.DESCENDING);
		return esQuery;
	}
	
	private static SearchResults<Setting> searchSettings(Query q, int from, int size) throws IOException {
		return SearchManager.searchInstance().search(Setting.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				Setting.COLUMN_FAMILY.toLowerCase(), 
				q, 
				from, 
				size);
	}
}
