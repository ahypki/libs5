package net.hypki.libs5.weblibs.language;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import net.sf.oval.configuration.annotation.Constraint;
import net.sf.oval.exception.ConstraintsViolatedException;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD})
@Constraint(checkWith = LangValidator.class)
public @interface LangValidatorAnnotation {
	/**
	 * message to be used for the ConstraintsViolatedException
	 * 
	 * @see ConstraintsViolatedException
	 */
	String message() default "language id is invalid";
}
