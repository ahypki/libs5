package net.hypki.libs5.weblibs.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.NotImplementedException;

/**
 * Servlet implementation class GetFileServlet
 */
public class GetFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFileServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		getFromFile(response);
		getFromDatabase(request, response);
	}

	private void getFromDatabase(HttpServletRequest request, HttpServletResponse response) throws IOException {
		throw new NotImplementedException();
//		try {
//			RawData att2 = RawData.get(getLoggeedUser().getUserUUID(), new UUID("uuid"));
//			
//			OutputStream out = response.getOutputStream();
//			out.write(att2.getBlobAsBytes());
//			out.close();
//		} catch (NotFoundException e) {
//			LibsLogger.error("Cannot find RawData", e);
//		}
	}

	@SuppressWarnings("unused")
	private void getFromFile(HttpServletResponse response, String filename) throws FileNotFoundException, IOException {
		// Get the absolute path of the image
		ServletContext sc = getServletContext();
		//sc.getRealPath("image.gif");
		
		// Get the MIME type of the image
		String mimeType = sc.getMimeType(filename);
		if (mimeType == null) {
			sc.log("Could not get MIME type of " + filename);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		
		// Set content type
		response.setContentType(mimeType);
		
		// Set content size
		File file = new File(filename);
		response.setContentLength((int) file.length());
		
		// Open the file and output streams
		FileInputStream in = new FileInputStream(file);
		OutputStream out = response.getOutputStream();
		
		// Copy the contents of the file to the output stream
		byte[] buf = new byte[1024];
		int count = 0;
		while ((count = in.read(buf)) >= 0) {
			out.write(buf, 0, count);
		}
		in.close();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
