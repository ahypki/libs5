package net.hypki.libs5.weblibs.settings;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.NotNull;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

public class SettingValue {

	@Expose
	@NotNull
	private String name = null;
	
	@Expose
//	@NotNull
	private String vall = null; // TODO change name
	
	@Expose
	private String def = null;
	
	@Expose
	private String description = null;
	
	@Expose
//	@NotNull
	private SettingType type = null;
	
	public SettingValue() {
		
	}
	
	public SettingValue(String name, String value) {
		setName(name);
		setValue(value);
		setType(SettingType.STRING);
	}
	
	public SettingValue(String name, boolean value) {
		setName(name);
		setValue(value);
		setType(SettingType.BOOLEAN);
	}
	
	@Override
	public String toString() {
		return String.format("%s: %s (%s, %s), default: %s", 
				getName() != null 	? getName() 			: "<name>", 
				getValue() != null	? getValue() 			: "<value>", 
				getType() != null 	? getType().toString() 	: "<type>", 
				getDescription() != null ? getDescription()	: "<desc>", 
				getDef() != null	? getDef()				: "<def>");
	}

	public String getName() {
		return name;
	}

	public SettingValue setName(String name) {
		this.name = name;
		return this;
	}
	
	public Integer getValueAsInteger() {
		Object v = getValue();
		if (v instanceof Integer)
			return (Integer) v;
		else if (v instanceof Long)
			return (Integer) v;
		else
			return NumberUtils.toInt(v.toString());
	}
	
	public Double getValueAsDouble() {
		Object v = getValue();
		if (v instanceof Double)
			return (Double) v;
		else
			return NumberUtils.toDouble(v.toString());
	}
	
	public String getValueAsString() {
		Object v = getValue();
		if (v instanceof String)
			return (String) v;
		else
			return String.valueOf(v);
	}
	
	public boolean getValueAsBoolean() {
		Object v = getValue();
		if (v instanceof Boolean)
			return (Boolean) v;
		else
			return Boolean.getBoolean(String.valueOf(v));
	}
	
	public SimpleDate getValueAsDate() {
		Object v = getValue();
		if (v == null)
			return null;
		else if (v instanceof SimpleDate)
			return (SimpleDate) v;
		else {
			try {
				return SimpleDate.parse(String.valueOf(v));
			} catch (Throwable e) {
				LibsLogger.error(SettingValue.class, "Cannot convert " + v + " to date", e);
				return null;
			}
		}
	}
	
	public void determineType() {
		if (getValue() == null) {
			// do nothing
		} else if (getValue() instanceof String) {
			String tmp = getValueAsString();
			setType(tmp.indexOf('\n') >= 0 ? SettingType.MULTILINE : SettingType.STRING);
		} else if (getValue() instanceof Boolean) {
			setType(SettingType.BOOLEAN);
		} else if (getValue() instanceof Float) {
			setType(SettingType.FLOAT);
		} else if (getValue() instanceof Integer) {
			setType(SettingType.INTEGER);
		} else if (getValue() instanceof SimpleDate) {
			setType(SettingType.DATE);
		}
	}

	public Object getValue() {
		if (getType() == SettingType.BOOLEAN)
			return Boolean.valueOf(vall);
		else if (getType() == SettingType.FLOAT)
			return Double.valueOf(vall);
		else if (getType() == SettingType.DOUBLE)
			return Double.valueOf(vall);
		else if (getType() == SettingType.INTEGER)
			return Integer.valueOf(vall);
		else if (getType() == SettingType.STRING)
			return vall;
		else if (getType() == SettingType.MULTILINE)
			return vall;
		else if (getType() == SettingType.DATE)
			return SimpleDate.parse(vall);
		else if (getType() == SettingType.PASS)
			return vall;
		else if (getType() == null)
			return vall;
		else
			throw new NotImplementedException();
	}

	public SettingValue setValue(Object value) {
		this.vall = value != null ? String.valueOf(value) : null;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public SettingValue setDescription(String description) {
		this.description = description;
		return this;
	}

	public SettingType getType() {
		return type;
	}

	public SettingValue setType(SettingType type) {
		this.type = type;
		return this;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}

	public boolean isDescriptionEmpty() {
		return StringUtilities.nullOrEmpty(getDescription());
	}
}
