package net.hypki.libs5.weblibs.file;

import java.io.IOException;

import net.hypki.libs5.db.db.WeblibsPath;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.StorageManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;


public class RemoveFileJob extends Job {
	
	@NotNull
	@AssertValid
	@Expose
	private UUID userId = null;
	
	@NotNull
	@AssertValid
	@Expose
	private UUID fileId = null;

	public RemoveFileJob() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}
	
	public RemoveFileJob(BlobFile file) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setFile(file);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (StorageManager.remove(null, getFileId()) == false)
			throw new IOException("Cannot remove file's data");
	}

	public void setFile(BlobFile file) {
		setUserId(file.getUserUUID());
		setFileId(file.getFileId());		
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public WeblibsPath getPath() {
		return new WeblibsPath(getUserId().getId(), getFileId().getId());
	}

	public UUID getFileId() {
		return fileId;
	}

	public void setFileId(UUID fileId) {
		this.fileId = fileId;
	}
}
