package net.hypki.libs5.weblibs.user;

import java.io.IOException;

import net.hypki.libs5.utils.LibsLogger;
import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;

public class UserUniquityValidator extends AbstractAnnotationCheck<UserUniquityValidationAnnotation> {
	public UserUniquityValidator() {
		
	}

	@Override
	public boolean isSatisfied(Object validatedObject, Object valueToValidate, OValContext context, Validator validator) throws OValException {
		if (validatedObject instanceof User) {
//			try {
				// check whether there is in the database user with identical email
				User validatedUser = (User) validatedObject;
				
				if (validatedUser.getStatus() == UserStatus.NORMAL) {
					LibsLogger.debug(UserUniquityValidator.class, "User " + validatedUser.getEmail().getEmail() + " has status " + UserStatus.NORMAL + ", no need to check uniquity.");
					return true;
				}
				
				if (validatedUser.getStatus() == UserStatus.NEW)
					return true;
				
				if (validatedUser.getStatus() == UserStatus.UNCONFIRMED)
					return true;
				
				if (validatedUser.getStatus() == UserStatus.BLOCKED)
					return true;
				
//				User user = User.getUser(validatedUser.getEmail());
//				if (user != null && user.getUserId().equals(validatedUser.getUserId()) == false) {
//					return false;
//				} else
//					return true;
//			} catch (IOException e) {
//				LibsLogger.error(UserUniquityValidator.class, "Cannot check user", e);
//				return false;
//			}
		}
		return false;
	}
}
