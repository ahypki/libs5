package net.hypki.libs5.weblibs.search;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;


public class IndexUserOperation extends Job {
	
	@NotNull
	@AssertValid
	@Expose
	private User user = null;
	
	public IndexUserOperation() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}
	
	public IndexUserOperation(User user) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setUser(user);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		User user = getUser();
		
		if (user == null) {
			LibsLogger.debug("User does not exist anymore, skipping job");
			return;
		}
		
		SearchManager.index(getUser());
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
