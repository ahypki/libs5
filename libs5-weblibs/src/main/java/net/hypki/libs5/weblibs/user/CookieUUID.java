package net.hypki.libs5.weblibs.user;

import static net.hypki.libs5.weblibs.CacheManager.cacheInstance;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

/**
 * Cassandra: {Keyspace}.CookieUUID[{userUUID}].{appId}.{cookieUuid}
 * @author ahypki
 *
 */
public class CookieUUID extends WeblibsObject<CookieUUID> {
	public static final String COLUMN_FAMILY = "cookieuuid";
		
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userUUID = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String userEmail = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String userName = null;
	
	@NotNull
	@AssertValid
	@Expose
	private UUID cookieId = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String clientDomain = null;
	
	@Expose
	@Min(value = 1)
	private long expirationDate = 0;
	
	@Expose
	@Min(value = 1)
	private long lastLogin = 0;
	
	@Expose
	private boolean rememberMe = false;
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getUuid().toString();
	}
	
	@Override
	public CookieUUID save() throws ValidationException, IOException {
		saveToCacheOnly();
		return super.save();
	}
	
	public void saveToCacheOnly() {		
		cacheInstance().put(CookieUUID.class.getSimpleName(), this.getUuid().toString(), this);
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		// remove from cache
		cacheInstance().put(CookieUUID.class.getSimpleName(), this.getUuid().toString(), null);
		super.remove();
	}

	public void setUuid(UUID uuid) {
		this.cookieId = uuid;
	}

	public UUID getUuid() {
		return cookieId;
	}

	public void setExpirationDate(long date) {
		this.expirationDate = date;
	}

	public long getExpirationDate() {
		return expirationDate;
	}
	
	public void setUserUUID(UUID userUUID) {
		this.userUUID = userUUID;
	}

	public UUID getUserUUID() {
		return userUUID;
	}

	public void setClientDomain(String clientDomain) {
		this.clientDomain = clientDomain;
	}

	public String getClientDomain() {
		return clientDomain;
	}

	public void setLastLogin(long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public long getLastLogin() {
		return lastLogin;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public boolean isExpired() {
		if (getExpirationDate() > System.currentTimeMillis())
			return false;
		
		long newExpirationDate = getExpirationDate() + (isRememberMe() ? DateUtils.ONE_WEEK_MS : DateUtils.HALF_AN_HOUR_MS);
		if (newExpirationDate > System.currentTimeMillis()) {
			setExpirationDate(newExpirationDate);
			saveToCacheOnly();
			return false;
		}
		
		cacheInstance().remove(CookieUUID.class.getSimpleName(), getUuid().toString());
		return true;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
}
