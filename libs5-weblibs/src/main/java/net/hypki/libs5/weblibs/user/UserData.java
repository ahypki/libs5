package net.hypki.libs5.weblibs.user;

import java.awt.image.BufferedImage;
import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.image.ImageUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.WeblibsObject;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class UserData extends WeblibsObject {
	
	public static final String COLUMN_FAMILY = "userdata";
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userId = null;
	
	@NotNull
	@Expose
	private byte[] logo = null;

	public UserData() {
		
	}
	
	public static UserData getUserData(UUID userId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, userId.getId())), 
				DbObject.COLUMN_DATA, 
				UserData.class);
	}
	
	public static byte [] createLogo(byte[] originalImage) throws IOException {
		try {
			BufferedImage logoScaled = ImageUtils.getThumbnailAndCrop(ImageUtils.byteArrayToBufferedImage(originalImage), 80, 80, false, 80, 80);
			return ImageUtils.bufferedImageToByteArray(logoScaled);
		} catch (InterruptedException e) {
			throw new IOException("Cannot crop image for logo", e);
		}
	}

	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getUserId().toString();
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
}
