package net.hypki.libs5.weblibs.search;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.db.Indexable;
import net.hypki.libs5.weblibs.db.WeblibsObjectIndexable;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.settings.Setting;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class IndexJob extends Job {
	
	@Expose
//	@NotNull
//	@NotEmpty
	private String objectData = null;
	
	@Expose
//	@NotNull
//	@NotEmpty
	private UUID objectId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String objectClass = null;
	
	public IndexJob() {
		super(WeblibsConst.CHANNEL_NORMAL);
	}
	
	public IndexJob(String objectClass, UUID objectId) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setObjectClass(objectClass);
		setObjectId(objectId);
	}
	
	public IndexJob(WeblibsObjectIndexable o) {
		super(WeblibsConst.CHANNEL_NORMAL);
		setObjectData(o.getData());
		setObjectClass(o.getClass().getName());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getObjectClass().equals("net.beanscode.model.settings.Setting"))
			setObjectClass(Setting.class.getName());
		
		try {
			WeblibsObjectIndexable bo = null;
			
			if (notEmpty(getObjectData()))
				bo = (WeblibsObjectIndexable) JsonUtils.fromJson(getObjectData(), Class.forName(getObjectClass()));
			else
				bo = WeblibsObjectIndexable.getWeblibsObjectIndexable(getObjectId(), getObjectClass());
			
			if (bo != null) {
				((Indexable) bo).index();
				LibsLogger.debug(IndexJob.class, "Object of the class ", getObjectClass(), " ", bo.getKey(), " indexed successfully");
			}
		} catch (Throwable e) {
			LibsLogger.error(IndexJob.class, "Cannot index object of the class " + getObjectClass(), e);
		}
	}

	public String getObjectData() {
		return objectData;
	}

	public void setObjectData(String objectData) {
		this.objectData = objectData;
	}

	public String getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}

	public UUID getObjectId() {
		return objectId;
	}

	public void setObjectId(UUID objectId) {
		this.objectId = objectId;
	}

}
