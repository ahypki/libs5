package net.hypki.libs5.weblibs.jobs;

import java.io.Serializable;
import java.lang.reflect.Type;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class JobInstanceCreator implements JsonDeserializer<Job>, Serializable {
//	InstanceCreator<Field>, 
//	public Field createInstance(Type type) {
//		return new StringField();
//	}

	@Override
	public Job deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		try {
			String className = json.getAsJsonObject().get("clazz").getAsString();
			
			Class<? extends Job> clazz = (Class<? extends Job>) Class.forName(className);
			return JsonUtils.fromJson(json, clazz);
		} catch (ClassNotFoundException e) {
			LibsLogger.error(JobInstanceCreator.class, "Cannot find class or deserialize from json", e);
			return null;
		}
	}
}