/**
 *  Copyright 2011 Alexandru Craciun, Eyal Kaspi
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var StockApplication = function(quoteProvider) {

    this.quoteProvider = quoteProvider;
    this.stocks = [];
};
stjs.extend(StockApplication, null, [], function(constructor, prototype){
    prototype.stocks = null;
    prototype.quoteProvider = null;
    constructor.main = function(args) {
        $(window).ready(function(ev) {
            var a = new StockApplication(new YahooQuoteProvider());
            a.init();
            a.startUpdate(5000);
            return false;
        });
    };
    prototype.init = function() {
        var that = this;
        // add stock
        $("#form").submit(function(ev) {
            that.quoteProvider.updateStock($("#newStock").val(), function(data, status, xhr) {
                var response = data;
                var quote = response.query.results.quote;
                $(that.generateRow(quote)).appendTo("table tbody");
                that.stocks.push(quote.symbol);
            });
            return false;
        });
        // the remove stock listener
        $(".removeStock").live("click", function(ev) {
            var $tr = $(this).parents("tr");
            var index = $tr.parent().find("tr").index($tr);
            that.stocks.splice(index);
            $tr.remove();
            return false;
        });
    };
    /**
	 * starts the regular update from the quote provider
	 * 
	 * @param interval
	 */
    prototype.startUpdate = function(interval) {
        var that = this;
        // automatic update
        setInterval(function() {
            for (var i in that.stocks) {
                if (!(that.stocks).hasOwnProperty(i)) continue;
                that.quoteProvider.updateStock(that.stocks[i], function(data, status, xhr) {
                    var response = data;
                    var quote = response.query.results.quote;
                    $("table tbody tr:nth(" + that.getRowForStock(quote.symbol) + ")").replaceWith(that.generateRow(quote));
                    $("#timestamp").text(new Date().toString());
                });
            }
        }, interval);
    };
    prototype.getRowForStock = function(stock) {
        for (var i in this.stocks) {
            if (!(this.stocks).hasOwnProperty(i)) continue;
            if (this.stocks[i] == stock) {
                return parseInt(i);
            }
        }
        return -1;
    };
    prototype.generateRow = function(quote) {
        var last = parseFloat(quote.LastTradePriceOnly);
        var close = parseFloat(quote.PreviousClose);
        var tr = "<tr>";
        tr += "<td>" + quote.symbol + "</td>";
        var color = last >= close ? "red" : "green";
        tr += "<td style='color:" + color + "'>" + (last).toFixed(2) + "</td>";
        var change = (last - close);
        var changePercent = change / close;
        tr += "<td style='color:" + color + "'>" + (change).toFixed(2) + "(" + (changePercent).toFixed(2) + "%)</td>";
        tr += "<td><button class='removeStock'>Remove</button></td>";
        tr += "</tr>";
        return tr;
    };
    constructor.Response = function StockApplication$Response(){};
    stjs.extend(StockApplication.Response, null, [], function(constructor, prototype){
        prototype.query = null;
    }, {"query":"StockApplication.Query"});

    constructor.Query = function StockApplication$Query(){};
    stjs.extend(StockApplication.Query, null, [], function(constructor, prototype){
        prototype.results = null;
    }, {"results":"StockApplication.Results"});

    constructor.Results = function StockApplication$Results(){};
    stjs.extend(StockApplication.Results, null, [], function(constructor, prototype){
        prototype.quote = null;
    }, {"quote":"StockApplication.Quote"});

    constructor.Quote = function StockApplication$Quote(){};
    stjs.extend(StockApplication.Quote, null, [], function(constructor, prototype){
        prototype.LastTradePriceOnly = null;
        prototype.PreviousClose = null;
        prototype.symbol = null;
    }, {});

}, {"stocks":{name:"Array", arguments:[null]}, "quoteProvider":"QuoteProvider"});

if (!stjs.mainCallDisabled) StockApplication.main();
//@ sourceMappingURL=StockApplication.map