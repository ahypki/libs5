var Notifier = function(){};
stjs.extend(Notifier, null, [], function(constructor, prototype){
    constructor.info = function(title, msg) {
        $.gritter.add({"title": title, 
            "text": msg, 
            "image": "/resources/res.Resources/images/info.png"});
    };
    constructor.error = function(title, msg) {
        $.gritter.add({"title": title, 
            "text": msg, 
            "image": Const.ERROR_IMAGE});
    };
}, {});

//@ sourceMappingURL=Notifier.map