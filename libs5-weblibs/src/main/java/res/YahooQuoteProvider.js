var YahooQuoteProvider = function(){};
stjs.extend(YahooQuoteProvider, null, [QuoteProvider], function(constructor, prototype){
    prototype.updateStock = function(stock, listener) {
        $.ajax({
            url : "http://query.yahooapis.com/v1/public/yql?q=select%20symbol,%20LastTradePriceOnly,PreviousClose%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22" + stock + "%22)%0A%09%09&format=json&env=http%3A%2F%2Fdatatables.org%2Falltables.env&callback=?",
            dataType : "jsonp",
            success : listener
        });
    };
}, {});

//@ sourceMappingURL=YahooQuoteProvider.map