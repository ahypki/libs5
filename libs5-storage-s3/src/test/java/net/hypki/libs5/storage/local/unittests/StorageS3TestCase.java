package net.hypki.libs5.storage.local.unittests;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;
import net.hypki.libs5.storage.local.StorageS3;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.ini.Ini;

public class StorageS3TestCase extends TestCase {

	private StorageS3 s3 = null;
	
	public StorageS3TestCase() {
		
	}
	
	protected StorageS3 getS3Client() {
		if (s3 == null) {
			try {
				s3 = new StorageS3();
				
				Ini ini = new Ini(new FileExt("$HOME/.aws/credentials"));
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(StorageS3.INIT_AWS_ACCESS_KEY_ID, ini.getString(StorageS3.INIT_AWS_ACCESS_KEY_ID, null));
				params.put(StorageS3.INIT_AWS_SECRET_ACCESS_KEY, ini.getString(StorageS3.INIT_AWS_SECRET_ACCESS_KEY, null));
				params.put(StorageS3.INIT_REGION, ini.getString(StorageS3.INIT_REGION, null));
				
				s3.init(params);
			} catch (Throwable e) {
				LibsLogger.error(StorageS3TestCase.class, "Cannot initialize S3 client", e);
			}
		}
		return s3;
	}
}
