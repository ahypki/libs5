package net.hypki.libs5.storage.local.unittests;

import java.io.File;
import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;

import org.junit.Test;

public class StorageS3UT extends StorageS3TestCase {
	
	@Test
	public void testSaveRead() throws IOException {
		final String set = "libs5";
		final UUID fileId = new UUID("a6056f9bd88d056a2260c9396103464c");
		
		getS3Client().save(set, fileId, SystemUtils.getResourceAsStream("test.pdf"), null);
		
        FileUtils.saveToFile(getS3Client().read(set, fileId), 
        		new File("test-copy-" + System.currentTimeMillis() + ".pdf"));
	}
}
