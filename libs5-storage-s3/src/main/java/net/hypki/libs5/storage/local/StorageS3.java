package net.hypki.libs5.storage.local;

import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static net.hypki.libs5.utils.utils.NumberUtils.toDouble;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.storage.StorageProvider;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.LRUWeightedMap;
import net.hypki.libs5.utils.collections.LRUWeightedMapCallback;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;

public class StorageS3 implements StorageProvider, LRUWeightedMapCallback {
	
	public static final String INIT_AWS_ACCESS_KEY_ID = "aws_access_key_id";
	public static final String INIT_AWS_SECRET_ACCESS_KEY = "aws_secret_access_key";
	public static final String INIT_REGION = "region";
	public static final String INIT_CACHE_GB = "cache_gb";
	
	private static String aws_access_key_id = null;
	private static String aws_secret_access_key = null;
	private static String region = null;
	
	private static AmazonS3 s3client = null;
	
	private static Set<String> bucketsCreated = null;
	
	private static boolean useCache = true;
	
	private static StorageProvider cacheStorage = null;
	
	private static LRUWeightedMap cacheLRU = null; // TODO OPTY serialize to disk
	
	private static double cacheGB = 10;
	
	public void init(Map<String,Object> params) throws IOException {
		assertTrue(params.get(INIT_AWS_ACCESS_KEY_ID) != null, "Expected " + INIT_AWS_ACCESS_KEY_ID + " key in params for " + getClass().getSimpleName());
		assertTrue(params.get(INIT_AWS_SECRET_ACCESS_KEY) != null, "Expected " + INIT_AWS_SECRET_ACCESS_KEY + " key in params for " + getClass().getSimpleName());
		assertTrue(params.get(INIT_REGION) != null, "Expected " + INIT_REGION + " key in params for " + getClass().getSimpleName());
		
		if (params.containsKey(INIT_CACHE_GB) && params.get(INIT_CACHE_GB) != null)
			cacheGB = toDouble(valueOf(params.get(INIT_CACHE_GB)), 10.0);
		
		aws_access_key_id = params.get("aws_access_key_id").toString();
		aws_secret_access_key = params.get("aws_secret_access_key").toString();
		region = params.get("region").toString();
	};
	
	private AmazonS3 getS3Client() {
		if (s3client == null) {
			if (nullOrEmpty(aws_access_key_id, aws_secret_access_key))
				return null;
			
			AWSCredentials credentials = new BasicAWSCredentials(aws_access_key_id, aws_secret_access_key);
			
			s3client = AmazonS3ClientBuilder
					  .standard()
					  .withCredentials(new AWSStaticCredentialsProvider(credentials))
					  .withRegion(region)
					  .build();
		}
		return s3client;
	}
	
	private StorageProvider getCache() {
		if (cacheStorage == null) {
			cacheStorage = new StorageLocal();
		}
		return cacheStorage;
	}
	
	private LRUWeightedMap getCacheLRU() {
		if (cacheLRU == null) {
			cacheLRU = new LRUWeightedMap(cacheGB * 1_000_000_000.0 /*max bytes*/, this);			
		}
		return cacheLRU;
	}
	
	@Override
	public void onRemove(String key) {
		try {
			String [] parts = StringUtilities.split(key, '\n');
			remove(parts[0], new UUID(parts[1]));
		} catch (IOException e) {
			LibsLogger.error(StorageS3.class, "Cannot remove file from local cache", e);
		}
	}

	@Override
	public void shutdown() {
		getS3Client().shutdown();
		
		if (isUseCache())
			getCache().shutdown();
		LibsLogger.debug(StorageS3.class, "S3 storage shutdown");
	}
	
	@Override
	public long save(String set, UUID fileId, InputStream dataStream, MetaList metaList) throws IOException {
		createBucket(set);
		
		// saving to cache first
		getCache().save(set, fileId, dataStream, metaList);
		long size = getCache().size(set, fileId);
		getCacheLRU().put(set + "\n" + fileId.getId(), size);

		// uploading to S3
		ObjectMetadata om = new ObjectMetadata();
		om.setContentLength(size);
		
		if (metaList != null)
			for (Meta meta : metaList) {
				om.addUserMetadata(meta.getName(), meta.getAsString());
			}

		getS3Client().putObject(set, fileId.getId(), getCache().read(set, fileId), om);
		LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " saved to S3");
		
		// removing from cache if needed
		if (isUseCache() == false) {
			getCache().remove(set, fileId);
			getCacheLRU().remove(set + "\n" + fileId.getId());
			LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " removed from local cache");
		}
		
		return size;
	}

	@Override
	public boolean remove(String set, UUID fileId) throws IOException {
		getS3Client().deleteObject(set, fileId.getId());
		LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " removed from S3");
		
		if (isUseCache()) {
			getCache().remove(set, fileId);
			getCacheLRU().remove(set + "\n" + fileId.getId());
			LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " removed from local cache");
		}
		
		return true;
	}
	
	public List<Bucket> getBucketList() {
		return getS3Client().listBuckets();
	}
	
	public void createBucket(String name) {
		if (getBucketsCreated().contains(name))
			return;
		
		try {
			// check if bucket exists
			for (Bucket bucket : getBucketList()) {
				if (bucket.getName().equals(name)) {
					LibsLogger.debug(StorageS3.class, "Bucket already exists on S3");
					getBucketsCreated().add(name);
					return;
				}
			}
			
			// create bucket
			getS3Client().createBucket(name);
			getBucketsCreated().add(name);
			LibsLogger.debug(StorageS3.class, "Bucket ", name, " created on S3");
	    } catch (AmazonS3Exception e) {
	    	LibsLogger.error(StorageS3.class, "Cannot create bukcet " + name, e);
	    }
	}
	
	@Override
	public InputStream read(String set, UUID fileId) throws IOException {
		if (isUseCache()) {
			if (getCacheLRU().contains(set + "\n" + fileId.getId())) {
				try {
					LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " reading from local cache...");
					return getCache().read(set, fileId);
				} catch (Throwable e) {
					LibsLogger.error(StorageS3.class, "Cannot read from local cache, reverting to S3", e);
				}
			}
		}
		
		LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " reading from S3");
		S3Object s3object = getS3Client().getObject(set, fileId.getId());
        return s3object.getObjectContent();
	}

	@Override
	public boolean exist(String set, UUID fileId) throws IOException {
		if (isUseCache()) {
			if (getCacheLRU().contains(set + "\n" + fileId.getId())) {
				LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " exists in local cache");
				return true;
			}
		}
		
		LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " checking if exist in S3");
		boolean b = getS3Client().doesObjectExist(set, fileId.getId());
		if (b) {
			long size = size(set, fileId);
			getCacheLRU().put(set + "\n" + fileId.getId(), size);
		}
		return b;
	}

	@Override
	public long size(String set, UUID fileId) throws IOException {
		if (isUseCache()) {
			if (getCacheLRU().contains(set + "\n" + fileId.getId())) {
				LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " taking size from local cache");
				return (long) getCacheLRU().getWeight(set + "\n" + fileId.getId());
			}
		}
		
		LibsLogger.debug(StorageS3.class, "File ", set, " ", fileId, " checking size in S3");
		ObjectMetadata objectMetadata = getS3Client().getObjectMetadata(set, fileId.getId());
		return objectMetadata != null ? objectMetadata.getContentLength() : -1;
	}

	private static boolean isUseCache() {
		return useCache;
	}

	private static void setUseCache(boolean useCache) {
		StorageS3.useCache = useCache;
	}

	private static Set<String> getBucketsCreated() {
		if (bucketsCreated == null)
			bucketsCreated = new HashSet<String>();
		return bucketsCreated;
	}

	private static void setBucketsCreated(Set<String> bucketsCreated) {
		StorageS3.bucketsCreated = bucketsCreated;
	}
}
